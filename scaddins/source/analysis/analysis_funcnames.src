/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: analysis_funcnames.src,v $
 * $Revision: 1.29 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#include "analysis.hrc"


Resource RID_ANALYSIS_FUNCTION_NAMES
{

    String ANALYSIS_FUNCNAME_Workday
    {
        Text [ en-US ] = "WORKDAY";
    };

    String ANALYSIS_FUNCNAME_Yearfrac
    {
        Text [ en-US ] = "YEARFRAC";
    };

    String ANALYSIS_FUNCNAME_Edate
    {
        Text [ en-US ] = "EDATE";
    };

    String ANALYSIS_FUNCNAME_Weeknum
    {
        Text [ en-US ] = "WEEKNUM";
    };

    String ANALYSIS_FUNCNAME_Eomonth
    {
        Text [ en-US ] = "EOMONTH";
    };

    String ANALYSIS_FUNCNAME_Networkdays
    {
        Text [ en-US ] = "NETWORKDAYS";
    };

    String ANALYSIS_FUNCNAME_Amordegrc
    {
        Text [ en-US ] = "AMORDEGRC";
    };

    String ANALYSIS_FUNCNAME_Amorlinc
    {
        Text [ en-US ] = "AMORLINC";
    };

    String ANALYSIS_FUNCNAME_Accrint
    {
        Text [ en-US ] = "ACCRINT";
    };

    String ANALYSIS_FUNCNAME_Accrintm
    {
        Text [ en-US ] = "ACCRINTM";
    };

    String ANALYSIS_FUNCNAME_Received
    {
        Text [ en-US ] = "RECEIVED";
    };

    String ANALYSIS_FUNCNAME_Disc
    {
        Text [ en-US ] = "DISC";
    };

    String ANALYSIS_FUNCNAME_Duration
    {
        Text [ en-US ] = "DURATION";
    };

    String ANALYSIS_FUNCNAME_Effect
    {
        Text [ en-US ] = "EFFECT";
    };

    String ANALYSIS_FUNCNAME_Cumprinc
    {
        Text [ en-US ] = "CUMPRINC";
    };

    String ANALYSIS_FUNCNAME_Cumipmt
    {
        Text [ en-US ] = "CUMIPMT";
    };

    String ANALYSIS_FUNCNAME_Price
    {
        Text [ en-US ] = "PRICE";
    };

    String ANALYSIS_FUNCNAME_Pricedisc
    {
        Text [ en-US ] = "PRICEDISC";
    };

    String ANALYSIS_FUNCNAME_Pricemat
    {
        Text [ en-US ] = "PRICEMAT";
    };

    String ANALYSIS_FUNCNAME_Mduration
    {
        Text [ en-US ] = "MDURATION";
    };

    String ANALYSIS_FUNCNAME_Nominal
    {
        Text [ en-US ] = "NOMINAL";
    };

    String ANALYSIS_FUNCNAME_Dollarfr
    {
        Text [ en-US ] = "DOLLARFR";
    };

    String ANALYSIS_FUNCNAME_Dollarde
    {
        Text [ en-US ] = "DOLLARDE";
    };

    String ANALYSIS_FUNCNAME_Yield
    {
        Text [ en-US ] = "YIELD";
    };

    String ANALYSIS_FUNCNAME_Yielddisc
    {
        Text [ en-US ] = "YIELDDISC";
    };

    String ANALYSIS_FUNCNAME_Yieldmat
    {
        Text [ en-US ] = "YIELDMAT";
    };

    String ANALYSIS_FUNCNAME_Tbilleq
    {
        Text [ en-US ] = "TBILLEQ";
    };

    String ANALYSIS_FUNCNAME_Tbillprice
    {
        Text [ en-US ] = "TBILLPRICE";
    };

    String ANALYSIS_FUNCNAME_Tbillyield
    {
        Text [ en-US ] = "TBILLYIELD";
    };

    String ANALYSIS_FUNCNAME_Oddfprice
    {
        Text [ en-US ] = "ODDFPRICE";
    };

    String ANALYSIS_FUNCNAME_Oddfyield
    {
        Text [ en-US ] = "ODDFYIELD";
    };

    String ANALYSIS_FUNCNAME_Oddlprice
    {
        Text [ en-US ] = "ODDLPRICE";
    };

    String ANALYSIS_FUNCNAME_Oddlyield
    {
        Text [ en-US ] = "ODDLYIELD";
    };

    String ANALYSIS_FUNCNAME_Xirr
    {
        Text [ en-US ] = "XIRR";
    };

    String ANALYSIS_FUNCNAME_Xnpv
    {
        Text [ en-US ] = "XNPV";
    };

    String ANALYSIS_FUNCNAME_Intrate
    {
        Text [ en-US ] = "INTRATE";
    };

    String ANALYSIS_FUNCNAME_Coupncd
    {
        Text [ en-US ] = "COUPNCD";
    };

    String ANALYSIS_FUNCNAME_Coupdays
    {
        Text [ en-US ] = "COUPDAYS";
    };

    String ANALYSIS_FUNCNAME_Coupdaysnc
    {
        Text [ en-US ] = "COUPDAYSNC";
    };

    String ANALYSIS_FUNCNAME_Coupdaybs
    {
        Text [ en-US ] = "COUPDAYBS";
    };

    String ANALYSIS_FUNCNAME_Couppcd
    {
        Text [ en-US ] = "COUPPCD";
    };

    String ANALYSIS_FUNCNAME_Coupnum
    {
        Text [ en-US ] = "COUPNUM";
    };

    String ANALYSIS_FUNCNAME_Fvschedule
    {
        Text [ en-US ] = "FVSCHEDULE";
    };

    String ANALYSIS_FUNCNAME_Iseven
    {
        Text [ en-US ] = "ISEVEN";
    };

    String ANALYSIS_FUNCNAME_Isodd
    {
        Text [ en-US ] = "ISODD";
    };

    String ANALYSIS_FUNCNAME_Gcd
    {
        Text [ en-US ] = "GCD";
    };

    String ANALYSIS_FUNCNAME_Lcm
    {
        Text [ en-US ] = "LCM";
    };

    String ANALYSIS_FUNCNAME_Multinomial
    {
        Text [ en-US ] = "MULTINOMIAL";
    };

    String ANALYSIS_FUNCNAME_Seriessum
    {
        Text [ en-US ] = "SERIESSUM";
    };

    String ANALYSIS_FUNCNAME_Quotient
    {
        Text [ en-US ] = "QUOTIENT";
    };

    String ANALYSIS_FUNCNAME_Mround
    {
        Text [ en-US ] = "MROUND";
    };

    String ANALYSIS_FUNCNAME_Sqrtpi
    {
        Text [ en-US ] = "SQRTPI";
    };

    String ANALYSIS_FUNCNAME_Randbetween
    {
        Text [ en-US ] = "RANDBETWEEN";
    };

    String ANALYSIS_FUNCNAME_Besseli
    {
        Text [ en-US ] = "BESSELI";
    };

    String ANALYSIS_FUNCNAME_Besselj
    {
        Text [ en-US ] = "BESSELJ";
    };

    String ANALYSIS_FUNCNAME_Besselk
    {
        Text [ en-US ] = "BESSELK";
    };

    String ANALYSIS_FUNCNAME_Bessely
    {
        Text [ en-US ] = "BESSELY";
    };

    String ANALYSIS_FUNCNAME_Bin2Dec
    {
        Text [ en-US ] = "BIN2DEC";
    };

    String ANALYSIS_FUNCNAME_Bin2Hex
    {
        Text [ en-US ] = "BIN2HEX";
    };

    String ANALYSIS_FUNCNAME_Bin2Oct
    {
        Text [ en-US ] = "BIN2OCT";
    };

    String ANALYSIS_FUNCNAME_Delta
    {
        Text [ en-US ] = "DELTA";
    };

    String ANALYSIS_FUNCNAME_Dec2Bin
    {
        Text [ en-US ] = "DEC2BIN";
    };

    String ANALYSIS_FUNCNAME_Dec2Hex
    {
        Text [ en-US ] = "DEC2HEX";
    };

    String ANALYSIS_FUNCNAME_Dec2Oct
    {
        Text [ en-US ] = "DEC2OCT";
    };

    String ANALYSIS_FUNCNAME_Erf
    {
        Text [ en-US ] = "ERF";
    };

    String ANALYSIS_FUNCNAME_Erfc
    {
        Text [ en-US ] = "ERFC";
    };

    String ANALYSIS_FUNCNAME_Gestep
    {
        Text [ en-US ] = "GESTEP";
    };

    String ANALYSIS_FUNCNAME_Hex2Bin
    {
        Text [ en-US ] = "HEX2BIN";
    };

    String ANALYSIS_FUNCNAME_Hex2Dec
    {
        Text [ en-US ] = "HEX2DEC";
    };

    String ANALYSIS_FUNCNAME_Hex2Oct
    {
        Text [ en-US ] = "HEX2OCT";
    };

    String ANALYSIS_FUNCNAME_Imabs
    {
        Text [ en-US ] = "IMABS";
    };

    String ANALYSIS_FUNCNAME_Imaginary
    {
        Text [ en-US ] = "IMAGINARY";
    };

    String ANALYSIS_FUNCNAME_Impower
    {
        Text [ en-US ] = "IMPOWER";
    };

    String ANALYSIS_FUNCNAME_Imargument
    {
        Text [ en-US ] = "IMARGUMENT";
    };

    String ANALYSIS_FUNCNAME_Imcos
    {
        Text [ en-US ] = "IMCOS";
    };

    String ANALYSIS_FUNCNAME_Imdiv
    {
        Text [ en-US ] = "IMDIV";
    };

    String ANALYSIS_FUNCNAME_Imexp
    {
        Text [ en-US ] = "IMEXP";
    };

    String ANALYSIS_FUNCNAME_Imconjugate
    {
        Text [ en-US ] = "IMCONJUGATE";
    };

    String ANALYSIS_FUNCNAME_Imln
    {
        Text [ en-US ] = "IMLN";
    };

    String ANALYSIS_FUNCNAME_Imlog10
    {
        Text [ en-US ] = "IMLOG10";
    };

    String ANALYSIS_FUNCNAME_Imlog2
    {
        Text [ en-US ] = "IMLOG2";
    };

    String ANALYSIS_FUNCNAME_Improduct
    {
        Text [ en-US ] = "IMPRODUCT";
    };

    String ANALYSIS_FUNCNAME_Imreal
    {
        Text [ en-US ] = "IMREAL";
    };

    String ANALYSIS_FUNCNAME_Imsin
    {
        Text [ en-US ] = "IMSIN";
    };

    String ANALYSIS_FUNCNAME_Imsub
    {
        Text [ en-US ] = "IMSUB";
    };

    String ANALYSIS_FUNCNAME_Imsum
    {
        Text [ en-US ] = "IMSUM";
    };

    String ANALYSIS_FUNCNAME_Imsqrt
    {
        Text [ en-US ] = "IMSQRT";
    };

    String ANALYSIS_FUNCNAME_Complex
    {
        Text [ en-US ] = "COMPLEX";
    };

    String ANALYSIS_FUNCNAME_Oct2Bin
    {
        Text [ en-US ] = "OCT2BIN";
    };

    String ANALYSIS_FUNCNAME_Oct2Dec
    {
        Text [ en-US ] = "OCT2DEC";
    };

    String ANALYSIS_FUNCNAME_Oct2Hex
    {
        Text [ en-US ] = "OCT2HEX";
    };

    String ANALYSIS_FUNCNAME_Convert
    {
        Text [ en-US ] = "CONVERT";
    };

    String ANALYSIS_FUNCNAME_Factdouble
    {
        Text [ en-US ] = "FACTDOUBLE";
    };

};
















































