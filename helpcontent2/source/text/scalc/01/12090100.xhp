<?xml version="1.0" encoding="UTF-8"?>


<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: 12090100.xhp,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************-->
	
<helpdocument version="1.0">
<meta>
<topic id="textscalc0112090100xml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">Select Source</title>
<filename>/text/scalc/01/12090100.xhp</filename>
</topic>
<history>
<created date="2003-10-31T00:00:00">Sun Microsystems, Inc.</created>
<lastedited date="2004-07-26T10:32:32">converted from old format - fpe</lastedited>
</history>
</meta>
<body>
<bookmark xml-lang="en-US" branch="hid/SID_OPENDLG_PIVOTTABLE" id="bm_id4138161" localize="false"/><!-- HID added by script -->
<bookmark xml-lang="en-US" branch="hid/.uno:DataDataPilotRun" id="bm_id3153822" localize="false"/>
<paragraph role="heading" id="hd_id3153663" xml-lang="en-US" level="1" l10n="U" oldref="1">Select Source</paragraph>
<section id="datenpilot">
<paragraph role="paragraph" id="par_id3145119" xml-lang="en-US" l10n="U" oldref="2"><ahelp hid=".uno:DataDataPilotRun">Opens a dialog where you can select the source for your DataPilot table, and then create your table.</ahelp></paragraph>
</section>
<section id="howtoget">
  <embed href="text/scalc/00/00000412.xhp#dndpa"/>
</section>
<paragraph role="heading" id="hd_id3154760" xml-lang="en-US" level="2" l10n="U" oldref="5">Selection</paragraph>
<paragraph role="paragraph" id="par_id3150543" xml-lang="en-US" l10n="U" oldref="6">Select a data source for the DataPilot table.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC:RADIOBUTTON:RID_SCDLG_DAPITYPE:BTN_SELECTION" id="bm_id3154908" localize="false"/>
<paragraph role="heading" id="hd_id3148799" xml-lang="en-US" level="3" l10n="U" oldref="7">Current Selection</paragraph>
<paragraph role="paragraph" id="par_id3125865" xml-lang="en-US" l10n="U" oldref="8"><ahelp hid="SC:RADIOBUTTON:RID_SCDLG_DAPITYPE:BTN_SELECTION">Uses the selected cells as the data source for the DataPilot table.</ahelp></paragraph>
<paragraph role="note" id="par_id3150011" xml-lang="en-US" l10n="U" oldref="13">The data columns in the DataPilot table use the same number format as the first data row in the current selection.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC:RADIOBUTTON:RID_SCDLG_DAPITYPE:BTN_DATABASE" id="bm_id3149664" localize="false"/>
<paragraph role="heading" id="hd_id3147348" xml-lang="en-US" level="3" l10n="U" oldref="9">Data source registered in $[officename]</paragraph>
<paragraph role="paragraph" id="par_id3145271" xml-lang="en-US" l10n="U" oldref="10"><ahelp hid="SC:RADIOBUTTON:RID_SCDLG_DAPITYPE:BTN_DATABASE">Uses a table or query in a database that is registered in $[officename] as the data source for the DataPilot table.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC:RADIOBUTTON:RID_SCDLG_DAPITYPE:BTN_EXTERNAL" id="bm_id3153876" localize="false"/>
<paragraph role="heading" id="hd_id3146119" xml-lang="en-US" level="3" l10n="U" oldref="11">External source/interface</paragraph>
<paragraph role="paragraph" id="par_id3145647" xml-lang="en-US" l10n="U" oldref="12"><ahelp hid="SC:RADIOBUTTON:RID_SCDLG_DAPITYPE:BTN_EXTERNAL">Opens the <emph>External Source </emph>dialog where you can select the <link href="text/shared/00/00000005.xhp" name="OLAP">OLAP</link> data source for the DataPilot table.</ahelp></paragraph>
<paragraph role="paragraph" id="par_idN10670" xml-lang="en-US" l10n="NEW"><link href="text/scalc/01/12090102.xhp" name="DataPilot dialog">DataPilot dialog</link></paragraph>
</body>
</helpdocument>
