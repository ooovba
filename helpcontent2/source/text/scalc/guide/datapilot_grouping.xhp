<?xml version="1.0" encoding="UTF-8"?>



<!--
 ***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: datapilot_grouping.xhp,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->


		<helpdocument version="1.0">
<meta>
<topic id="textscalcguidedatapilot_groupingxml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">Grouping DataPilot Tables</title>
<filename>/text/scalc/guide/datapilot_grouping.xhp</filename>
</topic>
</meta>
<body>
<bookmark xml-lang="en-US" branch="index" id="bm_id4195684"><bookmark_value>grouping; DataPilot tables</bookmark_value>
<bookmark_value>DataPilot function;grouping table entries</bookmark_value>
<bookmark_value>ungrouping entries in DataPilot tables</bookmark_value>
</bookmark>
<paragraph role="heading" id="par_idN10643" xml-lang="en-US" level="1" l10n="NEW"><variable id="datapilot_grouping"><link href="text/scalc/guide/datapilot_grouping.xhp">Grouping DataPilot Tables</link>
</variable></paragraph><comment>see http://specs.openoffice.org/calc/compatibility/grouping.sxw</comment><paragraph role="paragraph" id="par_idN10661" xml-lang="en-US" l10n="NEW">The resulting DataPilot table can contain many different entries. By grouping the entries, you can improve the visible result.</paragraph>
<list type="ordered">
<listitem>
<paragraph role="paragraph" id="par_idN10667" xml-lang="en-US" l10n="NEW">Select a cell or range of cells in the DataPilot table.</paragraph>
</listitem>
<listitem>
<paragraph role="paragraph" id="par_idN1066B" xml-lang="en-US" l10n="CHG">Choose <emph>Data - Group and Outline - Group</emph>.</paragraph>
</listitem>
</list>
<paragraph role="paragraph" id="par_idN1066E" xml-lang="en-US" l10n="NEW">Depending on the format of the selected cells, either a new group field is added to the DataPilot table, or you see one of the two <link href="text/scalc/01/12090400.xhp">Grouping</link> dialogs, either for numeric values, or for date values.</paragraph>
<paragraph role="paragraph" id="par_id3328653" xml-lang="en-US" l10n="NEW">The DataPilot table must be organized in a way that grouping can be applied.</paragraph>
<list type="unordered">
<listitem>
<paragraph role="paragraph" id="par_idN10682" xml-lang="en-US" l10n="CHG">To remove a grouping, click inside the group, then choose <emph>Data - Group and Outline - Ungroup</emph>.</paragraph>
</listitem>
</list>
<section id="relatedtopics">
<embed href="text/scalc/guide/datapilot.xhp#datapilot"/>
<embed href="text/scalc/guide/datapilot_createtable.xhp#datapilot_createtable"/>
<embed href="text/scalc/guide/datapilot_filtertable.xhp#datapilot_filtertable"/>
<embed href="text/scalc/guide/datapilot_updatetable.xhp#datapilot_updatetable"/>
<embed href="text/scalc/guide/datapilot_tipps.xhp#datapilot_tipps"/>
<embed href="text/scalc/guide/datapilot_deletetable.xhp#datapilot_deletetable"/>
</section>
</body>
</helpdocument>
