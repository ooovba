<?xml version="1.0" encoding="UTF-8"?>
<helpdocument version="1.0">
	
<!--
***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: soffice2xmlhelp.xsl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->
 
	
<meta>
      <topic id="textscalcguidedatapilot_filtertablexml" indexer="include" status="PUBLISH">
         <title xml-lang="en-US" id="tit">Filtering DataPilot Tables</title>
         <filename>/text/scalc/guide/datapilot_filtertable.xhp</filename>
      </topic>
   </meta>
   <body>
<bookmark xml-lang="en-US" branch="index" id="bm_id3150792"><bookmark_value>DataPilot function; filtering tables</bookmark_value>
      <bookmark_value>filtering;DataPilot tables</bookmark_value>
</bookmark><comment>mw deleted "filters;"</comment>
<paragraph xml-lang="en-US" id="hd_id3150792" role="heading" level="1" l10n="U"
                 oldref="35"><variable id="datapilot_filtertable"><link href="text/scalc/guide/datapilot_filtertable.xhp" name="Filtering DataPilot Tables">Filtering DataPilot Tables</link>
</variable></paragraph>
      <paragraph xml-lang="en-US" id="par_id3153192" role="paragraph" l10n="U" oldref="36">You can use filters to remove unwanted data from a DataPilot table.</paragraph><comment>UFI: removed help id</comment>
<paragraph xml-lang="en-US" id="par_id3150441" role="paragraph" l10n="CHG" oldref="37">Click the <emph>Filter</emph> button in the sheet to call up the dialog for the filter conditions. Alternatively, call up the context menu of the DataPilot table and select the <emph>Filter</emph> command. The <link href="text/scalc/01/12090103.xhp" name="Filter"><emph>Filter</emph></link> dialog appears. Here you can filter the DataPilot table.</paragraph>
      <section id="relatedtopics">
         <embed href="text/scalc/guide/datapilot.xhp#datapilot"/>
         <embed href="text/scalc/guide/datapilot_createtable.xhp#datapilot_createtable"/>
         <embed href="text/scalc/guide/datapilot_edittable.xhp#datapilot_edittable"/>
         <embed href="text/scalc/guide/datapilot_updatetable.xhp#datapilot_updatetable"/>
         <embed href="text/scalc/guide/datapilot_grouping.xhp#datapilot_grouping"/>
         <embed href="text/scalc/guide/datapilot_tipps.xhp#datapilot_tipps"/>
         <embed href="text/scalc/guide/datapilot_deletetable.xhp#datapilot_deletetable"/>
      </section>
   </body>
</helpdocument>