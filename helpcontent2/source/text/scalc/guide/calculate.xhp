<?xml version="1.0" encoding="UTF-8"?>
<helpdocument version="1.0">
	
<!--
***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: soffice2xmlhelp.xsl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->
 
	
<meta>
      <topic id="textscalcguidecalculatexml" indexer="include" status="PUBLISH">
         <title xml-lang="en-US" id="tit">Calculating in Spreadsheets</title>
         <filename>/text/scalc/guide/calculate.xhp</filename>
      </topic>
   </meta>
   <body>
<bookmark xml-lang="en-US" branch="index" id="bm_id3150791"><bookmark_value>spreadsheets; calculating</bookmark_value>
      <bookmark_value>calculating; spreadsheets</bookmark_value>
      <bookmark_value>formulas; calculating</bookmark_value>
</bookmark><comment>mw changed "formulas;..." entry</comment>
<paragraph xml-lang="en-US" id="hd_id3150791" role="heading" level="1" l10n="U"
                 oldref="25"><variable id="calculate"><link href="text/scalc/guide/calculate.xhp" name="Calculating in Spreadsheets">Calculating in Spreadsheets</link>
</variable></paragraph>
      <paragraph xml-lang="en-US" id="par_id3146120" role="paragraph" l10n="CHG" oldref="6">The following is an example of a calculation in $[officename] Calc.</paragraph>
      <list type="ordered">
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3153951" role="listitem" l10n="CHG" oldref="7">Click in a cell, and type a number</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_idN10656" role="listitem" l10n="NEW">Press <item type="keycode">Enter</item>.</paragraph>
            <paragraph xml-lang="en-US" id="par_idN1065D" role="listitem" l10n="NEW">The cursor moves down to the next cell.</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3155064" role="listitem" l10n="CHG" oldref="9">Enter another number.</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_idN1066F" role="listitem" l10n="NEW">Press the <item type="keycode">Tab</item> key.</paragraph>
            <paragraph xml-lang="en-US" id="par_idN10676" role="listitem" l10n="NEW">The cursor moves to the right into the next cell.</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3154253" role="listitem" l10n="CHG" oldref="10">Type in a formula, for example, <item type="literal">=A3 * A4 / 100.</item>
</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_idN1068B" role="listitem" l10n="NEW"> Press <item type="keycode">Enter</item>.</paragraph>
            <paragraph xml-lang="en-US" id="par_id3147343" role="listitem" l10n="U" oldref="20">The result of the formula appears in the cell. If you want, you can edit the formula in the input line of the Formula bar.</paragraph>
            <paragraph xml-lang="en-US" id="par_id3155378" role="listitem" l10n="U" oldref="12">When you edit a formula, the new result is calculated automatically.</paragraph>
         </listitem>
      </list>
      <section id="relatedtopics">
         <embed href="text/scalc/guide/calc_date.xhp#calc_date"/>
         <embed href="text/scalc/guide/calc_series.xhp#calc_series"/>
         <embed href="text/scalc/guide/calc_timevalues.xhp#calc_timevalues"/>
         <embed href="text/scalc/guide/cellreferences.xhp#cellreferences"/>
      </section>
   </body>
</helpdocument>