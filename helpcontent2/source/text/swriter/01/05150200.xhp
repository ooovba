<?xml version="1.0" encoding="UTF-8"?>
<helpdocument version="1.0">
	
<!--
***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: soffice2xmlhelp.xsl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->
 
	
<meta>
<topic id="textswriter0105150200xml" indexer="include">
<title id="tit" xml-lang="en-US">Apply</title>
<filename>/text/swriter/01/05150200.xhp</filename>
</topic>
</meta>
<body>
<section id="dokument">
<bookmark xml-lang="en-US" branch="hid/FN_AUTOFORMAT_APPLY" id="bm_id9349581" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/.uno:AutoFormatApply" id="bm_id3154656" localize="false"/>
<bookmark xml-lang="en-US" branch="index" id="bm_id5028839"><bookmark_value>automatic heading formatting</bookmark_value>
</bookmark>
<bookmark xml-lang="en-US" branch="index" id="bm_id"><bookmark_value>AutoFormat function;headings</bookmark_value>
<bookmark_value>headings;automatic</bookmark_value>
<bookmark_value>separator lines;AutoFormat function</bookmark_value>
</bookmark>
<paragraph role="heading" id="hd_id3155962" xml-lang="en-US" level="1" l10n="U" oldref="1"><link href="text/swriter/01/05150200.xhp" name="Apply">Apply</link></paragraph>
<paragraph role="paragraph" id="par_id3149871" xml-lang="en-US" l10n="CHG" oldref="2"><ahelp hid=".uno:AutoFormatApply">Automatically formats the file according to the options that you set under <emph>Tools - AutoCorrect</emph>
<emph> Options</emph>.</ahelp></paragraph>
</section>
<section id="howtoget">
<embed href="text/swriter/00/00000405.xhp#autoformat2"/>
</section>
<embed href="text/swriter/guide/auto_off.xhp#auto_off"/>
<paragraph role="paragraph" id="par_id3147404" xml-lang="en-US" l10n="U" oldref="15">When you apply automatic formats, the following rules apply:</paragraph>
<paragraph role="heading" id="hd_id3155625" xml-lang="en-US" level="2" l10n="U" oldref="3">AutoCorrect for Headings</paragraph>
<paragraph role="paragraph" id="par_id3154505" xml-lang="en-US" l10n="U" oldref="4">A paragraph is formatted as a heading when the following conditions are met:</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_id3145241" xml-lang="en-US" l10n="U" oldref="19">paragraph begins with a capital letter</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3148386" xml-lang="en-US" l10n="U" oldref="20">paragraph does not end with a punctuation mark</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3150564" xml-lang="en-US" l10n="U" oldref="21">empty paragraph above and below the paragraph</paragraph>
</listitem>
</list>
<paragraph role="heading" id="hd_id3149030" xml-lang="en-US" level="2" l10n="U" oldref="8">AutoCorrect for Bullets / Numbering</paragraph>
<paragraph role="paragraph" id="par_id3156316" xml-lang="en-US" l10n="U" oldref="9">To create a bulleted list, type a hyphen (-), star (*), or plus sign (+), followed by a space or tab at the beginning of a paragraph.</paragraph>
<paragraph role="paragraph" id="par_id3150763" xml-lang="en-US" l10n="U" oldref="18">To create a numbered list, type a number followed by a period (.), followed by a space or tab at the beginning of a paragraph. </paragraph>
<paragraph role="note" id="par_id3147507" xml-lang="en-US" l10n="U" oldref="16">Automatic numbering is only applied to paragraphs formatted with the <emph>Standard</emph>, <emph>Text body</emph> or <emph>Text body indent</emph> paragraph styles.</paragraph>
<paragraph role="heading" id="hd_id3152941" xml-lang="en-US" level="2" l10n="U" oldref="10">AutoCorrect for Separator Lines</paragraph>
<paragraph role="paragraph" id="par_id3154105" xml-lang="en-US" l10n="U" oldref="11">If you type three or more hyphens (---), underscores (___) or equal signs (===) on line and then press Enter, the paragraph is replaced by a horizontal line as wide as the page. The line is actually the <link href="text/shared/01/05030500.xhp" name="lower border">lower border</link> of the preceding paragraph. The following rules apply:</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_id3153530" xml-lang="en-US" l10n="U" oldref="12">Three hyphens (-) yield a single line (0.05 pt thick, gap 0.75 mm).</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3154477" xml-lang="en-US" l10n="U" oldref="13">Three underscore (_) yield a single line (1 pt thick, gap 0.75 mm).</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3150982" xml-lang="en-US" l10n="U" oldref="14">Three equal signs (=) yield a double line (1.10 pt thick, gap 0.75 mm).</paragraph>
</listitem>
</list>
</body>
</helpdocument>
