<?xml version="1.0" encoding="UTF-8"?>
<helpdocument version="1.0">
	
<!--
***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: soffice2xmlhelp.xsl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->
 
	
<meta>
      <topic id="textswriterguideusing_thesaurusxml" indexer="include">
         <title xml-lang="en-US" id="tit">Thesaurus</title>
         <filename>/text/swriter/guide/using_thesaurus.xhp</filename>
      </topic>
   </meta>
   <body>
<bookmark xml-lang="en-US" branch="index" id="bm_id3145576"><bookmark_value>thesaurus; related words</bookmark_value>
      <bookmark_value>related words in thesaurus</bookmark_value>
      <bookmark_value>spelling in thesaurus</bookmark_value>
      <bookmark_value>dictionaries; thesaurus</bookmark_value>
      <bookmark_value>lexicon, see thesaurus</bookmark_value>
      <bookmark_value>synonyms in thesaurus</bookmark_value>
      <bookmark_value>searching;synonyms</bookmark_value>
</bookmark>
<paragraph xml-lang="en-US" id="hd_id3145576" role="heading" level="1" l10n="U"
                 oldref="15"><variable id="using_thesaurus"><link href="text/swriter/guide/using_thesaurus.xhp" name="Thesaurus">Thesaurus</link>
</variable></paragraph>
      <paragraph xml-lang="en-US" id="par_id3149820" role="paragraph" l10n="U" oldref="16">You can use the thesaurus to look up synonyms, and related words.</paragraph>
      <list type="ordered">
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3155920" role="listitem" l10n="U" oldref="17">Click in the word that you want to look up.</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3155867" role="listitem" l10n="CHG" oldref="39">Choose <emph>Tools - Language - Thesaurus</emph>, or press <switchinline select="sys"><caseinline select="MAC">Command
</caseinline><defaultinline>Ctrl</defaultinline></switchinline>+F7.</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3149848" role="listitem" l10n="U" oldref="40">In the <item type="menuitem">Meaning</item>
               <emph/>list, select the definition that matches the context of the word.</paragraph>
         </listitem>
      </list>
      <list type="ordered" startwith="4">
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3153136" role="listitem" l10n="U" oldref="45">Select the replacement word in the <item type="menuitem">Synonym</item>
               <emph/>list.</paragraph>
         </listitem>
         <listitem>
            <paragraph xml-lang="en-US" id="par_id3149644" role="listitem" l10n="U" oldref="46">Click <emph>OK</emph>.</paragraph>
         </listitem>
      </list>
      <paragraph xml-lang="en-US" id="par_id3145113" role="note" l10n="U" oldref="18">To look up the word in a different language, click <item type="menuitem">Language</item>
         <emph/>in the<emph/>
         <item type="menuitem">Thesaurus</item> dialog, select one of the installed languages, and then click <item type="menuitem">OK</item>. In the <item type="menuitem">Thesaurus</item> dialog, click <item type="menuitem">Search</item>. The thesaurus may not be available for all installed languages.</paragraph>
      <paragraph xml-lang="en-US" id="par_id3156263" role="paragraph" l10n="U" oldref="19">If you applied a different language setting to individual words, or paragraphs, the thesaurus for the applied language is used.</paragraph>
      <embed href="text/shared/00/00000004.xhp#related"/>
      <embed href="text/swriter/guide/spellcheck_dialog.xhp#spellcheck_dialog"/>
      <embed href="text/swriter/guide/using_hyphen.xhp#using_hyphen"/>
      <paragraph xml-lang="en-US" id="par_id3154392" role="paragraph" l10n="U" oldref="41"><link href="text/shared/01/06020000.xhp" name="Thesaurus">Thesaurus</link></paragraph>
   </body>
</helpdocument>