<?xml version="1.0" encoding="UTF-8"?>


<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: hyperlink_search.xhp,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************-->
	
<helpdocument version="1.0">
<meta>
<topic id="textsharedguidehyperlink_searchxml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">Searching With the Hyperlink Bar</title>
<filename>/text/shared/guide/hyperlink_search.xhp</filename>
</topic>
<history>
<created date="2003-10-31T00:00:00">Sun Microsystems, Inc.</created>
<lastedited date="2004-10-19T08:59:12">converted from old format - fpe
checked - yj</lastedited>
</history>
</meta>
<body>
<bookmark xml-lang="en-US" branch="index" id="bm_id3150789"><bookmark_value>Internet; starting searches</bookmark_value>
<bookmark_value>searching;Internet</bookmark_value>
</bookmark>
<paragraph role="heading" id="hd_id3150789" xml-lang="en-US" level="1" l10n="U" oldref="16"><variable id="hyperlink_search"><link href="text/shared/guide/hyperlink_search.xhp" name="Searching With the Hyperlink Bar">Searching With the Hyperlink Bar</link>
</variable></paragraph>
<paragraph role="paragraph" id="par_id3154751" xml-lang="en-US" l10n="CHG" oldref="17">The <emph>Hyperlink</emph> Bar can be used to search for the selected text in the Internet.</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_idN10620" xml-lang="en-US" l10n="CHG">Choose <emph>View - Toolbars - Hyperlink Bar</emph> to open the Hyperlink Bar.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3143267" xml-lang="en-US" l10n="U" oldref="18">Enter the text you want to find in the <emph>URL Name</emph> field.</paragraph>
<paragraph role="listitem" id="par_id3150774" xml-lang="en-US" l10n="CHG" oldref="23">If you select text in your document, the text is automatically entered.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3153662" xml-lang="en-US" l10n="U" oldref="19">Click the <emph>Find</emph> icon to open a submenu.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3148538" xml-lang="en-US" l10n="U" oldref="20">Select your preferred search engine from the list.</paragraph>
</listitem>
</list>
<paragraph role="paragraph" id="par_id3143271" xml-lang="en-US" l10n="CHG" oldref="21">$[officename] opens your Web Browser, which calls the selected Internet search engine. After a short time you see the result of your search on screen.</paragraph>
<paragraph role="tip" id="par_idN10657" xml-lang="en-US" l10n="NEW">Choose <link href="text/shared/optionen/01020200.xhp" name="Search function in the Hyperlink Bar"><emph>Tools - Options - Internet - Search</emph></link> to define the search options.</paragraph>
<embed href="text/shared/00/00000004.xhp#related"/>
<paragraph role="paragraph" id="par_id3159157" xml-lang="en-US" l10n="U" oldref="24"><link href="text/shared/main0209.xhp" name="Hyperlink Bar">Hyperlink Bar</link></paragraph>
</body>
</helpdocument>
