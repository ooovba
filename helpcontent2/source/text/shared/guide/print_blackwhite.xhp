<?xml version="1.0" encoding="UTF-8"?>


<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: print_blackwhite.xhp,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************-->
	
<helpdocument version="1.0">
<meta>
<topic id="textsharedguideprint_blackwhitexml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">Printing in Black and White</title>
<filename>/text/shared/guide/print_blackwhite.xhp</filename>
</topic>
<history>
<created date="2003-10-31T00:00:00">Sun Microsystems, Inc.</created>
<lastedited date="2004-09-08T12:15:36">converted from old format - fpe</lastedited>
</history>
</meta>
<body>
<bookmark xml-lang="en-US" branch="index" id="bm_id3150125"><bookmark_value>printing; black and white</bookmark_value>
<bookmark_value>black and white printing</bookmark_value>
<bookmark_value>colors; not printing</bookmark_value>
<bookmark_value>text; printing in black</bookmark_value>
</bookmark>
<paragraph role="heading" id="hd_id3150125" xml-lang="en-US" level="1" l10n="U" oldref="1"><variable id="print_blackwhite"><link href="text/shared/guide/print_blackwhite.xhp" name="Printing in Black and White">Printing in Black and White</link>
</variable></paragraph>
<paragraph role="heading" id="hd_id3150499" xml-lang="en-US" level="2" l10n="U" oldref="3">Printing text and graphics in black and white</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_id3149346" xml-lang="en-US" l10n="U" oldref="4">Choose <emph>File - Print</emph>. The <emph>Print</emph> dialog opens.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3155892" xml-lang="en-US" l10n="U" oldref="5">Click on <emph>Properties</emph>. This opens the Properties dialog for your printer.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3145313" xml-lang="en-US" l10n="U" oldref="6">Select the option to print in black and white. For further information, refer to the user's manual of your printer.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3153345" xml-lang="en-US" l10n="U" oldref="7">Confirm the <emph>Properties</emph> dialog and click <emph>OK</emph> in the <emph>Print</emph> dialog.</paragraph>
<paragraph role="listitem" id="par_id3156113" xml-lang="en-US" l10n="U" oldref="8">The current document will be printed in black and white.</paragraph>
</listitem>
</list>
<paragraph role="heading" id="hd_id3147653" xml-lang="en-US" level="2" l10n="U" oldref="9">Printing in black and white in <item type="productname">%PRODUCTNAME</item> Impress and <item type="productname">%PRODUCTNAME</item> Draw</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_id3149233" xml-lang="en-US" l10n="CHG" oldref="10">Choose <emph>Tools - Options - </emph>
<emph>
<item type="productname">%PRODUCTNAME</item>
</emph>
<emph> Impress </emph>or <emph>Tools - Options - </emph>
<emph>
<item type="productname">%PRODUCTNAME</item>
</emph>
<emph> Draw</emph>, as appropriate.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3150275" xml-lang="en-US" l10n="U" oldref="11">Then choose <emph>Print</emph>.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3147573" xml-lang="en-US" l10n="U" oldref="12">Under <emph>Quality,</emph> select either <emph>Grayscale</emph> or <emph>Black &amp; white</emph> and click <emph>OK</emph>.</paragraph>
<paragraph role="listitem" id="par_id3154307" xml-lang="en-US" l10n="U" oldref="13">When either of these options is selected, all presentations or drawings will be printed without color. If you only want to print in black for the <emph>current</emph> print job, select the option in <emph>File - Print - Options</emph>.</paragraph>
<paragraph role="listitem" id="par_id3149786" xml-lang="en-US" l10n="U" oldref="15">
<emph>Grayscale</emph> converts all colors to a maximum of 256 gradations from black to white. All text will be printed in black. A background set by <emph>Format - Page - Background</emph> will not be printed.</paragraph>
<paragraph role="listitem" id="par_id3145610" xml-lang="en-US" l10n="U" oldref="16">
<emph>Black &amp; white</emph> converts all colors into the two values black and white. All borders around objects are printed black. All text will be printed in black. A background set by <emph>Format - Page - Background</emph> will not be printed.</paragraph>
</listitem>
</list>
<paragraph role="heading" id="hd_id3153896" xml-lang="en-US" level="2" l10n="U" oldref="17">Printing only text in black and white</paragraph>
<paragraph role="paragraph" id="par_id3147559" xml-lang="en-US" l10n="U" oldref="18">In <item type="productname">%PRODUCTNAME</item> Writer you can choose to print color-formatted text in black and white. You can specify this either for all subsequent text documents to be printed, or only for the current printing process.</paragraph>
<paragraph role="heading" id="hd_id3150358" xml-lang="en-US" level="3" l10n="U" oldref="19">Printing all text documents with black and white text</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_id3150541" xml-lang="en-US" l10n="U" oldref="20">Choose <emph>Tools - Options - </emph>
<emph>
<item type="productname">%PRODUCTNAME</item>
</emph>
<emph> Writer </emph>or <emph>Tools - Options - </emph>
<emph>
<item type="productname">%PRODUCTNAME</item>
</emph>
<emph> Writer/Web</emph>.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3147084" xml-lang="en-US" l10n="U" oldref="21">Then choose <emph>Print</emph>.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3154910" xml-lang="en-US" l10n="U" oldref="22">Under <emph>Contents,</emph> mark <emph>Print black</emph> and click <emph>OK</emph>.</paragraph>
<paragraph role="listitem" id="par_id3144762" xml-lang="en-US" l10n="U" oldref="23">All text documents or HTML documents will be printed with black text.</paragraph>
</listitem>
</list>
<paragraph role="heading" id="hd_id3148920" xml-lang="en-US" level="3" l10n="U" oldref="24">Printing the current text document with black and white text</paragraph>
<list type="ordered">
<listitem>
<paragraph role="listitem" id="par_id3152933" xml-lang="en-US" l10n="U" oldref="25">Choose <emph>File - Print</emph>. This opens the <emph>Print</emph> dialog.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3149560" xml-lang="en-US" l10n="U" oldref="26">Click the <emph>Options</emph> button. This opens the <emph>Printer Options</emph> dialog.</paragraph>
</listitem>
<listitem>
<paragraph role="listitem" id="par_id3149667" xml-lang="en-US" l10n="U" oldref="27">Under <emph>Contents</emph> mark <emph>Print black</emph> and click <emph>OK</emph>.</paragraph>
</listitem>
</list>
<list type="ordered" startwith="4">
<listitem>
<paragraph role="listitem" id="par_id3155308" xml-lang="en-US" l10n="U" oldref="28">The text document or HTML document now being printed will be printed in black text.</paragraph>
</listitem>
</list>
<embed href="text/shared/00/00000004.xhp#related"/>
<paragraph role="paragraph" id="par_id3153726" xml-lang="en-US" l10n="U" oldref="29"><link href="text/shared/01/01130000.xhp" name="Printing dialogs">Printing dialogs</link></paragraph>
<paragraph role="paragraph" id="par_id3154146" xml-lang="en-US" l10n="U" oldref="30"><link href="text/shared/optionen/01000000.xhp" name="Tools - Options dialog">Tools - Options dialog</link></paragraph>
</body>
</helpdocument>
