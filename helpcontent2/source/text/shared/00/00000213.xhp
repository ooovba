<?xml version="1.0" encoding="UTF-8"?>


<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: 00000213.xhp,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************-->
	
<helpdocument version="1.0">
<meta>
<topic id="textshared0000000213xml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">EPS Export Options</title>
<filename>/text/shared/00/00000213.xhp</filename>
</topic>
<history>
<created date="2003-10-31T00:00:00">Sun Microsystems, Inc.</created>
<lastedited date="2004-05-17T14:58:18">converted from old format - fpe</lastedited>
</history>
</meta>
<body>
<paragraph role="heading" id="hd_id3149976" xml-lang="en-US" level="1" l10n="U" oldref="1">EPS Export Options</paragraph>
<paragraph role="paragraph" id="par_id3156045" xml-lang="en-US" l10n="U" oldref="2">Defines several export options for EPS files.</paragraph>
<paragraph role="paragraph" id="par_id3149948" xml-lang="en-US" l10n="U" oldref="27"><switchinline select="sys">
<caseinline select="WIN">You must print an EPS file with a PostScript printer. Other printers will only print the embedded preview.
</caseinline></switchinline></paragraph>
<section id="howtoget">
  <embed href="text/shared/00/00000401.xhp#epsexport"/>
</section>
<paragraph role="heading" id="hd_id3154598" xml-lang="en-US" level="2" l10n="U" oldref="3">Preview</paragraph>
<paragraph role="paragraph" id="par_id3150146" xml-lang="en-US" l10n="U" oldref="4">Specifies options for embedded preview for the EPS file.</paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:CHECKBOX:DLG_EXPORT_EPS:CB_PREVIEW_TIFF" id="bm_id3153884" localize="false"/>
<paragraph role="heading" id="hd_id3145348" xml-lang="en-US" level="3" l10n="U" oldref="5">Image preview (TIFF)</paragraph>
<paragraph role="paragraph" id="par_id3155271" xml-lang="en-US" l10n="U" oldref="6"><ahelp hid="GOODIES:CHECKBOX:DLG_EXPORT_EPS:CB_PREVIEW_TIFF">Specifies whether a preview image is exported in the TIFF format together with the actual PostScript file.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:CHECKBOX:DLG_EXPORT_EPS:CB_PREVIEW_EPSI" id="bm_id3155069" localize="false"/>
<paragraph role="heading" id="hd_id3149748" xml-lang="en-US" level="3" l10n="U" oldref="29">Interchange (EPSI)</paragraph>
<paragraph role="paragraph" id="par_id3144740" xml-lang="en-US" l10n="U" oldref="30"><ahelp hid="GOODIES:CHECKBOX:DLG_EXPORT_EPS:CB_PREVIEW_EPSI">Specifies whether a monochrome preview graphic in EPSI format is exported together with the PostScript file.</ahelp> This format only contains printable characters from the 7-bit ASCII code.</paragraph>
<paragraph role="heading" id="hd_id3159224" xml-lang="en-US" level="2" l10n="U" oldref="9">Version</paragraph>
<paragraph role="paragraph" id="par_id3151100" xml-lang="en-US" l10n="U" oldref="10">Determines the PostScript level for exporting the EPS file.</paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_LEVEL1" id="bm_id3150040" localize="false"/>
<paragraph role="heading" id="hd_id3143281" xml-lang="en-US" level="3" l10n="U" oldref="11">Level 1</paragraph>
<paragraph role="paragraph" id="par_id3150935" xml-lang="en-US" l10n="U" oldref="12"><ahelp hid="GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_LEVEL1">Compression is not available at this level. Select the<emph> Level 1 </emph>option if your PostScript printer does not offer the capabilities of Level 2.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_LEVEL2" id="bm_id3149640" localize="false"/>
<paragraph role="heading" id="hd_id3150616" xml-lang="en-US" level="3" l10n="U" oldref="13">Level 2</paragraph>
<paragraph role="paragraph" id="par_id3159201" xml-lang="en-US" l10n="U" oldref="14"><ahelp hid="GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_LEVEL2">Select the<emph> Level 2 </emph>option if your output device supports colored bitmaps, palette graphics and compressed graphics.</ahelp></paragraph>
<paragraph role="heading" id="hd_id3147261" xml-lang="en-US" level="2" l10n="U" oldref="15">Color format</paragraph>
<paragraph role="paragraph" id="par_id3147571" xml-lang="en-US" l10n="U" oldref="16">The <emph>Color format</emph> option gives you the choice between exporting in color or in grayscale.</paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_COLOR" id="bm_id3153049" localize="false"/>
<paragraph role="heading" id="hd_id3152801" xml-lang="en-US" level="3" l10n="U" oldref="17">Color</paragraph>
<paragraph role="paragraph" id="par_id3147250" xml-lang="en-US" l10n="U" oldref="18"><ahelp hid="GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_COLOR">Exports the file in color.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_GRAYSCALE" id="bm_id3153346" localize="false"/>
<paragraph role="heading" id="hd_id3152551" xml-lang="en-US" level="3" l10n="U" oldref="19">Grayscale</paragraph>
<paragraph role="paragraph" id="par_id3147088" xml-lang="en-US" l10n="U" oldref="20"><ahelp hid="GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_GRAYSCALE">Exports the file in grayscale tones.</ahelp></paragraph>
<paragraph role="heading" id="hd_id3153577" xml-lang="en-US" level="2" l10n="U" oldref="21">Compression</paragraph>
<paragraph role="paragraph" id="par_id3149827" xml-lang="en-US" l10n="U" oldref="22">Specifies whether the data is exported with compression.</paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_COMPRESSION_LZW" id="bm_id3157863" localize="false"/>
<paragraph role="heading" id="hd_id3155338" xml-lang="en-US" level="3" l10n="U" oldref="23">LZW encoding</paragraph>
<paragraph role="paragraph" id="par_id3153683" xml-lang="en-US" l10n="U" oldref="24"><ahelp hid="GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_COMPRESSION_LZW">LZW compression is the compression of a file into a smaller file using a table-based lookup algorithm invented by Abraham Lempel, Jacob Ziv, and Terry Welch. If you choose to activate this, select the<emph> LZW encoding </emph>option.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_COMPRESSION_NONE" id="bm_id3147653" localize="false"/>
<paragraph role="heading" id="hd_id3147242" xml-lang="en-US" level="3" l10n="U" oldref="25">None</paragraph>
<paragraph role="paragraph" id="par_id3152780" xml-lang="en-US" l10n="U" oldref="26"><ahelp hid="GOODIES:RADIOBUTTON:DLG_EXPORT_EPS:RB_COMPRESSION_NONE">Specifies that you do not wish to use compression.</ahelp></paragraph>
</body>
</helpdocument>
