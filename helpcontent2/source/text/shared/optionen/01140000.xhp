<?xml version="1.0" encoding="UTF-8"?>



<!--
 ***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: 01140000.xhp,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->


		<helpdocument version="1.0">
<meta>
<topic id="textsharedoptionen01140000xml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">Languages</title>
<filename>/text/shared/optionen/01140000.xhp</filename>
</topic>
</meta>
<body>
<section id="sprachen">
<bookmark xml-lang="en-US" branch="index" id="bm_id3154751"><bookmark_value>languages; locale settings</bookmark_value>
<bookmark_value>locale settings</bookmark_value>
<bookmark_value>Asian languages; enabling</bookmark_value>
<bookmark_value>languages; Asian support</bookmark_value>
<bookmark_value>complex text layout; enabling</bookmark_value>
<bookmark_value>Arabic;language settings</bookmark_value>
<bookmark_value>Hebrew;language settings</bookmark_value>
<bookmark_value>Thai;language settings</bookmark_value>
<bookmark_value>Hindi;language settings</bookmark_value>
<bookmark_value>decimal separator key</bookmark_value>
</bookmark><comment>mw deleted "language settings;" and "Asian languages;supporting"</comment><paragraph role="heading" id="hd_id3151299" xml-lang="en-US" level="1" l10n="U" oldref="1"><link href="text/shared/optionen/01140000.xhp" name="Languages">Languages</link></paragraph>
<paragraph role="paragraph" id="par_id3148520" xml-lang="en-US" l10n="CHG" oldref="2"><ahelp hid=".">Defines the default languages and some other locale settings for documents.</ahelp></paragraph>
</section>
<section id="howtoget">
  <embed href="text/shared/00/00000406.xhp#sprachen"/>
</section>
<paragraph role="heading" id="hd_id3156042" xml-lang="en-US" level="2" l10n="U" oldref="21">Language of</paragraph>
<bookmark xml-lang="en-US" branch="hid/svx_ListBox_OFA_TP_LANGUAGES_LB_USERINTERFACE" id="bm_id7035737" localize="false"/><paragraph role="heading" id="par_idN10681" xml-lang="en-US" level="3" l10n="NEW">User interface</paragraph>
<paragraph role="paragraph" id="par_idN10685" xml-lang="en-US" l10n="CHG"><ahelp hid=".">Select the language used for the user interface, for example menus, dialogs, help files. You must have installed at least one additional language pack or a multi-language version of %PRODUCTNAME.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_LOCALESETTING" id="bm_id3145136" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_ListBox_OFA_TP_LANGUAGES_LB_LOCALESETTING" id="bm_id1609241" localize="false"/><paragraph role="heading" id="hd_id3154751" xml-lang="en-US" level="3" l10n="U" oldref="22">Locale setting</paragraph>
<paragraph role="paragraph" id="par_id3157958" xml-lang="en-US" l10n="CHG" oldref="23"><ahelp hid="OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_LOCALESETTING">Specifies the locale setting of the country setting. This influences settings for numbering, currency and units of measure.</ahelp></paragraph>
<paragraph role="paragraph" id="par_id3156410" xml-lang="en-US" l10n="U" oldref="27">A change in this field is immediately applicable. However, some formats that were formatted by default change only if the document is newly loaded.</paragraph>
<bookmark xml-lang="en-US" branch="hid/svx_CheckBox_OFA_TP_LANGUAGES_CB_DECIMALSEPARATOR" id="bm_id2873012" localize="false"/><paragraph role="heading" id="par_idN106DE" xml-lang="en-US" level="3" l10n="NEW">Decimal separator key - Same as locale setting</paragraph>
<paragraph role="paragraph" id="par_idN106E2" xml-lang="en-US" l10n="NEW"><ahelp hid=".">Specifies to use the decimal separator key that is set in your system when you press the respective key on the number pad.</ahelp></paragraph>
<paragraph role="paragraph" id="par_idN106F1" xml-lang="en-US" l10n="NEW">If this checkbox is activated, the character shown after "Same as locale setting" is inserted when you press the key on the number pad. If this checkbox is not activated, the character that your keyboard driver software provides is inserted.</paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_CURRENCY" id="bm_id3152996" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_ListBox_OFA_TP_LANGUAGES_LB_CURRENCY" id="bm_id9471419" localize="false"/><paragraph role="heading" id="hd_id3147209" xml-lang="en-US" level="3" l10n="U" oldref="24">Default currency</paragraph>
<paragraph role="paragraph" id="par_id3145120" xml-lang="en-US" l10n="U" oldref="25"><ahelp hid="OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_CURRENCY">Specifies the default currency that is used for the currency format and the currency fields.</ahelp> If you change the locale setting, the default currency changes automatically.</paragraph>
<paragraph role="paragraph" id="par_id3148491" xml-lang="en-US" l10n="U" oldref="29">The default entry applies to the currency format that is assigned to the selected locale setting.</paragraph>
<paragraph role="paragraph" id="par_id3157909" xml-lang="en-US" l10n="U" oldref="26">A change in <emph>Default currency</emph> field will be transmitted to all open documents and will lead to corresponding changes in the dialogs and icons that control the currency format in these documents.</paragraph>
<paragraph role="heading" id="hd_id3153127" xml-lang="en-US" level="2" l10n="U" oldref="5">Default languages for documents</paragraph>
<paragraph role="paragraph" id="par_id3149763" xml-lang="en-US" l10n="U" oldref="6">Specifies the languages for spellchecking, thesaurus and hyphenation.</paragraph>
<embed href="text/shared/guide/language_select.xhp#language_select"/>
<section id="sprachmodul">
<paragraph role="warning" id="par_id3148663" xml-lang="en-US" l10n="U" oldref="8">The spellcheck for the selected language only functions when you have installed the corresponding language module. <embedvar href="text/shared/optionen/01010401.xhp#sprachenfeld"/>
</paragraph>
</section>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_WEST_LANG" id="bm_id3148452" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_ListBox_OFA_TP_LANGUAGES_LB_WEST_LANG" id="bm_id4734946" localize="false"/><paragraph role="heading" id="hd_id3151210" xml-lang="en-US" level="3" l10n="U" oldref="9">Western</paragraph>
<paragraph role="paragraph" id="par_id3153192" xml-lang="en-US" l10n="U" oldref="10"><ahelp hid="OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_WEST_LANG">Specifies the language used for the spellcheck function in western alphabets.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_ASIAN_LANG" id="bm_id3152933" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_ListBox_OFA_TP_LANGUAGES_LB_ASIAN_LANG" id="bm_id908841" localize="false"/><paragraph role="heading" id="hd_id3156422" xml-lang="en-US" level="3" l10n="U" oldref="11">Asian</paragraph>
<paragraph role="paragraph" id="par_id3159149" xml-lang="en-US" l10n="U" oldref="12"><ahelp hid="OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_ASIAN_LANG">Specifies the language used for the spellcheck function in Asian alphabets.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_COMPLEX_LANG" id="bm_id3151041" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_ListBox_OFA_TP_LANGUAGES_LB_COMPLEX_LANG" id="bm_id2717062" localize="false"/><paragraph role="heading" id="hd_id3158407" xml-lang="en-US" level="3" l10n="U" oldref="31">CTL</paragraph>
<paragraph role="paragraph" id="par_id3156212" xml-lang="en-US" l10n="U" oldref="32"><ahelp hid="OFFMGR_LISTBOX_OFA_TP_LANGUAGES_LB_COMPLEX_LANG">Specifies the language for the complex text layout spellcheck.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_CHECKBOX_OFA_TP_LANGUAGES_CB_CURRENT_DOC" id="bm_id3155131" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_CheckBox_OFA_TP_LANGUAGES_CB_CURRENT_DOC" id="bm_id3376262" localize="false"/><paragraph role="heading" id="hd_id3149807" xml-lang="en-US" level="3" l10n="U" oldref="15">For the current document only</paragraph>
<paragraph role="paragraph" id="par_id3155432" xml-lang="en-US" l10n="U" oldref="16"><ahelp hid="OFFMGR_CHECKBOX_OFA_TP_LANGUAGES_CB_CURRENT_DOC">Specifies that the settings for default languages are valid only for the current document.</ahelp></paragraph>
<paragraph role="heading" id="hd_id3156441" xml-lang="en-US" level="2" l10n="CHG" oldref="17">Enhanced language support</paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_CHECKBOX_OFA_TP_LANGUAGES_CB_ASIANSUPPORT" id="bm_id3155854" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_CheckBox_OFA_TP_LANGUAGES_CB_ASIANSUPPORT" id="bm_id105594" localize="false"/><paragraph role="heading" id="hd_id3148575" xml-lang="en-US" level="3" l10n="CHG" oldref="19">Enabled for Asian languages</paragraph>
<paragraph role="paragraph" id="par_id3145748" xml-lang="en-US" l10n="U" oldref="20"><ahelp hid="OFFMGR_CHECKBOX_OFA_TP_LANGUAGES_CB_ASIANSUPPORT">Activates Asian languages support. You can now modify the corresponding Asian language settings in <item type="productname">%PRODUCTNAME</item>.</ahelp></paragraph>
<paragraph role="paragraph" id="par_id3152938" xml-lang="en-US" l10n="U" oldref="18">If you want to write in Chinese, Japanese or Korean, you can activate the support for these languages in the user interface.</paragraph>
<bookmark xml-lang="en-US" branch="hid/OFFMGR_CHECKBOX_OFA_TP_LANGUAGES_CB_CTLSUPPORT" id="bm_id3150010" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/svx_CheckBox_OFA_TP_LANGUAGES_CB_CTLSUPPORT" id="bm_id295724" localize="false"/><paragraph role="heading" id="hd_id3146147" xml-lang="en-US" level="3" l10n="CHG" oldref="33">Enabled for complex text layout (CTL)</paragraph>
<paragraph role="paragraph" id="par_id3149667" xml-lang="en-US" l10n="U" oldref="35"><ahelp hid="OFFMGR_CHECKBOX_OFA_TP_LANGUAGES_CB_CTLSUPPORT">Activates complex text layout support. You can now modify the settings corresponding to complex text layout in <item type="productname">%PRODUCTNAME</item>.</ahelp></paragraph>
<embed href="text/shared/guide/ctl.xhp#ctl"/>
</body>
</helpdocument>
