<?xml version="1.0" encoding="UTF-8"?>
<helpdocument version="1.0">
	
<!--
***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: 01060100.xhp,v $
 * $Revision: 1.15.4.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************
 -->
 
	
<meta>
<topic id="textsharedoptionen01060100xml" indexer="include">
<title id="tit" xml-lang="en-US">View</title>
<filename>/text/shared/optionen/01060100.xhp</filename>
</topic>
</meta>
<body>
<section id="inhalte">
<bookmark xml-lang="en-US" branch="index" id="bm_id3147242"><bookmark_value>cells; showing grid lines (Calc)</bookmark_value>
<bookmark_value>borders; cells on screen (Calc)</bookmark_value>
<bookmark_value>grids; displaying lines (Calc)</bookmark_value>
<bookmark_value>colors; grid lines and cells (Calc)</bookmark_value>
<bookmark_value>page breaks; displaying (Calc)</bookmark_value>
<bookmark_value>guides; showing (Calc)</bookmark_value>
<bookmark_value>handles; showing simple/large handles (Calc)</bookmark_value>
<bookmark_value>displaying; zero values (Calc)</bookmark_value>
<bookmark_value>zero values; displaying (Calc)</bookmark_value>
<bookmark_value>displaying; comments (Calc)</bookmark_value>
<bookmark_value>comments; displaying (Calc)</bookmark_value>
<bookmark_value>tables in spreadsheets; value highlighting</bookmark_value>
<bookmark_value>cells; formatting without effect (Calc)</bookmark_value>
<bookmark_value>cells; coloring (Calc)</bookmark_value>
<bookmark_value>anchors; displaying (Calc)</bookmark_value>
<bookmark_value>colors;restriction (Calc)</bookmark_value>
<bookmark_value>text overflow in spreadsheet cells</bookmark_value>
<bookmark_value>references; displaying in color (Calc)</bookmark_value>
<bookmark_value>objects; displaying in spreadsheets</bookmark_value>
<bookmark_value>pictures; displaying in Calc</bookmark_value>
<bookmark_value>charts; displaying (Calc)</bookmark_value>
<bookmark_value>draw objects; displaying (Calc)</bookmark_value>
<bookmark_value>row headers; displaying (Calc)</bookmark_value>
<bookmark_value>column headers; displaying (Calc)</bookmark_value>
<bookmark_value>scrollbars; displaying (Calc)</bookmark_value>
<bookmark_value>sheet tabs; displaying</bookmark_value>
<bookmark_value>tabs; displaying sheet tabs</bookmark_value>
<bookmark_value>outlines;outline symbols</bookmark_value>
</bookmark><comment>mw deleted "formulas;..." and copied "displaying; formulas..." to Calc guide formula_value.xhp and changed "sheets;.." into "sheet tabs;..."</comment><comment>MW made "outline..." a two level entry</comment>
<bookmark xml-lang="en-US" branch="hid/HID_SCPAGE_CONTENT" id="bm_id3157898" localize="false"/>
<paragraph role="heading" id="hd_id3150445" xml-lang="en-US" level="1" l10n="U" oldref="1"><link href="text/shared/optionen/01060100.xhp" name="View">View</link></paragraph>
<paragraph role="paragraph" id="par_id3153988" xml-lang="en-US" l10n="U" oldref="2"><ahelp hid="HID_SCPAGE_CONTENT">Defines which elements of the <item type="productname">%PRODUCTNAME</item> Calc main window are displayed. You can also show or hide highlighting of values in tables.</ahelp></paragraph>
</section>
<section id="howtoget">
<embed href="text/shared/00/00000406.xhp#tabelleinhalte"/>
</section>
<paragraph role="heading" id="hd_id3153682" xml-lang="en-US" level="2" l10n="U" oldref="28">Visual aids</paragraph>
<paragraph role="paragraph" id="par_id3153311" xml-lang="en-US" l10n="U" oldref="29">Specifies which lines are displayed.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_GRID" id="bm_id3148538" localize="false"/>
<paragraph role="heading" id="hd_id3147242" xml-lang="en-US" level="3" l10n="U" oldref="30">Grid lines</paragraph>
<paragraph role="paragraph" id="par_id3153088" xml-lang="en-US" l10n="U" oldref="31"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_GRID">Specifies whether to display grid lines between the cells.</ahelp><switchinline select="appl"><caseinline select="CALC">For printing, choose <emph>Format - Page - </emph><link href="text/scalc/01/05070500.xhp" name="Sheet"><emph>Sheet</emph></link> and mark the <emph>Grid</emph> check box. 
</caseinline></switchinline></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_LISTBOX_RID_SCPAGE_CONTENT_LB_COLOR" id="bm_id3150504" localize="false"/>
<paragraph role="heading" id="hd_id3156326" xml-lang="en-US" level="3" l10n="U" oldref="32">Color</paragraph>
<paragraph role="paragraph" id="par_id3154286" xml-lang="en-US" l10n="U" oldref="33"><ahelp hid="SC_LISTBOX_RID_SCPAGE_CONTENT_LB_COLOR">Specifies a color for the grid lines in the current document.</ahelp> To see the grid line color that was saved with the document, go to <emph>Tools - Options - %PRODUCTNAME</emph>
<emph>- Appearance</emph>, under <emph>Scheme</emph> find the entry <emph>Spreadsheet - Grid lines</emph> and set the color to "Automatic".</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_PAGEBREAKS" id="bm_id3148563" localize="false"/>
<paragraph role="heading" id="hd_id3152349" xml-lang="en-US" level="3" l10n="U" oldref="34">Page breaks</paragraph>
<paragraph role="paragraph" id="par_id3151245" xml-lang="en-US" l10n="U" oldref="35"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_PAGEBREAKS">Specifies whether to view the page breaks within a defined print area.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_GUIDELINE" id="bm_id3148946" localize="false"/>
<paragraph role="heading" id="hd_id3149669" xml-lang="en-US" level="3" l10n="U" oldref="36">Guides while moving</paragraph>
<paragraph role="paragraph" id="par_id3148550" xml-lang="en-US" l10n="U" oldref="37"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_GUIDELINE">Specifies whether to view guides when moving drawings, frames, graphics and other objects.</ahelp> These guides help you align objects.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_HANDLES" id="bm_id3153380" localize="false"/>
<paragraph role="heading" id="hd_id3150358" xml-lang="en-US" level="3" l10n="U" oldref="38">Simple handles</paragraph>
<paragraph role="paragraph" id="par_id3154140" xml-lang="en-US" l10n="U" oldref="39"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_HANDLES">Specifies whether to display the handles (the eight points on a selection box) as simple squares without 3D effect.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_BIGHANDLES" id="bm_id3151176" localize="false"/>
<paragraph role="heading" id="hd_id3149202" xml-lang="en-US" level="3" l10n="U" oldref="40">Large handles</paragraph>
<paragraph role="paragraph" id="par_id3154123" xml-lang="en-US" l10n="U" oldref="41"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_BIGHANDLES">Specifies that larger than normal handles (the eight points on a selection box) are displayed.</ahelp></paragraph>
<paragraph role="heading" id="hd_id3152920" xml-lang="en-US" level="2" l10n="U" oldref="3">Display</paragraph>
<paragraph role="paragraph" id="par_id3125864" xml-lang="en-US" l10n="U" oldref="4">Select various options for the screen display.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_FORMULA" id="bm_id3150767" localize="false"/>
<paragraph role="heading" id="hd_id3154218" xml-lang="en-US" level="3" l10n="U" oldref="5">Formulas</paragraph>
<paragraph role="paragraph" id="par_id3150440" xml-lang="en-US" l10n="U" oldref="6"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_FORMULA">Specifies whether to show formulas instead of results in the cells.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_NIL" id="bm_id3146146" localize="false"/>
<paragraph role="heading" id="hd_id3155132" xml-lang="en-US" level="3" l10n="U" oldref="7">Zero values</paragraph>
<paragraph role="paragraph" id="par_id3147318" xml-lang="en-US" l10n="U" oldref="8"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_NIL">Specifies whether to show numbers with the value of 0.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_ANNOT" id="bm_id3163711" localize="false"/>
<paragraph role="heading" id="hd_id3147348" xml-lang="en-US" level="3" l10n="U" oldref="9">Comment indicator</paragraph>
<paragraph role="paragraph" id="par_id3146974" xml-lang="en-US" l10n="CHG" oldref="10"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_ANNOT">Specifies that a small rectangle in the top right corner of the cell indicates that a comment exists. The comment will be shown only when you enable tips under <emph>Tools - Options - %PRODUCTNAME - General</emph>.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/FID_NOTE_VISIBLE" id="bm_id2862651" localize="false"/>
<bookmark xml-lang="en-US" branch="hid/.uno:NoteVisible" id="bm_id3153574" localize="false"/>
<paragraph role="paragraph" id="par_id3150487" xml-lang="en-US" l10n="U" oldref="24"><ahelp hid=".uno:NoteVisible">To display a comment permanently, select the <emph>Show comment</emph> command from the cell's context menu.</ahelp></paragraph>
<paragraph role="paragraph" id="par_id3149667" xml-lang="en-US" l10n="CHG" oldref="25">You can type and edit comments with the <link href="text/shared/01/04050000.xhp" name="Insert - Comment"><emph>Insert - Comment</emph></link> command. Comments that are permanently displayed can be edited by clicking the comment box. Click the Navigator and under the <emph>Comments</emph> entry you can view all comments in the current document. By double clicking a comment in Navigator, the cursor will jump to the corresponding cell containing the comment.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_VALUE" id="bm_id3155306" localize="false"/>
<paragraph role="heading" id="hd_id3150872" xml-lang="en-US" level="3" l10n="U" oldref="11">Value highlighting</paragraph>
<paragraph role="paragraph" id="par_id3154792" xml-lang="en-US" l10n="U" oldref="12"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_VALUE">Mark the <emph>Value highlighting</emph> box to highlight all values in the sheet. Text cells are highlighted in black, cells with numbers in blue, and cells containing formulas, logical values, dates, and so on, in green.</ahelp></paragraph>
<paragraph role="warning" id="par_id3151319" xml-lang="en-US" l10n="U" oldref="13">When this command is active, any colors assigned in the document will not be displayed until the function is deactivated.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_ANCHOR" id="bm_id3146972" localize="false"/>
<paragraph role="heading" id="hd_id3157846" xml-lang="en-US" level="3" l10n="U" oldref="14">Anchor</paragraph>
<paragraph role="paragraph" id="par_id3147494" xml-lang="en-US" l10n="U" oldref="15"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_ANCHOR">Specifies whether the anchor icon is displayed when an inserted object, such as a graphic, is selected.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_CLIP" id="bm_id3146789" localize="false"/>
<paragraph role="heading" id="hd_id3146898" xml-lang="en-US" level="3" l10n="U" oldref="26">Text overflow</paragraph>
<paragraph role="paragraph" id="par_id3153707" xml-lang="en-US" l10n="CHG" oldref="27"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_CLIP">If a cell contains text that is wider than the width of the cell, the text is displayed over empty neighboring cells in the same row. If there is no empty neighboring cell, a small triangle at the cell border indicates that the text continues.</ahelp><comment>UFI: sc.features "Alignment of text that is larger than a cell"</comment></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_RFIND" id="bm_id3153927" localize="false"/>
<paragraph role="heading" id="hd_id3150327" xml-lang="en-US" level="3" l10n="U" oldref="54">Show references in color</paragraph>
<paragraph role="paragraph" id="par_id3153766" xml-lang="en-US" l10n="U" oldref="55"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_RFIND">Specifies that each reference is highlighted in color in the formula. The cell range is also enclosed by a colored border as soon as the cell containing the reference is selected for editing.</ahelp></paragraph>
<paragraph role="heading" id="hd_id3155444" xml-lang="en-US" level="2" l10n="U" oldref="16">Objects</paragraph>
<paragraph role="paragraph" id="par_id3148405" xml-lang="en-US" l10n="CHG" oldref="17">Defines whether to display or hide objects for up to three object groups.</paragraph><comment>removed "placeholder" text, see i81634</comment>
<bookmark xml-lang="en-US" branch="hid/SC_LISTBOX_RID_SCPAGE_CONTENT_LB_OBJGRF" id="bm_id3154256" localize="false"/>
<paragraph role="heading" id="hd_id3150043" xml-lang="en-US" level="3" l10n="U" oldref="18">Objects/Graphics</paragraph>
<paragraph role="paragraph" id="par_id3163549" xml-lang="en-US" l10n="CHG" oldref="19"><ahelp hid="SC_LISTBOX_RID_SCPAGE_CONTENT_LB_OBJGRF">Defines if objects and graphics are shown or hidden.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_LISTBOX_RID_SCPAGE_CONTENT_LB_DIAGRAM" id="bm_id3147344" localize="false"/>
<paragraph role="heading" id="hd_id3151249" xml-lang="en-US" level="3" l10n="U" oldref="20">Charts</paragraph>
<paragraph role="paragraph" id="par_id3149106" xml-lang="en-US" l10n="CHG" oldref="21"><ahelp hid="SC_LISTBOX_RID_SCPAGE_CONTENT_LB_DIAGRAM">Defines if charts in your document are shown or hidden.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_LISTBOX_RID_SCPAGE_CONTENT_LB_DRAW" id="bm_id3155334" localize="false"/>
<paragraph role="heading" id="hd_id3154703" xml-lang="en-US" level="3" l10n="U" oldref="22">Drawing objects</paragraph>
<paragraph role="paragraph" id="par_id3155959" xml-lang="en-US" l10n="CHG" oldref="23"><ahelp hid="SC_LISTBOX_RID_SCPAGE_CONTENT_LB_DRAW">Defines if drawing objects in your document are shown or hidden.</ahelp></paragraph>
<paragraph role="heading" id="hd_id0909200810585828" xml-lang="en-US" level="2" l10n="NEW">Zoom</paragraph>
<bookmark xml-lang="en-US" branch="hid/sc_CheckBox_RID_SCPAGE_CONTENT_CB_SYNCZOOM" id="bm_id0909200810584462" localize="false"/>
<paragraph role="heading" id="hd_id0909200810585881" xml-lang="en-US" level="3" l10n="NEW">Synchronize sheets</paragraph>
<paragraph role="paragraph" id="par_id0909200810585870" xml-lang="en-US" l10n="NEW"><ahelp hid=".">If checked, all sheets are shown with the same zoom factor. If not checked, each sheet can have its own zoom factor.</ahelp></paragraph>
<paragraph role="heading" id="hd_id3153920" xml-lang="en-US" level="2" l10n="U" oldref="42">Window</paragraph>
<paragraph role="paragraph" id="par_id3154661" xml-lang="en-US" l10n="U" oldref="43">Specifies whether some Help elements will or will not appear in the table.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_ROWCOLHEADER" id="bm_id3145791" localize="false"/>
<paragraph role="heading" id="hd_id3149923" xml-lang="en-US" level="3" l10n="U" oldref="44">Column/Row headers</paragraph>
<paragraph role="paragraph" id="par_id3149816" xml-lang="en-US" l10n="U" oldref="45"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_ROWCOLHEADER">Specifies whether to display row and column headers.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_HSCROLL" id="bm_id3154120" localize="false"/>
<paragraph role="heading" id="hd_id3154205" xml-lang="en-US" level="3" l10n="U" oldref="46">Horizontal scrollbar</paragraph>
<paragraph role="paragraph" id="par_id3155578" xml-lang="en-US" l10n="U" oldref="47"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_HSCROLL">Specifies whether to display a horizontal scrollbar at the bottom of the document window.</ahelp> Note that there is a slider between the horizontal scrollbar and the sheet tabs that may be set to one end.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_VSCROLL" id="bm_id3158413" localize="false"/>
<paragraph role="heading" id="hd_id3148422" xml-lang="en-US" level="3" l10n="U" oldref="48">Vertical scrollbar</paragraph>
<paragraph role="paragraph" id="par_id3147128" xml-lang="en-US" l10n="U" oldref="49"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_VSCROLL">Specifies whether to display a vertical scrollbar at the right of the document window.</ahelp></paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_TBLREG" id="bm_id3158214" localize="false"/>
<paragraph role="heading" id="hd_id3150826" xml-lang="en-US" level="3" l10n="U" oldref="50">Sheet tabs</paragraph>
<paragraph role="paragraph" id="par_id3154658" xml-lang="en-US" l10n="U" oldref="51"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_TBLREG">Specifies whether to display the sheet tabs at the bottom of the spreadsheet document.</ahelp> If this box is not checked, you will only be able to switch between the sheets through the <switchinline select="appl"><caseinline select="CALC"><link href="text/scalc/01/02110000.xhp" name="Navigator">Navigator</link>
</caseinline><defaultinline>Navigator</defaultinline></switchinline>. Note that there is a slider between the horizontal scrollbar and the sheet tabs that may be set to one end.</paragraph>
<bookmark xml-lang="en-US" branch="hid/SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_OUTLINE" id="bm_id3154199" localize="false"/>
<paragraph role="heading" id="hd_id3152584" xml-lang="en-US" level="3" l10n="U" oldref="52">Outline symbols</paragraph>
<paragraph role="paragraph" id="par_id3145135" xml-lang="en-US" l10n="U" oldref="53"><ahelp hid="SC_CHECKBOX_RID_SCPAGE_CONTENT_CB_OUTLINE">If you have defined an <switchinline select="appl"><caseinline select="CALC"><link href="text/scalc/01/12080000.xhp" name="outline">outline</link>
</caseinline><defaultinline>outline</defaultinline></switchinline>, the <emph>Outline symbols</emph> option specifies whether to view the outline symbols at the border of the sheet.</ahelp></paragraph>
</body>
</helpdocument>
