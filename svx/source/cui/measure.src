/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: measure.src,v $
 * $Revision: 1.30 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 // include ---------------------------------------------------------------
#include <svx/dialogs.hrc>
#include "measure.hrc"
#include "helpid.hrc"
#define DELTA 20
 // pragma ----------------------------------------------------------------

 // RID_SVXPAGE_MEASURE ---------------------------------------------------
TabPage RID_SVXPAGE_MEASURE
{
    HelpId = HID_PAGE_MEASURE ;
    Hide = TRUE ;
    Size = MAP_APPFONT ( 260 , 185 ) ;
    Text [ en-US ] = "Dimensioning" ;

    FixedLine FL_LINE
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 120 , 8 ) ;
        Text [ en-US ] = "Line";
    };
    FixedText FT_LINE_DIST
    {
        Pos = MAP_APPFONT ( 12 , 16  ) ;
        Size = MAP_APPFONT ( 65 , 8 ) ;
        Text [ en-US ] = "Line ~distance" ;
    };
    MetricField MTR_LINE_DIST
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 78 , 14  ) ;
        Size = MAP_APPFONT ( 42 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_MM ;
        DecimalDigits = 2 ;
        Minimum = -10000 ;
        First = -10000 ;
        Maximum = 10000 ;
        Last = 10000 ;
        SpinSize = 10 ;
    };
    FixedText FT_HELPLINE_OVERHANG
    {
        Pos = MAP_APPFONT ( 12 , 32  ) ;
        Size = MAP_APPFONT ( 65 , 8 ) ;
        Text [ en-US ] = "Guide ~overhang" ;
    };
    MetricField MTR_FLD_HELPLINE_OVERHANG
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 78 , 30  ) ;
        Size = MAP_APPFONT ( 42 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_MM ;
        DecimalDigits = 2 ;
        Minimum = -10000 ;
        First = -10000 ;
        Maximum = 10000 ;
        Last = 10000 ;
        SpinSize = 10 ;
    };
    FixedText FT_HELPLINE_DIST
    {
        Pos = MAP_APPFONT ( 12 , 48  ) ;
        Size = MAP_APPFONT ( 65 , 8 ) ;
        Text [ en-US ] = "~Guide distance" ;
    };
    MetricField MTR_FLD_HELPLINE_DIST
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 78 , 46  ) ;
        Size = MAP_APPFONT ( 42 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_MM ;
        DecimalDigits = 2 ;
        Minimum = -10000 ;
        First = -10000 ;
        Maximum = 10000 ;
        Last = 10000 ;
        SpinSize = 10 ;
    };
    FixedText FT_HELPLINE1_LEN
    {
        Pos = MAP_APPFONT ( 12 , 64  ) ;
        Size = MAP_APPFONT ( 65 , 8 ) ;
        Text [ en-US ] = "~Left guide" ;
    };
    MetricField MTR_FLD_HELPLINE1_LEN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 78 , 62 ) ;
        Size = MAP_APPFONT ( 42 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_MM ;
        DecimalDigits = 2 ;
        Minimum = -10000 ;
        First = -10000 ;
        Maximum = 10000 ;
        Last = 10000 ;
        SpinSize = 10 ;
    };
    FixedText FT_HELPLINE2_LEN
    {
        Pos = MAP_APPFONT ( 12 , 80 ) ;
        Size = MAP_APPFONT ( 65 , 8 ) ;
        Text [ en-US ] = "~Right guide" ;
    };
    MetricField MTR_FLD_HELPLINE2_LEN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 78 , 78 ) ;
        Size = MAP_APPFONT ( 42 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_MM ;
        DecimalDigits = 2 ;
        Minimum = -10000 ;
        First = -10000 ;
        Maximum = 10000 ;
        Last = 10000 ;
        SpinSize = 10 ;
    };
    TriStateBox TSB_BELOW_REF_EDGE
    {
        Pos = MAP_APPFONT ( 12 , 96  ) ;
        Size = MAP_APPFONT ( 108 , 10 ) ;
        TabStop = TRUE ;
        Text [ en-US ] = "Measure ~below object";
    };
    FixedText FT_DECIMALPLACES
    {
        Pos = MAP_APPFONT ( 12 , 112 ) ;
        Size = MAP_APPFONT ( 65 , 8 ) ;

        Text [ en-US ] = "Decimal places" ;
    };
    MetricField MTR_FLD_DECIMALPLACES
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 78 , 110 ) ;
        Size = MAP_APPFONT ( 42 , 12 ) ;
        TabStop = TRUE ;
        Right = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 99 ;
        StrictFormat = TRUE ;
        Last = 99 ;
        SpinSize = 1 ;
    };
    FixedLine FL_VERT
    {
        Pos = MAP_APPFONT ( 127 , 14 ) ;
        Size = MAP_APPFONT ( 4 , 108 ) ;
        Vert = TRUE ;
    };
    FixedLine FL_LABEL
    {
        Pos = MAP_APPFONT ( 132 , 3 ) ;
        Size = MAP_APPFONT ( 122 , 8 ) ;
        Text [ en-US ] = "Legend";
    };
    FixedText FT_POSITION
    {
        Pos = MAP_APPFONT ( 138 , 14  ) ;
        Size = MAP_APPFONT ( 110 , 8 ) ;
        Text [ en-US ] = "~Text position" ;
    };
    Control CTL_POSITION
    {
        HelpId = HID_MEASURE_CTL_POSITION ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 154 , 25  ) ;
        Size = MAP_APPFONT ( 60 , 24 ) ;
        TabStop = TRUE ;
    };
    TriStateBox TSB_AUTOPOSV
    {
        Pos = MAP_APPFONT ( 154 , 54 ) ;
        Size = MAP_APPFONT ( 101 , 10 ) ;
        TabStop = TRUE ;
        Text [ en-US ] = "~AutoVertical" ;
    };
    TriStateBox TSB_AUTOPOSH
    {
        Pos = MAP_APPFONT ( 154 , 68  ) ;
        Size = MAP_APPFONT ( 101 , 10 ) ;
        TabStop = TRUE ;
        Text [ en-US ] = "A~utoHorizontal" ;
    };
    TriStateBox TSB_PARALLEL
    {
        Pos = MAP_APPFONT ( 138 , 82 ) ;
        Size = MAP_APPFONT ( 110 , 10 ) ;
        TabStop = TRUE ;
        Text [ en-US ] = "~Parallel to line";
    };
    TriStateBox TSB_SHOW_UNIT
    {
        Pos = MAP_APPFONT ( 138 , 96  ) ;
        Size = MAP_APPFONT ( 64+40 , 10 ) ;
        TabStop = TRUE ;
        Text [ en-US ] = "Show ~meas. units" ;
    };
    ListBox LB_UNIT
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 154 , 110 ) ;
        Size = MAP_APPFONT ( 60 , 80+35 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
    };
    Control CTL_PREVIEW
    {
        HelpId = HID_MEASURE_CTL_PREVIEW ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 132 ) ;
        Size = MAP_APPFONT ( 248 , 47 ) ;
    };
    String STR_MEASURE_AUTOMATIC
    {
        Text [ en-US ] = "Automatic" ;
    };
};
 // ********************************************************************** EOF














































