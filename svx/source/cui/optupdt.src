﻿/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: optupdt.src,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// include --------------------------------------------------------------
#include "optupdt.hrc"
#include <svx/dialogs.hrc>
#include "helpid.hrc"

 // RID_SVXPAGE_ONLINEUPDATE --------------------------------------------
TabPage RID_SVXPAGE_ONLINEUPDATE
{
    HelpId = HID_SVX_OPTIONS_ONLINEUPDATE ;
    OutputSize = TRUE ;
    SVLook = TRUE ;
    Hide = TRUE ;
    Size = MAP_APPFONT ( 260 , 185 ) ;
    Text [ en-US ] = "OnlineUpdate" ;
    FixedLine FL_OPTIONS
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 248 , 8 ) ;
        Text [ en-US ] = "Online Update Options" ;
    };
    CheckBox CB_AUTOCHECK
    {
        Pos = MAP_APPFONT ( 12, 14 ) ;
        Size = MAP_APPFONT ( 210, 10 ) ;
        Text [ en-US ] = "~Check for updates automatically" ;
    };
    RadioButton RB_EVERYDAY
    {
        Pos = MAP_APPFONT ( 18, 27 ) ;
        Size = MAP_APPFONT ( 160, 10 ) ;        
        Text [ en-US ] = "Every Da~y" ;
    };
    RadioButton RB_EVERYWEEK
    {
        Pos = MAP_APPFONT ( 18, 40 ) ;
        Size = MAP_APPFONT ( 160, 10 ) ;        
        Text [ en-US ] = "Every ~Week" ;
    };
    RadioButton RB_EVERYMONTH
    {
        Pos = MAP_APPFONT ( 18, 53 ) ;
        Size = MAP_APPFONT ( 160, 10 ) ;        
        Text [ en-US ] = "Every ~Month" ;
    };
    FixedText FT_LASTCHECKED
    {
        Pos = MAP_APPFONT ( 12, 69 ) ;
        Size = MAP_APPFONT ( 160, 10 ) ;
        Text [ en-US ] = "Last checked: %DATE%, %TIME%" ;
    };
    PushButton PB_CHECKNOW
    {
        Pos = MAP_APPFONT ( 12, 82 ) ;
        Size = MAP_APPFONT ( 50, 14 ) ;
        Text [ en-US ] = "Check ~now" ;
    };
    CheckBox CB_AUTODOWNLOAD
    {
        Pos = MAP_APPFONT ( 12, 130 ) ;
        Size = MAP_APPFONT ( 160, 10 ) ;
        Text [ en-US ] = "~Download updates automatically" ;
    };
    FixedText FT_DESTPATHLABEL
    {
        Pos = MAP_APPFONT ( 22, 143 ) ;
        Size = MAP_APPFONT ( 150, 8 ) ;
        Text [ en-US ] = "Download destination:" ;
    };
    FixedText FT_DESTPATH
    {
        Pos = MAP_APPFONT ( 22, 154 ) ;
        Size = MAP_APPFONT ( 138, 8 ) ;
    };
    PushButton PB_CHANGEPATH
    {
        Pos = MAP_APPFONT ( 162, 151 ) ;
        Size = MAP_APPFONT ( 50, 14 ) ;
        Text [ en-US ] = "Ch~ange..." ;
    };
    String STR_NEVERCHECKED
    {
        Text [ en-US ] = "Last checked: Not yet" ;
    };
};
