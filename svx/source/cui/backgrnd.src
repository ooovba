/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: backgrnd.src,v $
 * $Revision: 1.40 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 // include ---------------------------------------------------------------
#include <svtools/controldims.hrc>
#include "helpid.hrc"
#include <svx/dialogs.hrc>
#include "backgrnd.hrc"
 // define ----------------------------------------------------------------
#define UNLINKED_IMAGE						\
    Text [ en-US ] = "Unlinked graphic" ; \

 // pragma ----------------------------------------------------------------

 // RID_SVXPAGE_BACKGROUND ------------------------------------------------
TabPage RID_SVXPAGE_BACKGROUND
{
    HelpId = HID_BACKGROUND ;
    Hide = TRUE ;
    Size = MAP_APPFONT ( 260 , 185 ) ;
    Text [ en-US ] = "Background" ;
    FixedText FT_SELECTOR
    {
        Pos = MAP_APPFONT ( 6 , 4 ) ;
        Size = MAP_APPFONT ( 24 , 10 ) ;
        Hide = TRUE ;
        LeftLabel = TRUE ;
        Text [ en-US ] = "A~s" ;
    };
    ListBox LB_SELECTOR
    {
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 33 , 3 ) ;
        Size = MAP_APPFONT ( 82 , 48 ) ;
        DropDown = TRUE ;
        StringList [ en-US ] =
        {
            < "Color" ; Default ; > ;
            < "Graphic" ; Default ; > ;
        };
    };
    FixedText FT_TBL_DESC
    {
        Pos = MAP_APPFONT ( 145 , 5 ) ;
        Size = MAP_APPFONT ( 24 , 10 ) ;
        Hide = TRUE ;
        LeftLabel = TRUE ;
        /* ### ACHTUNG: Neuer Text in Resource? F�~r : F�~r */
        Text [ en-US ] = "F~or" ;
    };
    ListBox LB_TBL_BOX
    {
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 172 , 3 ) ;
        Size = MAP_APPFONT ( 82 , 48 ) ;
        DropDown = TRUE ;
        StringList [ en-US ] =
        {
            < "Cell" ; > ;
            < "Row" ; > ;
            < "Table" ; > ;
        };
    };
    ListBox LB_PARA_BOX
    {
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 172 , 3 ) ;
        Size = MAP_APPFONT ( 82 , 48 ) ;
        DropDown = TRUE ;
        StringList [ en-US ] =
        {
            < "Paragraph" ; > ;
            < "Character" ; > ;
        };
    };
     // Hintergrund-Brush  ----------------------------------------------------
    Control CT_BORDER
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 12 , 32 ) ;
        Size = MAP_APPFONT ( 116+2 , 145+2 ) ;
        DialogControl = TRUE;
    };
    Control SET_BGDCOLOR
    {
        HelpId = HID_BACKGROUND_CTL_BGDCOLORSET ;
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 0 , 0 ) ;
        Size = MAP_APPFONT ( 116 , 145 ) ;
        TabStop = TRUE ;
    };
    FixedLine GB_BGDCOLOR
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 , 21 ) ;
        Size = MAP_APPFONT ( 248 , 8) ;
        Text [ en-US ] = "Background color" ;
    };
    Window WIN_PREVIEW1
    {
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 166 , 34 ) ;
        Size = MAP_APPFONT ( 82 , 40 ) ;
    };
    FixedText FT_COL_TRANS
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 166, 74 + RSC_SP_CTRL_Y + 2 ) ;
        Size = MAP_APPFONT ( 50 , 8) ;
        Text [ en-US ] = "~Transparency" ;
    };
    MetricField MF_COL_TRANS
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 218 , 74 + RSC_SP_CTRL_Y ) ;
        Size = MAP_APPFONT ( 30 , 12) ;
        Border = TRUE ;
        Group = TRUE ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_CUSTOM ;
        CustomUnitText = "%" ;
        SpinSize = 5 ;
        Minimum = 0;
        Maximum = 100;
    };

     // Hintergrund-Bitmap ----------------------------------------------------
    FixedLine GB_FILE
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 , 23 ) ;
        Size = MAP_APPFONT ( 153 , 8 ) ;
        Text [ en-US ] = "File" ;
    };
    PushButton BTN_BROWSE
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 + RSC_SP_FLGR_INNERBORDER_LEFT , 23 + RSC_CD_FIXEDLINE_HEIGHT + RSC_SP_FLGR_INNERBORDER_TOP ) ;
        Size = MAP_APPFONT ( 60 , 14 ) ;
        Text [ en-US ] = "~Browse..." ;
    };
    CheckBox BTN_LINK
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 + RSC_SP_FLGR_INNERBORDER_LEFT + 60 + 2*RSC_SP_CTRL_X, 23 + RSC_CD_FIXEDLINE_HEIGHT + RSC_SP_FLGR_INNERBORDER_TOP + 3 ) ;
        Size = MAP_APPFONT ( 78 , 10 ) ;
        Text [ en-US ] = "~Link" ;
    };
    FixedText FT_FILE
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 + RSC_SP_FLGR_INNERBORDER_LEFT , 23 + RSC_CD_FIXEDLINE_HEIGHT + RSC_SP_FLGR_INNERBORDER_TOP + RSC_CD_PUSHBUTTON_HEIGHT + RSC_SP_CTRL_Y ) ;
        Size = MAP_APPFONT ( 141 , 8 ) ;
        UNLINKED_IMAGE
    };
    FixedLine GB_POSITION
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 , 78 ) ;
        Size = MAP_APPFONT ( 153 , 8 ) ;
        Text [ en-US ] = "Type" ;
    };
    RadioButton BTN_POSITION
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 12 , 89 ) ;
        Size = MAP_APPFONT ( 49 , 10 ) ;
        Text [ en-US ] = "~Position" ;
    };
    RadioButton BTN_AREA
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 12 , 103 ) ;
        Size = MAP_APPFONT ( 49 , 10 ) ;
        /* ### ACHTUNG: Neuer Text in Resource? ~Fl�che : ~Fl�che */
        Text [ en-US ] = "Ar~ea" ;
    };
    RadioButton BTN_TILE
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 12 , 117 ) ;
        Size = MAP_APPFONT ( 49 , 10 ) ;
        Text [ en-US ] = "~Tile" ;
    };
    Control WND_POSITION
    {
        HelpId = HID_BACKGROUND_CTL_POSITION ;
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 64 , 89 ) ;
        Size = MAP_APPFONT ( 90 , 87 ) ;
        TabStop = TRUE ;
    };
    FixedLine FL_GRAPH_TRANS
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 , 157 ) ;
        Size = MAP_APPFONT ( 153 , 8) ;
        Text [ en-US ] = "Transparency" ;
    };
    MetricField MF_GRAPH_TRANS
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 12 , 168 ) ;
        Size = MAP_APPFONT ( 30 , 12) ;
        Border = TRUE ;
        Group = TRUE ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_CUSTOM ;
        CustomUnitText = "%" ;
        SpinSize = 5 ;
        Minimum = 0;
        Maximum = 100;
    };
    Window WIN_PREVIEW2
    {
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 170 , 23 + 4 ) ;
        Size = MAP_APPFONT ( 81 , 139 ) ;
    };
    CheckBox BTN_PREVIEW
    {
        Pos = MAP_APPFONT ( 170 , 23 + 4 + 139 + RSC_SP_CTRL_GROUP_Y ) ;
        Size = MAP_APPFONT ( 141 , 10 ) ;
        Text [ en-US ] = "Pre~view" ;
    };
    String STR_BROWSE
    {
        Text [ en-US ] = "Find graphics" ;
    };
    String STR_UNLINKED
    {
        UNLINKED_IMAGE
    };
};
 // ********************************************************************** EOF




































