/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: sdbcdriverenum.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _OFFMGR_SDBCDRIVERENUMERATION_HXX_
#define _OFFMGR_SDBCDRIVERENUMERATION_HXX_


#include <sal/types.h>

#include <vector>

namespace rtl
{
    class OUString;
}

//........................................................................
namespace offapp
{
//........................................................................

    //====================================================================
    //= ODriverEnumeration
    //====================================================================
    class ODriverEnumerationImpl;
    /** simple class for accessing SDBC drivers registered within the office
        <p>Rather small, introduced to not contaminate other instances with the
        exception handling (code-size-bloating) implementations here.
        </p>
    */
    class ODriverEnumeration
    {
    private:
        ODriverEnumerationImpl*	m_pImpl;

    public:
        ODriverEnumeration() throw();
        ~ODriverEnumeration() throw();
        typedef ::std::vector< ::rtl::OUString >::const_iterator const_iterator;

        const_iterator	begin() const throw();
        const_iterator	end() const throw();
        sal_Int32 size() const throw();
    };

//........................................................................
}	// namespace offapp
//........................................................................

#endif // _OFFMGR_SDBCDRIVERENUMERATION_HXX_


