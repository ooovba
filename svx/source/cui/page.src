/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: page.src,v $
 * $Revision: 1.71 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

 // include ---------------------------------------------------------------

#include <svx/dialogs.hrc>
#include "helpid.hrc"
#include "page.hrc"
#include "page.h"

 // RID_SVXPAGE_PAGE ------------------------------------------------------

TabPage RID_SVXPAGE_PAGE
{
    HelpId = HID_FORMAT_PAGE ;
    Hide = TRUE ;
    Size = MAP_APPFONT ( 260 , 185 ) ;
    FixedLine FL_PAPER_SIZE
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text [ en-US ] = "Paper format" ;
    };
    FixedText FT_PAPER_FORMAT
    {
        Pos = MAP_APPFONT ( 12 , 16 ) ;
        Size = MAP_APPFONT ( 35 , 8 ) ;
        Text [ en-US ] = "~Format";
    };
    ListBox LB_PAPER_SIZE
    {
        Pos = MAP_APPFONT ( 50 , 14 ) ;
        Size = MAP_APPFONT ( 50 , 64 ) ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
    };
    FixedText FT_PAPER_WIDTH
    {
        Pos = MAP_APPFONT ( 12 , 32 ) ;
        Size = MAP_APPFONT ( 35 , 8 ) ;
        Text [ en-US ] = "~Width" ;
        Left = TRUE ;
    };
    MetricField ED_PAPER_WIDTH
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 50, 30 ) ;
        Size = MAP_APPFONT ( 40 , 12 ) ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        // #i4219# taken from congiguration now Maximum = 11900 ;
        DecimalDigits = 2 ;
        Unit = FUNIT_CM ;
        // #i4219# Last = 11900 ;
        SpinSize = 10 ;
    };
    FixedText FT_PAPER_HEIGHT
    {
        Pos = MAP_APPFONT ( 12 , 48 ) ;
        Size = MAP_APPFONT ( 35 , 8 ) ;
        Text [ en-US ] = "~Height" ;
        Left = TRUE ;
    };
    MetricField ED_PAPER_HEIGHT
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 50 , 46 ) ;
        Size = MAP_APPFONT ( 40 , 12 ) ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        // #i4219# taken from congiguration now Maximum = 11900 ;
        DecimalDigits = 2 ;
        Unit = FUNIT_CM ;
        // #i4219# Last = 11900 ;
        SpinSize = 10 ;
    };
    FixedText FT_ORIENTATION
    {
        Pos = MAP_APPFONT ( 12 , 62 ) ;
        Size = MAP_APPFONT ( 35 , 8 ) ;
        Text [ en-US ] = "Orientation";
    };
    RadioButton RB_PORTRAIT
    {
        Pos = MAP_APPFONT ( 50 , 62 ) ;
        Size = MAP_APPFONT ( 53 , 10 ) ;
        Check = TRUE ;
        Text [ en-US ] = "~Portrait" ;
    };
    RadioButton RB_LANDSCAPE
    {
        Pos = MAP_APPFONT ( 50 , 74 ) ;
        Size = MAP_APPFONT ( 53 , 10 ) ;
        Text [ en-US ] = "L~andscape";
    };
    Window WN_BSP
    {
        Pos = MAP_APPFONT ( 176 , 6 ) ;
        Size = MAP_APPFONT ( 75 , 46 ) ;
    };

    FixedText FT_TEXT_FLOW
    {
        Pos = MAP_APPFONT ( 113 , 60 ) ;
        Size = MAP_APPFONT ( 50 , 8 ) ;
        Left = TRUE ;
        Hide = TRUE ;
        Text [ en-US ] = "~Text direction";
    };
    ListBox LB_TEXT_FLOW
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 165 , 58 ) ;
        Size = MAP_APPFONT ( 83 , 50 ) ;
        DropDown = TRUE ;
        Hide = TRUE ;
    };

    FixedText FT_PAPER_TRAY
    {
        Pos = MAP_APPFONT ( 113 , 74 ) ;
        Size = MAP_APPFONT ( 50 , 8 ) ;
        Left = TRUE ;
        Text [ en-US ] = "Paper ~tray";
    };
    ListBox LB_PAPER_TRAY
    {
        Pos = MAP_APPFONT ( 165 , 72 ) ;
        Size = MAP_APPFONT ( 83 , 50 ) ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
    };
    FixedLine FL_MARGIN
    {
        Pos = MAP_APPFONT ( 6 , 90 ) ;
        Size = MAP_APPFONT ( 95 , 8 ) ;
        Text [ en-US ] = "Margins";
    };
    FixedText FT_LEFT_MARGIN
    {
        Pos = MAP_APPFONT ( 12 , 103 ) ;
        Size = MAP_APPFONT ( 40 , 8 ) ;
        Text [ en-US ] = "~Left" ;
        Left = TRUE ;
    };
    MetricField ED_LEFT_MARGIN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 55 , 101 ) ;
        Size = MAP_APPFONT ( 40 , 12 ) ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        // #i4219# taken from congiguration now Maximum = 9999 ;
        DecimalDigits = 2 ;
        Unit = FUNIT_CM ;
        // #i4219# Last = 9999 ;
        SpinSize = 10 ;
    };
    FixedText FT_RIGHT_MARGIN
    {
        Pos = MAP_APPFONT ( 12 , 119 ) ;
        Size = MAP_APPFONT ( 40 , 8 ) ;
        Text [ en-US ] = "~Right" ;
        Left = TRUE ;
    };
    MetricField ED_RIGHT_MARGIN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 55 , 117 ) ;
        Size = MAP_APPFONT ( 40 , 12 ) ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        // #i4219# taken from congiguration now Maximum = 9999 ;
        DecimalDigits = 2 ;
        Unit = FUNIT_CM ;
        // #i4219# Last = 9999 ;
        SpinSize = 10 ;
    };
    FixedText FT_TOP_MARGIN
    {
        Pos = MAP_APPFONT ( 12 , 135 ) ;
        Size = MAP_APPFONT ( 40 , 8 ) ;
        Text [ en-US ] = "~Top" ;
        Left = TRUE ;
    };
    MetricField ED_TOP_MARGIN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 55 , 133 ) ;
        Size = MAP_APPFONT ( 40 , 12 ) ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        // #i4219# taken from congiguration now Maximum = 9999 ;
        DecimalDigits = 2 ;
        Unit = FUNIT_CM ;
        // #i4219# Last = 9999 ;
        SpinSize = 10 ;
    };
    FixedText FT_BOTTOM_MARGIN
    {
        Pos = MAP_APPFONT ( 12 , 151 ) ;
        Size = MAP_APPFONT ( 40 , 8 ) ;
        Text [ en-US ] = "~Bottom" ;
        Left = TRUE ;
    };
    MetricField ED_BOTTOM_MARGIN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 55 , 149 ) ;
        Size = MAP_APPFONT ( 40 , 12 ) ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        // #i4219# taken from congiguration now Maximum = 9999 ;
        DecimalDigits = 2 ;
        Unit = FUNIT_CM ;
        // #i4219# Last = 9999 ;
        SpinSize = 10 ;
    };
    FixedLine FL_BOTTOM_SEP
    {
        Pos = MAP_APPFONT ( 102 , 101 ) ;
        Size = MAP_APPFONT ( 4 , 68 ) ;
        Vert = TRUE;
    };
    FixedLine FL_LAYOUT
    {
        Pos = MAP_APPFONT ( 107, 90 ) ;
        Size = MAP_APPFONT ( 147 , 8 ) ;
        Text [ en-US ] = "Layout settings";
    };
    FixedText FT_PAGELAYOUT
    {
        Pos = MAP_APPFONT ( 113 , 103 ) ;
        Size = MAP_APPFONT ( 54 , 8 ) ;
        Text [ en-US ] = "Page layout" ;
    };
    ListBox LB_LAYOUT
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 170, 101 ) ;
        Size = MAP_APPFONT ( 78 , 44 ) ;
        DropDown = TRUE ;
        StringList [ en-US ] =
        {
            < "Right and left" ; Default ; > ;
            < "Mirrored" ; Default ; > ;
            < "Only right" ; Default ; > ;
            < "Only left" ; Default ; > ;
        };
    };
    FixedText FT_NUMBER_FORMAT
    {
        Pos = MAP_APPFONT ( 113 , 119 ) ;
        Size = MAP_APPFONT ( 54 , 8 ) ;
        Text [ en-US ] = "For~mat";
    };
    ListBox LB_NUMBER_FORMAT
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 170 , 117 ) ;
        Size = MAP_APPFONT ( 78 , 64 ) ;
        DropDown = TRUE ;
        StringList [ en-US ] =
        {
            < "A, B, C, ..." ; Default ; > ;
            < "a, b, c, ..." ; Default ; > ;
            < "I, II, III, ..." ; Default ; > ;
            < "i, ii, iii, ..." ; Default ; > ;
            < "1, 2, 3, ..." ; Default ; > ;
            < "None" ; Default ; > ;
        };
    };
    FixedText FT_TBL_ALIGN
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 113 , 133 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "Table alignment";
    };
    CheckBox CB_HORZ
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 175 , 133 ) ;
        Size = MAP_APPFONT ( 73 , 10 ) ;
        Text [ en-US ] = "Hori~zontal" ;
    };
    CheckBox CB_VERT
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 175 , 147 ) ;
        Size = MAP_APPFONT ( 73 , 10 ) ;
        Text [ en-US ] = "~Vertical" ;
    };
    CheckBox CB_ADAPT
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 113 , 133 ) ;
        Size = MAP_APPFONT ( 135 , 10 ) ;
        Text [ en-US ] = "~Fit object to paper format";
    };
    /* Registerhaltigkeit (fuer SW) */
    CheckBox CB_REGISTER
    {
        Pos = MAP_APPFONT ( 113 , 133 ) ;
        Size = MAP_APPFONT ( 135 , 10 ) ;
        Hide = TRUE ;
        Text [ en-US ] = "Register-true" ;
    };
    FixedText FT_REGISTER
    {
        Pos = MAP_APPFONT ( 122 , 147 ) ;
        Size = MAP_APPFONT ( 126 , 8 ) ;
        Text [ en-US ] = "Reference ~Style" ;
        Hide = TRUE ;
    };
    ListBox LB_REGISTER
    {
        Pos = MAP_APPFONT ( 122 , 157 ) ;
        Size = MAP_APPFONT ( 126 , 50 ) ;
        DropDown = TRUE ;
        Hide = TRUE ;
        Sort = TRUE ;
        HScroll = TRUE ;
        DDExtraWidth = TRUE ;
    };
    String STR_INSIDE
    {
        Text [ en-US ] = "I~nner" ;
    };
    String STR_OUTSIDE
    {
        Text [ en-US ] = "O~uter" ;
    };
    String STR_QUERY_PRINTRANGE
    {
        Text [ en-US ] = "The margin settings are out of print range.\n\nDo you still want to apply these settings?";
    };
};
 // App-spezifische Res
String RID_SVXSTR_WRITER_PAGE
{
    Text [ en-US ] = "Continuation page" ;
};
 // Papierformate
String RID_SVXSTR_PAPER_A0
{
    Text = "A0" ;
};
String RID_SVXSTR_PAPER_A1
{
    Text = "A1" ;
};
String RID_SVXSTR_PAPER_A2
{
    Text = "A2" ;
};
String RID_SVXSTR_PAPER_A3
{
    Text = "A3" ;
};
String RID_SVXSTR_PAPER_A4
{
    Text = "A4" ;
};
String RID_SVXSTR_PAPER_A5
{
    Text = "A5" ;
};
String RID_SVXSTR_PAPER_B4_ISO
{
    Text = "B4 (ISO)" ;
};
String RID_SVXSTR_PAPER_B5_ISO
{
    Text = "B5 (ISO)" ;
};
String RID_SVXSTR_PAPER_LETTER
{
    Text = "Letter" ;
};
String RID_SVXSTR_PAPER_LEGAL
{
    Text = "Legal" ;
};
String RID_SVXSTR_PAPER_TABLOID
{
    Text = "Tabloid" ;
};
String RID_SVXSTR_PAPER_USER
{
    Text [ en-US ] = "User Defined" ;
};
String RID_SVXSTR_PAPER_B6_ISO
{
    Text = "B6 (ISO)" ;
};
String RID_SVXSTR_PAPER_C4
{
    Text = "C4 Envelope" ;
};
String RID_SVXSTR_PAPER_C5
{
    Text = "C5 Envelope" ;
};
String RID_SVXSTR_PAPER_C6
{
    Text = "C6 Envelope" ;
};
String RID_SVXSTR_PAPER_C65
{
    Text = "C6/5 Envelope" ;
};
String RID_SVXSTR_PAPER_DL
{
    Text = "DL Envelope" ;
};
String RID_SVXSTR_PAPER_DIA
{
    Text = "Dia Slide" ;
};
String RID_SVXSTR_PAPER_SCREEN
{
    Text [ en-US ] = "Screen" ;
};
String RID_SVXSTR_PAPER_C
{
    Text = "C" ;
};
String RID_SVXSTR_PAPER_D
{
    Text = "D" ;
};
String RID_SVXSTR_PAPER_E
{
    Text = "E" ;
};
String RID_SVXSTR_PAPER_EXECUTIVE
{
       Text = "Executive" ;
};
String RID_SVXSTR_PAPER_LEGAL2
{
    Text = "Long Bond" ;
};
String RID_SVXSTR_PAPER_MONARCH
{
    Text = "#8 (Monarch) Envelope" ;
};
String RID_SVXSTR_PAPER_COM675
{
    Text = "#6 3/4 (Personal) Envelope" ;
};
String RID_SVXSTR_PAPER_COM9
{
    Text = "#9 Envelope" ;
};
String RID_SVXSTR_PAPER_COM10
{
    Text = "#10 Envelope" ;
};
String RID_SVXSTR_PAPER_COM11
{
    Text = "#11 Envelope" ;
};
String RID_SVXSTR_PAPER_COM12
{
    Text = "#12 Envelope" ;
};
String RID_SVXSTR_PAPER_KAI16
{
    Text = "16 Kai" ;
};
String RID_SVXSTR_PAPER_KAI32
{
    Text = "32 Kai" ;
};
String RID_SVXSTR_PAPER_KAI32BIG
{
    Text = "Big 32 Kai" ;
};
String RID_SVXSTR_PAPER_B4_JIS
{
    Text = "B4 (JIS)" ;
};
String RID_SVXSTR_PAPER_B5_JIS
{
    Text = "B5 (JIS)" ;
};
String RID_SVXSTR_PAPER_B6_JIS
{
    Text = "B6 (JIS)" ;
};
StringArray RID_SVXSTRARY_PAPERSIZE_STD
{
    ItemList [ en-US ] =
    {
        < "A5" ; PAPERSIZE_A5 ; > ;
        < "A4" ; PAPERSIZE_A4 ; > ;
        < "A3" ; PAPERSIZE_A3 ; > ;
        < "B6 (ISO)" ; PAPERSIZE_B6_ISO ; > ;
        < "B5 (ISO)" ; PAPERSIZE_B5_ISO ; > ;
        < "B4 (ISO)" ; PAPERSIZE_B4_ISO ; > ;
        < "Letter" ; PAPERSIZE_LETTER ; > ;
        < "Legal" ; PAPERSIZE_LEGAL ; > ;
        < "Long Bond" ; PAPERSIZE_LEGAL2 ; > ;
        < "Tabloid" ; PAPERSIZE_TABLOID ; > ;
        < "B6 (JIS)" ; PAPERSIZE_B6_JIS ; > ;
        < "B5 (JIS)" ; PAPERSIZE_B5_JIS ; > ;
        < "B4 (JIS)" ; PAPERSIZE_B4_JIS ; > ;
        < "16 Kai" ; PAPERSIZE_KAI16; > ;
        < "32 Kai" ; PAPERSIZE_KAI32; > ;
        < "Big 32 Kai" ; PAPERSIZE_KAI32BIG; > ;
        < "User" ; PAPERSIZE_USER ; > ;
        < "DL Envelope" ; PAPERSIZE_DL ; > ;
        < "C6 Envelope" ; PAPERSIZE_C6 ; > ;
        < "C6/5 Envelope" ; PAPERSIZE_C65 ; > ;
        < "C5 Envelope" ; PAPERSIZE_C5 ; > ;
        < "C4 Envelope" ; PAPERSIZE_C4 ; > ;
        < "#6 3/4 (Personal) Envelope" ; PAPERSIZE_COM675; > ;
        < "#8 (Monarch) Envelope" ; PAPERSIZE_MONARCH; > ;
        < "#9 Envelope" ; PAPERSIZE_COM9; > ;
        < "#10 Envelope" ; PAPERSIZE_COM10; > ;
        < "#11 Envelope" ; PAPERSIZE_COM11; > ;
        < "#12 Envelope" ; PAPERSIZE_COM12; > ;
    };
};
StringArray RID_SVXSTRARY_PAPERSIZE_DRAW
{
    ItemList [ en-US ] =
    {
        < "A5" ; PAPERSIZE_A5 ; > ;
        < "A4" ; PAPERSIZE_A4 ; > ;
        < "A3" ; PAPERSIZE_A3 ; > ;
        < "A2" ; PAPERSIZE_A2 ; > ;
        < "A1" ; PAPERSIZE_A1 ; > ;
        < "A0" ; PAPERSIZE_A0 ; > ;
        < "B6 (ISO)" ; PAPERSIZE_B6_ISO ; > ;
        < "B5 (ISO)" ; PAPERSIZE_B5_ISO ; > ;
        < "B4 (ISO)" ; PAPERSIZE_B4_ISO ; > ;
        < "Letter" ; PAPERSIZE_LETTER ; > ;
        < "Legal" ; PAPERSIZE_LEGAL ; > ;
        < "Long Bond" ; PAPERSIZE_LEGAL2 ; > ;
        < "Tabloid" ; PAPERSIZE_TABLOID ; > ;
        < "B6 (JIS)" ; PAPERSIZE_B6_JIS ; > ;
        < "B5 (JIS)" ; PAPERSIZE_B5_JIS ; > ;
        < "B4 (JIS)" ; PAPERSIZE_B4_JIS ; > ;
        < "16 Kai" ; PAPERSIZE_KAI16; > ;
        < "32 Kai" ; PAPERSIZE_KAI32; > ;
        < "Big 32 Kai" ; PAPERSIZE_KAI32BIG; > ;
        < "User" ; PAPERSIZE_USER ; > ;
        < "DL Envelope" ; PAPERSIZE_DL ; > ;
        < "C6 Envelope" ; PAPERSIZE_C6 ; > ;
        < "C6/5 Envelope" ; PAPERSIZE_C65 ; > ;
        < "C5 Envelope" ; PAPERSIZE_C5 ; > ;
        < "C4 Envelope" ; PAPERSIZE_C4 ; > ;
        < "Dia Slide" ; PAPERSIZE_DIA ; > ;
        < "Screen" ; PAPERSIZE_SCREEN ; > ;
    };
};
String RID_SVXSTR_PAPERBIN
{
    Text [ en-US ] = "Paper tray" ;
};
String RID_SVXSTR_PAPERBIN_SETTINGS
{
    Text [ en-US ] = "[From printer settings]" ;
};
 // ********************************************************************** EOF



































































