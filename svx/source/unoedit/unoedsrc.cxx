/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: unoedsrc.cxx,v $
 * $Revision: 1.13 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_svx.hxx"
#include <svtools/brdcst.hxx>

#include <svx/unoedsrc.hxx>


//------------------------------------------------------------------------

void SvxEditSource::addRange( SvxUnoTextRangeBase* )
{
}

//------------------------------------------------------------------------

void SvxEditSource::removeRange( SvxUnoTextRangeBase* )
{
}

//------------------------------------------------------------------------

const SvxUnoTextRangeBaseList& SvxEditSource::getRanges() const
{
    static SvxUnoTextRangeBaseList gList;
    return gList;
}

//------------------------------------------------------------------------

SvxTextForwarder::~SvxTextForwarder()
{
}

//------------------------------------------------------------------------

SvxViewForwarder::~SvxViewForwarder()
{
}

//------------------------------------------------------------------------

SvxEditSource::~SvxEditSource()
{
}

SvxViewForwarder* SvxEditSource::GetViewForwarder()
{
    return NULL;
}

SvxEditViewForwarder* SvxEditSource::GetEditViewForwarder( sal_Bool )
{
    return NULL;
}

SfxBroadcaster&	SvxEditSource::GetBroadcaster() const
{
    DBG_ERROR("SvxEditSource::GetBroadcaster called for implementation missing this feature!");

    static SfxBroadcaster aBroadcaster;

    return aBroadcaster;
}
