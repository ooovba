#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.8 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..

PRJNAME=svx
PROJECTPCH=xout
PROJECTPCHSOURCE=xoutpch
TARGET=xout
ENABLE_EXCEPTIONS=TRUE

# --- Settings -----------------------------------------------------

.INCLUDE :  settings.mk
.INCLUDE :  $(PRJ)$/util$/makefile.pmk

# --- Files --------------------------------------------------------

SLOFILES= \
        $(SLO)$/xattr.obj  		\
        $(SLO)$/xattr2.obj  	\
        $(SLO)$/xattrbmp.obj	\
        $(SLO)$/xpool.obj  		\
        $(SLO)$/xtable.obj 		\
        $(SLO)$/xtabcolr.obj	\
        $(SLO)$/xtablend.obj	\
        $(SLO)$/xtabdash.obj	\
        $(SLO)$/xtabhtch.obj	\
        $(SLO)$/xtabgrdt.obj	\
        $(SLO)$/xtabbtmp.obj	\
        $(SLO)$/xexch.obj		\
        $(SLO)$/_xpoly.obj  	\
        $(SLO)$/_xoutbmp.obj

.INCLUDE :  target.mk
