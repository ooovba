/*************************************************************************
 *
 *  OpenOffice.org - a multi-platform office productivity suite
 *
 *  $RCSfile: linkwarn.hxx,v $
 *
 *  $Revision: $
 *
 *  last change: $Author: $ $Date: $
 *
 *  The Contents of this file are made available subject to
 *  the terms of GNU Lesser General Public License Version 2.1.
 *
 *
 *    GNU Lesser General Public License Version 2.1
 *    =============================================
 *    Copyright 2005 by Sun Microsystems, Inc.
 *    901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License version 2.1, as published by the Free Software Foundation.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *    MA  02111-1307  USA
 *
 ************************************************************************/
#ifndef _SFX_LINKWARN_HXX
#define _SFX_LINKWARN_HXX

#include <vcl/button.hxx>
#include <vcl/fixed.hxx>
#include <sfx2/basedlgs.hxx>
#include "svx/svxdllapi.h"

class SVX_DLLPUBLIC SvxLinkWarningDialog : public SfxModalDialog
{
private:
    FixedImage              m_aQueryImage;
    FixedText               m_aInfoText;
    OKButton                m_aLinkGraphicBtn;
    CancelButton            m_aEmbedGraphicBtn;
    FixedLine               m_aOptionLine;
    CheckBox                m_aWarningOnBox;

    void                    InitSize();

public:
             SvxLinkWarningDialog( Window* pParent, const String& _rFileName );
    virtual ~SvxLinkWarningDialog();
};

#endif // #ifndef _SFX_LINKWARN_HXX

