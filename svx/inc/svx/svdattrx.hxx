/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svdattrx.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVDATTRX_HXX
#define _SVDATTRX_HXX

#include <svx/sxmlhitm.hxx>
#include <svx/sxallitm.hxx>
#include <svx/sxcaitm.hxx>
#include <svx/sxcecitm.hxx>

#ifndef _SXCGIITM_HXX
#include <svx/sxcgitm.hxx>
#endif
#include <svx/sxciaitm.hxx>
#include <svx/sxcikitm.hxx>
#include <svx/sxcllitm.hxx>
#include <svx/sxctitm.hxx>
#include <svx/sxekitm.hxx>
#include <svx/sxelditm.hxx>
#include <svx/sxenditm.hxx>

#ifndef _SXFIITM_HXX
#include <svx/sxfiitm.hxx>
#endif

#ifndef _SXLAYITM_HXX
#include <svx/sxlayitm.hxx>
#endif
#include <svx/sxlogitm.hxx>
#include <svx/sxmbritm.hxx>
#include <svx/sxmfsitm.hxx>
#include <svx/sxmkitm.hxx>
#include <svx/sxmoitm.hxx>
#include <svx/sxmovitm.hxx>

#ifndef _SXMSIITM_HXX
#include <svx/sxmsitm.hxx>
#endif
#include <svx/sxmspitm.hxx>
#include <svx/sxmsuitm.hxx>
#include <svx/sxmtaitm.hxx>
#include <svx/sxmtfitm.hxx>
#include <svx/sxmtpitm.hxx>
#include <svx/sxmtritm.hxx>
#include <svx/sxmuitm.hxx>
#include <svx/sxoneitm.hxx>
#include <svx/sxonitm.hxx>
#include <svx/sxopitm.hxx>
#include <svx/sxraitm.hxx>
#include <svx/sxreaitm.hxx>
#include <svx/sxreoitm.hxx>
#include <svx/sxroaitm.hxx>
#include <svx/sxrooitm.hxx>
#include <svx/sxsaitm.hxx>
#include <svx/sxsalitm.hxx>
#include <svx/sxsiitm.hxx>
#include <svx/sxsoitm.hxx>
#include <svx/sxtraitm.hxx>

#endif  //_SVDATTRX_HXX
