/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: fntszctl.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_FNTSZCTL_HXX
#define _SVX_FNTSZCTL_HXX

// include ---------------------------------------------------------------

#include <sfx2/mnuitem.hxx>
#include "svx/svxdllapi.h"



class SfxBindings;
class FontSizeMenu;

// class SvxFontSizeMenuControl ------------------------------------------

class SVX_DLLPUBLIC SvxFontSizeMenuControl : public SfxMenuControl
{
private:
    FontSizeMenu*	pMenu;
    Menu&			rParent;
    SfxStatusForwarder	aFontNameForwarder;

//#if 0 // _SOLAR__PRIVATE
    DECL_LINK( MenuSelect, FontSizeMenu * );
//#endif

protected:
    virtual void	StateChanged( USHORT nSID, SfxItemState eState,
                                  const SfxPoolItem* pState );

public:
    SvxFontSizeMenuControl(	USHORT nId, Menu&, SfxBindings& );
    ~SvxFontSizeMenuControl();

    virtual PopupMenu*  GetPopup() const;

    SFX_DECL_MENU_CONTROL();
};



#endif

