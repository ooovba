/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svdedtv.hxx,v $
 * $Revision: 1.3.148.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVDEDTV_HXX
#define _SVDEDTV_HXX

#include <svx/svdmrkv.hxx>
#include <svx/xpoly.hxx>
#include <svx/svdmodel.hxx>
#include "svx/svxdllapi.h"

//************************************************************
//   Vorausdeklarationen
//************************************************************

class SfxUndoAction;
class SdrUndoAction;
class SdrUndoGroup;
class SfxStyleSheet;
class SdrLayer;
class SvdProgressInfo;

//************************************************************
//   Defines
//************************************************************

enum SdrHorAlign  {
    SDRHALIGN_NONE,
    SDRHALIGN_LEFT,
    SDRHALIGN_RIGHT,
    SDRHALIGN_CENTER
};

enum SdrVertAlign {
    SDRVALIGN_NONE,
    SDRVALIGN_TOP,
    SDRVALIGN_BOTTOM,
    SDRVALIGN_CENTER
};

enum SdrMergeMode {
    SDR_MERGE_MERGE,
    SDR_MERGE_SUBSTRACT,
    SDR_MERGE_INTERSECT
};

// Optionen fuer InsertObject()
#define SDRINSERT_DONTMARK    0x0001 /* Obj wird nicht markiert (aktuelle Markierung bleibt bestehen) */
#define SDRINSERT_ADDMARK     0x0002 /* Das Obj wird zu einer ggf. bereits bestehenden Selektion hinzumarkiert */
#define SDRINSERT_SETDEFATTR  0x0004 /* Die aktuellen Attribute (+StyleSheet) werden dem Obj zugewiesen */
#define SDRINSERT_SETDEFLAYER 0x0008 /* Der aktuelle Layer wird dem Obj zugewiesen */
#define SDRINSERT_NOBROADCAST 0x0010 /* Einfuegen mit NbcInsertObject() fuer SolidDragging */

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  @@@@@ @@@@@  @@ @@@@@@  @@ @@ @@ @@@@@ @@   @@
//  @@    @@  @@ @@   @@    @@ @@ @@ @@    @@   @@
//  @@    @@  @@ @@   @@    @@ @@ @@ @@    @@ @ @@
//  @@@@  @@  @@ @@   @@    @@@@@ @@ @@@@  @@@@@@@
//  @@    @@  @@ @@   @@     @@@  @@ @@    @@@@@@@
//  @@    @@  @@ @@   @@     @@@  @@ @@    @@@ @@@
//  @@@@@ @@@@@  @@   @@      @   @@ @@@@@ @@   @@
//
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

class SVX_DLLPUBLIC SdrEditView: public SdrMarkView
{
    friend class				SdrPageView;
    friend class				SdrDragDistort;
    friend class				SdrDragCrook;

protected:

    // Die Transformationsnachfragen, etc. etwas cachen
    unsigned					bPossibilitiesDirty : 1;
    unsigned					bReadOnly : 1;
    unsigned					bGroupPossible : 1;
    unsigned					bUnGroupPossible : 1;
    unsigned					bGrpEnterPossible : 1;
    unsigned					bDeletePossible : 1;
    unsigned					bToTopPossible : 1;
    unsigned					bToBtmPossible : 1;
    unsigned					bReverseOrderPossible : 1;
    unsigned					bImportMtfPossible : 1;
    unsigned					bCombinePossible : 1;
    unsigned					bDismantlePossible : 1;
    unsigned					bCombineNoPolyPolyPossible : 1;
    unsigned					bDismantleMakeLinesPossible : 1;
    unsigned					bOrthoDesiredOnMarked : 1;
    unsigned					bMoreThanOneNotMovable : 1;   // Es ist mehr als ein Objekt nicht verschiebbar
    unsigned					bOneOrMoreMovable : 1;        // Wenigstens 1 Obj verschiebbar
    unsigned					bMoreThanOneNoMovRot : 1;     // Es ist mehr als ein Objekt nicht verschieb- und drehbar (Crook)
    unsigned					bContortionPossible : 1;      // Alles Polygone (ggf. gruppiert)
    unsigned					bAllPolys : 1;                // Alles Polygone (nicht gruppiert)
    unsigned					bOneOrMorePolys : 1;          // Mindestens 1 Polygon (nicht gruppiert)
    unsigned					bMoveAllowed : 1;
    unsigned					bResizeFreeAllowed : 1;
    unsigned					bResizePropAllowed : 1;
    unsigned					bRotateFreeAllowed : 1;
    unsigned					bRotate90Allowed : 1;
    unsigned					bMirrorFreeAllowed : 1;
    unsigned					bMirror45Allowed : 1;
    unsigned					bMirror90Allowed : 1;
    unsigned					bShearAllowed : 1;
    unsigned					bEdgeRadiusAllowed : 1;
    unsigned					bTransparenceAllowed : 1;
    unsigned					bGradientAllowed : 1;
    unsigned					bCanConvToPath : 1;
    unsigned					bCanConvToPoly : 1;
    unsigned					bCanConvToContour : 1;
    unsigned					bCanConvToPathLineToArea : 1;
    unsigned					bCanConvToPolyLineToArea : 1;
    unsigned					bMoveProtect : 1;
    unsigned					bResizeProtect : 1;
    // Z-Order von virtuellen Objekten zusammenhalten (Writer)
    unsigned					bBundleVirtObj : 1;

private:
    SVX_DLLPRIVATE void ImpClearVars();
    SVX_DLLPRIVATE void ImpResetPossibilityFlags();

protected:
    void ImpBroadcastEdgesOfMarkedNodes();

    // Konvertierung der markierten Objekte in Poly bzw. Bezier.
    void ImpConvertTo(BOOL bPath, BOOL bLineToArea);

    // Konvertiert ein Obj, wirft bei Erfolg das alte as seiner Liste und
    // fuegt das neue an dessen Position ein. Inkl Undo. Es wird weder ein
    // MarkEntry noch ein ModelChgBroadcast generiert.
    SdrObject* ImpConvertOneObj(SdrObject* pObj, BOOL bPath, BOOL bLineToArea);

    // Setzen der beiden Flags bToTopPossible und bToBtmPossible.
    // bToTopPossibleDirty und bToBtmPossibleDirty werden dabei gleichzeitig
    // zurueckgesetzt.
    void ImpCheckToTopBtmPossible();

    // fuer den Writer werden virtuelle Objekte buendig zusammengehalten (Z-Order)
    void ImpBundleVirtObjOfMarkList();

    // fuer CombineMarkedObjects und DismantleMarkedObjects
    void ImpCopyAttributes(const SdrObject* pSource, SdrObject* pDest) const;

    // fuer CombineMarkedObjects
    sal_Bool ImpCanConvertForCombine1(const SdrObject* pObj) const;
    sal_Bool ImpCanConvertForCombine(const SdrObject* pObj) const;
    basegfx::B2DPolyPolygon ImpGetPolyPolygon1(const SdrObject* pObj, sal_Bool bCombine) const;
    basegfx::B2DPolyPolygon ImpGetPolyPolygon(const SdrObject* pObj, sal_Bool bCombine) const;
    basegfx::B2DPolygon ImpCombineToSinglePolygon(const basegfx::B2DPolyPolygon& rPolyPolygon) const;

    // fuer DismantleMarkedObjects
    sal_Bool ImpCanDismantle(const basegfx::B2DPolyPolygon& rPpolyPpolygon, sal_Bool bMakeLines) const;
    sal_Bool ImpCanDismantle(const SdrObject* pObj, sal_Bool bMakeLines) const;
    void ImpDismantleOneObject(const SdrObject* pObj, SdrObjList& rOL, ULONG& rPos, SdrPageView* pPV, BOOL bMakeLines);
    void ImpCrookObj(SdrObject* pO, const Point& rRef, const Point& rRad, SdrCrookMode eMode,
        BOOL bVertical, BOOL bNoContortion, BOOL bRotate, const Rectangle& rMarkRect);
    void ImpDistortObj(SdrObject* pO, const Rectangle& rRef, const XPolygon& rDistortedRect, BOOL bNoContortion);
    BOOL ImpDelLayerCheck(SdrObjList* pOL, SdrLayerID nDelID) const;
    void ImpDelLayerDelObjs(SdrObjList* pOL, SdrLayerID nDelID);

    // Entfernt alle Obj der MarkList aus ihren ObjLists inkl Undo.
    // Die Eintraege in rMark bleiben erhalten.
    void DeleteMarkedList(const SdrMarkList& rMark); // DeleteMarked -> DeleteMarkedList

    // Die Transformationsnachfragen etwas cachen
    //void ImpCheckMarkTransform() const; veraltet
    // Checken, was man so mit den markierten Objekten alles machen kann
    virtual void CheckPossibilities();
    void ForcePossibilities() const { if (bPossibilitiesDirty || bSomeObjChgdFlag) ((SdrEditView*)this)->CheckPossibilities(); }

protected:
    // #i71538# make constructors of SdrView sub-components protected to avoid incomplete incarnations which may get casted to SdrView
    SdrEditView(SdrModel* pModel1, OutputDevice* pOut = 0L);
    virtual ~SdrEditView();

public:
    // Jeder Aufruf einer undofaehigen Methode an der View generiert eine
    // UndoAction. Moechte man mehrere
    // Methodenaufrufe zu einer UndoAction zusammenfassen, so kann man diese
    // mit BegUndo() / EndUndo() klammern (beliebig tief). Als Kommentar der
    // UndoAction wird der des ersten BegUndo(String) aller Klammerungen
    // verwendet. NotifyNewUndoAction() wird in diesem Fall erst beim letzten
    // EndUndo() gerufen. NotifyNewUndoAction() wird nicht gerufen bei einer
    // leeren Klammerung.
#ifndef WIN
    void BegUndo()                       { pMod->BegUndo();         } // Undo-Klammerung auf
    void BegUndo(const String& rComment) { pMod->BegUndo(rComment); } // Undo-Klammerung auf
    void BegUndo(const String& rComment, const String& rObjDescr, SdrRepeatFunc eFunc=SDRREPFUNC_OBJ_NONE) { pMod->BegUndo(rComment,rObjDescr,eFunc); } // Undo-Klammerung auf
    void BegUndo(SdrUndoGroup* pUndoGrp) { pMod->BegUndo(pUndoGrp); } // Undo-Klammerung auf
    void EndUndo();                                                   // Undo-Klammerung zu (inkl BroadcastEdges)
    void AddUndo(SdrUndoAction* pUndo)   { pMod->AddUndo(pUndo);    } // Action hinzufuegen
    // nur nach dem 1. BegUndo oder vor dem letzten EndUndo:
    void SetUndoComment(const String& rComment) { pMod->SetUndoComment(rComment); }
    void SetUndoComment(const String& rComment, const String& rObjDescr) { pMod->SetUndoComment(rComment,rObjDescr); }
#else  // ifndef WIN
    void BegUndo();
    void BegUndo(const String& rComment);
    void BegUndo(const String& rComment, const String& rObjDescr, SdrRepeatFunc eFunc=SDRREPFUNC_OBJ_NONE);
    void BegUndo(SdrUndoGroup* pUndoGrp);
    void EndUndo();                                                   // Undo-Klammerung zu (inkl BroadcastEdges)
    void AddUndo(SdrUndoAction* pUndo);
    // nur nach dem 1. BegUndo oder vor dem letzten EndUndo:
    void SetUndoComment(const String& rComment);
    void SetUndoComment(const String& rComment, const String& rObjDescr);
#endif
    bool IsUndoEnabled() const;

    std::vector< SdrUndoAction* > CreateConnectorUndo( SdrObject& rO );
    void AddUndoActions( std::vector< SdrUndoAction* >& );

    // Layerverwaltung. Mit Undo.
    SdrLayer* InsertNewLayer(const String& rName, USHORT nPos=0xFFFF);
    // Loeschen eines Layer inkl. aller darauf befindlichen Objekte
    void      DeleteLayer(const String& rName);
    // Verschieben eines Layer (Layerreihenfolge aendern)
    void      MoveLayer(const String& rName, USHORT nNewPos);

    // Markierte Objekte die ausserhalb ihrer Page liegen
    // werden ggf. einer anderen Page zugewiesen
    // z.Zt. noch ohne Undo!!!
    void ForceMarkedObjToAnotherPage();
    void ForceMarkedToAnotherPage()   { ForceMarkedObjToAnotherPage(); }

    BOOL IsReadOnly() const { ForcePossibilities(); return bReadOnly; }

    // Loeschen aller markierten Objekte
    void DeleteMarkedObj();
    BOOL IsDeleteMarkedObjPossible() const { ForcePossibilities(); return bDeletePossible; }

    // Logisch- umschliessendes Rect aller markierten Objekte setzen.
    // Das das wirklich geschieht ist nicht garantiert, denn eine
    // waagerechte Linie hat z.B. immer eine Hoehe von 0.
    void SetMarkedObjRect(const Rectangle& rRect, BOOL bCopy=FALSE);
    void MoveMarkedObj(const Size& rSiz, bool bCopy=false);
    void ResizeMarkedObj(const Point& rRef, const Fraction& xFact, const Fraction& yFact, bool bCopy=false);
    long GetMarkedObjRotate() const;
    void RotateMarkedObj(const Point& rRef, long nWink, bool bCopy=false);
    void MirrorMarkedObj(const Point& rRef1, const Point& rRef2, bool bCopy=false);
    void MirrorMarkedObjHorizontal(BOOL bCopy=FALSE);
    void MirrorMarkedObjVertical(BOOL bCopy=FALSE);
    long GetMarkedObjShear() const;
    void ShearMarkedObj(const Point& rRef, long nWink, bool bVShear=false, bool bCopy=false);
    void CrookMarkedObj(const Point& rRef, const Point& rRad, SdrCrookMode eMode, bool bVertical=false, bool bNoContortion=false, bool bCopy=false);
    void DistortMarkedObj(const Rectangle& rRef, const XPolygon& rDistortedRect, bool bNoContortion=false, bool bCopy=false);

    // Markierte Objekte kopieren und anstelle der alten markieren
    void CopyMarkedObj();
    void SetAllMarkedRect(const Rectangle& rRect, BOOL bCopy=FALSE) { SetMarkedObjRect(rRect,bCopy); }
    void MoveAllMarked(const Size& rSiz, BOOL bCopy=FALSE) { MoveMarkedObj   (rSiz,bCopy); }
    void ResizeAllMarked(const Point& rRef, const Fraction& xFact, const Fraction& yFact, BOOL bCopy=FALSE) { ResizeMarkedObj (rRef,xFact,yFact,bCopy); }
    long GetAllMarkedRotate() const { return GetMarkedObjRotate(); }
    void RotateAllMarked(const Point& rRef, long nWink, BOOL bCopy=FALSE) { RotateMarkedObj(rRef,nWink,bCopy); }
    void MirrorAllMarked(const Point& rRef1, const Point& rRef2, BOOL bCopy=FALSE) { MirrorMarkedObj(rRef1,rRef2,bCopy); }
    void MirrorAllMarkedHorizontal(BOOL bCopy=FALSE) { MirrorMarkedObjHorizontal(bCopy); }
    void MirrorAllMarkedVertical(BOOL bCopy=FALSE) { MirrorMarkedObjVertical(bCopy); }
    long GetAllMarkedShear() const { return GetMarkedObjShear(); }
    void ShearAllMarked(const Point& rRef, long nWink, BOOL bVShear=FALSE, BOOL bCopy=FALSE) { ShearMarkedObj(rRef,nWink,bVShear,bCopy); }
    void CrookAllMarked(const Point& rRef, const Point& rRad, SdrCrookMode eMode, BOOL bVertical=FALSE, BOOL bNoContortion=FALSE, BOOL bCopy=FALSE) { CrookMarkedObj(rRef,rRad,eMode,bVertical,bNoContortion,bCopy); }
    void CopyMarked() { CopyMarkedObj(); }
    BOOL IsMoveAllowed() const { ForcePossibilities(); return bMoveAllowed && !bMoveProtect; }
    BOOL IsResizeAllowed(BOOL bProp=FALSE) const;
    BOOL IsRotateAllowed(BOOL b90Deg=FALSE) const;
    BOOL IsMirrorAllowed(BOOL b45Deg=FALSE, BOOL b90Deg=FALSE) const;
    BOOL IsTransparenceAllowed() const;
    BOOL IsGradientAllowed() const;
    BOOL IsShearAllowed() const;
    BOOL IsEdgeRadiusAllowed() const;
    BOOL IsCrookAllowed(BOOL bNoContortion=FALSE) const;
    BOOL IsDistortAllowed(BOOL bNoContortion=FALSE) const;

    // Vereinigen mehrerer Objekte zu einem PolyPolygon:
    // - Rechtecke/Kreise/Text... werden implizit gewandelt.
    // - Polylines werden automatisch geschlossen.
    // - Die Attribute und der Layer werden vom Ersten der markierten Objekte
    //   uebernommen (also vom untersten der Z-Order).
    // - Gruppenobjekte werden miteinbezogen, wenn alle! Memberobjekte der
    //   Gruppe wandelbar sind. Beinhaltet eine Gruppe also beispielsweise
    //   eine Bitmap oder ein OLE-Objekt, wird die gesamte Gruppe nicht
    //   beruecksichtigt.
    // bNoPolyPoly=TRUE: Alles wird zu einem einzigen Polygon zusammengefasst
    void CombineMarkedObjects(sal_Bool bNoPolyPoly = sal_True);

    // for combining multiple polygons, with direct support of the modes
    // SID_POLY_MERGE, SID_POLY_SUBSTRACT, SID_POLY_INTERSECT
    void MergeMarkedObjects(SdrMergeMode eMode);

    // for distribution dialog function
    void DistributeMarkedObjects();

    // Markierte Polypolygonobjekte in Polygone zerlegen
    // Gruppenobjekte werden durchsucht und zerlegt, wenn es sich bei allen
    // Memberobjekten um PathObjs handelt.
    // bMakeLines=TRUE: alle Polygone werden in einzelne Linien bzw.
    //                  Beziersegmente zerlegt
    void DismantleMarkedObjects(BOOL bMakeLines=FALSE);
    BOOL IsCombinePossible(BOOL bNoPolyPoly=FALSE) const;
    BOOL IsDismantlePossible(BOOL bMakeLines=FALSE) const;

    // Ein neues bereits fertig konstruiertes Obj einfuegen. Das Obj gehoert
    // anschliessend dem Model. Nach dem Einfuegen wird das neue Objekt
    // markiert (wenn dies nicht via nOptions unterbunden wird).
    // U.U. wird das Obj jedoch nicht eingefuegt, sondern deleted, naemlich
    // wenn der Ziel-Layer gesperrt oder nicht sichtbar ist. In diesem Fall
    // returniert die Methode mit FALSE.
    // Die Methode generiert u.a. auch eine Undo-Action.
    BOOL InsertObjectAtView(SdrObject* pObj, SdrPageView& rPV, ULONG nOptions=0);

    // Ein Zeichenobjekt durch ein neues ersetzen. *pNewObj gehoert
    // anschliessend mir, *pOldObj wandert ins Undo.
    // Sollte in jedem Fall mit einer Undo-Klammerung versehen werden, z.B.:
    // aStr+=" ersetzen";
    // BegUndo(aStr);
    // ReplaceObject(...);
    // ...
    // EndUndo();
    void ReplaceObjectAtView(SdrObject* pOldObj, SdrPageView& rPV, SdrObject* pNewObj, BOOL bMark=TRUE);

    void SetNotPersistAttrToMarked(const SfxItemSet& rAttr, BOOL bReplaceAll);
    void MergeNotPersistAttrFromMarked(SfxItemSet& rAttr, BOOL bOnlyHardAttr) const;
    void MergeAttrFromMarked(SfxItemSet& rAttr, BOOL bOnlyHardAttr) const;
    SfxItemSet GetAttrFromMarked(BOOL bOnlyHardAttr) const;
    void SetAttrToMarked(const SfxItemSet& rAttr, BOOL bReplaceAll);

    // Geometrische Attribute (Position, Groesse, Drehwinkel)
    // Bei der Position wird ein evtl. gesetzter PageOrigin beruecksichtigt.
    SfxItemSet GetGeoAttrFromMarked() const;
    void SetGeoAttrToMarked(const SfxItemSet& rAttr);

    // Returnt NULL wenn:
    // - Nix markiert,
    // - kein StyleSheet an den markierten Objekten gesetzt
    // - Bei Mehrfachselektion die markierten Objekte auf unterschiedliche
    //   StyleSheets verweisen.
    SfxStyleSheet* GetStyleSheetFromMarked() const;

    // z.Zt. noch ohne Undo :(
    void SetStyleSheetToMarked(SfxStyleSheet* pStyleSheet, BOOL bDontRemoveHardAttr);

    /* new interface src537 */
    BOOL GetAttributes(SfxItemSet& rTargetSet, BOOL bOnlyHardAttr) const;

    BOOL SetAttributes(const SfxItemSet& rSet, BOOL bReplaceAll);
    SfxStyleSheet* GetStyleSheet() const; // SfxStyleSheet* GetStyleSheet(BOOL& rOk) const;
    BOOL SetStyleSheet(SfxStyleSheet* pStyleSheet, BOOL bDontRemoveHardAttr);

    // Alle markierten Objekte zu einer Gruppe zusammenfassen.
    // Anschliessend wird die neue Gruppe markiert. Bei einer
    // seitenuebergreifenden Markierung wird eine Gruppe je Seite erzeugt.
    // Alle erzeugten Gruppen sind anschliessend markiert.
    // Ueber pUserGrp kann ein eigenes Gruppenobjekt vorgegeben werden. Dieses
    // wird  jedoch nicht direkt verwendet, sondern via Clone kopiert.
    // Wird NULL uebergeben, macht sich die Methode SdrObjGroup-Instanzen.
    void GroupMarked(const SdrObject* pUserGrp=NULL);

    // Alle markierten Objektgruppen werden aufgeloesst (1 Level).
    // Anschliessend sind statt der Gruppenobjekte alle ehemaligen
    // Memberobjekte der aufgeloesten Gruppen markiert. Waren zum auch Objekte
    // markiert, die keine Gruppenobjekte sind, so bleiben diese weiterhin
    // zusaetzlich markiert.
    void UnGroupMarked();

    BOOL IsGroupPossible() const { ForcePossibilities(); return bGroupPossible; }
    BOOL IsUnGroupPossible() const { ForcePossibilities(); return bUnGroupPossible; }
    BOOL IsGroupEnterPossible() const { ForcePossibilities(); return bGrpEnterPossible; }

    // Markierte Objekte in Polygone/Bezierkurven verwandeln. Die BOOL-
    // Funktionen returnen TRUE, wenn wenigstens eins der markierten
    // Objekte gewandelt werden kann. Memberobjekte von Gruppenobjekten
    // werden ebenfalls gewandelt. Naehere Beschreibung siehe SdrObj.HXX.
    BOOL IsConvertToPathObjPossible(BOOL bLineToArea) const { ForcePossibilities(); return BOOL(bLineToArea ? bCanConvToPathLineToArea : bCanConvToPath); }
    BOOL IsConvertToPolyObjPossible(BOOL bLineToArea) const { ForcePossibilities(); return BOOL(bLineToArea ? bCanConvToPolyLineToArea : bCanConvToPoly); }
    BOOL IsConvertToContourPossible() const { ForcePossibilities(); return bCanConvToContour; }
    void ConvertMarkedToPathObj(BOOL bLineToArea);
    void ConvertMarkedToPolyObj(BOOL bLineToArea);

    // Alle markierten Objekte untereinander ausrichten. Normalerweise werden
    // das SnapRect der Obj verwendet. Ist bBoundRects=TRUE, werden stattdessen
    // die BoundRects ausgerichtet.
    void AlignMarkedObjects(SdrHorAlign eHor, SdrVertAlign eVert, BOOL bBoundRects=FALSE);
    BOOL IsAlignPossible() const;

    // Markierte Objekte etwas nach "oben" holen
    void MovMarkedToTop();

    // Markierte Objekte etwas nach "unten" holen
    void MovMarkedToBtm();

    // Markierte Objekte ganz nach "oben" stellen
    void PutMarkedToTop();

    // Markierte Objekte ganz nach "unten" stellen
    void PutMarkedToBtm();

    // Markierte direkt vor das uebergebene Objekt stellen
    // NULL -> wie PutMarkedToTop();
    void PutMarkedInFrontOfObj(const SdrObject* pRefObj);

    // Markierte direkt hinter das uebergebene Objekt stellen
    // NULL -> wie PutMarkedToBtm();
    void PutMarkedBehindObj(const SdrObject* pRefObj);

    // Z-Order der markierten Objekte vertauschen
    void ReverseOrderOfMarked();

    // Feststellen, ob nach vorn/hinten stellen moeglich ist
    // GetMaxToTop/BtmObj() wird von diesen Methoden nur begrenzt
    // beruecksichtigt, d.h. es kann vorkommen dass IsToTopPossible()
    // TRUE liefert, MovMarkedToTop() jedoch nichts aendert (z.B. bei
    // Mehrfachselektion), weil eine von der abgeleiteten View ueber
    // GetMaxToTopObj() auferlegte Restriktion dies verhindert.
    BOOL IsToTopPossible() const { ForcePossibilities(); return bToTopPossible; }
    BOOL IsToBtmPossible() const { ForcePossibilities(); return bToBtmPossible; }
    BOOL IsReverseOrderPossible() const { ForcePossibilities(); return bReverseOrderPossible; }

    // Ueber diese Methoden stellt die View fest, wie weit ein Objekt
    // nach vorn bzw. nach hinten gestellt werden darf (Z-Order). Das
    // zurueckgegebene Objekt wird dann nicht "ueberholt". Bei Rueckgabe
    // von NULL (Defaultverhalten) bestehen keine Restriktionen.
    virtual SdrObject* GetMaxToTopObj(SdrObject* pObj) const;
    virtual SdrObject* GetMaxToBtmObj(SdrObject* pObj) const;

    // Folgende Methode wird gerufen, wenn z.B. durch ToTop, ToBtm, ... die
    // Reihenfolgen der Objekte geaendert wurde. Der Aufruf erfolgt dann nach
    // jedem SdrObjList::SetObjectOrdNum(nOldPos,nNewPos);
    virtual void ObjOrderChanged(SdrObject* pObj, ULONG nOldPos, ULONG nNewPos);

    // Falls ein oder mehrere Objekte des Types SdrGrafObj oder SdrOle2Obj
    // markiert sind und diese in der Lage sind ein StarView-Metafile zu
    // liefern, konvertiert diese Methode das Metafile in Drawingobjekte.
    // Die SdrGrafObjs/SdrOle2Objs werden dann durch die neue Objekte ersetzt.
    void DoImportMarkedMtf(SvdProgressInfo *pProgrInfo=NULL);
    BOOL IsImportMtfPossible() const { ForcePossibilities(); return bImportMtfPossible; }

    // Wird der Modus VirtualObjectBundling eingeschaltet, werden beim
    // ToTop/ToBtm virtuelle Objekte die dasselbe Objekt referenzieren
    // in ihrer Z-Order buendig zusammengehalten (Writer).
    // Defaulteinstellung ist FALSE=ausgeschaltet.
    void SetVirtualObjectBundling(BOOL bOn) { bBundleVirtObj=bOn; }
    BOOL IsVirtualObjectBundling() const { return bBundleVirtObj; }

    // von der SdrMarkView ueberladen fuer den internen gebrauch
    virtual void MarkListHasChanged();
    virtual void ModelHasChanged();
};

#endif //_SVDEDTV_HXX

