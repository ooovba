/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svdattr.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVDATTR_HXX
#define _SVDATTR_HXX

#ifndef _SOLAR_HRC
#include <svtools/solar.hrc>
#endif
#include <tools/solar.h>
#include <svx/sdangitm.hxx>
#include <svx/sderitm.hxx>
#include <svx/sdmetitm.hxx>
#include <svx/sdooitm.hxx>
#include <svx/sdprcitm.hxx>
#include <svx/sdshcitm.hxx>
#include <svx/sdshitm.hxx>
#include <svx/sdshtitm.hxx>
#include <svx/sdsxyitm.hxx>
#include <svx/sdtaaitm.hxx>
#ifndef _SDTACITM_HXX
#include <svx/sdtacitm.hxx>
#endif
#ifndef _SDTACITM_HXX
#include <svx/sdtaditm.hxx>
#endif
#include <svx/sdtagitm.hxx>
#include <svx/sdtaiitm.hxx>
#include <svx/sdtaitm.hxx>
#ifndef _SDTAKITM_HXX
#include <svx/sdtakitm.hxx>
#endif
#include <svx/sdtayitm.hxx>
#ifndef SDTCFITM_HXX
#include <svx/sdtcfitm.hxx>
#endif
#ifndef _SDTDITM_HXX
#include <svx/sdtditm.hxx>
#endif
#include <svx/sdtfsitm.hxx>
#include <svx/sdtmfitm.hxx>
#include <svx/sdynitm.hxx>
#include <svx/sdgluitm.hxx>
#include <svx/sdginitm.hxx>
#include <svx/sdgtritm.hxx>
#include <svx/sdgcoitm.hxx>
#include <svx/sdggaitm.hxx>
#include <svx/sdasitm.hxx>
#include <svx/sdgmoitm.hxx>
#include <svx/sdasaitm.hxx>
#endif

