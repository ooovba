/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: opengrf.hxx,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_OPENGRF_HXX
#define _SVX_OPENGRF_HXX

#include <memory>		// auto_ptr
#include <svtools/filter.hxx>
#include "svx/svxdllapi.h"


struct  SvxOpenGrf_Impl;

class SVX_DLLPUBLIC SvxOpenGraphicDialog
{
public:
    SvxOpenGraphicDialog	( const String& rTitle );
    ~SvxOpenGraphicDialog	();

    short					Execute();

    void					SetPath( const String& rPath );
    void					SetPath( const String& rPath, sal_Bool bLinkState );
    String					GetPath() const;

    int 					GetGraphic(Graphic&) const;

    void					EnableLink(sal_Bool);
    void					AsLink(sal_Bool);
    sal_Bool				IsAsLink() const;

    String					GetCurrentFilter() const;
    void					SetCurrentFilter(const String&);

    /// Set dialog help id at FileDlgHelper
    void					SetControlHelpIds( const INT16* _pControlId, const INT32* _pHelpId );
    /// Set control help ids at FileDlgHelper
    void					SetDialogHelpId( const INT32 _nHelpId );
private:
    // disable copy and assignment	
    SVX_DLLPRIVATE SvxOpenGraphicDialog	(const SvxOpenGraphicDialog&);
    SVX_DLLPRIVATE SvxOpenGraphicDialog& operator = ( const SvxOpenGraphicDialog & );
    
    const std::auto_ptr< SvxOpenGrf_Impl >	mpImpl;
};

#endif // _SVX_OPENGRF_HXX

