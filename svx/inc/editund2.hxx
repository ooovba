/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: editund2.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _EDITUND2_HXX
#define _EDITUND2_HXX

#include "svx/svxdllapi.h"
#include <svtools/undo.hxx>

class ImpEditEngine;

class SVX_DLLPUBLIC EditUndoManager : public SfxUndoManager
{
    using SfxUndoManager::Undo;
    using SfxUndoManager::Redo;

private:
    ImpEditEngine*	pImpEE;
public:
                    EditUndoManager( ImpEditEngine* pImpEE );

    virtual BOOL	Undo( USHORT nCount=1 );
    virtual BOOL	Redo( USHORT nCount=1 );
};

// -----------------------------------------------------------------------
// EditUndo
// ------------------------------------------------------------------------
class SVX_DLLPUBLIC EditUndo : public SfxUndoAction
{
private:
    USHORT 			nId;
    ImpEditEngine*	pImpEE;

public:
                    TYPEINFO();
                    EditUndo( USHORT nI, ImpEditEngine* pImpEE );
    virtual 		~EditUndo();

    ImpEditEngine*	GetImpEditEngine() const	{ return pImpEE; }

    virtual void	Undo() 		= 0;
    virtual void	Redo()		= 0;

    virtual BOOL	CanRepeat(SfxRepeatTarget&) const;
    virtual String	GetComment() const;
    virtual USHORT	GetId() const;
};

#endif	// _EDITUND2_HXX


