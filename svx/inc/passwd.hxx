/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: passwd.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_PASSWD_HXX
#define _SVX_PASSWD_HXX

// include ---------------------------------------------------------------

#include <sfx2/basedlgs.hxx>

#ifndef _FIXED_HXX //autogen
#include <vcl/fixed.hxx>
#endif

#ifndef _EDIT_HXX //autogen
#include <vcl/edit.hxx>
#endif

#ifndef _BUTTON_HXX //autogen
#include <vcl/button.hxx>
#endif
#include "svx/svxdllapi.h"

// class SvxPasswordDialog -----------------------------------------------

class SVX_DLLPUBLIC SvxPasswordDialog : public SfxModalDialog
{
private:
    FixedLine       aOldFL;
    FixedText		aOldPasswdFT;
    Edit			aOldPasswdED;
    FixedLine       aNewFL;
    FixedText		aNewPasswdFT;
    Edit			aNewPasswdED;
    FixedText		aRepeatPasswdFT;
    Edit			aRepeatPasswdED;
    OKButton		aOKBtn;
    CancelButton	aEscBtn;
    HelpButton		aHelpBtn;

    String			aOldPasswdErrStr;
    String			aRepeatPasswdErrStr;

    Link            aCheckPasswordHdl;

    BOOL		    bEmpty;

    DECL_LINK( ButtonHdl, OKButton * );
    DECL_LINK( EditModifyHdl, Edit * );

public:
                    SvxPasswordDialog( Window* pParent, BOOL bAllowEmptyPasswords = FALSE, BOOL bDisableOldPassword = FALSE );
                    ~SvxPasswordDialog();

    String			GetOldPassword() const { return aOldPasswdED.GetText(); }
    String			GetNewPassword() const { return aNewPasswdED.GetText(); }

    void            SetCheckPasswordHdl( const Link& rLink ) { aCheckPasswordHdl = rLink; }
};


#endif

