/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: optgenrl.hxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_OPTGENRL_HXX
#define _SVX_OPTGENRL_HXX

// Defines for setting the fokus of a Edit via a slot from external.
#define	UNKNOWN_EDIT	((USHORT)0)
#define COMPANY_EDIT	((USHORT)1)
#define FIRSTNAME_EDIT	((USHORT)2)
#define LASTNAME_EDIT	((USHORT)3)
#define STREET_EDIT		((USHORT)4)
#define COUNTRY_EDIT	((USHORT)5)
#define PLZ_EDIT		((USHORT)6)
#define CITY_EDIT		((USHORT)7)
#define STATE_EDIT		((USHORT)8)
#define TITLE_EDIT		((USHORT)9)
#define POSITION_EDIT	((USHORT)10)
#define SHORTNAME_EDIT	((USHORT)11)
#define TELPRIV_EDIT	((USHORT)12)
#define TELCOMPANY_EDIT	((USHORT)13)
#define FAX_EDIT		((USHORT)14)
#define EMAIL_EDIT		((USHORT)15)

#endif // #ifndef _SVX_OPTGENRL_HXX


