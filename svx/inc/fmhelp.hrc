/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: fmhelp.hrc,v $
 * $Revision: 1.17 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_FMHELP_HRC
#define _SVX_FMHELP_HRC

// include	   -----------------------------------------------------------
#include <svtools/solar.hrc>

// in solar.hrc
//#define HID_FORMS_START		 		(HID_LIB_START+4000)
//#define HID_FORMS_END					(HID_LIB_START+4999)

// Help-Ids --------------------------------------------------------------

// insgesamt 200
#define HID_DLG_DBINFO							(HID_FORMS_START +   0)
#define HID_DLG_DBMSG							(HID_FORMS_START +   1)

#define HID_FM_OTHER_START						(HID_FORMS_START + 300)
#define HID_FORM_NAVIGATOR						(HID_FM_OTHER_START +  0)
#define HID_FORM_NAVIGATOR_WIN					(HID_FM_OTHER_START +  1)
#define HID_FIELD_SEL							(HID_FM_OTHER_START +  2)
#define HID_FIELD_SEL_WIN						(HID_FM_OTHER_START +  3)
#define HID_FILTER_NAVIGATOR					(HID_FM_OTHER_START +  4)
#define HID_FILTER_NAVIGATOR_WIN				(HID_FM_OTHER_START +  5)

#define HID_FORM_DLG_START						(HID_FORMS_START + 400)
// FREE
// FREE
// FREE
#define HID_FM_PROPDLG_WINDOW					(HID_FORM_DLG_START +   3)
#define HID_FM_PROPDLG_CONTAINER				(HID_FORM_DLG_START +   4)
#define HID_FM_PROPDLG_TABCTR					(HID_FORM_DLG_START +   5)
#define HID_FM_PROPDLG_TAB_GENERAL				(HID_FORM_DLG_START +   6)
#define HID_FM_PROPDLG_TAB_DATA					(HID_FORM_DLG_START +   7)
#define HID_FM_PROPDLG_TAB_EVT					(HID_FORM_DLG_START +   8)
#define HID_FM_DLG_SEARCH						(HID_FORM_DLG_START +   9)

#define HID_SEARCH_TEXT							(HID_FORM_DLG_START +  10)
#define HID_SEARCH_ALLFIELDS					(HID_FORM_DLG_START +  11)
#define HID_SEARCH_SINGLEFIELD					(HID_FORM_DLG_START +  12)
#define HID_SEARCH_FIELDSELECTION				(HID_FORM_DLG_START +  13)
#define HID_SEARCH_POSITION						(HID_FORM_DLG_START +  14)
#define HID_SEARCH_FORMATTER					(HID_FORM_DLG_START +  15)
#define HID_SEARCH_CASE							(HID_FORM_DLG_START +  16)
#define HID_SEARCH_BACKWARD						(HID_FORM_DLG_START +  17)
#define HID_SEARCH_STARTOVER					(HID_FORM_DLG_START +  18)
#define HID_SEARCH_WILDCARD						(HID_FORM_DLG_START +  19)
#define HID_SEARCH_REGULAR						(HID_FORM_DLG_START +  20)
#define HID_SEARCH_APPROX						(HID_FORM_DLG_START +  21)
#define HID_SEARCH_APPROXSETTINGS				(HID_FORM_DLG_START +  22)
#define HID_SEARCH_BTN_SEARCH					(HID_FORM_DLG_START +  23)
#define HID_SEARCH_BTN_CLOSE					(HID_FORM_DLG_START +  24)

#define HID_TABORDER_CONTROLS					(HID_FORM_DLG_START +  25)

#define	HID_CONTROLS_DATE_N_TIME				(HID_FORM_DLG_START +  26)
#define HID_FM_DLG_PARAM						(HID_FORM_DLG_START +  27)

#define HID_GRID_TRAVEL_FIRST					(HID_FORM_DLG_START +  28)
#define HID_GRID_TRAVEL_PREV					(HID_FORM_DLG_START +  29)
#define HID_GRID_TRAVEL_NEXT					(HID_FORM_DLG_START +  30)
#define HID_GRID_TRAVEL_LAST					(HID_FORM_DLG_START +  31)
#define HID_GRID_TRAVEL_NEW						(HID_FORM_DLG_START +  32)
#define HID_GRID_TRAVEL_ABSOLUTE				(HID_FORM_DLG_START +  33)
#define HID_GRID_NUMBEROFRECORDS				(HID_FORM_DLG_START +  34)

#define UID_SEARCH_RECORDSTATUS					(HID_FORM_DLG_START +  35)

#define UID_FORMPROPBROWSER_FRAME				(HID_FORM_DLG_START +  37)
#define UID_ABSOLUTE_RECORD_WINDOW				(HID_FORM_DLG_START +  38)

#define HID_DATA_NAVIGATOR_WIN					(HID_FM_OTHER_START +  39)
#define HID_XFORMS_ADDDATAITEM_DLG				(HID_FM_OTHER_START +  40)
#define HID_XFORMS_ADDCONDITION_DLG				(HID_FM_OTHER_START +  41)
#define HID_XFORMS_NAMESPACEITEM_DLG			(HID_FM_OTHER_START +  42)
#define HID_XFORMS_NAMESPACEITEM_LIST			(HID_FM_OTHER_START +  43)
#define HID_XFORMS_MANAGENAMESPACE_DLG			(HID_FM_OTHER_START +  44)

#define HID_TP_XFORMS_INSTANCE                  (HID_FM_OTHER_START +  45)
#define HID_TP_XFORMS_SUBMISSION                (HID_FM_OTHER_START +  46)
#define HID_TP_XFORMS_BINDING	                (HID_FM_OTHER_START +  47)

#define HID_MN_XFORMS_MODELS_ADD				(HID_FM_OTHER_START +  48)
#define HID_MN_XFORMS_MODELS_EDIT				(HID_FM_OTHER_START +  49)
#define HID_MN_XFORMS_MODELS_REMOVE				(HID_FM_OTHER_START +  50)
#define HID_MN_XFORMS_INSTANCES_ADD	  			(HID_FM_OTHER_START +  51)
#define HID_MN_XFORMS_INSTANCES_EDIT  			(HID_FM_OTHER_START +  52)
#define HID_MN_XFORMS_INSTANCES_REMOVE			(HID_FM_OTHER_START +  53)
#define HID_MN_XFORMS_SHOW_DETAILS				(HID_FM_OTHER_START +  54)
#define HID_XFORMS_TOOLBOX						(HID_FM_OTHER_START +  55)
#define HID_XFORMS_TOOLBOX_ITEM_ADD				(HID_FM_OTHER_START +  56)
#define HID_XFORMS_TOOLBOX_ITEM_ADD_ELEMENT		(HID_FM_OTHER_START +  57)
#define HID_XFORMS_TOOLBOX_ITEM_ADD_ATTRIBUTE	(HID_FM_OTHER_START +  58)
#define HID_XFORMS_TOOLBOX_ITEM_EDIT			(HID_FM_OTHER_START +  59)
#define HID_XFORMS_TOOLBOX_ITEM_REMOVE			(HID_FM_OTHER_START +  60)
#define HID_XFORMS_ITEMS_LIST					(HID_FM_OTHER_START +  61)
#define HID_XFORMS_MODELS_LIST					(HID_FM_OTHER_START +  62)
#define HID_XFORMS_MODELS_MENUBTN				(HID_FM_OTHER_START +  63)
#define HID_XFORMS_INSTANCES_MENUBTN			(HID_FM_OTHER_START +  64)
#define HID_XFORMS_ADDSUBMISSION_DLG			(HID_FM_OTHER_START +  65)
#define HID_XFORMS_ADDMODEL_DLG					(HID_FM_OTHER_START +  66)
#define HID_XFORMS_ADDINSTANCE_DLG				(HID_FM_OTHER_START +  67)
#define HID_XFORMS_MID_INSERT_CONTROL           (HID_FM_OTHER_START +  68)
#define HID_XFORMS_TAB_CONTROL                  (HID_FM_OTHER_START +  69)

// if you add a new define here, please adjust the overflow check
// at the end of the file!!


// -----------------------------------------------------------------------
// "Uberlaufpr"ufung -----------------------------------------------------
// -----------------------------------------------------------------------

#define ACT_FM_HID_END      HID_XFORMS_TAB_CONTROL

#if ACT_FM_HID_END > HID_FORMS_END
#error Resource-Ueberlauf in #line, #file
#endif

#endif

