/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: xattr.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _XATTR_HXX
#define _XATTR_HXX

// include ---------------------------------------------------------------

#ifndef _XDEF_HXX
//#include <svx/xdef.hxx>
#endif
#ifndef _XENUM_HXX
//#include <svx/xenum.hxx>
#endif
#ifndef _XPOLY_HXX
//#include <svx/xpoly.hxx>
#endif
#ifndef _SVX_RECTENUM_HXX
//#include <svx/rectenum.hxx>
#endif

class XColorTable;
class XDashTable;
class XLineEndTable;
class XHatchTable;
class XBitmapTable;
class XGradientTable;

#include <svx/xit.hxx>
#include <svx/xcolit.hxx>
#include <svx/xgrad.hxx>
#include <svx/xhatch.hxx>
#include <svx/xlineit.hxx>
#include <svx/xfillit.hxx>
#include <svx/xtextit.hxx>
#include <svx/xlineit0.hxx>
#include <svx/xfillit0.hxx>
#include <svx/xtextit0.hxx>
#include <svx/xsetit.hxx>
#include <xlinjoit.hxx>


#endif      // _XATTR_HXX

