/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: protitem.hxx,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_PROTITEM_HXX
#define _SVX_PROTITEM_HXX

// include ---------------------------------------------------------------


#include <svtools/poolitem.hxx>
#include "svx/svxdllapi.h"

class SvXMLUnitConverter;
namespace rtl
{
    class OUString;
}

// class SvxProtectItem --------------------------------------------------


/*
[Beschreibung]
Dieses Item beschreibt, ob Inhalt, Groesse oder Position geschuetzt werden
sollen.
*/

class SVX_DLLPUBLIC SvxProtectItem : public SfxPoolItem
{
    BOOL bCntnt :1;     //Inhalt geschuetzt
    BOOL bSize  :1;     //Groesse geschuetzt
    BOOL bPos   :1;     //Position geschuetzt

public:
    TYPEINFO();

    inline SvxProtectItem( const USHORT nId  );
    inline SvxProtectItem &operator=( const SvxProtectItem &rCpy );

    // "pure virtual Methoden" vom SfxPoolItem
    virtual int 			 operator==( const SfxPoolItem& ) const;

    virtual SfxItemPresentation GetPresentation( SfxItemPresentation ePres,
                                    SfxMapUnit eCoreMetric,
                                    SfxMapUnit ePresMetric,
                                    String &rText, const IntlWrapper * = 0 ) const;


    virtual SfxPoolItem*     Clone( SfxItemPool *pPool = 0 ) const;
    virtual SfxPoolItem*	 Create(SvStream &, USHORT) const;
    virtual SvStream&		 Store(SvStream &, USHORT nItemVersion ) const;

    BOOL IsCntntProtected() const { return bCntnt; }
    BOOL IsSizeProtected()  const { return bSize;  }
    BOOL IsPosProtected()   const { return bPos;   }
    void SetCntntProtect( BOOL bNew ) { bCntnt = bNew; }
    void SetSizeProtect ( BOOL bNew ) { bSize  = bNew; }
    void SetPosProtect  ( BOOL bNew ) { bPos   = bNew; }

    virtual	sal_Bool        	 QueryValue( com::sun::star::uno::Any& rVal, BYTE nMemberId = 0 ) const;
    virtual	sal_Bool			 PutValue( const com::sun::star::uno::Any& rVal, BYTE nMemberId = 0 );
};

inline SvxProtectItem::SvxProtectItem( const USHORT nId )
    : SfxPoolItem( nId )
{
    bCntnt = bSize = bPos = FALSE;
}

inline SvxProtectItem &SvxProtectItem::operator=( const SvxProtectItem &rCpy )
{
    bCntnt = rCpy.IsCntntProtected();
    bSize  = rCpy.IsSizeProtected();
    bPos   = rCpy.IsPosProtected();
    return *this;
}




#endif

