/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svdshort.hxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#error svdshort wird nicht mehr verwendet!

#ifndef _SVDSHORT_HXX
#define _SVDSHORT_HXX

////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef Weg_Mit_Den_Doofen_Abkuerzungen
                                           // Statistik - Stand 02-03-1995
                                           //   Haeufigkeit  Ersparnis
#define SdrHelpLineKind              SdrFLK /* HL ist schon besetzt */
#define SdrHelpLineList              SdrFLL /* HL ist schon besetzt */
#define SdrHelpLine                  SdrFL  /* HL ist schon besetzt */
#define SdrObjTransformInfoRec       SdrTI
#define SdrDragCrook                 SdrDC
#define SdrDragMirror                SdrDI
#define SdrDragMovHdl                SdrDH
#define SdrDragResize                SdrDZ
#define SdrDragRotate                SdrDR
#define SdrDragShear                 SdrDE
#define SdrDragMove                  SdrDM
#define SdrCreateCmd                 SdrCC
#define SdrUndoAttrObj               SdrAU
#define SdrObjKind                   SdrOK
#define SdrUndoGroup                 SdrUG
#define SdrUndoAction                SdrUA
#define SdrAttrObj                   SdrAO
#define SdrGrafObj                   SdrGO
#define SdrMarkList                  SdrML
#define SdrHdlList                   SdrHL
#define SdrLayerAdmin                SdrLA
#define SdrObjUserCall               SdrUC
#define SdrObjUnknown                SdrUO
#define SdrExchangeView              SdrXV
#define SdrCreateView                SdrCV
#define SdrOle2Obj                   SdrOO
#define SdrObjGeoData                SdrGD
#define SdrDragView                  SdrDV
#define SdrSnapView                  SdrSV
#define SdrObjList                   SdrOL
#define SdrEdgeObj                   SdrEO
#define SdrCircObj                   SdrCO
#define SdrObjGroup                  SdrOG
#define SdrPage                      SdrPg
#define SdrObjEditView               SdrOV
#define SdrModel                     SdrMD
#define SdrEditView                  SdrEV
#define SdrPaintView                 SdrNV
#define SdrPolyObj                   SdrPO
#define SdrRectObj                   SdrRO
#define SdrTextObj                   SdrTO
#define SdrMarkView                  SdrMV
#define SdrPathObj                   SdrBO
#define SdrPageView                  SdrPV
#define SdrDragStat                  SdrDS
#define SdrVirtObj                   SdrVO
#define SdrObject                    SdrO

#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

#endif //_SVDSHORT_HXX

