/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: layctrl.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_LAYCTRL_HXX
#define _SVX_LAYCTRL_HXX

// include ---------------------------------------------------------------


#include <sfx2/tbxctrl.hxx>
#include "svx/svxdllapi.h"

// class SvxTableToolBoxControl ------------------------------------------

class SVX_DLLPUBLIC SvxTableToolBoxControl : public SfxToolBoxControl
{
private:
    FASTBOOL	bEnabled;

public:
    virtual SfxPopupWindowType	GetPopupWindowType() const;
    virtual SfxPopupWindow* 	CreatePopupWindow();
    virtual SfxPopupWindow* 	CreatePopupWindowCascading();
    virtual void				StateChanged( USHORT nSID,
                                              SfxItemState eState,
                                              const SfxPoolItem* pState );

    SFX_DECL_TOOLBOX_CONTROL();

    SvxTableToolBoxControl( USHORT nSlotId, USHORT nId, ToolBox& rTbx );
    ~SvxTableToolBoxControl();
};

// class SvxColumnsToolBoxControl ----------------------------------------

class SVX_DLLPUBLIC SvxColumnsToolBoxControl : public SfxToolBoxControl
{
    FASTBOOL	bEnabled;
public:
    virtual SfxPopupWindowType	GetPopupWindowType() const;
    virtual SfxPopupWindow* 	CreatePopupWindow();
    virtual SfxPopupWindow* 	CreatePopupWindowCascading();

    SFX_DECL_TOOLBOX_CONTROL();

    SvxColumnsToolBoxControl( USHORT nSlotId, USHORT nId, ToolBox& rTbx );
    ~SvxColumnsToolBoxControl();

    virtual void				StateChanged( USHORT nSID,
                                              SfxItemState eState,
                                              const SfxPoolItem* pState );
};


#endif

