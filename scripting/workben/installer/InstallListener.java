package installer;

public interface InstallListener
{
    public void installationComplete(InstallationEvent e);
}
