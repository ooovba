/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: doctokutil.cxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <util.hxx>

using namespace ::std;

namespace writerfilter {
namespace doctok {
void util_assert(bool bTest)
{
    if (! bTest)
        clog << "ASSERT!\n" << endl;
}

void printBytes(ostream & o, const string & str)
{
    unsigned int nCount = str.size();
    for (unsigned int n = 0; n < nCount; ++n)
    {
        unsigned char c = static_cast<unsigned char>(str[n]);
        if (c < 128 && isprint(c))
            o << str[n];
        else
            o << ".";
    }
}

}}
