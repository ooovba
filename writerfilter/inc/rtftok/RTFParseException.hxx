/**
  Copyright 2005 Sun Microsystems, Inc.
*/

#ifndef INCLUDED_RTFPARSEEXCEPTION_HXX
#define INCLUDED_RTFPARSEEXCEPTION_HXX

namespace writerfilter { namespace rtftok {

class RTFParseException
{
public:
  RTFParseException(char *message);
};

} } /* end namespace writerfilter::rtftok */


#endif /* INCLUDED_RTFPARSEEXCEPTION_HXX  */
