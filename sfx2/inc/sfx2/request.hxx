/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: request.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SFXREQUEST_HXX
#define _SFXREQUEST_HXX

#include "sal/config.h"
#include "sfx2/dllapi.h"
#include "sal/types.h"
#include <svtools/itemset.hxx>
#include <svtools/hint.hxx>

#include <com/sun/star/uno/Sequence.hxx>
#include <com/sun/star/beans/PropertyValue.hpp>
#include <com/sun/star/frame/XDispatchRecorder.hpp>

class SfxMacro;
class SfxPoolItem;
class SfxAllItemSet;
class SfxItemSet;
class SfxItemPool;
class SfxShell;
class SfxSlot;
class SfxMacroStatement;
class SfxArguments;
class String;
class SfxViewFrame;
struct SfxRequest_Impl;

//==================================================================

class SFX2_DLLPUBLIC SfxRequest: public SfxHint
{
friend struct SfxRequest_Impl;

    USHORT				nSlot;
    SfxAllItemSet*		pArgs;
    SfxRequest_Impl*	pImp;

    //---------------------------------------------------------------------
//#if 0 // _SOLAR__PRIVATE
public:
    SAL_DLLPRIVATE void Record_Impl( SfxShell &rSh, const SfxSlot &rSlot,
                                     com::sun::star::uno::Reference< com::sun::star::frame::XDispatchRecorder > xRecorder,
                                     SfxViewFrame* );
private:
    SAL_DLLPRIVATE void Done_Impl( const SfxItemSet *pSet );

//#endif
    //---------------------------------------------------------------------

public:
                        SfxRequest( SfxViewFrame*, USHORT nSlotId );
                        SfxRequest( USHORT nSlot, USHORT nCallMode, SfxItemPool &rPool );
                        SfxRequest( const SfxSlot* pSlot, const com::sun::star::uno::Sequence < com::sun::star::beans::PropertyValue >& rArgs,
                                            USHORT nCallMode, SfxItemPool &rPool );
                        SfxRequest( USHORT nSlot, USHORT nCallMode, const SfxAllItemSet& rSfxArgs );
                        SfxRequest( const SfxRequest& rOrig );
                        ~SfxRequest();

    USHORT				GetSlot() const { return nSlot; }
    void				SetSlot(USHORT nNewSlot) { nSlot = nNewSlot; }

    USHORT              GetModifier() const;
    void                SetModifier( USHORT nModi );
    SAL_DLLPRIVATE void SetInternalArgs_Impl( const SfxAllItemSet& rArgs );
    SAL_DLLPRIVATE const SfxItemSet* GetInternalArgs_Impl() const;
    const SfxItemSet* 	GetArgs() const { return pArgs; }
    void				SetArgs( const SfxAllItemSet& rArgs );
    void				AppendItem(const SfxPoolItem &);
    void				RemoveItem( USHORT nSlotId );

    static const SfxPoolItem* GetItem( const SfxItemSet*, USHORT nSlotId,
                                       bool bDeep = false,
                                       TypeId aType = 0 );
    const SfxPoolItem*  GetArg( USHORT nSlotId, FASTBOOL bDeep = FALSE, TypeId aType = 0 ) const;
    void                ReleaseArgs();
    void				SetReturnValue(const SfxPoolItem &);
    const SfxPoolItem*	GetReturnValue() const;

    static SfxMacro*	GetRecordingMacro();
    static com::sun::star::uno::Reference< com::sun::star::frame::XDispatchRecorder > GetMacroRecorder( SfxViewFrame* pFrame=NULL );
    static BOOL         HasMacroRecorder( SfxViewFrame* pFrame=NULL );
    USHORT  			GetCallMode() const;
    FASTBOOL			IsRecording() const;
    void                AllowRecording( BOOL );
    BOOL                AllowsRecording() const;
    BOOL				IsAPI() const;
    BOOL                IsSynchronCall() const;
    void                SetSynchronCall( BOOL bSynchron );
    void				SetTarget( const String &rTarget );

    BOOL				IsDone() const;
    void				Done( BOOL bRemove = FALSE );

    void				Ignore();
    void				Cancel();
    BOOL				IsCancelled() const;
    void				Done(const SfxItemSet &, FASTBOOL bKeep = TRUE );

    void                ForgetAllArgs();

private:
    const SfxRequest&	operator=(const SfxRequest &); // n.i.!!
};

//------------------------------------------------------------------------

#define SFX_REQUEST_ARG(rReq, pItem, ItemType, nSlotId, bDeep) \
        const ItemType *pItem = (const ItemType*) \
                rReq.GetArg( nSlotId, bDeep, TYPE(ItemType) )
#define SFX_ITEMSET_ARG(pArgs, pItem, ItemType, nSlotId, bDeep) \
    const ItemType *pItem = (const ItemType*) \
        SfxRequest::GetItem( pArgs, nSlotId, bDeep, TYPE(ItemType) )

#endif
