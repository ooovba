/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: newstyle.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _NEWSTYLE_HXX
#define _NEWSTYLE_HXX

#include "sal/config.h"
#include "sfx2/dllapi.h"

#ifndef _SV_BUTTON_HXX
#include <vcl/button.hxx>
#endif
#include <vcl/msgbox.hxx>
#include <vcl/combobox.hxx>
#include <vcl/dialog.hxx>
#include <vcl/fixed.hxx>

class SfxStyleSheetBasePool;

class SFX2_DLLPUBLIC SfxNewStyleDlg : public ModalDialog
{
private:
    FixedLine				aColFL;
    ComboBox				aColBox;
    OKButton				aOKBtn;
    CancelButton			aCancelBtn;

    QueryBox				aQueryOverwriteBox;
    SfxStyleSheetBasePool&	rPool;

//#if 0 // _SOLAR__PRIVATE
    DECL_DLLPRIVATE_LINK( OKHdl, Control * );
    DECL_DLLPRIVATE_LINK( ModifyHdl, ComboBox * );
//#endif

public:
    SfxNewStyleDlg( Window* pParent, SfxStyleSheetBasePool& );
    ~SfxNewStyleDlg();

    String 					GetName() const { return aColBox.GetText().EraseLeadingChars(); }
};

#endif

