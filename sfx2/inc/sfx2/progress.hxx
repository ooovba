/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: progress.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SFX_PROGRESS_HXX
#define _SFX_PROGRESS_HXX

#include "sal/config.h"
#include "sfx2/dllapi.h"
#include "sal/types.h"
#include <tools/solar.h>
#include <tools/link.hxx>

class String;
class SfxObjectShell;
class SfxStatusBarManager;
class INetHint;
class SvDispatch;
struct SfxProgress_Impl;
struct PlugInLoadStatus;
struct SvProgressArg;

//=======================================================================

class SFX2_DLLPUBLIC SfxProgress
{
    SfxProgress_Impl*		pImp;
    ULONG					nVal;
    BOOL					bSuspended;

public:
                            SfxProgress( SfxObjectShell* pObjSh,
                                         const String& rText,
                                         ULONG nRange, BOOL bAllDocs = FALSE,
                                         BOOL bWait = TRUE );
    virtual 				~SfxProgress();

    virtual void			SetText( const String& rText );
    BOOL					SetStateText( ULONG nVal, const String &rVal, ULONG nNewRange = 0 );
    virtual BOOL			SetState( ULONG nVal, ULONG nNewRange = 0 );
    ULONG					GetState() const { return nVal; }

    void					Resume();
    void					Suspend();
    BOOL					IsSuspended() const { return bSuspended; }

    void					Lock();
    void					UnLock();
    void					Reschedule();

    void					Stop();

    void					SetWaitMode( BOOL bWait );
    BOOL					GetWaitMode() const;

    static SfxProgress* 	GetActiveProgress( SfxObjectShell *pDocSh = 0 );
    static void				EnterLock();
    static void				LeaveLock();

//#if 0 // _SOLAR__PRIVATE
    DECL_DLLPRIVATE_STATIC_LINK( SfxProgress, SetStateHdl, PlugInLoadStatus* );
    DECL_DLLPRIVATE_STATIC_LINK( SfxProgress, DefaultBindingProgress, SvProgressArg* );
    SAL_DLLPRIVATE FASTBOOL StatusBarManagerGone_Impl(SfxStatusBarManager*pStb);
    SAL_DLLPRIVATE const String& GetStateText_Impl() const;
//#endif
};

#endif

