/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tabdlg.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _LAYOUT_SFX_TABDLG_HXX
#define _LAYOUT_SFX_TABDLG_HXX

#undef ENABLE_LAYOUT_SFX_TABDIALOG
#define ENABLE_LAYOUT_SFX_TABDIALOG 1

#undef NAMESPACE_LAYOUT_SFX_TABDIALOG
#define NAMESPACE_LAYOUT_SFX_TABDIALOG namespace layout {

#undef END_NAMESPACE_LAYOUT_SFX_TABDIALOG
#define END_NAMESPACE_LAYOUT_SFX_TABDIALOG } //end namespace layout

#undef LAYOUT_NS_SFX_TABDIALOG
#define LAYOUT_NS_SFX_TABDIALOG layout::

#undef _SFXTABDLG_HXX
#include <sfx2/tabdlg.hxx>

#endif /* _LAYOUT_SFX_TABDLG_HXX */

