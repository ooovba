/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tplpitem.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SFX_TPLPITEM_HXX
#define _SFX_TPLPITEM_HXX

#include "sal/config.h"
#include "sfx2/dllapi.h"
#include <tools/string.hxx>
#include <tools/rtti.hxx>
#include <svtools/flagitem.hxx>

class SFX2_DLLPUBLIC SfxTemplateItem: public SfxFlagItem
{
    String aStyle;
public:
    TYPEINFO();
    SfxTemplateItem();
    SfxTemplateItem( USHORT nWhich,
                     const String &rStyle,
                     USHORT nMask = 0xffff );
    SfxTemplateItem( const SfxTemplateItem& );

    const String&			GetStyleName() const { return aStyle; }

    virtual SfxPoolItem*	Clone( SfxItemPool *pPool = 0 ) const;
    virtual int 			operator==( const SfxPoolItem& ) const;
    virtual BYTE 			GetFlagCount() const;
    virtual	sal_Bool        QueryValue( com::sun::star::uno::Any& rVal, BYTE nMemberId = 0 ) const;
    virtual	sal_Bool		PutValue( const com::sun::star::uno::Any& rVal, BYTE nMemberId = 0 );
};

#endif
