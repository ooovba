/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: view.src,v $
 * $Revision: 1.55 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 // include ---------------------------------------------------------------
#include "view.hrc"
#include <sfx2/sfx.hrc>
#include "helpid.hrc"
#include "sfxlocal.hrc"

 // Strings ---------------------------------------------------------------
String STR_NODEFPRINTER
{
    Text [ en-US ] = "No default printer found.\nPlease choose a printer and try again." ;
};
String STR_NOSTARTPRINTER
{
    Text [ en-US ] = "Could not start printer.\nPlease check your printer configuration." ;
};
String STR_PRINTER_NOTAVAIL
{
    Text [ en-US ] = "This document has been formatted for the printer $1. The specified printer is not available. \nDo you want to use the standard printer $2 ?" ;
};
String STR_PRINT_OPTIONS
{
    Text [ en-US ] = "Options..." ;
};
String STR_PRINT_OPTIONS_TITLE
{
    Text [ en-US ] = "Printer Options" ;
};
String STR_ERROR_PRINTER_BUSY
{
    Text [ en-US ] = "Printer busy" ;
};
String STR_ERROR_PRINT
{
    Text [ en-US ] = "Error while printing" ;
};
String STR_PRINTING
{
    Text [ en-US ] = "Printing" ;
};
String STR_PAGE
{
    Text [ en-US ] = "Page " ;
};
String STR_ERROR_SAVE_TEMPLATE
{
    Text [ en-US ] = "Error saving template " ;
};
String STR_READONLY
{
    Text [ en-US ] = " (read-only)" ;
};
String STR_PRINT_NEWORI
{
    Text [ en-US ] = "The page size and orientation have been modified.\nWould you like to save the new settings in the\nactive document?" ;
};
String STR_PRINT_NEWSIZE
{
    Text [ en-US ] = "The page size has been modified.\nShould the new settings be saved\nin the active document?" ;
};
String STR_PRINT_NEWORISIZE
{
    Text [ en-US ] = "The page size and orientation have been modified.\nWould you like to save the new settings in the\nactive document?" ;
};
String STR_PREVIEW_DOCINFO
{
    Text [ en-US ] = "<html><body BGCOLOR=\"#c0c0c0\"><font FACE=\"Arial\"><dl><dt><b>Title:</b><dd>$(TITEL)<dt><b>Subject:</b><dd>$(THEME)<dt><b>Keywords:</b><dd>$(KEYWORDS)<dt><b>Description:</b><dd>$(TEXT)</dl></font></body></html>" ;
};
String STR_PREVIEW_NODOCINFO
{
    Text [ en-US ] = "<HTML><BODY BGCOLOR=\"#c0c0c0\"><BR><BR><P><FONT FACE=\"Arial\"><B>No document properties found.</B></FONT></P></BODY></HTML>" ;
};
 // -----------------------------------------------------------------------
InfoBox MSG_CANT_CLOSE
{
    Message [ en-US ] = "The document cannot be closed because a\n print job is being carried out." ;
};
 // DLG_PRINTMONITOR ------------------------------------------------------

#define	DLG_PRINTMONITOR_TEXT \
    Text [ en-US ] = "Print Monitor" ; \

ModelessDialog DLG_PRINTMONITOR
{
    HelpID = HID_PRINTMONITOR ;
    OutputSize = TRUE ;
    SVLook = TRUE ;
    Hide = TRUE ;
    Moveable = TRUE;
    Size = MAP_APPFONT ( 112 , 81 ) ;
    DLG_PRINTMONITOR_TEXT
    FixedText FT_DOCNAME
    {
        Pos = MAP_APPFONT ( 6 , 6 ) ;
        Size = MAP_APPFONT ( 100 , 10 ) ;
        Center = TRUE ;
    };
    FixedText FT_PRINTING
    {
        Pos = MAP_APPFONT ( 6 , 19 ) ;
        Size = MAP_APPFONT ( 100 , 10 ) ;
        Center = TRUE ;
        Text [ en-US ] = "is being printed on" ;
    };
    FixedText FT_PRINTER
    {
        Pos = MAP_APPFONT ( 6 , 32 ) ;
        Size = MAP_APPFONT ( 100 , 10 ) ;
        Center = TRUE ;
    };
    FixedText FT_PRINTINFO
    {
        Pos = MAP_APPFONT ( 6 , 45 ) ;
        Size = MAP_APPFONT ( 100 , 10 ) ;
        Center = TRUE ;
    };
    CancelButton PB_CANCELPRNMON
    {
        Pos = MAP_APPFONT ( 31 , 61 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    String STR_FT_PREPARATION
    {
        Text [ en-US ] = "is being prepared for printing";
    };
};

 // MSG_ERROR_SEND_MAIL ---------------------------------------------------

InfoBox MSG_ERROR_SEND_MAIL
{
    BUTTONS = WB_OK ;
    DEFBUTTON = WB_DEF_OK ;
    Message [ en-US ] = "An error occurred in sending the message. Possible errors could be a missing user account or a defective setup.\nPlease check the %PRODUCTNAME settings or your e-mail program settings." ;
};

 // QueryBoxen ------------------------------------------------------------
QueryBox MSG_QUERY_OPENASTEMPLATE
{
    Buttons = WB_YES_NO ;
    DefButton = WB_DEF_NO ;
    Message [ en-US ] = "This document cannot be edited, possibly due to missing access rights. Do you want to edit a copy of the document?" ;
};
String STR_REPAIREDDOCUMENT
{
    Text [ en-US ] = " (repaired document)" ;
};

ErrorBox MSG_ERROR_NO_WEBBROWSER_FOUND
{
    BUTTONS = WB_OK ;
    DEFBUTTON = WB_DEF_OK ;
    Message[ en-US ] = "%PRODUCTNAME could not find a web browser on your system. Please check your Desktop Preferences or install a web browser (for example, Mozilla) in the default  location requested during the browser installation." ;
};











