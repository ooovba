/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: xmloff_numithdl.cxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_PROPERTYHANDLER_NUMRULE_HXX
#include <numithdl.hxx>
#endif
namespace binfilter {

using namespace ::com::sun::star;

///////////////////////////////////////////////////////////////////////////////
//
// class XMLNumRulePropHdl
//

XMLNumRulePropHdl::XMLNumRulePropHdl( ::com::sun::star::uno::Reference< ::com::sun::star::ucb::XAnyCompare > xNumRuleCompare )
: mxNumRuleCompare( xNumRuleCompare )
{
}

XMLNumRulePropHdl::~XMLNumRulePropHdl()
{
    // Nothing to do
}

sal_Bool XMLNumRulePropHdl::equals( const uno::Any& r1, const uno::Any& r2 ) const
{
    return mxNumRuleCompare.is() && mxNumRuleCompare->compare( r1, r2 ) == 0;
}

sal_Bool XMLNumRulePropHdl::importXML( const ::rtl::OUString& rStrImpValue, ::com::sun::star::uno::Any& rValue, const SvXMLUnitConverter& rUnitConverter ) const
{ 
    return sal_False; 
}

sal_Bool XMLNumRulePropHdl::exportXML( ::rtl::OUString& rStrExpValue, const ::com::sun::star::uno::Any& rValue, const SvXMLUnitConverter& rUnitConverter ) const
{ 
    return sal_False;
}

}//end of namespace binfilter
