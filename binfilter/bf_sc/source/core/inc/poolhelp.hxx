/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: poolhelp.hxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_POOLHELP_HXX
#define SC_POOLHELP_HXX

#ifndef _VOS_REFERNCE_HXX_
#include <vos/refernce.hxx>
#endif

namespace binfilter {

class SvNumberFormatter;
class SfxItemPool;
class ScDocument;
class ScDocumentPool;
class ScStyleSheetPool;


class ScPoolHelper : public vos::OReference
{
private:
    ScDocumentPool*		pDocPool;
    ScStyleSheetPool*	pStylePool;
    SvNumberFormatter*	pFormTable;
    SfxItemPool*		pEditPool;						// EditTextObjectPool
    SfxItemPool*		pEnginePool;					// EditEnginePool

public:
                ScPoolHelper( ScDocument* pSourceDoc );
    virtual		~ScPoolHelper();

                // called in dtor of main document
    void		SourceDocumentGone();

                // access to pointers (are never 0):
    ScDocumentPool*		GetDocPool() const		{ return pDocPool; }
    ScStyleSheetPool*	GetStylePool() const	{ return pStylePool; }
    SvNumberFormatter*	GetFormTable() const	{ return pFormTable; }
    SfxItemPool*		GetEditPool() const		{ return pEditPool; }
    SfxItemPool*		GetEnginePool() const	{ return pEnginePool; }
};

} //namespace binfilter
#endif

