/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: itabenum.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _ITABENUM_HXX
#define _ITABENUM_HXX
namespace binfilter {

enum SwTableInsMode
{
    DEFAULT_BORDER     = 0x01,
    HEADLINE           = 0x02,
    REPEAT             = 0x04,
    HEADLINE_REPEAT    = 0x06,	// Headline + Repeat
    SPLIT_LAYOUT       = 0x08,
    HEADLINE_NO_BORDER = HEADLINE_REPEAT|SPLIT_LAYOUT,
    ALL_TBL_INS_ATTR   = DEFAULT_BORDER|HEADLINE_REPEAT|SPLIT_LAYOUT
};


} //namespace binfilter
#endif

