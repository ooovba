/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svdattr.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVDATTR_HXX
#define _SVDATTR_HXX

#ifndef _SOLAR_HRC
#include <bf_svtools/solar.hrc>
#endif

#ifndef _SOLAR_H
#include <tools/solar.h>
#endif

#ifndef _SDANGITM_HXX
#include <bf_svx/sdangitm.hxx>
#endif
#ifndef _SDERITM_HXX
#include <bf_svx/sderitm.hxx>
#endif
#ifndef _SDMETITM_HXX
#include <bf_svx/sdmetitm.hxx>
#endif
#ifndef _SDMSITM_HXX
#include <bf_svx/sdmsitm.hxx>
#endif
#ifndef _SDOLSITM_HXX
#include <bf_svx/sdolsitm.hxx>
#endif
#ifndef _SDOOITM_HXX
#include <bf_svx/sdooitm.hxx>
#endif
#ifndef _SDPRCITM_HXX
#include <bf_svx/sdprcitm.hxx>
#endif
#ifndef _SDSHCITM_HXX
#include <bf_svx/sdshcitm.hxx>
#endif
#ifndef _SDSHITM_HXX
#include <bf_svx/sdshitm.hxx>
#endif
#ifndef _SDSHSITM_HXX
#include <bf_svx/sdshsitm.hxx>
#endif
#ifndef _SDSHTITM_HXX
#include <bf_svx/sdshtitm.hxx>
#endif
#ifndef _SDSXYITM_HXX
#include <bf_svx/sdsxyitm.hxx>
#endif
#ifndef _SDTAAITM_HXX
#include <bf_svx/sdtaaitm.hxx>
#endif
#ifndef _SDTACITM_HXX
#include <bf_svx/sdtacitm.hxx>
#endif
#ifndef _SDTACITM_HXX
#include <bf_svx/sdtaditm.hxx>
#endif
#ifndef _SDTAGITM_HXX
#include <bf_svx/sdtagitm.hxx>
#endif
#ifndef _SDTAIITM_HXX
#include <bf_svx/sdtaiitm.hxx>
#endif
#ifndef _SDTAITM_HXX
#include <bf_svx/sdtaitm.hxx>
#endif
#ifndef _SDTAKITM_HXX
#include <bf_svx/sdtakitm.hxx>
#endif
#ifndef _SDTAYITM_HXX
#include <bf_svx/sdtayitm.hxx>
#endif
#ifndef SDTCFITM_HXX
#include <bf_svx/sdtcfitm.hxx>
#endif
#ifndef _SDTDITM_HXX
#include <bf_svx/sdtditm.hxx>
#endif
#ifndef _SDTFSITM_HXX
#include <bf_svx/sdtfsitm.hxx>
#endif
#ifndef _SDTMFITM_HXX
#include <bf_svx/sdtmfitm.hxx>
#endif
#ifndef _SDYNITM_HXX
#include <bf_svx/sdynitm.hxx>
#endif
#ifndef _SDGLUITM_HXX
#include <bf_svx/sdgluitm.hxx>
#endif
#ifndef _SDGINITM_HXX
#include <bf_svx/sdginitm.hxx>
#endif
#ifndef _SDGTRITM_HXX
#include <bf_svx/sdgtritm.hxx>
#endif
#ifndef _SDGCOITM_HXX
#include <bf_svx/sdgcoitm.hxx>
#endif
#ifndef _SDGGAITM_HXX
#include <bf_svx/sdggaitm.hxx>
#endif
#ifndef _SDGRSITM_HXX
#include <bf_svx/sdgrsitm.hxx>
#endif
#ifndef _SDGMOITM_HXX
#include <bf_svx/sdgmoitm.hxx>
#endif
#ifndef _SDASAITM_HXX
#include <bf_svx/sdasaitm.hxx>
#endif
#endif

