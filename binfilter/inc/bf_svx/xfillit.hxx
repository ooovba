/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: xfillit.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVX_FILLITEM_HXX
#define _SVX_FILLITEM_HXX

#ifndef _BITMAP_HXX //autogen
#include <vcl/bitmap.hxx>
#endif
#include <bf_svx/xcolit.hxx>
#include <bf_svx/xgrad.hxx>
#include <bf_svx/xhatch.hxx>

#ifndef _XENUM_HXX //autogen
#include <bf_svx/xenum.hxx>
#endif
#ifndef _SFXENUMITEM_HXX //autogen
#include <bf_svtools/eitem.hxx>
#endif

#include <bf_svx/xflclit.hxx>
#include <bf_svx/xflgrit.hxx>
#include <bf_svx/xflhtit.hxx>
#include <bf_svx/xbitmap.hxx>
#include <bf_svx/xbtmpit.hxx>
#include <bf_svx/xflftrit.hxx>
namespace binfilter {

class XPolygon;
class XGradient;
class XOBitmap;

}//end of namespace binfilter
#endif
