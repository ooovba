/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: xlineit.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVX_XLINIIT_HXX
#define _SVX_XLINIIT_HXX

#include <bf_svx/xit.hxx>
#include <bf_svx/xcolit.hxx>

#ifndef _XPOLY_HXX //autogen
#include <bf_svx/xpoly.hxx>
#endif
#ifndef _XENUM_HXX //autogen
#include <bf_svx/xenum.hxx>
#endif

#ifndef _SFXMETRICITEM_HXX //autogen
#include <bf_svtools/metitem.hxx>
#endif

#ifndef _SFXENUMITEM_HXX //autogen
#include <bf_svtools/eitem.hxx>
#endif

class SvStream; 

namespace binfilter {
class XDash;
class XDashTable;
}//end of namespace binfilter

#include <bf_svx/xdash.hxx>
#include <bf_svx/xlndsit.hxx>
#include <bf_svx/xlnwtit.hxx>
#include <bf_svx/xlnclit.hxx>
#include <bf_svx/xlnstit.hxx>
#include <bf_svx/xlnedit.hxx>
#include <bf_svx/xlnstwit.hxx>
#include <bf_svx/xlnedwit.hxx>
#include <bf_svx/xlnstcit.hxx>
#include <bf_svx/xlnedcit.hxx>

#endif

