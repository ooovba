/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: xattr.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _XATTR_HXX
#define _XATTR_HXX
namespace binfilter {

// include ---------------------------------------------------------------

class XColorTable;
class XDashTable;
class XLineEndTable;
class XHatchTable;
class XBitmapTable;
class XGradientTable;
}//end of namespace binfilter

#include <bf_svx/xit.hxx>
#include <bf_svx/xcolit.hxx>
#include <bf_svx/xgrad.hxx>
#include <bf_svx/xhatch.hxx>
#include <bf_svx/xlineit.hxx>
#include <bf_svx/xfillit.hxx>
#include <bf_svx/xlineit0.hxx>
#include <bf_svx/xfillit0.hxx>
#include <bf_svx/xtextit0.hxx>
#include <bf_svx/xlinjoit.hxx>

#endif      // _XATTR_HXX

