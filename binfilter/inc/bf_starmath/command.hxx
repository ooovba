/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: command.hxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef MATH_COMMAND_HXX
#define MATH_COMMAND_HXX



#ifndef _BITMAP_HXX //autogen
#include <vcl/bitmap.hxx>
#endif
class String; 
namespace binfilter {



class SmCommandDesc: public Resource
{
protected:
    String		*pSample;
    String		*pCommand;
    String		*pText;
    String		*pHelp;
    Bitmap		*pGraphic;

public:
    SmCommandDesc(const ResId& rResId);
    ~SmCommandDesc();

    const String  &GetSample()	const { return (*pSample);	}
    const String  &GetCommand() const { return (*pCommand); }
    const String  &GetText()  	const { return (*pText); }
    const String  &GetHelp()  	const { return (*pHelp); }
    const Bitmap  &GetGraphic() const { return (*pGraphic); }
};


} //namespace binfilter
#endif

