/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: plugobj.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _PLUGOBJ_HXX
#define _PLUGOBJ_HXX

#ifndef _PLUGIN_HXX //autogen
#include <bf_so3/plugin.hxx>
#endif
#include "bf_sfx2/app.hxx"

namespace binfilter {

//=========================================================================
struct SfxPluginObject_Impl;
class SfxFrameDescriptor;


struct SfxPluginObjectFactoryPtr
{
    // Ist n"otig, da im SO2_DECL_BASIC_CLASS_DLL-Macro ein Pointer auf
    // eine exportierbare struct/class "ubergeben werden mu\s
    SotFactory *pSfxPluginObjectFactory;
    SfxPluginObjectFactoryPtr();
};

class SfxPluginObject : public SvPlugInObject
/*	[Beschreibung]

*/
{
    SfxPluginObject_Impl*	pImpl;

#if _SOLAR__PRIVATE
    DECL_STATIC_LINK( SfxPluginObject, MIMEAvailable_Impl, String* );
#endif

protected:
    virtual void    		FillClass( SvGlobalName * pClassName,
                               ULONG * pFormat,
                               String * pAppName,
                               String * pFullTypeName,
                               String * pShortTypeName ) const;

                            // Protokoll
    virtual void    		InPlaceActivate( BOOL );

                            ~SfxPluginObject();
public:

    static SfxPluginObjectFactoryPtr*
                            GetFactoryPtr();
                            SfxPluginObject();

                            SO2_DECL_BASIC_CLASS_DLL(SfxPluginObject, GetFactoryPtr())
};

inline SfxPluginObjectFactoryPtr::SfxPluginObjectFactoryPtr()
    : pSfxPluginObjectFactory(0) 	// sonst funzt ClassFactory() nicht!
{
}

SO2_DECL_IMPL_REF(SfxPluginObject)


}//end of namespace binfilter
#endif
