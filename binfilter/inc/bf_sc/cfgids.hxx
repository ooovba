/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cfgids.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CFGID_HXX
#define _CFGID_HXX

#define SCCFG_DOC               SFX_ITEMTYPE_SC_BEGIN
#define SCCFG_VIEW              (SFX_ITEMTYPE_SC_BEGIN + 1)
#define SCCFG_APP               (SFX_ITEMTYPE_SC_BEGIN + 2)
#define SCCFG_SPELLCHECK        (SFX_ITEMTYPE_SC_BEGIN + 3)
#define SCCFG_PRINT             (SFX_ITEMTYPE_SC_BEGIN + 4)
#define SCCFG_STATUSBAR			(SFX_ITEMTYPE_SC_BEGIN + 5)
#define SCCFG_ACCELERATOR		(SFX_ITEMTYPE_SC_BEGIN + 6)
#define SCCFG_MENUBAR			(SFX_ITEMTYPE_SC_BEGIN + 7)
#define SCCFG_INPUT				(SFX_ITEMTYPE_SC_BEGIN + 8)
#define SCCFG_NAVIPI			(SFX_ITEMTYPE_SC_BEGIN + 9)
#define SCCFG_PLUGINMENU		(SFX_ITEMTYPE_SC_BEGIN + 10)


#endif

