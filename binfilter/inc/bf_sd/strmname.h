/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: strmname.h,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SD_STRMNAME_H
#define _SD_STRMNAME_H

// Alter Name des Dokument-Streams
static const String pStarDrawDoc( RTL_CONSTASCII_USTRINGPARAM( "StarDrawDocument" ));

// Name des Dokument-Streams
static const String pStarDrawDoc3( RTL_CONSTASCII_USTRINGPARAM( "StarDrawDocument3" ));

// Sonstige
static const String pSfxStyleSheets( RTL_CONSTASCII_USTRINGPARAM( "SfxStyleSheets" ));
static const String pVCItemPoolName( RTL_CONSTASCII_USTRINGPARAM( "VCPool" ));
static const String pPreviewName( RTL_CONSTASCII_USTRINGPARAM( "StarDrawTemplatePreview" ));

// PowerPoint-Filter
static const String pFilterPowerPoint97( RTL_CONSTASCII_USTRINGPARAM( "MS PowerPoint 97" ));
static const String pFilterPowerPoint97Template( RTL_CONSTASCII_USTRINGPARAM( "MS PowerPoint 97 Vorlage" ));

// XML content stream
static const String pStarDrawXMLContent( RTL_CONSTASCII_USTRINGPARAM( "content.xml" ));
static const String pStarDrawOldXMLContent( RTL_CONSTASCII_USTRINGPARAM( "Content.xml" ));

#endif  // _SD_STRMNAME_H
