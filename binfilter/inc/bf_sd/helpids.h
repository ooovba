/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: helpids.h,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SOLAR_HRC
#include <bf_svtools/solar.hrc>
#endif

#define HID_STANDARD_STYLESHEET_NAME		(HID_SD_START + 70)
#define HID_POOLSHEET_OBJWITHARROW          (HID_SD_START + 71)
#define HID_POOLSHEET_OBJWITHSHADOW         (HID_SD_START + 72)
#define HID_POOLSHEET_OBJWITHOUTFILL        (HID_SD_START + 73)
#define HID_POOLSHEET_TEXT                  (HID_SD_START + 74)
#define HID_POOLSHEET_TEXTBODY              (HID_SD_START + 75)
#define HID_POOLSHEET_TEXTBODY_JUSTIFY      (HID_SD_START + 76)
#define HID_POOLSHEET_TEXTBODY_INDENT       (HID_SD_START + 77)
#define HID_POOLSHEET_TITLE                 (HID_SD_START + 78)
#define HID_POOLSHEET_TITLE1                (HID_SD_START + 79)
#define HID_POOLSHEET_TITLE2                (HID_SD_START + 80)
#define HID_POOLSHEET_HEADLINE              (HID_SD_START + 81)
#define HID_POOLSHEET_HEADLINE1             (HID_SD_START + 82)
#define HID_POOLSHEET_HEADLINE2             (HID_SD_START + 83)
#define HID_POOLSHEET_MEASURE               (HID_SD_START + 84)

#define HID_PSEUDOSHEET_TITLE               (HID_SD_START + 85)
#define HID_PSEUDOSHEET_OUTLINE             (HID_SD_START + 86)
#define HID_PSEUDOSHEET_OUTLINE1            (HID_SD_START + 87)
#define HID_PSEUDOSHEET_OUTLINE2            (HID_SD_START + 88)
#define HID_PSEUDOSHEET_OUTLINE3            (HID_SD_START + 89)
#define HID_PSEUDOSHEET_OUTLINE4            (HID_SD_START + 90)
#define HID_PSEUDOSHEET_OUTLINE5            (HID_SD_START + 91)
#define HID_PSEUDOSHEET_OUTLINE6            (HID_SD_START + 92)
#define HID_PSEUDOSHEET_OUTLINE7            (HID_SD_START + 93)
#define HID_PSEUDOSHEET_OUTLINE8            (HID_SD_START + 94)
#define HID_PSEUDOSHEET_OUTLINE9            (HID_SD_START + 95)
#define HID_PSEUDOSHEET_BACKGROUNDOBJECTS   (HID_SD_START + 96)
#define HID_PSEUDOSHEET_BACKGROUND          (HID_SD_START + 97)
#define HID_PSEUDOSHEET_NOTES               (HID_SD_START + 98)
#define HID_PSEUDOSHEET_SUBTITLE            (HID_SD_START + 101)
