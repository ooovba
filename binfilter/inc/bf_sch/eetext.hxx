/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: eetext.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _EETEXT_HXX
#define _EETEXT_HXX
namespace binfilter {

// Zeichenattribute....
#define ITEMID_FONT 			EE_CHAR_FONTINFO
#define ITEMID_POSTURE			EE_CHAR_ITALIC
#define ITEMID_WEIGHT			EE_CHAR_WEIGHT
#define ITEMID_SHADOWED 		EE_CHAR_SHADOW
#define ITEMID_CONTOUR			EE_CHAR_OUTLINE
#define ITEMID_CROSSEDOUT		EE_CHAR_STRIKEOUT
#define ITEMID_UNDERLINE		EE_CHAR_UNDERLINE
#define ITEMID_FONTHEIGHT		EE_CHAR_FONTHEIGHT
#define ITEMID_COLOR			EE_CHAR_COLOR
#define ITEMID_WORDLINEMODE 	0
#define ITEMID_PROPSIZE 		0
#define ITEMID_CHARSETCOLOR 	0
#define ITEMID_KERNING			0
#define ITEMID_AUTOKERN			0
#define ITEMID_CASEMAP			0
#define ITEMID_LANGUAGE			0
#define ITEMID_ESCAPEMENT		0
#define ITEMID_NOLINEBREAK		0
#define ITEMID_NOHYPHENHERE 	0

// Absatzattribute
#define ITEMID_ADJUST			EE_PARA_JUST
#define ITEMID_LINESPACING		EE_PARA_SBL
#define ITEMID_WIDOWS			0
#define ITEMID_ORPHANS			0
#define ITEMID_HYPHENZONE		0

#define ITEMID_TABSTOP			EE_PARA_TABS

#define ITEMID_PAPERBIN 		0
#define ITEMID_LRSPACE			EE_PARA_LRSPACE
#define ITEMID_ULSPACE			EE_PARA_ULSPACE
#define ITEMID_PRINT			0
#define ITEMID_OPAQUE			0
#define ITEMID_PROTECT			0
#define ITEMID_BACKGROUND		0
#define ITEMID_SHADOW			0
#define ITEMID_MACRO			0
#define ITEMID_BOX				0
#define ITEMID_BOXINFO			0


} //namespace binfilter
#endif  // EETEXT_HXX


