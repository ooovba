/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: datalog.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SCH_DATALOG
#define _SCH_DATALOG

#include "memchrt.hxx"
#include <float.h>
#include <math.h>
#define SCH_DATALOG_ANY -1
namespace binfilter {

class SchDataLogBook
{
    long* mpRowCoordinates;
    long* mpColCoordinates;

    long dummy;

    long mnRows;
    long mnCols;
    long mnColsInitial;
    long mnRowsInitial;

    long mnRowsAdded;
    long mnColsAdded;
    long mnRowsLeft;
    long mnColsLeft;

    BOOL mbValid;
    BOOL mbRowChanged;
    BOOL mbColChanged;
    BOOL mbGetCol;

    void IncreaseRowCount();
    void IncreaseColCount();

public:

     void Reset();
    ~SchDataLogBook();
};

} //namespace binfilter
#endif
