/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: objadj.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SCH_OBJADJ_HXX
#define _SCH_OBJADJ_HXX

#ifndef _SCH_ADJUST_HXX
#include "adjust.hxx"
#endif

#ifndef _SVX_CHRTITEM_HXX //autogen
#include <bf_svx/chrtitem.hxx>
#endif
#ifndef _SVDOBJ_HXX //autogen
#include <bf_svx/svdobj.hxx>
#endif
#ifndef _STREAM_HXX //autogen
#include <tools/stream.hxx>
#endif
namespace binfilter {


/*************************************************************************
|*
|* Ausrichtung von Chart-Objekten
|*
\************************************************************************/

class SchObjectAdjust : public SdrObjUserData
{
    ChartAdjust			eAdjust;	// Ausrichtung
    SvxChartTextOrient	eOrient;	// Orientierung

public:
    SchObjectAdjust();
    SchObjectAdjust(ChartAdjust eAdj, SvxChartTextOrient eOr);

    virtual SdrObjUserData* Clone(SdrObject *pObj) const;

    virtual void WriteData(SvStream& rOut);
    virtual void ReadData(SvStream& rIn);

    void SetAdjust(ChartAdjust eAdj) { eAdjust = eAdj; }
    ChartAdjust GetAdjust() { return eAdjust; }

    void SetOrient(SvxChartTextOrient eOr) { eOrient = eOr; }
    SvxChartTextOrient GetOrient() { return eOrient; }
};

/*************************************************************************
|*
|* Tool-Funktion fuer Objekt-Ausrichtung
|*
\************************************************************************/

extern SchObjectAdjust* GetObjectAdjust(const SdrObject& rObj);

} //namespace binfilter
#endif	// _SCH_OBJADJ_HXX


