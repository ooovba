/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: crypter.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SW3CRYPT_HXX
#define _SW3CRYPT_HXX

#ifndef SOLAR_H
#include <tools/solar.h>
#endif
class String; 
namespace binfilter {


#define PASSWDLEN 16

class Crypter
{
    BYTE cPasswd[ PASSWDLEN ];
public:
    Crypter( const ByteString& rPasswd );
    short GetMaxPasswdLen() const { return PASSWDLEN; }
    short GetMinPasswdLen() const { return 5;         }
 
    void Encrypt( ByteString& rTxt ) const;
    void Decrypt( ByteString& rTxt ) const;
};

} //namespace binfilter
#endif
