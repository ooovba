/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cfgid.h,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CFGID_H
#define _CFGID_H

#ifndef _SFX_HRC //autogen
#include <bf_sfx2/sfx.hrc>
#endif

#define CFG_STATUSBAR                         (SFX_ITEMTYPE_SW_BEGIN +13)
#define CFG_SW_MENU 			(SFX_ITEMTYPE_SW_BEGIN +16)
#define CFG_SW_ACCEL 			(SFX_ITEMTYPE_SW_BEGIN +17)
#define CFG_SWWEB_MENU          (SFX_ITEMTYPE_SW_BEGIN +19)
#define CFG_SWWEB_ACCEL         (SFX_ITEMTYPE_SW_BEGIN +20)
#define CFG_INSERT_DBCOLUMN_ITEM 	(SFX_ITEMTYPE_SW_BEGIN +25)
#define CFG_SW_MENU_PORTAL			(SFX_ITEMTYPE_SW_BEGIN +27)
#define CFG_SWWEB_MENU_PORTAL       (SFX_ITEMTYPE_SW_BEGIN +28)
#define CFG_SWGLOBAL_MENU           (SFX_ITEMTYPE_SW_BEGIN +29)
#define CFG_SWGLOBAL_MENU_PORTAL    (SFX_ITEMTYPE_SW_BEGIN +30)

#endif
