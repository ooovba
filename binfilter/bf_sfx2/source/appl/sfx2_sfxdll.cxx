/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: sfx2_sfxdll.cxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifdef WIN
#ifndef _SVWIN_H
#include <svwin.h>
#endif
#endif

#ifdef _MSC_VER
#pragma hdrstop
#endif

#ifdef WIN
namespace binfilter {

// Statische DLL-Verwaltungs-Variablen
static HINSTANCE hDLLInst = 0;

//==========================================================================

/*N*/ extern "C" int CALLBACK LibMain( HINSTANCE hDLL, WORD, WORD nHeap, LPSTR )
/*N*/ {
/*N*/ #ifndef WNT
/*N*/ 	if ( nHeap )
/*N*/ 		UnlockData( 0 );
/*N*/ #endif
/*N*/ 
/*N*/ 	hDLLInst = hDLL;
/*N*/ 
/*N*/ 	return TRUE;
/*N*/ }


//--------------------------------------------------------------------------

/*N*/ extern "C" int CALLBACK WEP( int )
/*N*/ {
/*N*/ 	return 1;
/*N*/ }



//==========================================================================
}
#endif



