/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svx_optgrid.cxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// include ---------------------------------------------------------------

#define _SVX_OPTGRID_CXX

#include "svxids.hrc"
#include "optgrid.hxx"
#include "dialogs.hrc"
namespace binfilter {

/*--------------------------------------------------------------------
    Beschreibung: Rastereinstellungen Ctor
 --------------------------------------------------------------------*/

/*N*/ SvxOptionsGrid::SvxOptionsGrid() :
/*N*/ 	nFldDrawX		( 100 ),
/*N*/ 	nFldDivisionX	( 0 ),
/*N*/ 	nFldDrawY		( 100 ),
/*N*/ 	nFldDivisionY	( 0 ),
/*N*/ 	nFldSnapX		( 100 ),
/*N*/ 	nFldSnapY		( 100 ),
/*N*/ 	bUseGridsnap	( 0 ),
/*N*/ 	bSynchronize	( 1 ),
/*N*/ 	bGridVisible	( 0 ),
/*N*/ 	bEqualGrid		( 1 )
/*N*/ {
/*N*/ }

/*--------------------------------------------------------------------
    Beschreibung: Rastereinstellungen Dtor
 --------------------------------------------------------------------*/

/*N*/ SvxOptionsGrid::~SvxOptionsGrid()
/*N*/ {
/*N*/ }

}
