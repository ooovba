/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tbx_ww.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/



#ifndef _SCH_TBX_WW_HXX
#define _SCH_TBX_WW_HXX

#ifndef _SV_TOOLBOX_HXX //autogen
#include <vcl/toolbox.hxx>
#endif
#ifndef _SFXTBXCTRL_HXX //autogen
#include <bf_sfx2/tbxctrl.hxx>
#endif


#include "schresid.hxx"
namespace binfilter {

//------------------------------------------------------------------------

class SchPopupWindowTbx : public SfxPopupWindow
{
private:
    ToolBox 			aTbx;

public:
    SchPopupWindowTbx( USHORT nId, SfxToolBoxControl* pTbxCtl,
                      SchResId aRIdWin, SchResId aRIdTbx,
                      SfxBindings& rBindings );
    ~SchPopupWindowTbx() {}

    void StartSelection() { aTbx.StartSelection(); }
    void SelectHdl( void* p );
};

} //namespace binfilter
#endif		// _SCH_TBX_WW_HXX

