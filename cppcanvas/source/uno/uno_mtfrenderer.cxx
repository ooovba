#include "uno_mtfrenderer.hxx"
#include <cppcanvas/vclfactory.hxx>
#include <comphelper/servicedecl.hxx>
#include <cppuhelper/factory.hxx>

using namespace ::com::sun::star;

void MtfRenderer::setMetafile (const uno::Sequence< sal_Int8 >& rMtf) throw (uno::RuntimeException)
{
        // printf ("MtfRenderer::setMetafile unimplemented, use fast property set or implement me\n");
}

void MtfRenderer::draw (double fScaleX, double fScaleY) throw (uno::RuntimeException)
{
    if (mpMetafile && mxCanvas.get()) {
        cppcanvas::VCLFactory& factory = cppcanvas::VCLFactory::getInstance();
        cppcanvas::BitmapCanvasSharedPtr canvas = factory.createCanvas (mxCanvas);
        cppcanvas::RendererSharedPtr renderer = factory.createRenderer (canvas, *mpMetafile, cppcanvas::Renderer::Parameters ());
        ::basegfx::B2DHomMatrix aMatrix;
        aMatrix.scale( fScaleX, fScaleY );
        canvas->setTransformation( aMatrix );
        renderer->draw ();
    }
}

void MtfRenderer::setFastPropertyValue( sal_Int32 nHandle, const uno::Any& aAny)  throw (uno::RuntimeException)
{
    if (nHandle == 0) {
        mpMetafile = (GDIMetaFile*) *reinterpret_cast<const sal_Int64*>(aAny.getValue()); 
    }
}

MtfRenderer::MtfRenderer (uno::Sequence<uno::Any> const& aArgs, uno::Reference<uno::XComponentContext> const&) : MtfRendererBase (m_aMutex), mpMetafile (NULL)
{
    if( aArgs.getLength() == 1 ) {
        aArgs[0] >>= mxCanvas;
    }
}

namespace sdecl = comphelper::service_decl;
#if defined (__GNUC__) && (__GNUC__ == 3 && __GNUC_MINOR__ <= 3)
 sdecl::class_<MtfRenderer, sdecl::with_args<true> > serviceImpl;
 const sdecl::ServiceDecl MtfRendererDecl(
     serviceImpl,
#else
 const sdecl::ServiceDecl MtfRendererDecl(
     sdecl::class_<MtfRenderer, sdecl::with_args<true> >(),
#endif
    "com.sun.star.comp.rendering.MtfRenderer",
    "com.sun.star.rendering.MtfRenderer" );

// The C shared lib entry points
COMPHELPER_SERVICEDECL_EXPORTS1(MtfRendererDecl)
