/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: PathFindFileNameW.cpp,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "macros.h"

#define _SHLWAPI_
#include <shlwapi.h>

#include <mbstring.h>

IMPLEMENT_THUNK( shlwapi, WINDOWS, LPWSTR, WINAPI, PathFindFileNameW, 
(
    LPCWSTR lpPathW
))
{
    AUTO_WSTR2STR(lpPath);    
    char* pFname = PathFindFileNameA(lpPathA);
    
    if (pFname > lpPathA)
    {        
        *pFname = '\0';
        LPWSTR pOutW = const_cast<LPWSTR>(lpPathW);
        return (pOutW + _mbslen(reinterpret_cast<unsigned char*>(lpPathA)));      
    }
    else
        return const_cast<LPWSTR>(lpPathW);
}
