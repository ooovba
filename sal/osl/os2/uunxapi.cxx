/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: uunxapi.cxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 
 #ifndef _LIMITS_H
 #include <limits.h>
 #endif
  
 #ifndef _RTL_USTRING_HXX_
 #include <rtl/ustring.hxx>
 #endif
 
 #ifndef _OSL_THREAD_H_
 #include <osl/thread.h>
 #endif
 
 #ifndef _OSL_UUNXAPI_HXX_
 #include "uunxapi.hxx"
 #endif 
 
 //###########################
 //access_u     
 int access_u(const rtl_uString* pustrPath, int mode)
 { 		
    return access(OUStringToOString(pustrPath).getStr(), mode);
 }
 
 //#########################
 //realpath_u  
 sal_Bool realpath_u(const rtl_uString* pustrFileName, rtl_uString** ppustrResolvedName)
 {
     rtl::OString fn = rtl::OUStringToOString(
        rtl::OUString(const_cast<rtl_uString*>(pustrFileName)),
        osl_getThreadTextEncoding());
        
    char  rp[PATH_MAX];
    bool  bRet = realpath(fn.getStr(), rp); 
    
    if (bRet)
    {
        rtl::OUString resolved = rtl::OStringToOUString(
            rtl::OString(static_cast<sal_Char*>(rp)),
            osl_getThreadTextEncoding());
            
        rtl_uString_assign(ppustrResolvedName, resolved.pData);
    }
    return bRet;
 }
 
 //#########################
 //lstat_u 
  int lstat_u(const rtl_uString* pustrPath, struct stat* buf)
 { 	
    return lstat(OUStringToOString(pustrPath).getStr(), buf);
 } 
 
 //#########################
 // @see mkdir
 int mkdir_u(const rtl_uString* path, mode_t mode)
 {    
    return mkdir(OUStringToOString(path).getStr(), mode);     
 }
 
