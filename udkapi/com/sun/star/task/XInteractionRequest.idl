/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XInteractionRequest.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_task_XInteractionRequest_idl__ 
#define __com_sun_star_task_XInteractionRequest_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_task_XInteractionContinuation_idl__ 
#include <com/sun/star/task/XInteractionContinuation.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module task {  
 
//============================================================================= 
 
// DocMerge from xml: interface com::sun::star::task::XInteractionRequest
/** The description of an interaction request.
 */
published interface XInteractionRequest: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: method com::sun::star::task::XInteractionRequest::getRequest
    /** Get information about the request itself.

        @returns
        an <type scope="com::sun::star::uno">Exception</type>, wrapped as an
        <atom>any</atom>.
     */
    any getRequest(); 
 
    //------------------------------------------------------------------------- 
    /** Get the set of
        <type scope="com::sun::star::task">XInteractionContinuation</type>s
        the client supports for this request.
     */
    sequence<com::sun::star::task::XInteractionContinuation> getContinuations(); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
