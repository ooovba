/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: PropertyBag.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_beans_PropertyBag_idl__ 
#define __com_sun_star_beans_PropertyBag_idl__ 
 
#ifndef __com_sun_star_beans_XPropertySet_idl__ 
#include <com/sun/star/beans/XPropertySet.idl> 
#endif 
 
#ifndef __com_sun_star_beans_XPropertyContainer_idl__ 
#include <com/sun/star/beans/XPropertyContainer.idl> 
#endif 
 
#ifndef __com_sun_star_beans_XPropertyAccess_idl__ 
#include <com/sun/star/beans/XPropertyAccess.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module beans {  
 
//============================================================================= 
 
/** Implementation of this service can keep any properties and is useful
    when an <type>XPropertySet</type> is to be used, for example, as parameters for a method call.

    <p>Scripting engines might not be able to use such objects as normal
    property sets, giving direct access to the properties.  In this case, 
    use the methods like <member>XPropertySet::getPropertyValue</member>.
 */
published service PropertyBag
{ 
    interface com::sun::star::beans::XPropertySet; 
    
    interface com::sun::star::beans::XPropertyContainer; 
    
    interface com::sun::star::beans::XPropertyAccess; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
