/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TolerantPropertySetResultType.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_beans_TolerantPropertySetResultType_idl__ 
#define __com_sun_star_beans_TolerantPropertySetResultType_idl__ 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module beans {  
 
//=============================================================================

/** specifies the possible failure types when using the
    <type scope="com::sun::star::beans">XTolerantMultiPropertySet</type>
    interface.

    <p>It usually matches one of the exception types that may occur when
    using the 
    <type scope="com::sun::star::beans">XPropertySet</type> or
    <type scope="com::sun::star::beans">XMultiPropertySet</type> interfaces.</p>
*/  
constants TolerantPropertySetResultType
{
    /** the property has been successfully set or retrieved.
    */
    const short SUCCESS             = 0;

    /** the property is not available.
        
        <p>For example if a 
        <type scope="com::sun::star::beans" >UnknownPropertyException</type> 
        was catched.</p>
    */
    const short UNKNOWN_PROPERTY    = 1;

    /** the value used with the property is not valid.

        <p>For example if a 
        <type scope="com::sun::star::lang" >IllegalArgumentException</type> 
        was catched.</p>
    */
    const short ILLEGAL_ARGUMENT    = 2;

    /** the property could not be changed at that time.

        <p>For example if a 
        <type scope="com::sun::star::beans" >PropertyVetoException</type> 
        was catched.</p>
    */
    const short PROPERTY_VETO       = 3;

    /** a <type scope="com::sun::star::lang">WrappedTargetException</type>
        did occur.
    */
    const short WRAPPED_TARGET      = 4;

    /** the operation failed and the reason is not known.
    */
    const short UNKNOWN_FAILURE     = 5;
};

//=============================================================================
 
}; }; }; };  
 
#endif 
