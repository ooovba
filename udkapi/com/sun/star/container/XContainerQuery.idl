/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XContainerQuery.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_container_XContainerQuery_idl__
#define __com_sun_star_container_XContainerQuery_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_container_XEnumeration_idl__
#include <com/sun/star/container/XEnumeration.idl>
#endif

#ifndef __com_sun_star_beans_NamedValue_idl__
#include <com/sun/star/beans/NamedValue.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module container {

//=============================================================================
/** supports simple query feature on a container

    <p>
    This interface makes it possible to create sub sets of container items
    which serve specified search criterion.
    <p>
 */
published interface XContainerQuery: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** creates a sub set of container items which match given query command

        <p>
        Items of this sub set must match used query string. Format of query depends
        from real implementation. Using of "param=value" pairs isn't neccessary.
        So it's possible to combine different parameters as one simple command
        string.
        <p>

        @param Query    items of sub set must match to this query<br>
                        example:<br>
                        (1)<br>
                        query as parameter sequence to return all elements wich match
                        the name pattern and supports a special feature; sort it ascending<br>
                        "name=*myname_??_;sort=ascending;feature=VISIBLE"<br>
                        (2)<br>
                        query as command to return all elements which support special feature
                        and match by name pattern;ascending sort is the default<br>
                        "getAllVisible(*myname_??_)"<br>

        @returns an sub set of container items as an enumeration.
     */
    XEnumeration createSubSetEnumerationByQuery( [in] string Query );

    //-------------------------------------------------------------------------
    /** creates a sub set of container items which supports searched properties as minimum

        <p>
        It's not possible to use special commands or search specific parameters here.
        You can match by properties only. Enumerated elements must provide queried
        properties as minimum. Not specified properties willn't be used for searching.
        <p>

        @param Properties   items of sub set must support given properties as minimum<br>
                            example:<br>
                            (supported)<br>
                            search for items wich match the name pattern and supports the VISIBLE feature<br>
                            Parameters[0].Name  = "name"<br>
                            Parameters[0].Value = "*myname_??_"<br>
                            Parameters[1].Name  = "feature"<br>
                            Parameters[1].Value = "VISIBLE"<br>
                            ...<br>
                            (unsupported)<br>
                            "sort" isn't a property of a container item!
                            Parameters[0].Name  = "sort"<br>
                            Parameters[0].Value = "ascending"<br>
                            ...<br>

        @returns an sub set of container items as an enumeration.
     */
    XEnumeration createSubSetEnumerationByProperties( [in] sequence< com::sun::star::beans::NamedValue > Properties );
};

//=============================================================================

}; }; }; };

#endif
