/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XChild.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_container_XChild_idl__ 
#define __com_sun_star_container_XChild_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_lang_NoSupportException_idl__ 
#include <com/sun/star/lang/NoSupportException.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module container {  
 
//============================================================================= 
 
/** provides access to the parent of the object.
    
    <p>This interface normally is only supported if the objects all have
    exactly one dedicated parent container.</p>
 */
published interface XChild: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    /** grants access to the object containing this content.
     */
    com::sun::star::uno::XInterface getParent(); 
 
    //------------------------------------------------------------------------- 
     
    /** sets the parent to this object. 

        @throws com::sun::star::lang::NoSupportException
            if the name of this object cannot be changed.
     */
    void setParent( [in] com::sun::star::uno::XInterface Parent ) 
            raises( com::sun::star::lang::NoSupportException ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
