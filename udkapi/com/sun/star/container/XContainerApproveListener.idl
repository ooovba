/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XContainerApproveListener.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_container_XContainerApproveListener_idl
#define __com_sun_star_container_XContainerApproveListener_idl

#ifndef __com_sun_star_container_ContainerEvent_idl__ 
#include <com/sun/star/container/ContainerEvent.idl>
#endif

#ifndef __com_sun_star_lang_WrappedTargetException_idl__ 
#include <com/sun/star/lang/WrappedTargetException.idl>
#endif

#ifndef __com_sun_star_util_XVeto_idl__
#include <com/sun/star/util/XVeto.idl>
#endif

//=============================================================================

module com { module sun { module star { module container { 

//=============================================================================

/** is notified to approve changes which happen to the content of a generic container
    
    @see XContainerApproveBroadcaster
 */
interface XContainerApproveListener
{
    /** is called for the listener to approve an insertion into the container

        @return
            an instance implementing the <type scope="com::sun::star::util">XVeto</type> interface,
            if the insertion is vetoed, <NULL/> otherwise.
     */
    com::sun::star::util::XVeto approveInsertElement( [in] ContainerEvent Event )
      raises ( com::sun::star::lang::WrappedTargetException );

    /** is called for the listener to approve a replacement inside the container

        @return
            an instance implementing the <type scope="com::sun::star::util">XVeto</type> interface,
            if the replacement is vetoed, <NULL/> otherwise.
     */
    com::sun::star::util::XVeto approveReplaceElement( [in] ContainerEvent Event )
      raises ( com::sun::star::lang::WrappedTargetException );

    /** is called for the listener to approve a removal of an element from the container

        @return
            an instance implementing the <type scope="com::sun::star::util">XVeto</type> interface,
            if the removal is vetoed, <NULL/> otherwise.
     */
    com::sun::star::util::XVeto approveRemoveElement( [in] ContainerEvent Event )
      raises ( com::sun::star::lang::WrappedTargetException );
};

//=============================================================================

}; }; }; }; 

//=============================================================================

#endif
