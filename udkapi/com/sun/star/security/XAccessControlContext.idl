/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XAccessControlContext.idl,v $
 * $Revision: 1.16 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_security_XAccessControlContext_idl__ 
#define __com_sun_star_security_XAccessControlContext_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl> 
#endif
#ifndef __com_sun_star_security_AccessControlException_idl__
#include <com/sun/star/security/AccessControlException.idl> 
#endif
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module security {  
 
//============================================================================= 
 
/** An XAccessControlContext is used to make system resource access decisions
    based on the context it encapsulates.
    <p>
    More specifically, it encapsulates a context and has methods to check
    permissions equivalent to XAccessController interface,
    with one difference:
    The XAccessControlContext makes access decisions
    based on the context it encapsulates, rather than
    that of the current execution thread.
    </p>

    @since OOo 1.1.2
*/
published interface XAccessControlContext : com::sun::star::uno::XInterface
{
    /** Determines whether the access request indicated by the specified
        permission should be allowed or denied, based on this context.
        The semantics are equivalent to the security permission classes of
        the Java platform.
        <p>
        You can also pass a sequence of permissions (sequence< any >) to check
        a set of permissions, e.g. for performance reasons.
        This method quietly returns if the access request is permitted,
        or throws a suitable AccessControlException otherwise.
        </p>
        
        @param perm
               permission to be checked
               
        @throws AccessControlException
                thrown if access is denied

        @see ::com::sun::star::security::AccessControlException
        @see ::com::sun::star::security::AllPermission
        @see ::com::sun::star::security::RuntimePermission
        @see ::com::sun::star::io::FilePermission
        @see ::com::sun::star::connection::SocketPermission
    */
    void checkPermission(
        [in] any perm )
        raises (AccessControlException);
};

//============================================================================= 

}; }; }; };

#endif
