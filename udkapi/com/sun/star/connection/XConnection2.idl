/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XConnection2.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _COM_SUN_STAR_CONNECTION_XCONNECTION2_IDL_ 
#define _COM_SUN_STAR_CONNECTION_XCONNECTION2_IDL_ 
 
#include <com/sun/star/io/IOException.idl> 
#include <com/sun/star/io/BufferSizeExceededException.idl> 
#include <com/sun/star/io/NotConnectedException.idl> 

#include <com/sun/star/connection/XConnection.idl> 
 
 
module com {  module sun {  module star {  module connection { 
 
 
 
 
/**
   XConnection2 extends the <code>XConnection</code> interface
   with <code>available</code> and <code>readSomeBytes</code>
*/
published interface XConnection2: com::sun::star::connection::XConnection
{ 
     
    /** Gives the number of bytes available via <code>read</code> 
        without blocking.
     */
    long available() raises(com::sun::star::io::IOException); 

    /** Blocks if no data is available otherwise reads at 
        max <var>nMaxBytesToRead</var> but at least 1 byte.
        </p>
     */
    long readSomeBytes([out] sequence<byte> aData, [in] long nMaxBytesToRead) 
        raises(com::sun::star::io::IOException); 
}; 
 
};};};};  
 
#endif 
