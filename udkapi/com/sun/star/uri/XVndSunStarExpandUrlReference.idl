/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XVndSunStarExpandUrlReference.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_uri_VndSunStarExpandUrlReference_idl__
#define __com_sun_star_uri_VndSunStarExpandUrlReference_idl__

#include "com/sun/star/uri/XUriReference.idl"
#include "com/sun/star/uri/XVndSunStarExpandUrl.idl"

module com { module sun { module star { module uri {

/**
   represents absolute &ldquo;vnd.sun.star.expand&rdquo; URL references.

   @since OOo 2.3
 */
published interface XVndSunStarExpandUrlReference {
    /**
       represents the generic features of the URL reference.

       <p>The behaviour of the methods of this interface will always reflect the
       fact that the represented URI reference is an absolute
       &ldquo;vnd.sun.star.expand&rdquo; URL reference.</p>
     */
    interface XUriReference;

    /**
       represents the scheme-specific features of the URL reference.
     */
    interface XVndSunStarExpandUrl;
};

}; }; }; };

#endif
