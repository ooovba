/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: SharedLibrary.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_loader_SharedLibrary_idl__ 
#define __com_sun_star_loader_SharedLibrary_idl__ 
 
#ifndef __com_sun_star_lang_XServiceInfo_idl_idl__ 
#include <com/sun/star/lang/XServiceInfo.idl> 
#endif 
 
 module com {  module sun {  module star {  module loader { 
 
 published interface XImplementationLoader; 
 
 
/** allows to access a native component stored in a shared library.
 */
published service SharedLibrary
{ 
     
    /** is used for writing persistent information in the registry for  
                an external implementation and for activating this 
                implementation.
     */
    interface XImplementationLoader; 
 
     
    /** gives information about other supported services
     */
    interface com::sun::star::lang::XServiceInfo; 
}; 
 
}; }; }; }; //module com.sun.star.loader 
 
/*===================================================================== 
 
      Source Code Control System - Update 
 
=====================================================================*/ 
#endif 
 
