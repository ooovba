/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDefaultProperty.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_script_XDefaultProperty_idl__
#define __com_sun_star_script_XDefaultProperty_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

module com { module sun { module star { module script {
//==============================================================================
/** 
    An object supporting this interface indicates to interested
    parties or clients the name of the default propery for
    this object. 
    <p>For example where ExampleObject is an instance of an Object that 
    supports this interface which returns the default property name 
    "Value".A scripting engine could use this information to support 
    syntax like <p>ExampleObject = "foo"</p> 
    which would be equivalent to writing 
    <p>ExampleObject.Value = "foo"</p>
    or
    <p>bar = ExampleObject</p>
    which would be equivalent to writing 
    <p>bar = ExampleObject.Value</p>
*/
interface XDefaultProperty : ::com::sun::star::uno::XInterface
{
  //-----------------------------------------------------------------------
  /**
    Returns the name of the default property

    @return
     The <atom>string</atom> name of default property
  */
  string getDefaultPropertyName();

};

};  };  };  };
#endif
