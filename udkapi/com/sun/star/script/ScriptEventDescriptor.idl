/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ScriptEventDescriptor.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_ScriptEventDescriptor_idl__ 
#define __com_sun_star_script_ScriptEventDescriptor_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module script {  
 
//============================================================================= 
/** describes an effect, especially a script to be executed,
    for a certain event given by the listener type and the name of the 
    event method.
 */
published struct ScriptEventDescriptor
{
    /** listener type as string, same as listener-XIdlClass.getName().
     */
    string ListenerType; 
 
    //------------------------------------------------------------------------- 
    /** event method as string.
     */
    string EventMethod; 
 
    //------------------------------------------------------------------------- 
    /** data to be used if the addListener method needs an additional
        parameter. 
        <p>If the type of this parameter is different from string,
        it will be converted, when added.</p>
     */
    string AddListenerParam; 
 
    //------------------------------------------------------------------------- 
    /** type of the script language as string; for example, "Basic" or "StarScript".
     */
    string ScriptType; 
 
    //------------------------------------------------------------------------- 
    /** script code as string (the code has to correspond with the language
        defined by ScriptType).
     */
    string ScriptCode; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
