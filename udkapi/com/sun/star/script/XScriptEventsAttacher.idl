/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XScriptEventsAttacher.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_XScriptEventsAttacher_idl__ 
#define __com_sun_star_script_XScriptEventsAttacher_idl__ 
 
#ifndef __com_sun_star_script_XScriptListener_idl__ 
#include <com/sun/star/script/XScriptListener.idl> 
#endif 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_lang_IllegalArgumentException_idl__ 
#include <com/sun/star/lang/IllegalArgumentException.idl> 
#endif 
 
#ifndef __com_sun_star_beans_IntrospectionException_idl__ 
#include <com/sun/star/beans/IntrospectionException.idl> 
#endif 
 
#ifndef __com_sun_star_script_CannotCreateAdapterException_idl__ 
#include <com/sun/star/script/CannotCreateAdapterException.idl> 
#endif 
 
#ifndef __com_sun_star_lang_ServiceNotRegisteredException_idl__ 
#include <com/sun/star/lang/ServiceNotRegisteredException.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module script {  
 
//============================================================================= 
 
/** 
    This interface can be used to attach script events to a number of 
    objects that give access to the definition of events that should
    be attached to them, e.g., by supporting XEventsSupplier
*/
published interface XScriptEventsAttacher: com::sun::star::uno::XInterface
{ 
    /** 
        Attaches the events defined by XScriptEventsSupplier to the
        corresponding object implementing XScriptEventsSupplier.
        
        @param Objects		
                            Sequence of all objects. Usually the objects should directly
                            support <type>XScriptEventsAttacher</type> to define the events
                            but this is not strictly required. It's also possible that
                            the object implementing <type>XScriptEventsAttacher</type>
                            knows how to get the necessary information for the objects.
        @param xListener	
                            All events (if defined by XScriptEventsSupplier) that are fired 
                            by one of the objects are mapped into a <type>ScriptEvent</type> 
                            and passed to the methods of this XScriptListener.
        @param Helper		
                            Helper object for the implementation. This value will be
                            passed to the XScriptListener as Helper property in the
                            <type>ScriptEvent</type>.
    */
    void attachEvents(	[in] sequence< com::sun::star::uno::XInterface > Objects, 
                        [in] com::sun::star::script::XScriptListener xListener,
                        [in] any Helper )
            raises( com::sun::star::lang::IllegalArgumentException, 
                    com::sun::star::beans::IntrospectionException, 
                    com::sun::star::script::CannotCreateAdapterException, 
                    com::sun::star::lang::ServiceNotRegisteredException ); 

}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 

