/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: FailReason.idl,v $
 * $Revision: 1.13 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_FailReason_idl__ 
#define __com_sun_star_script_FailReason_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module script {  
 
//============================================================================= 
 
/** These values specify the reason why a type conversion failed.
*/
published constants FailReason
{ 
    /** The given value does not fit in the range of the destination type.
    */
    const long OUT_OF_RANGE = 1; 
 
    /** The given value cannot be converted to a number.
    */
    const long IS_NOT_NUMBER = 2; 
 
    /** The given value cannot be converted to an enumeration.
    */
    const long IS_NOT_ENUM = 3; 
 
    /** The given value cannot be converted to a boolean.
    */
    const long IS_NOT_BOOL = 4; 
 
    /** The given value is not an interface or cannot queried to the right interface.
    */
    const long NO_SUCH_INTERFACE = 5; 
 
    /** The given value cannot be converted to right structure or exception type.
    */
    const long SOURCE_IS_NO_DERIVED_TYPE = 6; 
 
    /** The type class of the given value is not supported.
    */
    const long TYPE_NOT_SUPPORTED = 7; 
 
    /** The given value cannot be converted and none of the other reasons match.
    */
    const long INVALID = 8; 
 
    /** This value is deprecated.  Do not use.
        @deprecated
    */
    const long NO_DEFAULT_AVAILABLE = 9; 
 
    /** This value is deprecated.  Do not use.
        @deprecated
    */
    const long UNKNOWN = 10;
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
