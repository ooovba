/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: InterruptEngineEvent.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_InterruptEngineEvent_idl__ 
#define __com_sun_star_script_InterruptEngineEvent_idl__ 
 
#ifndef __com_sun_star_lang_EventObject_idl__ 
#include <com/sun/star/lang/EventObject.idl> 
#endif 
 
#ifndef __com_sun_star_script_InterruptReason_idl__ 
#include <com/sun/star/script/InterruptReason.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module script {  
 
//============================================================================= 
/** describes an interrupt which occurs in the scripting engine.
    @deprecated
 */
published struct InterruptEngineEvent: com::sun::star::lang::EventObject
{ 
    //------------------------------------------------------------------------- 
    /** fully qualified name to address the module or function affected by the event that 
        took place. 
        
        <p>If the module or function can't be addressed by name (for example, in case 
        that a runtime-generated eval-module is executed), this string is empty.</p>
     */
    string Name; 
 
    //------------------------------------------------------------------------- 
    /** source code of the Module affected by the event that took place. 
        
        <p>If the source can 
        be accessed using the ModuleName, or if the source is unknown (executing compiled 
        code), this string can be empty.</p>
     */
    string SourceCode; 
 
    //------------------------------------------------------------------------- 
    /** contains the first line in the module's source code that is affected
        by the event that took place.
        
        
        
        <p>If "name" addresses a function, all line and column values
        are nevertheless given relative to the module's source. If
        source code is not available, this value addresses a binary
        position in the compiled code.  </p>

        @see XLibraryAccess::getModuleCode
        @see XLibraryAccess::getFunctionCode
     */
    long StartLine; 
 
    //------------------------------------------------------------------------- 
    /** contains the first column in the "StartLine" that is affected by the
        event that took place.
     */
    long StartColumn; 
 
    //------------------------------------------------------------------------- 
    /** contains the last line in the module's source code that is affected
        by the event that took place.
     */
    long EndLine; 
 
    //------------------------------------------------------------------------- 
    /** contains the first column in the "EndLine" which is NOT affected by 
        the event that took place.
     */
    long EndColumn; 
 
    //------------------------------------------------------------------------- 
    /** error message. 
        <p>Only valid if Reason is RuntimeError or CompileError.</p>
     */
    string ErrorMessage; 
 
    //------------------------------------------------------------------------- 
    /** contains the interrupt reason.
     */
    com::sun::star::script::InterruptReason Reason; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
