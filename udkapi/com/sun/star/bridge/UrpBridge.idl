/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: UrpBridge.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_bridge_Urp_idl__ 
#define __com_sun_star_bridge_Urp_idl__ 

#ifndef __com_sun_star_lang_XInitialization_idl__ 
#include <com/sun/star/lang/XInitialization.idl>
#endif 

#ifndef __com_sun_star_lang_XComponent_idl__ 
#include <com/sun/star/lang/XComponent.idl>
#endif 

#ifndef __com_sun_star_bridge_XBridge_idl__ 
#include <com/sun/star/bridge/XBridge.idl>
#endif 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module bridge {  
 
//============================================================================= 
/** Concrete service of the meta service Bridge for the urp protocol.

    <p>This bridge works with the urp protocol.

   @see com.sun.star.bridge.Bridge
 */
published service UrpBridge
{ 
    /** This interface allows to initialize the bridge service with the necessary
        arguments. The sequence&lt;any&gt; must have 4 members.
    
        <ol>
            <li> String: The unique name of the bridge or an empty string</li>
            <li> String: urp</li>
            <li> XConnection: The bidirectional connection, the bridge should work on</li>
            <li> XInstanceProvider: 
                The instance provider, that shall be called to access the initial object.</li>
        </ol>
     */
    interface com::sun::star::lang::XInitialization;
    
    /** The main interface of the service.
     */
    interface com::sun::star::bridge::XBridge;

    /** allows to terminate the interprocess bridge.
     */
    interface com::sun::star::lang::XComponent;

}; 
//============================================================================= 
 
}; }; }; };  
#endif 
