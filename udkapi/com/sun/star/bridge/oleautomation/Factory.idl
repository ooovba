/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Factory.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_bridge_oleautomation_Factory_idl__ 
#define __com_sun_star_bridge_oleautomation_Factory_idl__ 
 
#ifndef __com_sun_star_lang_XMultiServiceFactory_idl__ 
#include <com/sun/star/lang/XMultiServiceFactory.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module bridge {  module oleautomation {
 
//============================================================================= 
 
/** makes it possible to create COM objects as UNO objects.
    
    <p>
    A COM object must have a ProgId since the ProgId is used as argument for
    <method scope="com::sun::star::lang">XMultiServiceFactory::createInstance</method>.
    The only interfaces which are mapped are <code>IUnknown</code> and
    <code>IDispatch</code>. The created UNO objects support
    <type scope="com::sun::star::script">XInvocation</type> if the original COM
    objects support <code>IDispatch</code>.
    </p>
    <p>
    The optional parameters of the method
    <method scope="com::sun::star::lang">XMultiServiceFactory::createInstanceWithArguments</method>
    are reserved for future use; at this time they are ignored.
    </p>
 */
service Factory
{
    interface com::sun::star::lang::XMultiServiceFactory;
}; 
 
//============================================================================= 
 
}; }; }; }; }; 
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
