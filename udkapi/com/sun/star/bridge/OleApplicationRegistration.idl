/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: OleApplicationRegistration.idl,v $
 * $Revision: 1.14 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_bridge_OleApplicationRegistration_idl__ 
#define __com_sun_star_bridge_OleApplicationRegistration_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module bridge {  
 
//============================================================================= 
 
/** registers UNO objects as COM objects.
    <p />
    That is, COM class factories are registered with COM at runtime. The required
    EXE server is the application which deploys this service. In order to access the
    factories by COM API functions, there need to be proper registry entries. This service
    does not provide for writing those entries.<p>
    The instantiation of the registered objects can be carried out by any ordinary mechanism
    which is used in a certain language to create COM components. For example, CreateObject
    in Visual Basic, CoCreateInstance in C++.
    <p />
    Currently only a factory for the service com.sun.star.long.MultiServiceFactory is registered
    by this service. The CLSID is {82154420-0FBF-11d4-8313-005004526AB4} and the ProgId is
    com.sun.star.ServiceManager.
    
    <p>OleApplicationRegistration does not provide any particular interface because the UNO objects 
    are registered while instantiating this service and deregistered 
    if the implementation, which makes use of this service, is being released.</p>
    <p>
    @deprecated
 */
published service OleApplicationRegistration
{ 

    interface com::sun::star::uno::XInterface; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
