/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XUnoUrlResolver.idl,v $
 * $Revision: 1.13 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_bridge_XUnoUrlResolver_idl__ 
#define __com_sun_star_bridge_XUnoUrlResolver_idl__ 
 
#include <com/sun/star/uno/XInterface.idl> 
#include <com/sun/star/lang/IllegalArgumentException.idl> 
#include <com/sun/star/connection/ConnectionSetupException.idl> 
#include <com/sun/star/connection/NoConnectException.idl> 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module bridge {  
 
//============================================================================= 
 
/** allows to resolve an object using the uno-url.
 */
published interface XUnoUrlResolver: com::sun::star::uno::XInterface
{ 
     
    /** resolves an object using the given uno-url.
        @param sUnoUrl the uno-url. The uno-url is specified 
        <a href="http://udk.openoffice.org/common/man/spec/uno-url.html">here</a>.
        
        @returns the resolved object, in general a proxy for a remote object.
                 You can use it the same way as you use local references.
     */
    com::sun::star::uno::XInterface resolve( [in] string sUnoUrl ) 
        raises (com::sun::star::connection::NoConnectException, 
                com::sun::star::connection::ConnectionSetupException, 
                com::sun::star::lang::IllegalArgumentException); 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
