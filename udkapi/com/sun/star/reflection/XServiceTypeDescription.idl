/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XServiceTypeDescription.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_reflection_XServiceTypeDescription_idl__
#define __com_sun_star_reflection_XServiceTypeDescription_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif
#ifndef __com_sun_star_reflection_XInterfaceTypeDescription_idl__
#include <com/sun/star/reflection/XInterfaceTypeDescription.idl>
#endif
#ifndef __com_sun_star_reflection_XPropertyTypeDescription_idl__
#include <com/sun/star/reflection/XPropertyTypeDescription.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module reflection {

//=============================================================================

/** Reflects a service.

    <p>This type is superseded by <type>XServiceTypeDescription2</type>, which
    supports single-interface&ndash;based services, in addition to the obsolete,
    accumulation-based services.</p>

    <p>The type class of this type is
    <member scope="com::sun::star::uno">TypeClass::SERVICE</member>.

    @since OOo 1.1.2
  */
published interface XServiceTypeDescription : com::sun::star::reflection::XTypeDescription
{
    /** Returns the type descriptions of the mandatory services
        defined for this service.

        @return a sequence containing service type descriptions, for an
            obsolete, accumulation-based service; for a
            single-interface&ndash;based service, an empty sequence is returned
      */
    sequence< XServiceTypeDescription > getMandatoryServices();

    /** Returns the type descriptions of the optional services
        defined for this service.

        @return a sequence containing service type descriptions, for an
            obsolete, accumulation-based service; for a
            single-interface&ndash;based service, an empty sequence is returned
      */
    sequence< XServiceTypeDescription > getOptionalServices();

    /** Returns the type descriptions of the mandatory interfaces
        defined for this service.

        @return a sequence containing interface type descriptions, for an
            obsolete, accumulation-based service; for a
            single-interface&ndash;based service, an empty sequence is returned
      */
    sequence< XInterfaceTypeDescription > getMandatoryInterfaces();

    /** Returns the type descriptions of the optional interface
        defined for this service.

        @return a sequence containing interface type descriptions, for an
            obsolete, accumulation-based service; for a
            single-interface&ndash;based service, an empty sequence is returned
      */
    sequence< XInterfaceTypeDescription > getOptionalInterfaces();

    /** Returns the properties defined for this service.

        @return a sequence containing property descriptions, for an obsolete,
            accumulation-based service; for a single-interface&ndash;based
            service, an empty sequence is returned
      */
    sequence< XPropertyTypeDescription > getProperties();
};

//=============================================================================

}; }; }; };

#endif
