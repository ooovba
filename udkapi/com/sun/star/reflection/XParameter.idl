/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XParameter.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_reflection_XParameter_idl__
#define __com_sun_star_reflection_XParameter_idl__

#include "com/sun/star/reflection/XMethodParameter.idl"

module com {  module sun {  module star {  module reflection {

/**
   Reflects a parameter of an interface method or a service constructor.

   <p>This type supersedes <type>XMethodParameter</type>, which only supports
   parameters of interface methods (which cannot have rest parameters).</p>

   @since OOo 2.0.0
 */
interface XParameter: XMethodParameter {
    /**
       Returns whether this is a rest parameter.

       <p>A rest parameter must always come last in a parameter list.</p>

       <p>Currently, only service constructors can have rest parameters, and
       those rest parameters must be in parameters of type <atom>any</atom>.</p>

       @return <TRUE/> if and only if this parameter is a rest parameter
     */
    boolean isRestParameter();
};

}; }; }; };

#endif
