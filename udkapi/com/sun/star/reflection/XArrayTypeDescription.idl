/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XArrayTypeDescription.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_reflection_XArrayTypeDescription_idl__ 
#define __com_sun_star_reflection_XArrayTypeDescription_idl__ 
 
#ifndef __com_sun_star_reflection_XTypeDescription_idl__ 
#include <com/sun/star/reflection/XTypeDescription.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module reflection {  
 
//============================================================================= 
 
/** Deprecated.  Arrays are not supported.
    Reflects a fixed-size array type.
    The type class of this description is TypeClass_ARRAY.

    @deprecated
*/
published interface XArrayTypeDescription: com::sun::star::reflection::XTypeDescription
{ 
    /** Returns the element type of the array.
        
        @return
                element type of the array
    */
    com::sun::star::reflection::XTypeDescription getType(); 
    
    /** Returns the number of dimensions of the array.
        
        @return
                dimension of the array
    */
    long getNumberOfDimensions(); 
    
    /** Returns dimensions of array (same length as getNumberOfDimensions()).
        
        @return
                dimensions of array
    */
    sequence< long > getDimensions();
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
