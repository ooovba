/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XEventListener.idl,v $
 * $Revision: 1.14 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_lang_XEventListener_idl__ 
#define __com_sun_star_lang_XEventListener_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_lang_EventObject_idl__ 
#include <com/sun/star/lang/EventObject.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module lang {  
 
//============================================================================= 
 
// DocMerge from xml: interface com::sun::star::lang::XEventListener
/** base interface for all event listeners interfaces.
 */
published interface XEventListener: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: method com::sun::star::lang::XEventListener::disposing
    /** gets called when the broadcaster is about to be disposed.
        
        <p>All listeners and all other objects, which reference the 
        broadcaster should release the reference to the source.
        No method should be invoked anymore on this object (
        including <member>XComponent::removeEventListener</member> ).
        </p>

        <p>This method is called for every listener registration
        of derived listener interfaced, not only for registrations
        at <type>XComponent</type>. </p>
     */
    void disposing( [in] com::sun::star::lang::EventObject Source ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
