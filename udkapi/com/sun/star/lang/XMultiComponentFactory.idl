/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMultiComponentFactory.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_lang_XMultiComponentFactory_idl__ 
#define __com_sun_star_lang_XMultiComponentFactory_idl__ 
 
#ifndef __com_sun_star_uno_XComponentContext_idl__
#include <com/sun/star/uno/XComponentContext.idl> 
#endif 


//=============================================================================

module com {  module sun {  module star {  module lang {

/** Factory interface for creating component instances giving a context from 
    which to retrieve deployment values.
    
    @see XInitialization 
*/
published interface XMultiComponentFactory : com::sun::star::uno::XInterface
{ 
    /** Creates an instance of a component which supports the
        services specified by the factory.
        
        @param aServiceSpecifier
               service name
        @param Context
               context the component instance gets its deployment values from
        @return
                component instance
    */
    com::sun::star::uno::XInterface createInstanceWithContext(
        [in] string aServiceSpecifier,
        [in] com::sun::star::uno::XComponentContext Context )
        raises (com::sun::star::uno::Exception);
    
    /** Creates an instance of a component which supports the
        services specified by the factory, and initializes the new instance
        with the given arguments and context.
        
        @param ServiceSpecifier
               service name
        @param Arguments
               arguments
        @param Context
               context the component instance gets its deployment values from
        @return
                component instance
    */
    com::sun::star::uno::XInterface createInstanceWithArgumentsAndContext(
        [in] string ServiceSpecifier, 
        [in] sequence<any> Arguments,
        [in] com::sun::star::uno::XComponentContext Context )
        raises (com::sun::star::uno::Exception);
    
    /** Gets the names of all supported services.
        
        @returns 
                 sequence of all service names
    */
    sequence< string > getAvailableServiceNames();
}; 
 
}; }; }; };  
 
#endif 
