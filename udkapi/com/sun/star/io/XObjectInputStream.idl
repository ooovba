/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XObjectInputStream.idl,v $
 * $Revision: 1.13 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_io_XObjectInputStream_idl__ 
#define __com_sun_star_io_XObjectInputStream_idl__ 
 
#ifndef __com_sun_star_io_XDataInputStream_idl__ 
#include <com/sun/star/io/XDataInputStream.idl> 
#endif 
 
#ifndef __com_sun_star_io_XPersistObject_idl__ 
#include <com/sun/star/io/XPersistObject.idl> 
#endif 
 
#ifndef __com_sun_star_io_IOException_idl__ 
#include <com/sun/star/io/IOException.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module io {  
 
//============================================================================= 
 
/** reads XPersistObject implementations from a stream
 */
published interface XObjectInputStream: com::sun::star::io::XDataInputStream
{ 
    //------------------------------------------------------------------------- 
     
    /** reads an object from the stream. In general, it
        reads the service name, instantiaties the object and
        calls read on the XPersistObject interface with itself
        as argument.
     */
    com::sun::star::io::XPersistObject readObject() 
            raises( com::sun::star::io::IOException ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
