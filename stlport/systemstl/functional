/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: functional,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SYSTEM_STL_FUNCTIONAL
#define SYSTEM_STL_FUNCTIONAL

#ifdef GCC
# ifdef __MINGW32__
#  define _SYSTEM_STL_MAKE_HEADER(path,header) <path/header>
#  include _SYSTEM_STL_MAKE_HEADER(GXX_INCLUDE_PATH,functional)
# else
#  include <ext/../functional>
# endif
#  include <ext/functional>

namespace std
{
	using __gnu_cxx::project1st;
	using __gnu_cxx::project2nd;
	using __gnu_cxx::select1st;
	using __gnu_cxx::select2nd;
	using __gnu_cxx::compose1;
	using __gnu_cxx::compose2;
	using __gnu_cxx::unary_compose;
	using __gnu_cxx::binary_compose;
# ifndef __GXX_EXPERIMENTAL_CXX0X__
	using __gnu_cxx::identity;
	using __gnu_cxx::mem_fun1;
	using __gnu_cxx::mem_fun1_ref;
# endif
}

#else
# error UNSUPPORTED COMPILER
#endif

#endif
/* vi:set tabstop=4 shiftwidth=4 expandtab: */
