/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Descriptor.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbcx_Descriptor_idl__ 
#define __com_sun_star_sdbcx_Descriptor_idl__ 
 
#ifndef __com_sun_star_beans_XPropertySet_idl__ 
#include <com/sun/star/beans/XPropertySet.idl> 
#endif 
 
 module com {  module sun {  module star {  module sdbcx { 
 
 
/** is used to create a new object within a database.
    
    <p>
    A descriptor is commonly created by the container of a specific object, such as, tables or views.
    After the creation of the descriptor the properties have to be filled.
    Afterwards, you append the descriptor to the container and the container creates a new object based
    on the informations of the descriptor. The descriptor can be used to create serveral objects.
    </p>
    <p>
    A descriptor containes at least the informations of the name of an object.
    </p>
    @see com::sun::star::sdbcx::XAppend
 */
published service Descriptor
{ 
    // gives access to the properties.
    interface com::sun::star::beans::XPropertySet; 
 
     
    /** is the name for the object to create.
     */
    [property] string Name; 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*=========================================================================== 
===========================================================================*/ 
#endif 
