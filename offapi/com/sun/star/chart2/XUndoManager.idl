/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XUndoManager.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart2_XUndoManager_idl
#define com_sun_star_chart2_XUndoManager_idl

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/frame/XModel.idl>
#include <com/sun/star/beans/PropertyValue.idl>

module com
{
module sun
{
module star
{
module chart2
{

/** An interface for undo functionality based on passing frame::XModel
    objects.
 */
interface XUndoManager : ::com::sun::star::uno::XInterface
{
    /** call this before you change the xCurrentModel
     */
    void preAction( [in] ::com::sun::star::frame::XModel xModelBeforeChange );

    /** call this before you change the xCurrentModel. You can pass
        parameters to refine the undo action.
     */
    void preActionWithArguments( [in] ::com::sun::star::frame::XModel xModelBeforeChange,
                                 [in] sequence< ::com::sun::star::beans::PropertyValue > aArguments );

    /** call this after you successfully did changes to your current model
     */
    void postAction( [in] string aUndoText );
    /** call this if you aborted the current action.
     */
    void cancelAction();

    /** same as cancelAction() but restores the given model to the
        state set in preAction.  This is useful for cancellation in
        live-preview dialogs.
    */
    void cancelActionWithUndo( [inout] ::com::sun::star::frame::XModel xModelToRestore );

    /** give the current model to be put into the redo-stack
     */
    void undo( [inout] ::com::sun::star::frame::XModel xCurrentModel );

    /** give the current model to be put into the undo-stack
     */
    void redo( [inout] ::com::sun::star::frame::XModel xCurrentModel );

    /** @return <TRUE/> if the undo stack is not empty, i.e. a call to undo() will succeed
     */
    boolean undoPossible();

    /** @return <TRUE/> if the redo stack is not empty, i.e. a call to redo() will succeed
     */
    boolean redoPossible();

    /** Retrieves the undo string for the most recent undo step
     */
    string getCurrentUndoString();

    /** Retrieves the redo string for the most recent undo step
     */
    string getCurrentRedoString();

    /** Retrieves the undo strings of all stored undo actions in
        chronological order starting with the most recent.
     */
    sequence< string > getAllUndoStrings();

    /** Retrieves the redo strings of all stored undo actions in
        chronological order starting with the most recent.
    */
    sequence< string > getAllRedoStrings();
};

} ; // chart2
} ; // com
} ; // sun
} ; // star

#endif
