/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XLabeled.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart2_XLabeled_idl
#define com_sun_star_chart2_XLabeled_idl

#ifndef __com_sun_star_frame_XModel_idl__
#include <com/sun/star/frame/XModel.idl>
#endif

#ifndef com_sun_star_chart2_XTitle_idl
#include <com/sun/star/chart2/XTitle.idl>
#endif

#ifndef __com_sun_star_drawing_RectanglePoint_idl__
#include <com/sun/star/drawing/RectanglePoint.idl>
#endif

module com
{
module sun
{
module star
{
module chart2
{

/**
  */
interface XLabeled
{
    /**
     */
    void setLabel( [in] XTitle xTitle );

    /**
     */
    XTitle getLabel();

    /**
     */
    void setOwnAnchor( [in] ::com::sun::star::drawing::RectanglePoint aAnchorPoint );

    /**
     */
    ::com::sun::star::drawing::RectanglePoint getOwnAnchor();

    /**
     */
    void setLabelAnchor( [in] ::com::sun::star::drawing::RectanglePoint aAnchorPoint );

    /**
     */
    ::com::sun::star::drawing::RectanglePoint getLabelAnchor();

    /**
     */
    void setOffset( [in] sequence< double > aOffsetVector );

    /**
     */
    sequence< double > getOffset();
};

} ; // chart2
} ; // com
} ; // sun
} ; // star

#endif
