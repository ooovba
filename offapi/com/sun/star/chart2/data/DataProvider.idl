/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DataProvider.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart_data_DataProvider_idl
#define com_sun_star_chart_data_DataProvider_idl

#include <com/sun/star/chart2/data/XDataProvider.idl>
#include <com/sun/star/chart2/data/XRangeXMLConversion.idl>

module com
{
module sun
{
module star
{
module chart2
{
module data
{

/**
 */
service DataProvider
{
    /** For accessing data a component provides for being used by
        charts.
     */
    interface ::com::sun::star::chart2::data::XDataProvider;

    /** allows you to convert the ranges a data provider deals with
        internally into valid XML.
     */
    [optional] interface ::com::sun::star::chart2::data::XRangeXMLConversion;

    /** If set to false <FALSE/>, values from hidden cells are not returned.
     */
    [optional, property] boolean                  IncludeHiddenCells;
};

} ; // data
} ; // chart2
} ; // com
} ; // sun
} ; // star

#endif
