/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: UnsupportedOverwriteRequest.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_task_UnsupportedOverwriteRequest_idl__
#define __com_sun_star_task_UnsupportedOverwriteRequest_idl__

#ifndef __com_sun_star_task_ClassifiedInteractionRequest_idl__
#include <com/sun/star/task/ClassifiedInteractionRequest.idl>
#endif


//=============================================================================

module com { module sun { module star { module task { 

//=============================================================================
/** this request is used in case a content can't keep files from overwriting

    <P>
    It is supported by <type>InteractionHandler</type> service, and can
    be used in case a content can not keep files from overwriting and
    user specifies to do so. Continuations for using with
    the mentioned service are Abort and Approve.
    </P>

    @since OOo 1.1.2
*/
published exception UnsupportedOverwriteRequest: ClassifiedInteractionRequest
{
    /** the name of the target that might be overwritten, can be empty.
    */
    string Name;
};

//=============================================================================

}; }; }; }; 

#endif

