/*************************************************************************
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
* 
* Copyright 2008 by Sun Microsystems, Inc.
*
* OpenOffice.org - a multi-platform office productivity suite
*
* $RCSfile: code,v $
*
* $Revision: 1.3 $
*
* This file is part of OpenOffice.org.
*
* OpenOffice.org is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* OpenOffice.org is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details
* (a copy is included in the LICENSE file that accompanied this code).
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with OpenOffice.org.  If not, see
* <http://www.openoffice.org/license.html>
* for a copy of the LGPLv3 License.
************************************************************************/

#ifndef __com_sun_star_sdb_XRowSetChangeListener_idl__
#define __com_sun_star_sdb_XRowSetChangeListener_idl__

#include <com/sun/star/lang/XEventListener.idl>

//=============================================================================

module com { module sun { module star { module sdb { 

//=============================================================================

/** is implemented by components which want to be notified when the <code>RowSet</code> supplied
    by a <type>XRowSetSupplier</type> changes.

    @see XRowSetChangeBroadcaster
    @see XRowSetSupplier
    @since OOo 3.2
 */
    interface XRowSetChangeListener : ::com::sun::star::lang::XEventListener
{
    /** notifies the listener that the <code>RowSet</code> associated with a <type>XRowSetSupplier</type>
        has changed.
    */
    [oneway] void onRowSetChanged( [in] ::com::sun::star::lang::EventObject i_Event );
};

//=============================================================================

}; }; }; }; 

//=============================================================================

#endif
