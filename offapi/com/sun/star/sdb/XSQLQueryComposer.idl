/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSQLQueryComposer.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdb_XSQLQueryComposer_idl__ 
#define __com_sun_star_sdb_XSQLQueryComposer_idl__ 
 
#ifndef __com_sun_star_beans_XPropertySet_idl__ 
#include <com/sun/star/beans/XPropertySet.idl> 
#endif 
 
#ifndef __com_sun_star_beans_PropertyValue_idl__ 
#include <com/sun/star/beans/PropertyValue.idl> 
#endif 
 
#ifndef __com_sun_star_sdbc_SQLException_idl__ 
#include <com/sun/star/sdbc/SQLException.idl> 
#endif 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module sdb { 
 
//============================================================================= 
 
/** should be provided by a tool which simplifies the handling with SQL select statements.
    
    <p>
    The interface can be used for composing SELECT statements without knowing the
    structure of the used query.
    </p>
 */
published interface XSQLQueryComposer: com::sun::star::uno::XInterface
{ 
     
    /** returns the query used for composing.
        @returns
            the query
     */
    string getQuery(); 
    //------------------------------------------------------------------------- 
     
    /** sets a new query for the composer, which may be expanded by filters
        and sort criteria.
        @param command
            the command to set
        @throws com::sun::star::sdbc::SQLException 
            if a database access error occurs.
     */
    void setQuery([in] string command ) 
            raises (com::sun::star::sdbc::SQLException); 
    //------------------------------------------------------------------------- 
     
    /** returns the query composed with filters and sort criterias.
        @returns
            the composed query
     */
    string getComposedQuery(); 
    //------------------------------------------------------------------------- 
     
    /** returns the currently used filter.
        
        <p>
        The filter criteria returned is part of the where condition of the
        select command, but it does not contain the where token.
        </p>
        @returns
            the filter
     */
    string getFilter(); 
    //------------------------------------------------------------------------- 
     
    /** returns the currently used filter. 
             <p>
             The filter criteria is split into levels. Each level represents the 
             OR criterias. Within each level, the filters are provided as an AND criteria 
             with the name of the column and the filter condition. The filter condition 
             is of type string.
             </p>
        @returns
            the structured filter
     */
    sequence< sequence<com::sun::star::beans::PropertyValue> > 
        getStructuredFilter(); 
    //------------------------------------------------------------------------- 
     
    /** returns the currently used sort order.
        
        
        <p>
        The order criteria returned is part of the ORDER BY clause of the
        select command, but it does not contain the ORDER BY keyword .
        </p>
        @returns
            the order
     */
    string getOrder(); 
    //------------------------------------------------------------------------- 
     
    /** appends a new filter condition by a 
        <type scope="com::sun::star::sdb">DataColumn</type>
        providing the name and the value for the filter.
        @param column
            the column which is used to create a filter
        @throws com::sun::star::sdbc::SQLException  
            if a database access error occurs.
     */
    void appendFilterByColumn([in] com::sun::star::beans::XPropertySet column) 
            raises (com::sun::star::sdbc::SQLException); 
    //------------------------------------------------------------------------- 
     
    /** appends an additional part to the sort order criteria of the select
        statement.
        @param column
            the column which is used to create a order part
        @param	ascending
            <TRUE/> when the order should be ascending, otherwise <FALSE/>
        @throws com::sun::star::sdbc::SQLException  
            if a database access error occurs.
     */
    void appendOrderByColumn([in] com::sun::star::beans::XPropertySet column, 
                              [in] boolean ascending) 
            raises (com::sun::star::sdbc::SQLException); 
    //------------------------------------------------------------------------- 
     
    /** makes it possible to set a filter condition	for the query.
        @param filter
            the filter to set
        @throws com::sun::star::sdbc::SQLException  
            if a database access error occurs.
     */
    void setFilter([in] string filter) 
            raises (com::sun::star::sdbc::SQLException); 
    //------------------------------------------------------------------------- 
     
    /** makes it possibile to set a sort condition for the query.
        @param order
            the order part to set
        @throws com::sun::star::sdbc::SQLException  
            if a database access error occurs.
     */
    void setOrder([in] string order) 
            raises (com::sun::star::sdbc::SQLException); 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
