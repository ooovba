/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DatabaseAccessContext.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdb_DatabaseAccessContext_idl__ 
#define __com_sun_star_sdb_DatabaseAccessContext_idl__ 
 
#ifndef __com_sun_star_container_XEnumerationAccess_idl__ 
#include <com/sun/star/container/XEnumerationAccess.idl> 
#endif 
 
#ifndef __com_sun_star_container_XNameAccess_idl__ 
#include <com/sun/star/container/XNameAccess.idl> 
#endif 
 
#ifndef __com_sun_star_util_XLocalizedAliases_idl__ 
#include <com/sun/star/util/XLocalizedAliases.idl> 
#endif 
 
#ifndef __com_sun_star_lang_XLocalizable_idl__ 
#include <com/sun/star/lang/XLocalizable.idl> 
#endif 
 
 module com {  module sun {  module star {  module sdb { 
 
/** is the context for data access beans. It allows to register aliases for database
    access beans. It is possible to have different aliases for different locales.
    
    
    <p>
    A DatabaseContext stores an alias for the URL of a database access component 
    for a given locale. It is also allowed to work with a default locale. This is useful
    in connection with Enumeration or NameAccess to the context. In common use, the
    default language is set during the initialization of the component.
    </p>
    <p>
    The service also provides a default handling for locales, where an alias isn't
    set. The first time an alias is registered for a programmatic name, the alias
    becomes the default for all other known locales.
    
    </p>@see com::sun::star::util::XLocalizedAliases

    @deprecated
 */
published service DatabaseAccessContext
{ 
    /** Enumeration on all registered data sources for a default locale.
     */
    interface com::sun::star::container::XEnumerationAccess; 
 
    /** NameAccess on all registered data sources for a default locale.
     */
    interface com::sun::star::container::XNameAccess; 
 
    /** Interface for registering aliases for data sources.
     */
    interface com::sun::star::util::XLocalizedAliases; 
 
    /** Interface for setting and retrieving the default language.
     */
    interface com::sun::star::lang::XLocalizable; 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*=========================================================================== 
===========================================================================*/ 
#endif 
