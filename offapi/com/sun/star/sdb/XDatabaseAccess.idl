/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDatabaseAccess.idl,v $
 * $Revision: 1.13 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdb_XDatabaseAccess_idl__ 
#define __com_sun_star_sdb_XDatabaseAccess_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_sdbc_XDataSource_idl__ 
#include <com/sun/star/sdbc/XDataSource.idl> 
#endif 
 
#ifndef __com_sun_star_sdbc_SQLException_idl__ 
#include <com/sun/star/sdbc/SQLException.idl> 
#endif 
 
 module com {  module sun {  module star {  module task { 
 published interface XInteractionHandler; 
};};};}; 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module sdb { 
 
 published interface XDatabaseAccessListener; 
 
//============================================================================= 
 
/** is used to connect to a data access bean. A data access bean represents a
    database connection and provides additional information related to the connection
    such as forms, reports, or queries.
 */
published interface XDatabaseAccess: com::sun::star::sdbc::XDataSource
{ 
     
    /** indicates that connections already exist.
        @returns
            <TRUE/> if so
     */
    boolean hasConnections(); 
    //------------------------------------------------------------------------- 
      
    /** attempts to establish a database connection, that can not be shared with
        other components. This should be used for transaction processing.
        @param user
            the user name
        @param password
            the password
        @returns
            an isolated connection object
        @throws com::sun::star::sdbc::SQLException 
            if a database access error occurs.
        @see com::sun::star::sdbc::XConnection
     */
    com::sun::star::sdbc::XConnection getIsolatedConnection([in]string user, [in]string password) 
      raises (com::sun::star::sdbc::SQLException); 
    //------------------------------------------------------------------------- 
     
    /** closes the all connections to database. This request could be aborted by
        listeners of the component.
        @throws com::sun::star::sdbc::SQLException 
            if a database access error occurs.
     */
    boolean suspendConnections() raises (com::sun::star::sdbc::SQLException); 
    //------------------------------------------------------------------------- 
     
    /** adds the specified listener to receive the events "connectionChanged",
        "approveConnectionClose", and "connectionClosing".
        @param	listener
            the listener to append
        @see com::sun::star::sdb::XDatabaseAccessListener
     */
    [oneway] void addDatabaseAccessListener([in]XDatabaseAccessListener listener); 
    //------------------------------------------------------------------------- 
     
    /** removes the specified listener.
        @param	listener
            the listener to append
        @see com::sun::star::sdb::XDatabaseAccessListener
     */
    [oneway] void removeDatabaseAccessListener( 
            [in]XDatabaseAccessListener listener); 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
