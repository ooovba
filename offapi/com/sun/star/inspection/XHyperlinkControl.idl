/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XHyperlinkControl.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_inspection_XHyperlinkControl_idl__
#define __com_sun_star_inspection_XHyperlinkControl_idl__

#ifndef __com_sun_star_inspection_XPropertyControl_idl__
#include <com/sun/star/inspection/XPropertyControl.idl>
#endif
#ifndef __com_sun_star_awt_XActionListener_idl__
#include <com/sun/star/awt/XActionListener.idl>
#endif

//=============================================================================
module com {  module sun {  module star {  module inspection {

//-----------------------------------------------------------------------------
/** defines the interface for an <type>XPropertyControl</type> which displays its value
    in a hyperlink-like way

    <p>Hyperlink controls exchange their value (<member>XPropertyControl::Value</member>) as strings.</p>

    @since OOo 2.0.3
*/
interface XHyperlinkControl : XPropertyControl
{
    /** adds a listener which will be notified when the user clicked the hyperlink text in the control
        @param listener
            the listener to notify of hyperlink clicks
    */
    void addActionListener( [in] com::sun::star::awt::XActionListener listener );

    /** removes a listener which was previously added via <member>addActionListener</member>
        @param listener
            the listener to revoke
    */
    void removeActionListener( [in] com::sun::star::awt::XActionListener listener );
};

//=============================================================================

}; }; }; };

#endif

