/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: PropertyLineElement.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_inspection_PropertyLineElement_idl__
#define __com_sun_star_inspection_PropertyLineElement_idl__

//=============================================================================
module com {  module sun {  module star {  module inspection {

//-----------------------------------------------------------------------------
/** describes elements of a single line in an object inspector, used to represent a
    single property

    @see XPropertyHandler::describePropertyLine
    @see LineDescriptor

    @since OOo 2.0.3
*/
constants PropertyLineElement
{
    /// specifies the input control in a group of controls related to a single property
    const short InputControl    = 0x01;
    /// specifies the primary button (if present) in a group of controls related to a single property
    const short PrimaryButton   = 0x02;
    /// specifies the secondary button (if present) in a group of controls related to a single property
    const short SecondaryButton = 0x04;

    /// specifies all elements
    const short All             = 0xFF;
};

//=============================================================================

}; }; }; };

#endif

