/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DefaultHelpProvider.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_inspection_DefaultHelpProvider_idl__
#define __com_sun_star_inspection_DefaultHelpProvider_idl__

#ifndef __com_sun_star_lang_IllegalArgumentException_idl__
#include <com/sun/star/lang/IllegalArgumentException.idl>
#endif

//=============================================================================

module com { module sun { module star { module inspection { 

interface XObjectInspectorUI;

//=============================================================================

/** implements a component which can default-fill the help section of an
    <type>ObjectInspector</type>.

    <p>The component registers a <type>XPropertyControlObserver</type> at an
    <type>XObjectInspectoryUI</type> interface. Whenever it then is notified
    of a <type>XPropertyControl</type> getting the focus, it will try to deduce
    the extended help text of this control's window, and set this help text at the
    object inspector's help section.</p>
 */
service DefaultHelpProvider : com::sun::star::uno::XInterface
{
    /** creates a help provider instance
        @param InspectorUI
            provides access to the UI of the ObjectInspector which should be
            observed. Must not be <NULL/>.
        @throws ::com::sun::star::lang::IllegalArgumentException
            if the given inspector UI is <NULL/>.
    */
    create( [in] XObjectInspectorUI InspectorUI )
        raises ( ::com::sun::star::lang::IllegalArgumentException );
};

//=============================================================================

}; }; }; };

//=============================================================================

#endif

