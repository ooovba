/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XFunctions.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_report_XFunctions_idl__
#define __com_sun_star_report_XFunctions_idl__

#ifndef __com_sun_star_container_XChild_idl__ 
#include <com/sun/star/container/XChild.idl> 
#endif 
#ifndef __com_sun_star_container_XContainer_idl__ 
#include <com/sun/star/container/XContainer.idl> 
#endif 
#ifndef __com_sun_star_container_XIndexContainer_idl__ 
#include <com/sun/star/container/XIndexContainer.idl> 
#endif 
#ifndef __com_sun_star_lang_XComponent_idl__ 
#include <com/sun/star/lang/XComponent.idl> 
#endif
//=============================================================================

 module com {  module sun {  module star {  module report { 
interface XReportDefinition;
interface XFunction;
//=============================================================================

/** This interface specifies the functions collections of the report definition or a group.
    @see XFunction
    @see XReportDefinition
 */
interface XFunctions
{
    /** allows to register listeners to be notified of changes in the container.
    */
    interface com::sun::star::container::XContainer; 
    /** gives access to the group elements.
        The elements are of type <type>XFunction</type>.
    */
    interface com::sun::star::container::XIndexContainer;

    /** allows the navigation to the report or group object.
        The method setParent from <type>XChild</type> is not supported and will throw an exception when called.
    */
    interface com::sun::star::container::XChild;

    /** allows life-time control of the functions instance.
     */
    interface com::sun::star::lang::XComponent;

    /** factory method for <type>XFunction</type>.
    */
    com::sun::star::report::XFunction createFunction();
};

//=============================================================================
}; }; }; };

#endif
