/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XPluginInstancePeer.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_mozilla_XPluginInstancePeer_idl__ 
#define __com_sun_star_mozilla_XPluginInstancePeer_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_io_XActiveDataSource_idl__ 
#include <com/sun/star/io/XActiveDataSource.idl> 
#endif 
 
#ifndef __com_sun_star_io_XInputStream_idl__ 
#include <com/sun/star/io/XInputStream.idl> 
#endif 
 
#ifndef __com_sun_star_lang_XMultiServiceFactory_idl__ 
#include <com/sun/star/lang/XMultiServiceFactory.idl> 
#endif 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module mozilla { 
 
//============================================================================= 
 
 published interface XPluginInstanceNotifySink; 
 
//============================================================================= 
 
 
// DocMerge from xml: interface com::sun::star::mozilla::XPluginInstancePeer
/** Allows to communicate with a plugin from the office side.
    This interface is oriented for communication with browsers plugins.
 */
published interface XPluginInstancePeer: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
 
    // DocMerge from xml: method com::sun::star::mozilla::XPluginInstancePeer::setWindowSize
    /** Alters the plugin's window size in the browser window.

        @param width	[in]: the new window width
        @param height	[in]: the new window height

        @return <CODE>TRUE</CODE> on success
     */
    boolean setWindowSize( [in] long width, [in] long heigth ); 
 
    //------------------------------------------------------------------------- 
     
 
    // DocMerge from xml: method com::sun::star::mozilla::XPluginInstancePeer::showStatusMessage
    /** Show status / hint message in browser's message area.

        @param message	[in]: the string to be displayed
     */
    [oneway] void showStatusMessage( [in] string message ); 
 
    //------------------------------------------------------------------------- 
     
 
    // DocMerge from xml: method com::sun::star::mozilla::XPluginInstancePeer::enableScripting
    /** Indicates to the plugin that the document was loaded successfully and scripting
        interfaces are now available.

        @param document			[in]: the active document
        @param servicemanager	[in]: the office servicemanager

     */
    [oneway] void enableScripting( [in] com::sun::star::uno::XInterface document, [in] com::sun::star::lang::XMultiServiceFactory servicemanager ); 
 
    //------------------------------------------------------------------------- 
     
 
    // DocMerge from xml: method com::sun::star::mozilla::XPluginInstancePeer::newStream
    /** Creates a new stream of data produced by the plug-in and consumed by
        the browser.

        @param MIMEDesc	[in]:	the MIME type of the plug-in to create
        @param target	[in]:	the name of the target window or frame (supports _blank, _self)
        @param data		[in]:	on success the outputstream will be	associated with this instance

     */
    [oneway] void newStream( 
        [in] string MIMEDesc, 
        [in] string target, 
        [in] com::sun::star::io::XActiveDataSource data 
    ); 
 
    //------------------------------------------------------------------------- 
     
 
    // DocMerge from xml: method com::sun::star::mozilla::XPluginInstancePeer::getURL
    /** Fetches an URL into the target window. The parameters and their meaning map to the 
        corresponding Netscape-API call.
        
        @param aURL				[in]: the URL to be fetched
        @param target			[in]: the name of the target window or frame (supports _blank, _self)
        @param alternativeHost	[in]: alternativeHost 
        @param referrer			[in]: referrer
        @param sink				[in]: the sink is notified on success

     */
    [oneway] void getURL ( 
        [in] string aURL, 
        [in] string target, 
        [in] string alternativeHost, 
        [in] string referrer, 
        [in] XPluginInstanceNotifySink sink 
    ); 
 
    //------------------------------------------------------------------------- 
     
 
    // DocMerge from xml: method com::sun::star::mozilla::XPluginInstancePeer::postURL
    /** Posts to a URL with post data and/or post headers. The parameters and their meaning 
        map to the corresponding Netscape-API call.

        @param aURL				[in]: the URL to be posted to
        @param postData			[in]: the data to be posted
        @param target			[in]: the name of the target window or frame (supports _blank, _self)
        @param alternativeHost	[in]: alternativeHost 
        @param referrer			[in]: referrer
        @param postHeaders		[in]: the header to be posted
        @param sink				[in]: the sink is notified on success

     */
    [oneway] void postURL ( 
        [in] string aURL, 
        [in] com::sun::star::io::XInputStream postData, 
        [in] string target, 
        [in] string alternativeHost, 
        [in] string referrer, 
        [in] com::sun::star::io::XInputStream postHeaders, 
        [in] XPluginInstanceNotifySink sink 
    ); 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
#endif 
