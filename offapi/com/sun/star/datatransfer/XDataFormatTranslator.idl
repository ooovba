/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDataFormatTranslator.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#ifndef __com_sun_star_datatransfer_XDataFormatTranslator_idl__
#define __com_sun_star_datatransfer_XDataFormatTranslator_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_datatransfer_DataFlavor_idl__
#include <com/sun/star/datatransfer/DataFlavor.idl>
#endif

module com { module sun { module star { module datatransfer {

//=============================================================================
/** Interface to be implemented by objects used to translate a <type>DataFlavor</type> to 
    a system dependent data transfer type and vice versa. 

    <p>Different platforms use different types to describe data formats available 
    during data exchange operations like clipboard or drag&drop. Windows for instance
    uses integer values to describe an available clipboard or drag&drop format, Unix 
    X11 uses so called Atoms etc.</p>	 
*/

published interface XDataFormatTranslator : com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** Converts a <type>DataFlavor</type> to system dependend data type.

        @param aDataFlavor
        Describes the format for which a system dependent data types is requested.

        @returns
        A system dependent data transfer type for the given <type>DataFlavor</type>
        if there is one available. 
        <p>If the is no system dependent data type for a given <type>DataFlavor</type>
        the returned any is empty.</p>
    */
    any getSystemDataTypeFromDataFlavor( [in] DataFlavor aDataFlavor );

    //-------------------------------------------------------------------------
    /** Converts a system dependent data type to a <type>DataFlavor</type>.

        @param aSysDataType
        A system dependent data type. If aSysDataType is empty so is the returned <type>DataFlavor</type>.

        @returns		
        A <type>DataFlavor</type> for the given system dependent data transfer type.
        <p>If there is no appropriate mapping for a sytem dependent data type, the returned <type>DataFlavor</type> will be empty.</p>
    */
    DataFlavor getDataFlavorFromSystemDataType( [in] any aSysDataType );
};

}; }; }; };

#endif

