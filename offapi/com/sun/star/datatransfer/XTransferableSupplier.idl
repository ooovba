/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XTransferableSupplier.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_XTransferableSupplier_idl__
#define __com_sun_star_datatransfer_XTransferableSupplier_idl__

#ifndef __com_sun_star_datatransfer_XTransferable_idl__
#include <com/sun/star/datatransfer/XTransferable.idl>
#endif

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

//=============================================================================

module com { module sun { module star { module datatransfer {

interface XTransferableSupplier
{
    //-------------------------------------------------------------------------
    /** To get access to a transferable representation of a selected part of an object.

        @returns
        The transferable object representing the selection inside the supplying object

        @see com::sun::star::datatransfer::XTransferable
    */
    XTransferable getTransferable();

    //-------------------------------------------------------------------------
    /** Hands over a transferable object that shall be inserted.

        @param xTrans
        The transferable object to be inserted
        <p>A NULL value is not allowed.</p>

        @throws com::sun::star::datatransfer::UnsupportedFlavorException
        if the given <type scope="com::sun::star::datatransfer">XTransferable</type>
        has no <type scope="com::sun::star::datatransfer">DataFlavor</type> or the called
        object can't handle any of the available ones.

        @see com::sun::star::datatransfer::XTransferable
    */
    void insertTransferable( [in] XTransferable xTrans )
        raises ( UnsupportedFlavorException );
};

//=============================================================================

}; }; }; };

#endif
