/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: OleDragSource.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_dnd_OleDragSource_idl__
#define __com_sun_star_datatransfer_dnd_OleDragSource_idl__

#ifndef __com_sun_star_lang_XComponent_idl__
#include <com/sun/star/lang/XComponent.idl>
#endif

#ifndef __com_sun_star_lang_XServiceInfo_idl__
#include <com/sun/star/lang/XServiceInfo.idl>
#endif

#ifndef __com_sun_star_lang_XInitialization_idl__
#include <com/sun/star/lang/XInitialization.idl>
#endif

#ifndef __com_sun_star_lang_XTypeProvider_idl__
#include <com/sun/star/lang/XTypeProvider.idl>
#endif

//=============================================================================

module com { module sun { module star { module datatransfer { module dnd {

//=============================================================================

 published interface XDragSource;

//=============================================================================
/** This service connects the Java-like UNO drag and drop protocol to the protocol
    used on window platforms. It realized the drag source.

    @see XDragSource
*/

published service OleDragSource
{
    //-------------------------------------------------------------------------
    /** Used to provide data to other applications via the Ole Drag & Drop protocol.
    */
    interface XDragSource;

    //-------------------------------------------------------------------------
    /** The service expects a byte sequence uniquely identifying the machine as 
        the first, and only, parameter. This idenifier should be checked to
        ensure that	the window ids used for creating DropTargets are valid for
        the service, that is, come from the same machine.

        <p>TODO: specify how such a machine id should look like.</p>
    
        The second parameter is a window handle of the native windows window.
        Is is passed as an unsigned long.
    */
    interface com::sun::star::lang::XInitialization;

    //-------------------------------------------------------------------------
    /** For shutdown and listener support.
    */
    interface com::sun::star::lang::XComponent;

    //-------------------------------------------------------------------------
    /** Service should always support this interface.
    */
    interface com::sun::star::lang::XServiceInfo;

    //-------------------------------------------------------------------------
    /** Service should always support this interface.
    */
    interface com::sun::star::lang::XTypeProvider;
};

//=============================================================================

}; }; }; }; };

#endif
