/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDropTargetDragContext.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_dnd_XDropTargetDragContext_idl__
#define __com_sun_star_datatransfer_dnd_XDropTargetDragContext_idl__

#ifndef __com_sun_star_datatransfer_dnd_InvalidDNDOperationException_idl__
#include <com/sun/star/datatransfer/dnd/InvalidDNDOperationException.idl>
#endif

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

//=============================================================================

module com { module sun { module star { module datatransfer { module dnd {

//=============================================================================
/** This interface is implemented by any drop target context object.

    <p>A drop target context is created whenever the logical cursor associated 
    with a Drag and Drop operation moves within the visible geometry of a 
    window associated with a drop target. </p>
    
    <p>The drop target context provides the mechanism for a potential receiver 
    of a drop operation to both provide the end user with the appropriate drag 
    under feedback and effect the subsequent data transfer, if appropriate. </p>
*/

published interface XDropTargetDragContext: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** Accept the Drag. 

        <p>This method should be called from the methods of <type>XDropTargetListener</type> 
        <ul>
            <li><member>XDropTargetListener::dragEnter()</member></li>
            <li><member>XDropTargetListener::dragOver()</member></li>
            <li><member>XDropTargetListener::dragActionChanged()</member></li>
        </ul>
        if the implementation wishes to accept the drag operation with the specified
        action.</p>

        @param dragOperation
        The operation accepted by the target.

        @see DNDConstants
        @see DropTargetDragEvent
    */
           
    [oneway] void acceptDrag( [in] byte dragOperation );

    //-------------------------------------------------------------------------
    /** Reject the drag as a result of examining the available 
        <type scope="com::sun::star::datatransfer">DataFlavor</type> types
        received in the <member scope="com::sun::star::datatransfer::dnd">XDropTargetListener::dragEnter()</member> method.
    */

    [oneway] void rejectDrag();
};

//=============================================================================

}; }; }; }; };

#endif
