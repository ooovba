/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDragGestureListener.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_dnd_XDragGestureListener_idl__
#define __com_sun_star_datatransfer_dnd_XDragGestureListener_idl__

#ifndef __com_sun_star_datatransfer_dnd_DragGestureEvent_idl__
#include <com/sun/star/datatransfer/dnd/DragGestureEvent.idl>
#endif

#ifndef __com_sun_star_lang_XEventListener_idl__
#include <com/sun/star/lang/XEventListener.idl>
#endif

//=============================================================================

module com { module sun { module star { module datatransfer { module dnd {

//=============================================================================
/** This interface will be used by a <type>XDragGestureRecognizer</type> 
    when it detects a drag initiating gesture. 

    <p>The implementor of this interface is responsible for starting the drag 
    as a result of receiving such notification.</p>

*/

published interface XDragGestureListener: com::sun::star::lang::XEventListener
{
    //-------------------------------------------------------------------------
    /** A <type>XDragGestureRecognizer</type> has detected a platform-dependent 
        drag initiating gesture and is notifying this listener in order 
        for it to initiate the action for the user.

        @param dge
        The DragGestureEvent describing the gesture that has just occurred.

    */
    [oneway] void dragGestureRecognized( [in] DragGestureEvent dge );
};

//=============================================================================

}; }; }; }; };

#endif
