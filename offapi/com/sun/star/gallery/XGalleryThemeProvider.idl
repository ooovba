/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XGalleryThemeProvider.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_gallery_XGalleryThemeProvider_idl__
#define __com_sun_star_gallery_XGalleryThemeProvider_idl__

#ifndef __com_sun_star_gallery_XGalleryTheme_idl__
#include <com/sun/star/gallery/XGalleryTheme.idl>
#endif
#ifndef __com_sun_star_container_XNameAccess_idl__
#include <com/sun/star/container/XNameAccess.idl>
#endif
#ifndef __com_sun_star_container_ElementExistException_idl__
#include <com/sun/star/container/ElementExistException.idl>
#endif
#ifndef __com_sun_star_container_NoSuchElementException_idl__
#include <com/sun/star/container/NoSuchElementException.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module gallery {

//=============================================================================

/** provides access to the Gallery themes. It also allows inserting and
    removing of Gallery themes by name.

    <p>This interface extends the interface
    <type scope="com::sun::star::container">XNameAccess</type> which provides
    access to existing Gallery themes collection.</p>

    @see com::sun::star::container::XNameAccess
 */
interface XGalleryThemeProvider : com::sun::star::container::XNameAccess
{
    /** creates a new Gallery theme and adds it to the collection.

        @param ThemeName
            The name of the Gallery theme to be added to the collection.
            The name must be unique.
        
        @returns XGalleryTheme
            The created theme interface

        @see com::sun::star::container::ElementExistException
     */
    XGalleryTheme insertNewByName( [in] string ThemeName )
        raises ( com::sun::star::container::ElementExistException );

    //-------------------------------------------------------------------------

    /** deletes a Gallery theme from the collection.

        @param ThemeName
            The name of the Gallery theme to be removed. The
            theme with the given name must exist.
         
        @see com::sun::star::container::NoSuchElementException
    */
    void removeByName( [in] string ThemeName )
        raises ( com::sun::star::container::NoSuchElementException );

};

//=============================================================================

}; }; }; };

#endif
