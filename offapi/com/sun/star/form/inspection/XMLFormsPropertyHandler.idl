/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMLFormsPropertyHandler.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_inspection_XMLFormsPropertyHandler_idl__
#define __com_sun_star_form_inspection_XMLFormsPropertyHandler_idl__

#ifndef __com_sun_star_inspection_XPropertyHandler_idl__
#include <com/sun/star/inspection/XPropertyHandler.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module form { module inspection {

//=============================================================================

/** implements a property handler for use with an <type scope="com::sun::star::inspection">ObjectInspector</type>
    which provides properties related to binding form control models to XForm bindings.

    <p>The handler introduces new properties to choose an <type scope="com::sun::star::xforms">XModel</type>
    and a <type scope="com::sun::star::xforms">Binding</type> within this model. Additionally,
    it introduces properties which reflect the different facets of the binding (e.g.
    <member scope="com::sun::star::xforms">Binding::BindingExpression</member>), so they can be changed
    directly in the <type scope="com::sun::star::inspection">ObjectInspector</type> as if they were a
    property of the form component which is being inspected.</p>

    <p>The handler expects a value named "ContextDocument" in the context in which it is created.
    That is, the <type scope="com::sun::star::uno">XComponentContext</type> used for creating the
    <type>CellBindingPropertyHandler</type> is examined for a value with this name. If the object in this
    value denotes a XML form document (indicated by supporting the <type scope="com::sun::star::xforms">XFormsSupplier</type>
    interface), this document is used to do XML binding related work.</p>

    @see com::sun::star::inspection::XPropertyHandler
    @see com::sun::star::form::binding::BindableControlModel
    @see com::sun::star::form::binding::ValueBinding
    @see com::sun::star::xforms::Binding
    @see com::sun::star::uno::XComponentContext::getValueByName
*/
service XMLFormsPropertyHandler
{
    interface com::sun::star::inspection::XPropertyHandler;
};

//=============================================================================

}; }; }; }; };

#endif

