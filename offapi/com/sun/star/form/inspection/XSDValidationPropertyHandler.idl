/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSDValidationPropertyHandler.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_inspection_XSDValidationPropertyHandler_idl__
#define __com_sun_star_form_inspection_XSDValidationPropertyHandler_idl__

#ifndef __com_sun_star_inspection_XPropertyHandler_idl__
#include <com/sun/star/inspection/XPropertyHandler.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module form { module inspection {

//=============================================================================

/** implements a property handler for use with an <type scope="com::sun::star::inspection">ObjectInspector</type>
    which provides properties related to binding form control models to XForm bindings and validating
    the form control content.

    <p>By using an <type>XMLFormsPropertyHandler</type>, an <type scope="com::sun::star::inspection">ObjectInspector</type>
    can be used to bind form components to <type scope="com::sun::star::xforms">Binding</type> instances.
    Since those instances also support validating form control content (by supporting an
    <type scope="com::sun::star::form::validation">XValidator</type> interface), it seems reasonable to
    edit those validate-related properties (like the XSD data type to validate against) in the
    <type scope="com::sun::star::inspection">ObjectInspector</type>, too. This is what an <type>XSDValidationPropertyHandler</type>
    is good for.</p>

    <p>The handler expects a value named "ContextDocument" in the context in which it is created.
    That is, the <type scope="com::sun::star::uno">XComponentContext</type> used for creating the
    <type>CellBindingPropertyHandler</type> is examined for a value with this name. If the object in this
    value denotes a XML form document (indicated by supporting the <type scope="com::sun::star::xforms">XFormsSupplier</type>
    interface), this document is used to do XML binding related work.</p>

    @see com::sun::star::inspection::XPropertyHandler
    @see com::sun::star::form::binding::BindableControlModel
    @see com::sun::star::form::binding::ValueBinding
    @see com::sun::star::forms::validation::ValidatableControlModel
    @see com::sun::star::xforms::Binding
    @see com::sun::star::xsd::XDataType
    @see com::sun::star::uno::XComponentContext::getValueByName
*/
service XSDValidationPropertyHandler
{
    interface com::sun::star::inspection::XPropertyHandler;
};

//=============================================================================

}; }; }; }; };

#endif

