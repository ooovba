/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XResetListener.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_XResetListener_idl__ 
#define __com_sun_star_form_XResetListener_idl__ 
 
#ifndef __com_sun_star_lang_XEventListener_idl__ 
#include <com/sun/star/lang/XEventListener.idl> 
#endif 
 
#ifndef __com_sun_star_lang_EventObject_idl__ 
#include <com/sun/star/lang/EventObject.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module form {  
 
//============================================================================= 
 
/** is the interface for receiving notificaions about reset events.
    
    <p>The listener is called if a component implementing the 
    <type>XReset</type> interface performs a reset.</br>
    Order of events:
    <ul><li>a reset is triggered on a component</li>
        <li>the component calls <member>XReset::approveReset</member> on all its listeners</li>
        <li>if all listeners approve the reset operation, the data is reset</li>
        <li>the component calls <member>XReset::resetted</member> on all its listeners</li>
    </ul>
    </p>
    
    @see      XReset
 */
published interface XResetListener: com::sun::star::lang::XEventListener
{ 
    //------------------------------------------------------------------------- 
     
    /** is invoked before a component is reset.
        
        @param rEvent
            the event happend.

        @returns
            <TRUE/> when reset was approved, <FALSE/> when the reset operation should be cancelled.
     */
    boolean approveReset( [in] com::sun::star::lang::EventObject rEvent ); 
 
    //------------------------------------------------------------------------- 
     
    /** is invoked when a component has been reset.

        @param rEvent
            the event happend.
     */
    [oneway] void resetted( [in] com::sun::star::lang::EventObject rEvent ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
