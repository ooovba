/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextField.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_control_TextField_idl__ 
#define __com_sun_star_form_control_TextField_idl__ 

#ifndef __com_sun_star_awt_UnoControlEdit_idl__ 
#include <com/sun/star/awt/UnoControlEdit.idl> 
#endif 

#ifndef __com_sun_star_form_XBoundControl_idl__ 
#include <com/sun/star/form/XBoundControl.idl> 
#endif 
#ifndef __com_sun_star_form_XChangeBroadcaster_idl__ 
#include <com/sun/star/form/XChangeBroadcaster.idl> 
#endif 

//============================================================================= 
 
 module com {  module sun {  module star {  module form {  module control { 
 
//============================================================================= 
 
/** describes a control for entering arbiotrary text which can (but not necessarily has to) be bound
    to a database field.

    <p>The model of the control has to support the <type scope="com::sun::star::form::component">TextField</type>
    service.</p>

    <p>In addition, this control can be used in HTML forms. It triggers the
    <method scope="com::sun::star::form">XSubmit::submit</method> method of the form it belongs to if
    the <em>enter</em> is pressed while it has the focus.</p>

    @see com::sun::star::awt::UnoControl
    @see com::sun::star::awt::UnoControlModel
*/
published service TextField
{
    service com::sun::star::awt::UnoControlEdit;

    interface com::sun::star::form::XBoundControl;

    /** allows broadcasts of HTML-compatible change events.

        <p>HTML-compatible means that a change event is broadcasted if and only if all of
        the following applies.
        <ul>
            <li>the control loses the focus</li>
            <li>the content of the control has changed, compared to the moment where it got the focus.</li>
        </ul>
        <p/>
    */
    interface com::sun::star::form::XChangeBroadcaster;
};

//============================================================================= 
 
}; }; }; }; }; 
 
/*============================================================================= 
 
=============================================================================*/ 

#endif 


