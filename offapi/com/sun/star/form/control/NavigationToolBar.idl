/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NavigationToolBar.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_control_NavigationToolBar_idl__
#define __com_sun_star_form_control_NavigationToolBar_idl__

#ifndef __com_sun_star_awt_UnoControl_idl__
#include <com/sun/star/awt/UnoControl.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module form {  module control {

//=============================================================================

/** This service specifies the model for control which provides controller
    functionality for a <type scope="com::sun::star::form::component">DataForm</type>, such as navigating or filtering
    the form.

    @see com::sun::star::form::component::NavigationToolBar
*/
service NavigationToolBar
{
    service com::sun::star::awt::UnoControl;
};

//=============================================================================

}; }; }; }; };


#endif
