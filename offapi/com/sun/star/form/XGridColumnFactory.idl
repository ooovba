/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XGridColumnFactory.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_XGridColumnFactory_idl__ 
#define __com_sun_star_form_XGridColumnFactory_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_beans_XPropertySet_idl__ 
#include <com/sun/star/beans/XPropertySet.idl> 
#endif 
 
#ifndef __com_sun_star_lang_IllegalArgumentException_idl__ 
#include <com/sun/star/lang/IllegalArgumentException.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module form { 
 
//============================================================================= 
 
/** allows to create columns to be added into a grid control model.

    <p>Grid columns (more precise: models of grid columns) are direct children of
    the grid control model they belong to. Grid columns can't be created on a global
    service factory, instead, you need to create them on the grid, where you want to
    insert them later on.</p>

    @see com::sun::star::form::component::GridControl
 */
published interface XGridColumnFactory: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    /** creates a new column object

        @param aColumnType
            the type of column to be created

        @returns
            the new column object

        @throws com::sun::star::lang::IllegalArgumentException
            if <arg>aColumnType</arg> is not available.			
     */
    com::sun::star::beans::XPropertySet createColumn( [in] string aColumnType ) 
            raises( com::sun::star::lang::IllegalArgumentException ); 
 
    //------------------------------------------------------------------------- 
     
    /** returns a list of available column types.

        @returns
            a list of column types.
     */
    sequence<string> getColumnTypes(); 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
