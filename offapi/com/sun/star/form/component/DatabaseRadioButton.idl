/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DatabaseRadioButton.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_component_DatabaseRadioButton_idl__ 
#define __com_sun_star_form_component_DatabaseRadioButton_idl__ 
 
#ifndef __com_sun_star_form_component_RadioButton_idl__ 
#include <com/sun/star/form/component/RadioButton.idl> 
#endif 
 
#ifndef __com_sun_star_form_DataAwareControlModel_idl__ 
#include <com/sun/star/form/DataAwareControlModel.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module form {  module component {  
 
//============================================================================= 
 
/** This service specifies a radio button which is data-aware, and can be bound to a database field.
 */
published service DatabaseRadioButton
{ 
    service com::sun::star::form::component::RadioButton; 
    
    service com::sun::star::form::DataAwareControlModel; 
 
}; 
 
//============================================================================= 
 
}; }; }; }; };  
 
#endif 
