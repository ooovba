/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: HTMLForm.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_component_HTMLForm_idl__ 
#define __com_sun_star_form_component_HTMLForm_idl__ 
 
#ifndef __com_sun_star_form_component_Form_idl__ 
#include <com/sun/star/form/component/Form.idl> 
#endif 
 
#ifndef __com_sun_star_form_XReset_idl__ 
#include <com/sun/star/form/XReset.idl> 
#endif 
 
#ifndef __com_sun_star_form_XSubmit_idl__ 
#include <com/sun/star/form/XSubmit.idl> 
#endif 
 
#ifndef __com_sun_star_form_FormSubmitMethod_idl__ 
#include <com/sun/star/form/FormSubmitMethod.idl> 
#endif 
 
#ifndef __com_sun_star_form_FormSubmitEncoding_idl__ 
#include <com/sun/star/form/FormSubmitEncoding.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module form {  module component { 
 
//============================================================================= 
 
/** This service specifies the special kind of <type>Form</type>s for HTML documents.
    
    <p>An HTMLForm fulfills the specification of forms in HTML. It supplies
    the possibility of submitting or resetting the contents of a form.
    For more information on HTML forms, please see the documentation of HTML.</p>
 */
published service HTMLForm
{ 
    service com::sun::star::form::component::Form; 
    
    /** resets the control.
    */
    interface com::sun::star::form::XReset; 
    
    /** allows to submit changes.

        <p>When a form is submitted, the data contained in the form is sent 
        to the target URL as a series of name/value pairs. The name portion
        of each pair is the name of a form component as specified by its NAME
        attribute. In most cases the value portion is the value displayed 
        by the element, for example, the text displayed in a text field.</p>
    */
    interface com::sun::star::form::XSubmit; 
 
    /** describes the frame, where to open the document specified by the TargetURL.
     */
    [property] string TargetFrame; 

    //------------------------------------------------------------------------- 
     
    /** specifies the URL, which should be used for submission.
     */
    [property] string TargetURL; 
    //------------------------------------------------------------------------- 
     
    /** specifies the kind of submission.
     */
    [property] com::sun::star::form::FormSubmitMethod SubmitMethod; 
    //------------------------------------------------------------------------- 
     
    /** specifies the kind of encoding for submission.
     */
    [property] com::sun::star::form::FormSubmitEncoding SubmitEncoding; 
}; 
 
//============================================================================= 
 
}; }; }; }; }; 
 
#endif 
