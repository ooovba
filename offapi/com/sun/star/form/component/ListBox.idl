/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ListBox.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_component_ListBox_idl__ 
#define __com_sun_star_form_component_ListBox_idl__ 
 
#ifndef __com_sun_star_awt_UnoControlListBoxModel_idl__ 
#include <com/sun/star/awt/UnoControlListBoxModel.idl> 
#endif 
 
#ifndef __com_sun_star_form_FormControlModel_idl__ 
#include <com/sun/star/form/FormControlModel.idl> 
#endif 
 
#ifndef __com_sun_star_form_XReset_idl__ 
#include <com/sun/star/form/XReset.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module form {  module component { 
 
//============================================================================= 
 
/** specifies a model for a control which allows to choose in a list of alternative values.
 */
published service ListBox
{ 
    service com::sun::star::awt::UnoControlListBoxModel; 
    
    service com::sun::star::form::FormControlModel; 
    
    /** can be used to reset the control to it's default state.

        @see ListBox::DefaultSelection
    */
    interface com::sun::star::form::XReset; 
 
    /** contains the indexes of entries of the listbox, which should selected 
        by default.

        <p>This selection is used initially or for a reset.</p>

        @see com::sun::star::awt::UnoControlListBoxModel::SelectedItems
        @see com::sun::star::form::XReset
     */
    [property] sequence<short> DefaultSelection; 
 

    /** contains the listbox entries.
     */
    [property] sequence<string> ListSource; 
 
}; 
 
//============================================================================= 
 
}; }; }; }; }; 
 
#endif 
