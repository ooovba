/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ChartAxisYSupplier.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_ChartAxisYSupplier_idl__ 
#define __com_sun_star_chart_ChartAxisYSupplier_idl__ 
 
#ifndef __com_sun_star_chart_XAxisYSupplier_idl__ 
#include <com/sun/star/chart/XAxisYSupplier.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module chart {  
 
//============================================================================= 
 
/** A helper service for the y-axis.
 */
published service ChartAxisYSupplier
{ 
    /** offers access to the axis object, the title, and the grids
     */
    interface com::sun::star::chart::XAxisYSupplier; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the y-axis is shown or hidden.@see ChartAxis
     */
    [property] boolean HasYAxis; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the description of the y-axis
        is shown or hidden.
     */
    [property] boolean HasYAxisDescription; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the major grid of the y-axis is
        shown or hidden.@see ChartGrid
     */
    [property] boolean HasYAxisGrid; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the minor grid of the y-axis is
        shown or hidden.@see ChartGrid
     */
    [property] boolean HasYAxisHelpGrid; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the title of the y-axis is shown
        or hidden.@see ChartTitle
     */
    [property] boolean HasYAxisTitle; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
