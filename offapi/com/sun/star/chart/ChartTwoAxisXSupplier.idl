/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ChartTwoAxisXSupplier.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_ChartTwoAxisXSupplier_idl__ 
#define __com_sun_star_chart_ChartTwoAxisXSupplier_idl__ 
 
#ifndef __com_sun_star_chart_XTwoAxisXSupplier_idl__ 
#include <com/sun/star/chart/XTwoAxisXSupplier.idl> 
#endif 
 
#ifndef __com_sun_star_chart_ChartAxisXSupplier_idl__ 
#include <com/sun/star/chart/ChartAxisXSupplier.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module chart {  
 
//============================================================================= 
 
/** a helper service for chart documents which supply
    primary and secondary x-axes.
 */
published service ChartTwoAxisXSupplier
{ 
    /** offers access to the axis object
     */
    interface com::sun::star::chart::XTwoAxisXSupplier; 
    
    service com::sun::star::chart::ChartAxisXSupplier; 
 
    //------------------------------------------------------------------------- 
     
    /** determines if the secondary x-axis is shown or hidden.

        @see ChartAxis
     */
    [property] boolean HasSecondaryXAxis; 
 
    //------------------------------------------------------------------------- 
     
    /** determines for the secondary x-axis
        if the labels at the tick marks are shown or hidden.
    */
    [property] boolean HasSecondaryXAxisDescription; 

    //------------------------------------------------------------------------- 

    /** determines if the title of the secondary X-axis is shown or hidden.

        @see ChartTitle

        @since OOo 3.0
    */
    [optional, property] boolean HasSecondaryXAxisTitle; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
