/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ChartAxisZSupplier.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_ChartAxisZSupplier_idl__ 
#define __com_sun_star_chart_ChartAxisZSupplier_idl__ 
 
#ifndef __com_sun_star_chart_XAxisZSupplier_idl__ 
#include <com/sun/star/chart/XAxisZSupplier.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module chart {  
 
//============================================================================= 
 
/** A helper service for chart documents which supply a z-axis.
 */
published service ChartAxisZSupplier
{ 
    /** offers access to the axis object, the title, and the grids
     */
    interface com::sun::star::chart::XAxisZSupplier; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the z-axis is shown or hidden.@see ChartAxis
     */
    [property] boolean HasZAxis; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the description of the z-axis
        is shown or hidden.
     */
    [property] boolean HasZAxisDescription; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the major grid of the z-axis
        is shown or hidden.@see ChartGrid
     */
    [property] boolean HasZAxisGrid; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the minor grid of the z-axis is shown
        or hidden.@see ChartGrid
     */
    [property] boolean HasZAxisHelpGrid; 
 
    //------------------------------------------------------------------------- 
     
    /** Determines if the title of the z-axis is shown
        or hidden.@see ChartTitle
     */
    [property] boolean HasZAxisTitle; 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
