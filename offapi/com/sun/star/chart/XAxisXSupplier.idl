/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XAxisXSupplier.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_XAxisXSupplier_idl__ 
#define __com_sun_star_chart_XAxisXSupplier_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_drawing_XShape_idl__ 
#include <com/sun/star/drawing/XShape.idl> 
#endif 
 
#ifndef __com_sun_star_beans_XPropertySet_idl__ 
#include <com/sun/star/beans/XPropertySet.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module chart {  
 
//============================================================================= 
 
/** gives access to the <i>x</i>-axis of a chart.

    <p>Note that not all diagrams are capable of displaying an
    <i>x</i>-axis, e.g., the <type>PieDiagram</type>.</p>

    @see XDiagram
 */
published interface XAxisXSupplier: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    /** @returns
            the <i>x</i>-axis title shape.

        @see ChartTitle
     */
    com::sun::star::drawing::XShape getXAxisTitle(); 
 
    //------------------------------------------------------------------------- 
     
    /** @returns 
            the properties of the <i>x</i>-axis of the diagram.

        <p>The returned property set contains scaling properties as
        well as formatting properties.</p>
                 
        @see ChartAxis
     */
    com::sun::star::beans::XPropertySet getXAxis(); 
 
    //------------------------------------------------------------------------- 
     
    /** @returns 
            the properties of the main grid (major grid) of the
            <i>x</i>-axis of the diagram.
                 
        @see ChartGrid
     */
    com::sun::star::beans::XPropertySet getXMainGrid(); 
 
    //------------------------------------------------------------------------- 
     
    /** @returns 
            the properties of the help grid (minor grid) of the
            <i>x</i>-axis of the diagram.
                 
        @see ChartGrid
     */
    com::sun::star::beans::XPropertySet getXHelpGrid(); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
