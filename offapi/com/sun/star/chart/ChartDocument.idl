/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ChartDocument.idl,v $
 * $Revision: 1.18 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_ChartDocument_idl__
#define __com_sun_star_chart_ChartDocument_idl__

#ifndef __com_sun_star_chart_XChartDocument_idl__
#include <com/sun/star/chart/XChartDocument.idl>
#endif

#ifndef __com_sun_star_beans_XPropertySet_idl__
#include <com/sun/star/beans/XPropertySet.idl>
#endif

#ifndef _com_sun_star_xml_UserDefinedAttributeSupplier_idl_
#include <com/sun/star/xml/UserDefinedAttributeSupplier.idl>
#endif

// #ifndef com_sun_star_chart2_data_XDataReceiver_idl
// #include <com/sun/star/chart2/data/XDataReceiver.idl>
// #endif

//=============================================================================

 module com {  module sun {  module star {  module chart {

//=============================================================================

/** is the service for a chart document.

    <p>A chart document consists of a reference to the data source,
    the diagram and some additional elements like a main title, a
    sub-title or a legend.

    </p>@see Diagram 
        @see ChartLegend 
        @see ChartTitle 
        @see ChartDataArray
 */
published service ChartDocument
{
    /** If a <type>ChartDocument</type> may be stored as XML file,
        this service should be supported in order to preserve unparsed
        XML attributes.

    @since OOo 1.1.2
     */
    [optional] service ::com::sun::star::xml::UserDefinedAttributeSupplier;

    interface ::com::sun::star::chart::XChartDocument;
    interface ::com::sun::star::beans::XPropertySet;

    /** If this interface is implemented, it is possible to connect
        data to a chart via the improved data connection method using
        an <type scope="com::sun::star::chart2::data">XDataProvider</type>.
     */
    // BM: #i32138# XDataReceiver is not yet published
//    [optional] interface ::com::sun::star::chart2::data::XDataReceiver;

    //------------------------------------------------------------------------- 

    /** determines if the main title is shown or hidden.
     */
    [property] boolean HasMainTitle;

    //------------------------------------------------------------------------- 

    /** determines if the subtitle is shown or hidden.
     */
    [property] boolean HasSubTitle;

    //------------------------------------------------------------------------- 

    /** determines if the legend is shown or hidden.
     */
    [property] boolean HasLegend;
};

//=============================================================================

}; }; }; };

#endif
