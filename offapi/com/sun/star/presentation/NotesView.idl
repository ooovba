/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NotesView.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_presentation_NotesView_idl__ 
#define __com_sun_star_presentation_NotesView_idl__ 
 
#ifndef __com_sun_star_drawing_DrawingDocumentDrawView_idl__ 
#include <com/sun/star/drawing/DrawingDocumentDrawView.idl> 
#endif 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module presentation {  
 
//============================================================================= 
 
/** This componend integrates a view to a handout page inside a presentation
    document into the desktop.

    @since OOo 1.1.2
 */
published service NotesView
{ 
    //------------------------------------------------------------------------- 

    /** this services offers the  integration of this component into the
        desktop and the basic interfaces and properties of a draw view.
    */
    service com::sun::star::drawing::DrawingDocumentDrawView;
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
