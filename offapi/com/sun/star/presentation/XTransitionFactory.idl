/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XTransitionFactory.idl,v $
 *
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_presentation_XTransitionFactory_idl__
#define __com_sun_star_presentation_XTransitionFactory_idl__

#ifndef __com_sun_star_presentation_XTransition_idl__ 
#include <com/sun/star/presentation/XTransition.idl> 
#endif 
#ifndef __com_sun_star_geometry_RealPoint2D_idl__ 
#include <com/sun/star/geometry/RealPoint2D.idl> 
#endif 


module com {  module sun {  module star {  

module rendering { interface XBitmap; };

module presentation {

interface XTransition;
interface XSlideShowView;

/** TransitionFactory interface to request optional custom Transition instances for slideshow transitions.<p>

    This interface provides the necessary methods to query and create optional transition effects for a SlideShow<p>

    @since OOo 2.4
 */
interface XTransitionFactory : ::com::sun::star::uno::XInterface
{
    /** Checks whether this instance provides an implementation for given transition id.<p>
     */
    boolean hasTransition( [in] short transitionType, [in] short transitionSubType );

    /** Actually create a transition for the given transition id

        @param transitionType
        Main type of transition (@see ::com::sun::star::animation::TransitionType)

        @param transitionSubType
        Subtype for the transition (@see ::com::sun::star::animation::TransitionSubType)

        @param view
        Slideshow view to display on

        @param leavingBitmap
        Bitmap of the slide that's going to leave the screen

        @param enteringBitmap
        Bitmap of the slide that's going to enter the screen

        @param slideOffset
        Offset in pixel from the top, left edge of the view, such that the
        slide displays similar to normal slideshow
     */
    XTransition createTransition( [in] short                                 transitionType, 
                                  [in] short                                 transitionSubType,
                                  [in] XSlideShowView                        view,
                                  [in] com::sun::star::rendering::XBitmap    leavingBitmap,
                                  [in] com::sun::star::rendering::XBitmap    enteringBitmap );
};

service TransitionFactory : XTransitionFactory;

}; }; }; };

#endif

