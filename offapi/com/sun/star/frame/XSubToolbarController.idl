/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSubToolbarController.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_XSubToolbarController_idl__
#define __com_sun_star_frame_XSubToolbarController_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module frame {

//=============================================================================
/** special interface to support sub-toolbars in a controller implementation.

    <p>
    This interface is normally used to implement the toolbar button/sub-
    toolbar function feature. It exchanges the function of the toolbar 
    button, that opened the sub-toolbar, with the one that has been selected 
    on the sub-toolbar.
    </p>
    
    @see com::sun::star::frame::ToolbarController
    
    @since OOo 2.0.0
 */
interface XSubToolbarController : com::sun::star::uno::XInterface
{
    //=============================================================================
    /** if the controller features a sub-toolbar. 

        @return
            <TRUE/> if the controller offers a sub toolbar, otherwise <FALSE/>.

        <p>
        Enables implementations to dynamically decide to support sub-toolbars
        or not.
        </p>
    */
    boolean opensSubToolbar();

    //=============================================================================
    /** provides the resource URL of the sub-toolbar this controller opens.

        @return
            name of the sub-toolbar this controller offers. A empty string 
            will be interpreted as if this controller offers no sub-toolbar.
    */
    string getSubToolbarName();

    //=============================================================================
    /** gets called to notify a controller that a sub-toolbar function has been 
        selected.

        @param aCommand
            a string which identifies the function that has been selected by
            a user.
    */
    void functionSelected( [in] string aCommand );

    //=============================================================================
    /** gets called to notify a controller that it should set an image which
        represents the current selected function.

        <p>
        Only the controller instance is able to set the correct image for the
        current function. A toolbar implementation will ask sub-toolbar 
        controllers to update their image whenever it has to update the images
        of all its buttons.
        </p>
    */
    void updateImage();
};

}; }; }; };

#endif
