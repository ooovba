/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: StatusbarControllerFactory.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_frame_StatusbarControllerFactory_idl__
#define __com_sun_star_frame_StatusbarControllerFactory_idl__

#ifndef __com_sun_star_lang_XMultiComponentFactory_idl__
#include <com/sun/star/lang/XMultiComponentFactory.idl>
#endif

#ifndef __com_sun_star_frame_XUIControllerRegistration_idl__
#include <com/sun/star/frame/XUIControllerRegistration.idl>
#endif

//============================================================================= 

module com { module sun { module star { module frame {

//============================================================================= 

/** specifies a factory that creates instances of registered status bar 
    controller.

    <p>
    A status bar controller can be registered for a command URL and a model 
    service name. A status bar will automatically create a status bar controller 
    if it contains a registered command URL.
    </p>

    @since OOo 2.0.0
*/

service StatusbarControllerFactory
{
    /** this interface provides functions to create new instances of a registered 
        status bar controller.
    
        <p>
        Use <member scope="com.sun.star.lang">XMultiComponentFactory::createInstanceWithArguments()</member> 
        to create a new status bar controller instance. Use the CommandURL as the 
        service specifier.
        
        This call supports the following arguments provided as 
        <type scope="com::sun::star::beans">PropertyValue</type>:
        <ul>
            <li><b>Frame</b><br>specifies the <type scope="com::sun::star::frame">XFrame</type> 
                   instance to which the status bar controller belongs to. This 
                   property must be provided to the status bar controller, otherwise it 
                   cannot dispatch its internal commands.</li>
            <li><b>ModuleIdentifier</b><br>optional string that specifies in which module 
                   context the status bar controller should be created.</li>
        </ul>
        </p>
     */
    interface com::sun::star::lang::XMultiComponentFactory;

    /** provides functions to query for, register and deregister a status bar 
        controller.
     */
    interface com::sun::star::frame::XUIControllerRegistration;
};

}; }; }; };

//============================================================================= 

#endif
