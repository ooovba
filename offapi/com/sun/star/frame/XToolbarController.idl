/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XToolbarController.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_XToolbarController_idl__
#define __com_sun_star_frame_XToolbarController_idl__

#ifndef __com_sun_star_awt_XWindow_idl__
#include <com/sun/star/awt/XWindow.idl>
#endif

#ifndef __com_sun_star_awt_KeyModifier_idl__
#include <com/sun/star/awt/KeyModifier.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module frame {

//=============================================================================
/** is an abstract service for a component which offers a more complex user interface
    to users within a toolbar.

    <p>
    A generic toolbar function is represented as a button which has a state 
    (enabled,disabled and selected, not selected). A toolbar controller can be added to a 
    toolbar and provide information or functions with a more sophisticated user interface.<br/>
    A typical example for toolbar controller is a font chooser on a toolbar. It provides
    all available fonts in a dropdown box and shows the current chosen font.
    <p>

    @see com::sun::star::frame::XDispatchProvider

    @since OOo 2.0.0
 */
interface XToolbarController : com::sun::star::uno::XInterface
{
    //=============================================================================
    /** provides a function to execute the command which is bound to the toolbar controller.

        @param 
            a combination of <type scope="com::sun::star::awt">KeyModifier</type> value that represent
            the current state of the modifier keys.

        <p>
        This function is usally called by a toolbar implementation when a user clicked on a toolbar button
        or pressed enter on the keyboard when the item has the input focus.
        </p>
    */
    void execute( [in] short KeyModifier );

    //=============================================================================
    /** notifies a component that a single click has been made on the toolbar item.
    */
    void click();

    //=============================================================================
    /** notifies a component that a double click has been made on the toolbar item.
    */
    void doubleClick();

    //=============================================================================
    /** requests to create a popup window for additional functions.

        @return
            a <type scope="com::sun::star::awt">XWindow</type> which provides additional functions 
            to the user. The reference must be empty if component does not want to provide a separate
            window.
    */
    com::sun::star::awt::XWindow createPopupWindow();

    //=============================================================================
    /** requests to create an item window which can be added to the toolbar.

        @param Parent
            a <type scope="com::sun::star::awt">XWindow</type> which must be used as a parent
            for the requested item window.
        
        @return
            a <type scope="com::sun::star::awt">XWindow</type> which can be added to a toolbar. 
            The reference must be empty if a component does not want to provide an item window.
    */
    com::sun::star::awt::XWindow createItemWindow( [in] com::sun::star::awt::XWindow Parent );
};

}; }; }; };

#endif
