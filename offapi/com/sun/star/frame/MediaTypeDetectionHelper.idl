/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: MediaTypeDetectionHelper.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_MediaTypeDetectionHelper_idl__
#define __com_sun_star_frame_MediaTypeDetectionHelper_idl__

#ifndef __com_sun_star_util_XStringMapping_idl__
#include <com/sun/star/util/XStringMapping.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module frame {

//=============================================================================
/** provides for mapping a given sequence of content identifier strings
    to a sequence of respective media (mime) types
 */
published service MediaTypeDetectionHelper
{
    //-------------------------------------------------------------------------
    /** provides a mapping from <atom>string<atom> to <atom>string<atom>

        <p>
        Order of given and their returned coressponding strings is important.
        Don't pack or optimize it. Every item of [in] list must match
        to an item of [out] list.
        </p>
     */
    interface com::sun::star::util::XStringMapping;
};

//=============================================================================

}; }; }; };

#endif

