/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DispatchHelper.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_DispatchHelper_idl__
#define __com_sun_star_frame_DispatchHelper_idl__

#ifndef __com_sun_star_frame_XDispatchHelper_idl__
#include <com/sun/star/frame/XDispatchHelper.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module frame {

//=============================================================================
/** provides an easy way to dispatch an URL using one call instead of multiple ones.

    <p>
    Normaly a complete dispatch is splitted into different parts:
    - converting and parsing the URL
    - searching for a valid dispatch object available on a dispatch provider
    - dispatching of the URL and it's parameters
    </p>

    @see DispatchProvider

    @since OOo 1.1.2
 */
published service DispatchHelper
{
    //-------------------------------------------------------------------------
    /** provides the easy way for dispatch requests.

        @see XDispatchProvider
        @see XDispatch
     */
    interface XDispatchHelper;
};

//=============================================================================

}; }; }; };

#endif
