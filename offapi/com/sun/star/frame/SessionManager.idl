/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: SessionManager.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_SessionManager_idl__
#define __com_sun_star_frame_SessionManager_idl__

#ifndef __com_sun_star_frame_XSessionManagerClient_idl__
#include <com/sun/star/frame/XSessionManagerClient.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module frame {

//=============================================================================
/** The SessionManager service provides an interface to the session manager
    of the desktop. A session manager keeps track of applications that are
    running when the desktop shuts down and starts them again in the same
    state they were left when the desktop starts up the next time. To be able
    to do this the session manager needs cooperation from applications;
    applications have to provide sufficient information to be started again as
    well as restore the state they were left in. The normal flow of operation
    looks like this:

    <ol>
    <li>The user starts the desktop shutdown.</li>
    <li>The session manager informs all its connected applications
    about the pending shutdown.</li>
    <li>Each application saves its current state; while doing this it may
        <ul>
        <li>The application may request to interact with the user (e.g. to ask
        where to save documents). This request is necessary because at any one
        time only one application can iteract with the user. The session manager
        coordinates these requests and grants every application in need of user
        interaction a timeslot in which it may interact with the user</li>
        <li>try to cancel the whole shutdown; the session manager may or may
        not honor that request.</li>
       </ul>
     </li>
     <li>After saving is done the session manager signals all applications
     to exit.</li>
     <li>Applications answer the exit message by disconnecting from the
     session manager.</li>
     <li>After all applications have exited or a reasonable timeout the
     session manager kills all remaining applications and finally lets the
     desktop shut down.</li>
*/
service SessionManager
{
    /** XSessionManagerClient (of which there can only be one instance per
        process) provides an application's interface to the session manager.
        It keeps track of every listener inside the application and multiplexes
        the session manager's signals as well as requests to the session manager.
    */
    interface XSessionManagerClient;
    
};
 
}; }; }; };

#endif
