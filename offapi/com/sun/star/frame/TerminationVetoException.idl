/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TerminationVetoException.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_TerminationVetoException_idl__
#define __com_sun_star_frame_TerminationVetoException_idl__

#ifndef __com_sun_star_uno_Exception_idl__
#include <com/sun/star/uno/Exception.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module frame {

//=============================================================================
/** can be thrown by a <type>XTerminateListener</type> to prevent the environment
    (e.g., desktop) from terminating

    <p>
    If a <type>XTerminateListener</type> use this exception for a veto against
    the termination of the office, he will be the new "owner" of it.
    After his own operation will be finished, he MUST try to terminate the
    office again. Any other veto listener can intercept that again or office
    will die realy.
    </p>

    @see XDesktop::terminate()
    @see XTerminateListener
 */
published exception TerminationVetoException: com::sun::star::uno::Exception
{
};

//=============================================================================

}; }; }; };

#endif
