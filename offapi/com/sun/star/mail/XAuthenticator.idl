/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XAuthenticator.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_mail_XAuthenticator_idl__
#define __com_sun_star_mail_XAuthenticator_idl__

#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl>
#endif

module com { module sun { module star { module mail {

/**
   Represents an interface that will be used to query for user
   information which are necessary to login to a network resource.  
   An implementation of this interface may for instance show a
   dialog to query the user for the necessary data.      
   
   @since OOo 2.0.0
 */
interface XAuthenticator: ::com::sun::star::uno::XInterface {
    
    /** 
        Will be called when the user name is needed.
    
        @returns 
        the user name. 
    */
    string getUserName();    
    
    /**
        Will be called when the password of the user is needed.
        
        @returns
        the password of the user.
    */
    string getPassword();    
};

}; }; }; };

#endif
