/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XFilePicker.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ui_dialogs_XFilePicker_idl__
#define __com_sun_star_ui_dialogs_XFilePicker_idl__

#ifndef __com_sun_star_lang_IllegalArgumentException_idl__
#include <com/sun/star/lang/IllegalArgumentException.idl>
#endif

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_ui_dialogs_XExecutableDialog_idl__
#include <com/sun/star/ui/dialogs/XExecutableDialog.idl>
#endif

//=============================================================================

module com { module sun { module star { module ui { module dialogs {

//=============================================================================
/** Specifies an interface for a FilePicker
*/
 
published interface XFilePicker: com::sun::star::ui::dialogs::XExecutableDialog
{

    //-------------------------------------------------------------------------
    /**	Enable/disable multiselection mode 

        <p>If the multiselection mode is enabled, multiple files 
        may be selected by the user else only one file selection at a time is possible</p>
    
        @param bMode 
        <p>A value of <TRUE/> enables the multiselection mode.</p>
        <p>A value of <FALSE/> disables the multiselection mode, this is the default.</p>
    */
    void setMultiSelectionMode( [in] boolean bMode );

    //-------------------------------------------------------------------------
    /**	Sets the default string that appears in the file name box of a FilePicker.

        @param aName
        <p>	Specifies the default file name, displayed when the FilePicker 
            is shown. The implementation may accept any string, and does not
            have to check for a valid file name or if the file really exists.
        </p>
    */
    void setDefaultName( [in] string aName );

    //-------------------------------------------------------------------------
    /**	Sets the directory that the file dialog initially displays.

        @param aDirectory
        Specifies the initial directory in URL format. The given URL must be
        conform to <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>). 
            
        @throws com::sun::star::lang::IllegalArgumentException
        if the URL is invalid (doesn't conform to <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>).		
    */
    void setDisplayDirectory( [in] string aDirectory )
        raises( ::com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /**	Returns the directory that the file dialog is currently showing or 
        was last showing before closing the dialog with Ok. If the user 
        did cancel the dialog, the returned value is undefined. 

        @returns 
        The directory in URL format, must be conform to <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>.		
     */
    string getDisplayDirectory();

    //-------------------------------------------------------------------------
    /**	Returns a sequence of the selected files including path information in
        URL format, conform to <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>.

        <p>If the user closed the dialog with cancel an empty sequence will be 
        returned.</p>
        <br/>
        <p>If the dialog is in execution mode and a single file is selected 
        the complete URL of this file will be returned.</p>
        <p>If the dialog is in execution mode and multiple files are selected 
        an empty sequence will be returned.</p>
        <p>If the dialog is in execution mode and the selected file name is false
        or any other error occurs an empty sequence will be returned.</p>

        @returns 
        <p>	The complete path of the file or directory currently selected
            in URL format. There are two different cases:
            <ol>
                <li>Multiselection is disabled: 
                The first and only entry of the sequence contains the complete 
                path/filename in URL format.</li>

                <li>Multiselection is enabled:  
                If only one file is selected, the first entry
                of the sequence contains the complete path/filename in URL format. 
                If multiple files are selected, the first entry of the sequence contains 
                the path in URL format, and the other entries contains the names of the selected 
                files without path information.</li>
            </ol>
            
            <br/>

            <p><strong>Notes for the implementation of a FileSave dialog:</strong>If there exists 
            a checkbox "Automatic File Extension" which is checked and a valid filter is currently selected
            the dialog may automatically add an extension to the selected file name.</p>
        </p>
    */
    sequence< string > getFiles();
};

//=============================================================================

}; }; }; }; };


#endif

