/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XAsynchronousExecutableDialog.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ui_dialogs_XAsynchronousExecutableDialog_idl__
#define __com_sun_star_ui_dialogs_XAsynchronousExecutableDialog_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_ui_dialogs_XDialogClosedListener_idl__
#include <com/sun/star/ui/dialogs/XDialogClosedListener.idl>
#endif

//=============================================================================

module com { module sun { module star { module ui { module dialogs {

//=============================================================================
/** Specifies an interface for an executable dialog in asynchronous mode.
*/
interface XAsynchronousExecutableDialog: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /**	Sets the title of the dialog.

        @param aTitle
        Set an abitrary title for the dialog,
        may be an empty string if the dialog should not have a title.
    */
    void setDialogTitle( [in] string aTitle );

    //-------------------------------------------------------------------------
    /** Executes (shows) the dialog and returns immediately.

        @param xListener
        This listener will be called when the dialog is closed.
    */
    void startExecuteModal( [in] XDialogClosedListener xListener );

};

//=============================================================================

}; }; }; }; };

#endif

