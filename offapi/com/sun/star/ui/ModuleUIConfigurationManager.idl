/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ModuleUIConfigurationManager.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ui_ModuleUIConfigurationManager_idl__
#define __com_sun_star_ui_ModuleUIConfigurationManager_idl__

#ifndef __com_sun_star_lang_XInitialization_idl__
#include <com/sun/star/lang/XInitialization.idl>
#endif

#ifndef __com_sun_star_ui_XUIConfigurationManager_idl__
#include <com/sun/star/ui/XUIConfigurationManager.idl>
#endif

#ifndef __com_sun_star_ui_XUIConfigurationPersistence_idl__
#include <com/sun/star/ui/XUIConfigurationPersistence.idl>
#endif

#ifndef __com_sun_star_ui_XModuleUIConfigurationManager_idl__
#include <com/sun/star/ui/XModuleUIConfigurationManager.idl>
#endif

#ifndef __com_sun_star_ui_XUIConfiguration_idl__
#include <com/sun/star/ui/XUIConfiguration.idl>
#endif

module com { module sun { module star { module ui { 

/** specifies a user interface configuration manager which gives access to user interface 
    configuration data of a module.
    
    <p>
    A module user interface configuratio manager supports two layers of configuration settings
    data:<br/>   
    1. Layer: A module default user interface configuration which describe all user interface 
    elements settings that are used by OpenOffice. It is not possible to insert, remove or change
    elements settings in this layer through the interfaces.</br>
    2. Layer: A module user interface configuration which only contains customized user interface 
    elements and user-defined ones. All changes on user interface element settings are done on
    this layer.</br>
    </p>
    
    @since OOo 2.0.0
*/

service ModuleUIConfigurationManager
{    
    /** provides a function to initialize a module user interface configuration manager instance.

        <p>
        A module user interface configuration manager instance needs the following arguments as 
        <type scope="com::sun::star::beans">PropertyValue</type> to be in a working state:
        <ul>
            <li><b>DefaultConfigStorage</b>a reference to a <type scope="com::sun::star::embed">Storage</type> that 
            contains the default module user interface configuration settings.</li>
            <li><b>UserConfigStorage</b>a reference to a <type scope="com::sun::star::embed">Storage</type> that 
            contains the user-defined module user interface configuration settings.</li>
            <li><b>ModuleIdentifier</b>string that provides the module identifier.</li>
            <li><b>UserRootCommit</b>a reference to a <type scope="com::sun::star::embed">XTransactedObject</type> which
            represents the customizable root storage. Every implementation must use this reference to commit its
            changes also at the root storage.</li>
        </ul>
        A non-initialized module user interface configuration manager cannot be used, it is treated
        as a read-only container.
        </p>
    */
    interface com::sun::star::lang::XInitialization;

    /** provides access to persistence functions to load/store user interface element
        settings from/to a module storage.
    */
    interface com::sun::star::ui::XUIConfigurationPersistence;

    /** provides functions to change, insert and remove user interface element settings
        from a module user interface configuration manager.
    */
    interface com::sun::star::ui::XUIConfigurationManager;

    /** provides access to the default layer of a module based ui configuration
        manager.
    */
    interface com::sun::star::ui::XModuleUIConfigurationManager;

    /** provides functions to add and remove listeners for changes within a module user
        interface configuration manager.
    */
    interface com::sun::star::ui::XUIConfiguration;
};

//=============================================================================

}; }; }; };

#endif
