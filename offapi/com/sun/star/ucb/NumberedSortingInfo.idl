/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NumberedSortingInfo.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_NumberedSortingInfo_idl__
#define __com_sun_star_ucb_NumberedSortingInfo_idl_

//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** contains information for sorting a <type>ContentResultSet</type>.

    <p> In contrast to the struct <type>SortingInfo</type> this struct is
    used to be on the safe side, that no one asks for sorting by a property
    which is not contained in a <type>ContentResultSet</type>.
*/
published struct NumberedSortingInfo
{
    //-------------------------------------------------------------------------
    /** sort the resultset by this column. Index starts with <code>1</code>.
    */
    long ColumnIndex;

    //-------------------------------------------------------------------------
    /** contains a flag indicating the sort mode (ascending or descending).
    */
    boolean Ascending;
};

//=============================================================================

}; }; }; };

#endif
