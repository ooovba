/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSimpleFileAccess2.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ucb_XSimpleFileAccess2_idl__
#define __com_sun_star_ucb_XSimpleFileAccess2_idl__

#ifndef __com_sun_star_ucb_XSimpleFileAccess_idl__
#include <com/sun/star/ucb/XSimpleFileAccess.idl>
#endif

#ifndef __com_sun_star_io_XInputStream_idl__
#include <com/sun/star/io/XInputStream.idl>
#endif

#ifndef __com_sun_star_uno_Exception_idl__
#include <com/sun/star/uno/Exception.idl>
#endif

//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** This is an extension to the interface <type>XSimpleFileAccess</type>.
*/
published interface XSimpleFileAccess2 : com::sun::star::ucb::XSimpleFileAccess
{

    //-------------------------------------------------------------------------
    /** Overwrites the file content with the given data.

        <p>If the file does not exist, it will be created.

        @param FileURL
        File to write

        @param data
        A stream containing the data for the file to be (over-)written
    */
    void writeFile( [in] string FileURL,
                    [in] com::sun::star::io::XInputStream data )
        raises( com::sun::star::uno::Exception );

};

//=============================================================================

}; }; }; };

#endif
