/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: InteractiveCHAOSException.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_InteractiveCHAOSException_idl__
#define __com_sun_star_ucb_InteractiveCHAOSException_idl__

#ifndef __com_sun_star_task_ClassifiedInteractionRequest_idl__
#include <com/sun/star/task/ClassifiedInteractionRequest.idl>
#endif


//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** An error capsuling error information as used by CHAOS.

    @deprecated
*/
published exception InteractiveCHAOSException: com::sun::star::task::ClassifiedInteractionRequest
{
    //-------------------------------------------------------------------------
    /** The error ID.

        <p>For a 'dynamic' error (with extra textual arguments), this ID
        does not contain the 'dynamic bits'.
    */
    long ID;

    //-------------------------------------------------------------------------
    /** Any textual arguments of a 'dynamic' error (which will get merged into
        the error message).

        <p>This sequence should contain at most two elements.
    */
    sequence<string> Arguments;

};

//=============================================================================

}; }; }; };

#endif
