/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XContentCreator.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_XContentCreator_idl__
#define __com_sun_star_ucb_XContentCreator_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_ucb_ContentInfo_idl__
#include <com/sun/star/ucb/ContentInfo.idl>
#endif

#ifndef __com_sun_star_ucb_XContent_idl__
#include <com/sun/star/ucb/XContent.idl>
#endif


//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** A creator for new (persistent) contents, like file system folders.

    <p>Creation of a new (persistent) content:
    <ol>
        <li>newObject = creator.createNewContent( ... )
        <li>initialize the new object (i.e. newObject.Property1 = ...)
        <li>let the new content execute the command "insert". That command
            commits the data and makes the new content persistent.
    </ol>

    @version  1.0
    @author   Kai Sommerfeld
    @see      XContent
    @see      XCommandProcessor
*/
published interface XContentCreator: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /**	returns a list with information about the creatable contents.

        @returns
        the list with information about the creatable contents.
    */
    sequence<com::sun::star::ucb::ContentInfo> queryCreatableContentsInfo();

    //-------------------------------------------------------------------------
    /**	creates a new content of given type..

        @param Info
        the content information.

        @returns
        the new content, if operation was succesful.
    */
    com::sun::star::ucb::XContent createNewContent(
                        [in] com::sun::star::ucb::ContentInfo Info );
};

//=============================================================================

}; }; }; };

#endif
