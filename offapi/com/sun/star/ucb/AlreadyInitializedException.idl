/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AlreadyInitializedException.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_AlreadyInitializedException_idl__
#define __com_sun_star_ucb_AlreadyInitializedException_idl__

#ifndef __com_sun_star_uno_Exception_idl__
#include <com/sun/star/uno/Exception.idl>
#endif


//=============================================================================

module com { module sun { module star { module ucb { 

//=============================================================================
/** This exception is thrown to indicate that an attempt was made to
    reinitialize an object that can only be	initialized once.
    
    @version	1.0
    @author 	Kai Sommerfeld
*/
published exception AlreadyInitializedException: com::sun::star::uno::Exception
{
};

//=============================================================================

}; }; }; }; 

#endif
