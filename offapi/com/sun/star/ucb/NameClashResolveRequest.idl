/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NameClashResolveRequest.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ucb_NameClashResolveRequest_idl__
#define __com_sun_star_ucb_NameClashResolveRequest_idl__

#ifndef __com_sun_star_task_ClassifiedInteractionRequest_idl__
#include <com/sun/star/task/ClassifiedInteractionRequest.idl>
#endif


//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** This request is used to indicate a name clash.

    <p>For example, when copying a file there migth be another file in the
    target folder that has the same file name as the source file.

    <p>If this exception is passed to an
    <type scope="com::sun::star::task">XInteractionHandler</type> an
    <type>XInteractionSupplyName</type> and an
    <type>XInteractionReplaceExistingData</type> should be supplied with
    the <type scope="com::sun::star::task">XInteractionRequest</type>. On
    return the <type>XInteractionSupplyName</type>, if selected, will contain
    a new name supposed to resolve the name clash. The
    <type>XInteractionReplaceExistingData</type> will be selected if the
    clashing resource shall be overwritten.

    @version	1.0
    @author 	Kai Sommerfeld
*/
published exception NameClashResolveRequest : com::sun::star::task::ClassifiedInteractionRequest
{
    //-------------------------------------------------------------------------
    /** contains the URL of the folder that contains the clashing resource.
     */
    string TargetFolderURL;

    //-------------------------------------------------------------------------
    /** contains the clashing name.
     */
    string ClashingName;

    //-------------------------------------------------------------------------
    /** contains a proposal for a new new, non-clashing name.
        <p>This field may be left empty if the implementation is not able
        to suggest a new name.
     */
    string ProposedNewName;
};

//=============================================================================

}; }; }; };

#endif
