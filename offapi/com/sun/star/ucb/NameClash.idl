/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NameClash.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_NameClash_idl__
#define __com_sun_star_ucb_NameClash_idl__


//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** These are the possible values for <member>TransferInfo::NameClash</member>.
*/
published constants NameClash
{
    //-------------------------------------------------------------------------
    /** Means to set an error and cancel the operation.
    */
    const long ERROR = 0;

    //-------------------------------------------------------------------------
    /** Means to overwrite the object in the target folder with the object to
        transfer.
    */
    const long OVERWRITE = 1;

    //-------------------------------------------------------------------------
    /** Means to rename the object to transfer to solve the clash.

        <p>The implementation needs to supply and set a suitable new name.
    */
    const long RENAME = 2;

    //-------------------------------------------------------------------------
    /** Deprecated. Do not use!

        @deprecated
    */
    const long KEEP = 3;

    //-------------------------------------------------------------------------
    /** Means to use a <type>NameClashResolveRequest</type> in order to solve
        the name clash.

        @see com::sun::star::task::XInteractionHandler
    */
    const long ASK = 4;
};

//=============================================================================

}; }; }; };

#endif
