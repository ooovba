/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: FetchError.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_FetchError_idl__
#define __com_sun_star_ucb_FetchError_idl__

//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** These values are used to specify whether and which error has occured
    while fetching data of some <type>ContentResultSet</type> rows.

    @see FetchResult
*/
published constants FetchError
{
    //-------------------------------------------------------------------------
    /** indicates that fetching of data was successful.
    */
    const short SUCCESS = 0;

    //-------------------------------------------------------------------------
    /** indicates that during fetching we went beyond the last or first row.

        <p>Therefore the <type>FetchResult</type> does not contain the full
        count of demanded rows, but the maximum possible count must be
        contained.
    */
    const short ENDOFDATA = 1;

    //-------------------------------------------------------------------------
    /** indicates that during fetching we got an exception.

        <p>The row, that causes the exceptione, and all following ( 'following'
        in readorder! ) rows are not contained in the <type>FetchResult</type>.
        Therefore the <type>FetchResult</type> does not contain the full count
        of demanded rows. But all properly readed rows so far must be contained.
    */
    const short EXCEPTION = 2;
};

//=============================================================================

}; }; }; };

#endif
