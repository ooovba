/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSimpleMailMessage.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_system_XSimpleMailMessage_idl__
#define __com_sun_star_system_XSimpleMailMessage_idl__

#ifndef __com_sun_star_uno_RuntimeException_idl__
#include <com/sun/star/uno/RuntimeException.idl>
#endif

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_lang_IllegalArgumentException_idl__
#include <com/sun/star/lang/IllegalArgumentException.idl>
#endif

//=============================================================================

module com { module sun { module star { module system {

//=============================================================================
/**	This interface lets a client set or get the information of a simple mail
    message.
*/
 

published interface XSimpleMailMessage: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /**	To set the recipient of the simple mail message.
        @param aRecipient 
        The email address of a recipient. The method doesn't check if
        the given email address is valid.		
    */
    void setRecipient( [in] string aRecipient );

    //-------------------------------------------------------------------------
    /**	To get the recipient of the simple mail message.
        @returns
        The specified email address of a recipient 
        if any has been specified or an empty string. 			
    */
    string getRecipient( );

    //-------------------------------------------------------------------------
    /**	To set the cc recipients of a simple mail message.

        @param aCcRecipient
        Sets a sequence with the email addresses of one or more cc recipients. 
        <p>The method does not check if the given addresses are valid. An empty
        sequence means there are no cc recipients.</p>
    */
    void setCcRecipient( [in] sequence< string > aCcRecipient );

    //-------------------------------------------------------------------------
    /**	To get the cc recipients of a simple mail message.
        @returns
        A sequence with the email addresses of one or more cc recipients.
        <p>If no cc recipients have been specified an empty sequence will be returned.</p>
    */
    sequence< string > getCcRecipient( );

    //-------------------------------------------------------------------------
    /**	To set the bcc recipient of a simple mail message.
        @param aBccRecipient
        A sequence with the email addresses of one or more bcc recipients. An empty
        sequence means there are no bcc recipients.</p>
    */
    void setBccRecipient( [in] sequence< string > aBccRecipient );

    //-------------------------------------------------------------------------
    /**	To get the bcc recipients of a simple mail message.
        @returns
        A sequence with the email addresses of one or more bcc recipients.
        <p>If no bcc recipients have been specified an empty sequence will be returned.</p>
    */
    sequence< string > getBccRecipient( );

    //-------------------------------------------------------------------------
    /**	To set the email address of the originator of a simple mail message.
        @param aOriginator
        Sets the email address of the originator of the mail.	
    */
    void setOriginator( [in] string aOriginator );

    //-------------------------------------------------------------------------
    /**	To get the email address of the originator of a simple mail message.
        @returns
        The email address of the originator of the mail.
        <p>If no originator has been specified an empty string will be returned.</p>
    */
    string getOriginator( );

    //-------------------------------------------------------------------------
    /**	To set the subject of a simple mail message.
        @param aSubject
        Sets the subject of the simple mail message.
    */
    void setSubject( [in] string aSubject );
    
    //-------------------------------------------------------------------------
    /**	To get the subject of a simple mail message.
        @returns
        The subject of the simple mail message.
        <p>If no subject has been specified an empty string will be returned.</p>		
    */
    string getSubject( );

    //-------------------------------------------------------------------------
    /**	To set an attachment of a simple mail message.
        @param aAttachement
        Sets a sequence of file URLs specifying the files that should be
        attached to the mail. The given file URLs must be conform to 
        <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>.
        The method does not check if the specified file or files really exist.

        @throws ::com::sun::star::lang::IllegalArgumentException
        if at least one of the given file URLs is invalid (doesn't conform to
        <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>).
    */
    void setAttachement( [in] sequence< string > aAttachement )
        raises( ::com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /**	To get the attachment of a simple mail message.
        @returns
        A sequence of file URLs specifying the files that should be attached to 
        the mail or an empty sequence if no attachments have been specified.
        The returned file URLs are conform to <a href="http://www.w3.org/Addressing/rfc1738.txt">Rfc1738</a>.
    */
    sequence< string > getAttachement( );
};

//=============================================================================

}; }; }; };

#endif

