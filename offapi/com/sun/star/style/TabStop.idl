/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TabStop.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_style_TabStop_idl__ 
#define __com_sun_star_style_TabStop_idl__ 
 
#ifndef __com_sun_star_style_TabAlign_idl__ 
#include <com/sun/star/style/TabAlign.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module style {  
 
//============================================================================= 
 
// DocMerge from xml: struct com::sun::star::style::TabStop
/** This structure is used to specify a single tabulator stop.
 */
published struct TabStop
{
    // DocMerge from xml: field com::sun::star::style::TabStop::Position
    /** This field specifies the position of the tabulator in relation
        to the left border.
     */
    long Position; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: field com::sun::star::style::TabStop::Alignment
    /** This field specifies the alignment of the text range before the 
        tabulator.
     */
    com::sun::star::style::TabAlign Alignment; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: field com::sun::star::style::TabStop::DecimalChar
    /** This field specifies which delimiter is used for the decimal.
     */
    char DecimalChar; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: field com::sun::star::style::TabStop::FillChar
    /** This field specifies the character that is used to fill up the
        space between the text in the text range and the tabulators.
     */
    char FillChar; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
