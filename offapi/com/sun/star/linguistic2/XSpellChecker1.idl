/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSpellChecker1.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_linguistic2_XSpellChecker1_idl__
#define __com_sun_star_linguistic2_XSpellChecker1_idl__

#ifndef __com_sun_star_linguistic2_XSupportedLanguages_idl__
#include <com/sun/star/linguistic2/XSupportedLanguages.idl>
#endif

#ifndef __com_sun_star_lang_IllegalArgumentException_idl__
#include <com/sun/star/lang/IllegalArgumentException.idl>
#endif

#ifndef __com_sun_star_linguistic2_XDictionaryList_idl__
#include <com/sun/star/linguistic2/XDictionaryList.idl>
#endif

#ifndef __com_sun_star_linguistic2_XSpellAlternatives_idl__
#include <com/sun/star/linguistic2/XSpellAlternatives.idl>
#endif

#ifndef _COM_SUN_STAR_BEANS_PROPERTYVALUES_idl_
#include <com/sun/star/beans/PropertyValues.idl>
#endif


//=============================================================================

module com { module sun { module star { module linguistic2 { 

//=============================================================================
/** @deprecated
*/
published interface XSpellChecker1 : com::sun::star::linguistic2::XSupportedLanguages
{
    //-------------------------------------------------------------------------
    boolean isValid( 
            [in] string aWord,
            [in] short nLanguage,
            [in] com::sun::star::beans::PropertyValues aProperties )
        raises( com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    com::sun::star::linguistic2::XSpellAlternatives spell( 
            [in] string aWord,
            [in] short nLanguage,
            [in] com::sun::star::beans::PropertyValues aProperties )
        raises( com::sun::star::lang::IllegalArgumentException );

};

//=============================================================================

}; }; }; }; 

#endif

