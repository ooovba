/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AccessibleCsvTable.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_AccessibleCsvTable_idl__
#define __com_sun_star_sheet_AccessibleCsvTable_idl__

#ifndef __com_sun_star_accessibility_XAccessibleContext_idl__
#include <com/sun/star/accessibility/XAccessibleContext.idl>
#endif

#ifndef __com_sun_star_accessibility_XAccessibleComponent_idl__
#include <com/sun/star/accessibility/XAccessibleComponent.idl>
#endif

#ifndef __com_sun_star_accessibility_XAccessibleTable_idl__
#include <com/sun/star/accessibility/XAccessibleTable.idl>
#endif

#ifndef __com_sun_star_accessibility_XAccessibleSelection_idl__
#include <com/sun/star/accessibility/XAccessibleSelection.idl>
#endif


//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** The accessible object of the data table in the CSV import dialog.
    @see ::com::sun::star::sheet::AccessibleCsvRuler
    @see ::com::sun::star::sheet::AccessibleCsvCell

     @since OOo 1.1.2
 */
published service AccessibleCsvTable
{
    //-------------------------------------------------------------------------

    /** This interface gives access to the whole content of the table.
     */
    interface ::com::sun::star::accessibility::XAccessibleContext;

    //-------------------------------------------------------------------------

    /** This interface gives access to the visibility of the table.
     */
    interface ::com::sun::star::accessibility::XAccessibleComponent;

    //-------------------------------------------------------------------------

    /** This interface gives access to functionality specific for a table.
     */
    interface ::com::sun::star::accessibility::XAccessibleTable;

    //-------------------------------------------------------------------------

    /** This interface gives access to selection functionality.
     */
    interface ::com::sun::star::accessibility::XAccessibleSelection;
};

//=============================================================================

}; }; }; };

#endif
