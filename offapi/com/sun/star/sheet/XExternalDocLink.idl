/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XExternalDocLink.idl,v $
 * $Revision: 1.1.2.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XExternalDocLink_idl__
#define __com_sun_star_sheet_XExternalDocLink_idl__

#include <com/sun/star/container/XEnumerationAccess.idl>
#include <com/sun/star/container/XIndexAccess.idl>
#include <com/sun/star/container/XNameAccess.idl>
#include <com/sun/star/sheet/XExternalSheetCache.idl>

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** Primary interface for the <type scope="com::sun::star::sheet">ExternalDocLink</type> service.

    @see com::sun::star::sheet::ExternalDocLink

    @since OOo 3.1.0
 */
interface XExternalDocLink
{
    interface com::sun::star::container::XNameAccess;
    interface com::sun::star::container::XIndexAccess;
    interface com::sun::star::container::XEnumerationAccess;

    //-------------------------------------------------------------------------

    /** <p>This method adds a new sheet cache instance to the external document
        link for a specified sheet name.  If a sheet cache instance already
        exists for the specified name, then the existing instance is returned.</p>

        <p>Note that a sheet name lookup is performed in a case-insensitive
        fashion.</p>

        @param aSheetName sheet name

        @return com::sun::star::sheet::XExternalSheetCache sheet cache instance
     */
    com::sun::star::sheet::XExternalSheetCache addSheetCache( [in] string aSheetName );

    //-------------------------------------------------------------------------

    /** Index corresponding to the external document link.

        <p>This index value corresponds with the external document
        represented by an instance of
        <type scope="com::sun::star::sheet">ExternalDocLink</type>.  This
        value is stored within a formula token instance.</p>

        <p>Each external document cache instance has a unique index value, and this
        index value can be used to retrieve the corresponding external document cache
        from the parent <type scope="com::sun::star::sheet">ExternalDocLinks</type> instance.</p>

        @see com::sun::star::sheet::ExternalDocLinks
        @see com::sun::star::sheet::FormulaToken
        @see com::sun::star::sheet::ExternalReference
     */
    [attribute, readonly] long TokenIndex;
};

//=============================================================================

}; }; }; };

#endif
