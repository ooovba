/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSubTotalCalculatable.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSubTotalCalculatable_idl__
#define __com_sun_star_sheet_XSubTotalCalculatable_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_sheet_XSubTotalDescriptor_idl__
#include <com/sun/star/sheet/XSubTotalDescriptor.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** contains methods to handle a subtotal descriptor.

    <p>The subtotal descriptor provides properties to set up the subtotal
    function.</p>

    @see com::sun::star::sheet::SheetCellRange
    @see com::sun::star::sheet::SubTotalDescriptor
 */
published interface XSubTotalCalculatable: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** creates a subtotal descriptor.

        @param bEmpty
            if set to <TRUE/>, creates an empty descriptor. If set to
            <FALSE/>, fills the descriptor with previous settings of the
            current object (i.e. a database range).
     */
    com::sun::star::sheet::XSubTotalDescriptor createSubTotalDescriptor(
            [in] boolean bEmpty );

    //-------------------------------------------------------------------------

    /** creates subtotals using the settings of the passed descriptor.

        @param xDescriptor
            the subtotal descriptor with the settings used for the subtotal
            operation.

        @param bReplace
            if set to <TRUE/>, replaces previous subtotal results.
     */
    void applySubTotals(
            [in] com::sun::star::sheet::XSubTotalDescriptor xDescriptor,
            [in] boolean bReplace );

    //-------------------------------------------------------------------------

    /** removes the subtotals from the current object.
     */
    void removeSubTotals();

};

//=============================================================================

}; }; }; };

#endif

