/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSheetOutline.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSheetOutline_idl__
#define __com_sun_star_sheet_XSheetOutline_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_table_CellRangeAddress_idl__
#include <com/sun/star/table/CellRangeAddress.idl>
#endif

#ifndef __com_sun_star_table_TableOrientation_idl__
#include <com/sun/star/table/TableOrientation.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides methods to access the outlines of a sheet.
 */
published interface XSheetOutline: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** creates an outline group.

        @param aRange
            contains the range of rows or columns, depending on
            the parameter nOrientation.

        @param nOrientation
            the orientation of the new outline (columns or rows).
     */
    void group(
            [in] com::sun::star::table::CellRangeAddress aRange,
            [in] com::sun::star::table::TableOrientation nOrientation );

    //-------------------------------------------------------------------------

    /** removes outline groups.

        <p>In the specified range, all outline groups on the innermost
        level are removed.</p>

        @param aRange
            contains the range of rows or columns, depending on
            the parameter nOrientation.

        @param nOrientation
            the orientation of the outlines to remove (columns or rows).
     */
    void ungroup(
            [in] com::sun::star::table::CellRangeAddress aRange,
            [in] com::sun::star::table::TableOrientation nOrientation );

    //-------------------------------------------------------------------------

    /** creates outline groups from formula references in a range.

        @param aRange
            the cell range for which outlines are generated.
     */
    void autoOutline( [in] com::sun::star::table::CellRangeAddress aRange );

    //-------------------------------------------------------------------------

    /** removes all outline groups from the sheet.
     */
    void clearOutline();

    //-------------------------------------------------------------------------

    /** collapses an outline group.

        @param aRange
            the cell range for which the outlines are collapsed.
     */
    void hideDetail( [in] com::sun::star::table::CellRangeAddress aRange );

    //-------------------------------------------------------------------------

    /** reopens an outline group.

        @param aRange
            the cell range for which the outlines are reopened.
     */
    void showDetail( [in] com::sun::star::table::CellRangeAddress aRange );

    //-------------------------------------------------------------------------

    /** shows all outlined groups below a specific level.

        @param nLevel
            all outline levels from 1 to this value will be opened and
            the higher levels will be closed.

        @param nOrientation
            the orientation of the outlines (columns or rows).
     */
    void showLevel(
            [in] short nLevel,
            [in] com::sun::star::table::TableOrientation nOrientation );

};

//=============================================================================

}; }; }; };

#endif

