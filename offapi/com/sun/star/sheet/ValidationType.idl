/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ValidationType.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_ValidationType_idl__
#define __com_sun_star_sheet_ValidationType_idl__

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** used to specify which cell contents are treated as valid.
 */
published enum ValidationType
{
    //-------------------------------------------------------------------------

    /** any cell content is valid; no conditions are used.
     */
    ANY,

    //-------------------------------------------------------------------------

    /** any whole number matching the specified condition is valid.
     */
    WHOLE,

    //-------------------------------------------------------------------------

    /** any number matching the specified condition is valid.
     */
    DECIMAL,

    //-------------------------------------------------------------------------

    /** any date value matching the specified condition is valid.
     */
    DATE,

    //-------------------------------------------------------------------------

    /** any time value matching the specified condition is valid.
     */
    TIME,

    //-------------------------------------------------------------------------

    /** string is valid if its length matches the specified condition.
     */
    TEXT_LEN,

    //-------------------------------------------------------------------------

    /** Only strings from a specified list are valid.
     */
    LIST,

    //-------------------------------------------------------------------------

    /** The specified formula detemines which contents are valid.
     */
    CUSTOM

};

//=============================================================================

}; }; }; };

#endif

