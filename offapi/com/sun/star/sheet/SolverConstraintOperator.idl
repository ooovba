/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: SolverConstraintOperator.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_SolverConstraintOperator_idl__
#define __com_sun_star_sheet_SolverConstraintOperator_idl__

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** is used to specify the type of <type>SolverConstraint</type>.
 */
enum SolverConstraintOperator
{
    /// The cell value is less or equal to the specified value.
    LESS_EQUAL,

    /// The cell value is equal to the specified value.
    EQUAL,

    /// The cell value is greater or equal to the specified value.
    GREATER_EQUAL,

    /// The cell value is an integer value.
    INTEGER,

    /// The cell value is a binary value (0 or 1).
    BINARY
};

//=============================================================================

}; }; }; };

#endif

