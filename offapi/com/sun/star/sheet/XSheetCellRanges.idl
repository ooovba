/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSheetCellRanges.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSheetCellRanges_idl__
#define __com_sun_star_sheet_XSheetCellRanges_idl__

#ifndef __com_sun_star_container_XIndexAccess_idl__
#include <com/sun/star/container/XIndexAccess.idl>
#endif

#ifndef __com_sun_star_container_XEnumerationAccess_idl__
#include <com/sun/star/container/XEnumerationAccess.idl>
#endif

#ifndef __com_sun_star_table_CellRangeAddress_idl__
#include <com/sun/star/table/CellRangeAddress.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides methods to access cell ranges in a collection via index and
    other helper methods.

    @see com::sun::star::sheet::SheetCellRanges
 */
published interface XSheetCellRanges: com::sun::star::container::XIndexAccess
{
    //-------------------------------------------------------------------------

    /** returns the collection of all used cells.

        @see com::sun::star::sheet::Cells
     */
    com::sun::star::container::XEnumerationAccess getCells();

    //-------------------------------------------------------------------------

    /** creates a string with addresses of all contained cell ranges.

        <p>The range addresses are separated with semicolons. For instance
        the string could have the form "Sheet1.A1:C3;Sheet2.D5:F8".</p>

        @returns
            a string containing the addresses of all cell ranges.
     */
    string getRangeAddressesAsString();

    //-------------------------------------------------------------------------

    /** creates a sequence with addresses of all contained cell ranges.

        @returns
            a sequence with the addresses of all cell ranges.
     */
    sequence<com::sun::star::table::CellRangeAddress> getRangeAddresses();

};

//=============================================================================

}; }; }; };

#endif

