/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: UniqueCellFormatRanges.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_UniqueCellFormatRanges_idl__
#define __com_sun_star_sheet_UniqueCellFormatRanges_idl__

#ifndef __com_sun_star_container_XIndexAccess_idl__
#include <com/sun/star/container/XIndexAccess.idl>
#endif

#ifndef __com_sun_star_container_XEnumerationAccess_idl__
#include <com/sun/star/container/XEnumerationAccess.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a collection of equal-formatted cell range collections.

    <p>All cells inside a cell range collection (a member of this
    collection) have the same formatting attributes.</p>

    @see com::sun::star::sheet::SheetCellRange
    @see com::sun::star::sheet::SheetCellRanges
    @see com::sun::star::sheet::CellFormatRanges
 */
published service UniqueCellFormatRanges
{
    //-------------------------------------------------------------------------

    /** provides methods to access the contained cell range
        collections by index.

        @see com::sun::star::sheet::SheetCellRanges
     */
    interface com::sun::star::container::XIndexAccess;

    //-------------------------------------------------------------------------

    /** creates an enumeration of all cell range collections.

        @see com::sun::star::sheet::UniqueCellFormatRangesEnumeration
     */
    interface com::sun::star::container::XEnumerationAccess;

};

//=============================================================================

}; }; }; };

#endif

