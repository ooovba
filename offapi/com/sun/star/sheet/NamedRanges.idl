/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NamedRanges.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_NamedRanges_idl__
#define __com_sun_star_sheet_NamedRanges_idl__

#ifndef __com_sun_star_container_XNameAccess_idl__
#include <com/sun/star/container/XNameAccess.idl>
#endif

#ifndef __com_sun_star_sheet_XNamedRanges_idl__
#include <com/sun/star/sheet/XNamedRanges.idl>
#endif
#ifndef __com_sun_star_container_XEnumerationAccess_idl__
#include <com/sun/star/container/XEnumerationAccess.idl>
#endif
#ifndef __com_sun_star_container_XIndexAccess_idl__
#include <com/sun/star/container/XIndexAccess.idl>
#endif
#ifndef __com_sun_star_document_XActionLockable_idl__
#include <com/sun/star/document/XActionLockable.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a collection of named ranges in a spreadsheet document.

    <p>In fact a named range is a named formula expression. A cell range
    address is one possible content of a named range.</p>

    @see com::sun::star::sheet::SpreadsheetDocument
 */
published service NamedRanges
{
    //-------------------------------------------------------------------------

    /** provides access to the named ranges and to insert and remove them.
     */
    interface com::sun::star::sheet::XNamedRanges;

    //-------------------------------------------------------------------------

    /** provides access to the named ranges via index.

        @see com::sun::star::sheet::NamedRange

     */
    interface com::sun::star::container::XIndexAccess;

    //-------------------------------------------------------------------------

    /** creates an enumeration of all named ranges.

        @see com::sun::star::sheet::NamedRangesEnumeration

     */
    interface com::sun::star::container::XEnumerationAccess;

    //-------------------------------------------------------------------------

    /** provides methods to control the internal update of named ranges.

        @since OOo 3.0
     */
    [optional] interface com::sun::star::document::XActionLockable;

};

//=============================================================================

}; }; }; };

#endif

