/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XFormulaQuery.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XFormulaQuery_idl__
#define __com_sun_star_sheet_XFormulaQuery_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_sheet_XSheetCellRanges_idl__
#include <com/sun/star/sheet/XSheetCellRanges.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides methods to query cells for dependencies
    in formulas.

    <p>All methods return a collection of cell ranges.</p>

    @see com::sun::star::sheet::SheetRangesQuery
    @see com::sun::star::sheet::SheetCellRanges
 */
published interface XFormulaQuery: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** queries all dependent formula cells.

        <p>Dependent cells are cells containing formulas with references to
        the original cell.</p>

        @param bRecursive
            <FALSE/> = queries cells dependent from the original range(s),
            <TRUE/> = repeates query with all found cells
            (finds dependents of dependents, and so on).

        @return
            all dependent cells of any formula cell of the current
            cell range(s).
     */
    com::sun::star::sheet::XSheetCellRanges queryDependents(
            [in] boolean bRecursive );

    //-------------------------------------------------------------------------

    /** queries all precedent cells.

        <p>Precedent cells are cells which are referenced from a formula
        cell.</p>

        @param bRecursive
            <FALSE/> = queries precedent cells of the original range(s),
            <TRUE/> = repeates query with all found cells
            (finds precedents of precedents, and so on).

        @return
            all precedent cells of any formula cell of the current cell
            range(s).
     */
    com::sun::star::sheet::XSheetCellRanges queryPrecedents(
            [in] boolean bRecursive );

};

//=============================================================================

}; }; }; };

#endif

