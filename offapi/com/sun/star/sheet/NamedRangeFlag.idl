/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NamedRangeFlag.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_NamedRangeFlag_idl__
#define __com_sun_star_sheet_NamedRangeFlag_idl__

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** used to specify the purpose of a named range.
 */
published constants NamedRangeFlag
{
    //-------------------------------------------------------------------------

    /** The range contains filter criteria.
     */
    const long FILTER_CRITERIA = 1;

    //-------------------------------------------------------------------------

    /** The range can be used as a print range.
     */
    const long PRINT_AREA = 2;

    //-------------------------------------------------------------------------

    /** The range can be used as column headers for printing.
     */
    const long COLUMN_HEADER = 4;

    //-------------------------------------------------------------------------

    /** The range can be used as row headers for printing.
     */
    const long ROW_HEADER = 8;

};

//=============================================================================

}; }; }; };

#endif

