/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Scenario.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_Scenario_idl__
#define __com_sun_star_sheet_Scenario_idl__

#ifndef __com_sun_star_sheet_XScenario_idl__
#include <com/sun/star/sheet/XScenario.idl>
#endif

#ifndef __com_sun_star_sheet_XScenarioEnhanced_idl__
#include <com/sun/star/sheet/XScenarioEnhanced.idl>
#endif

#ifndef __com_sun_star_beans_XPropertySet_idl__
#include <com/sun/star/beans/XPropertySet.idl>
#endif

#ifndef __com_sun_star_container_XNamed_idl__
#include <com/sun/star/container/XNamed.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a scenario in a spreadsheet document.

 */
service Scenario
{
    interface com::sun::star::sheet::XScenario;

    [optional] interface com::sun::star::sheet::XScenarioEnhanced;

    interface com::sun::star::beans::XPropertySet;

    interface com::sun::star::container::XNamed;

    //-------------------------------------------------------------------------

    /** specifies if the scenario is active.
     */
    [optional, property] boolean IsActive;

    //-------------------------------------------------------------------------

    /** specifies the color of the border of the scenario.
     */
    [optional, property] long BorderColor;

    //-------------------------------------------------------------------------

    /** specifies if the scenario is protected.
     */
    [optional, property] boolean Protected;

    //-------------------------------------------------------------------------

    /** specifies if the scenario shows a border.
     */
    [optional, property] boolean ShowBorder;

    //-------------------------------------------------------------------------

    /** specifies if the scenario prints a border.
     */
    [optional, property] boolean PrintBorder;

    //-------------------------------------------------------------------------

    /** specifies if the data should be copied back into the scenario.
     */
    [optional, property] boolean CopyBack;

    //-------------------------------------------------------------------------

    /** specifies if the styles are copied.
     */
    [optional, property] boolean CopyStyles;

    //-------------------------------------------------------------------------

    /** specifies if the formulas are copied or only the results.
     */
    [optional, property] boolean CopyFormulas;


};

//=============================================================================

}; }; }; };

#endif

