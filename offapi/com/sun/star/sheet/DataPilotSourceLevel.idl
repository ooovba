/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DataPilotSourceLevel.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_DataPilotSourceLevel_idl__
#define __com_sun_star_sheet_DataPilotSourceLevel_idl__

#ifndef __com_sun_star_container_XNamed_idl__
#include <com/sun/star/container/XNamed.idl>
#endif

#ifndef __com_sun_star_sheet_XMembersSupplier_idl__
#include <com/sun/star/sheet/XMembersSupplier.idl>
#endif

#ifndef __com_sun_star_sheet_XDataPilotMemberResults_idl__
#include <com/sun/star/sheet/XDataPilotMemberResults.idl>
#endif

#ifndef __com_sun_star_beans_XPropertySet_idl__
#include <com/sun/star/beans/XPropertySet.idl>
#endif

#ifndef __com_sun_star_sheet_GeneralFunction_idl__
#include <com/sun/star/sheet/GeneralFunction.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a level in a data pilot source hierarchy.

    @see com::sun::star::sheet::DataPilotSourceHierarchy
    @see com::sun::star::sheet::DataPilotSource
 */
published service DataPilotSourceLevel
{
    //-------------------------------------------------------------------------

    /** provides access to the name of the level, i.e. used in collections.
     */
    interface com::sun::star::container::XNamed;

    //-------------------------------------------------------------------------

    /** provides access to the collection of members of this level.
     */
    interface com::sun::star::sheet::XMembersSupplier;

    //-------------------------------------------------------------------------

    /** provides access to a sequence of results of this level.
     */
    interface com::sun::star::sheet::XDataPilotMemberResults;

    //-------------------------------------------------------------------------

//!published service PropertySet
    /** provides access to the properties.
     */
    interface com::sun::star::beans::XPropertySet;

    //=========================================================================

    /** specifies the subtotals that are inserted for the level.

        <p>The subtotals are calculated with the members of this level.</p>
     */
    [property] sequence< com::sun::star::sheet::GeneralFunction > SubTotals;

    //-------------------------------------------------------------------------

    /** specifies whether empty members are shown.
     */
    [property] boolean ShowEmpty;

};

//=============================================================================

}; }; }; };

#endif

