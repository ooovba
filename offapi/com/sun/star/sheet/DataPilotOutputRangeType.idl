/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DataPilotOutputRangeType.idl,v $
 *
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_DataPilotOutputRangeType_idl__
#define __com_sun_star_sheet_DataPilotOutputRangeType_idl__

module com { module sun { module star { module sheet {

//============================================================================

/** specifies region type of DataPilot table range

    <p>This constant set is used to indicate the type of output range desired when
    <method>XDataPilotTable2::getOutputRangeByType</method> is called, which 
    returns a different cell range depending upon the value passed to it as the argument.</p>

    @see com::sun::star::sheet::XDataPilotTable2

    @since OOo 3.0.0
 */
constants DataPilotOutputRangeType
{
    //------------------------------------------------------------------------

    /** whole DataPilot output range including the header area above the table
        where the filter and page field buttons are located. */
    const long WHOLE = 0;

    //------------------------------------------------------------------------

    /** whole table but without the header area where the filter and page field 
        buttons are located. */
    const long TABLE = 1;

    //------------------------------------------------------------------------

    /** result area where the result values are displayed.  This also includes
        the column and row subtotal areas when they are displayed. */
    const long RESULT = 2;
};

//============================================================================

}; }; }; };



#endif
