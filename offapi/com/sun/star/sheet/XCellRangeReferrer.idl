/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XCellRangeReferrer.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XCellRangeReferrer_idl__
#define __com_sun_star_sheet_XCellRangeReferrer_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_table_XCellRange_idl__
#include <com/sun/star/table/XCellRange.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** allows direct access to the cells in a named range or to the cells
    which are visible in a view, without the need to get the document
    object first.

    @see com::sun::star::sheet::NamedRange
    @see com::sun::star::sheet::DatabaseRange
    @see com::sun::star::sheet::SpreadsheetViewPane
 */
published interface XCellRangeReferrer: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** returns the cell range object that is represented.
     */
    com::sun::star::table::XCellRange getReferredCells();

};

//=============================================================================

}; }; }; };

#endif

