/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: CellAreaLink.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_CellAreaLink_idl__
#define __com_sun_star_sheet_CellAreaLink_idl__

#ifndef __com_sun_star_sheet_XAreaLink_idl__
#include <com/sun/star/sheet/XAreaLink.idl>
#endif

#ifndef __com_sun_star_util_XRefreshable_idl__
#include <com/sun/star/util/XRefreshable.idl>
#endif

#ifndef __com_sun_star_beans_XPropertySet_idl__
#include <com/sun/star/beans/XPropertySet.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a linked cell range.

    <p>A linked cell range is a range which is linked to an equal-sized
    range in an external document. The contents of the external range is
    copied into the range of this document.</p>

    @see com::sun::star::sheet::CellAreaLinks
 */
published service CellAreaLink
{
    //-------------------------------------------------------------------------

    /** provides methods to change the settings of the linked cell range.
     */
    interface com::sun::star::sheet::XAreaLink;

    //-------------------------------------------------------------------------

    /** provides methods to reload the external data.
     */
    interface com::sun::star::util::XRefreshable;

    //-------------------------------------------------------------------------

//!published service PropertySet
    /** provides access to the properties.
     */
    interface com::sun::star::beans::XPropertySet;

     //========================================================================

    /** specifies the URL of the source document.
     */
    [property] string Url;

    //-------------------------------------------------------------------------

    /** specifies the name of the filter used to load the source document.
     */
    [property] string Filter;

    //-------------------------------------------------------------------------

    /** specifies the filter options needed to load the source document.
     */
    [property] string FilterOptions;

    //-------------------------------------------------------------------------

    /** specifies the delay time between two refresh actions in seconds.

    @deprecated

     */
    [property] long RefreshDelay;

    //-------------------------------------------------------------------------

    /** specifies the time between two refresh actions in seconds.

        @since OOo 2.0.0
     */
    [optional, property] long RefreshPeriod;


};

//=============================================================================

}; }; }; };

#endif

