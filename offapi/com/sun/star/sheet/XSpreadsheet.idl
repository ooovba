/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSpreadsheet.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSpreadsheet_idl__
#define __com_sun_star_sheet_XSpreadsheet_idl__

#ifndef __com_sun_star_sheet_XSheetCellRange_idl__
#include <com/sun/star/sheet/XSheetCellRange.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

 published interface XSheetCellCursor;

//=============================================================================

/** provides methods to create a cell range cursor.
 */
published interface XSpreadsheet: com::sun::star::sheet::XSheetCellRange
{
    //-------------------------------------------------------------------------

    /** creates a cell cursor including the whole spreadsheet.

        @see com::sun::star::sheet::SheetCellCursor
     */
    com::sun::star::sheet::XSheetCellCursor createCursor();

    //-------------------------------------------------------------------------

    /** creates a cell cursor to travel in the given range context.

        @param aRange
            the cell range for the cursor.

        @see com::sun::star::sheet::SheetCellCursor
     */
    com::sun::star::sheet::XSheetCellCursor createCursorByRange(
            [in] com::sun::star::sheet::XSheetCellRange aRange );

};

//=============================================================================

}; }; }; };

#endif

