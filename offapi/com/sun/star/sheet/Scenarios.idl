/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Scenarios.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_Scenarios_idl__
#define __com_sun_star_sheet_Scenarios_idl__

#ifndef __com_sun_star_sheet_XScenariosSupplier_idl__
#include <com/sun/star/sheet/XScenariosSupplier.idl>
#endif
#ifndef __com_sun_star_container_XEnumerationAccess_idl__
#include <com/sun/star/container/XEnumerationAccess.idl>
#endif
#ifndef __com_sun_star_container_XIndexAccess_idl__
#include <com/sun/star/container/XIndexAccess.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a collection of scenarios.
 */
published service Scenarios
{
    //-------------------------------------------------------------------------

    /** provides access via name to the scenarios in the collection.
     */
    interface com::sun::star::sheet::XScenarios;

    //-------------------------------------------------------------------------

    /** creates an enumeration of scenarios.

        @see com::sun::star::sheet::ScenariosEnumeration
     */
    interface com::sun::star::container::XEnumerationAccess;

    //-------------------------------------------------------------------------

    /** provides access to the scenarios in the collection via index.

        @see com::sun::star::sheet::Spreadsheet

     */
    interface com::sun::star::container::XIndexAccess;

};

//=============================================================================

}; }; }; };

#endif

