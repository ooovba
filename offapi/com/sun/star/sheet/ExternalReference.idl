/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ExternalReference.idl,v $
 * $Revision: 1.1.2.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_ExternalReference_idl__
#define __com_sun_star_sheet_ExternalReference_idl__

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** Data structure to store information about an external reference.  An
    external reference can be either a single cell reference, a cell range
    reference, or a named range.

    @see FormulaMapGroupSpecialOffset::PUSH

    @since OOo 3.1
 */
struct ExternalReference
{
    //-------------------------------------------------------------------------

    /** Index of an externally linked document.  Each externally-linked document
        has a unique index value.

        <p>You can get the index value of an external document from the
        corresponding <type scope="com::sun::star::sheet">ExternalDocLink</type>
        instance through its attribute <type scope="com::sun::star::sheet::ExternalDocLink">TokenIndex</type>.</p>

        @see com::sun::star::sheet::ExternalDocLink
        @see com::sun::star::sheet::ExternalDocLink::TokenIndex
     */
    long Index;

#if 0
    //-------------------------------------------------------------------------

    /** Name of the sheet that the external reference points to.

        <p>In case of a cell range reference that spans across multiple
        sheets, this is the name of the first sheet in that range.</p>

        <p>Note that an external range name ignores this value at the moment,
        but <i>it may make use of this data in the future when Calc supports a
        sheet-specific range name.</i></p>
     */
    string SheetName;
#endif

    //-------------------------------------------------------------------------

    /** Reference data.

        <p>This can store either <type>SingleReference</type> for a single
        cell reference, <type>ComplexReference</type> for a cell range
        reference, or simply a <type>string</type> for a defined name.</p>

        <p>The <member>SingleReference::Sheet</member> member shall contain
        the index of the external sheet cache containing the values of the
        externally referenced cells.</p>

        @see com::sun::star::sheet::SingleReference
        @see com::sun::star::sheet::ComplexReference
     */
    any Reference;
};

//=============================================================================

}; }; }; };

#endif
