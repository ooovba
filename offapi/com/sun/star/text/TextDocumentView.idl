/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextDocumentView.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_TextDocumentView_idl__ 
#define __com_sun_star_text_TextDocumentView_idl__ 
 
#ifndef __com_sun_star_view_OfficeDocumentView_idl__ 
#include <com/sun/star/view/OfficeDocumentView.idl> 
#endif 
 
#ifndef __com_sun_star_view_XViewSettingsSupplier_idl__ 
#include <com/sun/star/view/XViewSettingsSupplier.idl> 
#endif 
 
#ifndef __com_sun_star_text_XTextViewCursorSupplier_idl__ 
#include <com/sun/star/text/XTextViewCursorSupplier.idl> 
#endif 
 
#ifndef __com_sun_star_beans_XPropertySet_idl__ 
#include <com/sun/star/beans/XPropertySet.idl> 
#endif 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
/** specifies the view of a <type>TextDocument</type>.
 */
published service TextDocumentView
{ 
    service com::sun::star::view::OfficeDocumentView; 
 
     
    /** This interface permits access to the properties of the view.
     */
    interface com::sun::star::view::XViewSettingsSupplier; 
 
     
    /** This interface makes it possible to access the cursor which belongs 
                to the view and can be visible for the user.
     */
    interface com::sun::star::text::XTextViewCursorSupplier; 
 

    /** Gives access to the objects properties.
        
        @since OOo 2.0
     */
    [optional] interface com::sun::star::beans::XPropertySet; 


    //-------------------------------------------------------------------------
    /** returns the number of pages in the document

        <p>Since the document needs to be formatted to get the result
        obtaining this value may take some time.</p>

        @since OOo 2.0
     */
    [optional, property, readonly] long PageCount;
    
    //-------------------------------------------------------------------------
    /** returns the number of lines in the document

        <p>Since the document needs to be formatted to get the result
        obtaining this value may take some time.</p>

        <p>Empty paragraphs are not counted.</p>
        
        @since OOo 2.0
     */
    [optional, property, readonly] long LineCount;
    
    //-------------------------------------------------------------------------
    /** specifies if spellchecking should be done while typing.

        @since OOo 2.0
     */
    [optional, property] boolean IsConstantSpellcheck;
    
    //-------------------------------------------------------------------------
    /** specifies if the marks for misspelled text should be displayed.

        @since OOo 2.0
     */
    [optional, property] boolean IsHideSpellMarks;
    
    //-------------------------------------------------------------------------
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
