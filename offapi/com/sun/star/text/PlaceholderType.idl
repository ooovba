/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: PlaceholderType.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_PlaceholderType_idl__ 
#define __com_sun_star_text_PlaceholderType_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
// DocMerge from idl: constants com::sun::star::text::PlaceholderType
/** These constants define how the place-holder text fields act in a document.
 */
published constants PlaceholderType
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::PlaceholderType::TEXT
    /** The field represents a piece of text.
     */
    const short TEXT = 0; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::PlaceholderType::TABLE
    /** The field initiates the insertion of a text table.
     */
    const short TABLE = 1; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::PlaceholderType::TEXTFRAME
    /** The field initiates the insertion of a text frame.
     */
    const short TEXTFRAME = 2; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::PlaceholderType::GRAPHIC
    /** The field initiates the insertion of a graphic object.
     */
    const short GRAPHIC = 3; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::PlaceholderType::OBJECT
    /** The field initiates the insertion of an embedded object.
     */
    const short OBJECT = 4; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
