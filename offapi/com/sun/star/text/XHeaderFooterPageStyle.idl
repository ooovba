/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XHeaderFooterPageStyle.idl,v $
 * $Revision: 1.10.122.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_XHeaderFooterPageStyle_idl__ 
#define __com_sun_star_text_XHeaderFooterPageStyle_idl__ 

#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 

#ifndef __com_sun_star_text_XHeaderFooter_idl__ 
#include <com/sun/star/text/XHeaderFooter.idl> 
#endif 


//============================================================================= 

 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
// DocMerge from xml: interface com::sun::star::text::XHeaderFooterPageStyle
/** @deprecated     Use the properties of
            <type scope="com::sun::star::style">PageProperties</type> instead
 */
published interface XHeaderFooterPageStyle: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: method com::sun::star::text::XHeaderFooterPageStyle::getHeader
    /** @returns  
                the interface of the header.
     */
    com::sun::star::text::XHeaderFooter getHeader(); 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: method com::sun::star::text::XHeaderFooterPageStyle::getFooter
    /** @returns 
                the interface of the footer.
     */
    com::sun::star::text::XHeaderFooter getFooter(); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  


#endif 
