/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: UserDataPart.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_UserDataPart_idl__ 
#define __com_sun_star_text_UserDataPart_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
// DocMerge from idl: constants com::sun::star::text::UserDataPart
/** These constants define which part of the user data is displayed in a 
        user data text field (service "sun.one.text.TextField.ExtendedUser")
 */
published constants UserDataPart
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::COMPANY
    /** The field shows the company name.
     */
    const short COMPANY = 0; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::FIRSTNAME
    /** The field shows the first name.
     */
    const short FIRSTNAME = 1; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::NAME
    /** The field shows the name.
     */
    const short NAME = 2; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::SHORTCUT
    /** The field shows the initials.
     */
    const short SHORTCUT = 3; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::STREET
    /** The field shows the street.
     */
    const short STREET = 4; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::COUNTRY
    /** The field shows the country.
     */
    const short COUNTRY = 5; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::ZIP
    /** The field shows the zip code.
     */
    const short ZIP = 6; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::CITY
    /** The field shows the city.
     */
    const short CITY = 7; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::TITLE
    /** The field shows the title.
     */
    const short TITLE = 8; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::POSITION
    /** The field shows the position.
     */
    const short POSITION = 9; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::PHONE_PRIVATE
    /** The field shows the no of the private phone.
     */
    const short PHONE_PRIVATE = 10; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::PHONE_COMPANY
    /** The field shows the number of the business phone.
     */
    const short PHONE_COMPANY = 11; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::FAX
    /** The field shows the fax no.
     */
    const short FAX = 12; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::EMAIL
    /** The field shows the e-Mail.
     */
    const short EMAIL = 13; 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: value com::sun::star::text::UserDataPart::STATE
    /** The field shows the state.
     */
    const short STATE = 14; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
