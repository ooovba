/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMailMergeListener.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_XMailMergeListener_idl__
#define __com_sun_star_text_XMailMergeListener_idl__


#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif
#ifndef __com_sun_star_text_MailMergeEvent_idl__
#include <com/sun/star/text/MailMergeEvent.idl>
#endif


//=============================================================================

module com { module sun { module star { module text {

//=============================================================================
/**  used to notify listeners about mail merge events.

    <p>Registered listeners will be notified with a
    <type scope="com::sun::star::text">MailMergeEvent</type>
    when a document is about to get merged.</p>
      
    @see    com::sun::star::text::MailMerge
    @see    com::sun::star::text::MailMergeEvent

    @since OOo 1.1.2
*/
published interface XMailMergeListener : com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** Notifies the listener about mail merge events.
    
        @param aEvent
        The Event containing the model of the document to be merged
        that is send to the listener.
    */
    void notifyMailMergeEvent( 
                [in] com::sun::star::text::MailMergeEvent aEvent );
};

//=============================================================================

}; }; }; };

#endif
