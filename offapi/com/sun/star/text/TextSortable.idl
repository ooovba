/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextSortable.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_util_TextSortable_idl__ 
#define __com_sun_star_util_TextSortable_idl__ 
  
#ifndef __com_sun_star_text_TextSortDescriptor_idl__ 
#include <com/sun/star/text/TextSortDescriptor.idl> 
#endif 

#ifndef __com_sun_star_util_XSortable_idl__ 
#include <com/sun/star/util/XSortable.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
/** provides an interface for sorting.

    @deprecated
    
    @since OOo 1.1.2
    
 */
published service TextSortable
{
        /** the properties returned by a call to the 'createSortDescriptor' method
            of this interfaces implementation are those of the
            'com.sun.star.text.TextSortDescriptor' service.
         
           @see <type scope="com::sun::star::text">TextSortDescriptor</type>
         */
        interface com::sun::star::util::XSortable;
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
