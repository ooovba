/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Bookmark.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_Bookmark_idl__ 
#define __com_sun_star_text_Bookmark_idl__ 
 
#ifndef __com_sun_star_text_TextContent_idl__ 
#include <com/sun/star/text/TextContent.idl> 
#endif 
 
#ifndef __com_sun_star_container_XNamed_idl__ 
#include <com/sun/star/container/XNamed.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
// DocMerge from xml: service com::sun::star::text::Bookmark
/** A bookmark is a <type>TextContent</type>, which is like a jump 
    target or a label.
 */
published service Bookmark
{ 
    // DocMerge: empty anyway
    service com::sun::star::text::TextContent; 
 
     
    // DocMerge from xml: service com::sun::star::text::Bookmark: interface com::sun::star::container::XNamed
    /** This interface specifies the name of the bookmark.
     */
    interface com::sun::star::container::XNamed; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
