/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XTextFieldsSupplier.idl,v $
 * $Revision: 1.11.122.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_XTextFieldsSupplier_idl__ 
#define __com_sun_star_text_XTextFieldsSupplier_idl__ 

#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 

#ifndef __com_sun_star_container_XEnumerationAccess_idl__ 
#include <com/sun/star/container/XEnumerationAccess.idl> 
#endif 

#ifndef __com_sun_star_container_XNameAccess_idl__ 
#include <com/sun/star/container/XNameAccess.idl> 
#endif 


//============================================================================= 

 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
// DocMerge from xml: interface com::sun::star::text::XTextFieldsSupplier
/** makes it possible to access the text fields used in this context
    (e.g. this document).@see com::sun::star::sheet::SpreadsheetDocument 
        @see TextDocument
 */
published interface XTextFieldsSupplier: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: method com::sun::star::text::XTextFieldsSupplier::getTextFields
    /** @returns 
                the collection of <type>TextField</type> instances 
                in this context (i.e. this document).
     */
    com::sun::star::container::XEnumerationAccess getTextFields(); 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from idl: method com::sun::star::text::XTextFieldsSupplier::getTextFieldMasters
    /** @returns 
                the collection of <type>TextFieldMaster</type> instances 
                which are defined in this context (i.e. this document).
     */
    com::sun::star::container::XNameAccess getTextFieldMasters(); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  

#endif 
