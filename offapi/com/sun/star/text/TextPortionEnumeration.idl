/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextPortionEnumeration.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_TextPortionEnumeration_idl__
#define __com_sun_star_text_TextPortionEnumeration_idl__

#ifndef __com_sun_star_container_XEnumeration_idl__
#include <com/sun/star/container/XEnumeration.idl>
#endif


//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

/** This interface creates an enumeration of paragraph within a text document.
 The elements created by this enumeration contains either parts of text with
 equal properties or text content elements like text fields, reference marks or bookmarks.
 */
published service TextPortionEnumeration
{
    /** This interface provides access to the elements of a paragraph in a text document.
     */
    interface 	::com::sun::star::container::XEnumeration;
};

//=============================================================================

}; }; }; };
#endif

