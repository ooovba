/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextContentCollection.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_TextContentCollection_idl__ 
#define __com_sun_star_text_TextContentCollection_idl__ 
 
#ifndef __com_sun_star_container_XNameAccess_idl__ 
#include <com/sun/star/container/XNameAccess.idl> 
#endif 
 
#ifndef __com_sun_star_container_XContainer_idl__ 
#include <com/sun/star/container/XContainer.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module text {  
 
//============================================================================= 
 
// DocMerge from xml: service com::sun::star::text::TextContentCollection
/** Objects of this type are collections of text contents of the same type.@see Text
 */
published service TextContentCollection
{ 
 
     
    // DocMerge from xml: service com::sun::star::text::TextContentCollection: interface com::sun::star::container::XNameAccess
    /** Each text content is accessible by its name, which is unique  
                within the collection.
     */
    interface com::sun::star::container::XNameAccess; 
 
     
    // DocMerge from xml: service com::sun::star::text::TextContentCollection: interface com::sun::star::container::XContainer
    /** The insertion and removal of text contents of this type are 
                broadcasted via this interface.
     */
    interface com::sun::star::container::XContainer; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
