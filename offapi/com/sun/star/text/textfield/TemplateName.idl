/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TemplateName.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_text_textfield_TemplateName_idl__
#define __com_sun_star_text_textfield_TemplateName_idl_

#include <com/sun/star/text/TextField.idl>

//=============================================================================

module com { module sun { module star { module text { module textfield {

//=============================================================================
/** specifies service text field that displays the name of the template the document
 has been created from.
    @see com::sun::star::text::TextField
*/
published service TemplateName
{
    service com::sun::star::text::TextField;
    /** determins the format the template file name is displayed as specified in
        <type scope="com::sun::star::text">FilenameDisplayFormat</type>.
      */
    [property]short FileFormat;

};

//=============================================================================

}; }; }; }; };

#endif

