/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Database.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_text_textfield_Database_idl__
#define __com_sun_star_text_textfield_Database_idl_

#include <com/sun/star/text/TextField.idl>
#include <com/sun/star/text/DependentTextField.idl>

//=============================================================================

module com { module sun { module star { module text { module textfield {

//=============================================================================
/** specifies service of a database text field which is used as mail merge field.
    @see com::sun::star::text::TextField
*/
published service Database
{
    service  com::sun::star::text::DependentTextField;

    /** contains the database content that was merged in the last database merge action.
     Initially it contains the colum name in parenthesis (<>).
     */
    [property]string Content;
    /** contains the current content of the text field.
        <p> This property is escpecially usefull for import/export purposes. </p>
     */
    [property]string CurrentPresentation;
    /** determins whether the number format is number display format is read
     from the database settings.
     */
    [property]boolean DataBaseFormat;
    /** this is the number format for this field.
        @see com::sun::star::util::NumberFormatter
    */
    [property]long NumberFormat;
};

//=============================================================================

}; }; }; }; };

#endif

