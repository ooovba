/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Info3.idl,v $
 * $Revision: 1.4.122.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_text_textfield_docinfo_Info3_idl__
#define __com_sun_star_text_textfield_docinfo_Info3_idl_

#include <com/sun/star/text/TextField.idl>

//=============================================================================

module com { module sun { module star {
    module text { module textfield { module docinfo {

//=============================================================================
/** specifies service of a text field that provides the Info3 field that is contained
    in the document information.
    @deprecated this service is no longer implemented as of OOo 3.0;
        use com::sun::star::text::textfield::docinfo::Custom instead.
    @see com::sun::star::text::TextField
*/
published service Info3
{
    /** contains content information.
     */
    [property] string Content;
    /** contains the current content of the text field.
        <p> This property is escpecially useful for import/export purposes. </p>
     */
    [property]string CurrentPresentation;
    /** If this flag is set to <FALSE/> the content updated everytime the document
        information is changed.
     */
    [property]boolean IsFixed;
};

//=============================================================================

}; }; }; }; }; };

#endif

