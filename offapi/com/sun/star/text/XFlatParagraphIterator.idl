/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XFlatParagraphIterator.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_text_XFlatParagraphIterator_idl__
#define __com_sun_star_text_XFlatParagraphIterator_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_lang_IllegalArgumentException_idl__
#include <com/sun/star/lang/IllegalArgumentException.idl>
#endif

#ifndef __com_sun_star_text_XFlatParagraph_idl__
#include <com/sun/star/text/XFlatParagraph.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module text {

//=============================================================================

/** provides functionality to ...

    @since OOo 3.0
 */

interface XFlatParagraphIterator: com::sun::star::uno::XInterface
{

    //-------------------------------------------------------------------------
    /** get the first flat paragraph to be checked or an empty reference if
        there are no more paragraphs to check.

        @returns
                the paragraph.
    */
    com::sun::star::text::XFlatParagraph getFirstPara();

    //-------------------------------------------------------------------------
    /** get the next flat paragraph to be checked or an empty reference if
        there are no more paragraphs to check.

        @returns
                the paragraph.
    */
    com::sun::star::text::XFlatParagraph getNextPara();

    //-------------------------------------------------------------------------
    /** get the last flat paragraph

        @returns
                the paragraph.
    */
    com::sun::star::text::XFlatParagraph getLastPara();

    //-------------------------------------------------------------------------
    /** get the flat paragraph before this one

        @param  xPara
                the current flat paragraph

        @returns
                the flat paragraph.

        @throws IllegalArgumentException
                if any argument is wrong.
    */
    com::sun::star::text::XFlatParagraph getParaBefore( [in] com::sun::star::text::XFlatParagraph xPara )
            raises( com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /** get the flat paragraph just following this one

        @param  xPara
                the current flat paragraph

        @returns
                the flat paragraph.

        @throws IllegalArgumentException
                if any argument is wrong.
    */
    com::sun::star::text::XFlatParagraph getParaAfter( [in] com::sun::star::text::XFlatParagraph xPara )
            raises( com::sun::star::lang::IllegalArgumentException );
};

//=============================================================================

}; }; }; };

#endif
