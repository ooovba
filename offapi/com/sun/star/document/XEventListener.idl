/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XEventListener.idl,v $
 * $Revision: 1.9.12.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_document_XEventListener_idl__
#define __com_sun_star_document_XEventListener_idl__

#ifndef __com_sun_star_lang_XEventListener_idl__
#include <com/sun/star/lang/XEventListener.idl>
#endif

#ifndef __com_sun_star_document_EventObject_idl__
#include <com/sun/star/document/EventObject.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module document {

//=============================================================================
/** makes it possible to register listeners, which are called whenever
    a document or document content event occurs

    <p>Such events will be broadcasted by a <type>XEventBroadcaster</type>.</p>

    @deprecated
    @see XDocumentEventListener
 */
published interface XEventListener: com::sun::star::lang::XEventListener
{
    //-------------------------------------------------------------------------
    /** is called whenever a document event (see <type>EventObject</type>) occurs

        @param Event
            specifies the event type
     */
    [oneway] void notifyEvent( [in] EventObject Event );
};

//=============================================================================

}; }; }; };

#endif
