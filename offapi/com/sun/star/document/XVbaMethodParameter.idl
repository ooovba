/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright IBM Corporation 2009
 * Copyright 2009 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XVbaMethodParameter,v $
 * $Revision: 1.13 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __org_openoffice_vba_XVbaMethodParameter_idl__
#define __org_openoffice_vba_XVbaMethodParameter_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

//============================================================================= 

module com {  module sun {  module star {  module document {

//============================================================================= 
//gives access to vba method input/output parameters
//
//some OO objects need to implement this interface to support the passing of input/output parameters
//for certain VBA events

interface XVbaMethodParameter : com::sun::star::uno::XInterface
{
	//------------------------------------------------------------------------- 
	 
	/** sets the value of the parameter with the specified name.
	 */
	void setVbaMethodParameter( [in] string PropertyName, 
			 [in] any Value ); 
 
	//------------------------------------------------------------------------- 

	/** returns the value of the parameter with the specified name.
     */
	any getVbaMethodParameter( [in] string PropertyName ); 
};

//============================================================================= 

}; }; }; };
#endif
