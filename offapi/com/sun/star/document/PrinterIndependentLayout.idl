/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: PrinterIndependentLayout.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_document_PrinterIndependentLayout_idl__
#define __com_sun_star_document_PrinterIndependentLayout_idl__

//=============================================================================

module com {   module sun {   module star {   module document {

//=============================================================================

/** specifies whether the document printer metric is used.

    @since OOo 1.1.2
*/
published constants PrinterIndependentLayout
{
    /** use printer-dependent metrics for layout */
    const short DISABLED = 1;

    /** use printer-independent metrics for layout, 
        assuming a generic 600dpi printer */
    const short LOW_RESOLUTION = 2;
    
    /** @deprecated ENABLED changed to LOW_RESOLUTION, 
                    to distinguish from HIGH_RESOLUTION */
    const short ENABLED = LOW_RESOLUTION;

    /** use printer-independent metrics for layout, 
        assuming a generic high-resolution printer (4800dpi) */
    const short HIGH_RESOLUTION = 3;

    /** [future:] use printer-independent-layout settings from parent object
    const short LIKE_PARENT = 4;
     */
};
    
//=============================================================================

}; }; }; };

#endif
