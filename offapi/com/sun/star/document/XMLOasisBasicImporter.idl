/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMLOasisBasicImporter.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_document_XMLOasisBasicImporter_idl__
#define __com_sun_star_document_XMLOasisBasicImporter_idl__

#ifndef __com_sun_star_document_XImporter_idl__
#include <com/sun/star/document/XImporter.idl>
#endif

#ifndef __com_sun_star_xml_sax_XDocumentHandler_idl__ 
#include <com/sun/star/xml/sax/XDocumentHandler.idl> 
#endif


//=============================================================================

module com { module sun { module star { module document {

//=============================================================================
    
/** Filter for importing Basic macros from the OASIS Open Office file format.

    <p>The <method>XImporter::setTargetDocument</method> method must be 
    called in order to provide the import component with the target document
    to which the data should be imported.
    The <type scope="com::sun::star::xml::sax">XDocumentHandler</type>
    interface is used to stream the XML data into the filter.</p>
        
    @since OOo 2.0.0
 */
published service XMLOasisBasicImporter
{
    //-------------------------------------------------------------------------

    /** sets the target document for this filter.
     */    
    interface com::sun::star::document::XImporter;

    //-------------------------------------------------------------------------

    /** receives notification of general document events.
     */    
    interface com::sun::star::xml::sax::XDocumentHandler;
    
};

//=============================================================================

}; }; }; };

#endif
