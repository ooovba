/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NumberFormatter.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_util_NumberFormatter_idl__ 
#define __com_sun_star_util_NumberFormatter_idl__ 
 
#ifndef __com_sun_star_util_XNumberFormatter_idl__ 
#include <com/sun/star/util/XNumberFormatter.idl> 
#endif 
 
#ifndef __com_sun_star_util_XNumberFormatPreviewer_idl__ 
#include <com/sun/star/util/XNumberFormatPreviewer.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module util {  
 
//============================================================================= 
 
/** represents an object which can format numbers and strings.

    <p>A NumberFormatter, if available, can be created by the global
    service manager.</p>
 */
published service NumberFormatter
{ 
    /** is used to format or parse numbers using formats from a
        <type>NumberFormats</type> object.
     */
    interface com::sun::star::util::XNumberFormatter; 
    
    /** is used to format numbers using a number format string
        that is not inserted into a <type>NumberFormats</type> object.
     */
    interface com::sun::star::util::XNumberFormatPreviewer; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif

