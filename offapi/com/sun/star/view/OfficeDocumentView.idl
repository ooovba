/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: OfficeDocumentView.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_view_OfficeDocumentView_idl__ 
#define __com_sun_star_view_OfficeDocumentView_idl__ 
 
#ifndef __com_sun_star_view_XSelectionSupplier_idl__ 
#include <com/sun/star/view/XSelectionSupplier.idl> 
#endif 
 
#ifndef __com_sun_star_view_XViewSettingsSupplier_idl__ 
#include <com/sun/star/view/XViewSettingsSupplier.idl> 
#endif 
 
#ifndef __com_sun_star_view_XControlAccess_idl__ 
#include <com/sun/star/view/XControlAccess.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module view {  
 
//============================================================================= 
 
/** specifies a view of a standard office document.
 */
published service OfficeDocumentView
{ 
    /** This mandatory interface gives access to the current user selection 
                within this office document view.

        <p>The type of the selection depends on the actual document type.</p>
     */
    interface com::sun::star::view::XSelectionSupplier; 
 
     
    /** This optional interface gives access to the view properties within this 
        control for an office document.

        @see ViewSettings
     */
    [optional] interface com::sun::star::view::XViewSettingsSupplier; 
 
     
    /** Within this office document view, this optional interface gives 
        access to the controls which belong to specified control models.
     */
    [optional] interface com::sun::star::view::XControlAccess; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
