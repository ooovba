/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: PrintJobEvent.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_view_PrintJobEvent_idl__
#define __com_sun_star_view_PrintJobEvent_idl__

#ifndef __com_sun_star_lang_EventObject_idl__
#include <com/sun/star/lang/EventObject.idl>
#endif

#ifndef __com_sun_star_view_PrintableState_idl__
#include <com/sun/star/view/PrintableState.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module view {

//=============================================================================

/** specifies the print progress of an <type>XPrintJob</type>.

    <p><member scope="com::sun::star::lang">EventObject::Source</member>
    contains the <type>XPrintJob</type> having changed its state</p>.

    @since OOo 1.1.2

 */
published struct PrintJobEvent : com::sun::star::lang::EventObject
{
    ///	contains the current state.
    PrintableState State;
};

//=============================================================================

}; }; }; };

#endif
