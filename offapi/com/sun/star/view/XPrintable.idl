/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XPrintable.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_view_XPrintable_idl__ 
#define __com_sun_star_view_XPrintable_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_beans_PropertyValue_idl__ 
#include <com/sun/star/beans/PropertyValue.idl> 
#endif 
 
#ifndef __com_sun_star_lang_IllegalArgumentException_idl__ 
#include <com/sun/star/lang/IllegalArgumentException.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module view {  
 
//============================================================================= 
 
/** offers printing functionality.
 */
published interface XPrintable: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
    /** @returns 
            a descriptor of the current printer. 
         
        <p>The attributes of the current printer are used for formatting. 
         
        @see PrinterDescriptor
     */
    sequence<com::sun::star::beans::PropertyValue> getPrinter(); 
 
    //------------------------------------------------------------------------- 
    /** assigns a new printer to the object.

        <p>Setting a new printer will cause reformatting.
        
        @see PrinterDescriptor
     */
    void setPrinter( [in] sequence<com::sun::star::beans::PropertyValue> aPrinter ) 
            raises( com::sun::star::lang::IllegalArgumentException ); 
 
    //------------------------------------------------------------------------- 
    /** prints the object.

        @param xOptions
            specifies the number of copies and some other values which do not
            affect formatting.

        @see PrintOptions
     */
    void print( [in] sequence<com::sun::star::beans::PropertyValue> xOptions ) 
            raises( com::sun::star::lang::IllegalArgumentException ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
