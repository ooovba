/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XPrintJob.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_view_XPrintJob_idl__
#define __com_sun_star_view_XPrintJob_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_view_XPrintable_idl__
#include <com/sun/star/view/XPrintable.idl>
#endif

#ifndef __com_sun_star_beans_PropertyValue_idl__
#include <com/sun/star/beans/PropertyValue.idl>
#endif


//=============================================================================

module com {  module sun {  module star {  module view {

//-----------------------------------------------------------------------------
/** allows for getting information about a print job.

    <p><type>XPrintJob</type> is implemented by print jobs that are created by
    classes that implement <type>XPrintable</type>. It gives information about
    the context of the print job.</p>

    @see XPrintJobListener

    @since OOo 1.1.2
 */
published interface XPrintJob : com::sun::star::uno::XInterface
{
    /** returns the PrintOptions used for the print job
     */
    sequence<com::sun::star::beans::PropertyValue> getPrintOptions();

    /** returns the Printer used for the print job
     */
    sequence<com::sun::star::beans::PropertyValue> getPrinter();

    /** returns the printed object used for the print job
     */
    XPrintable getPrintable();

    void cancelJob();
};

}; }; }; };

#endif
