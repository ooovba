/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XNode.idl,v $
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_rdf_XNode_idl__
#define __com_sun_star_rdf_XNode_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif


//=============================================================================

module com {   module sun {   module star {   module rdf {

//=============================================================================
/** represents a node that may occur in a RDF graph.

    <p>
    In the RDF data model, there are three distinct types of nodes:
    URIs, blank nodes, and literals.
    </p>

    <pre>
    XNode
    |
    |---XLiteral
    |
    XResource
    |
    |---XBlankNode
    |
    XURI
    </pre>

    @since OOo 3.0

    @see XRepository
    @see Statement
    @see XResource
    @see XBlankNode
    @see XURI
    @see XLiteral
 */
interface XNode
{
    /// the string value of the node.
    [readonly, attribute] string StringValue;
};

//=============================================================================

}; }; }; };

#endif
