/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XQuerySelectResult.idl,v $
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_rdf_XQuerySelectResult_idl__
#define __com_sun_star_rdf_XQuerySelectResult_idl__

#ifndef __com_sun_star_container_XEnumeration_idl__
#include <com/sun/star/container/XEnumeration.idl>
#endif


//=============================================================================

module com {   module sun {   module star {   module rdf {

//=============================================================================
/** represents the result of a SPARQL "SELECT" query.

    <p>
    The result consists of:
    <ol>
    <li>a list of query variable names (column labels)</li>
    <li>an iterator of query results (rows),
        each being a list of bindings for the above variables</li>
    </ol>
    Note that each query result retrieved via
    <member scope="com::sun::star::container">XEnumeration::nextElement</member>
    has the type <type dim="[]">XNode</type>,
    the length of the sequence being the same as the number of query variables. 
    </p>

    @since OOo 3.0

    @see XRepository::querySelect
    @see XNode
 */
interface XQuerySelectResult : com::sun::star::container::XEnumeration
{
    //-------------------------------------------------------------------------
    /** get the names of the query variables.

        <p>
        </p>
     */
     sequence<string> getBindingNames();
};

//=============================================================================

}; }; }; };

#endif
