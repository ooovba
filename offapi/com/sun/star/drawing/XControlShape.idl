/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XControlShape.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_XControlShape_idl__ 
#define __com_sun_star_drawing_XControlShape_idl__ 
 
#ifndef __com_sun_star_drawing_XShape_idl__ 
#include <com/sun/star/drawing/XShape.idl> 
#endif 
 
#ifndef __com_sun_star_awt_XControlModel_idl__ 
#include <com/sun/star/awt/XControlModel.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module drawing {  
 
//============================================================================= 
 
/** is implemented by a <type>ControlShape</type> to access the controls model.

    @see com::sun::star::drawing::ControlShape
    @see com::sun::star::awt::UnoControlModel
*/
published interface XControlShape: com::sun::star::drawing::XShape
{ 
    //------------------------------------------------------------------------- 
     
    /** returns the control model of this <type>Shape</type>.

        @return
            if there is already a control model assigned
            to this <type>ControlShape</type>, than its returned.
            Otherwise you get an empty reference.
     */
    com::sun::star::awt::XControlModel getControl(); 
 
    //------------------------------------------------------------------------- 
     
    /** sets the control model for this <type>Shape</type>.

        @param xControl
            this will be the new control model that is
            displayed with this shape. You may change
            the model more than once during the lifetime
            of a <type>ControlShape</type> 
    
     */
    [oneway] void setControl( [in] com::sun::star::awt::XControlModel xControl ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 

