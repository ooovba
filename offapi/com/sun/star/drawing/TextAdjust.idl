/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextAdjust.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_TextAdjust_idl__ 
#define __com_sun_star_drawing_TextAdjust_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module drawing {  
 
//============================================================================= 
 
// DocMerge from xml: enum com::sun::star::drawing::TextAdjust
/** This enumeration specifies the position of a text inside a shape in 
    relation to the shape. 
    
    @deprecated	
    
    <p>This counts for the complete text, not individual lines. </p>
 */
published enum TextAdjust
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::TextAdjust::LEFT
    /** The left edge of the text is adjusted to the left edge of the shape.
     */
    LEFT, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::TextAdjust::CENTER
    /** The text is centered inside the shape.
     */
    CENTER, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::TextAdjust::RIGHT
    /** The right edge of the text is adjusted to the right edge of the shape.
     */
    RIGHT, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::TextAdjust::BLOCK
    /** The text extends from the left to the right edge of the shape.
     */
    BLOCK, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::TextAdjust::STRETCH
    /** The text is stretched so that the longest line goes from the left
        to the right edge of the shape.
     */
    STRETCH 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 

