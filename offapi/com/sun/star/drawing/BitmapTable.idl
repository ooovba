/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: BitmapTable.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_BitmapTable_idl__ 
#define __com_sun_star_drawing_BitmapTable_idl__ 
 
#ifndef __com_sun_star_container_XNameContainer_idl__ 
#include <com/sun/star/container/XNameContainer.idl> 
#endif

//============================================================================= 
 
 module com {  module sun {  module star {  module drawing {  
 
//============================================================================= 

/** this is a container for URLs to bitmaps.

    <p>It is used for example to access the bitmaps that
    are used inside a document for filling. 

    @see DrawingDocumentFactory
    @see FillStyle::FillBitmapURL
*/
published service BitmapTable
{
    /** this container lets you access the URLs that
        are indexed with a name.

        <p>Note: You can add new entries for later use, but you cannot
        remove entries that are used inside the document.
    */
    interface com::sun::star::container::XNameContainer;
};

//============================================================================= 
 
}; }; }; };  
 
#endif 

