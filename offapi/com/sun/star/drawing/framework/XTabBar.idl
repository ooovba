/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XTabBar.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_drawing_framework_XTabBar_idl__
#define __com_sun_star_drawing_framework_XTabBar_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif
#ifndef __com_sun_star_awt_XWindow_idl__
#include <com/sun/star/awt/XWindow.idl>
#endif
#ifndef __com_sun_star_drawing_framework_TabBarButton_idl__
#include <com/sun/star/drawing/framework/TabBarButton.idl>
#endif

module com { module sun { module star { module drawing { module framework {

/** UI control for the selection of views in a pane.
    <p>Every tab of a tab bar has, besides its localized title and help
    text, the URL of a view.  A possible alternative would be to use a
    command URL instead of the view URL.</p>
    <p>In the current Impress implementation a tab bar is only used for the
    center pane to switch between views in the center pane.  Tab bars can
    make sense for other panes as well, i.e. for showing either the slide
    sorter or the outline view in the left pane.</p>
    <p>Tab bar buttons are identified by their resource id.  Note that
    because the resource anchors are all the same (the tab bar), it is the
    resource URL that really identifies a button. There can not be two
    buttons with the same resource id.</p>
    </p>
    <p>A better place for this interface (in an extended version) would be
    <code>com::sun::star::awt</code></p>
    @see TabBarButton
*/
interface XTabBar
{
    /** Add a tab bar button to the right of another one.
        @param aButton
            The new tab bar button that is to be inserted.  If a button with
            the same resource id is already present than that is removed before the
            new button is inserted.
        @param aAnchor
            The new button is inserted to the right of this button.  When
            its ResourceId is empty then the new button is inserted at the left
            most position.
    */
    void addTabBarButtonAfter ([in] TabBarButton aButton, [in] TabBarButton aAnchor);

    /** Add a tab bar button at the right most position.
        @param aButton
            The new tab bar button that is to be inserted.
    */
    void appendTabBarButton ([in] TabBarButton aButton);

    /** Remove a tab bar button.
        @param aButton
            The tab bar button to remove.  When there is no button with the
            specified resource id then this call is silently ignored.
    */
    void removeTabBarButton ([in] TabBarButton aButton);

    /** Test whether the specified button exists in the tab bar.
        @param aButton
            The tab bar button whose existence is tested.
        @return
            Returns <TRUE/> when the button exists.
    */
    boolean hasTabBarButton ([in] TabBarButton aButton);

    /** Return a sequence of all the tab bar buttons.
        <p>Their order reflects the visible order in the tab bar.</p>
        <p>This method can be used when
        <member>addTabBarButtonAfter()</member> does not provide enough
        control as to where to insert a new button.</p>
    */
    sequence<TabBarButton> getTabBarButtons ();
};

}; }; }; }; }; // ::com::sun::star::drawing::framework

#endif
