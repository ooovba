/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ConfigurationController.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_drawing_framework_ConfigurationController_idl__
#define __com_sun_star_drawing_framework_ConfigurationController_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif
#ifndef __com_sun_star_drawing_framework_XConfigurationController_idl__
#include <com/sun/star/drawing/framework/XConfigurationController.idl>
#endif
#ifndef __com_sun_star_frame_XController_idl__
#include <com/sun/star/frame/XController.idl>
#endif

module com { module sun { module star { module drawing { module framework {

/** See <type>XConfigurationController</type> for a description of the
    configuration controller.

    <p>This service is used at the moment by the
    <type>XControllerManager</type> to create a configuration controller. 
    This allows developers to replace the default implementation of the
    configuration controller with their own.  This may not be a usefull
    feature.  Furthermore the sub controllers may need a tighter coupling
    than the interfaces allow.  These are reasons for removing this service
    in the future and let the controller manager create the sub controllers
    directly.</p>
*/
service ConfigurationController
    : XConfigurationController
{
    create ([in] ::com::sun::star::frame::XController xController);
};

}; }; }; }; }; // ::com::sun::star::drawing::framework

#endif
