/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDrawPageSummarizer.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_XDrawPageSummarizer_idl__ 
#define __com_sun_star_drawing_XDrawPageSummarizer_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_drawing_XDrawPage_idl__ 
#include <com/sun/star/drawing/XDrawPage.idl> 
#endif 
 
#ifndef __com_sun_star_drawing_XDrawPages_idl__ 
#include <com/sun/star/drawing/XDrawPages.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module drawing {  
 
//============================================================================= 
 
// DocMerge from xml: interface com::sun::star::drawing::XDrawPageSummarizer
/** is implemented by documents that can create summaries 
    of their <type>DrawPage</type>s.

    @deprecated
 */
published interface XDrawPageSummarizer: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: method com::sun::star::drawing::XDrawPageSummarizer::summarize
    /** creates a new <type>DrawPage</type> with a summary of all
        <type>DrawPage</type>s in the given collection.
     */
    com::sun::star::drawing::XDrawPage summarize( [in] com::sun::star::drawing::XDrawPages xPages ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  

#endif 

