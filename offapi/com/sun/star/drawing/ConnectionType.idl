/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ConnectionType.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_ConnectionType_idl__ 
#define __com_sun_star_drawing_ConnectionType_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module drawing {  
 
//============================================================================= 
 
// DocMerge from xml: enum com::sun::star::drawing::ConnectionType
/** the direction where the
    connection line leaves the connection point.
 */
published enum ConnectionType
{ 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::ConnectionType::AUTO
    /** the connection point is chosen automatically,
     */
    AUTO, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::ConnectionType::LEFT
    /** the connection line leaves the connected object to the left,
     */
    LEFT, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::ConnectionType::TOP
    /** the connection line leaves the connected object from the top,
     */
    TOP, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::ConnectionType::RIGHT
    /** the connection line leaves the connected object to the right,
     */
    RIGHT, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::ConnectionType::BOTTOM
    /** the connection line leaves the connected object from the bottom,
     */
    BOTTOM, 
 
    //------------------------------------------------------------------------- 
     
    // DocMerge from xml: value com::sun::star::drawing::ConnectionType::SPECIAL
    /** not implemented, yet.
     */
    SPECIAL 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 

