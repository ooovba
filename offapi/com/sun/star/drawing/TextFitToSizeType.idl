/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TextFitToSizeType.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_TextFitToSizeType_idl__ 
#define __com_sun_star_drawing_TextFitToSizeType_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module drawing {  
 
//============================================================================= 
 
// DocMerge from xml: enum com::sun::star::drawing::TextFitToSizeType
/** This enumeration specifies how the text within a shape relates to the 
    size of the shape.
 */
published enum TextFitToSizeType
{ 
    //------------------------------------------------------------------------- 
    /** the text size is only defined by the font properties
    */
    NONE, 
 
    //------------------------------------------------------------------------- 

    /** if the shape is scaled, the text character size is scaled proportional
    */
    PROPORTIONAL, 

    //------------------------------------------------------------------------- 

    /** like <code>PROPORTIONAL</code>, but the width of each text row is
        also scaled proportional.
    */
    ALLLINES, 
 
    //------------------------------------------------------------------------- 

	/** if the shape is scaled, the font is scaled isotrophically to
		fit the avaiable space. Auto line-breaks will keep working
   */
	AUTOFIT 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 

