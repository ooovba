/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ZipFileAccess.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_packages_zip_ZipFileAccess_idl__
#define __com_sun_star_packages_zip_ZipFileAccess_idl__ 
 
#ifndef __com_sun_star_packages_zip_XZipFileAccess_idl__ 
#include <com/sun/star/packages/zip/XZipFileAccess.idl> 
#endif 

#ifndef __com_sun_star_lang_XInitialization_idl__ 
#include <com/sun/star/lang/XInitialization.idl> 
#endif 

#ifndef __com_sun_star_lang_XComponent_idl__ 
#include <com/sun/star/lang/XComponent.idl> 
#endif 

#ifndef __com_sun_star_container_XNameAccess_idl__ 
#include <com/sun/star/container/XNameAccess.idl> 
#endif 

#ifndef __com_sun_star_io_IOException_idl__ 
#include <com/sun/star/io/IOException.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {   module packages {  module zip {
 
//============================================================================= 
 
/** allows to get reading access to nonencrypted entries inside zip file.
 */
service ZipFileAccess
{ 
    interface XZipFileAccess;
    interface ::com::sun::star::container::XNameAccess;
    interface ::com::sun::star::lang::XInitialization;
    interface ::com::sun::star::lang::XComponent;
}; 
 
//============================================================================= 
 
}; }; }; }; };
 
#endif 

