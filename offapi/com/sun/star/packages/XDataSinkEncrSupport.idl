/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDataSinkEncrSupport.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_packages_XDataSinkEncrSupport_idl__ 
#define __com_sun_star_packages_XDataSinkEncrSupport_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_io_XInputStream_idl__ 
#include <com/sun/star/io/XInputStream.idl> 
#endif 
 
#ifndef __com_sun_star_io_IOException_idl__ 
#include <com/sun/star/io/IOException.idl> 
#endif 

#ifndef __com_sun_star_packages_WrongPasswordException_idl__ 
#include <com/sun/star/packages/WrongPasswordException.idl> 
#endif 

#ifndef __com_sun_star_packages_NoEncryptionException_idl__ 
#include <com/sun/star/packages/NoEncryptionException.idl> 
#endif 
 
#ifndef __com_sun_star_packages_EncryptionNotAllowedException_idl__ 
#include <com/sun/star/packages/EncryptionNotAllowedException.idl> 
#endif 

#ifndef __com_sun_star_packages_NoRawFormatException_idl__ 
#include <com/sun/star/packages/NoRawFormatException.idl> 
#endif 


//============================================================================= 
 
module com {  module sun {  module star {  module packages {  
 
//============================================================================= 
 
/** Allows to get access to the stream of a <type>PackageStream</type>.
 */
interface XDataSinkEncrSupport: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
    /** Allows to get access to the data of the PackageStream.
    <p>
        In case stream is encrypted one and the key for the stream is not set,
    an exception must be thrown.
    </p>

    @returns
        the stream

    @throws ::com::sun::star::packages::WrongPasswordException
        no key or a wrong one is set

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    ::com::sun::star::io::XInputStream getDataStream()
        raises( ::com::sun::star::packages::WrongPasswordException,
                ::com::sun::star::io::IOException ); 
 

    //------------------------------------------------------------------------- 
    /** Allows to get access to the data of the PackageStream as to raw stream.
    In case stream is not encrypted an exception will be thrown.
    <p>
        The difference of raw stream is that it contains header for encrypted data,
    so an encrypted stream can be copyed from one PackageStream to
    another one without decryption.
    </p>

    @returns
        the raw representation of stream

    @throws ::com::sun::star::packages::NoEncryptionException
        the PackageStream object is not encrypted

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    ::com::sun::star::io::XInputStream getRawStream()
        raises( ::com::sun::star::packages::NoEncryptionException,
                ::com::sun::star::io::IOException ); 

    //------------------------------------------------------------------------- 
    /** Allows to set a data stream for the PackageStream.
    <p>
        In case PackageStream is marked as encrypted the data stream will be encrypted on storing.
    </p>

    @param aStream
        new data stream

    @throws ::com::sun::star::io::IOException
        in case of io problems
     */
    void setDataStream( [in] ::com::sun::star::io::XInputStream aStream )
        raises( ::com::sun::star::io::IOException ); 

    //------------------------------------------------------------------------- 
    /** Allows to set raw stream for the PackageStream.
    The PackageStream object can not be marked as encrypted one,
    an exception will be thrown in such case.

    @param aStream
        the new raw representation of stream

    @throws ::com::sun::star::packages::EncryptionNotAllowedException
        the PackageStream object is marked as encrypted

    @throws ::com::sun::star::packages::NoRawFormatException
        the stream is not a correct raw representation of encrypted package stream

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    void setRawStream( [in] ::com::sun::star::io::XInputStream aStream )
        raises( ::com::sun::star::packages::EncryptionNotAllowedException,
                ::com::sun::star::packages::NoRawFormatException,
                ::com::sun::star::io::IOException ); 

    //------------------------------------------------------------------------- 
    /** Allows to get access to the raw data of the stream as it is stored in
        the package.

    @returns
        the plain raw stream as it is stored in the package

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    ::com::sun::star::io::XInputStream getPlainRawStream()
        raises( ::com::sun::star::io::IOException ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
/*============================================================================= 
 
=============================================================================*/ 
#endif 
