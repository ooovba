/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XFastContextHandler.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_xml_sax_XFastContextHandler_idl__ 
#define __com_sun_star_xml_sax_XFastContextHandler_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_xml_sax_SAXException_idl__ 
#include <com/sun/star/xml/sax/SAXException.idl> 
#endif 
 
#ifndef __com_sun_star_xml_sax_XFastAttributeList_idl__ 
#include <com/sun/star/xml/sax/XFastAttributeList.idl> 
#endif 
 
#ifndef __com_sun_star_xml_sax_XLocator_idl__ 
#include <com/sun/star/xml/sax/XLocator.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module xml {  module sax {  
 
//============================================================================= 
 
/** receives notification of sax document events from a
    <type>XFastParser</type>.
    
    @see XFastDocumentHandler
 */
interface XFastContextHandler: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    /** receives notification of the beginning of an element .

        @param Element
            contains the integer token from the <type>XFastTokenHandler</type>
            registered at the <type>XFastParser</type>.<br>

            If the element has a namespace that was registered with the
            <type>XFastParser</type>, <param>Element</param> contains the integer
            token of the elements local name from the <type>XFastTokenHandler</type>
            and the integer token of the namespace combined with an arithmetic
            <b>or</b> operation.

        @param Attribs
            Contains a <type>XFastAttrbitueList</type> to access the attributes
            from the element.

    */
    void startFastElement( [in] long Element, [in] XFastAttributeList Attribs ) 
            raises( com::sun::star::xml::sax::SAXException ); 

    //------------------------------------------------------------------------- 

    /** receives notification of the beginning of an unknown element .

        @param Namespace
            contains the namespace url (not the prefix!) of this element.
        @param Name
            contains the elements local name.
        @param Attribs
            Contains a <type>XFastAttrbitueList</type> to access the attributes
            from the element.
     */
    void startUnknownElement( [in] string Namespace, [in] string Name, [in] XFastAttributeList Attribs ) 
            raises( com::sun::star::xml::sax::SAXException ); 

    //------------------------------------------------------------------------- 

    /** receives notification of the end of an known element.
        @see startFastElement
     */
    void endFastElement( [in] long Element ) 
            raises( com::sun::star::xml::sax::SAXException ); 

    //------------------------------------------------------------------------- 

    /** receives notification of the end of an kown element.
        @see startUnknownElement
     */
    void endUnknownElement( [in] string Namespace, [in] string Name ) 
            raises( com::sun::star::xml::sax::SAXException ); 
 
    //------------------------------------------------------------------------- 
     
    /** receives notification of the beginning of a known child element.

        @param Element
            contains the integer token from the <type>XFastTokenHandler</type>
            registered at the <type>XFastParser</type>.

            <br>If the element has a namespace that was registered with the
            <type>XFastParser</type>, <param>Element</param> contains the
            integer token of the elements local name from the
            <type>XFastTokenHandler</type> and the integer token of the
            namespace combined with an arithmetic <b>or</b> operation.

        @param Attribs
            Contains a <type>XFastAttrbitueList</type> to access the attributes
            from the element.
     */
    XFastContextHandler createFastChildContext( [in] long Element, [in] XFastAttributeList Attribs ) 
            raises( com::sun::star::xml::sax::SAXException ); 

    //------------------------------------------------------------------------- 
     
    /** receives notification of the beginning of a unknown child element .

        @param Namespace
            contains the namespace url (not the prefix!) of this element.
        @param Name
            contains the elements local name.
        @param Attribs
            Contains a <type>XFastAttrbitueList</type> to access the attributes
            the element.
     */
    XFastContextHandler createUnknownChildContext( [in] string Namespace, [in] string Name, [in] XFastAttributeList Attribs ) 
            raises( com::sun::star::xml::sax::SAXException ); 

    //------------------------------------------------------------------------- 
     
    /** receives notification of character data.
     */
    void characters( [in] string aChars ) 
            raises( com::sun::star::xml::sax::SAXException ); 
  }; 
 
//============================================================================= 
 
}; }; }; }; };  
 
#endif 
