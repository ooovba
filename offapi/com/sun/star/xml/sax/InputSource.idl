/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: InputSource.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_xml_sax_InputSource_idl__ 
#define __com_sun_star_xml_sax_InputSource_idl__ 
 
#ifndef __com_sun_star_io_XInputStream_idl__ 
#include <com/sun/star/io/XInputStream.idl> 
#endif 
 
 
//============================================================================= 
 
module com {  module sun {  module star {  module xml {  module sax {  
 
//============================================================================= 
 
/** specifies the Datasource plus some additional information for the parser.
    
    <p>There are two places where the application will deliver this input
    source to the parser: 
    </p>
    <ul>
        <li>as the argument of <method>XParser::parseStream</method></li>
        <li>as the return value of <method>XEntityReslover::resolveEntity</method>.
    </li>
    </ul>
 */
published struct InputSource
{
    /** contains the byte input stream of the document.
     */
    com::sun::star::io::XInputStream aInputStream; 
 
    //------------------------------------------------------------------------- 
     
    /** contains the encoding of the data stream. This is used by the parser 
        to do unicode conversions. 
        
        <p>Note that in general you do not need to specify an encoding. 
        Either it is UTF-8 or UTF-16 which is recognized by the parser 
        or it is specified in the first line of the XML-File 
        ( e.g. <em>?xml encoding="EUC-JP"?</em> ).</p>
     */
    string sEncoding; 
 
    //------------------------------------------------------------------------- 
     
    /** constains the public Id of the document, for example, needed in 
        exception-message strings.
     */
    string sPublicId; 
 
    //------------------------------------------------------------------------- 
     
    /** contains the sytemID of the document.
     */
    string sSystemId; 
}; 
 
//============================================================================= 
 
}; }; }; }; };  
 
#endif 
