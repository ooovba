/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XFocusListener.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_XFocusListener_idl__ 
#define __com_sun_star_awt_XFocusListener_idl__ 
 
#ifndef __com_sun_star_lang_XEventListener_idl__ 
#include <com/sun/star/lang/XEventListener.idl> 
#endif 
 
#ifndef __com_sun_star_awt_FocusEvent_idl__ 
#include <com/sun/star/awt/FocusEvent.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module awt {  
 
//============================================================================= 
 
/** makes it possible to receive keyboard focus events.
            
    <p>The window which has the keyboard focus is the window which gets
    the keyboard events.</p>
 */
published interface XFocusListener: com::sun::star::lang::XEventListener
{ 
    //------------------------------------------------------------------------- 
     
    /** is invoked when a window gains the keyboard focus.

        @see XActivateListener::windowActivated
     */
    [oneway] void focusGained( [in] com::sun::star::awt::FocusEvent e ); 
 
    //------------------------------------------------------------------------- 
     
    /** is invoked when a window loses the keyboard focus.

        @see XActivateListener::windowDeactivated
     */
    [oneway] void focusLost( [in] com::sun::star::awt::FocusEvent e ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
