/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: MenuLogo.idl,v $
 * $Revision: 1.0 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_MenuLogo_idl__
#define __com_sun_star_awt_MenuLogo_idl__

#ifndef com_sun_star_graphic_XGraphic_idl
#include <com/sun/star/graphic/XGraphic.idl>
#endif

#ifndef __com_sun_star_util_color_idl__
#include <com/sun/star/util/Color.idl>
#endif

//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** specifies a logo to be displayed on a menu, with a background gradient.
 */
struct MenuLogo
{
    /** specifies the logo image.
     */
    ::com::sun::star::graphic::XGraphic Graphic;

    //-------------------------------------------------------------------------

    /** specifies the color at the start point of the gradient.
     */
    ::com::sun::star::util::Color StartColor;

    //-------------------------------------------------------------------------

    /** specifies the color at the end point of the gradient.
     */
    ::com::sun::star::util::Color EndColor;

};

//=============================================================================

}; }; }; };

#endif
