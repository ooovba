/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: SimpleFontMetric.idl,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_SimpleFontMetric_idl__ 
#define __com_sun_star_awt_SimpleFontMetric_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module awt {  
 
//============================================================================= 
 
/** describes the general metrics of a certain font.
 */
published struct SimpleFontMetric
{
    /** specifies the portion of a lower case character that
        rises above the height of the character "x" of the font. 
                        
        <p>For example, the letters 'b', 'd', 'h', 'k' and 'l' 
        have an ascent unequal to 0.</p>
        
        <p>The ascent is measured in pixels, thus the font metric is
        device dependent.</p>
     */
    short Ascent; 
 
    //------------------------------------------------------------------------- 
     
    /** specifies the portion of a letter falling below the baseline.
                        
        <p>For example, the letters 'g', 'p', and 'y'
        have a descent unequal to 0.</p>
        
        <p>The descent is measured in pixels, thus the font metric is 
        device dependent.</p>
     */
    short Descent; 
 
    //------------------------------------------------------------------------- 
     
    /** specifies the vertical space between lines of this font;
        it is also called internal linespacing. 
                        
        <p>The leading is measured in pixels, thus the font metric is 
        device dependent.</p>
     */
    short Leading; 
 
    //------------------------------------------------------------------------- 
     
    /** specifies the slant of the characters (italic).
                        
        <p>The slant is measured in degrees from 0 to 359.</p>
     */
    short Slant; 
 
    //------------------------------------------------------------------------- 
     
    /** specifies the code of the first printable character in the font.
     */
    char FirstChar; 
 
    //------------------------------------------------------------------------- 
     
    /** specifies the code of the last printable character in the font.
     */
    char LastChar; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 

#endif 
