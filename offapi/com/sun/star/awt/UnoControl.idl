/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: UnoControl.idl,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_UnoControl_idl__ 
#define __com_sun_star_awt_UnoControl_idl__ 
 
#ifndef __com_sun_star_lang_XComponent_idl__ 
#include <com/sun/star/lang/XComponent.idl> 
#endif

#ifndef __com_sun_star_awt_XControl_idl__ 
#include <com/sun/star/awt/XControl.idl> 
#endif 

#ifndef __com_sun_star_awt_XWindow_idl__ 
#include <com/sun/star/awt/XWindow.idl> 
#endif 

#ifndef __com_sun_star_awt_XView_idl__ 
#include <com/sun/star/awt/XView.idl> 
#endif 

#ifndef __com_sun_star_accessibility_XAccessible_idl__
#include <com/sun/star/accessibility/XAccessible.idl>
#endif

 
//============================================================================= 
 
 module com {  module sun {  module star {  module awt {
     
//============================================================================= 
 
/** specifies an abstract control. 
        
    <p>All components which implement this service can 
    be integrated in a windowing environment. This service describes 
    the controller of the Smalltalk model view controller design.</p>
    
    <p>You must set a model and a stub to the UnoControl before using 
    other methods.  The implementation only allows the change of the 
    graphics (<type>XView</type>) if the window is not visible. The 
    change of the graphics in visible state should redirect the output 
    to these graphics, but this behavior is implementation-specific.</p>
    
    <p>The change of data directly at the control may not affect the 
    model data. To ensure this behavior, modify the data of the model.</p>
 */
published service UnoControl
{	 
    interface com::sun::star::lang::XComponent; 
    
    interface com::sun::star::awt::XControl; 
    
    interface com::sun::star::awt::XWindow; 
    
    interface com::sun::star::awt::XView;

    /** provides access to the accessible context associated with this object.

        @since OOo 1.1.2
     */
    [optional] interface com::sun::star::accessibility::XAccessible;
    
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
