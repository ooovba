/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XUserInputInterception.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_XUserInputInterception_idl__
#define __com_sun_star_awt_XUserInputInterception_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_awt_XKeyHandler_idl__
#include <com/sun/star/awt/XKeyHandler.idl>
#endif

#ifndef __com_sun_star_awt_XMouseClickHandler_idl__
#include <com/sun/star/awt/XMouseClickHandler.idl>
#endif

module com {  module sun {  module star {  module awt {

/** Interface to add handlers for key and mouse events. A handler is not a passive
   listener, it can even consume the event.
   
    @since OOo 1.1.2
 */
published interface XUserInputInterception : ::com::sun::star::uno::XInterface
{
    /** Add a new listener that is called on <type
        scope="::com::sun::star::awt">KeyEvent</type>s.  Every listener is
        given the opportunity to consume the event, i.e. prevent the not yet
        called listeners from being called.
        @param xHandler
            If this is a valid reference it is inserted into the list of
            handlers.  It is the task of the caller to not register the
            same handler twice (otherwise that listener will be called
            twice.)
    */
    [oneway] void addKeyHandler (
        [in] ::com::sun::star::awt::XKeyHandler xHandler);


    /** Remove the specified listener from the list of listeners.
        @param xHandler
            If the reference is empty then nothing will be changed.  If the
            handler has been registered twice (or more) then all refrences
            will be removed.
    */
    [oneway] void removeKeyHandler (
        [in] ::com::sun::star::awt::XKeyHandler xHandler);

    /** Add a new listener that is called on <type
        scope="::com::sun::star::awt">MouseEvent</type>s.  Every listener is
        given the opportunity to consume the event, i.e. prevent the not yet
        called listeners from being called.
        @param xHandler
            If this is a valid reference it is inserted into the list of
            handlers.  It is the task of the caller to not register the
            same handler twice (otherwise that listener will be called
            twice.)
    */
    [oneway] void addMouseClickHandler (
        [in] ::com::sun::star::awt::XMouseClickHandler xHandler);

    /** Remove the specified listener from the list of listeners.
        @param xHandler
            If the reference is empty then nothing will be changed.  If the
            handler has been registered twice (or more) then all refrences
            will be removed.
    */
    [oneway] void removeMouseClickHandler (
        [in] ::com::sun::star::awt::XMouseClickHandler xHandler);
};

}; }; }; };

#endif
