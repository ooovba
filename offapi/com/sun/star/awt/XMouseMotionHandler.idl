/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMouseMotionHandler.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_XMouseMotionHandler_idl__ 
#define __com_sun_star_awt_XMouseMotionHandler_idl__ 
 
#ifndef __com_sun_star_lang_XEventListener_idl__ 
#include <com/sun/star/lang/XEventListener.idl> 
#endif 
 
#ifndef __com_sun_star_awt_MouseEvent_idl__ 
#include <com/sun/star/awt/MouseEvent.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module awt {  
 
//============================================================================= 
 
/** makes it possible to receive mouse motion events on a window.
 */
published interface XMouseMotionHandler: com::sun::star::lang::XEventListener
{ 
    //------------------------------------------------------------------------- 
     
    /** is invoked when a mouse button is pressed on a window and then 
        dragged.  
                        
        <p>Mouse drag events will continue to be delivered to
        the window where the first event originated until the mouse button is
        released (regardless of whether the mouse position is within the
        bounds of the window).</p>

        @return
            When <FALSE/> is returned the other handlers are called and a
            following handling of the event by the broadcaster takes place.
            Otherwise, when <TRUE/> is returned, no other handler will be
            called and the broadcaster will take no further actions
            regarding the event.

     */
    boolean mouseDragged( [in] com::sun::star::awt::MouseEvent e ); 
 
    //------------------------------------------------------------------------- 
     
    /** is invoked when the mouse button has been moved on a window
        (with no buttons down).

        @return
            When <FALSE/> is returned the other handlers are called and a
            following handling of the event by the broadcaster takes place.
            Otherwise, when <TRUE/> is returned, no other handler will be
            called and the broadcaster will take no further actions
            regarding the event.

     */
    boolean mouseMoved( [in] com::sun::star::awt::MouseEvent e ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
