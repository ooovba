/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TreeExpansionEvent.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_tree_TreeExpansionEvent_idl__ 
#define __com_sun_star_awt_tree_TreeExpansionEvent_idl__ 
 
#ifndef __com_sun_star_lang_EventObject_idl__ 
#include <com/sun/star/lang/EventObject.idl> 
#endif 

#ifndef __com_sun_star_awt_tree_XTreeNode_idl__
#include <com/sun/star/awt/tree/XTreeNode.idl>
#endif

//============================================================================= 
 
module com {  module sun {  module star {  module awt { module tree {
 
//============================================================================= 
 
/** This event tells you what node is currently expanding or collapsing.

    @see XTreeExpansionListener
    @see XTreeControl
 */
struct TreeExpansionEvent : ::com::sun::star::lang::EventObject
{
    /* The node that has been expanded/collapsed.*/
    com::sun::star::awt::tree::XTreeNode Node;
}; 
 
//============================================================================= 
 
}; }; }; }; };
 
#endif 
