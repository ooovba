/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: CharSet.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_CharSet_idl__ 
#define __com_sun_star_awt_CharSet_idl__ 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module awt {  
 
//============================================================================= 
 
/** These values are used to specify the characters which are available in  
    a font and their codes. 
         
    <P>The currently defined constants of <CODE>CharSet</CODE> have the same
    numerical values as the corresponding enum values of the C/C++
    <CODE>rtl_TextEncoding</CODE> (from <CODE>rtl/textenc.h</CODE>).  This
    correspondence is by design.  Since <CODE>CharSet</CODE> is deprecated,
    however, it is not planned to add further constants to keep it in sync with
    <CODE>rtl_TextEncoding</CODE>.</P>
     
    @deprecated
 */
published constants CharSet
{ 
    //------------------------------------------------------------------------- 
        
    /** specifies an unknown character set.
     */
    const short DONTKNOW = 0; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the ANSI character set.
     */
    const short ANSI = 1; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the <regtm>Apple Macintosh</regtm> character set.
     */
    const short MAC = 2; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the IBM PC character set number 437.
     */
    const short IBMPC_437 = 3; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the IBM PC character set number 850.
     */
    const short IBMPC_850 = 4; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the IBM PC character set number 860.
     */
    const short IBMPC_860 = 5; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the IBM PC character set number 861.
     */
    const short IBMPC_861 = 6; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the IBM PC character set number 863.
     */
    const short IBMPC_863 = 7; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the IBM PC character set number 865.
     */
    const short IBMPC_865 = 8; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies the system character set.
     */
    const short SYSTEM = 9; 
 
    //------------------------------------------------------------------------- 
        
    /** specifies a set of symbols.
     */
    const short SYMBOL = 10; 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
