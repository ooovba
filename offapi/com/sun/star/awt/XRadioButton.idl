/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XRadioButton.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_XRadioButton_idl__ 
#define __com_sun_star_awt_XRadioButton_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_awt_XItemListener_idl__ 
#include <com/sun/star/awt/XItemListener.idl> 
#endif 
 
 
//============================================================================= 
 
 module com {  module sun {  module star {  module awt {  
 
//============================================================================= 
 
/** gives access to the state of a radio button and makes it possible
    to register item event listeners.
 */
published interface XRadioButton: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
        
    /** registers a listener for item events.
     */
    [oneway] void addItemListener( [in] com::sun::star::awt::XItemListener l ); 
 
    //------------------------------------------------------------------------- 
        
    /** unregisters a listener for item events.
     */
    [oneway] void removeItemListener( [in] com::sun::star::awt::XItemListener l ); 
 
    //------------------------------------------------------------------------- 
     
    /** returns <true/> if the button is checked, <false/> otherwise.
     */
    boolean getState(); 
 
    //------------------------------------------------------------------------- 
        
    /** sets the state of the radio button.
     */
    [oneway] void setState( [in] boolean b ); 
 
    //------------------------------------------------------------------------- 
        
    /** sets the label of the radio button.
     */
    [oneway] void setLabel( [in] string Label ); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
