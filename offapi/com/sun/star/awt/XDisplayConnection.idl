/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XDisplayConnection.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_XDisplayConnection_idl__
#define __com_sun_star_awt_XDisplayConnection_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

//=============================================================================

module com { module sun { module star { module awt {

 published interface XEventHandler;

//=============================================================================

/** This interface should be implemented by toolkits that want to give access
    to their internal message handling loop.
*/
published interface XDisplayConnection: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** registers an event handler.

        @param window
        the platform specific window id. If empty, the handler should be 
        registered for all windows.

        @param eventHandler
        the handler to register.

        @param eventMask
        the event mask specifies the events the handler is interested in.
    */
    [oneway] void addEventHandler( [in] any window, [in] XEventHandler eventHandler, [in] long eventMask );

    //-------------------------------------------------------------------------

    /** removes a eventHandler from the handler list.

        @param window
        the platform specific window id the handler should be deregistered for. 
        If empty, the handler should be deregistered completly.

        @param eventHandler
        the handler to remove.
    */
    [oneway] void removeEventHandler( [in] any window, [in] XEventHandler eventHandler );
    
    //-------------------------------------------------------------------------

    /** register an error handler for toolkit specific errors.

        @param errorHandler
        the handler to register.
    */
    [oneway] void addErrorHandler( [in] XEventHandler errorHandler );

    //-------------------------------------------------------------------------

    /** remover an error handler from the handler list.

        @param errorHandler
        the handler to remove.
    */
    [oneway] void removeErrorHandler( [in] XEventHandler errorhandler );

    //-------------------------------------------------------------------------

    /** returns a identifier.

        @returns a unique platform dependend identifier for a display connection.
    */
    any getIdentifier();
    
};

//=============================================================================

}; }; }; };

#endif
