/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AccessibleListItem.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_AccessibleListItem_idl__
#define __com_sun_star_awt_AccessibleListItem_idl__

#ifndef __com_sun_star_accessibility_AccessibleContext_idl__
#include <com/sun/star/accessibility/AccessibleContext.idl>
#endif


module com { module sun { module star { module accessibility {

 published interface XAccessibleText;
 published interface XAccessibleComponent;

}; }; }; };

module com { module sun { module star { module awt {

/** specifies accessibility support for a list item.

    @see com::sun::star::accessibility::AccessibleContext
    @see com::sun::star::accessibility::XAccessibleComponent
    @see com::sun::star::accessibility::XAccessibleText

    @since OOo 1.1.2
*/    
published service AccessibleListItem
{
    /** This interface gives access to the structural information of a list item:

       <ul>
       <li>Role: The role of a list item is <const 
           scope="com::sun::star::accessibility"
           >AccessibleRole::LIST_ITEM</const>.</li>
       <li>Name: The name of a list item is the text of the item.</li>
       <li>Description: The description of a list item is empty.</li>
       <li>Children: There exists no children.</li>
       <li>Parent: The parent is of type <type scope="com::sun::star::awt">AccessibleList</type>.</li>
       <li>Relations: There are no relations.</li>
       <li>States: The states supported by this service are
           <ul>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::DEFUNC</const>
               is set if the object has already been disposed
               and subsequent calls to this object result in				
               <type scope="com::sun::star::lang">DisposedException</type>
               exceptions.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::ENABLED</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::SHOWING</const> is set
               if the object is displayed on the screen.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::VISIBLE</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::TRANSIENT</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::SELECTABLE</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::SELECTED</const> is set 
               when the item is selected.</li>
           </ul>
           </li>
       </ul>
    */
    service   com::sun::star::accessibility::AccessibleContext;

    interface com::sun::star::accessibility::XAccessibleComponent;

    /** This interface gives read-only access to the text representation
        of a list item.
     */
    interface com::sun::star::accessibility::XAccessibleText;
};
          
}; }; }; };

#endif
