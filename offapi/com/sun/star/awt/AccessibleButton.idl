/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AccessibleButton.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_AccessibleButton_idl__
#define __com_sun_star_awt_AccessibleButton_idl__

#ifndef __com_sun_star_accessibility_XAccessibleContext_idl__
#include <com/sun/star/accessibility/XAccessibleContext.idl>
#endif
#ifndef __com_sun_star_accessibility_XAccessibleEventBroadcaster_idl__
#include <com/sun/star/accessibility/XAccessibleEventBroadcaster.idl>
#endif
#ifndef __com_sun_star_accessibility_XAccessibleComponent_idl__
#include <com/sun/star/accessibility/XAccessibleComponent.idl>
#endif
#ifndef __com_sun_star_accessibility_XAccessibleExtendedComponent_idl__
#include <com/sun/star/accessibility/XAccessibleExtendedComponent.idl>
#endif
#ifndef __com_sun_star_accessibility_XAccessibleText_idl__
#include <com/sun/star/accessibility/XAccessibleText.idl>
#endif
#ifndef __com_sun_star_accessibility_XAccessibleAction_idl__
#include <com/sun/star/accessibility/XAccessibleAction.idl>
#endif
#ifndef __com_sun_star_accessibility_XAccessibleValue_idl__
#include <com/sun/star/accessibility/XAccessibleValue.idl>
#endif

module com { module sun { module star { module awt {
    
/** specifies accessibility support for a button.

    @since OOo 1.1.2
 */    
published service AccessibleButton
{
    /** This interface gives access to the structural information of a button:

        <ul>
        <li>Role: The role of a button is <const 
            scope="com::sun::star::accessibility"
            >AccessibleRole::PUSH_BUTTON</const>.</li>
        <li>Name: The name of a button is its localized label.</li>
        <li>Description: The description of a button is its localized
            help text.</li>
        <li>Children: There are no children.</li>            
        <li>Parent: The parent is the window that contains the button.</li>
        <li>Relations: There are no relations.</li>
        <li>States: The states supported by this service are
            <ul>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::CHECKED</const> is set
                if the object is currently checked.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::DEFUNC</const>
                is set if the object has already been disposed
                and subsequent calls to this object result in				
                <type scope="com::sun::star::lang">DisposedException</type>
                exceptions.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::ENABLED</const> is set
                if the object is enabled.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::FOCUSABLE</const> is always set.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::FOCUSED</const> is set
                if the object currently has the keyboard focus.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::PRESSED</const> is set
                if the object is currently pressed.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::SHOWING</const> is set
                if the object is displayed on the screen.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::VISIBLE</const> is always set.</li>
            </ul>
            </li>
        </ul>
     */
    interface ::com::sun::star::accessibility::XAccessibleContext;
    
    interface ::com::sun::star::accessibility::XAccessibleEventBroadcaster;
    
    interface ::com::sun::star::accessibility::XAccessibleComponent;
    
    interface ::com::sun::star::accessibility::XAccessibleExtendedComponent;

    /** This interface gives read-only access to the text representation
        of a button.
     */    
    interface ::com::sun::star::accessibility::XAccessibleText;

    /** This interface gives access to the actions that can be executed for
        a button. The supported actions for a button are:
        <ul>
        <li>click</li>
        </ul>
     */
    interface ::com::sun::star::accessibility::XAccessibleAction;

    /** This interface gives access to the numerical value of a button,
        which is related to the button's 
        <const scope="com::sun::star::accessibility"
        >AccessibleStateType::PRESSED</const> state.
     */
    interface ::com::sun::star::accessibility::XAccessibleValue;
    
};
          
}; }; }; };

#endif
