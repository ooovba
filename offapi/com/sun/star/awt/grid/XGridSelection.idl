/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XListBox.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_grid_XGridSelection_idl__ 
#define __com_sun_star_awt_grid_XGridSelection_idl__ 
 
#include <com/sun/star/uno/XInterface.idl> 
#include <com/sun/star/awt/grid/XGridSelectionListener.idl> 
#include <com/sun/star/view/SelectionType.idl> 
 
//============================================================================= 
 
module com {  module sun {  module star {  module awt { module grid {  
 
//============================================================================= 

/** This interfaces provides access to the selection of row for <type>UnoControlGrid</type>. 
 */     
interface XGridSelection: com::sun::star::uno::XInterface
{ 
    /** Returns the lowest index of the selection. 
        @returns
            the lowest index.
    */
    long getMinSelectionIndex();

    /** Returns the highest index of the selection. 
        @returns
            the highest index.
    */
    long getMaxSelectionIndex();

    /** Adds a selection intervall.
        @param start
                the start row index.
        @param length
                the number of rows to be selected.
    */
    [oneway] void insertIndexIntervall( [in] long start, [in] long length);

    /** Removes a selection intervall.
        @param start
                the start row index.
        @param length
                the number of rows to be selected.
    */
    [oneway] void removeIndexIntervall( [in] long start, [in] long length);

    /** Returns the indicies of all selected rows.
        @returns
            a sequence of indicies.
    */
    sequence< long > getSelection();

    /** Returns whether rows are selected.
        @returns
            <true/> if rows are selected otherwise <false/>.
    */
    boolean isSelectionEmpty();

    /** Returns whether a specific row is selected.
        @param
            the index of a row.
        @returns
            <true/> if row are selected otherwise <false/>.
    */
    boolean isSelectedIndex( [in] long index);

    /** Marks a row as selected.
        @param
            the index of a row.
    */
    [oneway] void selectRow( [in] long y);

    /*
    [oneway] void selectColumn( [in] long x);
    */

    /** Adds a listener for the <type>GridSelectionEvent</type> posted after the grid changes.
        @param Listener
            the listener to add.
    */    
    [oneway] void addSelectionListener( [in] XGridSelectionListener  listener); 
 
    //------------------------------------------------------------------------- 

    /** Removes a listener previously added with <method>addSelectionListener()</method>.
        @param Listener
            the listener to remove.
    */   
    [oneway] void removeSelectionListener( [in] XGridSelectionListener listener); 
 

}; 
 
//============================================================================= 
 
}; }; }; }; };
 
#endif 
