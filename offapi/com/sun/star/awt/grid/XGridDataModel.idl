/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XListBox.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_grid_XGridDataModel_idl__ 
#define __com_sun_star_awt_grid_XGridDataModel_idl__ 
 

#include <com/sun/star/lang/XComponent.idl> 
#include <com/sun/star/awt/grid/XGridDataListener.idl> 


//============================================================================= 
 
module com {  module sun {  module star {  module awt { module grid { 
 
//============================================================================= 
/** An instance of this interface is used by the <type>UnoControlGrid</type> to
    retrieve the content data that is displayed in the actual control.

    If you do not need your own model implementation, you can also use the <type>DefaultGridDataModel</type>.
*/   
interface XGridDataModel: ::com::sun::star::lang::XComponent 
{ 
    /** Specifies the height of each row.
    */
    [attribute] long RowHeight;

    /** Contains the row header.
    */
    [attribute] sequence< string > RowHeaders;

    /** Returns the content of each row.
    */
    [attribute,readonly] sequence< sequence< string > > Data;

    /** Returns the number of rows in in the model.
        @returns
                the number of rows.
    */
    long getRowCount();

    /** Adds a row to the model.

        @param headername
                specifies the name of the row. 
        @param data
                the content of the row.
    */
    void addRow( [in] string headername, [in] sequence< string > data );

    /** Removes a row from the model.

        @param index
                the index of the row that should be removed.
    */
    void removeRow( [in] long index);

    /** Removes all rows from the model.
    */
    void removeAll();

    //------------------------------------------------------------------------- 

    /** Adds a listener for the <type>GridDataEvent</type> posted after the grid changes.
        @param Listener
            the listener to add.
    */    
    [oneway] void addDataListener( [in] XGridDataListener  listener); 
 
    //------------------------------------------------------------------------- 

    /** Removes a listener previously added with <method>addDataListener()</method>.
        @param Listener
            the listener to remove.
    */       
    [oneway] void removeDataListener( [in] XGridDataListener listener); 
  
}; 
 
//============================================================================= 
 
}; }; }; };};  
 
#endif 
