/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XRegion.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_XRegion_idl__ 
#define __com_sun_star_awt_XRegion_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_awt_Rectangle_idl__ 
#include <com/sun/star/awt/Rectangle.idl> 
#endif 


//============================================================================= 
 
 module com {  module sun {  module star {  module awt {  
 
//============================================================================= 
 
/** manages multiple rectangles which make up a region.
 */
published interface XRegion: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    /** returns the bounding box of the shape.
     */
    Rectangle getBounds(); 
 
    //------------------------------------------------------------------------- 
        
    /** makes this region an empty region.
     */
    [oneway] void clear(); 
 
    //------------------------------------------------------------------------- 
        
    /** moves this region by the specified horizontal and vertical delta.
     */
    [oneway] void move( [in] long nHorzMove, 
             [in] long nVertMove ); 
 
    //------------------------------------------------------------------------- 
        
    /** adds the specified rectangle to this region.
     */
    [oneway] void unionRectangle( [in] Rectangle Rect ); 
 
    //------------------------------------------------------------------------- 
        
    /** intersects the specified rectangle with the current region.
     */
    [oneway] void intersectRectangle( [in] Rectangle Region ); 
 
    //------------------------------------------------------------------------- 
        
    /** removes the area of the specified rectangle from this region.
     */
    [oneway] void excludeRectangle( [in] Rectangle Rect ); 
 
    //------------------------------------------------------------------------- 
        
    /** applies an exclusive-or operation with the specified rectangle
        to this region.
     */
    [oneway] void xOrRectangle( [in] Rectangle Rect ); 
 
    //------------------------------------------------------------------------- 
        
    /** adds the specified region to this region.
     */
    [oneway] void unionRegion( [in] XRegion Region ); 
 
    //------------------------------------------------------------------------- 
        
    /** intersects the specified region with the current region.
     */
    [oneway] void intersectRegion( [in] XRegion Region ); 
 
    //------------------------------------------------------------------------- 
        
    /** removes the area of the specified region from this region.
     */
    [oneway] void excludeRegion( [in] XRegion Region ); 
 
    //------------------------------------------------------------------------- 
        
    /** applies an exclusive-or operation with the specified region
        to this region.
     */
    [oneway] void xOrRegion( [in] XRegion Region ); 
 
    //------------------------------------------------------------------------- 
     
    /** returns all rectangles which are making up this region.
     */
    sequence<Rectangle> getRectangles(); 
 
}; 
 
//============================================================================= 
 
}; }; }; };  
 
#endif 
