/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: RealRectangle2D.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_geometry_RealRectangle2D_idl__
#define __com_sun_star_geometry_RealRectangle2D_idl__

module com {  module sun {  module star {  module geometry {

/*  Removed, because XCanvas is private API until further notice.

    The values are stored as reals. Please note that the
    <type>com.sun.star.rendering.XCanvas</type> defines the
    screen representation of rectangles in such a way that the lower
    and the rightmost line of the rectangle are not drawn on
    screen. Thus, if for two rectangles R1 and R2, R1.x2 equals R2.x1,
    the screen representation of these rectangles will not overlap,
    but being exactly adjacent. That also means, that an
    IntegerRectangle2D with X1 equal X2 or Y1 equal Y2 can be
    considered empty.<p>
*/

/** This structure contains the necessary information for a
    two-dimensional rectangle.<p>

    @since OOo 2.0.0
 */
struct RealRectangle2D
{
    /// X coordinate of upper left corner .
    double X1;

    //-------------------------------------------------------------------------

    /// Y coordinate of upper left corner.
    double Y1;

    //-------------------------------------------------------------------------

    /** X coordinate of lower right corner.<p>

        Must be greater than x1 for non-empty rectangles.<p>.
    */
    double X2;

    //-------------------------------------------------------------------------

    /** Y coordinate of lower right corner.<p>

        Must be greater than y1 for non-empty rectangles.<p>
     */
    double Y2;
};

}; }; }; };

#endif
