/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: RealBezierSegment2D.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_geometry_RealBezierSegment2D_idl__
#define __com_sun_star_geometry_RealBezierSegment2D_idl__

module com {  module sun {  module star {  module geometry {

/** This structure contains the relevant data for a cubic Bezier
    curve.<p>

    The data is stored real-valued. The last point of the segment is
    taken from the first point of the following segment, and thus not
    included herein. That is, when forming a polygon out of cubic
    Bezier segments, each two consecutive RealBezierSegment2D
    define the actual curve, with the very last segment providing only
    the end point of the last curve, and the remaining members
    ignored.<p>

    @see com.sun.star.rendering.XBezierPolyPolygon2D
    @since OOo 2.0.0
 */
struct RealBezierSegment2D
{
    /// The x coordinate of the start point.
    double Px;
    /// The y coordinate of the start point.
    double Py;

    /// The x coordinate of the first control point.
    double C1x;
    /// The y coordinate of the first control point.
    double C1y;

    /// The x coordinate of the second control point.
    double C2x;
    /// The y coordinate of the second control point.
    double C2y;

};

}; }; }; };

#endif
