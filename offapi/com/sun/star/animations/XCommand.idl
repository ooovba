/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XCommand.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_animations_XCommand_idl__ 
#define __com_sun_star_animations_XCommand_idl__ 

#ifndef __com_sun_star_animations_XAnimationNode_idl__
#include <com/sun/star/animations/XAnimationNode.idl>
#endif

//============================================================================= 
 
 module com {  module sun {  module star {  module animations {  
 
//============================================================================= 

/** Execution of the XCommand animation node causes the slideshow component
    to call back the application to perform the command.
*/
interface XCommand : XAnimationNode
{
    /** The application specific target.
        See documentation of used application for supported targets.
    */
    [attribute] any Target;

    /** This identifies the application specific command.
        See documentation of used application for commands.
    */
    [attribute] short Command;

    /** The application specific parameter for this command.
        See documentation of used application for supported parameters
        for different commands and target combinations.
    */
    [attribute] any Parameter;
};

//============================================================================= 
 
}; }; }; };  

#endif
