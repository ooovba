/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XAnimateColor.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_animations_XAnimateColor_idl__ 
#define __com_sun_star_animations_XAnimateColor_idl__ 

#ifndef __com_sun_star_animations_XAnimate_idl__
#include <com/sun/star/animations/XAnimate.idl>
#endif

//============================================================================= 
 
 module com {  module sun {  module star {  module animations {  
 
//============================================================================= 

/** Interface for animation by defining color changes over time.
    <br>
    Only color value will be legal values for the following members
    <ul>
    <li><member>XAnimate::Values</member></li>
    <li><member>XAnimate::From</member></li>
    <li><member>XAnimate::To</member></li>
    <li><member>XAnimate::By</member></li>
    </ul>
    @see http://www.w3.org/TR/smil20/animation.html#edef-animateColor
*/
interface XAnimateColor : XAnimate
{
    /** defines the color space which is used to perform the interpolation.
        <br>
        @see <const>AnimateColorSpace</const>
    */
    [attribute] short ColorInterpolation;

    /** defines the direction which is used to perform the interpolation
        inside the color space defined with <member>ColorSpace</member>.
        <br>
        Values could be <true/> for clockwise and <false/> for counterclockwise.

        This attribute will be ignored for color spaces where this does
        not make any sense.
    */
    [attribute] boolean Direction;
};

//============================================================================= 
 
}; }; }; };  

#endif
