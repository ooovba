#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.6 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..$/..$/..

PRJNAME=offapi

TARGET=cssanimations
PACKAGE=com$/sun$/star$/animations

# --- Settings -----------------------------------------------------
.INCLUDE :  $(PRJ)$/util$/makefile.pmk

# ------------------------------------------------------------------------

IDLFILES=\
    AnimationAdditiveMode.idl\
    AnimationCalcMode.idl\
    AnimationColorSpace.idl\
    AnimationEndSync.idl\
    AnimationFill.idl\
    AnimationNodeType.idl\
    AnimationRestart.idl\
    AnimationTransformType.idl\
    AnimationValueType.idl\
    Event.idl\
    EventTrigger.idl\
    TargetProperties.idl \
    TimeFilterPair.idl\
    Timing.idl\
    TransitionSubType.idl\
    TransitionType.idl\
    ValuePair.idl\
    XAnimate.idl\
    XAnimateColor.idl\
    XAnimateMotion.idl\
    XAnimateSet.idl\
    XAnimateTransform.idl\
    XAnimationNode.idl\
    XAnimationNodeSupplier.idl\
    XAudio.idl\
    XIterateContainer.idl\
    XTargetPropertiesCreator.idl \
    XTimeContainer.idl\
    XTransitionFilter.idl\
    XCommand.idl\
    XAnimationListener.idl

# ------------------------------------------------------------------

.INCLUDE :  target.mk
.INCLUDE :  $(PRJ)$/util$/target.pmk
