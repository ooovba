/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TransitionSubType.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_animations_TransitionSubType_idl__ 
#define __com_sun_star_animations_TransitionSubType_idl__ 

//============================================================================= 
 
 module com {  module sun {  module star {  module animations {  
 
//============================================================================= 

/**	@see http://www.w3.org/TR/smil20/smil-transitions.html#Table%201:%20Taxonomy%20Table
*/
constants TransitionSubType
{
    const short DEFAULT = 0;
    const short LEFTTORIGHT = 1;
    const short TOPTOBOTTOM = 2;
    const short TOPLEFT = 3;
    const short TOPRIGHT = 4;
    const short BOTTOMRIGHT = 5;
    const short BOTTOMLEFT = 6;
    const short TOPCENTER = 7;
    const short RIGHTCENTER = 8;
    const short BOTTOMCENTER = 9;
    const short LEFTCENTER = 10;
    const short CORNERSIN = 11;
    const short CORNERSOUT = 12;
    const short VERTICAL = 13;
    const short HORIZONTAL = 14;
    const short DIAGONALBOTTOMLEFT = 15;
    const short DIAGONALTOPLEFT = 16;
    const short DOUBLEBARNDOOR = 17;
    const short DOUBLEDIAMOND = 18;
    const short DOWN = 19;
    const short LEFT = 20;
    const short UP = 21;
    const short RIGHT = 22;
    const short RECTANGLE = 25;
    const short DIAMOND = 26;
    const short CIRCLE = 27;
    const short FOURPOINT = 28;
    const short FIVEPOINT = 29;
    const short SIXPOINT = 30;
    const short HEART = 31;
    const short KEYHOLE = 32;
    const short CLOCKWISETWELVE = 33;
    const short CLOCKWISETHREE = 34;
    const short CLOCKWISESIX = 35;
    const short CLOCKWISENINE = 36;
    const short TWOBLADEVERTICAL = 37;
    const short TWOBLADEHORIZONTAL = 38;
    const short FOURBLADE = 39;
    const short CLOCKWISETOP = 40;
    const short CLOCKWISERIGHT = 41;
    const short CLOCKWISEBOTTOM = 42;
    const short CLOCKWISELEFT = 43;
    const short CLOCKWISETOPLEFT = 44;
    const short COUNTERCLOCKWISEBOTTOMLEFT = 45;
    const short CLOCKWISEBOTTOMRIGHT = 46;
    const short COUNTERCLOCKWISETOPRIGHT = 47;
    const short CENTERTOP = 48;
    const short CENTERRIGHT = 49;
    const short TOP = 50;
    const short BOTTOM = 52;
    const short FANOUTVERTICAL = 54;
    const short FANOUTHORIZONTAL = 55;
    const short FANINVERTICAL = 56;
    const short FANINHORIZONTAL = 57;
    const short PARALLELVERTICAL = 58;
    const short PARALLELDIAGONAL = 59;
    const short OPPOSITEVERTICAL = 60;
    const short OPPOSITEHORIZONTAL = 61;
    const short PARALLELDIAGONALTOPLEFT = 62;
    const short PARALLELDIAGONALBOTTOMLEFT = 63;
    const short TOPLEFTHORIZONTAL = 64;
    const short TOPLEFTDIAGONAL = 65;
    const short TOPRIGHTDIAGONAL = 66;
    const short BOTTOMRIGHTDIAGONAL = 67;
    const short BOTTOMLEFTDIAGONAL = 68;
    const short TOPLEFTCLOCKWISE = 69;
    const short TOPRIGHTCLOCKWISE = 70;
    const short BOTTOMRIGHTCLOCKWISE = 71;
    const short BOTTOMLEFTCLOCKWISE = 72;
    const short TOPLEFTCOUNTERCLOCKWISE = 73;
    const short TOPRIGHTCOUNTERCLOCKWISE = 74;
    const short BOTTOMRIGHTCOUNTERCLOCKWISE = 75;
    const short BOTTOMLEFTCOUNTERCLOCKWISE = 76;
    const short VERTICALTOPSAME = 77;
    const short VERTICALBOTTOMSAME = 78;
    const short VERTICALTOPLEFTOPPOSITE = 79;
    const short VERTICALBOTTOMLEFTOPPOSITE = 80;
    const short HORIZONTALLEFTSAME = 81;
    const short HORIZONTALRIGHTSAME = 82;
    const short HORIZONTALTOPLEFTOPPOSITE = 83;
    const short HORIZONTALTOPRIGHTOPPOSITE = 84;
    const short DIAGONALBOTTOMLEFTOPPOSITE = 85;
    const short DIAGONALTOPLEFTOPPOSITE = 86;
    const short TWOBOXTOP = 87;
    const short TWOBOXBOTTOM = 88;
    const short TWOBOXLEFT = 89;
    const short TWOBOXRIGHT = 90;
    const short FOURBOXVERTICAL = 91;
    const short FOURBOXHORIZONTAL = 92;
    const short VERTICALLEFT = 93;
    const short VERTICALRIGHT = 94;
    const short HORIZONTALLEFT = 95;
    const short HORIZONTALRIGHT = 96;
    const short FROMLEFT = 97;
    const short FROMTOP = 98;
    const short FROMRIGHT = 99;
    const short FROMBOTTOM = 100;
    const short CROSSFADE = 101;
    const short FADETOCOLOR = 102;
    const short FADEFROMCOLOR = 103;
    const short FADEOVERCOLOR = 104;
    // new
    const short THREEBLADE = 105;
    const short EIGHTBLADE = 106;
    const short ONEBLADE = 107;
    const short ACROSS = 108;
    const short TOPLEFTVERTICAL = 109;
    const short COMBHORIZONTAL = 110;
    const short COMBVERTICAL = 111;
    const short IN = 112;
    const short OUT = 113;
    const short ROTATEIN = 114;
    const short ROTATEOUT = 115;
    const short FROMTOPLEFT = 116;
    const short FROMTOPRIGHT = 117;
    const short FROMBOTTOMLEFT = 118;
    const short FROMBOTTOMRIGHT = 119;
};

//============================================================================= 
 
}; }; }; };  

#endif
