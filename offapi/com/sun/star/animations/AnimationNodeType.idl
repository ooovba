/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AnimationNodeType.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_animations_AnimationNodeType_idl__ 
#define __com_sun_star_animations_AnimationNodeType_idl__ 

//============================================================================= 
 
 module com {  module sun {  module star {  module animations {  
 
//============================================================================= 

/** This constants defines a type for an animation node.
    <br>
    It can be used to quickly identify semantic blocks inside an animation hierachy.

    @see AnimationNode
*/
constants AnimationNodeType
{
    /** Defines a custom time node. */
    const short CUSTOM = 0;

    /** Defines a parallel time container. */
    const short PAR = 1;
        
    /** Defines a sequence time container. */
    const short SEQ = 2;

    /** Defines an iterate time container. */
    const short ITERATE = 3;

    /** Defines a generic attribute animation. */
    const short ANIMATE = 4;

    /** Defines a simple mean of just setting the value of
        an attribute for a specified duration. */
    const short SET = 5;

    /** Defines a move animation along a path. */
    const short ANIMATEMOTION = 6;

    /** Defines an animation of a color attribute. */
    const short ANIMATECOLOR = 7;

    /** Defines an animation of a transformation attribute. */
    const short ANIMATETRANSFORM = 8;

    /** Defines an animation of a filter behavior. */
    const short TRANSITIONFILTER = 9;

    /** Defines an audio effect. */
    const short AUDIO = 10;

    /** Defines a command effect. */
    const short COMMAND = 11;

};

//============================================================================= 
 
}; }; }; };  

#endif
