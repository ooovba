/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ConfigurationRegistry.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_ConfigurationRegistry_idl__
#define __com_sun_star_configuration_ConfigurationRegistry_idl__

//=============================================================================

#ifndef __com_sun_star_registry_XSimpleRegistry_idl__
#include <com/sun/star/registry/XSimpleRegistry.idl>
#endif
#ifndef __com_sun_star_util_XFlushable_idl__
#include <com/sun/star/util/XFlushable.idl>
#endif

module com { module sun { module star { module configuration { 

//=============================================================================


/*	provides access to a configuration tree as a registry. 

    <p> Using the <type scope="com::sun::star::registry">XSimpleRegistry</type> 
    interface, the service can be bound to a subtree within the 
    configuration tree, as provided by an 
    <type scope="com.sun.star.configuration">ConfigurationProvider</type>
    service, which must be accessible from the service factory you use for the
    creation of this service.
    </p>
    <p>	There are some restrictions when accessing a configuration tree using a registry.
    Most of them are implications of the fact that a configuration tree is very static in
    it's structure. Removal and addition of sub nodes is allowed only for special
    nodes, and even then the new elements have to comply to a given scheme (which is some
    kind of attribute of the container node). So for instance the
    <member scope="com.sun.star.registry">XRegistryKey::createKey()</member> method is not allowed
    for some nodes.
    </p><p>
    Thus anybody using this service is strongly advised to read and understand the specification
    of an configuration provider (<type scope="com.sun.star.configuration">ConfigurationProvider</type>)
    and all of it's aspects.
    </p>

    @author	Frank Schoenheit
    @version 1.0 2000/07/06
*/

published service ConfigurationRegistry
{
    /** controls the binding of the object to a configuration sub tree.
        <p><member scope="com::sun::star::registry">XSimpleRegistry::open()</member> 
        is used to open a special subtree within the configuration tree.
        </p>
        <p>The parameters of this method control the location of the root of the to-be-opened
        configuration node and the access mode (read only or updatable).
        </p>
    */
    interface com::sun::star::registry::XSimpleRegistry;

    /** is used to commit changes to the configuration.
        <p>As specified for the 
        <type scope="com::sun::star::configuration">ConfigurationUpdateAccess</type>
        service, all changes made to an configuration subtree have to be commited 
        before they become persistent. 
        To do this for a configuration accessed as a registry, call
        <method scope="com::sun::star::util">XFlushable::flush</method>.
        </p>
        <p><strong>Warning:</strong><em>Changes that are not flushed will be 
        lost.</em>
        </p>
    */
    interface com::sun::star::util::XFlushable;
};


//=============================================================================

}; }; }; }; 


#endif

