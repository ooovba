/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XSchemaSupplier.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_backend_XSchemaSupplier_idl__
#define __com_sun_star_configuration_backend_XSchemaSupplier_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif 

#ifndef __com_sun_star_configuration_backend_XSchema_idl__
#include <com/sun/star/configuration/backend/XSchema.idl>
#endif 

#ifndef __com_sun_star_configuration_backend_BackendAccessException_idl__
#include <com/sun/star/configuration/backend/BackendAccessException.idl>
#endif

#ifndef __com_sun_star_lang_IllegalArgumentException_idl__
#include <com/sun/star/lang/IllegalArgumentException.idl>
#endif 

//============================================================================= 

module com { module sun { module star { module configuration { module backend {

//============================================================================= 

/** 
  provides access to configuration component schemas.

  @since OOo 1.1.2
*/
published interface XSchemaSupplier : ::com::sun::star::uno::XInterface 
{
    //------------------------------------------------------------------------- 

    /**
      Returns the schema information (component + templates)
      for a particular component.

      @param aComponent 
                component whose schema will be accessed

      @returns   
                an object allowing access to the various parts of the schema,
                <NULL/> if the component doesn't exist.

      @throws   com::sun::star::lang::IllegalArgumentException
                if the component identifier is invalid.

      @throws   com::sun::star::configuration::backend::BackendAccessException
                if an error occurs while accessing the data.
    */
    XSchema getComponentSchema([in] string aComponent)
        raises (BackendAccessException,
                com::sun::star::lang::IllegalArgumentException) ;

    //------------------------------------------------------------------------- 
} ;

//============================================================================= 

} ; } ; } ; } ; } ;

#endif 
