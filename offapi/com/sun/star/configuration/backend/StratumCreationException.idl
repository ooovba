/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: StratumCreationException.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_backend_StratumCreationException_idl__ 
#define __com_sun_star_configuration_backend_StratumCreationException_idl__ 
 
#ifndef __com_sun_star_configuration_backend_BackendSetupException_idl__
#include <com/sun/star/configuration/backend/BackendSetupException.idl> 
#endif 
 
//============================================================================= 

module com { module sun { module star { module configuration { module backend {
 
//============================================================================= 
 
/** is passed to an <type>InteractionHandler<type> when creating a stratum backend fails.

    @since OOo 2.0.0
 */
exception StratumCreationException : BackendSetupException
{ 
    /** 
        Identifier of the stratum service that could not be created.
    */
    string StratumService;

    /** 
        Initialization data passed to the stratum instance.
    */
    string StratumData;

}; 
 
//============================================================================= 
 
}; }; }; }; };
 
#endif 
