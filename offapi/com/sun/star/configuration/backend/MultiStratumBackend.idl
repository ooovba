/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: MultiStratumBackend.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_backend_MultiStratumBackend_idl__
#define __com_sun_star_configuration_backend_MultiStratumBackend_idl__

#ifndef __com_sun_star_configuration_backend_Backend_idl__
#include <com/sun/star/configuration/backend/Backend.idl>
#endif

#ifndef __com_sun_star_lang_XInitialization_idl__
#include <com/sun/star/lang/XInitialization.idl>
#endif

//=============================================================================

module com { module sun { module star { module configuration { module backend {

//=============================================================================

/**
   implements <type>Backend</type>  provides access to a configuration database
    composed of one or more storage backends containing settings used by software modules.
*/
service MultiStratumBackend
{
    //-------------------------------------------------------------------------

    /** characterizes the functionality.
      */
    service Backend ;


    /**
        allows initialization of backend data sources

      <p>If present a type
         <type scope="com::sun::star::configuration::bootstrap">BootstrapContext</type>
         must be passed, that provides further backend initialization settings.
      </p>


    */

    [optional] interface com::sun::star::lang::XInitialization ;

    //-------------------------------------------------------------------------
} ;

//=============================================================================

} ; } ; } ; } ; } ;
#endif
