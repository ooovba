/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: InteractionHandler.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_configuration_backend_InteractionHandler_idl__
#define __com_sun_star_configuration_backend_InteractionHandler_idl__

module com { module sun { module star {
    module lang { published interface XInitialization; };
    module task { published interface XInteractionHandler; };
}; }; };

module com { module sun { module star { module configuration { module backend {

//============================================================================
/** An interaction request handler that lets the user handle a number of well
    known requests via GUI dialogs.

    <P>The well known requests handled by this service include
    <UL>
        <LI><type>MergeRecoveryRequest</type>*</LI>
    </UL>
    The requests marked with an asterisk are only handled if (a) their
    continuations match certain restrictions (see below), and (b) the
    necessary resource strings are available (this can be exploited by
    applications that carry only a subset of all resource files with
    them).</P>

    <P>The continuation restrictions are as follows:  Let <VAR>C</VAR> be the
    subset of the provided continuations that are of type
    <type scope="com::sun::star::task">XInteractionApprove</type>,
    <type scope="com::sun::star::task">XInteractionDisapprove</type>,
    <type scope="com::sun::star::task">XInteractionRetry</type>, or
    <type scope="com::sun::star::task">XInteractionAbort</type> (or of a
    derived type).  All other continuations are ignored for these requests.
    The request is only handled if the set <VAR>C</VAR> is any of the
    following:
    <UL>
        <LI>Abort</LI>
        <LI>Retry, Abort</LI>
        <LI>Approve</LI>
        <LI>Approve, Abort</LI>
        <LI>Approve, Disapprove</LI>
        <LI>Approve, Disapprove, Abort</LI>
    </UL></P>

    @since OOo 2.0.0

    @see com::sun::star::task::InteractionHandler
 */
published service InteractionHandler
{
    //------------------------------------------------------------------------
    /** Handle an interaction request.
     */
    interface com::sun::star::task::XInteractionHandler;

    //------------------------------------------------------------------------
    /** Initialize the interaction handler.

        <P>The arguments must be a sequence of
        <type scope="com::sun::star::beans">PropertyValue</type>s.  The
        currently supported properties are:
        <UL>
            <LI><code>"Parent"</code> of type
            <type scope="com::sun::star::awt">XWindow</type> denotes the
            parent window for any GUI dialogs the interaction handler pops up;
            it is strongly recommended that this property is supplied;</LI>
            <LI><code>"Context"</code> of type <atom>string</atom> is a
            textual description of the current context (used, e.g., as a first
            line of text in error boxes); this property is optional.</LI>
        </UL></P>
     */
    interface com::sun::star::lang::XInitialization;
};

}; }; }; }; };

#endif
