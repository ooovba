/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: StringContext.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_rendering_StringContext_idl__
#define __com_sun_star_rendering_StringContext_idl__

module com { module sun { module star { module rendering {

/** Collection of string-related arguments used on all canvas text
    interfaces.<p>

    A possibly much larger string than later rendered is necessary
    here, because in several languages, glyph selection is context
    dependent.<p>

    @since OOo 2.0.0
 */
struct StringContext
{
    /** The complete text, from which a subset is selected by the
        parameters below.
     */
    string 	Text;

    //-------------------------------------------------------------------------

    /** Start position within the string.<p>

        The first character has index 0.<p>
     */
    long 	StartPosition;

    //-------------------------------------------------------------------------

    /** Length of the substring to actually use.<p>

        Must be within the range [0,INTMAX].<p>
    */
    long	Length;

};

}; }; }; };

#endif
