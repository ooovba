/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AnimationAttributes.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_rendering_AnimationAttributes_idl__
#define __com_sun_star_rendering_AnimationAttributes_idl__

#ifndef __com_sun_star_geometry_RealSize2D_idl__
#include <com/sun/star/geometry/RealSize2D.idl>
#endif

module com { module sun { module star { module rendering {

/** This structure contains attributes needed to run an animation.

    @since OOo 2.0.0
 */
struct AnimationAttributes
{
    /// Preferred duration of the animation sequence in seconds.
    double 										Duration;
    
    //-------------------------------------------------------------------------

    /** Repeat mode of the animation sequence.<p>
    
        This value determines how the [0,1] parameter space of the animation
        should be sweeped through. Permissible values are given in
        <type>AnimationRepeat</type>.<p>
        
        @see AnimationRepeat.
     */
    byte										RepeatMode;
    
    //-------------------------------------------------------------------------

    /** Size of the untrasnformed animation sequence.<p>

        This value specifies the size of the animation when rendered
        with the identity view transform. This permits
        e.g. <type>XSprite</type> implementations to cache rendered
        animation content in finite-sized bitmaps.<p>
     */
    ::com::sun::star::geometry::RealSize2D	UntransformedSize;
}; 

}; }; }; };

#endif
