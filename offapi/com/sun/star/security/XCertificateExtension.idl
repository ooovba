/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XCertificateExtension.idl,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 
//i20156 - new file for xmlsecurity module

/** -- idl definition -- **/

#ifndef __com_sun_star_security_XCertificateExtension_idl_
#define __com_sun_star_security_XCertificateExtension_idl_

#include <com/sun/star/uno/XInterface.idl>

module com { module sun { module star { module security {

/**
 * Interface of a PKI Certificate
 *
 * <p>This interface represents a x509 certificate.</p>
 */
interface XCertificateExtension : com::sun::star::uno::XInterface
{
    /**
     * Check whether it is a critical extension
     */
    boolean isCritical() ;

    /**
     * Get the extension object identifier in string.
     */
    [attribute, readonly] sequence< byte > ExtensionId ;

    /**
     * Get the extension value
     */
    [attribute, readonly] sequence< byte > ExtensionValue ;
}; 

} ; } ; } ; } ;

#endif 

