/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: LicenseDialog.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#if ! defined INCLUDED_com_sun_star_deployment_ui_LicenseDialog_idl
#define INCLUDED_com_sun_star_deployment_ui_LicenseDialog_idl

#include <com/sun/star/ui/dialogs/XExecutableDialog.idl>
#include <com/sun/star/awt/XWindow.idl>


module com { module sun { module star { module deployment { module ui {

/** The <type>LicenseDialog</type> is used to display a license text.
    
    @since OOo 2.0.4
*/
service LicenseDialog : com::sun::star::ui::dialogs::XExecutableDialog
{
    /** Create a GUI using the specific parent window and focus on the
        given context.
        
        @param xParent
               parent window
        @param licenseText
               text to be displayed
    */
    create( [in] com::sun::star::awt::XWindow xParent,
            [in] string licenseText );
};

}; }; }; }; };

#endif
