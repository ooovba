/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: thePackageManagerFactory.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#if ! defined INCLUDED_com_sun_star_deployment_thePackageManagerFactory_idl
#define INCLUDED_com_sun_star_deployment_thePackageManagerFactory_idl

#include <com/sun/star/deployment/XPackageManagerFactory.idl>


module com { module sun { module star { module deployment {

/** <type>thePackageManagerFactory</type> denotes the one and only
    <type>XPackageManagerFactory</type> object to be used.
    <p>
    The component context entry is
    <code>
    /singletons/com.sun.star.deployment.thePackageManagerFactory
    </code>.
    </p>
    
    @since OOo 2.0.0
*/
singleton thePackageManagerFactory : XPackageManagerFactory;

}; }; }; };

#endif
