/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: UpdateInformationEntry.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

module com { module sun { module star { module xml { module dom {
interface XElement;
}; }; }; }; };

module com { module sun { module star { module deployment {

/** Objects of this type are used as elements of the enumeration
    returned by <type>XUpdateInformationProvider</type>.
    
    @since OOo 2.3
*/
struct UpdateInformationEntry
{
    /** the DOM representation of an update information entry
     */
    com::sun::star::xml::dom::XElement UpdateDocument;

    /** the (optional) description for an update information
     *  entry extracted from the update feed container
     */
    string Description;
};

}; }; }; };

