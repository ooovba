/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: AccessBridge.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_accessibility_AccessBridge_idl__
#define __com_sun_star_accessibility_AccessBridge_idl__

module com { module sun { module star { module lang { 
     published interface XInitialization; 
}; }; }; };

module com { module sun { module star { module accessibility { 

/** The UNO <-> Java Access Bridge allows any native frame window
    to become accessible by implementing the XAccessible interface.
    
    <p>Therefor the UNO <-> Java Access Bridge utilizes the Java <-> Windows
    and Java <-> GNOME access bridge by registering the native frames as
    Java windows.</p>

    @since OOo 1.1.2
*/

published service AccessBridge
{
    /** Expects and instance of XExtendedToolkit as first parameter */
    interface ::com::sun::star::lang::XInitialization;
};
          
}; }; }; };

#endif
