/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XOrdinalSuffix.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef INCLUDED_com_sun_star_i18n_XOrdinalSuffix_idl
#define INCLUDED_com_sun_star_i18n_XOrdinalSuffix_idl

#include <com/sun/star/lang/Locale.idl>

//============================================================================

module com { module sun { module star { module i18n {

//============================================================================

/** provides access to locale specific ordinal suffix systems.

    @since OOo2.2

    @internal

    ATTENTION: This interface is marked <em>internal</em> and does not
    have the <em>published</em> flag, which means it is subject to
    change without notice and should not be used outside the OOo core.
    The current version is a draft and works only for English language
    locales. Future enhancements adding functionality for other locales
    should use the 'ordinal' RuleBasedNumberFormat of the ICU if
    possible, see
    http://icu.sourceforge.net/apiref/icu4c/classRuleBasedNumberFormat.html
    which might make it necessary to change the interface.
 */

interface XOrdinalSuffix : com::sun::star::uno::XInterface
{
    //------------------------------------------------------------------------
    /** Returns the ordinal suffix for the number, for example,
        "<b>st</b>", "<b>nd</b>", "<b>rd</b>", "<b>th</b>"
        in an English locale.
     */
    string getOrdinalSuffix( [in] long nNumber, [in] com::sun::star::lang::Locale aLocale );
};

//============================================================================
}; }; }; };
//============================================================================

#endif
