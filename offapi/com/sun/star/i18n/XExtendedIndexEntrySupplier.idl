/*************************************************************************
 * 
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XExtendedIndexEntrySupplier.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_i18n_XExtendedIndexEntrySupplier_idl__
#define __com_sun_star_i18n_XExtendedIndexEntrySupplier_idl__

#include <com/sun/star/i18n/XIndexEntrySupplier.idl>
#include <com/sun/star/lang/Locale.idl>

//=============================================================================

module com { module sun { module star { module i18n {

//=============================================================================


/**
    This interface provides information for creating "Table of Index"

    <p> It is derived from
    <type scope="::com::sun::star::i18n">XIndexEntrySupplier</type> and 
    provides following additional functionalities.</p>
    <ul>
    <li>Provide supported language/locale list.
    <li>Provide supported algorithm list.
    <li>Provide phonetic entry support for CJK languge.
    <li>Provide method to compare index entry.
    </ul>

     @since OOo 1.1.2
 */
published interface XExtendedIndexEntrySupplier : ::com::sun::star::i18n::XIndexEntrySupplier
{
    //-------------------------------------------------------------------------
    /**
        Returns locale list for which the IndexEntrySupplier provides service.
     */
    sequence < com::sun::star::lang::Locale > getLocaleList();

    //-------------------------------------------------------------------------
    /**
        Returns index algorithm list for specific locale
     */
    sequence < string > getAlgorithmList(
                        [in] com::sun::star::lang::Locale aLocale );

    //-------------------------------------------------------------------------
    /**
        Checks if Phonetic Entry should be used for the locale.
     */
    boolean usePhoneticEntry( [in] com::sun::star::lang::Locale aLocale);

    //-------------------------------------------------------------------------
    /**
        Returns phonetic candidate for index entry for the locale.
     */
    string getPhoneticCandidate( [in] string aIndexEntry,
                    [in] com::sun::star::lang::Locale aLocale);

    //-------------------------------------------------------------------------
    /**
        Loads index algorithm for the locale.

        @param aIndexAlgorithm
            Index algorithm to be loaded.
            
        @param nCollatorOptions
            Sorting option of <type
            scope="::com::sun::star::i18n">CollatorOptions</type> for
            comparing index entries

        @return
            <TRUE/> if algorithm successfully loaded,
            <FALSE/> else.
     */
    boolean loadAlgorithm( [in] com::sun::star::lang::Locale aLocale,
                    [in] string aIndexAlgorithm,
                    [in] long nCollatorOptions );

    //-------------------------------------------------------------------------
    /**
        Returns index key.
        
        <p> Note that loadAlgorithm should be called before calling 
        this function. </p>

        @param aIndexEntry
            Index entry

        @param aPhoneticEntry
            Phonetic entry

        @param aLocale
            Language attribute for index and phonetic entry. <br/>
            aLocale and the locale in loadAlgorithm may be different.
            In the case they are different, phonetic entry will not
            be used in the index key generation.
     */
    string getIndexKey( [in] string aIndexEntry, [in] string aPhoneticEntry,
                    [in] com::sun::star::lang::Locale aLocale );

    //-------------------------------------------------------------------------
    /**
        Compares index entries

        <p> Note that loadAlgorithm should be called before calling 
        this function. </p>

        @param aIndexEntry1 
        @param aIndexEntry2
            Index entries to be compared

        @param aPhoneticEntry1
        @param aPhoneticEntry2
            Phonetic entries to be compared

        @param aLocale1
        @param aLocale2
            Language attribute for index and phonetic entry. <br/>
            aLocale and the locale in loadAlgorithm may be different.
            In the case they are different, phonetic entry will not
            be used in the index key generation.
     */
    short compareIndexEntry( [in] string aIndexEntry1, [in] string aPhoneticEntry1,
                    [in] com::sun::star::lang::Locale aLocale1,
                    [in] string aIndexEntry2, [in] string aPhoneticEntry2,
                    [in] com::sun::star::lang::Locale aLocale2 );
};

//=============================================================================
}; }; }; };

#endif
