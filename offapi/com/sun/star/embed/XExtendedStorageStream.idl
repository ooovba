/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XExtendedStorageStream.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_XExtendedStorageStream_idl__
#define __com_sun_star_embed_XExtendedStorageStream_idl__

#ifndef __com_sun_star_io_XStream_idl__
#include <com/sun/star/io/XStream.idl>
#endif

#ifndef __com_sun_star_io_XSeekable_idl__
#include <com/sun/star/io/XSeekable.idl>
#endif

#ifndef __com_sun_star_embed_XEncryptionProtectedSource_idl__
#include <com/sun/star/embed/XEncryptionProtectedSource.idl>
#endif

#ifndef __com_sun_star_lang_XComponent_idl__
#include <com/sun/star/lang/XComponent.idl>
#endif

#ifndef __com_sun_star_beans_XPropertySet_idl__
#include <com/sun/star/beans/XPropertySet.idl>
#endif

#ifndef __com_sun_star_embed_XTransactedObject_idl__
#include <com/sun/star/embed/XTransactedObject.idl>
#endif

#ifndef __com_sun_star_embed_XTransactionBroadcaster_idl__
#include <com/sun/star/embed/XTransactionBroadcaster.idl>
#endif

//============================================================================

 module com {  module sun {  module star {  module embed {

//============================================================================
/** This interface allows access to an extended storage stream that might be
    transacted.
 */
interface XExtendedStorageStream
{
    // INTERFACES
    //
    // -----------------------------------------------------------------------
    /** Stream access.
     */
    interface ::com::sun::star::io::XStream;

    // -----------------------------------------------------------------------
    /** allows to control object lifetime.
     */
    interface ::com::sun::star::lang::XComponent;

    // -----------------------------------------------------------------------
    /** allows to seek to a specified position within the stream.
        
        <p>
        This interface must be supported in case either seekable readonly
        or read-write access is requested.
        </p>
     */
    [optional] interface ::com::sun::star::io::XSeekable;

    // -----------------------------------------------------------------------
    /** allows to set password to the stream.
    
        <p>
        This interface must be supported by a stream with readwrite access
        to allow to set a password that should be used next time the
        stream is stored if the encryption is supported. 
        </p>

        <p>
        If the password is set or changed by this interface and the
        stream is closed the new password should be used to get access to the
        stream next time.
        </p>
     */
    [optional] interface ::com::sun::star::embed::XEncryptionProtectedSource;

    // -----------------------------------------------------------------------
    /** allows to get access to stream properties.
     */
    [optional] interface ::com::sun::star::beans::XPropertySet;

    // -----------------------------------------------------------------------
    /** allows to have transacted access.
     */
    [optional] interface ::com::sun::star::embed::XTransactedObject;

    // -----------------------------------------------------------------------
    /** allows to register a listener for transaction actions.
        
        <p>
        If <type>XTransactedObject</type> interface is implemented this
        interface must be implemented as well.
        </p>
     */
    [optional] interface ::com::sun::star::embed::XTransactionBroadcaster;

};

//============================================================================

}; }; }; };

#endif

