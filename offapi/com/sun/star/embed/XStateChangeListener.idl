/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XStateChangeListener.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_XStateChangeListener_idl__ 
#define __com_sun_star_embed_XStateChangeListener_idl__ 
 
#ifndef __com_sun_star_lang_XEventListener_idl__ 
#include <com/sun/star/lang/XEventListener.idl> 
#endif 
 
#ifndef __com_sun_star_lang_EventObject_idl__ 
#include <com/sun/star/lang/EventObject.idl> 
#endif 
 
#ifndef __com_sun_star_embed_WrongStateException_idl__ 
#include <com/sun/star/embed/WrongStateException.idl> 
#endif 
 
//============================================================================
 
module com {  module sun {  module star {  module embed {  
 
//============================================================================
 
/** makes it possible to receive events when an embedded object changes it's
    state.
 */
interface XStateChangeListener: com::sun::star::lang::XEventListener
{ 
    //------------------------------------------------------------------------
    /** is called just before the object changes state.

        <p>
        Actually the listener can try to complain about state changing, but
        it is up to object to decide whether the state change can be
        prevented. Anyway the possibility to complain must be used very
        carefully.
        </p>

        @param aEvent
            specifies the object that is going to change own state

        @param nOldState
            specifies the old state of the object

        @param nNewState
            specifies the new state of the object

        @throws ::com::sun::star::embed::WrongStateException
            the state change is unexpected by listener
     */
    void changingState( [in] com::sun::star::lang::EventObject aEvent,
                        [in] long nOldState,
                        [in] long nNewState )
        raises( ::com::sun::star::embed::WrongStateException );

    //------------------------------------------------------------------------
    /** is called after the object has changed state.

        @param aEvent
            specifies the object that has changed own state

        @param nOldState
            specifies the old state of the object

        @param nNewState
            specifies the new state of the object
     */
    void stateChanged( [in] com::sun::star::lang::EventObject aEvent,
                        [in] long nOldState,
                        [in] long nNewState );
}; 
 
//============================================================================
 
}; }; }; };  
 
#endif

