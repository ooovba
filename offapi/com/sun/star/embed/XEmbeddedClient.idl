/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XEmbeddedClient.idl,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_XEmbeddedClient_idl__
#define __com_sun_star_embed_XEmbeddedClient_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_embed_WrongStateException_idl__
#include <com/sun/star/embed/WrongStateException.idl>
#endif

#ifndef __com_sun_star_embed_XComponentSupplier_idl__
#include <com/sun/star/embed/XComponentSupplier.idl>
#endif

#ifndef __com_sun_star_embed_ObjectSaveVetoException_idl__
#include <com/sun/star/embed/ObjectSaveVetoException.idl>
#endif



//============================================================================

 module com {  module sun {  module star {  module embed {

//============================================================================
/** represents common functionality for embedded clients.
 */
interface XEmbeddedClient: XComponentSupplier
{
    //------------------------------------------------------------------------
    /** asks client to let the object store itself.
    
        @throws com::sun::star::uno::ObjectSaveVetoException
            in case container whants to avoid saving of object

        @throws com::sun::star::uno::Exception
            in case of problems during saving
     */
    void saveObject()
        raises( ::com::sun::star::embed::ObjectSaveVetoException,
                ::com::sun::star::uno::Exception );

    //------------------------------------------------------------------------
    /** An object can use this method to notify the client when the object
        outplace window becomes visible or invisible.

        @param bVisible
            visibility state of the window

        @throws com::sun::star::embed::WrongStateException
            the object is in wrong state
     */
    void visibilityChanged( [in] boolean bVisible )
        raises( ::com::sun::star::embed::WrongStateException );

};

//============================================================================

}; }; }; };

#endif

