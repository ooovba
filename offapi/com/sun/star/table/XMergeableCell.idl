/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMergeableCell.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_XMergeableCell_idl__
#define __com_sun_star_table_XMergeableCell_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

#ifndef __com_sun_star_table_XCell_idl__
#include <com/sun/star/table/XCell.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** provides methods to access information about a cell that is mergeable with
    other sells.

    @see com::sun::star::table::Cell
 */
interface XMergeableCell: com::sun::star::table::XCell
{
    //-------------------------------------------------------------------------

    /** returns the number of columns this cell spans.
     */
    long getRowSpan();

    //-------------------------------------------------------------------------

    /** returns the number of rows this cell spans.
     */
    long getColumnSpan();

    //-------------------------------------------------------------------------

    /** returns <TRUE/> if this cell is merged with another cell.
     */
    boolean isMerged();
};

//=============================================================================

}; }; }; };

#endif

