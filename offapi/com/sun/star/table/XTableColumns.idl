/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XTableColumns.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_XTableColumns_idl__
#define __com_sun_star_table_XTableColumns_idl__

#ifndef __com_sun_star_container_XIndexAccess_idl__
#include <com/sun/star/container/XIndexAccess.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** provides methods to access columns via index and to insert and remove
    columns.

    @see com::sun::star::table::TableColumns
 */
published interface XTableColumns: com::sun::star::container::XIndexAccess
{
    //-------------------------------------------------------------------------

    /** inserts new columns.

        <p>When the index or combination of index and count is out
        of bounds an exception will be thrown.</p>

        @param nIndex
            is the index the first inserted column will have.

        @param nCount
            is the number of columns to insert.
     */
    void insertByIndex( [in] long nIndex, [in] long nCount );

    //-------------------------------------------------------------------------

    /** deletes columns.

        <p>When the index or combination of index and count is out
        of bounds an exception will be thrown.</p>

        @param nIndex
            is the index of the first column to delete.

        @param nCount
            is the number of columns to delete.
     */
    void removeByIndex( [in] long nIndex, [in] long nCount );

};

//=============================================================================

}; }; }; };

#endif

