/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: BorderLine.idl,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_BorderLine_idl__
#define __com_sun_star_table_BorderLine_idl__

#ifndef __com_sun_star_util_Color_idl__
#include <com/sun/star/util/Color.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** describes the line type for a single cell edge.
 */
published struct BorderLine
{
    //-------------------------------------------------------------------------

    /** contains the color value of the line.
     */
    com::sun::star::util::Color Color;

    //-------------------------------------------------------------------------

    /** contains the width of the inner part of a double line (in 1/100 mm).

        <p>If this value is zero, only a single line is drawn.</p>
     */
    short InnerLineWidth;

    //-------------------------------------------------------------------------

    /** contaions the width of a single line or the width of outer part of
        a double line (in 1/100 mm).

        <p>If this value is zero, no line is drawn.</p>
     */
    short OuterLineWidth;

    //-------------------------------------------------------------------------

    /** contains the distance between the inner and outer parts of a
        double line (in 1/100 mm).
     */
    short LineDistance;

};

//=============================================================================

}; }; }; };

#endif

