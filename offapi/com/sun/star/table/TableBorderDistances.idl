/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TableBorderDistances.idl,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_TableBorderDistances_idl__
#define __com_sun_star_table_TableBorderDistances_idl__

#ifndef __com_sun_star_table_BorderLine_idl__
#include <com/sun/star/table/BorderLine.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** contains the distance settings of the border lines of all cells in a cell
    range.

    <p>In a queried structure, the flags in
        <member>TableBorderDistances::Is...DistanceValid</member> indicate that not all
    lines of the boxes have the same values.</p>

    <p>In a structure which is used for setting, these flags determine
        if the corresponding distance should be set or if the old value should
    be kept.</p>
 */
published struct TableBorderDistances
{

        /** contains the distance between the top lines and other contents.
     */
        short TopDistance;

    //-------------------------------------------------------------------------

        /** specifies whether the value of <member>TableBorder::TopDistance</member>
        is used.
     */
        boolean IsTopDistanceValid;
        //-------------------------------------------------------------------------

        /** contains the distance between the bottom lines and other contents.
         */
        short BottomDistance;

        //-------------------------------------------------------------------------

        /** specifies whether the value of <member>TableBorder::BottomDistance</member>
                is used.
         */
        boolean IsBottomDistanceValid;
        //-------------------------------------------------------------------------

        /** contains the distance between the left lines and other contents.
         */
        short LeftDistance;

        //-------------------------------------------------------------------------

        /** specifies whether the value of <member>TableBorder::LeftDistance</member>
                is used.
         */
        boolean IsLeftDistanceValid;
        //-------------------------------------------------------------------------

        /** contains the distance between the right lines and other contents.
         */
        short RightDistance;

        //-------------------------------------------------------------------------

        /** specifies whether the value of <member>TableBorder::RightDistance</member>
                is used.
         */
        boolean IsRightDistanceValid;

};

//=============================================================================

}; }; }; };

#endif

