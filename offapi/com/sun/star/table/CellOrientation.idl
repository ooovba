/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: CellOrientation.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_CellOrientation_idl__
#define __com_sun_star_table_CellOrientation_idl__

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** specifies the orientation of a cell.
 */
published enum CellOrientation
{
    //-------------------------------------------------------------------------

    /** contents are printed from left to right.
     */
    STANDARD,

    //-------------------------------------------------------------------------

    /** contents are printed from top to bottom.
     */
    TOPBOTTOM,

    //-------------------------------------------------------------------------

    /** contents are printed from bottom to top.
     */
    BOTTOMTOP,

    //-------------------------------------------------------------------------

    /** contents are printed from top to bottom with individual
        characters in normal (horizontal) orientation.
     */
    STACKED

};

//=============================================================================

}; }; }; };

#endif

