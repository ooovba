/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TableSortDescriptor2.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_TableSortDescriptor2_idl__
#define __com_sun_star_table_TableSortDescriptor2_idl__

#ifndef __com_sun_star_util_SortDescriptor2_idl__ 
#include <com/sun/star/util/SortDescriptor2.idl>
#endif
#ifndef __com_sun_star_beans_XPropertySet_idl__
#include <com/sun/star/beans/XPropertySet.idl>
#endif
#ifndef __com_sun_star_table_TableSortField_idl__
#include <com/sun/star/table/TableSortField.idl>
#endif

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** specifies properties which describe sorting of fields (rows or columns) 
    in a table.

    @since OOo 1.1.2
 */
published service TableSortDescriptor2
{
    service com::sun::star::util::SortDescriptor2;

    //-------------------------------------------------------------------------
    /** specifies a list of individual sort fields.

        <p>Each entry specifies properties that state the
        row/column to be sorted and how that should be done.</p>
     */
    [property] sequence< com::sun::star::table::TableSortField > SortFields;

    //-------------------------------------------------------------------------
    /** contains the maximum number of sort fields the descriptor can hold.
     */
    [readonly, property] long MaxSortFieldsCount;

    //------------------------------------------------------------------------- 
    /** specifies if the columns or rows are to be sorted.
        
        <dl>
            <dt><TRUE/></dt>
            <dd>The columns are to be sorted.</dd>
            <dt><FALSE/></dt>
            <dd>The rows are to be sorted.</dd>
        </dl>
     */
    [property] boolean IsSortColumns; 
};

//=============================================================================

}; }; }; };

#endif

