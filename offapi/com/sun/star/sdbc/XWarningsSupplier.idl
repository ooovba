/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XWarningsSupplier.idl,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbc_XWarningsSupplier_idl__ 
#define __com_sun_star_sdbc_XWarningsSupplier_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_sdbc_SQLWarning_idl__ 
#include <com/sun/star/sdbc/SQLWarning.idl> 
#endif 
 
 module com {  module sun {  module star {  module sdbc { 
 
 
/** should be implemented of objects which may report warnings or non critical
    errors.
    @see com::sun::star::sdbc::SQLWarning
 */
published interface XWarningsSupplier: com::sun::star::uno::XInterface
{ 
    //------------------------------------------------------------------------- 
     
    /** returns the first warning reported by calls on an object that supports
        the usage of warnings.
        
        <p>
        <b>Note:</b> Subsequent warnings will be chained to this
        <type scope="com::sun::star::sdbc">SQLWarning</type>
        .		
        </p>
        @returns
            the warnings
        @throws SQLException 
                if a database access error occurs.
     */
    any getWarnings() raises (SQLException); 
 
    //------------------------------------------------------------------------- 
     
    /** clears all warnings reported for the object implementing the interface.
        After a call to this method, the method 
        <member scope="com::sun::star::sdbc">XWarningsSupplier::getWarnings()</member>
        returns 
        <void/>
        until a new warning is reported for the object.
        @throws SQLException 
                if a database access error occurs.
     */
    void clearWarnings() raises (SQLException); 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*=========================================================================== 
===========================================================================*/ 
#endif 
