/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: BestRowType.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbc_BestRowType_idl__ 
#define __com_sun_star_sdbc_BestRowType_idl__ 
 
 module com {  module sun {  module star {  module sdbc { 
 
 
/** determines the type of the best row identifier.
 */
published constants BestRowType
{ 
     
    /** indicates that the best row identifier may or may not be a pseudo-column. 
             A possible value for the column 
             <code>PSEUDO_COLUMN</code>
             in the
             <type scope="com::sun::star::sdbc">XResultSet</type>
             object 
             returned by the method 
             <member>XDatabaseMetaData::getBestRowIdentifier()</member>
             .
     */
    const long UNKNOWN	= 0; 
    //------------------------------------------------------------------------- 
     
    /** indicates that the best row identifier is NOT a pseudo-column. 
             A possible value for the column 
             <code>PSEUDO_COLUMN</code>
             in the
             <type scope="com::sun::star::sdbc">XResultSet</type>
             object 
             returned by the method 
             <member>XDatabaseMetaData::getBestRowIdentifier()</member>
             .
     */
    const long NOT_PSEUDO	= 1; 
    //------------------------------------------------------------------------- 
     
    /** indicates that the best row identifier is a pseudo-column. 
             A possible value for the column 
             <code>PSEUDO_COLUMN</code>
             in the
             <type scope="com::sun::star::sdbc">XResultSet</type>
             object 
             returned by the method
             <member>XDatabaseMetaData::getBestRowIdentifier()</member>
             .
     */
    const long PSEUDO	= 2; 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*=========================================================================== 
===========================================================================*/ 
#endif 
