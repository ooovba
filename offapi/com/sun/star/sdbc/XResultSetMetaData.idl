/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XResultSetMetaData.idl,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbc_XResultSetMetaData_idl__ 
#define __com_sun_star_sdbc_XResultSetMetaData_idl__ 
 
#ifndef __com_sun_star_uno_XInterface_idl__ 
#include <com/sun/star/uno/XInterface.idl> 
#endif 
 
#ifndef __com_sun_star_sdbc_SQLException_idl__ 
#include <com/sun/star/sdbc/SQLException.idl> 
#endif 
 
 module com {  module sun {  module star {  module sdbc { 
 
 
/** can be used to find out about the types and properties
    of the columns in a ResultSet.
 */
published interface XResultSetMetaData: com::sun::star::uno::XInterface
{ 
     
    /** returns the number of columns in this ResultSet.
        @returns
            the column count
        @throws SQLException 
            if a database access error occurs.
     */
    long getColumnCount() raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether the column is automatically numbered, thus read-only.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isAutoIncrement([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether a column's case matters.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isCaseSensitive([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether the column can be used in a where clause.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isSearchable([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether the column is a cash value.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isCurrency([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates the nullability of values in the designated column.@see com::sun::star::sdbc::ColumnValue
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    long isNullable([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether values in the column are signed numbers.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isSigned([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates the column's normal max width in chars.
        @param column
            the first column is 1, the second is 2,
        @returns
            the normal maximum number of characters allowed as the width of the designated column
        @throws SQLException 
            if a database access error occurs.
     */
    long getColumnDisplaySize([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets the suggested column title for use in printouts and
        displays.
        @param column
            the first column is 1, the second is 2,
        @returns
            the suggested column title
        @throws SQLException 
            if a database access error occurs.
     */
    string getColumnLabel([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets a column's name.
        @param column
            the first column is 1, the second is 2,
        @returns
            the column name
        @throws SQLException 
            if a database access error occurs.
     */
    string getColumnName([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets a column's table's schema.
        @param column
            the first column is 1, the second is 2,
        @returns
            the schema name
        @throws SQLException 
            if a database access error occurs.
     */
    string getSchemaName([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets a column's number of decimal digits.
        @param column
            the first column is 1, the second is 2,
        @returns
            precision
        @throws SQLException 
            if a database access error occurs.
     */
    long getPrecision([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets a column's number of digits to right of the decimal point.
        @param column
            the first column is 1, the second is 2,
        @returns
            scale
        @throws SQLException 
            if a database access error occurs.
     */
    long getScale([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets a column's table name.
        @param column
            the first column is 1, the second is 2,
        @returns
            the table name
        @throws SQLException 
            if a database access error occurs.
     */
    string getTableName([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** gets a column's table's catalog name.
        @param column
            the first column is 1, the second is 2,
        @returns
            the catalog name
        @throws SQLException 
            if a database access error occurs.
     */
    string getCatalogName([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** retrieves a column's SQL type.
        @param column
            the first column is 1, the second is 2,
        @returns
            the column type
        @throws SQLException 
            if a database access error occurs.
     */
    long getColumnType([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** retrieves a column's database-specific type name.
        @param column
            the first column is 1, the second is 2,
        @returns
            the type name
        @throws SQLException 
            if a database access error occurs.
     */
    string getColumnTypeName([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether a column is definitely not writable.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isReadOnly([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether it is possible for a write on the column to succeed.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isWritable([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** indicates whether a write on the column will definitely succeed.
        @param column
            the first column is 1, the second is 2,
        @returns
            <TRUE/> if so
        @throws SQLException 
            if a database access error occurs.
     */
    boolean isDefinitelyWritable([in]long column) raises (SQLException); 
    //------------------------------------------------------------------------- 
     
    /** returns the fully-qualified name of the service whose instances
        are manufactured if the method 
        <member scope="com::sun::star::sdbc">XResultSet::.getObject()</member>
        is called to retrieve a value from the column.
        @param column
            the first column is 1, the second is 2,
        @returns
            the service name
        @throws SQLException 
            if a database access error occurs.
     */
    string getColumnServiceName([in]long column) raises (SQLException); 
}; 
 
//============================================================================= 
 
}; }; }; }; 
 
/*=========================================================================== 
===========================================================================*/ 
#endif 
