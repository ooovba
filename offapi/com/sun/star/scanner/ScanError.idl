/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ScanError.idl,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_scanner_ScanError_idl__
#define __com_sun_star_scanner_ScanError_idl__


//=============================================================================

module com { module sun { module star { module scanner { 

//=============================================================================
/** enum ScanError describes error codes of scanner component
*/
published enum ScanError
{
    //-------------------------------------------------------------------------
    // DOCUMENTATION CHANGED FOR ScanError::	ScanErrorNone,
    /** ScanErrorNone: no error occured
     */
    ScanErrorNone,

    //-------------------------------------------------------------------------
    // DOCUMENTATION CHANGED FOR ScanError::	ScannerNotAvailable,
    /** ScannerNotAvailable: the requested device could not be opened
     */
    ScannerNotAvailable,

    //-------------------------------------------------------------------------
    // DOCUMENTATION CHANGED FOR ScanError::	ScanFailed,
    /** ScanFailed: an error occured during scanning
     */
    ScanFailed,

    //-------------------------------------------------------------------------
    // DOCUMENTATION CHANGED FOR ScanError::	ScanInProgress,
    /** ScanInProgress: a scan is already in progress on this device that has
        to end before a new one can be started
    */
    ScanInProgress,

    //-------------------------------------------------------------------------
    // DOCUMENTATION CHANGED FOR ScanError::	ScanCanceled,
    /** ScanCanceled: the scan was canceled by the user
     */
    ScanCanceled,

    //-------------------------------------------------------------------------
    // DOCUMENTATION CHANGED FOR ScanError::	InvalidContext
    /** InvalidContext: a device was requested that does not exist
     */
    InvalidContext

};

//=============================================================================

}; }; }; }; 

#endif
