/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: GraphicObject.idl,v $
 * $Revision: 1.1.2.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef com_sun_star_graphic_GraphicObject_idl
#define com_sun_star_graphic_GraphicObject_idl

#include <com/sun/star/graphic/XGraphicObject.idl>

module com { module sun { module star { module graphic
{
/** The <code>GraphicObject</code> service can be used to create <type>XGraphicObject</type> instances.

    <p><type>XGraphicObject</type> objects are accessable using GraphicObject scheme urls like
    <code>vnd.sun.star.GraphicObject:10000000000001940000012FB99807BD</code>.
    As long as at least one instance of <type>XGraphicObject</type> with a particular UniqueID exists,
    the associated image/graphic is available.</p>

    @see GraphicObject
    @see GraphicProvider 
    @see MediaProperties 
*/

service GraphicObject : XGraphicObject
{
    /** Creates an <type>GraphicObject</type>
    */
    create();

    /** Creates an <type>GraphicObject</type> with <code>uniqueId</code>
        @param uniqueId 
            If another <type>XGraphicObject</type> with <code>uniqueId</code> exists, this GraphicObject
            is populated with the other <type>GraphicObject</type>'s data.
    */
    createWithId( [in] string uniqueId );
};

} ; } ; } ; } ; 

#endif
