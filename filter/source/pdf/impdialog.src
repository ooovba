/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: impdialog.src,v $
 * $Revision: 1.41.4.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "impdialog.hrc"

#define TAB_PDF_SIZE Size = MAP_APPFONT ( 176, 239 )
//string for TabDialog standard buttons
String STR_PDF_EXPORT
{
        Text[ en-US ] = "E~xport";
};

//strings used in encryption UI

//password dialog title
String STR_PDF_EXPORT_UDPWD
{
    Text[ en-US ] = "Set Open Password";
};

//password dialog title
String STR_PDF_EXPORT_ODPWD
{
    Text[ en-US ] = "Set Permission Password";
};

//////////////////////////////////////////////////////////////
//tab page for PDF Export, general preferences
TabPage  RID_PDF_TAB_GENER
{
    HelpId = HID_FILTER_PDF_OPTIONS ;
    Hide = TRUE ;
    Text[ en-US ] = "General";
    TAB_PDF_SIZE;

    FixedLine FL_PAGES
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Range";
    };
    RadioButton RB_ALL
    {
        Pos = MAP_APPFONT ( 12 , 14 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~All";
    };
    RadioButton RB_RANGE
    {
        Pos = MAP_APPFONT ( 12 , 27 ) ;
        Size = MAP_APPFONT ( 101 , 10 ) ;
        Text[ en-US ] = "~Pages";
    };
    Edit ED_PAGES
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 116, 26 ) ;
        Size = MAP_APPFONT ( 48 , 12 ) ;
    };
    RadioButton RB_SELECTION
    {
        Pos = MAP_APPFONT ( 12 , 40 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Selection";
    };
    FixedLine FL_IMAGES
    {
        Pos = MAP_APPFONT ( 6 , 53 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Images";
    };
    RadioButton RB_LOSSLESSCOMPRESSION
    {
        Pos = MAP_APPFONT ( 12 , 64 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Lossless compression";
    };
    RadioButton RB_JPEGCOMPRESSION
    {
        Pos = MAP_APPFONT ( 12 , 76 ) ;
        Size = MAP_APPFONT ( 158, 10 ) ;
        Text[ en-US ] = "~JPEG compression";
    };
    FixedText FT_QUALITY
    {
        Pos = MAP_APPFONT ( 30 , 89 ) ;
        Size = MAP_APPFONT ( 83, 10 ) ;
        Text[ en-US ] = "~Quality";
    };
    MetricField NF_QUALITY
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 116, 88 ) ;
        Size = MAP_APPFONT ( 48, 12 ) ;
        TabStop = TRUE ;
        Spin = TRUE ;
        StrictFormat = TRUE ;
        Last = 100 ;
        Repeat = TRUE ;
    };
    CheckBox CB_REDUCEIMAGERESOLUTION
    {
        Pos = MAP_APPFONT ( 12 , 103 ) ;
        Size = MAP_APPFONT ( 101 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Reduce image resolution";
    };
    ComboBox CO_REDUCEIMAGERESOLUTION
    {
        Pos = MAP_APPFONT ( 116 , 102 ) ;
        Size = MAP_APPFONT ( 48 , 50 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        StringList =
        {
            "75 DPI" ;
            "150 DPI" ;
            "300 DPI" ;
            "600 DPI" ;
            "1200 DPI" ;
        };
    };
    FixedLine FL_GENERAL
    {
        Pos = MAP_APPFONT ( 6 , 117 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "General";
    };
    CheckBox CB_PDFA_1B_SELECT
    {
        Pos = MAP_APPFONT ( 12, 128 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "P~DF/A-1a";
    };
    CheckBox CB_TAGGEDPDF
    {
        Pos = MAP_APPFONT ( 12 , 141 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Tagged PDF";
    };
    CheckBox CB_EXPORTFORMFIELDS
    {
        Pos = MAP_APPFONT ( 12 , 154 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Create PDF form";
    };
    FixedText FT_FORMSFORMAT
    {
        Pos = MAP_APPFONT ( 30 , 168 ) ;
        Size = MAP_APPFONT ( 93, 8 ) ;
        Text[ en-US ] = "Submit ~format";
    };
    ListBox LB_FORMSFORMAT
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 126, 166 ) ;
        Size = MAP_APPFONT ( 38, 48 ) ;
        DeltaLang = < Default ; Default ; Default ; Default ; > ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        StringList =
        {
            < "FDF" ; Default; > ;
            < "PDF" ; > ;
            < "HTML" ; > ;
            < "XML" ; > ;
        };
    };
    CheckBox CB_EXPORTBOOKMARKS
    {
        Pos = MAP_APPFONT ( 12 , 180 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Export ~bookmarks";
    };
    CheckBox CB_EXPORTNOTES
    {
        Pos = MAP_APPFONT ( 12 , 193 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Export comments";
    };
    CheckBox CB_EXPORTEMPTYPAGES
    {
        Pos = MAP_APPFONT ( 12 , 206 ) ;
        Size = MAP_APPFONT ( 158 , 16 ) ;
        TabStop = TRUE ;
        WordBreak = TRUE ;
        Text[ en-US ] = "Exp~ort automatically inserted blank pages";
    };
    CheckBox CB_ADDSTREAM
    {
        Pos = MAP_APPFONT ( 12 , 224 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Create ~hybrid file";
    };
};

//----------------------------------------------------------
//tab page for PDF Export, opening features
TabPage  RID_PDF_TAB_OPNFTR
{
    HelpId = HID_FILTER_PDF_INITIAL_VIEW ;
    Text[ en-US ] = "Initial View" ;
    TAB_PDF_SIZE;
    Hide = TRUE;

////////////////////////////////////////
    FixedLine FL_INITVIEW
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Panes" ;
    };
    RadioButton  RB_OPNMODE_PAGEONLY
    {
        Pos = MAP_APPFONT ( 12 , 14 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Page only" ;
    };
    RadioButton  RB_OPNMODE_OUTLINE
    {
        Pos = MAP_APPFONT ( 12 , 26 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Bookmarks and page" ;
    };
    RadioButton  RB_OPNMODE_THUMBS
    {
        Pos = MAP_APPFONT ( 12 , 38 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Thumbnails and page" ;
    };

    FixedText FT_MAGNF_INITIAL_PAGE
    {
        Pos = MAP_APPFONT( 12, 52 );
        Size = MAP_APPFONT( 109, 10 );
        Text[ en-US ] = "Open on page";
    };
    NumericField NUM_MAGNF_INITIAL_PAGE
    {
        Pos = MAP_APPFONT( 124, 52 );
        Size = MAP_APPFONT( 40, 12 );
        Value = 1;
        Spin = TRUE;
        Border = TRUE;
        Minimum = 1;
    };

///////////////////////////////////////////////////
    FixedLine FL_MAGNIFICATION
    {
        Pos = MAP_APPFONT ( 6 , 68 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Magnification" ;
    };
    RadioButton  RB_MAGNF_DEFAULT
    {
        // see PDF ref v 1.5 tab 8.2, pg. 542 ( /XYZ )
        Pos = MAP_APPFONT ( 12 , 80 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Default" ;
    };
    RadioButton  RB_MAGNF_WIND
    {
        // see PDF ref v 1.5 tab 8.2, pg. 542 ( /Fit )
        Pos = MAP_APPFONT ( 12 , 92 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Fit in window" ;
    };
    RadioButton  RB_MAGNF_WIDTH
    {
        // see PDF ref v 1.5 tab 8.2, pg. 542 ( /FitH top )
        Pos = MAP_APPFONT ( 12 , 104 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "Fit ~width" ;
    };
    RadioButton  RB_MAGNF_VISIBLE
    {
        // see PDF ref v 1.5 tab 8.2, pg. 542 ( /FitBH top )
        Pos = MAP_APPFONT ( 12 , 116 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "Fit ~visible" ;
    };
    RadioButton  RB_MAGNF_ZOOM
    {
        // see PDF ref v 1.6 tab 8.2, pg. 551 ( /XYZ left top zoom )
        Pos = MAP_APPFONT ( 12 , 128 ) ;
        Size = MAP_APPFONT ( 109 , 10 ) ;
        Text[ en-US ] = "~Zoom factor" ;
    };
    MetricField NUM_MAGNF_ZOOM
    {
        Pos = MAP_APPFONT( 124, 128 ) ;
        Size = MAP_APPFONT( 40, 12 ) ;
        Unit = FUNIT_PERCENT;
        Value = 100;
        Spin = TRUE;
        Border = TRUE;
        Minimum = 50;
        Maximum = 1600;
    };
////////////////////////////////////////
    FixedLine FL_PAGE_LAYOUT
    {
        Pos = MAP_APPFONT ( 6 , 146 ) ;
        Size = MAP_APPFONT (164  , 8 ) ;
        Text[ en-US ] = "Page layout" ;
    };
    RadioButton  RB_PGLY_DEFAULT
    {
        Pos = MAP_APPFONT ( 12 , 158 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "D~efault" ;
    };
    RadioButton  RB_PGLY_SINGPG
    {
        Pos = MAP_APPFONT ( 12 , 170 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Single page" ;
    };
    RadioButton  RB_PGLY_CONT
    {
        Pos = MAP_APPFONT ( 12 , 182 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Continuous" ;
    };
    RadioButton  RB_PGLY_CONTFAC
    {
        Pos = MAP_APPFONT ( 12 , 194 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "C~ontinuous facing" ;
    };
    CheckBox  CB_PGLY_FIRSTLEFT
    {
        Pos = MAP_APPFONT ( 22 , 206 ) ;
        Size = MAP_APPFONT ( 148 , 10 ) ;
        Text[ en-US ] = "First page is ~left" ;
    };
};

//----------------------------------------------------------
//tab page for PDF Export, viewer preferences
TabPage  RID_PDF_TAB_VPREFER
{
    HelpId = HID_FILTER_PDF_USER_INTERFACE ;
    Text[ en-US ] = "User Interface" ;
    TAB_PDF_SIZE;
    Hide = TRUE;

//////////////////////////////////////
    FixedLine FL_WINOPT
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Window options" ;
    };

    CheckBox CB_WNDOPT_RESINIT
    {
        Pos = MAP_APPFONT ( 12 , 14 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Resize window to initial page";
    };
    CheckBox CB_WNDOPT_CNTRWIN
    {
        Pos = MAP_APPFONT ( 12 , 26 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Center window on screen";
    };
    CheckBox CB_WNDOPT_OPNFULL
    {
        Pos = MAP_APPFONT ( 12 , 38 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "~Open in full screen mode" ;
    };
    CheckBox CB_DISPDOCTITLE
    {
        Pos = MAP_APPFONT ( 12 , 50 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Display document title";
    };

////////////////////////////////
    FixedLine FL_USRIFOPT
    {
        Pos = MAP_APPFONT ( 6 , 64 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "User interface options" ;
    };
    CheckBox CB_UOP_HIDEVMENUBAR
    {
        Pos = MAP_APPFONT ( 12 , 76 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Hide ~menubar";
    };
    CheckBox CB_UOP_HIDEVTOOLBAR
    {
        Pos = MAP_APPFONT ( 12 , 88 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Hide ~toolbar";
    };
    CheckBox CB_UOP_HIDEVWINCTRL
    {
        Pos = MAP_APPFONT ( 12 , 100 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Hide ~window controls";
    };

////////////////////////////////
    FixedLine FL_TRANSITIONS
    {
        Pos = MAP_APPFONT ( 6 , 114 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Transitions" ;
    };
    CheckBox CB_TRANSITIONEFFECTS
    {
        Pos = MAP_APPFONT ( 12 , 126 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "~Use transition effects";
    };
////////////////////////////////
    FixedLine FL_BOOKMARKS
    {
        Pos = MAP_APPFONT ( 6 , 140 ) ;
        Size = MAP_APPFONT ( 164 , 8 ) ;
        Text[ en-US ] = "Bookmarks" ;
    };
    RadioButton RB_ALLBOOKMARKLEVELS
    {
        Pos = MAP_APPFONT ( 12 , 152 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        Text[ en-US ] = "All bookmark levels";
    };
    RadioButton RB_VISIBLEBOOKMARKLEVELS
    {
        Pos = MAP_APPFONT ( 12 , 166 ) ;
        Size = MAP_APPFONT ( 117 , 10 ) ;
        Text[ en-US ] = "Visible bookmark levels";
    };
    NumericField NUM_BOOKMARKLEVELS
    {
        Pos = MAP_APPFONT ( 132 , 165 ) ;
        Size = MAP_APPFONT ( 32 , 12 ) ;
        Border = TRUE;
        Spin = TRUE;
        Minimum = 1;
        Maximum = 10;
    };
};

//----------------------------------------------------------
//tab page for PDF Export, security
TabPage  RID_PDF_TAB_SECURITY
{
    HelpId = HID_FILTER_PDF_SECURITY ;
    Text [ en-US ] = "Security";
    TAB_PDF_SIZE;
    Hide = TRUE;

//////////////////////////////////////
    PushButton BTN_USER_PWD
    {
        TabStop = TRUE ;
        Disable = TRUE ;
        Pos = MAP_APPFONT ( 12, 5 ) ;
        Size = MAP_APPFONT ( 120 , 13 ) ;
        Text[ en-US ] = "Set ~open password...";
    };

    FixedText FT_USER_PWD
    {
        Pos = MAP_APPFONT(12 , 25 );
        Size = MAP_APPFONT( 160, 20 );
    };
    
    String  STR_USER_PWD_SET
    {
        Text [ en-US ] = "Open password set";
    };

    String  STR_USER_PWD_ENC
    {
        Text [ en-US ] = "PDF document will be encrypted";
    };

    String  STR_USER_PWD_UNSET
    {
        Text [ en-US ] = "No open password set";
    };

    String  STR_USER_PWD_UNENC
    {
        Text [ en-US ] = "PDF document will not be encrypted";
    };

    PushButton BTN_OWNER_PWD
    {
        TabStop = TRUE ;
        Disable = TRUE ;
        Pos = MAP_APPFONT ( 12, 45 ) ;
        Size = MAP_APPFONT ( 120 , 13 ) ;
        Text[ en-US ] = "Set ~permission password...";
    };

    FixedText FT_OWNER_PWD
    {
        Pos = MAP_APPFONT( 12 , 65 );
        Size = MAP_APPFONT( 160, 20 );
    };

    String  STR_OWNER_PWD_SET
    {
        Text [ en-US ] = "Permission password set";
    };

    String  STR_OWNER_PWD_REST
    {
        Text [ en-US ] = "PDF document will be restricted";
    };

    String  STR_OWNER_PWD_UNSET
    {
        Text [ en-US ] = "No permission password set";
    };

    String  STR_OWNER_PWD_UNREST
    {
        Text [ en-US ] = "PDF document will be unrestricted";
    };

//////////////////////////////
    FixedLine FL_PRINT_PERMISSIONS
    {
        Pos = MAP_APPFONT ( 12 , 90 ) ;
        Size = MAP_APPFONT (156  , 8 ) ;
        Text[ en-US ] = "Printing" ;
    };
    RadioButton RB_PRINT_NONE
    {
        Pos = MAP_APPFONT ( 18 , 101 ) ;
        Size = MAP_APPFONT ( 150 , 10 ) ;
        Text[ en-US ] = "~Not permitted";
    };
    RadioButton RB_PRINT_LOWRES
    {
        Pos = MAP_APPFONT ( 18 , 112 ) ;
        Size = MAP_APPFONT ( 150 , 10 ) ;
        Text[ en-US ] = "~Low resolution (150 dpi)";
    };
    RadioButton RB_PRINT_HIGHRES
    {
        Pos = MAP_APPFONT ( 18 , 123 ) ;
        Size = MAP_APPFONT ( 150 , 10 ) ;
        Text[ en-US ] = "~High resolution";
    };

/////////////////////////////
    FixedLine FL_CHANGES_ALLOWED
    {
        Pos = MAP_APPFONT ( 12 , 134 ) ;
        Size = MAP_APPFONT (156  , 8 ) ;
        Text[ en-US ] = "Changes" ;
    };
    RadioButton RB_CHANGES_NONE
    {
        Pos = MAP_APPFONT ( 18 , 145 ) ;
        Size = MAP_APPFONT ( 150 , 10 ) ;
        Text[ en-US ] = "No~t permitted";
    };
    RadioButton RB_CHANGES_INSDEL
    {
        Pos = MAP_APPFONT ( 18 , 157 ) ;
        Size = MAP_APPFONT ( 150 , 10 ) ;
        Text[ en-US ] = "~Inserting, deleting, and rotating pages";
    };
    RadioButton RB_CHANGES_FILLFORM
    {
        Pos = MAP_APPFONT ( 18 , 168 ) ;
        Size = MAP_APPFONT ( 150 , 10 ) ;
        Text[ en-US ] = "~Filling in form fields";
    };
    RadioButton RB_CHANGES_COMMENT
    {
        Pos = MAP_APPFONT ( 18 , 179 ) ;
        Size = MAP_APPFONT ( 152 , 16 ) ;
        WordBreak = TRUE ;
        Text[ en-US ] = "~Commenting, filling in form fields";
    };
    RadioButton RB_CHANGES_ANY_NOCOPY
    {
        Pos = MAP_APPFONT ( 18 , 198 ) ;
        Size = MAP_APPFONT ( 152 , 10 ) ;
        Text[ en-US ] = "~Any except extracting pages";
    };

    CheckBox CB_ENDAB_COPY
    {
        Pos = MAP_APPFONT ( 12 , 211 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Ena~ble copying of content" ;
    };

    CheckBox CB_ENAB_ACCESS
    {
        Pos = MAP_APPFONT ( 12 , 224 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Enable text access for acce~ssibility tools" ;
    };
};

//----------------------------------------------------------
//tab page for PDF Export, links management
TabPage  RID_PDF_TAB_LINKS
{
    HelpId = HID_FILTER_PDF_LINKS;
    Text [ en-US ] = "---";
    TAB_PDF_SIZE;
    Hide = TRUE;

    CheckBox CB_EXP_BMRK_TO_DEST
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 164 , 16 ) ;
        TabStop = TRUE ;
        WordBreak = TRUE ;
        Text[ en-US ] = "Export bookmarks as named destinations" ;
    };

    CheckBox CB_CNV_OOO_DOCTOPDF
    {
        Pos = MAP_APPFONT ( 6 , 22 ) ;
        Size = MAP_APPFONT ( 164 , 16 ) ;
        TabStop = TRUE ;
        WordBreak = TRUE ;
        Text[ en-US ] = "Convert document references to PDF targets" ;
    };

    CheckBox CB_ENAB_RELLINKFSYS
    {
        Pos = MAP_APPFONT ( 6 , 41 ) ;
        Size = MAP_APPFONT ( 164 , 16 ) ;
        TabStop = TRUE ;
        WordBreak = TRUE ;
        Text[ en-US ] = "Export URLs relative to file system" ;
    };

    FixedLine FL_DEFAULT_LINK_ACTION
    {
        Pos = MAP_APPFONT ( 6 , 60 ) ;
        Size = MAP_APPFONT ( 164  , 8 ) ;
        Text[ en-US ] = "Cross-document links" ;
    };

    RadioButton CB_VIEW_PDF_DEFAULT
    {
        Pos = MAP_APPFONT ( 12 , 71 ) ;
        Size = MAP_APPFONT ( 158 , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Default mode" ;
    };

    RadioButton CB_VIEW_PDF_APPLICATION
    {
        Pos = MAP_APPFONT ( 12 , 84 ) ;
        Size = MAP_APPFONT ( 158  , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Open with PDF reader application" ;
    };

    RadioButton CB_VIEW_PDF_BROWSER
    {
        Pos = MAP_APPFONT ( 12 , 97 ) ;
        Size = MAP_APPFONT ( 158  , 10 ) ;
        TabStop = TRUE ;
        Text[ en-US ] = "Open with Internet browser" ;
    };
};

//----------------------------------------------------------
TabDialog  RID_PDF_EXPORT_DLG
{
    HelpId = HID_FILTER_PDF_OPTIONS ;
    OutputSize = TRUE;
    SVLook = TRUE;
    Moveable = TRUE;
    Text [ en-US ] = "PDF Options";

    TabControl 1
    {
        HelpId = HID_FILTER_PDF_OPTIONS ;
        OutputSize = TRUE;
        PageList =
        {
            PageItem
            {
                Identifier = RID_PDF_TAB_GENER;
                Text [ en-US ] = "General";
            };
            PageItem
            {
                Identifier = RID_PDF_TAB_OPNFTR;
                Text [ en-US ] = "Initial View";
            };
            PageItem
            {
                Identifier = RID_PDF_TAB_VPREFER;
                Text [ en-US ] = "User Interface";
            };
            PageItem
            {
                Identifier = RID_PDF_TAB_LINKS;
                Text [ en-US ] = "Links";
            };
            PageItem
            {
                Identifier = RID_PDF_TAB_SECURITY;
                Text [ en-US ] = "Security";
            };
        };
    };
};

ModalDialog RID_PDF_ERROR_DLG
{
    OutputSize = TRUE;
    SVLook = TRUE;
    Moveable = TRUE;
    Text [en-US] = "Problems during PDF export";
    Size = MAP_APPFONT( 200, 150 );

    FixedText FT_PROCESS
    {
        WordBreak = TRUE;
        Pos = MAP_APPFONT( 5, 5 );
        Size = MAP_APPFONT( 210, 24 );
        Text [en-US] = "During PDF export the following problems occurred:";
    };

    Bitmap IMG_WARN
    {
        File = "ballgreen_7.png";
    };
    Bitmap IMG_ERR
    {
        File = "ballred_7.png";
    };

    String STR_WARN_TRANSP_PDFA_SHORT
    {
        Text [en-US] = "PDF/A transparency";
    };
    String STR_WARN_TRANSP_PDFA
    {
        Text [en-US] = "PDF/A forbids transparency. A transparent object was painted opaque instead.";
    };
    String STR_WARN_TRANSP_VERSION_SHORT
    {
        Text [en-US] = "PDF version conflict";
    };
    String STR_WARN_TRANSP_VERSION
    {
        Text [en-US] = "Transparency is not supported in PDF versions earlier than PDF 1.4. A transparent object was painted opaque instead";
    };
    String STR_WARN_FORMACTION_PDFA_SHORT
    {
        Text [en-US] = "PDF/A form action";
    };
    String STR_WARN_FORMACTION_PDFA
    {
        Text [en-US] = "A form control contained an action not supported by the PDF/A standard. The action was skipped";
    };
    String STR_WARN_TRANSP_CONVERTED
    {
        Text [en-US] = "Some objects were converted to an image in order to remove transparencies, because the target PDF format does not support transparencies. Possibly better results can be achieved if you remove the transparent objects before exporting.";
    };
    String STR_WARN_TRANSP_CONVERTED_SHORT
    {
        Text [en-US] = "Transparencies removed";
    };
};
