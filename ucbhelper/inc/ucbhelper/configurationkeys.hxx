/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: configurationkeys.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _UCBHELPER_CONFIGURATIONKEYS_HXX_
#define _UCBHELPER_CONFIGURATIONKEYS_HXX_

//============================================================================
/** Various primary and secondary keys under which UCB configurations can be
    accessed in the configuration database.
 */
#define UCB_CONFIGURATION_KEY1_LOCAL "Local"
#define UCB_CONFIGURATION_KEY1_SERVER "Server"
#define UCB_CONFIGURATION_KEY2_UNIVERSAL_CONTENT_BROKER \
    "UniversalContentBroker"
#define UCB_CONFIGURATION_KEY2_OFFICE "Office"
#define UCB_CONFIGURATION_KEY2_WEB_SERVER "WebServer"
#define UCB_CONFIGURATION_KEY2_CLIENT_ACCESS "ClientAccess"

#endif // _UCBHELPER_CONFIGURATIONKEYS_HXX_
