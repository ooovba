/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: toolbox.src,v $
 * $Revision: 1.24 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "app.hrc"
#include "cfgids.hxx"
#include "helpids.h"
#include <svx/svxids.hrc>
 // Werkzeugleiste (Impress)
 // Objektleiste (Impress)
 // Optionsleiste (Impress)
#include "toolbox2_tmpl.src"
 // Werkzeugleiste (Draw (Graphic))
 // Objektleiste (Draw (Graphic))
 // Optionsleiste (Draw (Graphic))
#undef SD_TOOLBOX
#define SD_TOOLBOX RID_GRAPHIC_TOOLBOX
#include "toolbox2_tmpl.src"

ToolBox RID_SLIDE_TOOLBOX
{
    HelpId = HID_SD_SLIDE_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_LEFT ;
    ItemList =
    {
        ToolBoxItem
        {
            Checkable = TRUE ;
            Identifier = SID_OBJECT_SELECT ;
            HelpID = SID_OBJECT_SELECT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_ZOOM_TOOLBOX ;
            HelpID = SID_ZOOM_TOOLBOX ;
            DropDown = TRUE ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_PRESENTATION ;
            HelpID = SID_PRESENTATION ;
        };
        TBI_ZOOM_IN
        TBI_ZOOM_OUT
    };
};
ToolBox RID_OUTLINE_TOOLBOX
{
    HelpId = HID_SD_OUTLINE_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_LEFT ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_ZOOM_TOOLBOX ;
            HelpID = SID_ZOOM_TOOLBOX ;
            DropDown = TRUE ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_OUTLINE_COLLAPSE_ALL ;
            HelpID = SID_OUTLINE_COLLAPSE_ALL ;
        };
        ToolBoxItem
        {
            Identifier = SID_OUTLINE_EXPAND_ALL ;
            HelpID = SID_OUTLINE_EXPAND_ALL ;
        };
        ToolBoxItem
        {
            Identifier = SID_OUTLINE_COLLAPSE ;
            HelpID = SID_OUTLINE_COLLAPSE ;
        };
        ToolBoxItem
        {
            Identifier = SID_OUTLINE_EXPAND ;
            HelpID = SID_OUTLINE_EXPAND ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_OUTLINE_FORMAT ;
            HelpID = SID_OUTLINE_FORMAT ;
        };
        ToolBoxItem
        {
            Identifier = SID_COLORVIEW ;
            HelpID = SID_COLORVIEW ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_PRESENTATION ;
            HelpID = SID_PRESENTATION ;
        };
        TBI_ZOOM_IN
        TBI_ZOOM_OUT
    };
};
ToolBox RID_DRAW_COMMONTASK_TOOLBOX
{
    HelpId = HID_SD_DRAW_COMMONTASK_TOOLBOX ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_INSERTPAGE ;
            HelpID = SID_INSERTPAGE ;
        };
        ToolBoxItem
        {
            Identifier = SID_MODIFYPAGE ;
            HelpID = SID_MODIFYPAGE ;
        };
        ToolBoxItem
        {
            Identifier = SID_PRESENTATION_LAYOUT ;
            HelpID = SID_PRESENTATION_LAYOUT ;
        };
        ToolBoxItem
        {
            Identifier = SID_DUPLICATE_PAGE ;
            HelpID = SID_DUPLICATE_PAGE ;
        };
        ToolBoxItem
        {
            Identifier = SID_EXPAND_PAGE ;
            HelpID = SID_EXPAND_PAGE ;
        };
    };
};
ToolBox RID_BEZIER_TOOLBOX
{
    HelpId = HID_SD_BEZIER_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_TOP ;
    ItemList =
    {
        ToolBoxItem
        {
            ITEM_FORMAT_BEZIER_EDIT
            AutoCheck = TRUE ;
            Checkable = TRUE ;
            RadioCheck = TRUE ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_MOVE ;
            HelpID = SID_BEZIER_MOVE ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_INSERT ;
            HelpID = SID_BEZIER_INSERT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_DELETE ;
            HelpID = SID_BEZIER_DELETE ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_CUTLINE ;
            HelpID = SID_BEZIER_CUTLINE ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_CONVERT ;
            HelpID = SID_BEZIER_CONVERT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_EDGE ;
            HelpID = SID_BEZIER_EDGE ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_SMOOTH ;
            HelpID = SID_BEZIER_SMOOTH ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_SYMMTR ;
            HelpID = SID_BEZIER_SYMMTR ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_CLOSE ;
            HelpID = SID_BEZIER_CLOSE ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_BEZIER_ELIMINATE_POINTS ;
            HelpID = SID_BEZIER_ELIMINATE_POINTS ;
        };
    };
};
ToolBox RID_GLUEPOINTS_TOOLBOX
{
    HelpId = HID_SD_GLUEPOINTS_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_TOP ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_GLUE_INSERT_POINT ;
            HelpID = SID_GLUE_INSERT_POINT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_ESCDIR_LEFT ;
            HelpID = SID_GLUE_ESCDIR_LEFT ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_ESCDIR_TOP ;
            HelpID = SID_GLUE_ESCDIR_TOP ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_ESCDIR_RIGHT ;
            HelpID = SID_GLUE_ESCDIR_RIGHT ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_ESCDIR_BOTTOM ;
            HelpID = SID_GLUE_ESCDIR_BOTTOM ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_PERCENT ;
            HelpID = SID_GLUE_PERCENT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_HORZALIGN_LEFT ;
            HelpID = SID_GLUE_HORZALIGN_LEFT ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_HORZALIGN_CENTER ;
            HelpID = SID_GLUE_HORZALIGN_CENTER ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_HORZALIGN_RIGHT ;
            HelpID = SID_GLUE_HORZALIGN_RIGHT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_VERTALIGN_TOP ;
            HelpID = SID_GLUE_VERTALIGN_TOP ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_VERTALIGN_CENTER ;
            HelpID = SID_GLUE_VERTALIGN_CENTER ;
        };
        ToolBoxItem
        {
            Identifier = SID_GLUE_VERTALIGN_BOTTOM ;
            HelpID = SID_GLUE_VERTALIGN_BOTTOM ;
        };
    };
};
ToolBox RID_SLIDE_OBJ_TOOLBOX
{
    HelpId = HID_SD_SLIDE_OBJ_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_TOP ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_REHEARSE_TIMINGS ;
            HelpID = SID_REHEARSE_TIMINGS ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_HIDE_SLIDE ;
            HelpID = SID_HIDE_SLIDE ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_PAGES_PER_ROW ;
            HelpID = SID_PAGES_PER_ROW ;
        };
    };
};
ToolBox RID_DRAW_GRAF_TOOLBOX
{
    HelpId = HID_SD_DRAW_GRAF_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_TOP ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_GRFFILTER;
            HelpID = SID_GRFFILTER;
            DropDown = TRUE ;
            Checkable = FALSE ;
            RadioCheck = FALSE ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_MODE;
            HelpID = SID_ATTR_GRAF_MODE;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_RED;
            HelpID = SID_ATTR_GRAF_RED;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_GREEN;
            HelpID = SID_ATTR_GRAF_GREEN;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_BLUE;
            HelpID = SID_ATTR_GRAF_BLUE;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_LUMINANCE;
            HelpID = SID_ATTR_GRAF_BLUE;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_CONTRAST;
            HelpID = SID_ATTR_GRAF_BLUE;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_GAMMA;
            HelpID = SID_ATTR_GRAF_BLUE;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_TRANSPARENCE;
            HelpID = SID_ATTR_GRAF_BLUE;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        // #i25616#
        ToolBoxItem
        {
            Identifier = SID_ATTRIBUTES_LINE;
            HelpID = SID_ATTRIBUTES_LINE;
        };
        ToolBoxItem
        {
            Identifier = SID_ATTRIBUTES_AREA;
            HelpID = SID_ATTRIBUTES_AREA;
        };
        ToolBoxItem
        {
            Identifier = SID_ATTR_FILL_SHADOW;
            HelpID = SID_ATTR_FILL_SHADOW;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };

        ToolBoxItem
        {
            Identifier = SID_ATTR_GRAF_CROP;
            HelpID = SID_ATTR_GRAF_CROP;
        };
    };
};

String RID_DRAW_VIEWER_TOOLBOX
{
    Text [ en-US ] = "Function Bar (viewing mode)" ;
};

ToolBox RID_DRAW_VIEWER_TOOLBOX
{
    Border = TRUE ;
    SVLook = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    HideWhenDeactivate = TRUE ;
    LineSpacing = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Hide = TRUE ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_SAVEASDOC ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_EDITDOC ;
        };
        ToolBoxItem
        {
            Identifier = SID_MAIL_SENDDOC ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_DIRECTEXPORTDOCASPDF ;
        };
        ToolBoxItem
        {
            Identifier = SID_PRINTDOCDIRECT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_COPY ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_SEARCH_DLG ;
        };
        ToolBoxItem
        {
            Identifier = SID_NAVIGATOR ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_ATTR_ZOOM ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_PRESENTATION;
        };
    };
    Scroll = TRUE ;
};

String RID_GRAPHIC_VIEWER_TOOLBOX
{
    Text [ en-US ] = "Function Bar (viewing mode)" ;
};

ToolBox RID_GRAPHIC_VIEWER_TOOLBOX
{
    Border = TRUE ;
    SVLook = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    HideWhenDeactivate = TRUE ;
    LineSpacing = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Hide = TRUE ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_SAVEASDOC ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_EDITDOC ;
        };
        ToolBoxItem
        {
            Identifier = SID_MAIL_SENDDOC ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_DIRECTEXPORTDOCASPDF ;
        };
        ToolBoxItem
        {
            Identifier = SID_PRINTDOCDIRECT ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_COPY ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_SEARCH_DLG ;
        };
        ToolBoxItem
        {
            Identifier = SID_NAVIGATOR ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
        ToolBoxItem
        {
            Identifier = SID_ATTR_ZOOM ;
        };
        ToolBoxItem
        {
            Type = TOOLBOXITEM_SEPARATOR ;
        };
    };
    Scroll = TRUE ;
};

ToolBox RID_DRAW_MEDIA_TOOLBOX
{
    HelpId = HID_SD_DRAW_MEDIA_TOOLBOX ;
    LineSpacing = TRUE ;
    Dockable = TRUE ;
    Moveable = TRUE ;
    Sizeable = TRUE ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Scroll = TRUE ;
    HideWhenDeactivate = TRUE ;
    Border = TRUE ;
    SVLook = TRUE ;
    Customize = TRUE ;
    MenuStrings = TRUE ;
    Size = MAP_APPFONT ( 0 , 0 ) ;
    Align = BOXALIGN_TOP ;
    ItemList =
    {
        ToolBoxItem
        {
            Identifier = SID_AVMEDIA_TOOLBOX;
            HelpID = SID_AVMEDIA_TOOLBOX;
        };
    };
};
String RID_DRAW_MEDIA_TOOLBOX
{
    Text [ en-US ] = "Media Playback" ;
};
String RID_DRAW_TABLE_TOOLBOX
{
    TEXT [ de ] = "Tabelle" ;
    Text [ en-US ] = "Table" ;
};
