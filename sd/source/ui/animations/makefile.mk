#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.7 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..$/..

PROJECTPCH=sd
PROJECTPCHSOURCE=$(PRJ)$/util$/sd
PRJNAME=sd
TARGET=animui
ENABLE_EXCEPTIONS=TRUE

# --- Settings -----------------------------------------------------

.INCLUDE :  settings.mk
.INCLUDE :  $(PRJ)$/util$/makefile.pmk

# --- Files --------------------------------------------------------

SRS1NAME=$(TARGET)
SRC1FILES =\
    AnimationSchemesPane.src\
    CustomAnimationPane.src\
    CustomAnimationDialog.src\
    CustomAnimationCreateDialog.src\
    SlideTransitionPane.src\
    CustomAnimationSchemesPane.src\
    CustomAnimation.src

SLOFILES =  \
        $(SLO)$/AnimationSchemesPane.obj \
        $(SLO)$/CustomAnimationCreateDialog.obj\
        $(SLO)$/CustomAnimationDialog.obj\
        $(SLO)$/CustomAnimationPane.obj \
        $(SLO)$/CustomAnimationList.obj \
        $(SLO)$/DialogListBox.obj \
        $(SLO)$/SlideTransitionPane.obj \
        $(SLO)$/STLPropertySet.obj \
        $(SLO)$/motionpathtag.obj

# --- Tagets -------------------------------------------------------

.INCLUDE :  target.mk


