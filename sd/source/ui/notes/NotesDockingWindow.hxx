/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NotesDockingWindow.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SD_TPV_BOTTOM_DOCKING_WINDOW_HXX_
#define SD_TPV_BOTTOM_DOCKING_WINDOW_HXX_

#include <sfx2/dockwin.hxx>

namespace sd { namespace notes {

class EditWindow;

class NotesDockingWindow
    : public SfxDockingWindow
{
public:
    NotesDockingWindow (
        SfxBindings *pBindings,
        SfxChildWindow *pChildWindow,
        Window* pParent);
    virtual ~NotesDockingWindow (void);

    virtual void Paint (const Rectangle& rBoundingBox);
    virtual void Resize (void);

private:
    EditWindow* mpEditWindow;
};

} } // end of namespace ::sd::notes

#endif
