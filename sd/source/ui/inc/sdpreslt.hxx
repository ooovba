/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: sdpreslt.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SD_PRES_LAYOUT_DLG_HXX
#define SD_PRES_LAYOUT_DLG_HXX

#include <vcl/dialog.hxx>
#ifndef _SV_BUTTON_HXX //autogen
#include <vcl/button.hxx>
#endif
#include <vcl/fixed.hxx>
#include <svtools/valueset.hxx>

class SfxItemSet;

namespace sd {
class DrawDocShell;
class ViewShell;
}



class SdPresLayoutDlg 
    : public ModalDialog
{
public:
    SdPresLayoutDlg(
        ::sd::DrawDocShell* pDocShell, 
        ::sd::ViewShell* pViewShell, 
        ::Window* pWindow,
        const SfxItemSet& rInAttrs);

    virtual ~SdPresLayoutDlg (void);

    void                GetAttr(SfxItemSet& rOutAttrs);

    DECL_LINK(ClickLayoutHdl, void *);
    DECL_LINK(ClickLoadHdl, void *);

private:
    ::sd::DrawDocShell* mpDocSh;
    ::sd::ViewShell*	mpViewSh;
    FixedText			maFtLayout;
    ValueSet            maVS;
    OKButton			maBtnOK;
    CancelButton		maBtnCancel;
    HelpButton			maBtnHelp;
    CheckBox			maCbxMasterPage;
    CheckBox			maCbxCheckMasters;
    PushButton          maBtnLoad;

    const SfxItemSet&	mrOutAttrs;

    List*               mpLayoutNames;

    String              maName;          // Layoutname oder Dateiname
    long				mnLayoutCount;	// Anzahl, der im Dokument vorhandenen MasterPages
    const String		maStrNone;

    void                FillValueSet();
    void				Reset();
};

#endif

