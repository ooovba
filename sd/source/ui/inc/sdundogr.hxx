/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: sdundogr.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SD_SDUNDOGR_HXX
#define _SD_SDUNDOGR_HXX

#include <tools/contnr.hxx>
#include "sdundo.hxx"
#include "sddllapi.h"

class SD_DLLPUBLIC SdUndoGroup : public SdUndoAction
{
    Container	   aCtn;
public:
    TYPEINFO();
                   SdUndoGroup(SdDrawDocument* pSdDrawDocument)
                              : SdUndoAction(pSdDrawDocument),
                                aCtn(16, 16, 16) {}
    virtual 	  ~SdUndoGroup();

    virtual BOOL   Merge( SfxUndoAction* pNextAction );

    virtual void   Undo();
    virtual void   Redo();

    void		   AddAction(SdUndoAction* pAction);
    SdUndoAction*  GetAction(ULONG nAction) const;
    ULONG		   Count() const { return aCtn.Count(); }

};

#endif	   // _SD_SDUNDOGR_HXX
