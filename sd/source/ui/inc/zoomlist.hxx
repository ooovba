/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: zoomlist.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SD_ZOOM_LIST_HXX
#define SD_ZOOM_LIST_HXX


#include <tools/gen.hxx>
#include <tools/list.hxx>


namespace sd {

class ViewShell;

class ZoomList : public List
{
public:
    ZoomList(ViewShell* pViewShell);
    virtual ~ZoomList();

    void		InsertZoomRect(const Rectangle& rRect);
    Rectangle	GetCurrentZoomRect() const;
    Rectangle	GetNextZoomRect();
    Rectangle	GetPreviousZoomRect();
    BOOL		IsNextPossible() const;
    BOOL		IsPreviousPossible() const;

private:
    ViewShell*	mpViewShell;
    ULONG		mnCurPos;
};

} // end of namespace sd

#endif


