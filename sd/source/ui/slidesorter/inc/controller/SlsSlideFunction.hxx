/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: SlsSlideFunction.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SD_SLIDESORTER_SLIDE_FUNCTION_HXX
#define SD_SLIDESORTER_SLIDE_FUNCTION_HXX

#include "fupoor.hxx"

class SdDrawDocument;

namespace sd { namespace slidesorter {
class SlideSorter;
} }


namespace sd { namespace slidesorter { namespace controller {

class SlideSorterController;


/** Base class for functions of the slide sorter.
*/
class SlideFunction
    : public FuPoor
{
public:
    TYPEINFO();

    static FunctionReference Create( SlideSorter& rSlideSorter, SfxRequest& rRequest );

    virtual BOOL MouseMove (const MouseEvent& rMEvt);
    virtual BOOL MouseButtonUp (const MouseEvent& rMEvt);
    virtual BOOL MouseButtonDown (const MouseEvent& rMEvt);

    /** Called from ForceScroll() before the actual scrolling.
    */
    virtual void ScrollStart (void);

    /** Called from ForceScroll() after the actual scrolling.
    */
    virtual void ScrollEnd (void);

protected:
    SlideFunction (
        SlideSorter& rSlideSorter,
        SfxRequest& rRequest);
};

} } } // end of namespace ::sd::slidesorter::controller

#endif
