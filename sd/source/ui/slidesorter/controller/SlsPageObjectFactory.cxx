/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: SlsPageObjectFactory.cxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_sd.hxx"

#include "controller/SlsPageObjectFactory.hxx"

#include "view/SlsPageObject.hxx"
#include "view/SlsPageObjectViewContact.hxx"
#include "view/SlsPageObjectViewObjectContact.hxx"

#include "sdpage.hxx"


namespace sd { namespace slidesorter { namespace controller {


PageObjectFactory::PageObjectFactory (
    const ::boost::shared_ptr<cache::PageCache>& rpCache,
    const ::boost::shared_ptr<controller::Properties>& rpProperties)
    : mpPageCache(rpCache),
      mpProperties(rpProperties)
{
}




PageObjectFactory::~PageObjectFactory (void)
{
}




view::PageObject* PageObjectFactory::CreatePageObject (
    SdPage* pPage,
    const model::SharedPageDescriptor& rpDescriptor) const
{
    return new view::PageObject(
        Rectangle (Point(0,0), pPage->GetSize()),
        pPage,
        rpDescriptor);
}




::sdr::contact::ViewContact* 
    PageObjectFactory::CreateViewContact (
        view::PageObject* pPageObject,
        const model::SharedPageDescriptor& rpDescriptor) const
{
    return new view::PageObjectViewContact (
        *pPageObject, 
        rpDescriptor);
}




::sdr::contact::ViewObjectContact* 
    PageObjectFactory::CreateViewObjectContact (
        ::sdr::contact::ObjectContact& rObjectContact,
        ::sdr::contact::ViewContact& rViewContact) const
{
    return new view::PageObjectViewObjectContact (
        rObjectContact, 
        rViewContact,
        mpPageCache,
        mpProperties);
}


} } } // end of namespace ::sd::slidesorter::controller
