/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: typemap.cxx,v $
 * $Revision: 1.15.70.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_sd.hxx"

#include "eetext.hxx"
#include <svx/eeitem.hxx>
#include <svx/langitem.hxx>
#include <svx/fhgtitem.hxx>
#include <svx/colritem.hxx>
#include <svx/cntritem.hxx>
#include <svx/shdditem.hxx>
#include <svx/crsditem.hxx>
#include <svx/udlnitem.hxx>
#include <svx/wghtitem.hxx>
#include <svx/postitem.hxx>
#include <svx/fontitem.hxx>
#include <svtools/poolitem.hxx>
#include <svx/tstpitem.hxx>
#include <svx/lrspitem.hxx>
#include <svx/protitem.hxx>
#include <svx/chrtitem.hxx>

#include <svtools/globalnameitem.hxx>
#include <svx/hlnkitem.hxx>
#include <svx/postattr.hxx>
#include <svx/editdata.hxx>
#include <svx/srchdlg.hxx>
#include <svx/rulritem.hxx>
#include <svx/clipfmtitem.hxx>
#include <sfx2/srchitem.hxx>
#include <svx/sizeitem.hxx>
#include <svx/svxenum.hxx>
#include <svx/algitem.hxx>
#include <svx/zoomitem.hxx>
#include <svx/pageitem.hxx>
#include <svx/svdattr.hxx>
#include <svx/grafctrl.hxx>
#include <svx/boxitem.hxx>
#include <svx/bolnitem.hxx>
#include "sdattr.hxx"
#include <svx/zoomslideritem.hxx>

#include <svx/xftstit.hxx>
#include <svx/xlnwtit.hxx>
#include <svx/xlineit0.hxx>
#include <svx/xlnclit.hxx>
#include <svx/xlndsit.hxx>
#include <svx/xflclit.hxx>
#include <svx/xfillit0.hxx>
#include <svx/xlnedit.hxx>
#include <svx/xlnstit.hxx>
#include <svx/xgrad.hxx>
#include <svx/xflgrit.hxx>
#include <svx/xflhtit.hxx>
#include <svx/xbtmpit.hxx>
#include <svx/xtextit0.hxx>
#include <svx/xftadit.hxx>
#include <svx/xftdiit.hxx>
#include <svx/xftmrit.hxx>
#include <svx/xftouit.hxx>
#include <svx/xftshit.hxx>
#include <svx/xftshcit.hxx>
#include <svx/xftshxy.hxx>
#include <svx/xftsfit.hxx>
#include <svx/xtextit0.hxx>
#include <avmedia/mediaitem.hxx>
#include <svx/drawitem.hxx>

// #UndoRedo#
#include <svtools/slstitm.hxx>

#include <svtools/lckbitem.hxx>


#define CharSetItem 				SfxUInt16Item
#define FontFamilyItem				SfxUInt16Item
#define FontPitchItem				SfxUInt16Item
#define FontAlignItem				SfxUInt16Item
#define FontWeightItem				SfxUInt16Item
#define FontUnderlineItem			SfxUInt16Item
#define FontStrikeoutItem			SfxUInt16Item
#define FontItalicItem				SfxUInt16Item
#define SvxDbTypeItem				SfxUInt16Item
#define SvxLineSpaceItem			SfxUInt16Item
#define SvxInterLineSpaceItem		SfxUInt16Item
#define SvxBreakItem				SfxUInt16Item
#define BrushStyleItem				SfxUInt16Item
#define SvxNumTypeItem				SfxUInt16Item
#define SvxShadowLocationItem		SfxUInt16Item
#define SvxDbTypeItem				SfxUInt16Item
//#define SvxChooseControlEnumItem	SfxUInt16Item
#define SvxDrawToolEnumItem 		SfxUInt16Item
#define SvxChooseControlItem		SfxEnumItem
#define SvxDrawToolItem 			SfxUInt16Item
#define SvxCellHorJustifyEnumItem	SfxUInt16Item
#define SvxCellVerJustifyEnumItem	SfxUInt16Item
#define SvxCellOrientationEnumItem	SfxUInt16Item
#define SvxLanguage 				SfxUInt16Item
//#define SfxLockBytesItem            SfxPoolItem
#define OfaStringListItem           SfxStringListItem
#define avmedia_MediaItem           ::avmedia::MediaItem
#include <sfx2/tplpitem.hxx>
#include <svtools/ptitem.hxx>
#include <svtools/rectitem.hxx>

#include <sfx2/frame.hxx>

#define SFX_TYPEMAP
#include "sdslots.hxx"

