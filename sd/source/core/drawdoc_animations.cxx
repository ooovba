/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: drawdoc_animations.cxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_sd.hxx"

#include "drawdoc.hxx"
#include "cusshow.hxx"

using namespace ::com::sun::star::uno;
using namespace ::com::sun::star::presentation;

/** replaces a slide from all custom shows with a new one or removes a slide from
    all custom shows if pNewPage is 0.
*/
void SdDrawDocument::ReplacePageInCustomShows( const SdPage* pOldPage, const SdPage* pNewPage )
{
    if ( mpCustomShowList )
    {
        for (ULONG i = 0; i < mpCustomShowList->Count(); i++)
        {
            SdCustomShow* pCustomShow = (SdCustomShow*) mpCustomShowList->GetObject(i);
            if( pNewPage == 0 )
                pCustomShow->RemovePage(pOldPage);
            else
                pCustomShow->ReplacePage(pOldPage,pNewPage);
        }
    }
}

extern Reference< XPresentation2 > CreatePresentation( const SdDrawDocument& rDocument );

const Reference< XPresentation2 >& SdDrawDocument::getPresentation() const
{
    if( !mxPresentation.is() )
    {
        const_cast< SdDrawDocument* >( this )->mxPresentation = CreatePresentation(*this);
    }
    return mxPresentation;
}
