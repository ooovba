PRJ=..
PRJNAME=sd

.INCLUDE :  settings.mk

.IF "$(ENABLE_OPENGL)" != "TRUE"
@all:
	@echo "Building without OpenGL Transitions..."
	$(RM) transitions-ogl.xml
.ELSE
@all:
	$(GNUCOPY) transitions-ogl transitions-ogl.xml
.ENDIF
