/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: sdiocmpt.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SD_SDIOCMPT_HXX
#define _SD_SDIOCMPT_HXX

#include <tools/stream.hxx>

//////////////////////////////////////////////////////////////////////////////
class SvStream;

class old_SdrDownCompat 
{
protected:
    SvStream&					rStream;
    UINT32						nSubRecSiz;
    UINT32						nSubRecPos;
    UINT16						nMode;
    BOOL						bOpen;

protected:
    void Read();
    void Write();

public:
    old_SdrDownCompat(SvStream& rNewStream, UINT16 nNewMode);
    ~old_SdrDownCompat();
    void  OpenSubRecord();
    void  CloseSubRecord();
};
//////////////////////////////////////////////////////////////////////////////
#include "sddllapi.h"

#define SDIOCOMPAT_VERSIONDONTKNOW (UINT16)0xffff

class SD_DLLPUBLIC SdIOCompat : public old_SdrDownCompat
{
private:
    UINT16 nVersion;

public:
                // nNewMode: STREAM_READ oder STREAM_WRITE
                // nVer:	 nur beim Schreiben angeben
            SdIOCompat(SvStream& rNewStream, USHORT nNewMode,
                       UINT16 nVer = SDIOCOMPAT_VERSIONDONTKNOW);
            ~SdIOCompat();
    UINT16	GetVersion() const { return nVersion; }
};

#endif		// _SD_SDIOCMPT_HXX


