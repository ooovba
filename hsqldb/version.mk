# when you want to change the HSQLDB version, you must update the d.lst
# in the HSQLDB_thon project accordingly !!!
HSQLDB_MAJOR=1
HSQLDB_MINOR=8
HSQLDB_MICRO=0
HSQLDB_VERSION=$(HSQLDB_MAJOR)_$(HSQLDB_MINOR)_$(HSQLDB_MICRO)
