/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: layerparser.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef CONFIGMGR_XML_LAYERPARSER_HXX
#define CONFIGMGR_XML_LAYERPARSER_HXX

#include "basicparser.hxx"

#include <com/sun/star/configuration/backend/XLayerHandler.hpp>

namespace configmgr
{
// -----------------------------------------------------------------------------
    namespace xml
    {
// -----------------------------------------------------------------------------
        namespace uno       = ::com::sun::star::uno;
        namespace lang      = ::com::sun::star::lang;

        namespace sax           = ::com::sun::star::xml::sax;
        namespace backenduno    = ::com::sun::star::configuration::backend;

// -----------------------------------------------------------------------------


        class LayerParser :	public BasicParser
        {
        public:
            LayerParser(uno::Reference< uno::XComponentContext > const & _xContext, uno::Reference< backenduno::XLayerHandler > const & _xHandler);
            virtual ~LayerParser();

        // XDocumentHandler
        public:
            virtual void SAL_CALL 
                startDocument(  ) 
                    throw (sax::SAXException, uno::RuntimeException);

            virtual void SAL_CALL 
                endDocument(  ) throw (sax::SAXException, uno::RuntimeException);

            virtual void SAL_CALL 
                startElement( const rtl::OUString& aName, const uno::Reference< sax::XAttributeList >& xAttribs )
                    throw (sax::SAXException, uno::RuntimeException);

            virtual void SAL_CALL 
                endElement( const rtl::OUString& aName ) 
                    throw (sax::SAXException, uno::RuntimeException);

        private:
            /// start an node
            void startNode( ElementInfo const & aInfo, const uno::Reference< sax::XAttributeList >& xAttribs );
           /// end a node
            void endNode();

            /// start a property
            void startProperty( ElementInfo const & aInfo, const uno::Reference< sax::XAttributeList >& xAttribs );
            /// end a property 
            void endProperty();

            /// start collecting data for a value - returns the locale of the value (property must have been started)
            void startValueData(const uno::Reference< sax::XAttributeList >& xAttribs);
            /// end collecting data for a value - returns the collected value
            void endValueData();

            /**
              Forces the addition or replacement of a property.
              As it is possible to "replace" an existing property,
              even though this amounts to a modify, this method
              first tries to add a new property and failing that,
              to replace the value of an existing one.

              @param aValue value to be set for the property
              */
            void addOrReplaceCurrentProperty(const uno::Any& aValue) ;

            bool isInRemoved() const { return m_bRemoved; }
            void checkNotRemoved();
        private:
            uno::Reference< backenduno::XLayerHandler >  m_xHandler; 
            bool        m_bRemoved;
            bool        m_bNewProp;
        };
// -----------------------------------------------------------------------------
    } // namespace xml
// -----------------------------------------------------------------------------

} // namespace configmgr
#endif 


    
    
