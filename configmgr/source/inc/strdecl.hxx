/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: strdecl.hxx,v $
 * $Revision: 1.15 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CONFIGMGR_STRDECL_HXX_
#define _CONFIGMGR_STRDECL_HXX_

#include "strings.hxx"

//.........................................................................
namespace configmgr
{
//.........................................................................

// extern declaration for predefined strings

    // simple types names
    DECLARE_CONSTASCII_USTRING(TYPE_BOOLEAN);
    DECLARE_CONSTASCII_USTRING(TYPE_SHORT);
    DECLARE_CONSTASCII_USTRING(TYPE_INT);	
    DECLARE_CONSTASCII_USTRING(TYPE_LONG);
    DECLARE_CONSTASCII_USTRING(TYPE_DOUBLE);
    DECLARE_CONSTASCII_USTRING(TYPE_STRING);
    // Type: Sequence<bytes>
    DECLARE_CONSTASCII_USTRING(TYPE_BINARY);
    // Universal type: Any
    DECLARE_CONSTASCII_USTRING(TYPE_ANY);

    // special template names for native/localized value types
    DECLARE_CONSTASCII_USTRING(TEMPLATE_MODULE_NATIVE_PREFIX);
    DECLARE_CONSTASCII_USTRING(TEMPLATE_MODULE_NATIVE_VALUE);
    DECLARE_CONSTASCII_USTRING(TEMPLATE_MODULE_LOCALIZED_VALUE);

    DECLARE_CONSTASCII_USTRING(TEMPLATE_LIST_SUFFIX);
    
} // namespace configmgr
#endif

