/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: backendstratalistener.cxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_configmgr.hxx"
#include "backendstratalistener.hxx"
namespace configmgr
{
// -----------------------------------------------------------------------------
    namespace backend
    {
    // -----------------------------------------------------------------------------
        BackendStrataListener::BackendStrataListener(const MultiStratumBackend& aBackend)
            :mBackend(aBackend)
        {}
        BackendStrataListener:: ~BackendStrataListener(){}

        void SAL_CALL BackendStrataListener::componentDataChanged(const backenduno::ComponentChangeEvent& aEvent)
        throw (::com::sun::star::uno::RuntimeException)
        {
            
            mBackend.notifyListeners(aEvent);
                
        }
        // -----------------------------------------------------------------------------
        void SAL_CALL BackendStrataListener::disposing( lang::EventObject const & /*rSource*/ )
        throw (uno::RuntimeException)
        {
        
        }
    }
}
