-o cfgmgr2.LocalSchemaSupplier
-o cfgmgr2.LocalSingleStratum
-o cfgmgr2.MultiStratumBackend
-o sysmgr1.SystemIntegration
#i46913# -o cfgmgr2.BootstrapContext
-o cfgmgr2.LayerUpdateMerger
-o cfgmgr2.LayerWriter
-o cfgmgr2.LocalDataImporter
-o cfgmgr2.LocalHierarchyBrowser
-o cfgmgr2.LocalSingleBackend
-o cfgmgr2.MergeImporter
-o cfgmgr2.OConfigurationRegistry
-o cfgmgr2.OInnerGroupInfoAccess
-o cfgmgr2.OInnerGroupUpdateAccess
-o cfgmgr2.OInnerSetInfoAccess
#i46185# -o cfgmgr2.OInnerTreeSetUpdateAccess
-o cfgmgr2.OInnerValueSetUpdateAccess
-o cfgmgr2.ORootElementGroupInfoAccess
-o cfgmgr2.ORootElementGroupUpdateAccess
-o cfgmgr2.ORootElementSetInfoAccess
#i46185# -o cfgmgr2.ORootElementTreeSetUpdateAccess
-o cfgmgr2.ORootElementValueSetUpdateAccess
-o cfgmgr2.OSetElementGroupInfoAccess
-o cfgmgr2.OSetElementGroupUpdateAccess
-o cfgmgr2.OSetElementSetInfoAccess
#i46185# -o cfgmgr2.OSetElementTreeSetUpdateAccess
#i84221# -o cfgmgr2.OSetElementValueSetUpdateAccess
-o cfgmgr2.SchemaParser
-o cfgmgr2.SingleBackendAdapter
-o cfgmgr2.ConfigurationProviderWrapper
#i87744# -o cfgmgr2.LayerParser
-o cfgmgr2.CopyImporter
-o cfgmgr2.AdministrationProvider
-o cfgmgr2.ConfigurationProvider
