/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cfgfile.cxx,v $
 * $Revision: 1.8 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_configmgr.hxx"


// -----------------------------------------------------------------------------
// ------------------------------------ main ------------------------------------
// -----------------------------------------------------------------------------

/*
  OUString operator+(const OUString &a, const OUString &b)
  {
  OUString c = a;
  c += b;
  return c;
  }
*/

namespace configmgr
{
    void simpleMappingTest();

    void importTest();
    void exportTest();

    void hierarchyTest();
    //void simpleTest();
    void speedTest();
    void stringTest();
    void classTest();
    void hash_test();
    void testRefs();
    void ConfigName();

    void oslTest();
}



#if (defined UNX) || (defined OS2)
int main( int argc, char * argv[] )
#else
    int _cdecl main( int argc, char * argv[] )
#endif
{
    
//	configmgr::hierarchyTest();


//	configmgr::importTest();
//	configmgr::exportTest();
//	configmgr::speedTest();
//	configmgr::simpleTest();

//	configmgr::simpleMappingTest();
//	configmgr::stringTest();
//	configmgr::classTest();

//	configmgr::hash_test();

    // configmgr::testRefs();
    // configmgr::ConfigName();

    configmgr::oslTest();
    return 0;
}


