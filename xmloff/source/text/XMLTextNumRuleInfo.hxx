/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XMLTextNumRuleInfo.hxx,v $
 * $Revision: 1.10.64.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_XMLTEXTNUMRULEINFO_HXX
#define _XMLOFF_XMLTEXTNUMRULEINFO_HXX

#include <com/sun/star/uno/Reference.hxx>
#include <com/sun/star/container/XIndexReplace.hpp>
#include <com/sun/star/style/NumberingType.hpp>

namespace com { namespace sun { namespace star {
    namespace text { class XTextContent; }
} } }
#include <sal/types.h>

class XMLTextListAutoStylePool;

/** information about list and list style for a certain paragraph

    OD 2008-04-24 #refactorlists#
    Complete refactoring of the class and enhancement of the class for lists.
    These changes are considered by method <XMLTextParagraphExport::exportListChange(..)>
*/
class XMLTextNumRuleInfo
{
    const ::rtl::OUString msNumberingRules;
    const ::rtl::OUString msNumberingLevel;
    const ::rtl::OUString msNumberingStartValue;
    const ::rtl::OUString msNumberingType;
    const ::rtl::OUString msParaIsNumberingRestart;
    const ::rtl::OUString msNumberingIsNumber;
    const ::rtl::OUString msNumberingIsOutline;
    const ::rtl::OUString msPropNameListId;
    const ::rtl::OUString msPropNameStartWith;
    // --> OD 2008-11-26 #158694#
    const ::rtl::OUString msContinueingPreviousSubTree;
    const ::rtl::OUString msListLabelStringProp;
    // <--

    // numbering rules instance and its name
    ::com::sun::star::uno::Reference <
                        ::com::sun::star::container::XIndexReplace > mxNumRules;
    ::rtl::OUString     msNumRulesName;

    // paragraph's list attributes
    ::rtl::OUString     msListId;
    sal_Int16           mnListStartValue;
    sal_Int16           mnNumberingType;
    sal_Int16           mnListLevel;
    sal_Bool            mbIsNumbered;
    sal_Bool            mbIsRestart;

    // numbering rules' attributes
    // --> OD 2008-05-07 #refactorlists#
    sal_Int16           mnListLevelStartValue;
    // <--

    // --> OD 2006-09-27 #i69627#
    sal_Bool mbOutlineStyleAsNormalListStyle;
    // <--

    // --> OD 2008-11-26 #158694#
    sal_Bool mbContinueingPreviousSubTree;
    ::rtl::OUString msListLabelString;
    // <--
public:

    XMLTextNumRuleInfo();

    inline XMLTextNumRuleInfo& operator=( const XMLTextNumRuleInfo& rInfo );

    // --> OD 2008-11-26 #158694#
    void Set( const ::com::sun::star::uno::Reference <
                        ::com::sun::star::text::XTextContent > & rTextContnt,
              const sal_Bool bOutlineStyleAsNormalListStyle,
              const XMLTextListAutoStylePool& rListAutoPool,
              const sal_Bool bExportTextNumberElement );
    // <--
    inline void Reset();

    inline const ::rtl::OUString& GetNumRulesName() const
    {
        return msNumRulesName;
    }
    inline const ::com::sun::star::uno::Reference <
        ::com::sun::star::container::XIndexReplace >& GetNumRules() const
    {
        return mxNumRules;
    }
    inline sal_Int16 GetListLevelStartValue() const
    {
        return mnListLevelStartValue;
    }

    inline const ::rtl::OUString& GetListId() const
    {
        return msListId;
    }

    inline sal_Int16 GetLevel() const
    {
        return mnListLevel;
    }

    inline sal_Bool HasStartValue() const
    {
        return mnListStartValue != -1;
    }
    inline sal_uInt32 GetStartValue() const
    {
        return mnListStartValue;
    }

    inline sal_Bool IsNumbered() const
    {
        return mbIsNumbered;
    }
    inline sal_Bool IsRestart() const
    {
        return mbIsRestart;
    }

    /** Compare num rules

        @param rCmp other numrule to compare with
        @param bIgnoreFormatting when true, different list styles still compare equal
     */
    sal_Bool BelongsToSameList( const XMLTextNumRuleInfo& rCmp, bool bIgnoreFormatting ) const;

    inline sal_Bool HasSameNumRules( const XMLTextNumRuleInfo& rCmp ) const
    {
        return rCmp.msNumRulesName == msNumRulesName;
    }

    // --> OD 2008-11-26 #158694#
    inline sal_Bool IsContinueingPreviousSubTree() const
    {
        return mbContinueingPreviousSubTree;
    }
    inline const ::rtl::OUString& ListLabelString() const
    {
        return msListLabelString;
    }
    // <--
};

inline XMLTextNumRuleInfo& XMLTextNumRuleInfo::operator=(
        const XMLTextNumRuleInfo& rInfo )
{
    msNumRulesName = rInfo.msNumRulesName;
    mxNumRules = rInfo.mxNumRules;
    msListId = rInfo.msListId;
    mnListStartValue = rInfo.mnListStartValue;
    mnNumberingType = rInfo.mnNumberingType;
    mnListLevel = rInfo.mnListLevel;
    mbIsNumbered = rInfo.mbIsNumbered;
    mbIsRestart = rInfo.mbIsRestart;
    // --> OD 2006-09-27 #i69627#
    mbOutlineStyleAsNormalListStyle = rInfo.mbOutlineStyleAsNormalListStyle;
    // <--
    // --> OD 2008-11-26 #158694#
    mbContinueingPreviousSubTree = rInfo.mbContinueingPreviousSubTree;
    msListLabelString = rInfo.msListLabelString;
    // <--

    return *this;
}

inline void XMLTextNumRuleInfo::Reset()
{
    mxNumRules = 0;
    msNumRulesName = ::rtl::OUString();
    msListId = ::rtl::OUString();
    mnListStartValue = -1;
    mnNumberingType = ::com::sun::star::style::NumberingType::CHAR_SPECIAL;
    mnListLevel = 0;
    // --> OD 2006-09-27 #i69627#
    mbIsNumbered = mbIsRestart =
    mbOutlineStyleAsNormalListStyle = sal_False;
    // <--
    // --> OD 2008-11-26 #158694#
    mbContinueingPreviousSubTree = sal_False;
    msListLabelString = ::rtl::OUString();
    // <--
}
#endif	//  _XMLOFF_XMLTEXTNUMRULEINFO_HXX
