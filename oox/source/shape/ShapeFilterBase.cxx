/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ShapeFilterBase.cxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "ShapeFilterBase.hxx"
#include "oox/drawingml/chart/chartconverter.hxx"

namespace oox {
namespace shape {

using namespace ::com::sun::star;

ShapeFilterBase::ShapeFilterBase
(const uno::Reference< ::com::sun::star::lang::XMultiServiceFactory >&
 rxFactory)
: XmlFilterBase(rxFactory)
, mxChartConv( new ::oox::drawingml::chart::ChartConverter )
{
}

ShapeFilterBase::~ShapeFilterBase()
{
}

const ::oox::drawingml::Theme* ShapeFilterBase::getCurrentTheme() const
{
    return 0;
}

::oox::vml::Drawing* ShapeFilterBase::getVmlDrawing()
{
    return 0;
}

const ::oox::drawingml::table::TableStyleListPtr ShapeFilterBase::getTableStyles()
{
    return ::oox::drawingml::table::TableStyleListPtr();
}

::oox::drawingml::chart::ChartConverter& ShapeFilterBase::getChartConverter()
{
    return *mxChartConv;
}

::rtl::OUString ShapeFilterBase::implGetImplementationName() const
{
    return ::rtl::OUString();
}

}
}
