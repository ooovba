/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: modelbase.cxx,v $
 *
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "oox/drawingml/chart/modelbase.hxx"
#include "oox/helper/attributelist.hxx"

using ::rtl::OUString;

namespace oox {
namespace drawingml {
namespace chart {

// ============================================================================

NumberFormat::NumberFormat() :
    mbSourceLinked( true )
{
}

void NumberFormat::setAttributes( const AttributeList& rAttribs )
{
    maFormatCode = rAttribs.getString( XML_formatCode, OUString() );
    // default is 'false', not 'true' as specified
    mbSourceLinked = rAttribs.getBool( XML_sourceLinked, false );
}

// ============================================================================

} // namespace chart
} // namespace drawingml
} // namespace oox

