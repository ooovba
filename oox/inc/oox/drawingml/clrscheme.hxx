/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: clrscheme.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef OOX_DRAWINGML_CLRSCHEME_HXX
#define OOX_DRAWINGML_CLRSCHEME_HXX

#include <boost/shared_ptr.hpp>
#include <map>
#include <vector>
#include "oox/core/namespaces.hxx"
#include "oox/drawingml/color.hxx"

namespace oox { namespace drawingml {

class ClrMap
{
    std::map < sal_Int32, sal_Int32 > maClrMap;

public:

    sal_Bool getColorMap( sal_Int32& nClrToken );
    void	 setColorMap( sal_Int32 nClrToken, sal_Int32 nMappedClrToken );
};

typedef boost::shared_ptr< ClrMap > ClrMapPtr;

class ClrScheme
{
    std::map < sal_Int32, sal_Int32 > maClrScheme;

public:

    ClrScheme();
    ~ClrScheme();

    sal_Bool getColor( sal_Int32 nSchemeClrToken, sal_Int32& rColor ) const;
    void	 setColor( sal_Int32 nSchemeClrToken, sal_Int32 nColor );
};

typedef boost::shared_ptr< ClrScheme > ClrSchemePtr;

} }

#endif  //  OOX_DRAWINGML_CLRSCHEME_HXX
