/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: recordinputstream.hxx,v $
 * $Revision: 1.3.22.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef OOX_HELPER_RECORDINPUTSTREAM_HXX
#define OOX_HELPER_RECORDINPUTSTREAM_HXX

#include "oox/helper/binaryinputstream.hxx"

namespace oox {

// ============================================================================

/** Reads the contents of a record from a binary OOBIN stream. */
class RecordInputStream : public SequenceInputStream
{
public:
    explicit            RecordInputStream( const StreamDataSequence& rData );

    /** Reads a string with leading 16-bit or 32-bit length field. */
    ::rtl::OUString     readString( bool b32BitLen = true );

    /** Stream operator for integral types. */
    template< typename Type >
    inline RecordInputStream& operator>>( Type& ornValue ) { readValue( ornValue ); return *this; }
    /** Stream operator for an ::rtl::OUString, reads 32-bit string length and Unicode array. */
    inline RecordInputStream& operator>>( ::rtl::OUString& orString ) { orString = readString(); return *this; }
};

// ============================================================================

} // namespace oox

#endif

