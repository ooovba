/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: macro_expander.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef CPPUHELPER_SOURCE_MACRO_EXPANDER_HXX
#define CPPUHELPER_SOURCE_MACRO_EXPANDER_HXX

#include "sal/config.h"

namespace rtl { class OUString; }

namespace cppuhelper {

namespace detail {

/**
 * Helper function to expand macros based on the unorc/uno.ini.
 *
 * @internal
 *
 * @param text
 * Some text.
 *
 * @return
 * The expanded text.
 *
 * @exception com::sun::star::lang::IllegalArgumentException
 * If uriReference is a vnd.sun.star.expand URL reference that contains unknown
 * macros.
 */
::rtl::OUString expandMacros(rtl::OUString const & text);

}

}

#endif
