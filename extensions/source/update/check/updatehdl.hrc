/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: updatehdl.hrc,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

 #define RID_UPDATE_HDL_START			   1200
 
 #define RID_UPDATE_STR_CHECKING            RID_UPDATE_HDL_START +  1
 #define RID_UPDATE_STR_NO_UPD_FOUND        RID_UPDATE_HDL_START +  2
 #define RID_UPDATE_STR_UPD_FOUND           RID_UPDATE_HDL_START +  3
 #define RID_UPDATE_STR_DLG_TITLE           RID_UPDATE_HDL_START +  4
 #define RID_UPDATE_STR_DOWNLOAD_ERR        RID_UPDATE_HDL_START +  5
 #define RID_UPDATE_STR_DOWNLOAD_WARN       RID_UPDATE_HDL_START +  6
 #define RID_UPDATE_STR_DOWNLOADING         RID_UPDATE_HDL_START +  7
 #define RID_UPDATE_STR_READY_INSTALL       RID_UPDATE_HDL_START +  8
 #define RID_UPDATE_STR_CANCEL_TITLE        RID_UPDATE_HDL_START +  9
 #define RID_UPDATE_STR_CANCEL_DOWNLOAD     RID_UPDATE_HDL_START + 10
 #define RID_UPDATE_STR_BEGIN_INSTALL       RID_UPDATE_HDL_START + 11
 #define RID_UPDATE_STR_INSTALL_NOW         RID_UPDATE_HDL_START + 12
 #define RID_UPDATE_STR_INSTALL_LATER       RID_UPDATE_HDL_START + 13
 #define RID_UPDATE_STR_CHECKING_ERR        RID_UPDATE_HDL_START + 14
 #define RID_UPDATE_STR_OVERWRITE_WARNING   RID_UPDATE_HDL_START + 15
 #define RID_UPDATE_STR_DOWNLOAD_PAUSE      RID_UPDATE_HDL_START + 16
 #define RID_UPDATE_STR_DOWNLOAD_UNAVAIL    RID_UPDATE_HDL_START + 17
 #define RID_UPDATE_STR_PERCENT             RID_UPDATE_HDL_START + 18
 #define RID_UPDATE_STR_DOWNLOAD_DESCR      RID_UPDATE_HDL_START + 19
 #define RID_UPDATE_STR_INSTALL_ERROR       RID_UPDATE_HDL_START + 20

 #define RID_UPDATE_FT_DESCRIPTION          RID_UPDATE_HDL_START + 25
 #define RID_UPDATE_FT_STATUS               RID_UPDATE_HDL_START + 26

 #define RID_UPDATE_BTN_CLOSE               RID_UPDATE_HDL_START + 30
 #define RID_UPDATE_BTN_DOWNLOAD            RID_UPDATE_HDL_START + 31
 #define RID_UPDATE_BTN_INSTALL             RID_UPDATE_HDL_START + 32
 #define RID_UPDATE_BTN_PAUSE               RID_UPDATE_HDL_START + 33
 #define RID_UPDATE_BTN_RESUME              RID_UPDATE_HDL_START + 34
 #define RID_UPDATE_BTN_CANCEL              RID_UPDATE_HDL_START + 35
 
 #define RID_UPDATE_BUBBLE_TEXT_START       RID_UPDATE_HDL_START + 40
 #define RID_UPDATE_BUBBLE_UPDATE_AVAIL     RID_UPDATE_BUBBLE_TEXT_START + 0
 #define RID_UPDATE_BUBBLE_UPDATE_NO_DOWN   RID_UPDATE_BUBBLE_TEXT_START + 1
 #define RID_UPDATE_BUBBLE_AUTO_START       RID_UPDATE_BUBBLE_TEXT_START + 2
 #define RID_UPDATE_BUBBLE_DOWNLOADING      RID_UPDATE_BUBBLE_TEXT_START + 3
 #define RID_UPDATE_BUBBLE_DOWNLOAD_PAUSED  RID_UPDATE_BUBBLE_TEXT_START + 4
 #define RID_UPDATE_BUBBLE_ERROR_DOWNLOADING    RID_UPDATE_BUBBLE_TEXT_START + 5
 #define RID_UPDATE_BUBBLE_DOWNLOAD_AVAIL   RID_UPDATE_BUBBLE_TEXT_START + 6
 #define RID_UPDATE_BUBBLE_EXT_UPD_AVAIL    RID_UPDATE_BUBBLE_TEXT_START + 7

 #define RID_UPDATE_BUBBLE_T_TEXT_START         RID_UPDATE_HDL_START + 50
 #define RID_UPDATE_BUBBLE_T_UPDATE_AVAIL       RID_UPDATE_BUBBLE_T_TEXT_START + 0
 #define RID_UPDATE_BUBBLE_T_UPDATE_NO_DOWN     RID_UPDATE_BUBBLE_T_TEXT_START + 1
 #define RID_UPDATE_BUBBLE_T_AUTO_START         RID_UPDATE_BUBBLE_T_TEXT_START + 2
 #define RID_UPDATE_BUBBLE_T_DOWNLOADING        RID_UPDATE_BUBBLE_T_TEXT_START + 3
 #define RID_UPDATE_BUBBLE_T_DOWNLOAD_PAUSED    RID_UPDATE_BUBBLE_T_TEXT_START + 4
 #define RID_UPDATE_BUBBLE_T_ERROR_DOWNLOADING  RID_UPDATE_BUBBLE_T_TEXT_START + 5
 #define RID_UPDATE_BUBBLE_T_DOWNLOAD_AVAIL     RID_UPDATE_BUBBLE_T_TEXT_START + 6
 #define RID_UPDATE_BUBBLE_T_EXT_UPD_AVAIL      RID_UPDATE_BUBBLE_T_TEXT_START + 7

