/*************************************************************************
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: logstorage.hxx,v $
 *
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 ************************************************************************/


#ifndef EXTENSIONS_OOOIMPROVEMENT_LOGSTORAGE_HXX
#define EXTENSIONS_OOOIMPROVEMENT_LOGSTORAGE_HXX

#include <com/sun/star/lang/XMultiServiceFactory.hpp>
#include <rtl/ustring.hxx>
#include <vector>


namespace oooimprovement
{
    #ifdef css
        #error css defined globally
    #endif
    #define css ::com::sun::star
    class LogStorage
    {
        public:
            LogStorage(const css::uno::Reference< css::lang::XMultiServiceFactory>& sf);
            void assureExists();
            void clear();
            const std::vector< ::rtl::OUString> getUnzippedLogFiles() const;
            const std::vector< ::rtl::OUString> getZippedLogFiles() const;
        private:
            const css::uno::Reference< css::lang::XMultiServiceFactory> m_ServiceFactory;
    };
    #undef css
}
#endif
