/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: chartpos.hxx,v $
 * $Revision: 1.3.32.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_CHARTPOS_HXX
#define SC_CHARTPOS_HXX

// -----------------------------------------------------------------------

#include "collect.hxx"
#include "rangelst.hxx"


class ScAddress;
class Table;

class ScChartPositionMap
{
    friend class ScChartPositioner;

            ScAddress**			ppData;
            ScAddress**			ppColHeader;
            ScAddress**			ppRowHeader;
            ULONG				nCount;
            SCCOL				nColCount;
            SCROW				nRowCount;

                                ScChartPositionMap( SCCOL nChartCols, SCROW nChartRows,
                                    SCCOL nColAdd,		// Header-Spalten
                                    SCROW nRowAdd,		// Header-Zeilen
                                    Table& rCols		// Table mit Col-Tables mit Address*
                                    );
                                ~ScChartPositionMap();	//! deletes all ScAddress*

                                // not implemented
                                ScChartPositionMap( const ScChartPositionMap& );
            ScChartPositionMap&	operator=( const ScChartPositionMap& );

public:

            ULONG				GetCount() const { return nCount; }
            SCCOL				GetColCount() const { return nColCount; }
            SCROW				GetRowCount() const { return nRowCount; }

            BOOL				IsValid( SCCOL nCol, SCROW nRow ) const
                                    { return nCol < nColCount && nRow < nRowCount; }
                                // Daten spaltenweise
            ULONG				GetIndex( SCCOL nCol, SCROW nRow ) const
                                    { return (ULONG) nCol * nRowCount + nRow; }

            const ScAddress*	GetPosition( ULONG nIndex ) const
                                    {
                                        if ( nIndex < nCount )
                                            return ppData[ nIndex ];
                                        return NULL;
                                    }

                                    //! kann NULL sein und damit "kein Wert"
            const ScAddress*	GetPosition( SCCOL nChartCol, SCROW nChartRow ) const
                                    {
                                        if ( IsValid( nChartCol, nChartRow ) )
                                            return ppData[ GetIndex( nChartCol, nChartRow ) ];
                                        return NULL;
                                    }
            const ScAddress*	GetColHeaderPosition( SCCOL nChartCol ) const
                                    {
                                        if ( nChartCol < nColCount )
                                            return ppColHeader[ nChartCol ];
                                        return NULL;
                                    }
            const ScAddress*	GetRowHeaderPosition( SCROW nChartRow ) const
                                    {
                                        if ( nChartRow < nRowCount )
                                            return ppRowHeader[ nChartRow ];
                                        return NULL;
                                    }
//UNUSED2009-05 ScRangeListRef		GetColRanges( SCCOL nChartCol ) const;
//UNUSED2009-05 ScRangeListRef		GetRowRanges( SCROW nChartRow ) const;
};


enum ScChartGlue {
    SC_CHARTGLUE_NA,
    SC_CHARTGLUE_NONE,      // alte Mimik
    SC_CHARTGLUE_COLS,		// alte Mimik
    SC_CHARTGLUE_ROWS,
    SC_CHARTGLUE_BOTH
};

class ScDocument;

class ScChartPositioner				// nur noch Parameter-Struct
{
    ScRangeListRef	aRangeListRef;
    ScDocument*	pDocument;
    ScChartPositionMap* pPositionMap;
    ScChartGlue	eGlue;
    SCCOL		nStartCol;
    SCROW		nStartRow;
    BOOL		bColHeaders;
    BOOL		bRowHeaders;
    BOOL		bDummyUpperLeft;

private:
    void		CheckColRowHeaders();

    void		GlueState();		// zusammengefasste Bereiche
    void		CreatePositionMap();

public:
    ScChartPositioner( ScDocument* pDoc, SCTAB nTab,
                    SCCOL nStartColP, SCROW nStartRowP,
                    SCCOL nEndColP, SCROW nEndRowP );
    ScChartPositioner( ScDocument* pDoc, const ScRangeListRef& rRangeList );
    ScChartPositioner( const ScChartPositioner& rPositioner );

    virtual	~ScChartPositioner();

    const ScRangeListRef&	GetRangeList() const { return aRangeListRef; }
    void	SetRangeList( const ScRangeListRef& rNew ) { aRangeListRef = rNew; }
    void	SetRangeList( const ScRange& rNew );

    void	SetHeaders(BOOL bCol, BOOL bRow) { bColHeaders=bCol; bRowHeaders=bRow; }
    BOOL	HasColHeaders() const			 { return bColHeaders; }
    BOOL	HasRowHeaders() const			 { return bRowHeaders; }
    void    SetDummyUpperLeft(BOOL bNew) { bDummyUpperLeft = bNew; }
    void    SeteGlue(ScChartGlue eNew) { eGlue = eNew; }
    void    SetStartCol(SCCOL nNew) { nStartCol = nNew; }
    void    SetStartRow(SCROW nNew) { nStartRow = nNew; }

    BOOL	operator==(const ScChartPositioner& rCmp) const;

    void					InvalidateGlue()
                                {
                                    eGlue = SC_CHARTGLUE_NA;
                                    if ( pPositionMap )
                                    {
                                        delete pPositionMap;
                                        pPositionMap = NULL;
                                    }
                                }
    const ScChartPositionMap*	GetPositionMap();
};


#endif

