/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: compressedarray.hxx,v $
 * $Revision: 1.7.32.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_SEGMENTTREE_HXX
#define SC_SEGMENTTREE_HXX

#include "address.hxx"

#include <memory>

class ScFlatBoolSegmentsImpl;

class ScFlatBoolRowSegments
{
public:
    struct RangeData
    {
        SCROW   mnRow1;
        SCROW   mnRow2;
        bool    mbValue;
    };
    ScFlatBoolRowSegments();
    ~ScFlatBoolRowSegments();

    void setTrue(SCROW nRow1, SCROW nRow2);
    void setFalse(SCROW nRow1, SCROW nRow2);
    bool getValue(SCROW nRow);
    bool getRangeData(SCROW nRow, RangeData& rData);
    void removeSegment(SCROW nRow1, SCROW nRow2);
    void insertSegment(SCROW nRow, SCROW nSize, bool bSkipStartBoundary);

private:
    ::std::auto_ptr<ScFlatBoolSegmentsImpl> mpImpl;
};

// ============================================================================

class ScFlatBoolColSegments
{
public:
    struct RangeData
    {
        SCCOL   mnCol1;
        SCCOL   mnCol2;
        bool    mbValue;
    };
    ScFlatBoolColSegments();
    ~ScFlatBoolColSegments();

    void setTrue(SCCOL nCol1, SCCOL nCol2);
    void setFalse(SCCOL nCol1, SCCOL nCol2);
    bool getValue(SCCOL nCol);
    bool getRangeData(SCCOL nCol, RangeData& rData);
    void removeSegment(SCCOL nCol1, SCCOL nCol2);
    void insertSegment(SCCOL nCol, SCCOL nSize, bool bSkipStartBoundary);

private:
    ::std::auto_ptr<ScFlatBoolSegmentsImpl> mpImpl;
};


#endif
