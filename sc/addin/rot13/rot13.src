/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: rot13.src,v $
 * $Revision: 1.23 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#include "sc.hrc" // Definition RID_XXX in StarCalc
#include "rot13.hrc"

/* #i54546# The code belonging to this resource file is sample code for the
 * legacy AddIn interface. The interface is still supported, but deprecated.
 * The strings here were displayed in the function wizard. To prevent
 * duplicated and useless translation effort (functions and strings are also
 * part of the new scaddin module), the strings here are now layed out as fixed
 * untranslatable strings. The entire mechanism with the ../util/cl2c.pl perl
 * script merging the raw .cl and the .src during build time didn't work
 * anymore anyway, since we switched from MS-LCIDs / telephone area codes to
 * ISO codes for resources, and introduced localize.sdf files. Returned was
 * always an empty string. Now at least the fixed English string is returned.
 * */

Resource RID_SC_ADDIN_ROT13
{
    String ROT13_DESC // Description
    {
        Text = "ROT13 Algorithm, each alphabetical character of the text is rotated by 13 in the alphabet";
    };

    String ROT13_PAR1_NAME // Name of Parameter 1
    {
        Text = "Text";
    };

    String ROT13_PAR1_DESC // Description of Parameter 1
    {
        Text = "The text that is to be rotated";
    };
};
