/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: prevwsh.sdi,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
interface TablePrintPreview : View
{
    //	von Basic aus nicht erreichbar, darum alles mit Export = FALSE

    SID_FORMATPAGE			[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_STATUS_PAGESTYLE	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_HFEDIT				[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_ATTR_ZOOM			[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    FID_SCALE				[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_STATUS_DOCPOS		[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_NEXT		[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_PREVIOUS	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_FIRST		[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_LAST		[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_MARGIN      [ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_SCALINGFACTOR   [ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_ATTR_ZOOMSLIDER     [ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]

    SfxVoidItem GoUpBlock SID_CURSORPAGEUP
    (
        SfxInt16Item By SID_CURSORPAGEUP
    )
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SfxVoidItem GoDownBlock SID_CURSORPAGEDOWN
    (
        SfxInt16Item By SID_CURSORPAGEDOWN
    )
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SID_CURSORHOME	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_CURSOREND	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]

    SfxVoidItem GoDown SID_CURSORDOWN
    (
        SfxInt16Item By SID_CURSORDOWN
    )
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]
    SfxVoidItem GoUp SID_CURSORUP
    (
        SfxInt16Item By SID_CURSORUP
    )
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]
    SfxVoidItem GoLeft SID_CURSORLEFT
    (
        SfxInt16Item By SID_CURSORLEFT
    )
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SfxVoidItem GoRight SID_CURSORRIGHT
    (
        SfxInt16Item By SID_CURSORRIGHT
    )
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SID_PREV_TABLE
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SID_NEXT_TABLE
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SfxVoidItem GoToStart SID_CURSORTOPOFFILE ()
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]

    SfxVoidItem GoToEndOfData SID_CURSORENDOFFILE ()
    [
        ExecMethod = Execute ;
        StateMethod = GetState ;
        Export = FALSE;
        GroupId = GID_INTERN ;
        Cachable ;
    ]
    SID_PREVIEW_ZOOMIN	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PREVIEW_ZOOMOUT	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_REPAINT			[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_UNDO			[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_REDO			[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_REPEAT			[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_PRINTPREVIEW	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ] // ole() api()
    SID_PREVIEW_CLOSE	[ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
    SID_CANCEL          [ ExecMethod = Execute; StateMethod = GetState; Export = FALSE; ]
}


 // ===========================================================================
shell ScPreviewShell : SfxViewShell
{
    import TablePrintPreview;
}
