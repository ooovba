/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: compressedarray.hxx,v $
 * $Revision: 1.7.32.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_sc.hxx"

#include "segmenttree.hxx"
#include "mdds/flatsegmenttree.hxx"

#define USE_TREE_SEARCH 1

class ScFlatBoolSegmentsImpl
{
public:
    struct RangeData
    {
        SCCOLROW    mnPos1;
        SCCOLROW    mnPos2;
        bool        mbValue;
    };
    ScFlatBoolSegmentsImpl(SCCOLROW nMax);
    ~ScFlatBoolSegmentsImpl();

    void setTrue(SCCOLROW nPos1, SCCOLROW nPos2);
    void setFalse(SCCOLROW nPos1, SCCOLROW nPos2);
    bool getValue(SCCOLROW nPos);
    bool getRangeData(SCCOLROW nPos, RangeData& rData);
    void removeSegment(SCCOLROW nPos1, SCCOLROW nPos2);
    void insertSegment(SCCOLROW nPos, SCCOLROW nSize, bool bSkipStartBoundary);

private:
    ScFlatBoolSegmentsImpl();
    ScFlatBoolSegmentsImpl(const ScFlatBoolSegmentsImpl&);

    ::mdds::flat_segment_tree<SCCOLROW, bool> maSegments;
};

ScFlatBoolSegmentsImpl::ScFlatBoolSegmentsImpl(SCCOLROW nMax) :
    maSegments(0, nMax+1, false)
{
}

ScFlatBoolSegmentsImpl::~ScFlatBoolSegmentsImpl()
{
}

void ScFlatBoolSegmentsImpl::setTrue(SCCOLROW nPos1, SCCOLROW nPos2)
{
    maSegments.insert_segment(nPos1, nPos2+1, true);
}

void ScFlatBoolSegmentsImpl::setFalse(SCCOLROW nPos1, SCCOLROW nPos2)
{
    maSegments.insert_segment(nPos1, nPos2+1, false);
}

bool ScFlatBoolSegmentsImpl::getValue(SCCOLROW nPos)
{
    bool bValue = false;
#if USE_TREE_SEARCH
    if (!maSegments.is_tree_valid())
        maSegments.build_tree();

    maSegments.search_tree(nPos, bValue);
#else
    maSegments.search(nPos, bValue);
#endif
    return bValue;
}

bool ScFlatBoolSegmentsImpl::getRangeData(SCCOLROW nPos, RangeData& rData)
{
#if USE_TREE_SEARCH
    if (!maSegments.is_tree_valid())
        maSegments.build_tree();
#endif

    bool bValue;
    SCCOLROW nPos1, nPos2;
#if USE_TREE_SEARCH
    if (!maSegments.search_tree(nPos, bValue, &nPos1, &nPos2))
        return false;
#else
    if (!maSegments.search(nPos, bValue, &nPos1, &nPos2))
        return false;
#endif

    rData.mnPos1 = nPos1;
    rData.mnPos2 = nPos2-1; // end point is not inclusive.
    rData.mbValue = bValue;
    return true;
}

void ScFlatBoolSegmentsImpl::removeSegment(SCCOLROW nPos1, SCCOLROW nPos2)
{
    maSegments.shift_segment_left(nPos1, nPos2);
}

void ScFlatBoolSegmentsImpl::insertSegment(SCCOLROW nPos, SCCOLROW nSize, bool bSkipStartBoundary)
{
    maSegments.shift_segment_right(nPos, nSize, bSkipStartBoundary);
}

// ============================================================================

ScFlatBoolRowSegments::ScFlatBoolRowSegments() :
    mpImpl(new ScFlatBoolSegmentsImpl(static_cast<SCCOLROW>(MAXROW)))
{
}

ScFlatBoolRowSegments::~ScFlatBoolRowSegments()
{
}

void ScFlatBoolRowSegments::setTrue(SCROW nRow1, SCROW nRow2)
{
    mpImpl->setTrue(static_cast<SCCOLROW>(nRow1), static_cast<SCCOLROW>(nRow2));
}

void ScFlatBoolRowSegments::setFalse(SCROW nRow1, SCROW nRow2)
{
    mpImpl->setFalse(static_cast<SCCOLROW>(nRow1), static_cast<SCCOLROW>(nRow2));
}

bool ScFlatBoolRowSegments::getValue(SCROW nRow)
{
    return mpImpl->getValue(static_cast<SCCOLROW>(nRow));
}

bool ScFlatBoolRowSegments::getRangeData(SCROW nRow, RangeData& rData)
{
    ScFlatBoolSegmentsImpl::RangeData aData;
    if (!mpImpl->getRangeData(static_cast<SCCOLROW>(nRow), aData))
        return false;

    rData.mbValue = aData.mbValue;
    rData.mnRow1  = static_cast<SCROW>(aData.mnPos1);
    rData.mnRow2  = static_cast<SCROW>(aData.mnPos2);
    return true;
}

void ScFlatBoolRowSegments::removeSegment(SCROW nRow1, SCROW nRow2)
{
    mpImpl->removeSegment(static_cast<SCCOLROW>(nRow1), static_cast<SCCOLROW>(nRow2));
}

void ScFlatBoolRowSegments::insertSegment(SCROW nRow, SCROW nSize, bool bSkipStartBoundary)
{
    mpImpl->insertSegment(static_cast<SCCOLROW>(nRow), static_cast<SCCOLROW>(nSize), bSkipStartBoundary);
}

// ============================================================================

ScFlatBoolColSegments::ScFlatBoolColSegments() :
    mpImpl(new ScFlatBoolSegmentsImpl(static_cast<SCCOLROW>(MAXCOL)))
{
}

ScFlatBoolColSegments::~ScFlatBoolColSegments()
{
}

void ScFlatBoolColSegments::setTrue(SCCOL nCol1, SCCOL nCol2)
{
    mpImpl->setTrue(static_cast<SCCOLROW>(nCol1), static_cast<SCCOLROW>(nCol2));
}

void ScFlatBoolColSegments::setFalse(SCCOL nCol1, SCCOL nCol2)
{
    mpImpl->setFalse(static_cast<SCCOLROW>(nCol1), static_cast<SCCOLROW>(nCol2));
}

bool ScFlatBoolColSegments::getValue(SCCOL nCol)
{
    return mpImpl->getValue(static_cast<SCCOLROW>(nCol));
}

bool ScFlatBoolColSegments::getRangeData(SCCOL nCol, RangeData& rData)
{
    ScFlatBoolSegmentsImpl::RangeData aData;
    if (!mpImpl->getRangeData(static_cast<SCCOLROW>(nCol), aData))
        return false;

    rData.mbValue = aData.mbValue;
    rData.mnCol1  = static_cast<SCCOL>(aData.mnPos1);
    rData.mnCol2  = static_cast<SCCOL>(aData.mnPos2);
    return true;
}

void ScFlatBoolColSegments::removeSegment(SCCOL nCol1, SCCOL nCol2)
{
    mpImpl->removeSegment(static_cast<SCCOLROW>(nCol1), static_cast<SCCOLROW>(nCol2));
}

void ScFlatBoolColSegments::insertSegment(SCCOL nCol, SCCOL nSize, bool bSkipStartBoundary)
{
    mpImpl->insertSegment(static_cast<SCCOLROW>(nCol), static_cast<SCCOLROW>(nSize), bSkipStartBoundary);
}
