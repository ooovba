/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ddelink.hxx,v $
 * $Revision: 1.11.32.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_DDELINK_HXX
#define SC_DDELINK_HXX

#include "address.hxx"
#include <sfx2/lnkbase.hxx>
#include <svtools/broadcast.hxx>
#include "scmatrix.hxx"

class ScDocument;
class ScMultipleReadHeader;
class ScMultipleWriteHeader;
class SvStream;

class ScDdeLink : public ::sfx2::SvBaseLink, public SvtBroadcaster
{
private:
static BOOL	bIsInUpdate;

    ScDocument*		pDoc;

    String			aAppl;			// Verbindungsdaten
    String			aTopic;
    String			aItem;
    BYTE			nMode;			// Zahlformat-Modus

    BOOL			bNeedUpdate;	// wird gesetzt, wenn Update nicht moeglich war

    ScMatrixRef		pResult;		// Ergebnis

public:
    TYPEINFO();

            ScDdeLink( ScDocument* pD,
                        const String& rA, const String& rT, const String& rI,
                        BYTE nM );
            ScDdeLink( ScDocument* pD, SvStream& rStream, ScMultipleReadHeader& rHdr );
            ScDdeLink( ScDocument* pD, const ScDdeLink& rOther );
    virtual ~ScDdeLink();

    void			Store( SvStream& rStream, ScMultipleWriteHeader& rHdr ) const;

                                            // von SvBaseLink ueberladen:
    virtual void	DataChanged( const String& rMimeType,
                                const ::com::sun::star::uno::Any & rValue );

                                            // von SvtBroadcaster ueberladen:
    virtual void	ListenersGone();

                                            // fuer Interpreter:

    const ScMatrix* GetResult() const           { return pResult; }
    void            SetResult( ScMatrix* pRes ) { pResult = pRes; }

                                            // XML and Excel import after NewData()
    ScMatrixRef     GetModifiableResult()   { return pResult; }

    const String&	GetAppl() const		{ return aAppl; }
    const String&	GetTopic() const	{ return aTopic; }
    const String&	GetItem() const		{ return aItem; }
    BYTE			GetMode() const		{ return nMode; }

    void			ResetValue();			// Wert zuruecksetzen
    void			TryUpdate();

    BOOL			NeedsUpdate() const { return bNeedUpdate; }

    static BOOL		IsInUpdate()		{ return bIsInUpdate; }
};


#endif

