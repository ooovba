/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: optutil.cxx,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_sc.hxx"



#include <vcl/svapp.hxx>

#include "optutil.hxx"
#include "global.hxx"       // for pSysLocale
#include <svtools/syslocale.hxx>

//------------------------------------------------------------------

// static
BOOL ScOptionsUtil::IsMetricSystem()
{
    //!	which language should be used here - system language or installed office language?

//	MeasurementSystem eSys = Application::GetAppInternational().GetMeasurementSystem();
    MeasurementSystem eSys = ScGlobal::pLocaleData->getMeasurementSystemEnum();

    return ( eSys == MEASURE_METRIC );
}

//------------------------------------------------------------------

ScLinkConfigItem::ScLinkConfigItem( const rtl::OUString& rSubTree ) :
    ConfigItem( rSubTree )
{
}

ScLinkConfigItem::ScLinkConfigItem( const rtl::OUString& rSubTree, sal_Int16 nMode ) :
    ConfigItem( rSubTree, nMode )
{
}

void ScLinkConfigItem::SetCommitLink( const Link& rLink )
{
    aCommitLink = rLink;
}

void ScLinkConfigItem::Notify( const com::sun::star::uno::Sequence<rtl::OUString>& /* aPropertyNames */ )
{
    //!	not implemented yet...
}

void ScLinkConfigItem::Commit()
{
    aCommitLink.Call( this );
}


