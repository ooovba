/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: inscodlg.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_INSCODLG_HXX
#define SC_INSCODLG_HXX

#include <vcl/dialog.hxx>
#include <vcl/imagebtn.hxx>
#include <vcl/fixed.hxx>
#include "global.hxx"

//------------------------------------------------------------------------
//CHINA001 #define INS_CONT_NOEMPTY		0x0100
//CHINA001 #define INS_CONT_TRANS		0x0200
//CHINA001 #define INS_CONT_LINK		0x0400
//CHINA001 
//CHINA001 #define SC_CELL_SHIFT_DISABLE_DOWN	0x01
//CHINA001 #define SC_CELL_SHIFT_DISABLE_RIGHT	0x02
#include "scui_def.hxx"

class ScInsertContentsDlg : public ModalDialog
{
public:
            ScInsertContentsDlg( Window*		pParent,
                                 USHORT  		nCheckDefaults = 0,
                                 const String*	pStrTitle = NULL );
            ~ScInsertContentsDlg();

    USHORT		GetInsContentsCmdBits() const;
    USHORT		GetFormulaCmdBits() const;
    BOOL		IsSkipEmptyCells() const {return aBtnSkipEmptyCells.IsChecked();}
    BOOL 		IsTranspose() const {return aBtnTranspose.IsChecked();}
    BOOL 		IsLink() const {return aBtnLink.IsChecked();}
    InsCellCmd	GetMoveMode();

    void	SetOtherDoc( BOOL bSet );
    void	SetFillMode( BOOL bSet );
    void	SetChangeTrack( BOOL bSet );
    void	SetCellShiftDisabled( int nDisable );

private:
    FixedLine       aFlFrame;
    CheckBox		aBtnInsAll;
    CheckBox		aBtnInsStrings;
    CheckBox		aBtnInsNumbers;
    CheckBox		aBtnInsDateTime;
    CheckBox		aBtnInsFormulas;
    CheckBox		aBtnInsNotes;
    CheckBox		aBtnInsAttrs;
    CheckBox		aBtnInsObjects;

    FixedLine       aFlSep1;
    FixedLine       aFlOptions;
    CheckBox		aBtnSkipEmptyCells;
    CheckBox		aBtnTranspose;
    CheckBox		aBtnLink;

    FixedLine       aFlOperation;
    RadioButton 	aRbNoOp;
    RadioButton 	aRbAdd;
    RadioButton 	aRbSub;
    RadioButton 	aRbMul;
    RadioButton 	aRbDiv;

    FixedLine       aFlSep2;
    FixedLine       aFlMove;
    RadioButton 	aRbMoveNone;
    RadioButton 	aRbMoveDown;
    RadioButton 	aRbMoveRight;

    OKButton		aBtnOk;
    CancelButton	aBtnCancel;
    HelpButton		aBtnHelp;

    BOOL			bOtherDoc;
    BOOL			bFillMode;
    BOOL			bChangeTrack;
    BOOL			bMoveDownDisabled;
    BOOL			bMoveRightDisabled;

    static BOOL		bPreviousAllCheck;
    static USHORT	nPreviousChecks;
    static USHORT	nPreviousChecks2;
    static USHORT	nPreviousFormulaChecks;
    static USHORT	nPreviousMoveMode;			// enum InsCellCmd

    void DisableChecks( BOOL bInsAllChecked = TRUE );
    void TestModes();

    // Handler
    DECL_LINK( InsAllHdl, void* );
    DECL_LINK( LinkBtnHdl, void* );
};


#endif // SC_INSCODLG_HXX


