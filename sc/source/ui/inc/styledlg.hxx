/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: styledlg.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_STYLEDLG_HXX
#define SC_STYLEDLG_HXX

#ifndef _SFX_HXX
#endif

#include <sfx2/styledlg.hxx>

//==================================================================

class SfxStyleSheetBase;

class ScStyleDlg : public SfxStyleDialog
{
public:
                ScStyleDlg( Window*				pParent,
                            SfxStyleSheetBase&	rStyleBase,
                            USHORT				nRscId );
                ~ScStyleDlg();

protected:
    virtual void				PageCreated( USHORT nPageId, SfxTabPage& rTabPage );
    virtual const SfxItemSet*	GetRefreshedSet();

private:
    USHORT nDlgRsc;
};


#endif // SC_STYLEDLG_HXX


