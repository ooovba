/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tpcalc.hxx,v $
 * $Revision: 1.9 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_TPFORMULA_HXX
#define SC_TPFORMULA_HXX

#include <sfx2/tabdlg.hxx>
#include <vcl/fixed.hxx>
#include <vcl/lstbox.hxx>
#include <vcl/edit.hxx>
#include <vcl/button.hxx>

#include <memory>

class ScDocOptions;
class SfxItemSet;
class Window;

class ScTpFormulaOptions : public SfxTabPage
{
public:
    static  SfxTabPage* Create (Window* pParent, const SfxItemSet& rCoreSet);

//  static  USHORT*     GetRanges();
    virtual BOOL FillItemSet(SfxItemSet& rCoreSet);
    virtual void Reset( const SfxItemSet& rCoreSet );
    virtual int DeactivatePage(SfxItemSet* pSet = NULL);

private:
    explicit ScTpFormulaOptions(Window* pParent, const SfxItemSet& rCoreSet);
    virtual ~ScTpFormulaOptions();

    void Init();
    void ResetSeparators();
    void OnFocusSeparatorInput(Edit* pEdit);

    bool IsValidSeparator(const ::rtl::OUString& rSep) const;
    bool IsValidSeparatorSet() const;

    DECL_LINK( ButtonHdl, PushButton* );
    DECL_LINK( SepModifyHdl, Edit* );
    DECL_LINK( SepEditOnFocusHdl, Edit* );

private:
    FixedLine maFlFormulaOpt;
    FixedText maFtFormulaSyntax;
    ListBox   maLbFormulaSyntax;

    FixedLine  maFlFormulaSeps;
    FixedText  maFtSepFuncArg;  
    Edit       maEdSepFuncArg;
    FixedText  maFtSepArrayCol;
    Edit       maEdSepArrayCol;
    FixedText  maFtSepArrayRow;
    Edit       maEdSepArrayRow;
    PushButton maBtnSepReset;

    ::std::auto_ptr<ScDocOptions> mpOldOptions;
    ::std::auto_ptr<ScDocOptions> mpNewOptions;

    /** Stores old separator value of currently focused separator edit box.
        This value is used to revert undesired value change. */
    ::rtl::OUString maOldSepValue;

    sal_Unicode mnDecSep;
};


#endif
