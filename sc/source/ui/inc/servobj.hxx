/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: servobj.hxx,v $
 * $Revision: 1.6.46.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_SERVOBJ_HXX
#define SC_SERVOBJ_HXX

#include <svtools/lstner.hxx>
#include <svtools/listener.hxx>
#include <sfx2/linksrc.hxx>
#include "global.hxx"
#include "address.hxx"

class ScDocShell;
class ScServerObject;

class ScServerObjectSvtListenerForwarder : public SvtListener
{
    ScServerObject* pObj;
    SfxBroadcaster  aBroadcaster;
public:
                    ScServerObjectSvtListenerForwarder( ScServerObject* pObjP);
    virtual         ~ScServerObjectSvtListenerForwarder();
    virtual void    Notify( SvtBroadcaster& rBC, const SfxHint& rHint);
};

class ScServerObject : public ::sfx2::SvLinkSource, public SfxListener
{
private:
    ScServerObjectSvtListenerForwarder  aForwarder;
    ScDocShell*		pDocSh;
    ScRange			aRange;
    String			aItemStr;
    BOOL			bRefreshListener;

    void	Clear();

public:
            ScServerObject( ScDocShell* pShell, const String& rItem );
    virtual ~ScServerObject();

    virtual BOOL GetData( ::com::sun::star::uno::Any & rData /*out param*/,
                             const String & rMimeType,
                             BOOL bSynchron = FALSE );

    virtual void Notify( SfxBroadcaster& rBC, const SfxHint& rHint );
            void    EndListeningAll();
};

//SO2_DECL_REF( ScServerObject )


#endif
