/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tabpages.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_TABPAGES_HXX
#define SC_TABPAGES_HXX

#ifndef _GROUP_HXX //autogen
#include <vcl/group.hxx>
#endif
#include <svtools/stdctrl.hxx>
#include <sfx2/tabdlg.hxx>

//========================================================================

class ScTabPageProtection : public SfxTabPage
{
public:
    static	SfxTabPage*	Create			( Window* 				pParent,
                                          const SfxItemSet&		rAttrSet );
    static	USHORT*		GetRanges		();
    virtual	BOOL		FillItemSet		( SfxItemSet& rCoreAttrs );
    virtual	void		Reset			( const SfxItemSet& );

protected:
    using SfxTabPage::DeactivatePage;
    virtual int 		DeactivatePage	( SfxItemSet* pSet = NULL );

private:
                ScTabPageProtection( Window* 			pParent,
                                     const SfxItemSet&	rCoreAttrs );
                ~ScTabPageProtection();

private:
    FixedLine   aFlProtect;
    TriStateBox	aBtnHideCell;
    TriStateBox	aBtnProtect;
    TriStateBox	aBtnHideFormula;
    FixedInfo	aTxtHint;

    FixedLine   aFlPrint;
    TriStateBox	aBtnHidePrint;
    FixedInfo	aTxtHint2;

                                    // aktueller Status:
    BOOL		bTriEnabled;		//	wenn vorher Dont-Care
    BOOL		bDontCare;			//	alles auf TriState
    BOOL		bProtect;			//	einzelne Einstellungen ueber TriState sichern
    BOOL		bHideForm;
    BOOL		bHideCell;
    BOOL		bHidePrint;

    // Handler:
    DECL_LINK( ButtonClickHdl, TriStateBox* pBox );
    void		UpdateButtons();
};



#endif // SC_TABPAGES_HXX
