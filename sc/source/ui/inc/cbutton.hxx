/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cbutton.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

//------------------------------------------------------------------

#ifndef SC_CBUTTON_HXX
#define SC_CBUTTON_HXX

#include <tools/gen.hxx>
#include <tools/color.hxx>

class OutputDevice;


//==================================================================

class ScDDComboBoxButton
{
public:
            ScDDComboBoxButton( OutputDevice* pOutputDevice );
            ~ScDDComboBoxButton();

    void    SetOutputDevice( OutputDevice* pOutputDevice );

    void	Draw( const Point&	rAt,
                  const Size&	rSize,
                  BOOL          bState,
                  BOOL			bBtnIn = FALSE );

    void	Draw( const Point&	rAt,
                  BOOL          bState,
                  BOOL			bBtnIn = FALSE )
                { Draw( rAt, aBtnSize, bState, bBtnIn ); }

    void    Draw( BOOL          bState,
                  BOOL          bBtnIn = FALSE )
                { Draw( aBtnPos, aBtnSize, bState, bBtnIn ); }

    void	SetOptSizePixel();

    void	SetPosPixel( const Point& rNewPos )  { aBtnPos = rNewPos; }
    Point	GetPosPixel() const				 	 { return aBtnPos; }

    void	SetSizePixel( const Size& rNewSize ) { aBtnSize = rNewSize; }
    Size	GetSizePixel() const				 { return aBtnSize; }

private:
    void	ImpDrawArrow( const Rectangle&	rRect,
                          BOOL              bState );

protected:
    OutputDevice* pOut;
    Point	aBtnPos;
    Size	aBtnSize;
};


#endif // SC_CBUTTON_HXX


