/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: select.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_SELECT_HXX
#define SC_SELECT_HXX

#ifndef _SELENG_HXX //autogen
#include <vcl/seleng.hxx>
#endif

#include "viewdata.hxx"		// ScSplitPos

// ---------------------------------------------------------------------------

class ScTabView;
class ScViewData;


class ScViewSelectionEngine : public SelectionEngine
{
private:
    ScSplitPos		eWhich;
public:
                    ScViewSelectionEngine( Window* pWindow, ScTabView* pView,
                                                    ScSplitPos eSplitPos );

    ScSplitPos		GetWhich() const			{ return eWhich; }
    void			SetWhich(ScSplitPos eNew)	{ eWhich = eNew; }
};


class ScViewFunctionSet : public FunctionSet			// View (Gridwin / Tastatur)
{
private:
    ScViewData*				pViewData;
    ScViewSelectionEngine*	pEngine;

    BOOL			bAnchor;
    BOOL			bStarted;
    ScAddress		aAnchorPos;

    ScSplitPos		GetWhich();

    ULONG           CalcUpdateInterval( const Size& rWinSize, const Point& rEffPos, 
                                        bool bLeftScroll, bool bTopScroll, bool bRightScroll, bool bBottomScroll );

public:
                    ScViewFunctionSet( ScViewData* pNewViewData );

    void			SetSelectionEngine( ScViewSelectionEngine* pSelEngine );

    void			SetAnchor( SCCOL nPosX, SCROW nPosY );
    void			SetAnchorFlag( BOOL bSet );

    virtual void	BeginDrag();
    virtual void	CreateAnchor();
    virtual void	DestroyAnchor();
    virtual BOOL	SetCursorAtPoint( const Point& rPointPixel, BOOL bDontSelectAtCursor = FALSE );
    virtual BOOL	IsSelectionAtPoint( const Point& rPointPixel );
    virtual void	DeselectAtPoint( const Point& rPointPixel );
    virtual void	DeselectAll();

    BOOL			SetCursorAtCell( SCsCOL nPosX, SCsROW nPosY, BOOL bScroll );
};


// ---------------------------------------------------------------------------


class ScHeaderFunctionSet : public FunctionSet			// Spalten- / Zeilenkoepfe
{
private:
    ScViewData*		pViewData;
    BOOL			bColumn;				// Col- / Rowbar
    ScSplitPos		eWhich;

    BOOL			bAnchor;
    SCCOLROW		nCursorPos;

public:
                    ScHeaderFunctionSet( ScViewData* pNewViewData );

    void			SetColumn( BOOL bSet );
    void			SetWhich( ScSplitPos eNew );

    virtual void	BeginDrag();
    virtual void	CreateAnchor();
    virtual void	DestroyAnchor();
    virtual BOOL	SetCursorAtPoint( const Point& rPointPixel, BOOL bDontSelectAtCursor = FALSE );
    virtual BOOL	IsSelectionAtPoint( const Point& rPointPixel );
    virtual void	DeselectAtPoint( const Point& rPointPixel );
    virtual void	DeselectAll();

    void			SetAnchorFlag(BOOL bSet)	{ bAnchor = bSet; }
};


class ScHeaderSelectionEngine : public SelectionEngine
{
public:
                    ScHeaderSelectionEngine( Window* pWindow, ScHeaderFunctionSet* pFuncSet );
};



#endif
