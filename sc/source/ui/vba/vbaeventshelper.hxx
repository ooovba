/*************************************************************************
 *
 *  OpenOffice.org - a multi-platform office productivity suite
 *
 *  $RCSfile: vbaeventshelper.hxx,v $
 *
 *  $Revision: 1.0 $
 *
 *  last change: $Author: vg $ $Date: 2007/12/07 10:42:26 $
 *
 *  The Contents of this file are made available subject to
 *  the terms of GNU Lesser General Public License Version 2.1.
 *
 *
 *    GNU Lesser General Public License Version 2.1
 *    =============================================
 *    Copyright 2005 by Sun Microsystems, Inc.
 *    901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License version 2.1, as published by the Free Software Foundation.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *    MA  02111-1307  USA
 *
 ************************************************************************/

#ifndef SC_VBAEVENTS_HXX
#define SC_VBAEVENTS_HXX

#include "document.hxx"
#include <cppuhelper/implbase1.hxx>
#include <com/sun/star/document/VbaEventId.hpp>
#include <com/sun/star/document/XVbaEventsHelper.hpp>
#include <com/sun/star/awt/XWindowListener.hpp>
#include "excelvbahelper.hxx"

#define INVALID_TAB -1

typedef ::cppu::WeakImplHelper1< com::sun::star::document::XVbaEventsHelper > VBAWorkbookEvent_BASE;

class VbaEventsListener;
class ScDocShell;
class ScVbaEventsHelper : public VBAWorkbookEvent_BASE
{
private:
	ScDocument* pDoc;
	ScDocShell* pDocShell;
    css::uno::Reference< css::uno::XComponentContext > m_xContext;
    css::uno::Reference< css::awt::XWindowListener > m_xVbaEventsListener;
    sal_Bool mbOpened;
    sal_Bool mbIgnoreEvents;

	String getSheetModuleName( SCTAB nTab );
	css::uno::Any createWorkSheet( SfxObjectShell* pShell, SCTAB nTab );
	css::uno::Any createRange( const css::uno::Any& aRange );
	css::uno::Any createHyperlink( const css::uno::Any& rCell );
	css::uno::Any createWindow( SfxObjectShell* pShell );
    SCTAB getTabFromArgs( const css::uno::Sequence< css::uno::Any > aArgs, const sal_Int32 nPos = 0 );
    rtl::OUString getEventName( const sal_Int32 nEventId );
    rtl::OUString getMacroPath( const sal_Int32 nEventId, const SCTAB nTab = INVALID_TAB );
    sal_Bool processVbaEvent( const sal_Int32 nEventId, const css::uno::Sequence< css::uno::Any >& rArgs, const SCTAB nTab = INVALID_TAB );

public:
	ScVbaEventsHelper( ScDocument* pDocument ):pDoc( pDocument ), mbOpened( sal_False ){};
    ScVbaEventsHelper( css::uno::Sequence< css::uno::Any > const& aArgs, css::uno::Reference< css::uno::XComponentContext > const& xContext );
    ~ScVbaEventsHelper();
	ScDocument* getDocument() { return pDoc; };
	ScDocShell* getDocumentShell() { return pDocShell; };
    // XVBAWorkbookEventHelper
    virtual sal_Bool SAL_CALL ProcessCompatibleVbaEvent( sal_Int32 nEventId, const css::uno::Sequence< css::uno::Any >& aArgs ) throw (css::uno::RuntimeException);
    virtual void SAL_CALL setIgnoreEvents( ::sal_Bool _ignoreevents ) throw (css::uno::RuntimeException);
    virtual ::sal_Bool SAL_CALL getIgnoreEvents() throw (css::uno::RuntimeException);
};

#endif

