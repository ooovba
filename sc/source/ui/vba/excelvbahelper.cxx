/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: vbahelper.cxx,v $
 * $Revision: 1.5.32.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#include <docuno.hxx>
#include "excelvbahelper.hxx"
#include "tabvwsh.hxx"
#include "transobj.hxx"
#include "scmod.hxx"
#include "cellsuno.hxx"
#include <comphelper/processfactory.hxx>

using namespace ::com::sun::star;
using namespace ::ooo::vba;

namespace ooo
{
namespace vba
{
namespace excel
{
void implSetZoom( const uno::Reference< frame::XModel >& xModel, sal_Int16 nZoom, std::vector< SCTAB >& nTabs )
{
    ScTabViewShell* pViewSh = excel::getBestViewShell( xModel );
    Fraction aFract( nZoom, 100 );
    pViewSh->GetViewData()->SetZoom( aFract, aFract, nTabs );
    pViewSh->RefreshZoom();
}
bool isInPrintPreview( SfxViewFrame* pView )
{
	sal_uInt16 nViewNo = SID_VIEWSHELL1 - SID_VIEWSHELL0;
	if ( pView->GetObjectShell()->GetFactory().GetViewFactoryCount() >
nViewNo && !pView->GetObjectShell()->IsInPlaceActive() )
	{
		SfxViewFactory &rViewFactory =
			pView->GetObjectShell()->GetFactory().GetViewFactory(nViewNo);
		if (  pView->GetCurViewId() == rViewFactory.GetOrdinal() )
			return true;
	}
	return false;
}

const ::rtl::OUString REPLACE_CELLS_WARNING(  RTL_CONSTASCII_USTRINGPARAM( "ReplaceCellsWarning"));

class PasteCellsWarningReseter
{
private:
	bool bInitialWarningState;
	static uno::Reference< beans::XPropertySet > getGlobalSheetSettings() throw ( uno::RuntimeException )
	{
		static uno::Reference< beans::XPropertySet > xTmpProps( ::comphelper::getProcessServiceFactory(), uno::UNO_QUERY_THROW );
		static uno::Reference<uno::XComponentContext > xContext( xTmpProps->getPropertyValue( rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "DefaultContext" ))), uno::UNO_QUERY_THROW );
		static uno::Reference<lang::XMultiComponentFactory > xServiceManager(
				xContext->getServiceManager(), uno::UNO_QUERY_THROW );
		static uno::Reference< beans::XPropertySet > xProps( xServiceManager->createInstanceWithContext( rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.sheet.GlobalSheetSettings" ) ) ,xContext ), uno::UNO_QUERY_THROW );
		return xProps;
	}

	bool getReplaceCellsWarning() throw ( uno::RuntimeException )
	{
		sal_Bool res = sal_False;
		getGlobalSheetSettings()->getPropertyValue( REPLACE_CELLS_WARNING ) >>= res;
		return ( res == sal_True );
	}

	void setReplaceCellsWarning( bool bState ) throw ( uno::RuntimeException )
	{
		getGlobalSheetSettings()->setPropertyValue( REPLACE_CELLS_WARNING, uno::makeAny( bState ) );
	}
public:
	PasteCellsWarningReseter() throw ( uno::RuntimeException )
	{
		bInitialWarningState = getReplaceCellsWarning();
		if ( bInitialWarningState )
			setReplaceCellsWarning( false );
	}
	~PasteCellsWarningReseter()
	{
		if ( bInitialWarningState )
		{
			// don't allow dtor to throw
			try
			{
				setReplaceCellsWarning( true );
			}
			catch ( uno::Exception& /*e*/ ){}
		}
	}
};

void
implnPaste( const uno::Reference< frame::XModel>& xModel )
{
	PasteCellsWarningReseter resetWarningBox;
	ScTabViewShell* pViewShell = getBestViewShell( xModel );
	if ( pViewShell )
	{
		pViewShell->PasteFromSystem();
		pViewShell->CellContentChanged();
	}
}


void
implnCopy( const uno::Reference< frame::XModel>& xModel )
{
	ScTabViewShell* pViewShell = getBestViewShell( xModel );
	if ( pViewShell )
		pViewShell->CopyToClip(NULL,false,false,true);
}

void 
implnCut( const uno::Reference< frame::XModel>& xModel )
{
	ScTabViewShell* pViewShell =  getBestViewShell( xModel );
	if ( pViewShell )
		pViewShell->CutToClip( NULL, TRUE );
}

void implnPasteSpecial( const uno::Reference< frame::XModel>& xModel, USHORT nFlags,USHORT nFunction,sal_Bool bSkipEmpty, sal_Bool bTranspose)
{
	PasteCellsWarningReseter resetWarningBox;
	sal_Bool bAsLink(sal_False), bOtherDoc(sal_False);
	InsCellCmd eMoveMode = INS_NONE;

	ScTabViewShell* pTabViewShell = getBestViewShell( xModel );
	if ( pTabViewShell )
	{
		ScViewData* pView = pTabViewShell->GetViewData();	
		Window* pWin = ( pView != NULL ) ? pView->GetActiveWin() : NULL;
		if ( pView && pWin )
		{
			if ( bAsLink && bOtherDoc )
				pTabViewShell->PasteFromSystem(0);//SOT_FORMATSTR_ID_LINK
			else 
			{
				ScTransferObj* pOwnClip = ScTransferObj::GetOwnClipboard( pWin );
				ScDocument* pDoc = NULL; 
				if ( pOwnClip )
					pDoc = pOwnClip->GetDocument();	
				pTabViewShell->PasteFromClip( nFlags, pDoc,
					nFunction, bSkipEmpty, bTranspose, bAsLink,
					eMoveMode, IDF_NONE, TRUE );
				pTabViewShell->CellContentChanged();
			}
		}
	}

}

ScDocShell* 
getDocShell( const css::uno::Reference< css::frame::XModel>& xModel ) 
{
	uno::Reference< uno::XInterface > xIf( xModel, uno::UNO_QUERY_THROW );
	ScModelObj* pModel = dynamic_cast< ScModelObj* >( xIf.get() );
	ScDocShell* pDocShell = NULL;
	if ( pModel )
		pDocShell = (ScDocShell*)pModel->GetEmbeddedObject();
	return pDocShell;

}

ScTabViewShell* 
getBestViewShell( const css::uno::Reference< css::frame::XModel>& xModel )
{
	ScDocShell* pDocShell = getDocShell( xModel );
	if ( pDocShell )
		return pDocShell->GetBestViewShell();
	return NULL;
}

ScTabViewShell* 
getCurrentBestViewShell(  const uno::Reference< uno::XComponentContext >& xContext )
{ 
	uno::Reference< frame::XModel > xModel = getCurrentExcelDoc( xContext );
	return getBestViewShell( xModel );
}

SfxViewFrame* 
getViewFrame( const uno::Reference< frame::XModel >& xModel )
{
	ScTabViewShell* pViewShell = getBestViewShell( xModel );	
	if ( pViewShell )
		return pViewShell->GetViewFrame();
	return NULL;
}

SfxItemSet*
ScVbaCellRangeAccess::GetDataSet( ScCellRangeObj* pRangeObj )
{
	SfxItemSet* pDataSet = pRangeObj ? pRangeObj->GetCurrentDataSet( true ) : NULL ;
	return pDataSet;
	
}
} //excel
} //vba 
} //ooo 
