/*************************************************************************
 *
 *  OpenOffice.org - a multi-platform office productivity suite
 *
 *  $RCSfile: vbaeventshelper.cxx,v $
 *
 *  $Revision: 1.0 $
 *
 *  last change: $Author: vg $ $Date: 2007/12/07 10:42:26 $
 *
 *  The Contents of this file are made available subject to
 *  the terms of GNU Lesser General Public License Version 2.1.
 *
 *
 *    GNU Lesser General Public License Version 2.1
 *    =============================================
 *    Copyright 2005 by Sun Microsystems, Inc.
 *    901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License version 2.1, as published by the Free Software Foundation.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *    MA  02111-1307  USA
 *
 ************************************************************************/
#include "vbaeventshelper.hxx"
#include <vbahelper/helperdecl.hxx>
#include <sfx2/objsh.hxx> 
#include "scextopt.hxx"
#include <sfx2/evntconf.hxx>
#include <sfx2/event.hxx>
#include <sfx2/sfx.hrc>
#include <toolkit/unohlp.hxx>
#include <comphelper/processfactory.hxx>
#include <cppuhelper/implbase1.hxx>
#include <com/sun/star/sheet/XSheetCellRangeContainer.hpp>
#include <com/sun/star/document/XEventsSupplier.hpp>
#include <com/sun/star/sheet/XCellRangeReferrer.hpp>
#include <com/sun/star/table/XCell.hpp> 
#include <com/sun/star/sheet/XSpreadsheetDocument.hpp>
#include <com/sun/star/sheet/XSpreadsheet.hpp>
#include <com/sun/star/container/XNamed.hpp>
#include <com/sun/star/awt/WindowEvent.hpp>
#include <com/sun/star/lang/EventObject.hpp>
#include <com/sun/star/util/XCloseListener.hpp>
#include <com/sun/star/util/XCloseBroadcaster.hpp>
#include <com/sun/star/frame/XControllerBorder.hpp>
#include <com/sun/star/frame/XBorderResizeListener.hpp>
#include <com/sun/star/util/XChangesListener.hpp>
#include <com/sun/star/util/ElementChange.hpp>
#include <com/sun/star/util/XChangesNotifier.hpp>
#include <com/sun/star/sheet/XCellRangeAddressable.hpp>
#include <cellsuno.hxx> 
#include <convuno.hxx>
#include <map>
#include <svx/msvbahelper.hxx>
#include <vcl/svapp.hxx>

using namespace std;
using namespace com::sun::star;
using namespace ooo::vba;
using namespace com::sun::star::document::VbaEventId;

typedef ::cppu::WeakImplHelper1< util::XChangesListener > WorksheetChangeListener_BASE;

class WorksheetChangeListener : public WorksheetChangeListener_BASE
{
private:
    ScVbaEventsHelper* pVbaEventsHelper;
public:
    WorksheetChangeListener(ScVbaEventsHelper* pHelper ) : pVbaEventsHelper( pHelper ){}
    virtual void SAL_CALL changesOccurred(const util::ChangesEvent& aEvent) throw (uno::RuntimeException);
    virtual void SAL_CALL disposing(const lang::EventObject& aSource) throw(uno::RuntimeException){}
};

void WorksheetChangeListener::changesOccurred(const util::ChangesEvent& aEvent) throw (uno::RuntimeException)
{
    sal_Int32 nCount = aEvent.Changes.getLength();
    if( nCount == 0 )
        return;
    
    util::ElementChange aChange = aEvent.Changes[ 0 ];
    rtl::OUString sOperation;
    aChange.Accessor >>= sOperation;
    if( !sOperation.equalsIgnoreAsciiCaseAscii("cell-change") )
        return;

    if( nCount == 1 )
    {
        uno::Reference< table::XCellRange > xRangeObj;
        aChange.ReplacedElement >>= xRangeObj;
        if( xRangeObj.is() )
        {
            uno::Sequence< uno::Any > aArgs(1);
            aArgs[0] <<= xRangeObj;
            pVbaEventsHelper->ProcessCompatibleVbaEvent( VBAEVENT_WORKSHEET_CHANGE, aArgs );
        }    
        return;
    }

    ScRangeList aRangeList;
    for( sal_Int32 nIndex = 0; nIndex < nCount; ++nIndex )
    {
        aChange = aEvent.Changes[ nIndex ];
        aChange.Accessor >>= sOperation;
        uno::Reference< table::XCellRange > xRangeObj;
        aChange.ReplacedElement >>= xRangeObj;
        if( xRangeObj.is() && sOperation.equalsIgnoreAsciiCaseAscii("cell-change") )
        {
            uno::Reference< sheet::XCellRangeAddressable > xCellRangeAddressable( xRangeObj, uno::UNO_QUERY );
            if( xCellRangeAddressable.is() )
            {
                ScRange aRange;
                ScUnoConversion::FillScRange( aRange, xCellRangeAddressable->getRangeAddress() );
                aRangeList.Append( aRange );
            }
        }
    }
    
    if( aRangeList.Count() > 0 )
    {
        uno::Reference< sheet::XSheetCellRangeContainer > xRanges( new ScCellRangesObj( pVbaEventsHelper->getDocumentShell(), aRangeList ) );
        uno::Sequence< uno::Any > aArgs(1);
        aArgs[0] <<= xRanges;
        pVbaEventsHelper->ProcessCompatibleVbaEvent( VBAEVENT_WORKSHEET_CHANGE, aArgs );
    }
}

typedef ::cppu::WeakImplHelper3< awt::XWindowListener, util::XCloseListener, frame::XBorderResizeListener > WindowListener_BASE;

// This class is to process Workbook window related event
class VbaEventsListener : public WindowListener_BASE
{
    ::osl::Mutex m_aMutex;
    ScVbaEventsHelper* pVbaEventsHelper;
    uno::Reference< frame::XModel > m_xModel;
    sal_Bool m_bWindowResized;
    sal_Bool m_bBorderChanged;
protected :
    uno::Reference< awt::XWindow > GetContainerWindow();
    uno::Reference< frame::XFrame > GetFrame();
    sal_Bool IsMouseReleased();
    DECL_LINK( fireResizeMacro, void* );
    void processWindowResizeMacro();
public :
    VbaEventsListener( ScVbaEventsHelper* pHelper );
    ~VbaEventsListener();
    void startEventsLinstener();
    void stopEventsLinstener();
    // XWindowListener
    virtual void SAL_CALL windowResized( const awt::WindowEvent& aEvent ) throw ( uno::RuntimeException );
    virtual void SAL_CALL windowMoved( const awt::WindowEvent& aEvent ) throw ( uno::RuntimeException );
    virtual void SAL_CALL windowShown( const lang::EventObject& aEvent ) throw ( uno::RuntimeException );
    virtual void SAL_CALL windowHidden( const lang::EventObject& aEvent ) throw ( uno::RuntimeException );
    virtual void SAL_CALL disposing( const lang::EventObject& aEvent ) throw ( uno::RuntimeException );
    // XCloseListener
    virtual void SAL_CALL queryClosing( const lang::EventObject& Source, ::sal_Bool GetsOwnership ) throw (util::CloseVetoException, uno::RuntimeException);
    virtual void SAL_CALL notifyClosing( const lang::EventObject& Source ) throw (uno::RuntimeException);
    // XBorderResizeListener
    virtual void SAL_CALL borderWidthsChanged( const uno::Reference< uno::XInterface >& aObject, const frame::BorderWidths& aNewSize ) throw (uno::RuntimeException);
};
VbaEventsListener::VbaEventsListener( ScVbaEventsHelper* pHelper ) : pVbaEventsHelper( pHelper )
{
    OSL_TRACE("VbaEventsListener::VbaEventsListener( 0x%x ) - ctor ", this );
    m_xModel.set( pVbaEventsHelper->getDocument()->GetDocumentShell()->GetModel(), uno::UNO_QUERY );
    m_bWindowResized = sal_False;
    m_bBorderChanged = sal_False;
}

VbaEventsListener::~VbaEventsListener()
{
    OSL_TRACE("VbaEventsListener::~VbaEventsListener( 0x%x ) - dtor ", this );
}
uno::Reference< frame::XFrame > 
VbaEventsListener::GetFrame()
{
    try
    {
        if( pVbaEventsHelper )
        {
            if( m_xModel.is() )
            {
                uno::Reference< frame::XController > xController( m_xModel->getCurrentController(), uno::UNO_QUERY );
                if( xController.is() )
                {
                    uno::Reference< frame::XFrame > xFrame( xController->getFrame(), uno::UNO_QUERY );
                    if( xFrame.is() )
                    {
                            return xFrame;
                    }
                }
            }
        }
    }
	catch( uno::Exception& /*e*/ )
    {
    }
    return uno::Reference< frame::XFrame >();
}
uno::Reference< awt::XWindow > 
VbaEventsListener::GetContainerWindow() 
{
    try
    {
        uno::Reference< frame::XFrame > xFrame( GetFrame(), uno::UNO_QUERY );
        if( xFrame.is() )
        {
            uno::Reference< awt::XWindow > xWindow( xFrame->getContainerWindow(), uno::UNO_QUERY );
            if( xWindow.is() )
                return xWindow;
        }
    }
	catch( uno::Exception& /*e*/ )
    {
    }
    return uno::Reference< awt::XWindow >();
}
sal_Bool
VbaEventsListener::IsMouseReleased()
{
    Window* pWindow = (VCLUnoHelper::GetWindow(  GetContainerWindow() ) );
    if( pWindow )
    {
        Window::PointerState aPointerState = pWindow->GetPointerState();
        if( !aPointerState.mnState & ( MOUSE_LEFT | MOUSE_MIDDLE | MOUSE_RIGHT ) )
            return sal_True; 
    }
    return sal_False;
}
void
VbaEventsListener::startEventsLinstener()
{
    if( m_xModel.is() )
    {
        // add window listener
        uno::Reference< awt::XWindow > xWindow( GetContainerWindow(), uno::UNO_QUERY );
        if( xWindow.is() )
            xWindow->addWindowListener( this );
        // add close listener
        //uno::Reference< util::XCloseBroadcaster > xCloseBroadcaster( GetFrame(), uno::UNO_QUERY );
        uno::Reference< util::XCloseBroadcaster > xCloseBroadcaster( m_xModel, uno::UNO_QUERY );
        if( xCloseBroadcaster.is() )
        {
            xCloseBroadcaster->addCloseListener( this );
        }
        // add Border resize listener
        uno::Reference< frame::XController > xController( m_xModel->getCurrentController(), uno::UNO_QUERY );
        if( xController.is() )
        {
            uno::Reference< frame::XControllerBorder > xControllerBorder( xController, uno::UNO_QUERY );
            if( xControllerBorder.is() )
            {
                xControllerBorder->addBorderResizeListener( this );
            }
        }
    }
}
void
VbaEventsListener::stopEventsLinstener()
{
    if( m_xModel.is() )
    {
        uno::Reference< awt::XWindow > xWindow( GetContainerWindow(), uno::UNO_QUERY );
        if( xWindow.is() )
        {
            xWindow->removeWindowListener( this );
        }
        //uno::Reference< util::XCloseBroadcaster > xCloseBroadcaster( GetFrame(), uno::UNO_QUERY );
        uno::Reference< util::XCloseBroadcaster > xCloseBroadcaster( m_xModel, uno::UNO_QUERY );
        if( xCloseBroadcaster.is() )
        {
            xCloseBroadcaster->removeCloseListener( this );
        }
        uno::Reference< frame::XController > xController( m_xModel->getCurrentController(), uno::UNO_QUERY );
        if( xController.is() )
        {
            uno::Reference< frame::XControllerBorder > xControllerBorder( xController, uno::UNO_QUERY );
            if( xControllerBorder.is() )
            {
                xControllerBorder->removeBorderResizeListener( this );
            }
        }
        pVbaEventsHelper = NULL;
    }
}

void
VbaEventsListener::processWindowResizeMacro()
{
    OSL_TRACE("**** Attempt to FIRE MACRO **** ");
    if( pVbaEventsHelper )
        pVbaEventsHelper->ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_WINDOWRESIZE, uno::Sequence< uno::Any >() );
}

IMPL_LINK( VbaEventsListener, fireResizeMacro, void*, pParam )
{
    if ( pVbaEventsHelper ) 
    {
        if( IsMouseReleased() )
                processWindowResizeMacro();
    }
    release();
    return 0;
}

void SAL_CALL
VbaEventsListener::windowResized(  const awt::WindowEvent& /*aEvent*/ ) throw ( uno::RuntimeException )
{
    ::osl::MutexGuard aGuard( m_aMutex );
    // Workbook_window_resize event
    m_bWindowResized = sal_True;
    Window* pWindow = (VCLUnoHelper::GetWindow(  GetContainerWindow() ) );

    if( pWindow && m_bBorderChanged )
    {
        m_bBorderChanged = m_bWindowResized = sal_False;
        acquire(); // ensure we don't get deleted before the event is handled
        Application::PostUserEvent( LINK( this, VbaEventsListener, fireResizeMacro ), NULL );
    }
}
void SAL_CALL
VbaEventsListener::windowMoved(  const awt::WindowEvent& /*aEvent*/ ) throw ( uno::RuntimeException )
{
    // not interest this time
}
void SAL_CALL
VbaEventsListener::windowShown(  const lang::EventObject& /*aEvent*/ ) throw ( uno::RuntimeException )
{
    // not interest this time
}
void SAL_CALL
VbaEventsListener::windowHidden(  const lang::EventObject& /*aEvent*/ ) throw ( uno::RuntimeException )
{
    // not interest this time
}
void SAL_CALL
VbaEventsListener::disposing(  const lang::EventObject& /*aEvent*/ ) throw ( uno::RuntimeException )
{
    ::osl::MutexGuard aGuard( m_aMutex );
    OSL_TRACE("VbaEventsListener::disposing(0x%x)", this);
    pVbaEventsHelper = NULL;
}
void SAL_CALL 
VbaEventsListener::queryClosing( const lang::EventObject& Source, ::sal_Bool GetsOwnership ) throw (util::CloseVetoException, uno::RuntimeException)
{
     // it can cancel the close, but need to throw a CloseVetoException, and it will be transmit to caller.
}
void SAL_CALL 
VbaEventsListener::notifyClosing( const lang::EventObject& Source ) throw (uno::RuntimeException)
{
    ::osl::MutexGuard aGuard( m_aMutex );
    stopEventsLinstener();
}
void SAL_CALL 
VbaEventsListener::borderWidthsChanged( const uno::Reference< uno::XInterface >& aObject, const frame::BorderWidths& aNewSize ) throw (uno::RuntimeException)
{
    ::osl::MutexGuard aGuard( m_aMutex );
    // work with WindowResized event to guard Window Resize event.
    m_bBorderChanged = sal_True;
    Window* pWindow = (VCLUnoHelper::GetWindow(  GetContainerWindow() ) );
    if( pWindow && m_bWindowResized )
    {
        m_bWindowResized = m_bBorderChanged = sal_False;
        acquire(); // ensure we don't get deleted before the timer fires.
        Application::PostUserEvent( LINK( this, VbaEventsListener, fireResizeMacro ), NULL );
    }
}

class ImplVbaEventNameInfo
{
private:
    map< sal_Int32, rtl::OUString > m_aEventNameMap;

protected:
    static ImplVbaEventNameInfo* pImplVbaEventNameInfo;
    ImplVbaEventNameInfo() { InitImplVbaEventNameInfo(); }
private:
    void insert( const sal_Int32 nId, const rtl::OUString& sEventName )
    {
        m_aEventNameMap.insert( make_pair( nId, sEventName ) );
    }
    void InitImplVbaEventNameInfo();
public:
    virtual ~ImplVbaEventNameInfo();
    rtl::OUString getEventName( const sal_Int32 nId )
    {
        map< sal_Int32, rtl::OUString >::iterator iter = m_aEventNameMap.find( nId );
        if( iter != m_aEventNameMap.end() )
            return iter->second;
        return rtl::OUString();
    }
    static ImplVbaEventNameInfo* GetImplVbaEventNameInfo();
};
ImplVbaEventNameInfo* ImplVbaEventNameInfo::pImplVbaEventNameInfo = NULL;

ImplVbaEventNameInfo::~ImplVbaEventNameInfo()
{
    if( pImplVbaEventNameInfo )
    {
        delete pImplVbaEventNameInfo;
        pImplVbaEventNameInfo = NULL;
    }
}

ImplVbaEventNameInfo* 
ImplVbaEventNameInfo::GetImplVbaEventNameInfo()
{
    if( !pImplVbaEventNameInfo )
    {
        pImplVbaEventNameInfo = new ImplVbaEventNameInfo;
    }
    return pImplVbaEventNameInfo;
}

#define CREATEOUSTRING(asciistr) rtl::OUString::createFromAscii(asciistr)

#define INSERT_EVENT_INFO( Object, Event, ObjectName, EventName ) \
        insert( VBAEVENT_##Object##_##Event, ObjectName + CREATEOUSTRING( EventName ) )

#define INSERT_WORKSHEET_EVENT_INFO( Event, EventName ) \
        INSERT_EVENT_INFO( WORKSHEET, Event,CREATEOUSTRING("Worksheet_"), EventName ); \
        INSERT_EVENT_INFO( WORKBOOK_SHEET, Event, CREATEOUSTRING("Workbook_Sheet"), EventName )

#define INSERT_WORKBOOK_EVENT_INFO( Event, EventName ) \
        INSERT_EVENT_INFO(  WORKBOOK, Event, CREATEOUSTRING("Workbook_"), EventName )

void ImplVbaEventNameInfo::InitImplVbaEventNameInfo()
{
    INSERT_WORKSHEET_EVENT_INFO( ACTIVATE, "Activate");
    INSERT_WORKSHEET_EVENT_INFO( BEFOREDOUBLECLICK, "BeforeDoubleClick" );
    INSERT_WORKSHEET_EVENT_INFO( BEFORERIGHTCLICK, "BeforeRightClick" );
    INSERT_WORKSHEET_EVENT_INFO( CALCULATE, "Calculate" );
    INSERT_WORKSHEET_EVENT_INFO( CHANGE, "Change" );
    INSERT_WORKSHEET_EVENT_INFO( DEACTIVATE, "Deactivate" );
    INSERT_WORKSHEET_EVENT_INFO( FOLLOWHYPERLINK, "FollowHyperlink" );
    INSERT_WORKSHEET_EVENT_INFO( PIVOTTABLEUPDATE, "PivotTableUpdate" );
    INSERT_WORKSHEET_EVENT_INFO( SELECTIONCHANGE, "SelectionChange" );

    // Workbook
    INSERT_WORKBOOK_EVENT_INFO( ACTIVATE, "Activate" );
    INSERT_WORKBOOK_EVENT_INFO( DEACTIVATE, "Deactivate" );
    INSERT_WORKBOOK_EVENT_INFO( OPEN, "Open" );
    // AUTOOPEN doesn't be used. TODO, this should be "auto_open"
    insert( VBAEVENT_WORKBOOK_AUTOOPEN, CREATEOUSTRING("Auto_Open") );                       
    INSERT_WORKBOOK_EVENT_INFO( BEFORECLOSE, "BeforeClose" );
    INSERT_WORKBOOK_EVENT_INFO( BEFOREPRINT, "BeforePrint" );
    INSERT_WORKBOOK_EVENT_INFO( BEFORESAVE, "BeforeSave" );
    INSERT_WORKBOOK_EVENT_INFO( NEWSHEET, "NewSheet" );
    INSERT_WORKBOOK_EVENT_INFO( WINDOWACTIVATE, "WindowActivate" );
    INSERT_WORKBOOK_EVENT_INFO( WINDOWDEACTIVATE, "WindowDeactivate" );
    INSERT_WORKBOOK_EVENT_INFO( WINDOWRESIZE, "WindowResize" );
}

ScVbaEventsHelper::ScVbaEventsHelper( uno::Sequence< css::uno::Any > const& aArgs, uno::Reference< uno::XComponentContext > const& xContext )
    : m_xContext( xContext ), mbOpened( sal_False ), mbIgnoreEvents( sal_False )
{
    uno::Reference< frame::XModel > xModel ( getXSomethingFromArgs< frame::XModel >( aArgs, 0 ), uno::UNO_QUERY );
    pDocShell = excel::getDocShell( xModel );
    pDoc = pDocShell->GetDocument();
    // Add worksheet change listener
    uno::Reference< util::XChangesNotifier > xChangesNotifier( xModel, uno::UNO_QUERY );
    if( xChangesNotifier.is() )
        xChangesNotifier->addChangesListener( uno::Reference< util::XChangesListener >( new WorksheetChangeListener( this ) ) );
}

ScVbaEventsHelper::~ScVbaEventsHelper()
{
}

rtl::OUString
ScVbaEventsHelper::getEventName( const sal_Int32 nId )
{
    rtl::OUString sEventName;
    ImplVbaEventNameInfo* pEventInfo = ImplVbaEventNameInfo::GetImplVbaEventNameInfo();
    if( pEventInfo )
        sEventName = pEventInfo->getEventName( nId );
    return sEventName;
}

uno::Any ScVbaEventsHelper::createWorkSheet( SfxObjectShell* pShell, SCTAB nTab )
{
	uno::Any aRet;
	try
	{
		uno::Reference< lang::XMultiComponentFactory > xSMgr( ::comphelper::getProcessServiceFactory(), uno::UNO_QUERY_THROW );
		uno::Reference< beans::XPropertySet > xProps( xSMgr, uno::UNO_QUERY_THROW );
		uno::Reference<uno::XComponentContext > xCtx( xProps->getPropertyValue( rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "DefaultContext" ))), uno::UNO_QUERY_THROW );
		// Eventually we will be able to pull the Workbook/Worksheet objects
		// directly from basic and register them as listeners

		// create Workbook
		uno::Sequence< uno::Any > aArgs(2);
		aArgs[0] = uno::Any( uno::Reference< uno::XInterface >() );
		aArgs[1] = uno::Any( pShell->GetModel() );
		uno::Reference< uno::XInterface > xWorkbook( ov::createVBAUnoAPIServiceWithArgs( pShell, "ooo.vba.excel.Workbook", aArgs ), uno::UNO_QUERY );

		// create WorkSheet
		String sSheetName;
		pDoc->GetName( nTab, sSheetName );
		aArgs = uno::Sequence< uno::Any >(3);
		aArgs[ 0 ] <<= xWorkbook;
		aArgs[ 1 ] <<= pShell->GetModel();
		aArgs[ 2 ] = uno::makeAny( rtl::OUString( sSheetName ) );
		aRet <<= ov::createVBAUnoAPIServiceWithArgs( pShell, "ooo.vba.excel.Worksheet", aArgs );
	}
	catch( uno::Exception& e )
	{
	}
	return aRet;
}

uno::Any ScVbaEventsHelper::createRange( const uno::Any& aRange )
{
	uno::Any aRet;
	try
	{
		uno::Reference< sheet::XSheetCellRangeContainer > xRanges( 	aRange, uno::UNO_QUERY );
		uno::Reference< table::XCellRange > xRange( aRange, uno::UNO_QUERY );
		uno::Reference< lang::XMultiComponentFactory > xSMgr( ::comphelper::getProcessServiceFactory(), uno::UNO_QUERY_THROW );
		uno::Reference< beans::XPropertySet > xProps( xSMgr, uno::UNO_QUERY_THROW );
		if (  xRanges.is() || xRange.is() )
		{
			uno::Reference<uno::XComponentContext > xCtx( xProps->getPropertyValue( rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "DefaultContext" ))), uno::UNO_QUERY_THROW );
			uno::Sequence< uno::Any > aArgs(2);
			aArgs[0] = uno::Any( uno::Reference< uno::XInterface >() ); // dummy parent
			if ( xRanges.is() )
			{
				aArgs[1] <<= xRanges;
			}
			else if ( xRange.is() )
			{
				aArgs[1] <<= xRange;
			}
			else
			{
				throw uno::RuntimeException(); // 
			}
			aRet <<= ov::createVBAUnoAPIServiceWithArgs( pDoc->GetDocumentShell(), "ooo.vba.excel.Range", aArgs );	
		}
	}
	catch( uno::Exception& e ) 
	{
	}
	return aRet;
}

uno::Any ScVbaEventsHelper::createHyperlink( const uno::Any& rCell )
{
	uno::Any aRet;
	try
	{
		uno::Reference< lang::XMultiComponentFactory > xSMgr( ::comphelper::getProcessServiceFactory(), uno::UNO_QUERY_THROW );
		uno::Reference< beans::XPropertySet > xProps( xSMgr, uno::UNO_QUERY_THROW ); 
		uno::Reference< table::XCell > xCell( rCell, uno::UNO_QUERY );
		if( xCell.is() )
		{
			uno::Reference<uno::XComponentContext > xCtx( xProps->getPropertyValue( rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "DefaultContext" ))), uno::UNO_QUERY_THROW );
			uno::Sequence< uno::Any > aArgs(2);
			aArgs[0] = uno::Any( uno::Reference< uno::XInterface >() ); // dummy parent
			aArgs[1] <<= rCell;
			
			aRet <<= ov::createVBAUnoAPIServiceWithArgs( pDoc->GetDocumentShell(), "ooo.vba.excel.Hyperlink", aArgs );	
		}
		else
		{
			throw uno::RuntimeException(); // 
		}
	}
	catch( uno::Exception& e ) 
	{
	}
	return aRet;
}

uno::Any ScVbaEventsHelper::createWindow( SfxObjectShell* pShell )
{
    try
    {
        uno::Reference< lang::XMultiServiceFactory > xSF( comphelper::getProcessServiceFactory(), uno::UNO_QUERY );
        uno::Reference< frame::XModel > xModel( pShell->GetModel(), uno::UNO_QUERY );
        uno::Sequence< uno::Any > aWindowArgs(2);
        aWindowArgs[0] = uno::Any( uno::Reference< uno::XInterface > () );
        aWindowArgs[1] = uno::Any( xModel );
        uno::Reference< uno::XInterface > xWindow( xSF->createInstanceWithArguments( rtl::OUString( RTL_CONSTASCII_USTRINGPARAM("ooo.vba.excel.Window" ) ), aWindowArgs ), uno::UNO_QUERY );
        if( xWindow.is() )
            return uno::makeAny( xWindow );
    }
	catch( uno::Exception& e )
	{
	}
    return uno::Any();
}

String ScVbaEventsHelper::getSheetModuleName( SCTAB nTab )
{
	ScExtDocOptions* pExtOptions = pDoc->GetExtDocOptions();
	String aCodeName;
	pDoc->GetName( nTab, aCodeName);
	// Use code name if that exists
	if ( pExtOptions )
		aCodeName = pExtOptions->GetCodeName( nTab );
	return aCodeName;	
}

rtl::OUString
ScVbaEventsHelper::getMacroPath( const sal_Int32 nEventId, const SCTAB nTab )
{
	SfxObjectShell* pShell = pDoc->GetDocumentShell();
    String sMacroName = getEventName( nEventId );
    VBAMacroResolvedInfo sMacroResolvedInfo;
    switch( nEventId )
    {
        // Worksheet
    	case VBAEVENT_WORKSHEET_ACTIVATE                      :
    	case VBAEVENT_WORKSHEET_BEFOREDOUBLECLICK             :
    	case VBAEVENT_WORKSHEET_BEFORERIGHTCLICK              :
    	case VBAEVENT_WORKSHEET_CALCULATE                     :
    	case VBAEVENT_WORKSHEET_CHANGE                        :
    	case VBAEVENT_WORKSHEET_DEACTIVATE                    :
    	case VBAEVENT_WORKSHEET_FOLLOWHYPERLINK               :
    	case VBAEVENT_WORKSHEET_PIVOTTABLEUPDATE              :
    	case VBAEVENT_WORKSHEET_SELECTIONCHANGE               :
        {
            String  aSheetModuleName = getSheetModuleName( nTab );
            sMacroName.Insert( '.', 0 ).Insert( aSheetModuleName, 0);
            sMacroResolvedInfo = resolveVBAMacro( pShell, sMacroName );
            break;
        }
        // Workbook
    	case VBAEVENT_WORKBOOK_ACTIVATE                       :
    	case VBAEVENT_WORKBOOK_DEACTIVATE                     :
    	case VBAEVENT_WORKBOOK_OPEN                           :
    	case VBAEVENT_WORKBOOK_BEFORECLOSE                    :
    	case VBAEVENT_WORKBOOK_BEFOREPRINT                    :
    	case VBAEVENT_WORKBOOK_BEFORESAVE                     :
    	case VBAEVENT_WORKBOOK_NEWSHEET                       :
    	case VBAEVENT_WORKBOOK_WINDOWACTIVATE                 :
    	case VBAEVENT_WORKBOOK_WINDOWDEACTIVATE               :
    	case VBAEVENT_WORKBOOK_WINDOWRESIZE                   :
        // Workbook_sheet
    	case VBAEVENT_WORKBOOK_SHEET_ACTIVATE                 :
    	case VBAEVENT_WORKBOOK_SHEET_BEFOREDOUBLECLICK        :
    	case VBAEVENT_WORKBOOK_SHEET_BEFORERIGHTCLICK         :
    	case VBAEVENT_WORKBOOK_SHEET_CALCULATE                :
    	case VBAEVENT_WORKBOOK_SHEET_CHANGE                   :
    	case VBAEVENT_WORKBOOK_SHEET_DEACTIVATE               :
    	case VBAEVENT_WORKBOOK_SHEET_FOLLOWHYPERLINK          :
    	case VBAEVENT_WORKBOOK_SHEET_PIVOTTABLEUPDATE         :
    	case VBAEVENT_WORKBOOK_SHEET_SELECTIONCHANGE          :
        {
	        ScExtDocOptions* pExtOptions = pDoc->GetExtDocOptions();
            String sWorkbookModuleName = pDoc->GetCodeName();
            if( pExtOptions )
            {
                ScExtDocSettings aExtDocSettings = pExtOptions->GetDocSettings();
                sWorkbookModuleName = aExtDocSettings.maGlobCodeName;
            }
           
            sMacroName.Insert( '.', 0 ).Insert( sWorkbookModuleName, 0);
            sMacroResolvedInfo = resolveVBAMacro( pShell, sMacroName );
            break;
        }
    	case VBAEVENT_WORKBOOK_AUTOOPEN                       :
        {
            sMacroResolvedInfo = resolveVBAMacro( pShell, sMacroName );
            break;
        }
        default:
            break;
    }
    return sMacroResolvedInfo.ResolvedMacro();
}

sal_Bool ScVbaEventsHelper::processVbaEvent( const sal_Int32 nEventId, const uno::Sequence< uno::Any >& rArgs, const SCTAB nTab )
{
	SfxObjectShell* pShell = pDoc->GetDocumentShell();

	sal_Bool result = sal_False;
    sal_Bool bCancel = sal_False;
	uno::Sequence< uno::Any > aArgs;
	uno::Any aRet;
	uno::Any aDummyCaller;
    
    // For most cases, there is no corresponsible event macro in the document. 
    // It is better fo check if the event macro exists before process the arguments to improve performance.
    rtl::OUString sMacroPath = getMacroPath( nEventId, nTab );
	if( sMacroPath.getLength() )
	{
		switch( nEventId )
		{
			case VBAEVENT_WORKSHEET_ACTIVATE:
			case VBAEVENT_WORKSHEET_CALCULATE:
			case VBAEVENT_WORKSHEET_DEACTIVATE:
            case VBAEVENT_WORKBOOK_ACTIVATE:
            case VBAEVENT_WORKBOOK_DEACTIVATE:
            case VBAEVENT_WORKBOOK_OPEN:
    	    case VBAEVENT_WORKBOOK_AUTOOPEN:
			{
				// no arguments
				break;
			}
    	    case VBAEVENT_WORKBOOK_SHEET_DEACTIVATE:
    	    case VBAEVENT_WORKBOOK_SHEET_CALCULATE:
    	    case VBAEVENT_WORKBOOK_SHEET_ACTIVATE:
            case VBAEVENT_WORKBOOK_NEWSHEET:
            {
				aArgs = uno::Sequence< uno::Any >(1);
				aArgs[0] = createWorkSheet( pShell, nTab ); 
				break;
            }
			case VBAEVENT_WORKSHEET_CHANGE:
			case VBAEVENT_WORKSHEET_SELECTIONCHANGE:
			{
				// one argument: range
				uno::Any aRange = createRange( rArgs[0] );
				aArgs = uno::Sequence< uno::Any >(1);
				aArgs[0] = aRange;
				break;
			}
    	    case VBAEVENT_WORKBOOK_SHEET_CHANGE:
    	    case VBAEVENT_WORKBOOK_SHEET_SELECTIONCHANGE:
            {
				uno::Any aRange = createRange( rArgs[0] );
				aArgs = uno::Sequence< uno::Any >(2);
				aArgs[0] = createWorkSheet( pShell, nTab );
				aArgs[1] = aRange;
				break;
            }
			case VBAEVENT_WORKSHEET_BEFOREDOUBLECLICK:
			case VBAEVENT_WORKSHEET_BEFORERIGHTCLICK:
			{
				// two aruments: range and cancel
				uno::Any aRange = createRange( rArgs[0] );
				aArgs = uno::Sequence< uno::Any >(2);
				aArgs[0] = aRange;
				aArgs[1] <<= bCancel;
				// TODO: process "cancel" action  
				break;
			}
    	    case VBAEVENT_WORKBOOK_SHEET_BEFOREDOUBLECLICK:
    	    case VBAEVENT_WORKBOOK_SHEET_BEFORERIGHTCLICK:
            {
				uno::Any aRange = createRange( rArgs[0] );
				aArgs = uno::Sequence< uno::Any >(3);
				aArgs[0] = createWorkSheet( pShell, nTab );
				aArgs[1] = aRange;
				aArgs[2] <<= bCancel;
				// TODO: process "cancel" action  
				break;
            }
			case VBAEVENT_WORKSHEET_FOLLOWHYPERLINK:
			{
				// one argument: hyperlink
				uno::Any aHyperlink = createHyperlink( rArgs[0] );
				aArgs = uno::Sequence< uno::Any >(1);
				aArgs[0] = aHyperlink;
				break;
			}
    	    case VBAEVENT_WORKBOOK_SHEET_FOLLOWHYPERLINK:
            {
				uno::Any aHyperlink = createHyperlink( rArgs[0] );
				aArgs = uno::Sequence< uno::Any >(2);
				aArgs[0] = createWorkSheet( pShell, nTab );
				aArgs[1] = aHyperlink;
				break;
            }
			case VBAEVENT_WORKSHEET_PIVOTTABLEUPDATE:
    	    case VBAEVENT_WORKBOOK_SHEET_PIVOTTABLEUPDATE:
			{
				// one argument: pivottable
                // TODO: not support yet
				return result;
			}
            case VBAEVENT_WORKBOOK_BEFORECLOSE:
            case VBAEVENT_WORKBOOK_BEFOREPRINT:
            {
                // process Cancel argument
                aArgs = uno::Sequence< uno::Any >(1);
                aArgs[0] <<= bCancel;
                executeMacro( pShell, sMacroPath, aArgs, aRet, aDummyCaller );
                aArgs[0] >>= bCancel;
                return bCancel;
            }
            case VBAEVENT_WORKBOOK_BEFORESAVE:
            {
                // two arguments: SaveAs and Cancel
                aArgs = uno::Sequence< uno::Any >(2);
                aArgs[0] = rArgs[0];
                aArgs[1] <<= bCancel;
                executeMacro( pShell, sMacroPath, aArgs, aRet, aDummyCaller );
                aArgs[1] >>= bCancel;
                return bCancel;
            }
            case VBAEVENT_WORKBOOK_WINDOWACTIVATE:
            case VBAEVENT_WORKBOOK_WINDOWDEACTIVATE:
            case VBAEVENT_WORKBOOK_WINDOWRESIZE:
            {
                // one argument: windows
                aArgs = uno::Sequence< uno::Any >(1);
                aArgs[0] = createWindow( pShell );
                break;
            }
            default:
                return result;
		}
    
        // excute the macro
        result = executeMacro( pShell, sMacroPath, aArgs, aRet, aDummyCaller );
	}
    
	return result;
}

SCTAB ScVbaEventsHelper::getTabFromArgs( const uno::Sequence< uno::Any > aArgs, const sal_Int32 nPos )
{
    SCTAB nTab = -1;
    uno::Reference< sheet::XCellRangeAddressable > xCellRangeAddressable( getXSomethingFromArgs< sheet::XCellRangeAddressable >( aArgs, nPos ), uno::UNO_QUERY );
    if( xCellRangeAddressable.is() )
    {
        table::CellRangeAddress aAddress = xCellRangeAddressable->getRangeAddress();
        nTab = aAddress.Sheet;
    }
    else
    {
        uno::Reference< sheet::XSheetCellRangeContainer > xRanges( getXSomethingFromArgs< sheet::XSheetCellRangeContainer >( aArgs, nPos ), uno::UNO_QUERY );
        if( xRanges.is() )
        {
            uno::Sequence< table::CellRangeAddress > aRangeAddresses = xRanges->getRangeAddresses();
            if( aRangeAddresses.getLength() > 0 )
            {
                nTab = aRangeAddresses[ 0 ].Sheet;
            }
        }
    }
    return nTab;
}

sal_Bool SAL_CALL 
ScVbaEventsHelper::ProcessCompatibleVbaEvent( sal_Int32 nEventId, const uno::Sequence< uno::Any >& aArgs ) throw (uno::RuntimeException)
{
	SfxObjectShell* pShell = pDoc->GetDocumentShell();
	if( !pShell || mbIgnoreEvents)
		return sal_False;
    
    // In order to better support "withevents" in the future, 
    // it is better to process a event at a time
    SCTAB nTab = INVALID_TAB;
    switch( nEventId )
    {
        // Worksheet
		case VBAEVENT_WORKSHEET_ACTIVATE:
        {
            aArgs[0] >>= nTab;
            if( nTab != INVALID_TAB )
            {
                // process the event
                processVbaEvent( nEventId, aArgs, nTab );
                // recursive process related workbook sheet event.
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_ACTIVATE, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_BEFOREDOUBLECLICK:
        {
            nTab = getTabFromArgs( aArgs );
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_BEFOREDOUBLECLICK, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_BEFORERIGHTCLICK:
        {
            nTab = getTabFromArgs( aArgs );
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_BEFORERIGHTCLICK, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_CALCULATE:
        {
            aArgs[0] >>= nTab;
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_CALCULATE, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_CHANGE:
        {
            nTab = getTabFromArgs( aArgs );
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_CHANGE, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_DEACTIVATE:
        {
            aArgs[0] >>= nTab;
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_DEACTIVATE, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_FOLLOWHYPERLINK:
        {
            nTab = getTabFromArgs( aArgs );
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_FOLLOWHYPERLINK, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKSHEET_PIVOTTABLEUPDATE:
            // TODO
            break;
		case VBAEVENT_WORKSHEET_SELECTIONCHANGE:
        {
            nTab = getTabFromArgs( aArgs );
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_SHEET_SELECTIONCHANGE, aArgs );
            }
            break;
        }
        // Workbook_sheet
    	case VBAEVENT_WORKBOOK_SHEET_ACTIVATE:
        case VBAEVENT_WORKBOOK_SHEET_CALCULATE:
    	case VBAEVENT_WORKBOOK_SHEET_DEACTIVATE:
        {
            aArgs[0] >>= nTab;
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
            }
            break;
        }
    	case VBAEVENT_WORKBOOK_SHEET_BEFOREDOUBLECLICK:
        case VBAEVENT_WORKBOOK_SHEET_BEFORERIGHTCLICK:
    	case VBAEVENT_WORKBOOK_SHEET_CHANGE:
    	case VBAEVENT_WORKBOOK_SHEET_FOLLOWHYPERLINK:
    	case VBAEVENT_WORKBOOK_SHEET_SELECTIONCHANGE:
        {
            nTab = getTabFromArgs( aArgs );
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
            }
            break;
        }
    	case VBAEVENT_WORKBOOK_SHEET_PIVOTTABLEUPDATE:
        // TODO
            break;
		// Workbook
		case VBAEVENT_WORKBOOK_ACTIVATE:
        {
            // if workbook open event do not be fired. fired it before 
            // workbook activate event to compatible with MSO.
            if( mbOpened )
            {
                // process workbook activate event
                processVbaEvent( nEventId, aArgs );
                // process workbook window activate event at the same time
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_WINDOWACTIVATE, aArgs );
            }
            break;
        }
		case VBAEVENT_WORKBOOK_DEACTIVATE:
        {
            processVbaEvent( nEventId, aArgs );
            // same as workbook window deactivate
            ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_WINDOWDEACTIVATE, aArgs );
            break;
        }
		case VBAEVENT_WORKBOOK_OPEN:
        {
            // process workbook open macro
            // does auto open work here?
            if( !mbOpened )
            {
                processVbaEvent( nEventId, aArgs );
                processVbaEvent( VBAEVENT_WORKBOOK_AUTOOPEN, aArgs );
                mbOpened = sal_True;
                ProcessCompatibleVbaEvent( VBAEVENT_WORKBOOK_ACTIVATE, aArgs );
            }
            // register the window listener.
            if( !m_xVbaEventsListener.is() )
            {
                m_xVbaEventsListener = new VbaEventsListener( this );
                VbaEventsListener* pEventsListener = dynamic_cast< VbaEventsListener* >( m_xVbaEventsListener.get() );
                pEventsListener->startEventsLinstener();
            }
            break;
        }
		case VBAEVENT_WORKBOOK_BEFORECLOSE:
        {
            sal_Bool bCancel = processVbaEvent( nEventId, aArgs ); 
            if( m_xVbaEventsListener.is() && !bCancel )
            {
                VbaEventsListener* pEventsListener = dynamic_cast< VbaEventsListener* >( m_xVbaEventsListener.get() );
                pEventsListener->stopEventsLinstener();
                m_xVbaEventsListener = NULL;
            }
            return bCancel;
        }
		case VBAEVENT_WORKBOOK_BEFOREPRINT:
		case VBAEVENT_WORKBOOK_BEFORESAVE:
		case VBAEVENT_WORKBOOK_WINDOWACTIVATE:
		case VBAEVENT_WORKBOOK_WINDOWDEACTIVATE:
		case VBAEVENT_WORKBOOK_WINDOWRESIZE:
        {
            return processVbaEvent( nEventId, aArgs );
        }
		case VBAEVENT_WORKBOOK_NEWSHEET:
        {
            aArgs[0] >>= nTab;
            if( nTab != INVALID_TAB )
            {
                processVbaEvent( nEventId, aArgs, nTab );
            }
            break;
        }
        default:
            OSL_TRACE( "Invalid Event" );
    }

    return sal_True;
}

::sal_Bool SAL_CALL 
ScVbaEventsHelper::getIgnoreEvents() throw (uno::RuntimeException)
{
    return mbIgnoreEvents;
}

void SAL_CALL 
ScVbaEventsHelper::setIgnoreEvents( ::sal_Bool _ignoreevents ) throw (uno::RuntimeException)
{
    mbIgnoreEvents = _ignoreevents;
}


namespace vbaeventshelper
{
namespace sdecl = comphelper::service_decl;
sdecl::class_<ScVbaEventsHelper, sdecl::with_args<true> > serviceImpl;
extern sdecl::ServiceDecl const serviceDecl(
    serviceImpl,
    "ScVbaEventsHelper",
    "com.sun.star.document.VbaEventsHelper" );
}
