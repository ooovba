#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.17 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..$/..

PRJNAME=sc
TARGET=app

# --- Settings -----------------------------------------------------

.INCLUDE :  scpre.mk
.INCLUDE :  settings.mk
.INCLUDE :  sc.mk
.INCLUDE :  $(PRJ)$/util$/makefile.pmk

# --- Files --------------------------------------------------------

SLOFILES =  \
    $(SLO)$/scmod.obj  \
    $(SLO)$/scmod2.obj  \
    $(SLO)$/scdll.obj  \
    $(SLO)$/typemap.obj  \
    $(SLO)$/transobj.obj \
    $(SLO)$/drwtrans.obj \
    $(SLO)$/lnktrans.obj \
    $(SLO)$/seltrans.obj \
    $(SLO)$/inputhdl.obj \
    $(SLO)$/inputwin.obj \
    $(SLO)$/rfindlst.obj \
    $(SLO)$/uiitems.obj  \
    $(SLO)$/msgpool.obj \
    $(SLO)$/client.obj

EXCEPTIONSFILES= \
    $(SLO)$/drwtrans.obj \
    $(SLO)$/scmod2.obj \
    $(SLO)$/scmod.obj \
    $(SLO)$/client.obj

#LIB3TARGET=$(SLB)$/ysclib.lib
#LIB3OBJFILES=$(SLO)$/sclib.obj

# --- Targets -------------------------------------------------------

.INCLUDE :  target.mk

$(SRS)$/app.srs: $(SOLARINCDIR)$/svx$/globlmn.hrc

