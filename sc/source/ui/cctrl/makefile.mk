#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.7 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..$/..

PRJNAME=sc
TARGET=cctrl
LIBTARGET=NO

# --- Settings -----------------------------------------------------

.INCLUDE :  scpre.mk
.INCLUDE :  settings.mk
.INCLUDE :  sc.mk
.INCLUDE :  $(PRJ)$/util$/makefile.pmk

# --- Files --------------------------------------------------------

EXCEPTIONSFILES= \
    $(SLO)$/tbzoomsliderctrl.obj \
    $(SLO)$/dpcontrol.obj

SLOFILES =	\
        $(SLO)$/popmenu.obj		\
        $(SLO)$/tbinsert.obj	\
        $(SLO)$/cbuttonw.obj	\
		$(SLO)$/dpcontrol.obj	\
        $(SLO)$/editfield.obj	\
        $(EXCEPTIONSFILES)

SRS1NAME=$(TARGET)
SRC1FILES = \
	dpcontrol.src

LIB1TARGET=$(SLB)$/$(TARGET).lib
LIB1OBJFILES= \
        $(SLO)$/popmenu.obj		\
        $(SLO)$/tbinsert.obj	\
        $(SLO)$/cbuttonw.obj	\
		$(SLO)$/dpcontrol.obj	\
        $(SLO)$/tbzoomsliderctrl.obj


# --- Tagets -------------------------------------------------------

.INCLUDE :  target.mk
