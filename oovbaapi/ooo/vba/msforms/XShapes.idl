/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: XShapes.idl,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __ooo_vba_msforms_XShapes_idl__
#define __ooo_vba_msforms_XShapes_idl__

#ifndef __ooo_vba_XHelperInterface_idl__
#include <ooo/vba/XHelperInterface.idl>
#endif
#ifndef  __ooo_vba_XCollection_idl__
#include <ooo/vba/XCollection.idl>
#endif
module ooo {  module vba { module msforms {

interface XShapeRange;
interface XShape;

interface XShapes 
{
    interface ooo::vba::XCollection;
    void SelectAll();
    XShapeRange Range( [in] any shapes );
    /*
    helper::calc
    */
    any AddLine( [in] long StartX, [in] long StartY, [in] long endX, [in] long endY );
    any AddShape([in] long ShapeType, [in] long StartX, [in] long StartY, [in] long endX, [in] long endY );
    any AddTextbox([in] long Orientation, [in] long Left, [in] long Top, [in] long Width, [in] long Height );
};
}; }; };


#endif
