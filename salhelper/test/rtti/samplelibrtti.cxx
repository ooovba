#include "samplelibrtti.hxx"
#include <stdio.h>

// MyClassA =============================================================
void MyClassA::funcA()
{
    printf("MyClassA::funcA \n");
}

void MyClassA::funcB()
{
}

void MyClassA::funcC()
{
}

void MyClassA::funcD()
{
}

// MyClassB ===============================================================
void MyClassB::funcA()
{

    printf("MyClassA::funcB \n");
}

void MyClassB::funcB()
{
}

void MyClassB::funcC()
{
}

void MyClassB::funcD()
{
}
