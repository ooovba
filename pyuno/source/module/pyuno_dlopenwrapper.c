/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: pyuno_dlopenwrapper.c,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <rtl/string.h>

#include <stdlib.h>
#include <string.h>

#ifdef LINUX
#  ifndef __USE_GNU
#  define __USE_GNU
#  endif
#endif
#include <dlfcn.h>

void initpyuno ()
{
    Dl_info dl_info;
    void (*func)(void);

    if (dladdr((void*)&initpyuno, &dl_info) != 0) { 
        void* h = 0;
    size_t len = strrchr(dl_info.dli_fname, '/') - dl_info.dli_fname + 1;
    char* libname = malloc(len + RTL_CONSTASCII_LENGTH( SAL_DLLPREFIX "pyuno" SAL_DLLEXTENSION ) + 1);
        strncpy(libname, dl_info.dli_fname, len);
        strcpy(libname + (len), SAL_DLLPREFIX "pyuno" SAL_DLLEXTENSION);

        h = dlopen (libname, RTLD_NOW | RTLD_GLOBAL);
    free(libname);
        if( h )
        {
            func = (void (*)())dlsym (h, "initpyuno");
            (func) ();
        }
    } 
}
