/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tutil.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SOLTOOLS_TESTSHL_TUTIL_HXX_
#define _SOLTOOLS_TESTSHL_TUTIL_HXX__

#include    <osl/file.hxx>

using namespace std;

#include <vector>

// <namespace_tstutl>
namespace tstutl {

sal_uInt32 getEntriesFromFile( sal_Char* fName, vector< sal_Char* >& entries );
::rtl::OUString cnvrtPth( ::rtl::OString sysPth );

// string copy, cat, len methods
sal_Char* cpy( sal_Char** dest, const sal_Char* src );
sal_Char* cat( const sal_Char* str1, const sal_Char* str2 );
sal_uInt32 ln( const sal_Char* str );

} // </namespace_tstutl>

#endif
