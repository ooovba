/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NeonInputStream.cxx,v $
 * $Revision: 1.11 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_ucb.hxx"
#include "NeonInputStream.hxx"
#include "DAVResourceAccess.hxx"

#include <rtl/memory.h>
#include <com/sun/star/ucb/CommandFailedException.hpp>

#include <comphelper/processfactory.hxx>
#include <com/sun/star/lang/XMultiServiceFactory.hpp>

#include <cstdio>

using namespace cppu;
using namespace com::sun::star::io;
using namespace com::sun::star;
using namespace webdav_ucp;

// -------------------------------------------------------------------
// Constructor
// -------------------------------------------------------------------
NeonInputStream::NeonInputStream()
: m_nLen( 0 ),
  m_nPos( 0 ),
  m_bDirty( sal_False )
{
}

// -------------------------------------------------------------------
// Destructor
// -------------------------------------------------------------------
NeonInputStream::~NeonInputStream( void )
{
}

// -------------------------------------------------------------------
// AddToStream
// Allows the caller to add some data to the "end" of the stream
// -------------------------------------------------------------------
void NeonInputStream::AddToStream( const char * inBuf, sal_Int32 inLen )
{
    OSL_ENSURE( !m_bDirty, "Cannot AddToStream() when it was already written to it." );

    m_aInputBuffer.realloc( sal::static_int_cast<sal_Int32>(m_nLen) + inLen );
    rtl_copyMemory( m_aInputBuffer.getArray() + m_nLen, inBuf, inLen );
    m_nLen += inLen;
}

// -------------------------------------------------------------------
// Associate a URL with this stream
// -------------------------------------------------------------------
void NeonInputStream::SetURL( const rtl::OUString &rURL )
{
    osl::MutexGuard aGuard( m_aLock );

    m_aURL = rURL;
}

// -------------------------------------------------------------------
// queryInterface
// -------------------------------------------------------------------
uno::Any NeonInputStream::queryInterface( const uno::Type &type )
						throw( uno::RuntimeException )
{
    uno::Any aRet = ::cppu::queryInterface( type,
            static_cast< XStream * >( this ),
            static_cast< XInputStream * >( this ),
            static_cast< XOutputStream * >( this ),
            static_cast< XSeekable * >( this ),
            static_cast< XTruncate * >( this ) );
    return aRet.hasValue() ? aRet : OWeakObject::queryInterface( type );
}

// -------------------------------------------------------------------
// getInputStream
// -------------------------------------------------------------------
com::sun::star::uno::Reference< com::sun::star::io::XInputStream > SAL_CALL
NeonInputStream::getInputStream( void )
	throw( com::sun::star::uno::RuntimeException )
{
    return uno::Reference< XInputStream >( this );
}

// -------------------------------------------------------------------
// getOutputStream
// -------------------------------------------------------------------
com::sun::star::uno::Reference< com::sun::star::io::XOutputStream > SAL_CALL
NeonInputStream::getOutputStream( void )
	throw( com::sun::star::uno::RuntimeException )
{
    return uno::Reference< XOutputStream >( this );
}

// -------------------------------------------------------------------
// readBytes
// "Reads" the specified number of bytes from the stream
// -------------------------------------------------------------------
sal_Int32 SAL_CALL NeonInputStream::readBytes(
  ::com::sun::star::uno::Sequence< sal_Int8 >& aData, sal_Int32 nBytesToRead )
        throw( ::com::sun::star::io::NotConnectedException,
               ::com::sun::star::io::BufferSizeExceededException,
               ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    // Work out how much we're actually going to write
    sal_Int32 theBytes2Read = nBytesToRead;
	sal_Int32 theBytesLeft  = sal::static_int_cast<sal_Int32>(m_nLen - m_nPos);
    if ( theBytes2Read > theBytesLeft )
        theBytes2Read = theBytesLeft;

    // Realloc buffer.
    aData.realloc( theBytes2Read );

    // Write the data
    rtl_copyMemory(
		aData.getArray(), m_aInputBuffer.getConstArray() + m_nPos, theBytes2Read );

    // Update our stream position for next time
	m_nPos += theBytes2Read;

    return theBytes2Read;
}

// -------------------------------------------------------------------
// readSomeBytes
// -------------------------------------------------------------------
sal_Int32 SAL_CALL NeonInputStream::readSomeBytes(
 ::com::sun::star::uno::Sequence< sal_Int8 >& aData, sal_Int32 nMaxBytesToRead )
        throw( ::com::sun::star::io::NotConnectedException,
               ::com::sun::star::io::BufferSizeExceededException,
               ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    // Warning: What should this be doing ?
    return readBytes( aData, nMaxBytesToRead );
}

// -------------------------------------------------------------------
// skipBytes
// Moves the current stream position forward
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::skipBytes( sal_Int32 nBytesToSkip )
        throw( ::com::sun::star::io::NotConnectedException,
               ::com::sun::star::io::BufferSizeExceededException,
               ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    m_nPos += nBytesToSkip;
    if ( m_nPos >= m_nLen )
        m_nPos = m_nLen;
}

// -------------------------------------------------------------------
// available
// Returns the number of unread bytes currently remaining on the stream
// -------------------------------------------------------------------
sal_Int32 SAL_CALL NeonInputStream::available(  )
        throw( ::com::sun::star::io::NotConnectedException,
               ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    return sal::static_int_cast<sal_Int32>(m_nLen - m_nPos);
}

// -------------------------------------------------------------------
// closeInput
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::closeInput( void )
         throw( ::com::sun::star::io::NotConnectedException,
                  ::com::sun::star::io::IOException,
                  ::com::sun::star::uno::RuntimeException )
{
}

// -------------------------------------------------------------------
// seek
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::seek( sal_Int64 location )
        throw( ::com::sun::star::lang::IllegalArgumentException,
               ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    if ( location < 0 )
        throw ::com::sun::star::lang::IllegalArgumentException();

    if ( location <= m_nLen )
        m_nPos = location;
    else
        throw ::com::sun::star::lang::IllegalArgumentException();
}

// -------------------------------------------------------------------
// getPosition
// -------------------------------------------------------------------
sal_Int64 SAL_CALL NeonInputStream::getPosition()
        throw( ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    return m_nPos;
}

// -------------------------------------------------------------------
// getLength
// -------------------------------------------------------------------
sal_Int64 SAL_CALL NeonInputStream::getLength()
        throw( ::com::sun::star::io::IOException,
               ::com::sun::star::uno::RuntimeException )
{
    return m_nLen;
}

// -------------------------------------------------------------------
// writeBytes
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::writeBytes( const com::sun::star::uno::Sequence< sal_Int8 >& aData )
	throw( com::sun::star::io::NotConnectedException,
	       com::sun::star::io::BufferSizeExceededException,
	       com::sun::star::io::IOException,
	       com::sun::star::uno::RuntimeException)
{
#if OSL_DEBUG_LEVEL > 0
    fprintf( stderr, "WebDAV: writeBytes()\n" );
#endif

    sal_Int32 nDataLen = aData.getLength();
    OSL_ASSERT( nDataLen >= 0 );

    // Anything to do?
    if ( nDataLen == 0 )
        return;

    // Update the length of the stream & size of the buffer
    if ( m_nLen < m_nPos + nDataLen )
    {
        m_nLen = m_nPos + nDataLen;
        if ( m_aInputBuffer.getLength() < m_nLen )
            m_aInputBuffer.realloc( sal::static_int_cast<sal_Int32>( m_nLen ) );
    }

    rtl_copyMemory( m_aInputBuffer.getArray() + m_nPos, aData.getConstArray(), nDataLen );
    m_nPos += nDataLen;

    m_bDirty = sal_True;
}

// -------------------------------------------------------------------
// flush
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::flush( void )
	throw( NotConnectedException, BufferSizeExceededException,
	       IOException, uno::RuntimeException )
{
    if ( m_bDirty )
    {
#if OSL_DEBUG_LEVEL > 0
        fprintf( stderr, "WebDAV: flush(), saving the changed file.\n" );
#endif
        // FIXME It's really hacky to create the new session
        // But so far it seems I have no other chance...
        uno::Reference< lang::XMultiServiceFactory > xFactory( ::comphelper::getProcessServiceFactory(), uno::UNO_QUERY );
        rtl::Reference< DAVSessionFactory > rDAVFactory( new DAVSessionFactory() );
        
        DAVResourceAccess aResourceAccess( xFactory, rDAVFactory, m_aURL );
        
        try {
            aResourceAccess.PUT( reinterpret_cast<const char*>( m_aInputBuffer.getConstArray() ), m_nLen,
                    DAVResourceAccess::createCommandEnvironment() );
        }
        catch ( DAVException & e )
        {
            throw ucb::CommandFailedException(
                    e.getData(),
                    uno::Reference< uno::XInterface >(),
                    uno::makeAny( e.getData() ) );
        }

        m_bDirty = sal_False;
    }
}
        
// -------------------------------------------------------------------
// closeOutput
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::closeOutput( void )
	throw( com::sun::star::io::NotConnectedException,
	       com::sun::star::io::IOException,
	       com::sun::star::uno::RuntimeException )
{
    if ( m_bDirty )
    {
#if OSL_DEBUG_LEVEL > 0
        fprintf( stderr, "WebDAV: TODO write on closeOutput(), the stream is dirty!\n" );
#endif
    }
}

// -------------------------------------------------------------------
// truncate
// -------------------------------------------------------------------
void SAL_CALL NeonInputStream::truncate( void )
	throw( com::sun::star::io::IOException,
	       com::sun::star::uno::RuntimeException )
{
#if OSL_DEBUG_LEVEL > 0
    fprintf( stderr, "WebDAV: truncate()\n" );
#endif

    if ( m_nLen > 0 )
    {
        m_nLen = m_nPos = 0;
        m_bDirty = sal_True;
    }
}
