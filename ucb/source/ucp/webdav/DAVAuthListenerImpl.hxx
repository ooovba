/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: DAVAuthListenerImpl.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _DAVAUTHLISTENERIMPL_HXX_
#define _DAVAUTHLISTENERIMPL_HXX_

#include "DAVAuthListener.hxx"


namespace webdav_ucp
{
    
//=========================================================================

//=========================================================================
//=========================================================================
//
// class DAVAuthListenerImpl.
//
//=========================================================================
//=========================================================================
    
    
    class DAVAuthListener_Impl : public DAVAuthListener
    {
    public:
        
        DAVAuthListener_Impl(
            const com::sun::star::uno::Reference<
                com::sun::star::ucb::XCommandEnvironment>& xEnv,
            const ::rtl::OUString & inURL )
            : m_xEnv( xEnv ), m_aURL( inURL ) 
        {
        }
        
        virtual int authenticate( const ::rtl::OUString & inRealm,
                                  const ::rtl::OUString & inHostName,
                                  ::rtl::OUString & inoutUserName,
                                  ::rtl::OUString & outPassWord,
                                  sal_Bool bAllowPersistentStoring,
                                  sal_Bool bCanUseSystemCredentials );
    private:
        
        const com::sun::star::uno::Reference<
            com::sun::star::ucb::XCommandEnvironment > m_xEnv;
        const rtl::OUString m_aURL;

        rtl::OUString m_aPrevPassword;
        rtl::OUString m_aPrevUsername;
    };

}

#endif
