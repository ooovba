/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cacheserv.cxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_ucb.hxx"
#include <com/sun/star/lang/XMultiServiceFactory.hpp>
#include <com/sun/star/lang/XSingleServiceFactory.hpp>
#include <com/sun/star/registry/XRegistryKey.hpp>
#include <cachedcontentresultset.hxx>
#include <cachedcontentresultsetstub.hxx>
#include <cacheddynamicresultset.hxx>
#include <cacheddynamicresultsetstub.hxx>

using namespace rtl;
using namespace com::sun::star::uno;
using namespace com::sun::star::lang;
using namespace com::sun::star::registry;

//=========================================================================
static sal_Bool writeInfo( void * pRegistryKey,
                           const OUString & rImplementationName,
                              Sequence< OUString > const & rServiceNames )
{
    OUString aKeyName( OUString::createFromAscii( "/" ) );
    aKeyName += rImplementationName;
    aKeyName += OUString::createFromAscii( "/UNO/SERVICES" );

    Reference< XRegistryKey > xKey;
    try
    {
        xKey = static_cast< XRegistryKey * >(
                                    pRegistryKey )->createKey( aKeyName );
    }
    catch ( InvalidRegistryException const & )
    {
    }

    if ( !xKey.is() )
        return sal_False;

    sal_Bool bSuccess = sal_True;

    for ( sal_Int32 n = 0; n < rServiceNames.getLength(); ++n )
    {
        try
        {
            xKey->createKey( rServiceNames[ n ] );
        }
        catch ( InvalidRegistryException const & )
        {
            bSuccess = sal_False;
            break;
        }
    }
    return bSuccess;
}

//=========================================================================
extern "C" void SAL_CALL component_getImplementationEnvironment(
    const sal_Char ** ppEnvTypeName, uno_Environment ** )
{
    *ppEnvTypeName = CPPU_CURRENT_LANGUAGE_BINDING_NAME;
}

//=========================================================================
extern "C" sal_Bool SAL_CALL component_writeInfo( void *, void * pRegistryKey )
{
    return pRegistryKey &&

    //////////////////////////////////////////////////////////////////////
    // CachedContentResultSetFactory.
    //////////////////////////////////////////////////////////////////////

    writeInfo( pRegistryKey,
       CachedContentResultSetFactory::getImplementationName_Static(),
       CachedContentResultSetFactory::getSupportedServiceNames_Static() ) &&

    //////////////////////////////////////////////////////////////////////
    // CachedContentResultSetStubFactory.
    //////////////////////////////////////////////////////////////////////

    writeInfo( pRegistryKey,
       CachedContentResultSetStubFactory::getImplementationName_Static(),
       CachedContentResultSetStubFactory::getSupportedServiceNames_Static() ) &&

    //////////////////////////////////////////////////////////////////////
    // CachedDynamicResultSetFactory.
    //////////////////////////////////////////////////////////////////////

    writeInfo( pRegistryKey,
       CachedDynamicResultSetFactory::getImplementationName_Static(),
       CachedDynamicResultSetFactory::getSupportedServiceNames_Static() ) &&

    //////////////////////////////////////////////////////////////////////
    // CachedDynamicResultSetStubFactory.
    //////////////////////////////////////////////////////////////////////

    writeInfo( pRegistryKey,
       CachedDynamicResultSetStubFactory::getImplementationName_Static(),
       CachedDynamicResultSetStubFactory::getSupportedServiceNames_Static() );
}

//=========================================================================
extern "C" void * SAL_CALL component_getFactory(
    const sal_Char * pImplName, void * pServiceManager, void * )
{
    void * pRet = 0;

    Reference< XMultiServiceFactory > xSMgr(
            reinterpret_cast< XMultiServiceFactory * >( pServiceManager ) );
    Reference< XSingleServiceFactory > xFactory;

    //////////////////////////////////////////////////////////////////////
    // CachedContentResultSetFactory.
    //////////////////////////////////////////////////////////////////////

    if ( CachedContentResultSetFactory::getImplementationName_Static().
                compareToAscii( pImplName ) == 0 )
    {
        xFactory = CachedContentResultSetFactory::createServiceFactory( xSMgr );
    }

    //////////////////////////////////////////////////////////////////////
    // CachedContentResultSetStubFactory.
    //////////////////////////////////////////////////////////////////////

    else if ( CachedContentResultSetStubFactory::getImplementationName_Static().
                compareToAscii( pImplName ) == 0 )
    {
        xFactory = CachedContentResultSetStubFactory::createServiceFactory( xSMgr );
    }

    //////////////////////////////////////////////////////////////////////
    // CachedDynamicResultSetFactory.
    //////////////////////////////////////////////////////////////////////

    else if ( CachedDynamicResultSetFactory::getImplementationName_Static().
                compareToAscii( pImplName ) == 0 )
    {
        xFactory = CachedDynamicResultSetFactory::createServiceFactory( xSMgr );
    }

    //////////////////////////////////////////////////////////////////////
    // CachedDynamicResultSetStubFactory.
    //////////////////////////////////////////////////////////////////////

    else if ( CachedDynamicResultSetStubFactory::getImplementationName_Static().
                compareToAscii( pImplName ) == 0 )
    {
        xFactory = CachedDynamicResultSetStubFactory::createServiceFactory( xSMgr );
    }

    //////////////////////////////////////////////////////////////////////

    if ( xFactory.is() )
    {
        xFactory->acquire();
        pRet = xFactory.get();
    }

    return pRet;
}

