/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: file_python.scp,v $
 * $Revision: 1.19 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "macros.inc"

File gid_File_Lib_Pyuno
    TXT_FILE_BODY;
  #ifdef UNX
    Name = STRING(CONCAT2(libpyuno,UNXSUFFIX));
  #else
    Name = "pyuno.pyd";
  #endif
    Dir = gid_Dir_Program;
    Styles = (PACKED);
End

File gid_File_Lib_Pythonloader
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
  #ifdef UNX
    Name = STRING(CONCAT2(pythonloader.uno,UNXSUFFIX));
  #else
    Name = "pythonloader.uno.dll";
  #endif
    RegistryID = gid_Starregistry_Services_Rdb;
    NativeServicesURLPrefix = "vnd.sun.star.expand:$OOO_BASE_DIR/program/";
    Styles = (PACKED,UNO_COMPONENT);
End

File gid_File_Py_Unohelper
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = "unohelper.py";
    Styles = (PACKED);
End

File gid_File_Py_Officehelper
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = "officehelper.py";
    Styles = (PACKED);
End

File gid_File_Py_Uno
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = "uno.py";
    Styles = (PACKED);
End

File gid_File_Py_Pythonloader
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = "pythonloader.py";
    Styles = (PACKED);
End

#ifndef SYSTEM_PYTHON
File gid_File_Py_Python_Core
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = STRING(CONCAT3(python-core-,PYVERSION,.zip));
    Styles = (ARCHIVE);
End

#ifdef UNX
File gid_File_Py_Python_Bin
    BIN_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = "python.bin";
    Styles = (PACKED);
End
#endif
#endif

// Scripting Framework Python script proxy

File gid_File_Py_Pythonscript
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = "pythonscript.py";
    RegistryID = gid_Starregistry_Services_Rdb;
    Styles = (PACKED,UNO_COMPONENT);
End
 
//Scripting Framework Python example scripts

File gid_File_Scripts_Python
    TXT_FILE_BODY; 
    Styles = (ARCHIVE);
    Dir = gid_Dir_Share_Scripts;
    Name = "ScriptsPython.zip";
End

// Scripting Framework Python configuration settings

File gid_File_Registry_Spool_Oo_Scripting_Python_Xcu
    TXT_FILE_BODY;
    Styles = (PACKED);
    Dir = gid_Dir_Share_Registry_Modules_Oo_Office_Scripting;
    Name = "/registry/spool/org/openoffice/Office/Scripting-python.xcu";
End

#ifndef SYSTEM_PYTHON
File gid_File_Lib_Python_So
    TXT_FILE_BODY;
    Dir = gid_Dir_Program;
    Name = STRING(PY_FULL_DLL_NAME);
    Styles = (PACKED);
End
#ifdef WNT
File gid_File_Lib_Python_So_Brand // Fix for system-python-problem on windows
    TXT_FILE_BODY;
    Dir = gid_Brand_Dir_Program;
    Name = STRING(PY_FULL_DLL_NAME);
    Styles = (PACKED);
End
#endif
#endif

#ifdef UNX

// pyuno.so even on Mac OS X, because it is a python module
File gid_File_Pyuno
    TXT_FILE_BODY;
    Name = "pyuno.so";
    Dir = gid_Dir_Program;
    Styles = (PACKED);
End

#endif

