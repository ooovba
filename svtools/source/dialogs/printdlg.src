/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: printdlg.src,v $
 * $Revision: 1.49 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "printdlg.hrc"

#define IMAGE_MAGENTA_MASK Color { Red = 0xFFFF; Green = 0x0000; Blue = 0xFFFF; }

ModalDialog DLG_SVT_PRNDLG_PRINTDLG
{
    SVLook = TRUE ;
    OutputSize = TRUE ;
    Moveable = TRUE ;
    Size = MAP_APPFONT ( 265 , 210 ) ;
    Text [ en-US ] = "Print" ;
    FixedLine FL_PRINTER
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 253 , 8 ) ;
        Text [ en-US ] = "Printer" ;
    };
    FixedText FT_NAME
    {
        Pos = MAP_APPFONT ( 12 , 14 ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Name" ;
    };
    ListBox LB_NAMES
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 60 , 13 ) ;
        Size = MAP_APPFONT ( 130 , 80 ) ;
        DropDown = TRUE ;
        Sort = TRUE ;
    };
    PushButton BTN_PROPERTIES
    {
        Pos = MAP_APPFONT ( 193 , 12 ) ;
        Size = MAP_APPFONT ( 60 , 14 ) ;
        Text [ en-US ] = "Propert~ies..." ;
    };
    FixedText FT_STATUS
    {
        Pos = MAP_APPFONT ( 12 , 29 ) ;
        Size = MAP_APPFONT ( 45 , 8 ) ;
        Text [ en-US ] = "Status" ;
    };
    FixedText FI_STATUS
    {
        Pos = MAP_APPFONT ( 60 , 29 ) ;
        Size = MAP_APPFONT ( 193 , 8 ) ;
    };
    FixedText FT_TYPE
    {
        Pos = MAP_APPFONT ( 12 , 40 ) ;
        Size = MAP_APPFONT ( 45 , 8 ) ;
        Text [ en-US ] = "Type" ;
    };
    FixedText FI_TYPE
    {
        Pos = MAP_APPFONT ( 60 , 40 ) ;
        Size = MAP_APPFONT ( 193 , 8 ) ;
    };
    FixedText FT_LOCATION
    {
        Pos = MAP_APPFONT ( 12 , 51 ) ;
        Size = MAP_APPFONT ( 45 , 8 ) ;
        Text [ en-US ] = "Location" ;
    };
    FixedText FI_LOCATION
    {
        Pos = MAP_APPFONT ( 60 , 51 ) ;
        Size = MAP_APPFONT ( 193 , 8 ) ;
    };
    FixedText FT_COMMENT
    {
        Pos = MAP_APPFONT ( 12 , 62 ) ;
        Size = MAP_APPFONT ( 45 , 8 ) ;
        Text [ en-US ] = "Comment" ;
    };
    FixedText FI_COMMENT
    {
        Pos = MAP_APPFONT ( 60 , 62 ) ;
        Size = MAP_APPFONT ( 193 , 8 ) ;
    };
    FixedText FI_FAXNO
    {
        Pos = MAP_APPFONT ( 12 , 75 ) ;
        Size = MAP_APPFONT ( 45 , 8 ) ;
        Text [ en-US ] = "Fax number";
    };
    Edit EDT_FAXNO
    {
        Border = TRUE;
        Pos = MAP_APPFONT ( 60 , 73 );
        Size = MAP_APPFONT ( 188 , 12 );
    };
    CheckBox CBX_FILEPRINT
    {
        Pos = MAP_APPFONT ( 12 , 73 ) ;
        Size = MAP_APPFONT ( 75 , 10 ) ;
        Text [ en-US ] = "Print to file" ;
    };
    FixedText FI_PRINTFILE
    {
        Pos = MAP_APPFONT ( 90 , 74 ) ;
        Size = MAP_APPFONT ( 163 , 8 ) ;
    };
    /*!!!
    PushButton BTN_BROWSE
    {
        Pos = MAP_APPFONT ( 234 , 75 ) ;
        Size = MAP_APPFONT ( 14 , 14 ) ;
        Text = "~..." ;
        Hide = TRUE ;
    };
    */
    FixedLine FL_PRINT
    {
        Pos = MAP_APPFONT ( 6 , 91 ) ;
        Size = MAP_APPFONT ( 117 , 8 ) ;
        Text [ en-US ] = "Print" ;
    };
    RadioButton RBT_ALL_SHEETS
    {
        Pos = MAP_APPFONT ( 12 , 102 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "All sheets" ;
    };
    RadioButton RBT_SELECTED_SHEETS
    {
        Pos = MAP_APPFONT ( 12 , 115 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "Selected sheets" ;
    };
    RadioButton RBT_SELECTED_CELLS
    {
        Pos = MAP_APPFONT ( 12 , 128 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "Selected cells" ;
    };
    FixedLine FL_PRINTRANGE
    {
        Pos = MAP_APPFONT ( 6 , 141 ) ;
        Size = MAP_APPFONT ( 117 , 8 ) ;
        Text [ en-US ] = "Print range" ;
    };
    RadioButton RBT_ALL
    {
        Pos = MAP_APPFONT ( 12 , 152 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "All pages" ;
    };
    RadioButton RBT_PAGES
    {
        Pos = MAP_APPFONT ( 12 , 165 ) ;
        Size = MAP_APPFONT ( 50 , 10 ) ;
        Text [ en-US ] = "Pages" ;
    };
    Edit EDT_PAGES
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 65 , 164 ) ;
        Size = MAP_APPFONT ( 52 , 12 ) ;
    };
    RadioButton RBT_SELECTION
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 12 , 179 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "Selection" ;
    };
    FixedLine FL_SEPCOPIESRANGE
    {
        Pos = MAP_APPFONT( 126, 102 );
        Size = MAP_APPFONT( 1, 74 );
        Vert = TRUE;
    };
    FixedLine FL_COPIES
    {
        Pos = MAP_APPFONT ( 129 , 91 ) ;
        Size = MAP_APPFONT ( 130 , 8 ) ;
        Text [ en-US ] = "Copies" ;
    };
    FixedText FT_COPIES
    {
        Pos = MAP_APPFONT ( 135 , 104 ) ;
        Size = MAP_APPFONT ( 63 , 8 ) ;
        Text [ en-US ] = "Number of copies" ;
    };
    NumericField NUM_COPIES
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 201 , 102 ) ;
        Size = MAP_APPFONT ( 33 , 12 ) ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Minimum = 1 ;
        Maximum = 9999 ;
        StrictFormat = TRUE ;
        First = 1 ;
        Last = 9999 ;
    };
    CheckBox CBX_COLLATE
    {
        Pos = MAP_APPFONT ( 201 , 123 ) ;
        Size = MAP_APPFONT ( 60 , 10 ) ;
        Text [ en-US ] = "Co~llate" ;
    };
    FixedImage IMG_COLLATE
    {
        Pos = MAP_APPFONT ( 132 , 117 ) ;
        Size = MAP_APPFONT ( 67 , 22 ) ;
        Hide = TRUE ;
    };
    FixedImage IMG_NOT_COLLATE
    {
        Pos = MAP_APPFONT ( 132 , 117 ) ;
        Size = MAP_APPFONT ( 67 , 22 ) ;
        Hide = TRUE ;
    };
    FixedLine FL_SEPBUTTONLINE
    {
        Pos = MAP_APPFONT( 0, 179 );
        Size = MAP_APPFONT( 265, 8 );
    };
    PushButton BTN_OPTIONS
    {
        Hide = TRUE ;
        Pos = MAP_APPFONT ( 6 , 190 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Options..." ;
    };
    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 100 , 190 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        DefButton = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 153 , 190 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 209 , 190 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    String STR_ALLFILTER
    {
        Text [ en-US ] = "<All>";
    };
};

Image RID_IMG_PRNDLG_COLLATE
{
    ImageBitmap = Bitmap { File = "collate.bmp" ; };
    MaskColor = IMAGE_MAGENTA_MASK ;
};

Image RID_IMG_PRNDLG_NOCOLLATE
{
    ImageBitmap = Bitmap { File = "ncollate.bmp" ; };
    MaskColor = IMAGE_MAGENTA_MASK ;
};

Image RID_IMG_PRNDLG_COLLATE_HC
{
    ImageBitmap = Bitmap { File = "collate_h.bmp" ; };
    MaskColor = IMAGE_MAGENTA_MASK ;
};

Image RID_IMG_PRNDLG_NOCOLLATE_HC
{
    ImageBitmap = Bitmap { File = "ncollate_h.bmp" ; };
    MaskColor = IMAGE_MAGENTA_MASK ;
};
































