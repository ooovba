/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: colrdlg.src,v $
 * $Revision: 1.29 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "colrdlg.hrc"
#define DIFF 3
ModalDialog DLG_COLOR
{
    OutputSize = TRUE ;
    SVLook = TRUE ;
    Size = MAP_APPFONT ( 260 , 165 + DIFF ) ;
    Moveable = TRUE ;
    Text [ en-US ] = "Color" ;
    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 205 , 6 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        DefButton = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 205 , 23 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 205 , 43 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    Control VAL_SET_COLOR
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 6 ) ;
        Size = MAP_APPFONT ( 91 , 100 ) ;
        TabStop = TRUE ;
    };
    Control CTL_COLOR
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 100 , 6 ) ;
        Size = MAP_APPFONT ( 100 , 100 ) ;
        TabStop = TRUE ;
    };
    FixedText FT_CYAN
    {
        Pos = MAP_APPFONT ( 6 , 110 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "~Cyan" ;
    };
    FixedText FT_MAGENTA
    {
        Pos = MAP_APPFONT ( 6 , 123 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "~Magenta" ;
    };
    FixedText FT_YELLOW
    {
        Pos = MAP_APPFONT ( 6 , 136 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "~Yellow" ;
    };
    FixedText FT_KEY
    {
        Pos = MAP_APPFONT ( 6 , 149 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "~Key" ;
    };
    MetricField NUM_CYAN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 42 , 109 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 100 ;
        Last = 100 ;
        Unit = FUNIT_CUSTOM ;
		CustomUnitText = "%" ;
    };
    MetricField NUM_MAGENTA
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 42 , 122 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 100 ;
        Last = 100 ;
        Unit = FUNIT_CUSTOM ;
		CustomUnitText = "%" ;
    };
    MetricField NUM_YELLOW
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 42 , 135 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 100 ;
        Last = 100 ;
        Unit = FUNIT_CUSTOM ;
		CustomUnitText = "%" ;
    };
    MetricField NUM_KEY
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 42 , 148 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 100 ;
        Last = 100 ;
        Unit = FUNIT_CUSTOM ;
		CustomUnitText = "%" ;
    };
    FixedText FT_RED
    {
        Pos = MAP_APPFONT ( 72 , 123 + DIFF ) ;
        Size = MAP_APPFONT ( 33 , 10 ) ;
        Text [ en-US ] = "~Red" ;
    };
    FixedText FT_GREEN
    {
        Pos = MAP_APPFONT ( 72 , 136 + DIFF ) ;
        Size = MAP_APPFONT ( 33 , 10 ) ;
        Text [ en-US ] = "~Green" ;
    };
    FixedText FT_BLUE
    {
        Pos = MAP_APPFONT ( 72 , 149 + DIFF ) ;
        Size = MAP_APPFONT ( 33 , 10 ) ;
        Text [ en-US ] = "~Blue" ;
    };
    NumericField NUM_RED
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 106 , 122 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 255 ;
        Last = 255 ;
    };
    NumericField NUM_GREEN
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 106 , 135 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 255 ;
        Last = 255 ;
    };
    NumericField NUM_BLUE
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 106 , 148 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 255 ;
        Last = 255 ;
    };
    FixedText FT_HUE
    {
        Pos = MAP_APPFONT ( 135 , 123 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "H~ue" ;
    };
    NumericField NUM_HUE
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 171 , 122 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 359 ;
        Last = 359 ;
    };
    FixedText FT_SATURATION
    {
        Pos = MAP_APPFONT ( 135 , 136 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "~Saturation" ;
    };
    NumericField NUM_SATURATION
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 171 , 135 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 100 ;
        Last = 100 ;
    };
    FixedText FT_LUMINANCE
    {
        Pos = MAP_APPFONT ( 135 , 149 + DIFF ) ;
        Size = MAP_APPFONT ( 34 , 10 ) ;
        Text [ en-US ] = "Bright~ness" ;
    };
    NumericField NUM_LUMINANCE
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 171 , 148 + DIFF ) ;
        Size = MAP_APPFONT ( 26 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 100 ;
        Last = 100 ;
    };
    PushButton BTN_1
    {
        Pos = MAP_APPFONT ( 80 , 109 ) ;
        Size = MAP_APPFONT ( 17 , 12 ) ;
        Text = "~<--" ;
        TabStop = TRUE ;
    };
    PushButton BTN_2
    {
        Pos = MAP_APPFONT ( 100 , 109 ) ;
        Size = MAP_APPFONT ( 17 , 12 ) ;
        Text = "--~>" ;
        TabStop = TRUE ;
    };
    Control CTL_PREVIEW_OLD
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 200 , 109 ) ;
        Size = MAP_APPFONT ( 26 , 51 + DIFF ) ;
        TabStop = TRUE ;
    };
    Control CTL_PREVIEW
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 229 , 109 ) ;
        Size = MAP_APPFONT ( 26 , 51 + DIFF ) ;
        TabStop = TRUE ;
    };
};
































