/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: textundo.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _TEXTUNDO_HXX
#define _TEXTUNDO_HXX

#include <svtools/undo.hxx>

class TextEngine;

class TextUndoManager : public SfxUndoManager
{
    TextEngine*		mpTextEngine;

protected:

    void			UndoRedoStart();
    void			UndoRedoEnd();

    TextView*		GetView() const { return mpTextEngine->GetActiveView(); }

public:
                    TextUndoManager( TextEngine* pTextEngine );
                    ~TextUndoManager();

    using SfxUndoManager::Undo;
    virtual BOOL	Undo( USHORT nCount=1 );
    using SfxUndoManager::Redo;
    virtual BOOL	Redo( USHORT nCount=1 );

};

class TextUndo : public SfxUndoAction
{
private:
    USHORT 				mnId;
    TextEngine*			mpTextEngine;

protected:

    TextView*			GetView() const { return mpTextEngine->GetActiveView(); }
    void				SetSelection( const TextSelection& rSel );

    TextDoc*			GetDoc() const { return mpTextEngine->mpDoc; }
    TEParaPortions*		GetTEParaPortions() const { return mpTextEngine->mpTEParaPortions; }

public:
                        TYPEINFO();
                        TextUndo( USHORT nId, TextEngine* pTextEngine );
    virtual 			~TextUndo();

    TextEngine*			GetTextEngine() const	{ return mpTextEngine; }

    virtual void		Undo() 		= 0;
    virtual void		Redo()		= 0;

    virtual XubString	GetComment() const;
    virtual USHORT		GetId() const;
};

#endif // _TEXTUNDO_HXX
