#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.15.108.1 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..

PRJNAME=svtools
TARGET=edit

# --- Settings -----------------------------------------------------

.INCLUDE :  settings.mk
.INCLUDE :  $(PRJ)$/util$/svt.pmk

# --- Files --------------------------------------------------------

SLOFILES=   \
            $(SLO)$/textdata.obj	\
            $(SLO)$/textdoc.obj		\
            $(SLO)$/texteng.obj		\
            $(SLO)$/textundo.obj	\
            $(SLO)$/textview.obj	\
            $(SLO)$/txtattr.obj		\
            $(SLO)$/xtextedt.obj	\
            $(SLO)$/sychconv.obj	\
            $(SLO)$/svmedit.obj		\
            $(SLO)$/svmedit2.obj    \
            $(SLO)$/textwindowpeer.obj	\
            $(SLO)$/syntaxhighlight.obj \
        $(SLO)$/editsyntaxhighlighter.obj

EXCEPTIONSFILES=	\
            $(SLO)$/textview.obj	\
            $(SLO)$/textdoc.obj     \
            $(SLO)$/texteng.obj     \
            $(SLO)$/textwindowpeer.obj

# --- Targets ------------------------------------------------------

.INCLUDE :  target.mk
