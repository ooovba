'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: wizard_firsttime.inc,v $
'*
'* $Revision: 1.2 $
'*
'* last change: $Author: rt $ $Date: 2008-08-01 09:48:30 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'*  owner : joerg.skottke@sun.com
'*
'*  short description : Updatetest for the first time wizard
'*
'\******************************************************************************

testcase tUpdtWizardFirsttime

    '///<h1>Updatetest for the First-Time / Migration-Wizard</h1>
    
    dim iClick as integer
    dim cFirstName as string
    dim cLastName as string
    dim cInitials as string
    
    cFirstName = "Tom"
    cLastName = "Cat"
    cInitials = "TC"
    
    dim cQuickstarterPath as string
        cQuickStarterPath = gNetzOfficePath & "program\quickstart.exe"
    
    '///<ul>
    
    '///+<li>Make sure exactly one single writer document is open</li>
    hInitSingleDoc()
    
    '///+<li>Open the wizard</li>
    printlog( "Open the Wizard" )
    FirstTimeWizard
    
    ' give the wizard a maximum of 2 seconds to open, it's a serve-url and
    ' should open almost instantly
    kontext "WelcomeDialog"
    if ( WelcomeDialog.exists( 2 ) ) then
        printlog( "The Wizard is open" )
        call dialogtest( WelcomeDialog )
        
        '///+<li>Click the 'next' button to get to the second page</li>
        printlog( "click 'next'")
        NextBtn.click()
    endif
    
    

    
    
    '///+<li>Check the license page</li>
    printlog( "Check the license page" )
    kontext "TabFirstStartLicense"
    if ( TabFirstStartLicense.exists( 2 ) ) then
        printlog( "License page is visible" )
        call dialogtest( TabFirstStartLicense )

    
        '///+<li>Enable the next-button by 'reading' the licensetext</li>
        printlog( "scroll down (button!) the licensetext to enable the >>-button" )
        for iClick = 1 to 50
    
            kontext "TabFirstStartLicense"
            if ( ScrollDown.isEnabled( 1 ) ) then
                ScrollDown.click()
            else
                kontext "WelcomeDialog"
                if ( NextBtn.isEnabled() ) then
                    printlog( "Needed " & iClick & " clicks on 'scroll down' " )
                    exit for
                else
                    warnlog( "Both Scroll-button and Next-button are disabled" )
                    goto endsub
                endif
            endif
        next iClick
    
        sleep( 1 )
    
        kontext "WelcomeDialog"
        if ( WelcomeDialog.exists( 2 ) ) then
            NextBtn.click()
        else
            warnlog( "Cannot access welcome dialog" )
        endif
    endif
    
    
    
    '///+<li>Data Migration Page</li>
    printlog( "Handle Data-Migration page" )
    kontext "TabPersonalDataMigration"
    if ( TabPersonalDataMigration.exists( 2 ) ) then
        call dialogtest( TabPersonalDataMigration )
        printlog( "The data migration page is visible" )
        TransferPersonalData.uncheck()
        '///+<li>Click the 'next' button to get to the next page</li>    
        kontext "WelcomeDialog"
        NextBtn.click()
    endif
    
    
    
    '///+<li>Page to enter personal data</li>
    printlog( "The personal data page should show up" )
    Kontext "TabFirstStartUser"
    if ( TabFirstStartUser.exists( 2 ) ) then
    
        call dialogtest( TabFirstStartUser )
        
        Kontext "TabFirstStartUser"
        printlog( "Enter lastname, name and initials" )
        FirstName.setText( cFirstname )
        LastName.setText( cLastName )
        Initials.setText( cInitials )
        
        kontext "WelcomeDialog"
        NextBtn.click()
    else
        warnlog( "Could not access TabFirstStartUser" )
    endif
    
    kontext "TabFirstStartOnlineUpdate"
    if ( TabFirstStartOnlineUpdate.exists( 2 ) ) then
        call dialogtest( TabFirstStartOnlineUpdate )
        CheckForUpdates.unCheck()
        kontext "WelcomeDialog"
        NextBtn.click()
    else
        warnlog( "Online Update Page is missing" )
    endif
        
    
    '///+<li>Handle the welcome Dialog - Internet Update page which should NOT appear</li>
    Kontext "TabFirstStartRegistration"
    if ( TabFirstStartRegistration.exists( 3 ) ) then
        printlog( "Registration page visible." )
    else
	    qaerrorlog( "An additional page appears on rerun of wizard" )
        kontext "WelcomeDialog"
        NextBtn.click()
    endif
 
    '///+<li>Last page: Registration. Choose not to register</li>
    printlog( "Registration page: " )
    Kontext "TabFirstStartRegistration"
    if ( TabFirstStartRegistration.exists( 2 ) ) then
        printlog( "Page is present." )
        call dialogtest( TabFirstStartRegistration )
        printlog( "Choose not to register" )
        DoNotWantRegister.check()
        
        '///+<li>Finally close the wizard</li>
        printlog( "close the wizard" )
    	kontext "WelcomeDialog"
     	WelcomeDialog.ok()
    endif
    
    '///+<li>Verify that the data made it into tools/options instantly</li>
    '///<ol>
    printlog( "Verify that the userdata made it to tools/options" )
    ToolsOptions
    hToolsOptions( "StarOffice" , "UserData" )

    '///+<li>First name</li>    
    if ( Vorname.getText() <> cFirstname ) then
        warnlog( "First name is not transferred to Tools/options/userdata" )
    endif
   
    '///+<li>Last name</li>
    if ( ZuName.getText() <> cLastName ) then
        warnlog( "Last name is not transferred to Tools/options/userdata" )
    endif
    
    '///+<li>Initials</li>
    if ( Kuerzel.getText() <> cInitials ) then
        warnlog( "Initials are not transferred to Tools/options/userdata" )
    endif
    
    '///+<li>Go to the OpenOffice.org / Memory page</li>
    hToolsOptions( "StarOffice", "Memory" )
    
    if ( LoadQuickstarter.exists() ) then
        if ( LoadQuickstarter.isEnabled() ) then
            printlog( "Quickstarter checkbox is enabled" )
            if ( LoadQuickstarter.isChecked() ) then
                printlog( "Quickstart is activated, turning it off." )
                LoadQuickstarter.unCheck()
                printlog( "Closing Tools/Options" )
                Kontext "OptionenDlg"
                OptionenDlg.cancel()
                printlog( "Restarting program" )
                call ExitRestartTheOffice()
            else
                printlog( "Quickstarter is not turned on." )
            endif
        else
            printlog( "Quickstarter checkbox is not enabled for this system" )
        endif
    else
        if ( gPlatGroup <> "unx" ) then
            if ( dir( cQuickStarterPath ) <> "" ) then
                warnlog( "Quickstarter checkbox is missing on options page" )
            else
                printlog( "Quickstarter is not installed/no checkbox present" )
            endif
        endif
    endif
    '///</ol>
        
    Kontext "OptionenDlg"
    if ( OptionenDlg.exists() ) then
        OptionenDlg.cancel()
    endif
    
    ' due to issue i105248 the Quickstart disabler needs to get called again, because the First Start Wizard resets the veto
    call hDisableQuickstarterAPI()

    do while( getDocumentCount() > 0 )
        call hCloseDocument()
    loop
   
    '///</ul>
endcase

