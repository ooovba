'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: help_compare_applications.inc,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 12:18:14 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* owner : joerg.skottke@sun.com
'*
'* short description : 
'*
'\******************************************************************************

testcase tCompareHelpApps()

    if ( gIsoLang <> "en-US" ) then
        printlog( "No testing for languages other than en_US" )
        goto endsub
    endif

    '///<h1>Compare the About-Items in the Help-Viewer against a reference</h1>
    '///<ul>
    
    ' file related variables
    dim sFileOut as string
    dim sFilein as string
    dim sFileName as string
    
    ' the array that holds the names of the applications
    dim aTopicsFromUI( 10 ) as string ' max index = 7 expected
    
    ' some incremant variables and temporary stuff
    dim iTopicsFromUI as integer
    dim iCurrentItem as integer
    dim sCurrentItem as string
    dim irc as integer
    dim brc as boolean
    
    ' define input and output paths, presetting variables
    sFileName = gProductName & "_help_applications_" & gIsoLang & ".txt"
    sFileOut = hGetWorkFile( sFilename )
    sFileIn  = gTesttoolPath & "framework\optional\input\help_browser\"  
    sFileIn  = sFileIn & sFileName
    sFileIn  = convertpath( sFileIn )
        
    aTopicsFromUI( 0 ) = "0"

    '///+<li>Open the Help</li>
    brc = hOpenHelp()
    if ( not brc ) then
        warnlog( "Help not open, aborting test" )
        goto endsub
    endif

    
    '///+<li>Switch to the Index-Tab</li>
    hSelectHelpTab( "index" )
    
    '///+<li>Retrieve the number of About-Items (Listbox, to the upper left)</li>
    iTopicsFromUI = HelpAbout.getItemCount()
    
    '///+<li>copy the strings from the ListBox into an array</li>
    for iCurrentItem = 1 to iTopicsFromUI
    
        HelpAbout.select( iCurrentItem )
        sCurrentItem = HelpAbout.getSelText()
        hListAppend( sCurrentItem, aTopicsFromUI() )
        
    next iCurrentItem
    
    call hCloseHelp()
    
    ' Compare the list against a reference or create a new list, if the 
    ' reference does not exist
    printlog( "" )
    printlog( "Beginning comparision" )
    
    '///+<li>Compare the array to the reference file</li>
    irc = hManageComparisionList( sFileIn, sFileOut, aTopicsFromUI() )
    if ( irc <> 0 ) then
	    warnlog( "Something went wrong, please review the log" )
    endif
    '///</ul>
    
endcase


