'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: security_trusted_path.inc,v $
'*
'* $Revision: 1.2 $
'*
'* last change: $Author: rt $ $Date: 2008-08-28 11:40:42 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/******************************************************************************
'*
'*  owner : joerg.skottke@sun.com
'*
'*  short description : Execute macros from trusted path
'*
'\******************************************************************************

testcase tSecTrustedPath( cWorkFile as string, iSecLevel as integer )

    '///<h1>Execute macros from a trusted path</h1>
    '///<i>This testcase takes parameters and requires a number of settings
    '///+ to run. See the calling .bas-file for details. <br>
    '///+ Furthermore a number of
    '///+ workfiles are used that have a documentbound macro as payload which is 
    '///+ executed at the &quot;On document load&quot; event.<br>
    '///<ul>

    dim irc as integer
    dim cMsg as string

    '///+<li>Click &quot;FileOpen&quot; or go to the dialog via menu</li>
    '///+<li>Enter the path to a file with macros within the trusted path</li>
    '///+<li>Open the file</li>
    cWorkFile = convertToUrl( convertpath( cWorkFile ) )
    FileOpen( "URL", cWorkFile, "FrameName", "_default"
    irc = hFileWait( false )
    
    ' We have quite a bunch of possible combinations here. Some trigger a security warning
    ' others do not. Files within a trusted path should never prompt.
    
    '         |---------------------------------------------------------|
    '         | Trusted |     Security Level / Warning displayed        |
    '         |  Path   |   Low     |  Medium   |   High    | Very High |
    '         |---------------------------------------------------------|
    '         |  Yes    |     No    |    No     |     No    |    No     |
    '         |---------------------------------------------------------|
    '         |   No*)  |     No    |    Yes    |  Blocked  |  Blocked  |
    '         |---------------------------------------------------------|
    ' *) Covered by test "f_sec_macrosecurity.bas"
       

    '///+<li>Test for security-warning, warn if it pops up</li>
    if ( irc = -2 ) then
        kontext "SecurityWarning"
        if ( SecurityWarning.exists() ) then
            warnlog( "Macro Security Warning was not expected at this point" )
            SecurityWarning.ok()
        endif
    endif

    '///+<li>Verify that the macro is executed</li>
    printlog( "Verify that the macro is executed" )
    kontext "Active"
    if ( active.exists( 2 ) ) then
        cMsg = active.getText()
        cMsg = hRemoveLineBreaks( cMsg )
        if ( cMsg = "TTMacro3" ) then
            printlog( "Macro was executed" )
        else
            warnlog( "Invalid messagebox is open, please check" )
        endif

        '///+<li>Close the macro</li>
        active.ok()

    endif

    '///+<li>Close the document</li>
    hDestroyDocument()

    '///</ul>


endcase

