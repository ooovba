'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: apicalls.inc,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 12:19:05 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/******************************************************************************
'*
'*  owner : joerg.skottke@sun.com
'*
'*  short description : Various calls to the API
'*
'\******************************************************************************

function hGetFileLocationAPI() as string

    '///<h3>Get the location of the current file from API - experimental, do not use</h3>

    dim oUnoConnect as object
    dim oUnoService as object
    dim oDocument as object
    dim cDocument as string
    
    const CFN = "hGetFileLocationAPI::"
    
    oUnoConnect = GetUNOApp
    if ( isNull( oUnoConnect ) ) then
        warnlog( CFN & "No UNO connection established" )
    else
        oUnoService = oUnoConnect.createInstance( "com.sun.star.frame.Desktop" )
        if ( isNull( oUnoService ) ) then
            warnlog( CFN & "No UNO Service available" )
        else
            oDocument = oUnoService.getCurrentComponent()
            if ( isNull( oDocument ) ) then
                warnlog( CFN & "No current component available" )
            else
                if ( oDocument.hasLocation() ) then
                    cDocument = oDocument.getLocation()
                    hGetFileLocationAPI() = cDocument
                    printlog( CFN & cDocument )
                else
                    warnlog( "Document has no storage location" )
                    hGetFileLocationAPI() = "" 
                endif
            endif
        endif
    endif
    
end function  

'*******************************************************************************

function hGetDocTypeAPI() as string

    '///<h3>Get the current document via API - experimental, do not use</h3>

    dim oUnoConnect as object
    dim oUnoService as object
    dim oDocument as object
    dim cDocument as string
    
    const CFN = "hGetDocTypeAPI::"
    
    oUnoConnect = GetUNOApp
    if ( isNull( oUnoConnect ) ) then
        warnlog( CFN & "No UNO connection established" )
    else
        oUnoService = oUnoConnect.createInstance( "com.sun.star.frame.Desktop" )
        if ( isNull( oUnoService ) ) then
            warnlog( CFN & "No UNO Service available" )
        else
            oDocument = oUnoService.getCurrentComponent()
            if ( isNull( oDocument ) ) then
                warnlog( CFN & "No current component available" )
            else
                if ( oDocument.SupportsService( "com.sun.star.sheet.SpreadsheetDocument" ) )then
                    hGetDoctypeAPI() = "CALC" 
                elseif( oDocument.SupportsService( "com.sun.star.text.TextDocument" ) ) then
                    hGetDocTypeAPI() = "WRITER"
                elseif( oDocument.SupportsService( "com.sun.star.drawing.DrawingDocument" ) ) then
                    hGetDocTypeAPI() = "DRAW"
                elseif( oDocument.SupportsService( "com.sun.star.formula.FormulaProperties" ) ) then
                    hGetDocTypeAPI() = "MATH" 
                elseif( oDocument.SupportsService("com.sun.star.presentation.PresentationDocument") ) then
                    hGetDocTypeAPI() = "IMPRESS"
                else
                    warnlog( "Unknown document type" )
                    hGetDocTypeAPI() = "" 
                endif
            endif
        endif
    endif
  
    
end function  
   
