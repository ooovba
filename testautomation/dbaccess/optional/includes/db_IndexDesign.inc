'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: db_IndexDesign.inc,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 07:43:41 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************

'*
'* owner : marc.neumann@sun.com
'*
'* short description : tests for indexes
'*
'\***********************************************************************

function tIndex(sFileName as String, sTableName as String)
    
    printlog sTableName
    printlog sFileName

    call fOpendatabase(sFileName)

    call fOpenTableInDesign(sTableName)
    
    Kontext "TableDesignTable"
        TableDesignTable.UseMenu        
        hMenuSelectNr(4) ' the tools menu
        hMenuSelectNr(1) ' the IndexDesign

    Kontext "DatabaseIndexes"
        if ( not DatabaseIndexes.exists(3) ) then
            warnlog "The index design doesn't exists"
        endif
        NewIndex.Click
        sleep(1)
        DatabaseIndexes.TypeKeys("<RETURN>")
        IndexFields.select 6
        DatabaseIndexesClose.Click
        
    Kontext "MessageBox"
        MessageBox.Yes
        
    '/// remove the created index again

    Kontext "TableDesignTable"
        TableDesignTable.UseMenu        
        hMenuSelectNr(4) ' the tools menu
        hMenuSelectNr(1) ' the IndexDesign

    Kontext "DatabaseIndexes"
        if ( not DatabaseIndexes.exists(3) ) then
            warnlog "The index design doesn't exists"
        endif
        IndexList.select 1
        DropIndex.Click
    Kontext "MessageBox"
        MessageBox.Yes
        sleep(1)

    Kontext "DatabaseIndexes"
        DatabaseIndexesClose.Click
        
    Kontext "MessageBox"
    	if MessageBox.exists(2) then
	        MessageBox.Yes
	    endif
                                
        
    call fCloseTableDesign()

    call fCloseDatabase()

end function
