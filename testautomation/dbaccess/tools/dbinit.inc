'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: dbinit.inc,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 07:43:46 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* owner : marc.neumann@sun.com
'*
'* short description : Helper Routines for Base tests.
'*
'***************************************************************************************
'*
' #1 null
'*
'\***********************************************************************************

Global gDatasourceName as String
Global gTableName as String
'--------------------------------------------------------------------
sub sDBInit
    
    'global
    use "global\system\includes\master.inc"
    use "global\system\includes\gvariabl.inc"
    
    'base
    use "dbaccess\tools\formtools.inc"
    use "dbaccess\tools\reporttools.inc"
    use "dbaccess\tools\tabletools.inc"
    use "dbaccess\tools\dbcreatetools.inc"
    use "dbaccess\tools\querytools.inc"
    use "dbaccess\tools\dbtools.inc"
    use "dbaccess\tools\controltools.inc"
    
end sub        
