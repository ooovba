'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: w_shortcuts.bas,v $
'*
'* $Revision: 1.2 $
'*
'* last change: $Author: fredrikh $ $Date: 2008-06-26 08:18:21 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* owner : helge.delfs@sun.com
'*
'* short description : Test of shortcuts in writer
'*
'\***********************************************************************

sub main
    Dim StartTime
    StartTime = Now()

    '/// This test is based on the following spec:
    '/// Localized Shortcuts
    '/// http://specs.openoffice.org/g11n/menus/LocalizedShortcuts.sxw

    use "writer\tools\includes\w_tools.inc"
    use "writer\optional\includes\shortcut\w_shortcuts.inc"

    printlog Chr(13) + "Loading of Include - Files takes: " + Wielange ( StartTime )
    printlog Chr(13) + "******* Writer - Shortcut - Test *******"

    Call hStatusIn ( "writer", "w_shortcuts.bas","Writer Shortcut-Test" )
	Call tShortcutGlobalNew
	Call tShortcutGlobalOpen
	Call tShortcutGlobalSave
	Call tShortcutGlobalSaveAs
	Call tShortcutSelectAll
	Call tShortcutFindAndReplace
	Call tShortcutBold
	Call tShortcutItalic
	Call tShortcutUnderline
	Call tShortcutDoubleUnderline
	Call tShortcutAlign
	Call tShortcutSuperscript
	Call tShortcutSubscript
	Call tShortcutPasteUnformattedText
    Call hStatusOut

    Printlog Chr(13) + "End of Shortcut - Test :"
    Printlog "Duration: "+ WieLange ( StartTime )
    Printlog "Date: " +  Date + "    Time: " + Time

end sub

sub LoadIncludeFiles
    use "global\system\includes\master.inc"
    use "global\system\includes\gvariabl.inc"
    Call GetUseFiles
    gApplication = "WRITER"
end sub
