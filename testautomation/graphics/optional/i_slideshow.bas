'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'*
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: i_slideshow.bas,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 10:42:38 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* Owner : wolfram.garten@sun.com
'*
'* short description : Graphics Function: Slideshow
'*
'\******************************************************************

public glLocale (15*20) as string
global S1 as string
global S2 as string
global S3 as string
global S4 as string

sub main
    PrintLog "------------------------- slideshow test -------------------------"
    Call hStatusIn ( "Graphics","i_slideshow.bas")

    use "graphics\tools\id_tools.inc"
    use "graphics\tools\id_tools_2.inc"
    use "graphics\optional\includes\impress\i_slideshow.inc"
    use "graphics\optional\includes\impress\i_slideshow2.inc"
    use "graphics\optional\includes\impress\i_slideshow3.inc"

    hSetLocaleStrings ( gTesttoolPath + "graphics\tools\locale_1.txt" , glLocale () )
    if glLocale (2) = "" then
        warnlog "Add 'slide' to FILE  /input/impress/locale_1.txt (take string from below)!!!"
    endif

    '/// LOCALE Slide - 2 ///'
    S1 = glLocale (2) + " 1"
    S2 = glLocale (2) + " 2"
    S3 = glLocale (2) + " 3"
    S4 = glLocale (2) + " 4"

    Call tSlideShowSlideTransition
    Call tPraesentationAnimation
    Call tAendernObjektBenennen
    Call tSlideShowRehearseTimings

    Call tSlideShowShowHideSlide
    Call tExtrasInteraktion
    Call tExtrasIndividuellePraesentation
    Call tExtrasPraesentationseinstellungen
    Call tSlideShowSlideShowSettings
    Call tSlideShowSlideShow
    Call tSlideShowInteraction
    Call tSlideShowCustomSlideShow
    Call tSlideshowBackgroundAllSlides
    Call tSlideshowBackgroundSelectedSlide
    Call tPraesentationEffekt
    Call tExtrasEffekt
    Call tSlideshowContextMenuOneSlide
    Call tSlideshowContextMenuMoreSlides
    Call tiMousePointerHides
    Call tSlideShowRehearseTimings

    Call hStatusOut
end sub

sub LoadIncludeFiles
    use "global\system\includes\master.inc"
    use "global\system\includes\gvariabl.inc"
    use "global\tools\includes\optional\t_ui_filters.inc"
    gApplication = "IMPRESS"
    Call GetUseFiles
end sub
