'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: im_004_.inc,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 10:42:42 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* Owner : wolfram.garten@sun.com
'*
'* short description : Impress Required Test Library (4)
'*
'\*****************************************************************

testcase tiInsertSlideExpandSummary

'/// open application ///'
   Call hNewDocument
   ' presupposition
'/// View->Master View->Outline View ///'
      ViewWorkspaceOutlineView
      Sleep 1
      Kontext "DocumentImpressOutlineView"
'/// Type 2 rows ///'
      DocumentImpressOutlineView.TypeKeys "Herbert<Return>Rudi"
'/// View->Master View->Drawing View ///'
      ViewWorkspaceDrawingView
      Sleep 1
   ' test menue entries
'/// Insert->Summery Slide ///'
   InsertSummerySlide
   Sleep 1
'/// Insert->Expand Slide ///'
   InsertExpandSlide
   Sleep 2
'/// close application ///'
   Call  hCloseDocument
endcase

