'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'* 
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: d_003_.inc,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 10:42:39 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* Owner : wolfram.garten@sun.com
'*
'* short description :
'*
'\*****************************************************************

testcase tdViewPagePane

    printlog " open application "
    Call hNewDocument
    sleep 1
    kontext "pagepane"
    if (NOT pagepane.exists) then
        qaerrorlog "Pages Panel not visible on opening application. Opening now."
        ViewPagePane
    endif
    kontext "pagepane"
    sleep (2)
    try
        printlog " View->Page Pane "
        ViewPagePane
        sleep (2)
        if (pagepane.exists) then
            warnlog "View->Page Pane failed."
            ViewPagePane
        endif
    catch
        warnlog "View->Page Pane couldn't get executed"
    endcatch
    sleep 1
    if (NOT pagepane.exists) then
        ViewPagePane
        sleep (1)
    endif
    printlog " close application "
    Call hCloseDocument
    
endcase 'tdViewPagePane

'-------------------------------------------------------------------------------
testcase tdViewSlide

    printlog " open application "
    hNewDocument
    kontext "DocumentDrawImpress" ' special case :-)
    printlog " click the button on the bottom: 'Master View' (because it is not accessible via the menu :-() "
    ViewMasterPage
    sleep 1
    printlog " View->Slide "
    ViewPagePane
    Sleep 1
    printlog " close application "
    Call  hCloseDocument
   
endcase 'tdViewSlide
