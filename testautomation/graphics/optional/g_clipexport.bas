'encoding UTF-8  Do not remove or change this line!
'**************************************************************************
'* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
'*
'* Copyright 2008 by Sun Microsystems, Inc.
'*
'* OpenOffice.org - a multi-platform office productivity suite
'*
'* $RCSfile: g_clipexport.bas,v $
'*
'* $Revision: 1.1 $
'*
'* last change: $Author: jsi $ $Date: 2008-06-16 10:42:35 $
'*
'* This file is part of OpenOffice.org.
'*
'* OpenOffice.org is free software: you can redistribute it and/or modify
'* it under the terms of the GNU Lesser General Public License version 3
'* only, as published by the Free Software Foundation.
'*
'* OpenOffice.org is distributed in the hope that it will be useful,
'* but WITHOUT ANY WARRANTY; without even the implied warranty of
'* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'* GNU Lesser General Public License version 3 for more details
'* (a copy is included in the LICENSE file that accompanied this code).
'*
'* You should have received a copy of the GNU Lesser General Public License
'* version 3 along with OpenOffice.org.  If not, see
'* <http://www.openoffice.org/license.html>
'* for a copy of the LGPLv3 License.
'*
'/************************************************************************
'*
'* Owner : wolfram.garten@sun.com
'*
'* short description : Clipboard export Test
'*
'\******************************************************************

sub main
    Call hStatusIn ( "Graphics","g_clipexport.bas")

    use "graphics\tools\id_tools.inc"
    use "graphics\tools\id_tools_2.inc"
    use "graphics\optional\includes\global\g_clipexport.inc"
    use "graphics\optional\includes\global\g_clipexport2.inc"
    use "graphics\optional\includes\global\g_clipexport3.inc"

    PrintLog "-------------------------" + gApplication + "-------------------"
    Call tClipboardFromDrawTest

    gApplication = "IMPRESS"
    PrintLog "-------------------------" + gApplication + "-------------------"
    Call tClipboardFromDrawTest

    'TODO FHA - Find and write bugs for exporting to writer and calc.
    '    gApplication = "WRITER"
    '    PrintLog "-------------------------" + gApplication + "-------------------"
    '       Call tClipboardFromDrawTest

    '    gApplication = "CALC"
    '    PrintLog "-------------------------" + gApplication + "-------------------"
    '       Call tClipboardFromDrawTest

    Call hStatusOut
end sub

sub LoadIncludeFiles
    use "global\system\includes\master.inc"
    use "global\system\includes\gvariabl.inc"
    use "global\tools\includes\required\t_menu.inc"   'Window-control
    gApplication = "DRAW"
    Call GetUseFiles
end sub
