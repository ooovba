#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.12 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************
PRJ=..

PRJNAME=xmlscript
TARGET=xcr
NO_BSYMBOLIC=TRUE
#USE_DEFFILE=TRUE
ENABLE_EXCEPTIONS=TRUE

# --- Settings -----------------------------------------------------

.INCLUDE :  settings.mk

#-------------------------------------------------------------------

LIB1TARGET=	$(SLB)$/$(TARGET).lib

LIB1FILES= \
        $(SLB)$/xml_helper.lib		\
        $(SLB)$/xmldlg_imexp.lib	\
        $(SLB)$/xmllib_imexp.lib	\
        $(SLB)$/xmlmod_imexp.lib	\
        $(SLB)$/xmlflat_imexp.lib	\
        $(SLB)$/misc.lib

SHL1TARGET=$(TARGET)$(DLLPOSTFIX)

SHL1LIBS= \
        $(LIB1TARGET)

SHL1STDLIBS= \
		$(COMPHELPERLIB)                \
        $(CPPUHELPERLIB)		\
        $(CPPULIB) 			\
        $(SALLIB)

SHL1DEPN=
SHL1IMPLIB=	i$(TARGET)
SHL1DEF=	$(MISC)$/$(SHL1TARGET).def

DEF1NAME=	$(SHL1TARGET)
DEFLIB1NAME=	$(TARGET)
DEF1DEPN=	$(MISC)$/$(SHL1TARGET).flt

# --- Targets -------------------------------------------------------

.INCLUDE :	target.mk

$(MISC)$/$(SHL1TARGET).flt : makefile.mk xcr.flt
    @echo ------------------------------
    @echo Making: $@
    $(TYPE) xcr.flt > $@
