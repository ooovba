Copy instmsia.exe and instmsiw.exe in this directory for Windows builds using
a .NET compiler. These programs are part of the .NET installation and can be
found in a directory similar to:
"c:\Program Files\Microsoft Visual Studio .NET 2003\Common7\Tools\Deployment\MsiRedist\"
