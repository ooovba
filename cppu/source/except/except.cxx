#include <sal/config.h>
#include <cppu/macros.hxx>
#include <com/sun/star/uno/Any.hxx>
#include <com/sun/star/uno/Reference.hxx>

// We don't want to hide all this shared goodness:
#undef CPPU_GCC_DLLPUBLIC_EXPORT
#define CPPU_GCC_DLLPUBLIC_EXPORT 
#undef CPPU_GCC_DLLPRIVATE
#define CPPU_GCC_DLLPRIVATE 

#define CPPU_INTERNAL_IMPL 1 

#include <linking_catch.hxx>

