# --- Settings -----------------------------------------------------

PRJ=..$/..

PRJNAME=cppu
TARGET=unotypes
ENABLE_EXCEPTIONS=TRUE

.INCLUDE :  settings.mk

LIB1TARGET= $(SLB)$/$(TARGET).lib
LIB1OBJFILES= $(SLO)$/except.obj

SHL1TARGET=exlink$(DLLPOSTFIX)
SHL1LIBS=$(LIB1TARGET)
SHL1STDLIBS= $(SALLIB) $(SALHELPERLIB) $(REGLIB) $(CPPULIB)

# --- Targets -------------------------------------------------------

.INCLUDE :  target.mk

