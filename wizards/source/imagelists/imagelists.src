/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: imagelists.src,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#define REPORTWIZARD_BASE 	1000
#define FORMWIZARD_BASE 	1100
#define WEBWIZARD_BASE	    1200

#define	IMG_REPORT_ORIENTATION_PORTRAIT	   		REPORTWIZARD_BASE + 0
Image IMG_REPORT_ORIENTATION_PORTRAIT
{
    ImageBitmap = Bitmap { File = "portrait_32.png"; };
};

#define	IMG_REPORT_ORIENTATION_PORTRAIT_HC		REPORTWIZARD_BASE + 1
Image IMG_REPORT_ORIENTATION_PORTRAIT_HC
{
    ImageBitmap = Bitmap { File = "portrait_32_h.png"; };
};

#define	IMG_REPORT_ORIENTATION_LANDSCAPE		REPORTWIZARD_BASE + 2
Image IMG_REPORT_ORIENTATION_LANDSCAPE
{
    ImageBitmap = Bitmap { File = "landscape_32.png"; };
};

#define	IMG_REPORT_ORIENTATION_LANDSCAPE_HC		REPORTWIZARD_BASE + 3
Image IMG_REPORT_ORIENTATION_LANDSCAPE_HC
{
    ImageBitmap = Bitmap { File = "landscape_32_h.png"; };
};

#define	IMG_FORM_ARRANGELISTSIDE				FORMWIZARD_BASE + 0
Image IMG_FORM_ARRANGELISTSIDE
{
    ImageBitmap = Bitmap { File = "formarrangelistside_42.png"; };
};

#define	IMG_FORM_ARRANGELISTSIDE_HC				FORMWIZARD_BASE + 1
Image IMG_FORM_ARRANGELISTSIDE_HC
{
    ImageBitmap = Bitmap { File = "formarrangelistside_42_h.png"; };
};

#define	IMG_FORM_ARRANGELISTTOP					FORMWIZARD_BASE + 2
Image IMG_FORM_ARRANGELISTTOP
{
    ImageBitmap = Bitmap { File = "formarrangelisttop_42.png"; };
};

#define	IMG_FORM_ARRANGELISTTOP_HC				FORMWIZARD_BASE + 3
Image IMG_FORM_ARRANGELISTTOP_HC
{
    ImageBitmap = Bitmap { File = "formarrangelisttop_42_h.png"; };
};

#define	IMG_FORM_ARRANGETABLE					FORMWIZARD_BASE + 4
Image IMG_FORM_ARRANGETABLE
{
    ImageBitmap = Bitmap { File = "formarrangetable_42.png"; };
};

#define	IMG_FORM_ARRANGETABLE_HC					FORMWIZARD_BASE + 5
Image IMG_FORM_ARRANGETABLE_HC
{
    ImageBitmap = Bitmap { File = "formarrangetable_42_h.png"; };
};


#define	IMG_FORM_ARRANGEFREE					FORMWIZARD_BASE + 6
Image IMG_FORM_ARRANGEFREE
{
    ImageBitmap = Bitmap { File = "formarrangefree_42.png"; };
};

#define	IMG_FORM_ARRANGEFREE_HC					FORMWIZARD_BASE + 7
Image IMG_FORM_ARRANGEFREE_HC
{
    ImageBitmap = Bitmap { File = "formarrangefree_42_h.png"; };
};


#define	IMG_WEB_LAYOUT_TABLE3						WEBWIZARD_BASE + 0
Image IMG_WEB_LAYOUT_TABLE3
{
    ImageBitmap = Bitmap { File = "table_3.png"; };
};

#define	IMG_WEB_LAYOUT_TABLE3_HC						WEBWIZARD_BASE + 1
Image IMG_WEB_LAYOUT_TABLE3_HC
{
    ImageBitmap = Bitmap { File = "table_3_h.png"; };
};


#define	IMG_WEB_LAYOUT_TABLE2						WEBWIZARD_BASE + 2
Image IMG_WEB_LAYOUT_TABLE2
{
    ImageBitmap = Bitmap { File = "table_2.png"; };
};


#define	IMG_WEB_LAYOUT_TABLE2_HC					WEBWIZARD_BASE + 3
Image IMG_WEB_LAYOUT_TABLE2_HC
{
    ImageBitmap = Bitmap { File = "table_2_h.png"; };
};


#define	IMG_WEB_LAYOUT_SIMPLE						WEBWIZARD_BASE + 4
Image IMG_WEB_LAYOUT_SIMPLE
{
    ImageBitmap = Bitmap { File = "simple.png"; };
};

#define	IMG_WEB_LAYOUT_SIMPLE_HC					WEBWIZARD_BASE + 5
Image IMG_WEB_LAYOUT_SIMPLE_HC
{
    ImageBitmap = Bitmap { File = "simple_h.png"; };
};

#define	IMG_WEB_LAYOUT_DIAGONAL						WEBWIZARD_BASE + 6
Image IMG_WEB_LAYOUT_DIAGONAL
{
    ImageBitmap = Bitmap { File = "diagonal.png"; };
};

#define	IMG_WEB_LAYOUT_DIAGONAL_HC					WEBWIZARD_BASE + 7
Image IMG_WEB_LAYOUT_DIAGONAL_HC
{
    ImageBitmap = Bitmap { File = "diagonal_h.png"; };
};

#define	IMG_WEB_LAYOUT_ZIGZAG						WEBWIZARD_BASE + 8
Image IMG_WEB_LAYOUT_ZIGZAG
{
    ImageBitmap = Bitmap { File = "zigzag.png"; };
};

#define	IMG_WEB_LAYOUT_ZIGZAG_HC						WEBWIZARD_BASE + 9
Image IMG_WEB_LAYOUT_ZIGZAG_HC
{
    ImageBitmap = Bitmap { File = "zigzag_h.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMELEFT						WEBWIZARD_BASE + 10
Image IMG_WEB_LAYOUT_FRAMELEFT
{
    ImageBitmap = Bitmap { File = "frame_left.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMELEFT_HC						WEBWIZARD_BASE + 11
Image IMG_WEB_LAYOUT_FRAMELEFT_HC
{
    ImageBitmap = Bitmap { File = "frame_left_h.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMERIGHT						WEBWIZARD_BASE + 12
Image IMG_WEB_LAYOUT_FRAMERIGHT
{
    ImageBitmap = Bitmap { File = "frame_right.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMERIGHT_HC						WEBWIZARD_BASE + 13
Image IMG_WEB_LAYOUT_FRAMERIGHT_HC
{
    ImageBitmap = Bitmap { File = "frame_right_h.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMETOP							WEBWIZARD_BASE + 14
Image IMG_WEB_LAYOUT_FRAMETOP
{
    ImageBitmap = Bitmap { File = "frame_top.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMETOP_HC						WEBWIZARD_BASE + 15
Image IMG_WEB_LAYOUT_FRAMETOP_HC
{
    ImageBitmap = Bitmap { File = "frame_top_h.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMEBOTTOM							WEBWIZARD_BASE + 16
Image IMG_WEB_LAYOUT_FRAMEBOTTOM
{
    ImageBitmap = Bitmap { File = "frame_bottom.png"; };
};

#define	IMG_WEB_LAYOUT_FRAMEBOTTOM_HC							WEBWIZARD_BASE + 17
Image IMG_WEB_LAYOUT_FRAMEBOTTOM_HC
{
    ImageBitmap = Bitmap { File = "frame_bottom_h.png"; };
};

