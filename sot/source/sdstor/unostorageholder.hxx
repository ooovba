/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: unostorageholder.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _UNOSTORAGEHOLDER_HXX
#define _UNOSTORAGEHOLDER_HXX

#include <com/sun/star/embed/XTransactionListener.hpp>
#include <cppuhelper/implbase1.hxx>

#include <unotools/tempfile.hxx>
#include <sot/storage.hxx>

class SotStorage;
class UNOStorageHolder : public ::cppu::WeakImplHelper1< 
                          ::com::sun::star::embed::XTransactionListener >

{
    SotStorage* m_pParentStorage;					// parent storage
    SotStorageRef m_rSotStorage;					// original substorage
    ::com::sun::star::uno::Reference< ::com::sun::star::embed::XStorage > m_xStorage;	// duplicate storage
    ::utl::TempFile* m_pTempFile;					// temporary file used by duplicate storage

public:
    UNOStorageHolder( SotStorage& aParentStorage,
                      SotStorage& aStorage,
                      ::com::sun::star::uno::Reference< ::com::sun::star::embed::XStorage > xStorage,
                      ::utl::TempFile* pTempFile );

    void InternalDispose();
    String GetStorageName();

    ::com::sun::star::uno::Reference< ::com::sun::star::embed::XStorage > GetDuplicateStorage() { return m_xStorage; }

    virtual void SAL_CALL preCommit( const ::com::sun::star::lang::EventObject& aEvent )
        throw ( ::com::sun::star::uno::Exception,
                ::com::sun::star::uno::RuntimeException );

    virtual void SAL_CALL commited( const ::com::sun::star::lang::EventObject& aEvent )
        throw ( ::com::sun::star::uno::RuntimeException );

    virtual void SAL_CALL preRevert( const ::com::sun::star::lang::EventObject& aEvent )
        throw ( ::com::sun::star::uno::Exception,
                ::com::sun::star::uno::RuntimeException );

    virtual void SAL_CALL reverted( const ::com::sun::star::lang::EventObject& aEvent )
        throw ( ::com::sun::star::uno::RuntimeException );

    virtual void SAL_CALL disposing( const ::com::sun::star::lang::EventObject& Source )
        throw ( ::com::sun::star::uno::RuntimeException );
};

#endif

