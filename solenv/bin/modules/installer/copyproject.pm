#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: copyproject.pm,v $
#
# $Revision: 1.9 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

package installer::copyproject;

use installer::control;
use installer::converter;
use installer::files;
use installer::globals;
use installer::logger;
use installer::mail;
use installer::systemactions;
use installer::worker;

####################################################
# Including header files into the logfile
####################################################

sub copy_project
{
    my ( $filesref, $scpactionsref, $loggingdir, $languagestringref, $shipinstalldir, $allsettingsarrayref ) = @_;

    # Creating directories

    installer::logger::include_header_into_logfile("Creating installation directory");

    my $current_install_number = "";

    my $installdir = installer::worker::create_installation_directory($shipinstalldir, $languagestringref, \$current_install_number);

    my $installlogdir = installer::systemactions::create_directory_next_to_directory($installdir, "log");

    # Copy files and ScpActions

    installer::logger::include_header_into_logfile("Copying files:");
    
    # copy Files
    
    for ( my $i = 0; $i <= $#{$filesref}; $i++ )
    {
        my $onefile = ${$filesref}[$i];

        my $source = $onefile->{'sourcepath'};
        my $destination = $installdir . $installer::globals::separator . $onefile->{'Name'};
        
        installer::systemactions::copy_one_file($source, $destination);

        if ( $destination =~ /install\s*$/ )
        {
            my $localcall = "chmod 775 $destination \>\/dev\/null 2\>\&1";
            system($localcall);
        }

            if ( $onefile->{'UnixRights'} )
        {
            my $localcall = "chmod $onefile->{'UnixRights'} $destination \>\/dev\/null 2\>\&1";
            system($localcall);
            }
    }
        
    # copy ScpActions	

    for ( my $i = 0; $i <= $#{$scpactionsref}; $i++ )
    {
        my $onefile = ${$scpactionsref}[$i];

        my $source = $onefile->{'sourcepath'};
        my $destination = $installdir . $installer::globals::separator . $onefile->{'DestinationName'};
        
        installer::systemactions::copy_one_file($source, $destination);
        
        if ( $destination =~ /install\s*$/ )
        {
            my $localcall = "chmod 775 $destination \>\/dev\/null 2\>\&1";
            system($localcall);
        }

        if ( $onefile->{'UnixRights'} )
        {
            my $localcall = "chmod $onefile->{'UnixRights'} $destination \>\/dev\/null 2\>\&1";
            system($localcall);
        }
    }

    # Analyzing the log file

    installer::worker::analyze_and_save_logfile($loggingdir, $installdir, $installlogdir, $allsettingsarrayref, $languagestringref, $current_install_number);

    # That's all
    
    exit(0);
}

1;
