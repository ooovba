#!/bin/bash
#*************************************************************************
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#***********************************************************************/

EXIT_SUCCESS=0
EXIT_FAILURE=1
EXIT_ERROR=2
EXIT_BUG=10

if [ x${SOLARENV}x = xx ]; then
    echo No environment found, please use 'configure' or 'setsolar'
    exit $EXIT_FAILURE
fi
usage() {
        echo "Usage: $SCRIPTNAME [-t DESTPATH] [-o] [-d] [-a]" >&2
        echo "" >&2
        echo "[-t] target path: path wehre tho office should installed to. The default is '$DESTPATH'" >&2
        echo "" >&2
        echo "[-o] force OpenOffice.org installation instead of StarOffice" >&2
        echo "" >&2
        echo "[-d] installation with debug output" >&2
        echo "" >&2
        echo "[-a] the office will be patched to run without FirstStartWizard" >&2
        echo "" >&2
        echo "[-i] impress should open documents without autopilot" >&2
        exit $EXIT_FAILURE
}

if [ x${USER}x = xx ]; then
    if [ x${LOGNAME}x = xx ]; then
        echo "ERROR: could not determine username. Please export variable USER" >&2
        exit $EXIT_FAILURE
    else 
        USER=$LOGNAME
        export USER
    fi
fi
     
DESTPATH=/tmp/$USER
PARAM=""

while getopts ':mt:aicdhot' OPTION ; do
        case $OPTION in
        d)        PARAM="$PARAM -debug true"
                ;;
        c)        PARAM="$PARAM -cwscheckapi true"
                ;;
        o)        PARAM="$PARAM -ooo true"
                ;;
        a)        PARAM="$PARAM -autorun true"
                ;;
        i)        PARAM="$PARAM -autoimpress true"
                ;;
        t)        DESTPATH="$OPTARG"
                ;;
        h)        usage $EXIT_SUCCESS
                ;;
        \?)        echo "unkown option \"-$OPTARG\"." >&2
                usage $EXIT_ERROR
                ;;
        *)        echo "this is not possible...">&2
                usage $EXIT_BUG
                ;;
        esac
done

shift `expr $OPTIND - 1`

LOCALINSTALLDIR=$DESTPATH/office
LOCALUNPACKDIR=$DESTPATH/unpack

export LOCALINSTALLDIR
export LOCALUNPACKDIR
echo "export LOCALINSTALLDIR"
echo "export LOCALUNPACKDIR"

unset LD_LIBRARY_PATH
unset FORCE2ARCHIVE

echo "### $SOLARENV/bin/installoffice.pl $PARAM -cleanup true $@"
exec perl -w $SOLARENV/bin/installoffice.pl $PARAM -cleanup true $@

exit $?
