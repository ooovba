:
eval 'exec perl -wS $0 ${1+"$@"}'
    if 0;
#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: createpdbrelocators.pl,v $
#
# $Revision: 1.5 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

#*************************************************************************
#
# createpdbrelocators - create for pdb relocator files
#                       PDB relocator files are used to find debug infos
#                       for analysis of creash reports
#
# usage: createpdbrelocators;
#
#************************************************************************* 

use strict;

#### module lookup

use lib ("$ENV{SOLARENV}/bin/modules");
use CreatePDBRelocators;

#### script id #####

( my $script_name = $0 ) =~ s/^.*\b(\w+)\.pl$/$1/;

my $script_rev;
my $id_str = ' $Revision: 1.5 $ ';
$id_str =~ /Revision:\s+(\S+)\s+\$/
  ? ($script_rev = $1) : ($script_rev = "-");

print "$script_name -- version: $script_rev\n";

my $inpath    = $ENV{INPATH};
my $milestone = $ENV{UPDMINOR};

if ( $ARGV[0] ) {
    if ( $milestone && ( $milestone ne $ARGV[0] ) ) {
        die "Error: specified milestone $ARGV[0] does not match your environment";
    }
    $milestone = $ARGV[0];
}

if ( !$inpath || !$milestone ) {
    print STDERR "$script_name: INAPTH or UPDMINOR not set!\n";
    exit(1);
}

my $rc = CreatePDBRelocators::create_pdb_relocators($inpath, $milestone, "");

if ( !$rc ) {
    print STDERR "$script_name: creating PDB relocators failed!\n";
    exit(2);
}

exit(0);
