/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: premac.h,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#define Boolean MacOSBoolean
#define BOOL MacOSBOOL
#define Button MacOSButton
#define Byte MacOSByte
#define Control MacOSControl
#define Cursor MacOSCursor
#define FontInfo MacOSFontInfo
#define MemoryBlock MacOSMemoryBlock
#define Point MacOSPoint
#define Size MacOSSize
#define Region MacOSRegion
#define Polygon MacOSPolygon
#define Ptr MacOSPtr
#define Palette MacOSPalette
#define LSize MacOSLSize
#define ModalDialog MacOSModalDialog
#define SetCursor MacOSSetCursor
//#define ShowWindow MacOSShowWindow
#define StringPtr MacOSStringPtr
#define DirInfo MacOSDirInfo
#define ULONG MacOSULONG
#define Line MacOSLine
#define TimeValue MacOSTimeValue
#define Pattern MacOSPattern
