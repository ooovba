#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.8 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************
PRJ=..
PRJINC=

PRJNAME=testshl2
TARGET=test_autoptr
TARGETTYPE=CUI

ENABLE_EXCEPTIONS=TRUE
#USE_LDUMP2=TRUE
#LDUMP2=LDUMP3

# --- Settings -----------------------------------------------------
.INCLUDE :  settings.mk

# ------------------------------------------------------------------
#--------------------------------- Objectfiles ---------------------------------
OBJFILES=\
    $(OBJ)$/test_autoptr.obj \
    $(OBJ)$/test_ostringstream.obj

#----------------------------- prog with *.lib file -----------------------------

APP1TARGET= $(TARGET)
APP1OBJS=$(OBJ)$/test_autoptr.obj 

APP1STDLIBS=$(SALLIB)

APP1DEPN= $(APP1OBJS)
APP1LIBS= 

#-------------------------------------------------------------------------------

APP2TARGET= test_ostringstream
APP2OBJS=$(OBJ)$/test_ostringstream.obj

APP2STDLIBS=$(SALLIB)

APP2DEPN= $(APP1OBJS)
APP2LIBS= 
#-------------------------------------------------------------------------------

APP3TARGET= test_filter
APP3OBJS=$(OBJ)$/test_filter.obj

APP3STDLIBS=$(SALLIB)

APP3DEPN= $(APP1OBJS)
APP3LIBS= 
#-------------------------------------------------------------------------------

# APP4TARGET= test_member
# APP4OBJS=$(OBJ)$/test_member.obj
# 
# APP4STDLIBS=$(SALLIB)
# 
# APP4DEPN=
 # APP4LIBS= 


#-------------------------------------------------------------------------------

# APP5TARGET= test_very_long_names
# APP5OBJS=$(OBJ)$/test_very_long_names.obj
# 
# APP5STDLIBS=$(SALLIB)
# 
# APP5DEPN=
# APP5LIBS= 


#-------------------------------------------------------------------------------

APP6TARGET= test_string
APP6OBJS=$(OBJ)$/test_string.obj

APP6STDLIBS=$(SALLIB)

APP6DEPN=
APP6LIBS= 
#-------------------------------------------------------------------------------

APP7TARGET= test_printf
APP7OBJS=$(OBJ)$/test_printf.obj

APP7STDLIBS=$(SALLIB)

APP7DEPN=
APP7LIBS= 
#-------------------------------------------------------------------------------

APP8TARGET= test_preproc
APP8OBJS=$(OBJ)$/test_preproc.obj

APP8STDLIBS=$(SALLIB)

APP8DEPN=
APP8LIBS= 
# --- Targets ------------------------------------------------------

.INCLUDE :  target.mk
