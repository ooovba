/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: registerfunc.h,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _register_h
#define _register_h

#include <sal/types.h>

typedef void (* FktPtr)(void);
// register the given void* as a function pointer, true, if successful
extern "C" bool SAL_CALL registerFunc(FktPtr aFunc, const char* aFuncName);

typedef bool (* FktRegFuncPtr)(FktPtr aFunc, const char* aFuncName);
extern "C" void SAL_CALL registerAllTestFunction(FktRegFuncPtr aFunc);

typedef void (* FktRegAllPtr)(FktRegFuncPtr aFunc);
#endif
