#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.mk,v $
#
# $Revision: 1.2 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************
PRJ=..$/..

PRJNAME=sal
TARGET=selftest_testshl2
# this is removed at the moment because we need some enhancements
# TESTDIR=TRUE

ENABLE_EXCEPTIONS=TRUE

# --- Settings -----------------------------------------------------

.INCLUDE :  settings.mk

#------------------------------- All object files -------------------------------
# do this here, so we get right dependencies
# SLOFILES= \
#	  $(SLO)$/unotest.obj

#----------------------------------- deliver test -----------------------------------

SHL1OBJS= \
    $(SLO)$/notdeliveredtest.obj 

SHL1TARGET= testshl2_deliver_test
SHL1STDLIBS=\
       $(SALLIB) \
        $(CPPUHELPERLIB) \
        $(COMPHELPERLIB) \
        $(CPPULIB) \
        $(CPPUNITLIB)

SHL1IMPLIB= i$(SHL1TARGET)

DEF1NAME    =$(SHL1TARGET)
SHL1VERSIONMAP = export.map

#----------------------------------- deliver test -----------------------------------

SHL2OBJS= \
    $(SLO)$/delivertest.obj 

SHL2TARGET= delivertest
SHL2STDLIBS=\
       $(SALLIB) \
        $(CPPUHELPERLIB) \
        $(COMPHELPERLIB) \
        $(CPPULIB) \
        $(CPPUNITLIB)

SHL2IMPLIB= i$(SHL2TARGET)

DEF2NAME    =$(SHL2TARGET)
SHL2VERSIONMAP = export.map



# --- Targets ------------------------------------------------------

.INCLUDE :  target.mk
.INCLUDE : _cppunit.mk

