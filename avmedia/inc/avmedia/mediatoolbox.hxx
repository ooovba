/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: mediatoolbox.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _AVMEDIA_MEDIATOOLBOX_HXX
#define _AVMEDIA_MEDIATOOLBOX_HXX

#include <svtools/lstner.hxx>
#include <sfx2/tbxctrl.hxx>

namespace avmedia
{

// -------------------------------
// - SvxGrafFilterToolBoxControl -
// -------------------------------

class MediaItem;

class MediaToolBoxControl : public SfxToolBoxControl 
{
     friend class MediaToolBoxControl_Impl;

public:

                                SFX_DECL_TOOLBOX_CONTROL();

                                MediaToolBoxControl( USHORT nSlotId, USHORT nId, ToolBox& rTbX );
                                ~MediaToolBoxControl();

    virtual void				StateChanged( USHORT nSID, SfxItemState eState, const SfxPoolItem* pState );
    virtual Window*				CreateItemWindow( Window* pParent );

private:

    void						implUpdateMediaControl();
    void 						implExecuteMediaControl( const MediaItem& rItem );
};

}

#endif // _AVMEDIA_MEDIATOOLBOX_HXX
