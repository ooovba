/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ConfigurationProviderWrapper.java,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


package mod._cfgmgr2;

import java.io.PrintWriter;
import java.util.Vector;

import lib.TestCase;
import lib.TestEnvironment;
import lib.TestParameters;

import com.sun.star.beans.PropertyState;
import com.sun.star.beans.PropertyValue;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.uno.XInterface;

public class ConfigurationProviderWrapper extends TestCase {
    
    /** Called to create an instance of <code>TestEnvironment</code> with an
     * object to test and related objects. Subclasses should implement this
     * method to provide the implementation and related objects. The method is
     * called from <code>getTestEnvironment()</code>.
     *
     * @param tParam test parameters
     * @param log writer to log information while testing
     *
     * @see TestEnvironment
     * @see #getTestEnvironment()
     *
     */
    protected TestEnvironment createTestEnvironment(TestParameters tParam, PrintWriter log) {
        
        XInterface oObj = null;

        try {
            PropertyValue[] cArgs = new PropertyValue[2];
            cArgs[0] = new PropertyValue();
            cArgs[0].Name = "Locale";
            cArgs[0].Value = "DE";
            cArgs[1] = new PropertyValue();
            cArgs[1].Name = "EnableAsync";
            cArgs[1].Value = new Boolean(true);            
            
            oObj = (XInterface) ((XMultiServiceFactory)tParam.getMSF())
                                      .createInstanceWithArguments("com.sun.star.comp.configuration.ConfigurationProvider",cArgs);
        } catch (com.sun.star.uno.Exception e) {
        }

        log.println("Implementation name: "+ util.utils.getImplName(oObj)); 
        
        PropertyValue[] nodeArgs = new PropertyValue[1];
        PropertyValue nodepath = new PropertyValue();
        nodepath.Name = "nodepath";
        nodepath.Value = "org.openoffice.Setup";
        nodepath.Handle = -1;
        nodepath.State = PropertyState.DEFAULT_VALUE;
        nodeArgs[0] = nodepath;

        Vector args = new Vector();

        args.add(0, nodeArgs);
        args.add(0, nodeArgs);
        args.add(0, nodeArgs);
        args.add(0, nodeArgs);
        args.add(0, nodeArgs);
        args.add(0, nodeArgs);
        args.add(0, nodeArgs);
        

        TestEnvironment tEnv = new TestEnvironment(oObj);

        tEnv.addObjRelation("XMSF.Args", 
                            args.toArray(new Object[args.size()][]));
        tEnv.addObjRelation("needArgs", "ConfigurationProvider");                
        
        return tEnv;
        
    }    
   
}
