/*************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): Jan Holesovsky <kendy@openoffice.org>
 *
 *
 ************************************************************************/

#ifndef _UNXNOTIFYTHREAD_HXX_
#include <UnxNotifyThread.hxx>
#endif

#ifndef _UNXFILEOPENIMPL_HXX_
#include <UnxFilePicker.hxx>
#endif

using namespace ::com::sun::star;

//////////////////////////////////////////////////////////////////////////
// UnxFilePickerNotifyThread
//////////////////////////////////////////////////////////////////////////

UnxFilePickerNotifyThread::UnxFilePickerNotifyThread( UnxFilePicker *pUnxFilePicker )
    : m_pUnxFilePicker( pUnxFilePicker ),
      m_bExit( sal_False ),
      m_eNotifyType( Nothing ),
      m_nControlId( 0 )
{
}

void SAL_CALL UnxFilePickerNotifyThread::addFilePickerListener( const uno::Reference< ui::dialogs::XFilePickerListener >& xListener )
    throw( uno::RuntimeException )
{
    ::osl::MutexGuard aGuard( m_aMutex );

    m_xListener = xListener;
}

void SAL_CALL UnxFilePickerNotifyThread::removeFilePickerListener( const uno::Reference< ui::dialogs::XFilePickerListener >& xListener ) 
    throw( uno::RuntimeException )
{
    ::osl::MutexGuard aGuard( m_aMutex );

    m_xListener.clear();
}

void SAL_CALL UnxFilePickerNotifyThread::exit()
{
    ::osl::MutexGuard aGuard( m_aMutex );

    m_bExit = sal_True;

    m_aExitCondition.reset();
    m_aNotifyCondition.set();
    
    m_aExitCondition.wait();
}

void SAL_CALL UnxFilePickerNotifyThread::fileSelectionChanged()
{
    ::osl::MutexGuard aGuard( m_aMutex );

    m_eNotifyType = FileSelectionChanged;
    m_nControlId = 0;
    
    m_aNotifyCondition.set();
}

void SAL_CALL UnxFilePickerNotifyThread::run()
{
    do {
        m_aNotifyCondition.reset();
        m_aNotifyCondition.wait();

        if ( m_xListener.is() && m_pUnxFilePicker )
        {
            ::osl::MutexGuard aGuard( m_aMutex );

            ui::dialogs::FilePickerEvent aEvent( *m_pUnxFilePicker, m_nControlId );

            switch ( m_eNotifyType )
            {
                case FileSelectionChanged:
                    m_xListener->fileSelectionChanged( aEvent );
                    break;

                // TODO More to come...

                default:
                    // nothing
                    break;
            }
        }
    } while ( !m_bExit );

    m_aExitCondition.set();
}
