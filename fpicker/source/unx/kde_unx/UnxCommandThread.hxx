/*************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): Jan Holesovsky <kendy@openoffice.org>
 *
 *
 ************************************************************************/

#ifndef _UNXCOMMANDTHREAD_HXX_
#define _UNXCOMMANDTHREAD_HXX_

#ifndef _COM_SUN_STAR_UNO_ANY_HXX_
#include <com/sun/star/uno/Any.hxx>
#endif

#ifndef _COM_SUN_STAR_UNO_SEQUENCE_HXX_
#include <com/sun/star/uno/Sequence.hxx>
#endif

#ifndef _OSL_CONDITN_HXX_
#include <osl/conditn.hxx>
#endif

#ifndef _OSL_MUTEX_HXX_
#include <osl/mutex.hxx>
#endif

#ifndef _OSL_THREAD_HXX_
#include <osl/thread.hxx>
#endif

#ifndef _RTL_USTRBUF_HXX_
#include <rtl/ustring.hxx>
#endif

#include <vcl/svapp.hxx>

#include <list>

class UnxFilePickerNotifyThread;

/** Synchronization for the 'thread-less' version of the fpicker.

    Something like osl::Condition, but calls Application::Yield() while in
    wait().
*/
class YieldingCondition
{
    ::osl::Mutex m_aMutex;
    bool m_bValue;

    bool get()
    {
        ::osl::MutexGuard aGuard( m_aMutex );
        return m_bValue;
    }

public:
    YieldingCondition() { reset(); }

    void reset()
    {
        ::osl::MutexGuard aGuard( m_aMutex );
        m_bValue = false;
    }

    void set()
    {
        ::osl::MutexGuard aGuard( m_aMutex );
        m_bValue = true;
    }

    void wait()
    {
        while ( !get() )
            Application::Yield();
    }
};

class UnxFilePickerCommandThread : public ::osl::Thread
{
protected:
    UnxFilePickerNotifyThread  *m_pNotifyThread;
    int                         m_nReadFD;

    ::osl::Mutex                m_aMutex;

    YieldingCondition           m_aExecCondition;
    sal_Bool                    m_aResult;

    ::osl::Condition            m_aGetCurrentFilterCondition;
    ::rtl::OUString             m_aGetCurrentFilter;

    ::osl::Condition            m_aGetDirectoryCondition;
    ::rtl::OUString             m_aGetDirectory;

    ::osl::Condition            m_aGetFilesCondition;
    ::std::list< ::rtl::OUString > m_aGetFiles;

    ::osl::Condition            m_aGetValueCondition;
    ::com::sun::star::uno::Any  m_aGetValue;

public:
    UnxFilePickerCommandThread( UnxFilePickerNotifyThread *pNotifyThread, int nReadFD );
    ~UnxFilePickerCommandThread();
    
    YieldingCondition& SAL_CALL execCondition() { return m_aExecCondition; }
    sal_Bool SAL_CALL           result();

    ::osl::Condition& SAL_CALL  getCurrentFilterCondition() { return m_aGetCurrentFilterCondition; }
    ::rtl::OUString SAL_CALL    getCurrentFilter();

    ::osl::Condition& SAL_CALL  getDirectoryCondition() { return m_aGetDirectoryCondition; }
    ::rtl::OUString SAL_CALL    getDirectory();
    
    ::osl::Condition& SAL_CALL  getFilesCondition() { return m_aGetFilesCondition; }
    ::com::sun::star::uno::Sequence< ::rtl::OUString > SAL_CALL getFiles();

    ::osl::Condition& SAL_CALL  getValueCondition() { return m_aGetValueCondition; }
    ::com::sun::star::uno::Any SAL_CALL getValue();

protected:
    virtual void SAL_CALL       run();
    
    virtual void SAL_CALL       handleCommand( const ::rtl::OUString &rCommand/*, sal_Bool &rQuit*/ );
    ::std::list< ::rtl::OUString > SAL_CALL tokenize( const ::rtl::OUString &rCommand );
};

#endif // _UNXCOMMANDTHREAD_HXX_
