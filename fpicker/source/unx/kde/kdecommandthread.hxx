/*************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): Jan Holesovsky <kendy@openoffice.org>
 *
 *
 ************************************************************************/

#ifndef _KDECOMMANDTHREAD_HXX_
#define _KDECOMMANDTHREAD_HXX_

#include <qevent.h>
#include <qmutex.h>
#include <qthread.h>

class CommandEvent : public QCustomEvent
{
public:
    enum CommandEventType {
        Unknown = 0,
        
        AppendControl,
        EnableControl,
        GetValue,
        SetValue,

        AppendFilter,
        AppendFilterGroup,
        UpdateFilters,
        GetCurrentFilter,
        SetCurrentFilter,

        GetDirectory,
        SetDirectory,
        
        GetFiles,

        SetTitle,
        SetType,
        SetDefaultName,
        SetMultiSelection,

        Exec
    };
    static const QEvent::Type   TypeId = (QEvent::Type) ( (int) QEvent::User + 42 /*random magic value*/ );
    
protected:
    CommandEventType            m_eCommand;

public:
    CommandEvent( const QString &qCommand, QStringList *pStringList );

    CommandEventType            command() const { return m_eCommand; }
    QStringList*                stringList() { return static_cast< QStringList* >( data() ); }
};

class CommandThread : public QThread
{
protected:
    QObject                    *m_pObject;

    QMutex                      m_aMutex;

public:
    CommandThread( QWidget *pObject );
    virtual ~CommandThread();

    virtual void                run();

protected:
    void                        handleCommand( const QString &rString, bool &bQuit );
    QStringList*                tokenize( const QString &rString );
};

#endif // _KDECOMMANDTHREAD_HXX_
