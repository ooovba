/*************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): Jan Holesovsky <kendy@openoffice.org>
 *
 *
 ************************************************************************/

#include <kdecommandthread.hxx>

#include <qstringlist.h>

#include <kapplication.h>

#include <iostream>

//////////////////////////////////////////////////////////////////////////
// CommandEvent
//////////////////////////////////////////////////////////////////////////

CommandEvent::CommandEvent( const QString &qCommand, QStringList *pStringList )
    : QCustomEvent( TypeId, pStringList ),
      m_eCommand( Unknown )
{
    struct {
        const char *pName;
        CommandEventType eType;
    } *pIdx, pMapping[] =
    {
        { "appendControl",     AppendControl },
        { "enableControl",     EnableControl },
        { "getValue",          GetValue },
        { "setValue",          SetValue },
        { "appendFilter",      AppendFilter },
        { "appendFilterGroup", AppendFilterGroup },
        { "getCurrentFilter",  GetCurrentFilter },
        { "setCurrentFilter",  SetCurrentFilter },
        { "getDirectory",      GetDirectory },
        { "setDirectory",      SetDirectory },
        { "getFiles",          GetFiles },
        { "setTitle",          SetTitle },
        { "setType",           SetType },
        { "setDefaultName",    SetDefaultName },
        { "setMultiSelection", SetMultiSelection },
        { "exec",              Exec },
        { 0, Unknown }
    };

    for ( pIdx = pMapping; pIdx->pName && qCommand != pIdx->pName; ++pIdx )
        ;

    m_eCommand = pIdx->eType;
}

//////////////////////////////////////////////////////////////////////////
// CommandThread
//////////////////////////////////////////////////////////////////////////

CommandThread::CommandThread( QWidget *pObject )
    : m_pObject( pObject )
{
}

CommandThread::~CommandThread()
{
}

void CommandThread::run()
{
    QTextIStream qStream( stdin );
    qStream.setEncoding( QTextStream::UnicodeUTF8 );

    QString qLine;
    bool bQuit = false;
    while ( !bQuit && !qStream.atEnd() )
    {
        qLine = qStream.readLine();
        handleCommand( qLine, bQuit );
    }
}

void CommandThread::handleCommand( const QString &rString, bool &bQuit )
{
    QMutexLocker qMutexLocker( &m_aMutex );

#if OSL_DEBUG_LEVEL > 0
    ::std::cerr << "kdefilepicker received: " << rString.latin1() << ::std::endl;
#endif

    bQuit = false;
    QStringList *pTokens = tokenize( rString );

    if ( !pTokens )
        return;
    if ( pTokens->empty() )
    {
        delete pTokens, pTokens = NULL;
        return;
    }

    QString qCommand = pTokens->front();
    pTokens->pop_front();

    if ( qCommand == "exit" )
    {
        bQuit = true;
        kapp->exit();
        kapp->wakeUpGuiThread();
    }
    else
        kapp->postEvent( m_pObject, new CommandEvent( qCommand, pTokens ) );
}

QStringList* CommandThread::tokenize( const QString &rString )
{
    // Commands look like:
    // command arg1 arg2 arg3 ...
    // Args may be enclosed in '"', if they contain spaces.

    QStringList *pList = new QStringList();

    QString qBuffer;
    qBuffer.reserve( 1024 );

    const QChar *pUnicode = rString.unicode();
    const QChar *pEnd     = pUnicode + rString.length();
    bool bQuoted          = false;
    
    for ( ; pUnicode != pEnd; ++pUnicode )
    {
        if ( *pUnicode == '\\' )
        {
            ++pUnicode;
            if ( pUnicode != pEnd )
            {
                if ( *pUnicode == 'n' )
                    qBuffer.append( '\n' );
                else
                    qBuffer.append( *pUnicode );
            }
        }
        else if ( *pUnicode == '"' )
            bQuoted = !bQuoted;
        else if ( *pUnicode == ' ' && !bQuoted )
        {
            pList->push_back( qBuffer );
            qBuffer.setLength( 0 );
        }
        else
            qBuffer.append( *pUnicode );
    }
    pList->push_back( qBuffer );
    
    return pList;
}
