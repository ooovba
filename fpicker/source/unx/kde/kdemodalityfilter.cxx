/*************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): Jan Holesovsky <kendy@openoffice.org>
 *
 *
 ************************************************************************/

#include <kdemodalityfilter.hxx>

#include <kapplication.h>
#include <kdialogbase.h>

#include <netwm.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

//////////////////////////////////////////////////////////////////////////
// Modality filter
//////////////////////////////////////////////////////////////////////////

ModalityFilter::ModalityFilter( WId nWinId )
    : m_nWinId( nWinId )
{
    kapp->installEventFilter( this );
}

ModalityFilter::~ModalityFilter()
{
    kapp->removeEventFilter( this );
}

bool ModalityFilter::eventFilter( QObject *pObject, QEvent *pEvent )
{
    if ( pObject->isWidgetType() && pEvent->type() == QEvent::Show )
    {
        KDialogBase* pDlg = ::qt_cast< KDialogBase* >( pObject );
        if ( pDlg != NULL && m_nWinId != 0 )
        {
            XSetTransientForHint( qt_xdisplay(), pDlg->winId(), m_nWinId );
            m_nWinId = 0;
        }
    }
    return false;
}
