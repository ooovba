/*************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): Jan Holesovsky <kendy@openoffice.org>
 *
 *
 ************************************************************************/

#ifndef _KDEFILEPICKER_HXX_
#define _KDEFILEPICKER_HXX_

#include <kfiledialog.h>
#include <kfilefiltercombo.h>

class QGrid;
class QHBox;
class QVBox;

class FileDialog : public KFileDialog
{
    Q_OBJECT

protected:
    typedef QPair< QString, QString > FilterEntry;
    typedef QValueList< FilterEntry > FilterList;
    
    QVBox                      *m_pCustomWidget;
    QHBox                      *m_pCombosAndButtons;
    
    QVBox                      *m_pLabels;
    QVBox                      *m_pComboBoxes;
    QVBox                      *m_pPushButtons;

    QGrid                      *m_pCheckBoxes;

    FilterList                  m_aFilters;

    /** Are we a "Save As" dialog?
     * 
     *  We cannot use KFileDialog::setOperationMode() here, because then
     *  it automatically adds an "Automatically select filename extension"
     *  check box, and completely destroys the dialog's layout
     *  (custom list boxes are under this check box, which looks ugly).
     */
    bool                        m_bIsSave;
    bool                        m_bIsExecuting;

    bool                        m_bCanNotifySelection;

public:
    FileDialog( const QString &startDir, const QString &filter,
                QWidget *parent, const char *name );
    virtual ~FileDialog();

protected:
    virtual void                resizeEvent( QResizeEvent *pEvent );
    virtual void                showEvent( QShowEvent *pEvent );
    void                        updateCustomWidgetLayout();

    virtual void                customEvent( QCustomEvent *pEvent );

protected:
    void                        appendControl( const QString &rId, const QString &rType, const QString &rTitle );
    QWidget*                    findControl( const QString &rId ) const;
    void                        enableControl( const QString &rId, const QString &rValue );
    void                        getValue( const QString &rId, const QString &rAction );
    void                        setValue( const QString &rId, const QString &rAction, const QStringList &rValue );
    
    void                        appendFilter( const QString &rTitle, const QString &rFilter );
    QString                     filters() const;
    QString                     addExtension( const QString &rFileName ) const;

    void                        setIsSave( bool bIsSave ) { m_bIsSave = bIsSave; }
    bool                        isSave( void ) const { return m_bIsSave; }

    void                        setIsExecuting( bool bIsExecuting ) { m_bIsExecuting = bIsExecuting; }
    bool                        isExecuting( void ) const { return m_bIsExecuting; }

    bool                        isSupportedProtocol( const QString &rProtocol ) const;
    KURL                        mostLocalURL( const KURL &rURL ) const;
    QString                     localCopy( const QString &rFileName ) const;

    void                        setCanNotifySelection( bool bCanNotifySelection ) { m_bCanNotifySelection = bCanNotifySelection; }
    bool                        canNotifySelection( void ) const { return m_bCanNotifySelection; }

protected slots:
    void                        fileHighlightedCommand( const QString & );
    void                        selectionChangedCommand();

protected:
    void                        sendCommand( const QString &rCommand );
    void                        appendURL( QString &rBuffer, const KURL &rURL );
    void                        appendEscaped( QString &rBuffer, const QString &rString );
    QString                     escapeString( const QString &rString );
};

class FileFilterComboHack : public KFileFilterCombo
{
public:
    void setCurrentFilter( const QString& filter );
};

#endif // _KDEFILEPICKER_HXX_
