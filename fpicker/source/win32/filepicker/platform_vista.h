/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: platform_vista.h,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _PLATFORM_VISTA_H_
#define _PLATFORM_VISTA_H_

#pragma once

// Change these values to use different versions
#undef WINVER
#undef _WIN32_WINNT
#undef _WIN32_IE
#undef _WTL_NO_CSTRING

#define WINVER          0x0600
#define _WIN32_WINNT    0x0600
#define _WIN32_IE       0x0700
#define _WTL_NO_CSTRING

#if defined _MSC_VER
#pragma warning(push, 1)
#include <comip.h>
#pragma warning(pop)
#endif

// ATL/WTL
//#include <atlbase.h>
//#include <atlstr.h>
//#include <atlapp.h>
//extern CAppModule _Module;
//#include <atlcom.h>
//#include <atlwin.h>
//#include <atlframe.h>
//#include <atlcrack.h>
//#include <atlctrls.h>
//#include <atlctrlx.h>
//#include <atldlgs.h>
//#include <atlmisc.h>

/*
// STL
#include <vector>

// Global functions
LPCTSTR PrepFilterString ( CString& sFilters );
bool    PathFromShellItem ( IShellItem* pItem, CString& sPath );
bool    BuildFilterSpecList ( _U_STRINGorID szFilterList,
                              std::vector<CString>& vecsFilterParts,
                              std::vector<COMDLG_FILTERSPEC>& vecFilters );
*/

#if defined _M_IX86
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#endif

