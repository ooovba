/*************************************************************************
 *
 *  OpenOffice.org - a multi-platform office productivity suite
 *
 *  $RCSfile$
 *
 *  $Revision: 12010 $
 *
 *  last change: $Author: tml $ $Date: 2008-03-26 02:30:23 +0200 (on, 26 mar 2008) $
 *
 *  The Contents of this file are made available subject to
 *  the terms of GNU Lesser General Public License Version 2.1.
 *
 *
 *    GNU Lesser General Public License Version 2.1
 *    =============================================
 *    Copyright 2005 by Sun Microsystems, Inc.
 *    901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License version 2.1, as published by the Free Software Foundation.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *    MA  02111-1307  USA
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_fpicker.hxx"

#include "ODMAFolderPicker.hxx"

#define _SVSTDARR_STRINGSDTOR
#include "svtools/svstdarr.hxx"

#ifndef  _COM_SUN_STAR_CONTAINER_XCONTENTENUMERATIONACCESS_HPP_
#include <com/sun/star/container/XContentEnumerationAccess.hpp>
#endif
#ifndef  _COM_SUN_STAR_CONTAINER_XSET_HPP_
#include <com/sun/star/container/XSet.hpp>
#endif
#ifndef  _COM_SUN_STAR_UNO_ANY_HXX_
#include <com/sun/star/uno/Any.hxx>
#endif
#ifndef  _CPPUHELPER_FACTORY_HXX_
#include <cppuhelper/factory.hxx>
#endif
#ifndef _COM_SUN_STAR_BEANS_XPROPERTYSET_HPP_
#include <com/sun/star/beans/XPropertySet.hpp>
#endif

#ifndef INCLUDED_SVTOOLS_PATHOPTIONS_HXX
#include <svtools/pathoptions.hxx>
#endif

#ifndef ODMA_LIB_HXX
#include <tools/prewin.h>
#include <odma_lib.hxx>
#include <tools/postwin.h>
#endif

// using ----------------------------------------------------------------

using namespace ::com::sun::star::beans;
using namespace ::com::sun::star::container;
using namespace ::com::sun::star::lang;
using namespace ::com::sun::star::uno;

//------------------------------------------------------------------------------------
// class ODMAFolderPicker
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
ODMAFolderPicker::ODMAFolderPicker( const Reference < XMultiServiceFactory >& xFactory ) :
	cppu::WeakComponentImplHelper2<
	  XFolderPicker,
	  XServiceInfo>( m_rbHelperMtx ),
	m_bUseDMS( sal_False )
{
	m_xInterface = xFactory->createInstance(
		::rtl::OUString::createFromAscii( "com.sun.star.ui.dialogs.SystemFolderPicker" ) );
}

// XExecutableDialog functions

void SAL_CALL ODMAFolderPicker::setTitle( const ::rtl::OUString& _rTitle ) throw (RuntimeException)
{
	if (m_bUseDMS)
		return;

	Reference< XExecutableDialog > xExecutableDialog( m_xInterface, UNO_QUERY );
	xExecutableDialog->setTitle( _rTitle);
}

sal_Int16 SAL_CALL ODMAFolderPicker::execute( )
	throw (RuntimeException)
{
	if (m_bUseDMS)
		throw new RuntimeException( );

	Reference< XExecutableDialog > xExecutableDialog( m_xInterface, UNO_QUERY );
	return xExecutableDialog->execute();
}

// XFolderPicker functions

void SAL_CALL ODMAFolderPicker::setDisplayDirectory( const ::rtl::OUString& aDirectory )
    throw( IllegalArgumentException, RuntimeException )
{
	if (m_bUseDMS)
		return;

	Reference< XFolderPicker > xFolderPicker( m_xInterface, UNO_QUERY );
	xFolderPicker->setDisplayDirectory( aDirectory );
}

::rtl::OUString SAL_CALL ODMAFolderPicker::getDisplayDirectory( )
	throw( RuntimeException )
{
	if (m_bUseDMS)
		return rtl::OUString();

	Reference< XFolderPicker > xFolderPicker( m_xInterface, UNO_QUERY );
	return xFolderPicker->getDisplayDirectory();
}

::rtl::OUString SAL_CALL ODMAFolderPicker::getDirectory( )
	throw( RuntimeException )
{
	if (m_bUseDMS)
		return rtl::OUString();

	Reference< XFolderPicker > xFolderPicker( m_xInterface, UNO_QUERY );
	return xFolderPicker->getDirectory();
}

void SAL_CALL ODMAFolderPicker::setDescription( const ::rtl::OUString& aDescription )
    throw( RuntimeException )
{
	if (m_bUseDMS)
		return;

	Reference< XFolderPicker > xFolderPicker( m_xInterface, UNO_QUERY );
	xFolderPicker->setDescription( aDescription );
}

// XServiceInfo

::rtl::OUString SAL_CALL ODMAFolderPicker::getImplementationName( )
	throw( RuntimeException )
{
	return impl_getStaticImplementationName();
}

sal_Bool SAL_CALL ODMAFolderPicker::supportsService( const ::rtl::OUString& sServiceName )
	throw( RuntimeException )
{
    Sequence< ::rtl::OUString > seqServiceNames = getSupportedServiceNames();
    const ::rtl::OUString* pArray = seqServiceNames.getConstArray();
    for ( sal_Int32 i = 0; i < seqServiceNames.getLength(); i++ )
	{
        if ( sServiceName == pArray[i] )
		{
            return sal_True ;
		}
	}
    return sal_False ;
}

Sequence< ::rtl::OUString > SAL_CALL ODMAFolderPicker::getSupportedServiceNames()
	throw( RuntimeException )
{
	return impl_getStaticSupportedServiceNames();
}

Sequence< ::rtl::OUString > ODMAFolderPicker::impl_getStaticSupportedServiceNames( )
{
    Sequence< ::rtl::OUString > seqServiceNames( 1 );
    ::rtl::OUString* pArray = seqServiceNames.getArray();
    pArray[0] = ::rtl::OUString::createFromAscii( "com.sun.star.ui.dialogs.ODMAFolderPicker" );
    return seqServiceNames ;
}

::rtl::OUString ODMAFolderPicker::impl_getStaticImplementationName( )
{
    return ::rtl::OUString::createFromAscii( "com.sun.star.svtools.ODMAFolderPicker" );
}

Reference< XInterface > SAL_CALL ODMAFolderPicker::impl_createInstance( const Reference< XComponentContext >& rxContext )
    throw( Exception )
{
	Reference< XMultiServiceFactory > xServiceManager (rxContext->getServiceManager(), UNO_QUERY_THROW);
	return Reference< XInterface >( *new ODMAFolderPicker( xServiceManager ) );
}
