/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: veto.cxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_dbaccess.hxx"

#ifndef DBACCESS_VETO_HXX
#include "veto.hxx"
#endif

/** === begin UNO includes === **/
/** === end UNO includes === **/

//........................................................................
namespace dbaccess
{
//........................................................................

    /** === begin UNO using === **/
    using ::com::sun::star::uno::Any;
    using ::com::sun::star::uno::RuntimeException;
    /** === end UNO using === **/

    //====================================================================
    //= Veto
    //====================================================================
    //--------------------------------------------------------------------
    Veto::Veto( const ::rtl::OUString& _rReason, const Any& _rDetails )
        :m_sReason( _rReason )
        ,m_aDetails( _rDetails )
    {
    }

    //--------------------------------------------------------------------
    Veto::~Veto()
    {
    }

    //--------------------------------------------------------------------
    ::rtl::OUString SAL_CALL Veto::getReason() throw (RuntimeException)
    {
        return m_sReason;
    }
    
    //--------------------------------------------------------------------
    Any SAL_CALL Veto::getDetails() throw (RuntimeException)
    {
        return m_aDetails;
    }

//........................................................................
} // namespace dbaccess
//........................................................................

