/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: CRowSetColumn.cxx,v $
 * $Revision: 1.16 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_dbaccess.hxx"

#ifndef DBACCESS_SHARED_DBASTRINGS_HRC
#include "dbastrings.hrc"
#endif
#ifndef _DBASHARED_APITOOLS_HXX_
#include "apitools.hxx"
#endif
#ifndef _COMPHELPER_TYPES_HXX_
#include <comphelper/types.hxx>
#endif
#ifndef _CPPUHELPER_TYPEPROVIDER_HXX_
#include <cppuhelper/typeprovider.hxx>
#endif
#ifndef DBACCESS_CORE_API_CROWSETCOLUMN_HXX
#include "CRowSetColumn.hxx"
#endif
#ifndef _COM_SUN_STAR_SDB_XCOLUMN_HPP_
#include <com/sun/star/sdb/XColumn.hpp>
#endif
#ifndef _COM_SUN_STAR_BEANS_PROPERTYATTRIBUTE_HPP_
#include <com/sun/star/beans/PropertyAttribute.hpp>
#endif


using namespace dbaccess;
//	using namespace connectivity;
using namespace ::com::sun::star::uno;
using namespace ::com::sun::star::beans;
//	using namespace ::com::sun::star::sdbcx;
using namespace ::com::sun::star::sdbc;
using namespace ::com::sun::star::sdb;
using namespace ::com::sun::star::container;
using namespace ::com::sun::star::lang;
//	using namespace ::com::sun::star::util;
using namespace cppu;
using namespace osl;


// -------------------------------------------------------------------------
//ORowSetColumn::ORowSetColumn(	const Reference < XResultSetMetaData >& _xMetaData,
//								const ::com::sun::star::uno::Reference < ::com::sun::star::sdbc::XRow >& _xRow,
//								sal_Int32 _nPos,
//								ORowSetMatrix::iterator& _rColumnValue,
//								ORowSetMatrix::iterator& _rEnd)
//	: ORowSetDataColumn(_xMetaData,_xRow,NULL,_nPos,_rColumnValue,_rEnd)
//{
//
//}
//------------------------------------------------------------------------------
Any SAL_CALL ORowSetColumn::queryInterface( const Type & _rType ) throw (RuntimeException)
{
    Any aReturn = OResultColumn::queryInterface(_rType);
    if (!aReturn.hasValue())
        aReturn = ::cppu::queryInterface(_rType,static_cast< XColumn* >(this));
    return aReturn;
}
// -------------------------------------------------------------------------
// comphelper::OPropertyArrayUsageHelper
//------------------------------------------------------------------------------
::cppu::IPropertyArrayHelper* ORowSetColumn::createArrayHelper( ) const
{
    BEGIN_PROPERTY_HELPER(30)
        DECL_PROP2(ALIGN,					sal_Int32,			BOUND,MAYBEVOID);
        DECL_PROP1(CATALOGNAME,				::rtl::OUString,	READONLY);
        DECL_PROP2(CONTROLDEFAULT,			::rtl::OUString,	BOUND,MAYBEVOID);
        DECL_PROP1_IFACE(CONTROLMODEL,		XPropertySet, 		BOUND		);
        DECL_PROP1(DISPLAYSIZE,				sal_Int32,			READONLY);
        DECL_PROP2(NUMBERFORMAT,			sal_Int32,			BOUND,MAYBEVOID);
        DECL_PROP2(HELPTEXT,			::rtl::OUString,	BOUND,MAYBEVOID);
        DECL_PROP1_BOOL(HIDDEN,	 							BOUND);
        DECL_PROP1_BOOL(ISAUTOINCREMENT,						READONLY);
        DECL_PROP1_BOOL(ISCASESENSITIVE,						READONLY);
        DECL_PROP1_BOOL(ISCURRENCY,								READONLY);
        DECL_PROP1_BOOL(ISDEFINITELYWRITABLE,					READONLY);		
        DECL_PROP1(ISNULLABLE,				sal_Int32,			READONLY);
        DECL_PROP1_BOOL(ISREADONLY,								READONLY);
        DECL_PROP1_BOOL(ISROWVERSION,                           READONLY);
        DECL_PROP1_BOOL(ISSEARCHABLE,							READONLY);
        DECL_PROP1_BOOL(ISSIGNED,								READONLY);
        DECL_PROP1_BOOL(ISWRITABLE,								READONLY);
        DECL_PROP1(LABEL,					::rtl::OUString,	READONLY);
        DECL_PROP1(NAME,					::rtl::OUString,	READONLY);
        DECL_PROP1(PRECISION,				sal_Int32,			READONLY);
        DECL_PROP2(RELATIVEPOSITION,	sal_Int32,			BOUND, MAYBEVOID);
        DECL_PROP1(SCALE,					sal_Int32,			READONLY);
        DECL_PROP1(SCHEMANAME,				::rtl::OUString,	READONLY);
        DECL_PROP1(SERVICENAME,				::rtl::OUString,	READONLY);
        DECL_PROP1(TABLENAME,				::rtl::OUString,	READONLY);
        DECL_PROP1(TYPE,					sal_Int32,			READONLY);
        DECL_PROP1(TYPENAME,				::rtl::OUString,	READONLY);
        DECL_PROP2(VALUE,					Any,				READONLY,BOUND);
        DECL_PROP1(WIDTH,					sal_Int32,			MAYBEVOID);
    END_PROPERTY_HELPER();
}

// cppu::OPropertySetHelper
//------------------------------------------------------------------------------
::cppu::IPropertyArrayHelper& ORowSetColumn::getInfoHelper()
{
    return *static_cast< ::comphelper::OPropertyArrayUsageHelper< ORowSetColumn >* >(this)->getArrayHelper();
}
// -------------------------------------------------------------------------
void SAL_CALL ORowSetColumn::getFastPropertyValue( Any& rValue, sal_Int32 nHandle ) const
{
    switch(nHandle)
    {
        case PROPERTY_ID_ALIGN:
        case PROPERTY_ID_NUMBERFORMAT:
        case PROPERTY_ID_RELATIVEPOSITION:
        case PROPERTY_ID_WIDTH:
        case PROPERTY_ID_HIDDEN:
        case PROPERTY_ID_CONTROLMODEL:
        case PROPERTY_ID_HELPTEXT:
        case PROPERTY_ID_CONTROLDEFAULT:
            OColumnSettings::getFastPropertyValue( rValue, nHandle );
            break;
        case PROPERTY_ID_VALUE:
            if ( !m_aColumnValue.isNull() && (*m_aColumnValue).isValid() )
                rValue = ((*m_aColumnValue)->get())[m_nPos].makeAny();
            break;
        default:
            ORowSetDataColumn::getFastPropertyValue(rValue,nHandle);
    }
}
// -------------------------------------------------------------------------
void SAL_CALL ORowSetColumn::setFastPropertyValue_NoBroadcast(sal_Int32 nHandle,const Any& rValue )throw (Exception)
{
    switch(nHandle)
    {
        case PROPERTY_ID_ALIGN:
        case PROPERTY_ID_NUMBERFORMAT:
        case PROPERTY_ID_RELATIVEPOSITION:
        case PROPERTY_ID_WIDTH:
        case PROPERTY_ID_HIDDEN:
        case PROPERTY_ID_CONTROLMODEL:
        case PROPERTY_ID_HELPTEXT:
        case PROPERTY_ID_CONTROLDEFAULT:
            OColumnSettings::setFastPropertyValue_NoBroadcast( nHandle, rValue );
            break;
        case PROPERTY_ID_VALUE:
            break;
        default:
            ORowSetDataColumn::setFastPropertyValue_NoBroadcast(nHandle,rValue );
    }
}
// -------------------------------------------------------------------------
sal_Bool SAL_CALL ORowSetColumn::convertFastPropertyValue( Any & rConvertedValue,
                                                            Any & rOldValue,
                                                            sal_Int32 nHandle,
                                                            const Any& rValue ) throw (IllegalArgumentException)
{
    sal_Bool bModified = sal_False;
    switch(nHandle)
    {
        case PROPERTY_ID_ALIGN:
        case PROPERTY_ID_NUMBERFORMAT:
        case PROPERTY_ID_RELATIVEPOSITION:
        case PROPERTY_ID_WIDTH:
        case PROPERTY_ID_HIDDEN:
        case PROPERTY_ID_CONTROLMODEL:
        case PROPERTY_ID_HELPTEXT:
        case PROPERTY_ID_CONTROLDEFAULT:
            bModified = OColumnSettings::convertFastPropertyValue( rConvertedValue, rOldValue, nHandle, rValue );
            break;
        case PROPERTY_ID_VALUE:
            rConvertedValue = rValue;
            getFastPropertyValue(rOldValue, PROPERTY_ID_VALUE);
            bModified = !::comphelper::compare(rConvertedValue, rOldValue);
            break;
        default:
            bModified = ORowSetDataColumn::convertFastPropertyValue(rConvertedValue, rOldValue, nHandle, rValue);

    }

    return bModified;
}
// com::sun::star::lang::XTypeProvider
//--------------------------------------------------------------------------
Sequence< Type > ORowSetColumn::getTypes() throw (RuntimeException)
{
    OTypeCollection aTypes(::getCppuType( (const Reference< XColumn > *)0 ),OColumn::getTypes());
    return aTypes.getTypes();
}
//--------------------------------------------------------------------------
