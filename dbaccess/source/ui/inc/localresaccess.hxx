/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: localresaccess.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _DBAUI_LOCALRESACCESS_HXX_
#define _DBAUI_LOCALRESACCESS_HXX_

#ifndef _SVTOOLS_LOCALRESACCESS_HXX_ 
#include <svtools/localresaccess.hxx>
#endif
#ifndef _DBAUI_MODULE_DBU_HXX_
#include "moduledbu.hxx"
#endif

//.........................................................................
namespace dbaui
{
//.........................................................................

//=========================================================================
//= LocalResourceAccess
//=========================================================================
/** helper class for acessing local resources
*/
typedef ::svt::OLocalResourceAccess LRA_Base;
class LocalResourceAccess : protected LRA_Base
{
    OModuleClient m_aModuleClient;
public:
    inline LocalResourceAccess( sal_uInt16 _nId, RESOURCE_TYPE _rType )
        :LRA_Base( ModuleRes( _nId ), _rType )
    {
    }
};

//.........................................................................
}	// namespace dbaui
//.........................................................................

#endif // _DBAUI_LOCALRESACCESS_HXX_

