/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: GeneralUndo.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef DBAUI_GENERALUNDO_HXX
#define DBAUI_GENERALUNDO_HXX

#ifndef _UNDO_HXX 
#include <svtools/undo.hxx>
#endif
#ifndef _DBAUI_MODULE_DBU_HXX_
#include "moduledbu.hxx"
#endif

namespace dbaui
{
    // ================================================================================================
    // SbaCommentUndoAction - Undo-Basisklasse fuer Aktionen, deren GetComment einen aus einer Sba-Ressource
    // geladenen String liefert

    class OCommentUndoAction : public SfxUndoAction
    {
        OModuleClient    m_aModuleClient;

    protected:
        String	                m_strComment; // undo, redo comment

    public:
        TYPEINFO();
        OCommentUndoAction(USHORT nCommentID) { m_strComment = String(ModuleRes(nCommentID)); }

        virtual UniString	GetComment() const { return m_strComment; }
    };															   
}
#endif // DBAUI_GENERALUNDO_HXX


