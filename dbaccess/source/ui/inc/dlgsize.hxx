/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: dlgsize.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _DBAUI_DLGSIZE_HXX
#define _DBAUI_DLGSIZE_HXX

#ifndef _DIALOG_HXX //autogen
#include <vcl/dialog.hxx>
#endif

#ifndef _FIELD_HXX //autogen
#include <vcl/field.hxx>
#endif

#ifndef _BUTTON_HXX //autogen
#include <vcl/button.hxx>
#endif

#ifndef _FIXED_HXX //autogen
#include <vcl/fixed.hxx>
#endif

//.........................................................................
namespace dbaui
{
//.........................................................................

    class DlgSize : public ModalDialog
    {
    private:
        sal_Int32		m_nPrevValue, m_nStandard;
        void			SetValue( sal_Int32 nVal );

    protected:
        DECL_LINK( CbClickHdl, Button * );

        FixedText		aFT_VALUE;
        MetricField		aMF_VALUE;
        CheckBox		aCB_STANDARD;
        OKButton		aPB_OK;
        CancelButton	aPB_CANCEL;
        HelpButton		aPB_HELP;

    public:
        DlgSize( Window * pParent, sal_Int32 nVal, BOOL bRow, sal_Int32 _nAlternativeStandard = -1 );
        ~DlgSize();

        sal_Int32 GetValue();
    };
//.........................................................................
}	// namespace dbaui
//.........................................................................

#endif // _DBAUI_DLGSIZE_HXX

