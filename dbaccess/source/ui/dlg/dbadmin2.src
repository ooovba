/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: dbadmin2.src,v $
 * $Revision: 1.10 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _DBA_DBACCESS_HELPID_HRC_
#include "dbaccess_helpid.hrc"
#endif

#ifndef _DBU_DLG_HRC_
#include "dbu_dlg.hrc"
#endif
#ifndef _DBAUI_DBADMIN_HRC_
#include "dbadmin.hrc"
#endif
#ifndef DBACCESS_UI_BROWSER_ID_HXX
#include "browserids.hxx"
#endif
#ifndef DBAUI_TOOLBOX_HXX
#include "toolbox.hrc"
#endif
#ifndef _DBAUI_AUTOCONTROLS_HRC_
#include "AutoControls.hrc"
#endif

//.........................................................................

String STR_ENTER_CONNECTION_PASSWORD
{
    Text [ en-US ] = "A password is needed to connect to the data source \"$name$\".";
};

String STR_QUERY_DROP_ALL
{
    Text[ en-US ] = "Do you want to delete all selected items?";
};

String STR_ASK_FOR_DIRECTORY_CREATION
{
    Text [ en-US ] = "The directory\n\n$path$\n\ndoes not exist. Should it be created?";
};

String STR_COULD_NOT_CREATE_DIRECTORY
{
    Text [ en-US ] = "The directory $name$ could not be created.";
};

String STR_ADDRESSBOOK_SYSTEM
{
    Text[ en-US ] = "Windows address book";
};
String STR_ADDRESSBOOK_OUTLOOK
{
    Text[ en-US ] = "MS Outlook";
};
String STR_ADDRESSBOOK_MOZILLA
{
    Text[ en-US ] = "Mozilla address book";
};
String STR_ADDRESSBOOK_THUNDERBIRD
{
    Text[ en-US ] = "Thunderbird address book";
};
String STR_ADDRESSBOOK_EVOLUTION
{
    Text[ en-US ] = "Evolution address book";
};
String STR_ADDRESSBOOK_LDAP
{
    Text[ en-US ] = "LDAP address book";
};

String STR_HINT_READONLY_CONNECTION
{
    Text [ en-US ] = "(Connection is read-only)";
};

String STR_HINT_CONNECTION_NOT_CAPABLE
{
    Text [ en-US ] = "(Not supported by this connection)";
};

#define EDIT_SIZE_X		50
#define FT_SIZE_X		90
#define WIN_X			220
#define WIN_Y			72

ModalDialog DLG_DOMAINPASSWORD
{
    Border = TRUE ;
    Moveable = TRUE ;
    OutputSize = TRUE ;
    SVLook = TRUE ;
    Size = MAP_APPFONT ( WIN_X , WIN_Y ) ;
    Text[ en-US ] = "Convert Database";

    FixedLine FT_PASSWORD
    {
        Pos = MAP_APPFONT ( 3 , 3 ) ;
        Size = MAP_APPFONT ( WIN_X - 3 - 6 - 6 - 50 , 8 ) ;
        Text[ en-US ] = "Please enter the ~password for the user 'DOMAIN'.";
    };

    Edit ET_PASSWORD
    {
        Border = TRUE ;
        Pos = MAP_APPFONT ( 12 + FT_SIZE_X , 16 ) ;
        Size = MAP_APPFONT ( EDIT_SIZE_X , 12 ) ;
        PassWord = TRUE ;
    };
    OKButton BTN_PASSWORD_OK
    {
        Pos = MAP_APPFONT ( WIN_X - 56 , 6 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        DefButton = TRUE ;
    };
    CancelButton BTN_PASSWORD_CANCEL
    {
        Pos = MAP_APPFONT ( WIN_X - 56 , 23 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    HelpButton BTN_PASSWORD_HELP
    {
        Pos = MAP_APPFONT ( WIN_X - 56 , 43 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };	
};

#define PAGE_X_T  (PAGE_X -80)
#define PAGE_Y_T  (PAGE_Y -50)

TabPage PAGE_TABLESUBSCRIPTION
{
    SVLook = TRUE ;
    Hide = TRUE;
    Pos = MAP_APPFONT ( 0 , 0 ) ;
    Size = MAP_APPFONT ( PAGE_X_T, PAGE_Y_T) ;
    HelpId = HID_DSADMIN_TABLE_SUBSCRIPTION;
    
    Text [ en-US ] = "Tables Filter" ;

    FixedLine FL_SEPARATOR1
    {
        Pos = MAP_APPFONT ( RELATED_CONTROLS , UNRELATED_CONTROLS ) ;
        Size = MAP_APPFONT ( PAGE_X_T - 2* RELATED_CONTROLS, FIXEDTEXT_HEIGHT ) ;
        Text [ en-US ] = "Tables and table filter";
    };
    Control CTL_TABLESUBSCRIPTION
    {
        Pos		= MAP_APPFONT ( UNRELATED_CONTROLS , UNRELATED_CONTROLS + FIXEDTEXT_HEIGHT + RELATED_CONTROLS) ;
        Size	= MAP_APPFONT ( PAGE_X_T - 2*UNRELATED_CONTROLS , 81 ) ;
        Group	= TRUE;
        Border	= TRUE ;
        TabStop = TRUE ;
        HelpId = HID_DSADMIN_TABLE_SELECTOR;
    };
    FixedText FT_FILTER_EXPLANATION
    {
        Pos = MAP_APPFONT ( UNRELATED_CONTROLS , 2*UNRELATED_CONTROLS + FIXEDTEXT_HEIGHT + RELATED_CONTROLS + 81 ) ;
        Size	= MAP_APPFONT ( PAGE_X_T - 2*UNRELATED_CONTROLS , 16 ) ;
        HelpId = HID_DSADMIN_FILTER_EXPLANATION;
        WordBreak = TRUE;
        Text [ en-US ] = "Mark the tables that should be visible for the applications.";
    };
};

