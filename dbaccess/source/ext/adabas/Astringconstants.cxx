/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Astringconstants.cxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

//============================================================
//= property names
//============================================================

IMPLEMENT_CONSTASCII_USTRING(PROPERTY_CREATECATALOG, "CreateCatalog");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_DATABASENAME, "DatabaseName");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_CONTROL_USER, "ControlUser");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_CONTROL_PASSWORD, "ControlPassword");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_USER, "User");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_PASSWORD, "Password");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_SYSDEVSPACE, "SYSDEVSPACE");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_DATADEVSPACE, "DataDevSpace");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_TRANSACTION_LOG, "TRANSACTION_LOG");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_BACKUPNAME, "Backup");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_CACHESIZE, "CacheSize");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_CACHESIZE_INCREMENT, "DataCacheSizeIncrement");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_RESTOREDATABASE, "RestoreDatabase");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_DOMAINPASSWORD, "DomainPassword");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_LOGDEVSIZE, "LogDevSize");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_DATADEVSIZE, "DataDevSize");
IMPLEMENT_CONSTASCII_USTRING(PROPERTY_SHUTDOWN, "ShutdownDatabase");
//============================================================
//= service names
//============================================================
//============================================================
//= SQLSTATE
//============================================================
IMPLEMENT_CONSTASCII_USTRING(SQLSTATE_GENERAL, "01000");





