/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: IBM Corporation
 *
 *  Copyright: 2008 by IBM Corporation
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): _______________________________________
 *
 *
 ************************************************************************/
/*************************************************************************
 * @file 
 *  For LWP filter architecture prototype
 ************************************************************************/
/*************************************************************************
 * Change History
 Jun 2005			Created
 ************************************************************************/
#include "lwpchangemgr.hxx"
#include <rtl/ustring.hxx>
#include "lwppara.hxx"
#include "lwpfribheader.hxx"
#include "lwpfribptr.hxx"
#include "lwpfrib.hxx"
#include "lwpstory.hxx"
#include "lwpfribsection.hxx"
#include "lwpsection.hxx"
#include "lwpfribbreaks.hxx"
#include "lwpfribframe.hxx"
#include "lwpfribtable.hxx"
#include "lwphyperlinkmgr.hxx"
#include "lwpfootnote.hxx"
#include "lwpnotes.hxx"
#include "lwpfribmark.hxx"
#include "xfilter/xftextspan.hxx"
#include "xfilter/xftextcontent.hxx"
#include "xfilter/xftabstop.hxx"
#include "xfilter/xflinebreak.hxx"
#include "xfilter/xfstylemanager.hxx"
#include "xfilter/xfhyperlink.hxx"

LwpChangeMgr::LwpChangeMgr()
{
	m_nCounter = 0;
	m_DocFribMap.clear();
	m_HeadFootFribMap.clear();	
	m_pFribMap = &m_DocFribMap;
	m_ChangeList.clear();
}
LwpChangeMgr::~LwpChangeMgr()
{
	m_pFribMap=NULL;
	m_DocFribMap.clear();
	m_HeadFootFribMap.clear();
	m_ChangeList.clear();
}

void LwpChangeMgr::AddChangeFrib(LwpFrib* pFrib)
{
	m_nCounter++;
	OUString sID = A2OUSTR("ct")+ Int32ToOUString(m_nCounter);
	m_pFribMap->insert(std::pair<LwpFrib*,OUString>(pFrib,sID));
}

OUString LwpChangeMgr::GetChangeID(LwpFrib* pFrib)
{
	std::map<LwpFrib*,OUString>::iterator iter;
	iter = m_pFribMap->find(pFrib);
	if (iter == m_pFribMap->end())
		return A2OUSTR("");
	else
		return iter->second;
}

void LwpChangeMgr::ConvertAllChange(IXFStream* pStream)
{
	std::map<LwpFrib*,OUString>::iterator iter;
	for (iter=m_DocFribMap.begin();iter !=m_DocFribMap.end();iter++)
	{
		if (iter->first->GetRevisionType() == LwpFrib::REV_INSERT)
		{
			XFChangeInsert* pInsert = new XFChangeInsert;
			pInsert->SetChangeID(iter->second);
			pInsert->SetEditor(iter->first->GetEditor());			
			m_ChangeList.push_back(pInsert);	
		}
		else if (iter->first->GetRevisionType() == LwpFrib::REV_DELETE)
		{
			XFChangeDelete* pDelete = new XFChangeDelete;
			pDelete->SetChangeID(iter->second);
			pDelete->SetEditor(iter->first->GetEditor());
///			ConvertFribContent(pDelete,iter->first); //delete tmp,note by ,2005/7/1
			m_ChangeList.push_back(pDelete);
		}
	}
	
	std::vector<XFChangeRegion*>::iterator iter1;
	pStream->GetAttrList()->Clear();
	if (m_ChangeList.size() == 0)
			return;
//Add by ,for disable change tracking,2005/11/21
	pStream->GetAttrList()->AddAttribute( A2OUSTR("text:track-changes"),A2OUSTR("false"));
//Add end			
	pStream->StartElement( A2OUSTR("text:tracked-changes") );
	for (iter1=m_ChangeList.begin();iter1 !=m_ChangeList.end();iter1++)
		(*iter1)->ToXml(pStream);

	pStream->EndElement(A2OUSTR("text:tracked-changes"));	
	
//	m_DocFribMap.clear();
	
	for (iter1=m_ChangeList.begin();iter1 !=m_ChangeList.end();iter1++)
	{
		delete *iter1;
		*iter1=NULL;
	}
	m_ChangeList.clear();		
}
void LwpChangeMgr::SetHeadFootFribMap(sal_Bool bFlag)
{
	if (bFlag == sal_True)
		m_pFribMap = &m_HeadFootFribMap;
	else
	{
		m_HeadFootFribMap.clear();
		m_pFribMap = &m_DocFribMap;
	}
}

void LwpChangeMgr::SetHeadFootChange(XFContentContainer* pCont)
{
	std::map<LwpFrib*,OUString>::iterator iter;
	XFChangeList* pChangeList = new XFChangeList;
	
	for (iter=m_HeadFootFribMap.begin();iter !=m_HeadFootFribMap.end();iter++)
	{
		if (iter->first->GetRevisionType() == LwpFrib::REV_INSERT)
		{
			XFChangeInsert* pInsert = new XFChangeInsert;
			pInsert->SetChangeID(iter->second);
			pInsert->SetEditor(iter->first->GetEditor());
			pChangeList->Add(pInsert);	
		}
		else if (iter->first->GetRevisionType() == LwpFrib::REV_DELETE)
		{
			XFChangeDelete* pDelete = new XFChangeDelete;
			pDelete->SetChangeID(iter->second);
			pDelete->SetEditor(iter->first->GetEditor());
			pChangeList->Add(pDelete);
		}
	}
	
	pCont->Add(pChangeList);
	
//	m_HeadFootFribMap.clear();
}

void LwpChangeMgr::ConvertFribContent(XFContentContainer* pCont, LwpFrib* pFrib)
{
	XFParagraph* pXFPara = new XFParagraph;
	LwpPara* pPara = pFrib->GetMyPara();
	if (pPara)
		pXFPara->SetStyleName(pPara->GetStyleName());
		
        switch(pFrib->GetType())//copy code from class lwpfribptr
        {
	    case FRIB_TAG_TEXT:
		{
			LwpFribText* textFrib= static_cast<LwpFribText*>(pFrib);
			textFrib->XFConvert(pXFPara,pPara->GetStory());
		}
		    break;
		case FRIB_TAG_TAB:
		{
			LwpFribTab* tabFrib = static_cast<LwpFribTab*>(pFrib);
			if (pFrib->m_ModFlag)
			{	
				XFTextSpan *pSpan = new XFTextSpan();
				pSpan->SetStyleName(tabFrib->GetStyleName());
				XFTabStop *pTab = new XFTabStop();
				pSpan->Add(pTab);               
				pXFPara->Add(pSpan); 
			}    
			else
			{
				XFTabStop *pTab = new XFTabStop();
				pXFPara->Add(pTab);               
			}	
		}	    
		    break;
/*		case FRIB_TAG_SECTION:
		{
			delete pXFPara;
			LwpFribSection* pSectionFrib = static_cast<LwpFribSection*>(pFrib);			
			pSectionFrib->ParseSection();	
		}
		break;
		case FRIB_TAG_PAGEBREAK:
		{
			LwpFribPageBreak* pPageBreak = static_cast<LwpFribPageBreak*>(pFrib);
			LwpPageLayout* pLayout = static_cast<LwpPageLayout*>(pPageBreak->GetLayout()->obj());
			if(pLayout)
			{							
				pPageBreak->ParseLayout();
			}
			else
			{
				if (pPageBreak->IsLastFrib() == sal_True)
				{
					pXFPara->SetStyleName( pPageBreak->GetStyleName() );
				}
				else
				{
					//parse pagebreak
					XFParagraph *pNewPara = new XFParagraph();
					pNewPara->SetStyleName(pFrib->GetStyleName());	
					pPara->AddXFContent(pNewPara);
				}
			}		
		}
			break;
		case FRIB_TAG_COLBREAK:
		{
			XFParagraph *pNewPara = new XFParagraph();
			pNewPara->SetStyleName(pFrib->GetStyleName());	
			pPara->AddXFContent(pNewPara);
		}
			break;	
*/		case FRIB_TAG_LINEBREAK:
		{
			XFLineBreak *pLineBreak = new XFLineBreak();
			pXFPara->Add(pLineBreak);  			
		}
			break;	
		case FRIB_TAG_UNICODE: //fall through
		case FRIB_TAG_UNICODE2: //fall through
		case FRIB_TAG_UNICODE3: //fall through
		{
			LwpFribUnicode* unicodeFrib= static_cast<LwpFribUnicode*>(pFrib);
			unicodeFrib->XFConvert(pXFPara,pPara->GetStory());
		}
			break;
		case FRIB_TAG_HARDSPACE:
		{
			rtl::OUString sHardSpace(sal_Unicode(0x00a0));
			LwpHyperlinkMgr* pHyperlink = 
					pPara->GetStory()->GetHyperlinkMgr();
			if (pHyperlink->GetHyperlinkFlag())
				pFrib->ConvertHyperLink(pXFPara,pHyperlink,sHardSpace);
			else
				pFrib->ConvertChars(pXFPara,sHardSpace);
		}
			break;
		case FRIB_TAG_SOFTHYPHEN:
		{
			rtl::OUString sSoftHyphen(sal_Unicode(0x00ad));
			pFrib->ConvertChars(pXFPara,sSoftHyphen);
		}
			break;
/*		case FRIB_TAG_FRAME:
		{
			LwpFribFrame* frameFrib= static_cast<LwpFribFrame*>(pFrib);
			LwpObject* pLayout = frameFrib->GetLayout();
			if (pLayout->GetTag() == VO_DROPCAPLAYOUT)
			{
				pPara->GetFoundry()->GetDropcapMgr()->SetXFPara(pXFPara); 
				//LwpObject* pDropCap = frameFrib->GetLayout();
				//pDropCap ->XFConvert(m_pXFPara);
			}
			//pLayout->XFConvert(m_pXFPara);
			frameFrib->XFConvert(pXFPara);
		}
			break;
*/		case FRIB_TAG_CHBLOCK:
		{
			LwpFribCHBlock* chbFrib = static_cast<LwpFribCHBlock*>(pFrib);
			chbFrib->XFConvert(pXFPara,pPara->GetStory());
		}
			break;
/*		case FRIB_TAG_TABLE:
		{
			LwpFribTable* tableFrib = static_cast<LwpFribTable*>(pFrib);
			//tableFrib->XFConvert(m_pPara->GetXFContainer());
			tableFrib->XFConvert(pXFPara);
		}
			break;
*/		case FRIB_TAG_BOOKMARK:
		{
			LwpFribBookMark* bookmarkFrib = static_cast<LwpFribBookMark*>(pFrib);
			bookmarkFrib->XFConvert(pXFPara);
		}
		break;	
		case FRIB_TAG_FOOTNOTE:
		{			
			LwpFribFootnote* pFootnoteFrib = static_cast<LwpFribFootnote*>(pFrib);
			pFootnoteFrib->XFConvert(pXFPara); 
			break;
		}
		case FRIB_TAG_FIELD:
		{
			LwpFribField* fieldFrib = static_cast<LwpFribField*>(pFrib);
			fieldFrib->XFConvert(pXFPara); 		
			break;
		}
		case FRIB_TAG_NOTE:
		{
			LwpFribNote* pNoteFrib = static_cast<LwpFribNote*>(pFrib);
			pNoteFrib->XFConvert(pXFPara); 
			break;
		}
		case FRIB_TAG_PAGENUMBER:
		{
			LwpFribPageNumber* pagenumFrib = static_cast<LwpFribPageNumber*>(pFrib);
			pagenumFrib->XFConvert(pXFPara); 
			break;
		}
		case FRIB_TAG_DOCVAR:
		{
			LwpFribDocVar* docFrib = static_cast<LwpFribDocVar*>(pFrib);
			docFrib->XFConvert(pXFPara); 
			break;
		}
		case FRIB_TAG_RUBYMARKER:
		{
			LwpFribRubyMarker* rubyFrib = static_cast<LwpFribRubyMarker*>(pFrib);
			rubyFrib->XFConvert(pXFPara); 
			break;
		}
		case FRIB_TAG_RUBYFRAME:
		{
			LwpFribRubyFrame* rubyfrmeFrib = static_cast<LwpFribRubyFrame*>(pFrib);
			rubyfrmeFrib->XFConvert(pXFPara); 
			break;
		}
		default :
			break;
	}	
	pCont->Add(pXFPara);
}

