#ifndef _WORDPERFECTIMPORTFILTER_HXX
#define _WORDPERFECTIMPORTFILTER_HXX

#include <com/sun/star/document/XFilter.hpp>
#include <com/sun/star/document/XImporter.hpp>
#include <com/sun/star/document/XExtendedFilterDetection.hpp>
#include <com/sun/star/lang/XInitialization.hpp>
#include <com/sun/star/lang/XServiceInfo.hpp>
#include <com/sun/star/xml/sax/XDocumentHandler.hpp>
#include <cppuhelper/implbase5.hxx>
#include <rtl/ustrbuf.hxx>

enum FilterType 
{
	FILTER_IMPORT,
	FILTER_EXPORT
};
/* This component will be instantiated for both import or export. Whether it calls
 * setSourceDocument or setTargetDocument determines which Impl function the filter
 * member calls */
class LotusWordProImportFilter : public cppu::WeakImplHelper5 
< 
	com::sun::star::document::XFilter,
	com::sun::star::document::XImporter,
	com::sun::star::document::XExtendedFilterDetection,
	com::sun::star::lang::XInitialization,
	com::sun::star::lang::XServiceInfo
>
{
private:

protected:
	// oo.org declares
	::com::sun::star::uno::Reference< ::com::sun::star::lang::XMultiServiceFactory > mxMSF;
	::com::sun::star::uno::Reference< ::com::sun::star::lang::XComponent > mxDoc;
	::rtl::OUString msFilterName;
	::com::sun::star::uno::Reference< ::com::sun::star::xml::sax::XDocumentHandler > mxHandler;

	FilterType meType;

	sal_Bool SAL_CALL importImpl( const ::com::sun::star::uno::Sequence< ::com::sun::star::beans::PropertyValue >& aDescriptor ) 
		throw (::com::sun::star::uno::RuntimeException);

public:
	LotusWordProImportFilter( const ::com::sun::star::uno::Reference< ::com::sun::star::lang::XMultiServiceFactory > &rxMSF)
        : mxMSF( rxMSF ) {}
	virtual ~LotusWordProImportFilter() {}

	// XFilter
        virtual sal_Bool SAL_CALL filter( const ::com::sun::star::uno::Sequence< ::com::sun::star::beans::PropertyValue >& aDescriptor ) 
		throw (::com::sun::star::uno::RuntimeException);
        virtual void SAL_CALL cancel(  ) 
		throw (::com::sun::star::uno::RuntimeException);

	// XImporter
        virtual void SAL_CALL setTargetDocument( const ::com::sun::star::uno::Reference< ::com::sun::star::lang::XComponent >& xDoc ) 
		throw (::com::sun::star::lang::IllegalArgumentException, ::com::sun::star::uno::RuntimeException);

 	//XExtendedFilterDetection
	virtual ::rtl::OUString SAL_CALL detect( com::sun::star::uno::Sequence< com::sun::star::beans::PropertyValue >& Descriptor ) 
		throw( com::sun::star::uno::RuntimeException );

	// XInitialization
        virtual void SAL_CALL initialize( const ::com::sun::star::uno::Sequence< ::com::sun::star::uno::Any >& aArguments ) 
		throw (::com::sun::star::uno::Exception, ::com::sun::star::uno::RuntimeException);

	// XServiceInfo
        virtual ::rtl::OUString SAL_CALL getImplementationName(  ) 
		throw (::com::sun::star::uno::RuntimeException);
        virtual sal_Bool SAL_CALL supportsService( const ::rtl::OUString& ServiceName ) 
		throw (::com::sun::star::uno::RuntimeException);
        virtual ::com::sun::star::uno::Sequence< ::rtl::OUString > SAL_CALL getSupportedServiceNames(  ) 
		throw (::com::sun::star::uno::RuntimeException);

};

::rtl::OUString LotusWordProImportFilter_getImplementationName()
	throw ( ::com::sun::star::uno::RuntimeException );

sal_Bool SAL_CALL LotusWordProImportFilter_supportsService( const ::rtl::OUString& ServiceName ) 
	throw ( ::com::sun::star::uno::RuntimeException );

::com::sun::star::uno::Sequence< ::rtl::OUString > SAL_CALL LotusWordProImportFilter_getSupportedServiceNames(  ) 
	throw ( ::com::sun::star::uno::RuntimeException );

::com::sun::star::uno::Reference< ::com::sun::star::uno::XInterface >
SAL_CALL LotusWordProImportFilter_createInstance( const ::com::sun::star::uno::Reference< ::com::sun::star::lang::XMultiServiceFactory > & rSMgr)
	throw ( ::com::sun::star::uno::Exception );

#endif
