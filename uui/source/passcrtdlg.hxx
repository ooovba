/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: passcrtdlg.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef UUI_PASSCRTDLG_HXX
#define UUI_PASSCRTDLG_HXX

#include <com/sun/star/task/PasswordRequestMode.hpp>
#include <svtools/stdctrl.hxx>
#include <vcl/dialog.hxx>
#include <vcl/edit.hxx>
#include <vcl/fixed.hxx>
#include <vcl/button.hxx>

//============================================================================
class PasswordCreateDialog : public ModalDialog
{
    FixedText		aFTPasswordCrt;
    Edit			aEDPasswordCrt;
    FixedText		aFTPasswordRepeat;
    Edit			aEDPasswordRepeat;
    FixedText       aFTWarning;
    FixedLine       aFixedLine1;
    OKButton		aOKBtn;
    CancelButton	aCancelBtn;
    HelpButton		aHelpBtn;

    ResMgr*											pResourceMgr;
    sal_uInt16										nMinLen;

    DECL_LINK( OKHdl_Impl, OKButton * );
    DECL_LINK( EditHdl_Impl, Edit * );

public:
    PasswordCreateDialog( Window* pParent, ResMgr * pResMgr, bool bMSCryptoMode = false );

    String			GetPassword() const { return aEDPasswordCrt.GetText(); }
};

#endif // UUI_PASSCRTDLG_HXX
