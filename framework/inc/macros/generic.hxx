/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: generic.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __FRAMEWORK_MACROS_GENERIC_HXX_
#define __FRAMEWORK_MACROS_GENERIC_HXX_

//_________________________________________________________________________________________________________________
//	includes
//_________________________________________________________________________________________________________________

#include <rtl/ustring.hxx>
#include <rtl/textenc.h>

//*****************************************************************************************************************
//	generic macros
//*****************************************************************************************************************

/*_________________________________________________________________________________________________________________
    DECLARE_ASCII( SASCIIVALUE )

    Use it to declare a constant ascii value at compile time in code.
    zB. OUSting sTest = DECLARE_ASCII( "Test" )
_________________________________________________________________________________________________________________*/

#define	DECLARE_ASCII( SASCIIVALUE )																			\
    ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( SASCIIVALUE ) )

/*_________________________________________________________________________________________________________________
    U2B( SUNICODEVALUE )
    B2U( SASCIIVALUE )
    U2B_ENC( SUNICODEVALUE, AENCODING )
    B2U_ENC( SASCIIVALUE, AENCODING )

    Use it to convert unicode strings to ascii values and reverse ...
    We use UTF8 as default textencoding. If you will change this use U2B_ENC and B2U_ENC!
_________________________________________________________________________________________________________________*/

#define	U2B( SUNICODEVALUE )																					\
    ::rtl::OUStringToOString( SUNICODEVALUE, RTL_TEXTENCODING_UTF8 )

#define	B2U( SASCIIVALUE )																						\
    ::rtl::OStringToOUString( SASCIIVALUE, RTL_TEXTENCODING_UTF8 )

#define	U2B_ENC( SUNICODEVALUE, AENCODING )																		\
    ::rtl::OUStringToOString( SUNICODEVALUE, AENCODING )

#define	B2U_ENC( SASCIIVALUE, AENCODING )																		\
    ::rtl::OStringToOUString( SASCIIVALUE, AENCODING )

//*****************************************************************************************************************
//	end of file
//*****************************************************************************************************************

#endif	//	#ifndef __FRAMEWORK_MACROS_GENERIC_HXX_
