(*
This script is meant to
	1) Identify installed instances of OOo
	2) check whether the user has write-access (and if not
	ask for authentification)
	3) install the shipped tarball
*)

-- strings for localisations - to be meant to be replaced
-- by a makefile or similar
set OKLabel to "OK"
set AbortLabel to "Abort"
set intro to "Welcome to the OpenOffice.org Languagepack Installer

The next step will try to detect installed versions of OpenOffice.org
This might take a moment"
set chooseMyOwn to "not listed (choose location in an extra step)"
set listPrompt to "Choose the installation for which you want to install the languagepack"
set chooseManual to "Point the dialog to your OOo installation"
set listOKLabel to "Continue"
set listCancelLabel to "Abort installation"
set appInvalid to " doesn't look like an OpenOffice.org installation
If the chosen application really is OpenOffice.org, please contact the developers. Otherwise just run the installer again and choose a valid OpenOffice.org installation" -- string will begin with the chosen application's name
set startInstall to "Click " & OKLabel & " to start the installation

(installation might take a minute...)"
set IdentifyQ to "Installation failed, most likely your account doesn't have the necessary privileges. Do you want to identify as administrator and try again?"
set IdentifyYES to "Yes, identify"
set IdentifyNO to "No, abort installation"
set installFailed to "Installation failed"
set installComplete to "Installation is completed - you should now be able to switch the UI language"

set sourcedir to (do shell script "dirname " & quoted form of POSIX path of (path to of me))

display dialog intro buttons {AbortLabel, OKLabel} default button 2

if (button returned of result) is AbortLabel then
	return 2
end if

set the found_ooos to (do shell script "mdfind \"kMDItemContentType == 'com.apple.application-bundle' && kMDItemDisplayName == 'OpenOffice.org*' && kMDItemDisplayName != 'OpenOffice.org Languagepack.app'\"") & "
" & chooseMyOwn

-- the choice returned is of type "list"
set the choice to (choose from list of paragraphs in found_ooos default items (get first paragraph of found_ooos) with prompt listPrompt OK button name listOKLabel cancel button name listCancelLabel)

if choice is false then
	-- do nothing, the user cancelled the installation
	return 2 --aborted by user
else if (choice as string) is chooseMyOwn then
	-- yeah, one needs to use "choose file", otherwise
	-- the user would not be able to select the .app
	set the choice to POSIX path of (choose file with prompt chooseManual of type "com.apple.application-bundle" without showing package contents and invisibles)
end if

-- now only check whether the path is really from OOo

try
	do shell script "grep '<string>org.openoffice.script</string>'   " & quoted form of (choice as string) & "/Contents/Info.plist"
on error
	display dialog (choice as string) & appInvalid buttons {OKLabel} default button 1 with icon 0
	return 3 --wrong target-directory
end try

display dialog startInstall buttons {AbortLabel, OKLabel} default button 2

if (button returned of result) is AbortLabel then
	return 2
end if

set tarCommand to "/usr/bin/tar -C " & quoted form of (choice as string) & " -xjf " & quoted form of sourcedir & "/tarball.tar.bz2"
try
	do shell script tarCommand
	
on error errMSG number errNUM
	display dialog IdentifyQ buttons {IdentifyYES, IdentifyNO} with icon 2
	if (button returned of result) is IdentifyYES then
		try
			do shell script tarCommand with administrator privileges
		on error errMSG number errNUM
			display dialog installFailed buttons {OKLabel} default button 1 with icon 0
			-- -60005 username/password wrong
			-- -128   aborted by user
			-- 2 error from tar - tarball not found (easy to test)
			return errNUM
		end try
	else
		return 2 -- aborted by user
	end if
end try

display dialog installComplete buttons {OKLabel} default button 1
