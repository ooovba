/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: prs_cpp.hxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef ADC_CPP_PRS_CPP_HXX
#define ADC_CPP_PRS_CPP_HXX



// USED SERVICES
    // BASE CLASSES
#include <autodoc/prs_code.hxx>
    // COMPONENTS
    // PARAMETERS

namespace cpp
{

struct S_RunningData;

class Cpluplus_Parser : public autodoc::CodeParser_Ifc
{
  public:
                        Cpluplus_Parser();
    virtual				~Cpluplus_Parser();


    virtual void		Setup(
                            ary::Repository &   o_rRepository,
                            const autodoc::DocumentationParser_Ifc &
                                                i_rDocumentationInterpreter );

    virtual void		Run(
                            const autodoc::FileCollector_Ifc &
                                                i_rFiles );
  private:
    Dyn<S_RunningData>  pRunningData;
};




}   // namespace cpp
#endif
