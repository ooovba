/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: ctokdeal.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef ADC_CPP_CTOKDEAL_HXX
#define ADC_CPP_CTOKDEAL_HXX



// USED SERVICES
    // BASE CLASSES
#include <tokens/tokdeal.hxx>
    // COMPONENTS
    // PARAMETERS


namespace cpp
{

class Token;
class Tok_UnblockMacro;


class TokenDealer : virtual public ::TokenDealer
{
  public:

    virtual void		Deal_CppCode(
                            cpp::Token & 		let_drToken ) = 0;

    /** This is to be used only by the internal macro expander
        ( ::cpp::PreProcessor ).
        These tokens are inserted into the source text temporary to make clear,
        where a specific macro replacement ends and therefore the macro's name
        becomes valid again.

        @see ::cpp::Tok_UnblockMacro
        @see ::cpp::PreProcessor
    */
    virtual void        Deal_Cpp_UnblockMacro(
                            Tok_UnblockMacro &  let_drToken ) = 0;
};



} // namespace cpp



#endif

