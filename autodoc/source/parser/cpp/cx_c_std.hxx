/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cx_c_std.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef ADC_CPP_CX_C_STD_HXX
#define ADC_CPP_CX_C_STD_HXX

// USED SERVICES
    // BASE CLASSES
#include <tokens/tkpcontx.hxx>
#include "cx_base.hxx"
    // COMPONENTS
#include <tokens/tkpstama.hxx>
    // PARAMETERS



namespace cpp {

class Context_Comment;

/**
*/
class Context_CppStd : public Cx_Base,
                       private StateMachineContext
{
  public:
    // 	LIFECYCLE
                        Context_CppStd(
                            DYN autodoc::TkpDocuContext &
                                                let_drContext_Docu	);
                        ~Context_CppStd();
    //	OPERATIONS
    virtual void		ReadCharChain(
                            CharacterSource &	io_rText );
    virtual void        AssignDealer(
                            Distributor &       o_rDealer );
  private:
    //	SERVICE FUNCTIONS
    void				PerformStatusFunction(
                            uintt				i_nStatusSignal,
                            StmArrayStatus::F_CRTOK
                                                i_fTokenCreateFunction,
                            CharacterSource &	io_rText );
    void				SetupStateMachine();

    //	DATA
    StateMachine		aStateMachine;

        // Contexts
    Dyn<autodoc::TkpDocuContext>
                        pDocuContext;

    Dyn<Context_Comment>
                        pContext_Comment;
    Dyn<Cx_Base>    	pContext_Preprocessor;
    Dyn<Cx_Base>        pContext_ConstString;
    Dyn<Cx_Base>        pContext_ConstChar;
    Dyn<Cx_Base>        pContext_ConstNumeric;
    Dyn<Cx_Base>        pContext_UnblockMacro;
};



}   // namespace cpp


#endif

