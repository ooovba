/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: cx_base.cxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <precomp.h>
#include "cx_base.hxx"

// NOT FULLY DECLARED SERVICES
#include <tokens/token.hxx>
#include "c_dealer.hxx"



namespace cpp {



Cx_Base::Cx_Base( TkpContext * io_pFollowUpContext )
    : 	pDealer(0),
        pFollowUpContext(io_pFollowUpContext)
        // pNewToken
{
}

bool
Cx_Base::PassNewToken()
{
    if (pNewToken)
    {
        pNewToken.Release()->DealOut(Dealer());
        return true;
    }
    return false;
}

TkpContext &                      
Cx_Base::FollowUpContext()
{
    csv_assert(pFollowUpContext != 0);
    return *pFollowUpContext;
}

void
Cx_Base::AssignDealer( Distributor &  o_rDealer )
{
    pDealer = &o_rDealer;
}


}   // namespace cpp




