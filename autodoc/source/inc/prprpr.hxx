/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: prprpr.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#ifndef ARY_CPP_PRPRPR_HXX  // PRePRocessorPRocessing
#define ARY_CPP_PRPRPR_HXX



// Implemented in autodoc/source/parser/cpp/defdescr.cxx .

bool                CheckForOperator(
                        bool &              o_bStringify,
                        bool &              o_bConcatenate,
                        const String &     i_sTextItem );
void                Do_bConcatenate(
                        csv::StreamStr &    o_rText,
                        bool &              io_bConcatenate );
void                Do_bStringify_begin(
                        csv::StreamStr &    o_rText,
                        bool                i_bStringify );
void                Do_bStringify_end(
                        csv::StreamStr &    o_rText,
                        bool &              io_bStringify );
bool                HandleOperatorsBeforeTextItem(  /// @return true, if text item is done here
                        csv::StreamStr &    o_rText,
                        bool &              io_bStringify,
                        bool &              io_bConcatenate,
                        const String &     i_sTextItem );




#endif
