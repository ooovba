/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: c_enuval.cxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <precomp.h>
#include <ary/cpp/c_enuval.hxx>


// NOT FULLY DECLARED SERVICES


namespace ary
{
namespace cpp
{


EnumValue::EnumValue( const String  &     i_sLocalName,
                      Ce_id               i_nOwner,
                      String              i_sInitialisation )
    :   aEssentials( i_sLocalName,
                     i_nOwner,
                     Lid(0) ),
           sInitialisation(i_sInitialisation)
{
}

EnumValue::~EnumValue()
{
}

const String  &
EnumValue::inq_LocalName() const
{
    return aEssentials.LocalName();
}

Cid
EnumValue::inq_Owner() const
{
    return aEssentials.Owner();
}

Lid
EnumValue::inq_Location() const
{
    return aEssentials.Location();
}

void
EnumValue::do_Accept(csv::ProcessorIfc & io_processor) const
{
    csv::CheckedCall(io_processor,*this);
}

ClassId
EnumValue::get_AryClass() const
{
    return class_id;
}


}   // namespace cpp
}   // namespace ary
