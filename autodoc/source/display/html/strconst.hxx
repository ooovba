/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: strconst.hxx,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef ADC_DISPLAY_HTML_STRCONST_HXX
#define ADC_DISPLAY_HTML_STRCONST_HXX


const char * const C_sDIR_NamespacesCpp = "names";
const char * const C_sDIR_Index = "index-files";   // Convention recognised by Javadoc

const char * const C_sPath_Index = "index-files/index-1.html";   // Convention recognised by Javadoc

const char * const C_sHFN_Css           = "cpp.css";
const char * const C_sHFN_Overview      = "index.html";
const char * const C_sHFN_Help          = "help.html";

const char * const C_sHFN_Namespace     = "index.html";

const char * const C_sTitle_SubNamespaces   = "Nested Namespaces";
const char * const C_sTitle_Classes         = "Classes";
const char * const C_sTitle_Structs         = "Structs";
const char * const C_sTitle_Unions          = "Unions";
const char * const C_sTitle_Enums           = "Enums";
const char * const C_sTitle_Typedefs        = "Typedefs";
const char * const C_sTitle_Operations      = "Functions";
const char * const C_sTitle_Constants       = "Constants";
const char * const C_sTitle_Variables       = "Variables";
const char * const C_sTitle_EnumValues      = "Values";

const char * const C_sLabel_SubNamespaces   = "subnsps";
const char * const C_sLabel_Classes         = "classes";
const char * const C_sLabel_Structs         = "structs";
const char * const C_sLabel_Unions          = "unions";
const char * const C_sLabel_Enums           = "enums";
const char * const C_sLabel_Typedefs        = "tydefs";
const char * const C_sLabel_Operations      = "ops";
const char * const C_sLabel_Constants       = "consts";
const char * const C_sLabel_Variables       = "vars";
const char * const C_sLabel_EnumValues      = "envals";

const char * const C_sHFTitle_Overview              = "C++ Reference Documentation Overview";
const char * const C_sHFTitle_Help                  = "How This Reference Document Is Organized";

const char * const C_sHFTitle_GlobalNamespaceCpp    = "Global Namespace in C++";
const char * const C_sHFTypeTitle_Namespace         = "namespace";
const char * const C_sHFTypeTitle_Class             = "class";
const char * const C_sHFTypeTitle_Struct            = "struct";
const char * const C_sHFTypeTitle_Union             = "union";
const char * const C_sHFTypeTitle_Enum              = "enum";
const char * const C_sHFTypeTitle_Typedef           = "typedef";


#endif

