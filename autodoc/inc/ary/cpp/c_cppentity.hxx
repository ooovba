/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: c_cppentity.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef ARY_CPP_C_CPPENTITY_HXX
#define ARY_CPP_C_CPPENTITY_HXX



// USED SERVICES
    // BASE CLASSES
#include <ary/entity.hxx>
    // OTHER
#include <ary/doc/d_docu.hxx>



namespace ary
{
namespace cpp
{


/** A C++ code entity as parsed by Autodoc.
*/
class CppEntity : public Entity
{
  public:
    // LIFECYCLE
    virtual             ~CppEntity() {}

    // OPERATIONS

    // INQUIRY
    const ary::doc::Documentation &
                        Docu() const;
    // ACCESS
    void                Set_Docu(
                            DYN ary::doc::Node &
                                                pass_docudata );
  private:
    // DATA
    ary::doc::Documentation
                        aDocu;
};




// IMPLEMENTATION
inline const doc::Documentation &
CppEntity::Docu() const
{
    return aDocu;
}

inline void
CppEntity::Set_Docu(ary::doc::Node & pass_docudata)
{
    aDocu.Set_Data(pass_docudata);
}




}   // namespace cpp
}   // namespace ary
#endif
