/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: TabPages.hrc,v $
 * $Revision: 1.7.20.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef CHART_TABPAGES_HRC
#define CHART_TABPAGES_HRC

#include "ResourceIds.hrc"

//see attrib.hrc in old chart

//#define TP_LEGEND_POS 900
#define GRP_LEGEND                  1
#define FL_LEGEND_TEXTORIENT        2
#define FT_LEGEND_TEXTDIR           3
#define LB_LEGEND_TEXTDIR           4

//#define TP_DATA_DESCR 901

//#define TP_ALIGNMENT 902
////#define FL_ORDER 2
////#define RBT_SIDEBYSIDE 1
////#define RBT_UPDOWN 2
////#define RBT_DOWNUP 3
////#define RBT_AUTOORDER 4

//#define TP_STAT                   905
#define FL_TEXTBREAK                3
#define CBX_TEXTBREAK               2
#define CBX_TEXTOVERLAP             4

//Seit 4/1998 koennen Texte frei gedreht werden: SCHATTR_TEXT_DEGREES
// ID's for title rotation tabpage
#define CTR_DIAL				6030
#define BTN_TXTSTACKED			6031
#define FT_DEGREES				6032
#define NF_ORIENT				6033
#define CTR_DUMMY				6034
#define FT_DUMMY				6035
#define FL_ALIGN				6037
#define FT_TEXTDIR              6038
#define LB_TEXTDIR              6039

/*
//Symbol-Tabpage (zum Teil sehr Aehnlich der SVX_AREA_TABPAGE) (obsolete)
//#define TP_SYMBOL                 906
#define RBT_COLOR                   1
#define RBT_GRADIENT                2
#define RBT_HATCHING                3
#define RBT_BITMAP                  4
#define RBT_INVISIBLE               5
#define LB_COLOR                    1
#define LB_GRADIENT                 2
#define LB_HATCHING                 3
#define LB_BITMAP                   4

#define GRP_FILL                    1
#define CTL_BITMAP_PREVIEW          6
*/

//------------
//from old chart tplabel.hrc

//#define TP_AXIS_LABEL					920

#define CB_AXIS_LABEL_SCHOW_DESCR		1

//#define FL_AXIS_LABEL_FORMATTING		2

#define FL_AXIS_LABEL_ORIENTATION		3
#define CT_AXIS_LABEL_DIAL				4
#define PB_AXIS_LABEL_TEXTSTACKED		5
#define FT_AXIS_LABEL_DEGREES			6
#define NF_AXIS_LABEL_ORIENT			7
#define FT_UNUSED						8
#define CT_UNUSED						9

#define FL_AXIS_LABEL_TEXTFLOW			10
#define CB_AXIS_LABEL_TEXTOVERLAP		11
#define CB_AXIS_LABEL_TEXTBREAK			12

#define FL_AXIS_LABEL_ORDER				13
#define RB_AXIS_LABEL_SIDEBYSIDE		14
#define RB_AXIS_LABEL_UPDOWN			15
#define RB_AXIS_LABEL_DOWNUP			16
#define RB_AXIS_LABEL_AUTOORDER			17

#define FL_SEPARATOR					18

#define FT_AXIS_TEXTDIR                 19
#define LB_AXIS_TEXTDIR                 20

//#define TP_SCALE		  903

#define FL_SCALE			1

#define TXT_MIN				1
#define TXT_MAX				2
#define TXT_STEP_MAIN		3
#define TXT_STEP_HELP		4
#define TXT_ORIGIN			5

#define CBX_AUTO_MIN		1
#define CBX_AUTO_MAX		2
#define CBX_AUTO_STEP_MAIN	3
#define CBX_AUTO_STEP_HELP	4
#define CBX_AUTO_ORIGIN		5
#define CBX_LOGARITHM		6
#define CBX_REVERSE         7

#define EDT_STEP_MAIN		1
#define EDT_MAX				2
#define EDT_MIN				3
#define EDT_ORIGIN          4

#define MT_STEPHELP		    10

//#define TP_AXIS_POSITIONS   904

#define FL_AXIS_LINE        1
#define FL_LABELS           2
#define FL_TICKS            3
#define FL_VERTICAL         4
#define FL_GRIDS            5

#define FT_CROSSES_OTHER_AXIS_AT        1
#define FT_AXIS_LABEL_DISTANCE			2
#define FT_PLACE_LABELS      			3
#define FT_MAJOR            			4
#define FT_MINOR            			5
#define FT_PLACE_TICKS                  6

#define LB_CROSSES_OTHER_AXIS_AT    1
#define LB_PLACE_LABELS             2
#define EDT_CROSSES_OTHER_AXIS_AT           3
#define EDT_CROSSES_OTHER_AXIS_AT_CATEGORY  4
#define EDT_AXIS_LABEL_DISTANCE			    5
#define LB_PLACE_TICKS              6

#define CB_AXIS_BETWEEN_CATEGORIES 1
#define CB_TICKS_INNER	2
#define CB_TICKS_OUTER	3
#define CB_MINOR_INNER	4
#define CB_MINOR_OUTER	5
#define CB_MAJOR_GRID   6
#define CB_MINOR_GRID   7

#define PB_MAJOR_GRID   1
#define PB_MINOR_GRID   2

#endif
