/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: tp_PolarOptions.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CHART2_TP_POLAROPTIONS_HXX
#define _CHART2_TP_POLAROPTIONS_HXX

#include <sfx2/tabdlg.hxx>
#include <vcl/fixed.hxx>
#include <vcl/button.hxx>
#include <vcl/field.hxx>
#include <svx/dialcontrol.hxx>

//.............................................................................
namespace chart
{
//.............................................................................

class PolarOptionsTabPage : public SfxTabPage
{

public:
    PolarOptionsTabPage(Window* pParent, const SfxItemSet& rInAttrs );
    virtual ~PolarOptionsTabPage();

    static SfxTabPage* Create(Window* pParent, const SfxItemSet& rInAttrs);
    virtual BOOL FillItemSet(SfxItemSet& rOutAttrs);
    virtual void Reset(const SfxItemSet& rInAttrs);
    
private:
    CheckBox         m_aCB_Clockwise;
    FixedLine        m_aFL_StartingAngle;
    svx::DialControl m_aAngleDial;
    FixedText        m_aFT_Degrees;
    NumericField     m_aNF_StartingAngle;

    FixedLine        m_aFL_PlotOptions;
    CheckBox         m_aCB_IncludeHiddenCells;
};

//.............................................................................
} //namespace chart
//.............................................................................

#endif
