/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: NoWarningThisInCTOR.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef CHART2_NOWARNINGTHISINCTOR_HXX
#define CHART2_NOWARNINGTHISINCTOR_HXX

/** Include this file, if you have to use "this" in the base initializer list of
    a constructor.

    Include it only, if the usage of this is unavoidable, like in the
    initialization of controls in a dialog.

    Do not include this header in other header files, because this might result
    in unintended suppression of the warning via indirect inclusion.
 */

#ifdef _MSC_VER
// warning C4355: 'this' : used in base member initializer list
#  pragma warning (disable : 4355)
#endif

// CHART2_NOWARNINGTHISINCTOR_HXX
#endif
