/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: CategoryPositionHelper.hxx,v $
 * $Revision: 1.3.44.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _CHART2_CATEGORYPOSITIONHELPER_HXX
#define _CHART2_CATEGORYPOSITIONHELPER_HXX

//.............................................................................
namespace chart
{
//.............................................................................

//-----------------------------------------------------------------------------
/**
*/

class CategoryPositionHelper
{
public:
    CategoryPositionHelper( double fSeriesCount, double CategoryWidth = 1.0);
    CategoryPositionHelper( const CategoryPositionHelper& rSource );
    virtual ~CategoryPositionHelper();

    double getSlotWidth() const;
    double getSlotPos( double fCategoryX, double fSeriesNumber ) const;

    //Distance between two neighboring bars in same category, seen relative to width of the bar
    void setInnerDistance( double fInnerDistance );

    //Distance between two neighboring bars in different category, seen relative to width of the bar:
    void setOuterDistance( double fOuterDistance );

protected:
    double m_fSeriesCount;
    double m_fCategoryWidth;
    //Distance between two neighboring bars in same category, seen relative to width of the bar:
    double m_fInnerDistance; //[-1,1] m_fInnerDistance=1 --> distance == width; m_fInnerDistance=-1-->all rects are painted on the same position
    //Distance between two neighboring bars in different category, seen relative to width of the bar:
    double m_fOuterDistance; //>=0 m_fOuterDistance=1 --> distance == width
};

//.............................................................................
} //namespace chart
//.............................................................................
#endif
