/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: VAxisProperties.hxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CHART2_VAXIS_PROPERTIES_HXX
#define _CHART2_VAXIS_PROPERTIES_HXX

#include "TickmarkProperties.hxx"
#include "PlottingPositionHelper.hxx"
#include "LabelAlignment.hxx"

#include <com/sun/star/chart/ChartAxisLabelPosition.hpp>
#include <com/sun/star/chart/ChartAxisMarkPosition.hpp>
#include <com/sun/star/chart/ChartAxisPosition.hpp>
#include <com/sun/star/chart2/XAxis.hpp>
#include <com/sun/star/chart2/AxisType.hpp>
#include <com/sun/star/chart2/data/XTextualDataSequence.hpp>
#include <com/sun/star/awt/Rectangle.hpp>
#include <com/sun/star/awt/Size.hpp>
#include <com/sun/star/drawing/TextVerticalAdjust.hpp>
#include <com/sun/star/drawing/TextHorizontalAdjust.hpp>
#include <com/sun/star/lang/Locale.hpp>

#include <vector>

//.............................................................................
namespace chart
{
//.............................................................................

//-----------------------------------------------------------------------------
/**
*/

//These properties describe how a couple of labels are arranged one to another.
//The couple can contain all labels for all tickmark depth or just the labels for one single depth or
//the labels from an coherent range of tick depths (e.g. the major and first minor tickmarks should be handled together).
//... only allow side by side for different tick depth
enum AxisLabelStaggering
{
      SIDE_BY_SIDE
    , STAGGER_EVEN
    , STAGGER_ODD
    , STAGGER_AUTO
};

struct AxisLabelProperties
{
    AxisLabelProperties();

    ::com::sun::star::awt::Size         m_aFontReferenceSize;//reference size to calculate the font height
    ::com::sun::star::awt::Rectangle    m_aMaximumSpaceForLabels;//Labels need to be clipped in order to fit into this rectangle

    sal_Int32            nNumberFormatKey;

    AxisLabelStaggering  eStaggering;

    sal_Bool             bLineBreakAllowed;
    sal_Bool             bOverlapAllowed;

    sal_Bool             bStackCharacters;
    double               fRotationAngleDegree;

    sal_Int32   nRhythm; //show only each nth label with n==nRhythm
    bool        bRhythmIsFix; //states wether the given rythm is fix or may be changed

    //methods:
    void init( const ::com::sun::star::uno::Reference<
                ::com::sun::star::chart2::XAxis >&  xAxisModel );

    sal_Bool            getIsStaggered() const;
};

struct AxisProperties
{
    ::com::sun::star::uno::Reference< ::com::sun::star::chart2::XAxis > m_xAxisModel;

    sal_Int32   m_nDimensionIndex;
    bool        m_bIsMainAxis;//not secondary axis
    bool        m_bSwapXAndY;

    ::com::sun::star::chart::ChartAxisPosition      m_eCrossoverType;
    ::com::sun::star::chart::ChartAxisLabelPosition m_eLabelPos;
    ::com::sun::star::chart::ChartAxisMarkPosition  m_eTickmarkPos;

    double*     m_pfMainLinePositionAtOtherAxis;
    double*     m_pfExrtaLinePositionAtOtherAxis;

    bool        m_bCrossingAxisHasReverseDirection;
    bool        m_bCrossingAxisIsCategoryAxes;
    bool        m_bAxisBetweenCategories;

    //this direction is used to indicate in which direction the labels are to be drawn
    double          m_fLabelDirectionSign;
    //this direction is used to indicate in which direction inner tickmarks are to be drawn
    double          m_fInnerDirectionSign;
    bool            m_bLabelsOutside;
    LabelAlignment  m_aLabelAlignment;
    sal_Bool        m_bDisplayLabels;

    sal_Int32       m_nNumberFormatKey;

    /*
    0: no tickmarks         1: inner tickmarks
    2: outer tickmarks      3: inner and outer tickmarks
    */
    sal_Int32                           m_nMajorTickmarks;
    sal_Int32                           m_nMinorTickmarks;
    ::std::vector<TickmarkProperties>   m_aTickmarkPropertiesList;

    VLineProperties                     m_aLineProperties;

    //for category axes ->
    sal_Int32                                        m_nAxisType;//REALNUMBER, CATEGORY etc. type ::com::sun::star::chart2::AxisType
    ::com::sun::star::uno::Reference<
        ::com::sun::star::chart2::data::XTextualDataSequence >
                                                    m_xAxisTextProvider; //for categries or series names
    //position of main tickmarks in respect to the indicated value: at value or between neighboured indicated values
    bool                                             m_bTickmarksAtIndicatedValue;
    //<- category axes

    //methods:

    AxisProperties( const ::com::sun::star::uno::Reference< ::com::sun::star::chart2::XAxis >& xAxisModel
                  , const ::com::sun::star::uno::Reference<
                        ::com::sun::star::chart2::data::XTextualDataSequence >& xAxisTextProvider );
    AxisProperties( const AxisProperties& rAxisProperties );
    virtual ~AxisProperties();
    virtual void init(bool bCartesian=false);//init from model data (m_xAxisModel)

    void initAxisPositioning( const ::com::sun::star::uno::Reference< ::com::sun::star::beans::XPropertySet >& xAxisProp );

    static TickmarkProperties getBiggestTickmarkProperties();

private:
    AxisProperties();

protected:
    virtual TickmarkProperties  makeTickmarkProperties( sal_Int32 nDepth ) const;
    VLineProperties      makeLinePropertiesForDepth( sal_Int32 nDepth ) const;
};

//.............................................................................
} //namespace chart
//.............................................................................
#endif
