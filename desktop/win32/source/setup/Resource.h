/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _RESOURCE_H
#define _RESOURCE_H

#define IDS_APP_TITLE	10
#define IDS_APP_PROD_TITLE	11
#define IDS_OUTOFMEM	12
#define IDS_NOMSI	13
#define IDS_USER_CANCELLED	14
#define IDS_REQUIRES_ADMIN_PRIV	15
#define IDS_FILE_NOT_FOUND	16
#define IDS_INVALID_PARAM	17
#define IDS_ALLOW_MSI_UPDATE	18
#define IDS_USAGE	19
#define IDS_ALREADY_RUNNING	20
#define IDS_UNKNOWN_ERROR	21
#define IDS_INVALID_PROFILE	22
#define IDS_UNKNOWN_LANG	23
#define IDS_SETUP_TO_OLD    24
#define IDS_SETUP_NOT_FOUND 25
#define IDS_LANGUAGE_ENGLISH	50
#define IDS_LANGUAGE_SPAIN	51
#define IDS_LANGUAGE_GERMAN	52
#define IDS_LANGUAGE_ZH_TW	53
#define IDS_LANGUAGE_CS	54
#define IDS_LANGUAGE_DA	55
#define IDS_LANGUAGE_DE_DE	56
#define IDS_LANGUAGE_EL	57
#define IDS_LANGUAGE_EN_US	58
#define IDS_LANGUAGE_ES	59
#define IDS_LANGUAGE_FI	60
#define IDS_LANGUAGE_FR_FR	61
#define IDS_LANGUAGE_HE	62
#define IDS_LANGUAGE_HU	63
#define IDS_LANGUAGE_IT_IT	64
#define IDS_LANGUAGE_JA	65
#define IDS_LANGUAGE_KO	66
#define IDS_LANGUAGE_NL_NL	67
#define IDS_LANGUAGE_NO_NO	68
#define IDS_LANGUAGE_PL	69
#define IDS_LANGUAGE_PT_BR	70
#define IDS_LANGUAGE_RU	71
#define IDS_LANGUAGE_SK	72
#define IDS_LANGUAGE_SV_SE	73
#define IDS_LANGUAGE_TH	74
#define IDS_LANGUAGE_TR	75
#define IDS_LANGUAGE_ET	76
#define IDS_LANGUAGE_ZH_CN	77
#define IDS_LANGUAGE_PT_PT	78

#define IDI_INSTALLER                  99

#endif
