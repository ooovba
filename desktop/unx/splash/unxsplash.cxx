/*************************************************************************
 *
 *  OpenOffice.org - a multi-platform office productivity suite
 *
 *  $RCSfile$
 *
 *  $Revision: 7305 $
 *
 *  last change: $Author: jholesovsky $ $Date: 2006-08-04 16:35:41 +0200 (Pá, 04 srp 2006) $
 *
 *  The Contents of this file are made available subject to
 *  the terms of GNU Lesser General Public License Version 2.1.
 *
 *
 *    GNU Lesser General Public License Version 2.1
 *    =============================================
 *    Copyright 2005 by Sun Microsystems, Inc.
 *    901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License version 2.1, as published by the Free Software Foundation.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *    MA  02111-1307  USA
 *
 ************************************************************************/

#include "unxsplash.hxx"
#include <stdio.h>
#ifndef _UTL_BOOTSTRAP_HXX
#include <unotools/bootstrap.hxx>
#endif
#ifndef _VOS_PROCESS_HXX_
#include <vos/process.hxx>
#endif
#ifndef _URLOBJ_HXX
#include <tools/urlobj.hxx>
#endif
#ifndef _STREAM_HXX
#include <tools/stream.hxx>
#endif
#ifndef _SFX_HRC
#include <sfx2/sfx.hrc>
#endif

#include <com/sun/star/registry/XRegistryKey.hpp>
#include <rtl/logfile.hxx>
#include <rtl/ustrbuf.hxx>
#include <rtl/math.hxx>

#define PIPE_ARG "-splash-pipe="

using namespace ::rtl;
using namespace ::com::sun::star::registry;

namespace desktop
{

UnxSplashScreen::UnxSplashScreen( const Reference< XMultiServiceFactory >& rSMgr )
    : m_rFactory( rSMgr ),
      m_pOutFd( NULL )
{
}

UnxSplashScreen::~UnxSplashScreen()
{
#if OSL_DEBUG_LEVEL > 1
    fprintf( stderr, "UnxSplashScreen::~UnxSplashScreen()\n" );
#endif

    if ( m_pOutFd )
    {
        fclose( m_pOutFd );
        m_pOutFd = NULL;
    }
}

void SAL_CALL UnxSplashScreen::start( const OUString& /*aText*/, sal_Int32 /*nRange*/ )
    throw ( RuntimeException )
{
}

void SAL_CALL UnxSplashScreen::end()
    throw ( RuntimeException )
{
#if OSL_DEBUG_LEVEL > 1
    fprintf( stderr, "UnxSplashScreen::end()\n" );
#endif

    fprintf( m_pOutFd, "end\n" );
    fflush( m_pOutFd );
}

void SAL_CALL UnxSplashScreen::reset()
    throw ( RuntimeException )
{
    // TODO?
}

void SAL_CALL UnxSplashScreen::setText( const OUString& /*aText*/ )
    throw ( RuntimeException )
{
    // TODO?
}

void SAL_CALL UnxSplashScreen::setValue( sal_Int32 nValue )
    throw ( RuntimeException )
{
    if ( m_pOutFd )
    {
        fprintf( m_pOutFd, "%d%%\n", nValue );
        fflush( m_pOutFd );
    }
}

// XInitialize
void SAL_CALL
UnxSplashScreen::initialize( const ::com::sun::star::uno::Sequence< ::com::sun::star::uno::Any>& aArguments )
    throw ( RuntimeException )
{
    ::vos::OStartupInfo aInfo;
    for ( sal_uInt32 i = 0; i < aInfo.getCommandArgCount(); i++ )
    {
        rtl::OUString aArg;
        if ( aInfo.getCommandArg( i, aArg ) )
            break;
        if ( aArg.matchIgnoreAsciiCaseAsciiL( PIPE_ARG, sizeof( PIPE_ARG ) - 1, 0 ) )
        {
            OUString aNum = aArg.copy( sizeof( PIPE_ARG ) - 1 );
            int fd = aNum.toInt32();
            m_pOutFd = fdopen( fd, "w" );
#if OSL_DEBUG_LEVEL > 1
            fprintf( stderr, "Got argument '-splash-pipe=%d ('%s') (%p)\n",
                     fd, (const sal_Char *)rtl::OUStringToOString( aNum, RTL_TEXTENCODING_UTF8 ),
                     m_pOutFd );
#endif
        }
    }
}

// get service instance...
UnxSplashScreen *UnxSplashScreen::m_pINSTANCE = NULL;
osl::Mutex UnxSplashScreen::m_aMutex;

Reference< XInterface > UnxSplashScreen::getInstance( const Reference< XMultiServiceFactory >& rSMgr )
{
    if ( m_pINSTANCE == NULL )
    {
        osl::MutexGuard guard( m_aMutex );
        if ( m_pINSTANCE == NULL )
            return (XComponent*) new UnxSplashScreen( rSMgr );
    }

    return (XComponent*)NULL;
}

// static service info...
const char* UnxSplashScreen::interfaces[] =
{
    "com.sun.star.task.XStartusIndicator",
    "com.sun.star.lang.XInitialization",
    NULL,
};
const sal_Char *UnxSplashScreen::serviceName = "com.sun.star.office.PipeSplashScreen";
const sal_Char *UnxSplashScreen::implementationName = "com.sun.star.office.comp.PipeSplashScreen";
const sal_Char *UnxSplashScreen::supportedServiceNames[] = { "com.sun.star.office.PipeSplashScreen", NULL };

OUString UnxSplashScreen::impl_getImplementationName()
{
    return OUString::createFromAscii( implementationName );
}

Sequence<OUString> UnxSplashScreen::impl_getSupportedServiceNames()
{
    Sequence<OUString> aSequence;
    for ( int i = 0; supportedServiceNames[i] != NULL; i++ )
    {
        aSequence.realloc( i+1 );
        aSequence[i] = OUString::createFromAscii( supportedServiceNames[i] );
    }
    return aSequence;
}

}
