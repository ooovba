PRJ=..$/..
PRJNAME=desktop
TARGET=oosplash

NO_DEFAULT_STL=TRUE

.INCLUDE :  settings.mk

STDLIB=

OBJFILES= \
	$(OBJ)$/splashx.obj \
	$(OBJ)$/start.obj

APP1TARGET = $(TARGET)
APP1RPATH  = BRAND
APP1OBJS   = $(OBJFILES)
APP1LIBSALCPPRT=
APP1CODETYPE = C
APP1STDLIBS = $(SALLIB) -lX11
.IF "$(OS)"=="SOLARIS"
APP1STDLIBS+= -lsocket
.ENDIF

# --- Targets ------------------------------------------------------

.INCLUDE :	target.mk

$(OBJ)$/start.obj : $(INCCOM)$/introbmpnames.hxx

.INCLUDE .IGNORE : $(MISC)$/intro_bmp_names.mk

.IF "$(INTRO_BITMAPS:f)"!="$(LASTTIME_INTRO_BITMAPS)"
DO_PHONY=.PHONY
.ENDIF			# "$(INTRO_BITMAPS:f)"!="$(LASTTIME_INTRO_BITMAPS)"

$(INCCOM)$/introbmpnames.hxx $(DO_PHONY):
	echo const char INTRO_BITMAP_STRINGLIST[]=$(EMQ)"$(INTRO_BITMAPS:f:t",")$(EMQ)"$(EMQ); > $@
	echo LASTTIME_INTRO_BITMAPS=$(INTRO_BITMAPS:f) > $(MISC)$/intro_bmp_names.mk
