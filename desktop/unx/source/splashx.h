/*************************************************************************
 *
 *  OpenOffice.org - a multi-platform office productivity suite
 *
 *  $RCSfile$
 *
 *  $Revision: 9695 $
 *
 *  last change: $Author: jholesovsky $ $Date: 2007-07-03 16:36:09 +0200 (Út, 03 čec 2007) $
 *
 *  The Contents of this file are made available subject to
 *  the terms of GNU Lesser General Public License Version 2.1.
 *
 *
 *    GNU Lesser General Public License Version 2.1
 *    =============================================
 *    Copyright 2005 by Sun Microsystems, Inc.
 *    901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License version 2.1, as published by the Free Software Foundation.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with this library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *    MA  02111-1307  USA
 *
 ************************************************************************/

#ifndef _SPLASHX_H
#define _SPLASHX_H

#ifdef __cplusplus
extern "C" {
#endif

// Load the specified Windows 24bit BMP we can have as a background of the
// splash.
//
// Note: Must be called before the create_window(), otherwise there will be no
// image in the splash, just black rectangle.
//
// Return: 1 - success, 0 - failure (non-existing, etc.)
int splash_load_bmp( char *filename );

// Init some of the values
// If not called, the defaults are used
// barc, framec - colors, posx, posy - position, w, h - size
void splash_setup( int barc[3], int framec[3], int posx, int posy, int w, int h );

// Create the splash window
// Return: 1 - success, 0 - failure
int splash_create_window( int argc, char** argv );

// Destroy the splash window
void splash_close_window();

// Update the progress bar
void splash_draw_progress( int progress );

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _SPLASHX_H
