 
 /* 
  * copyright text to see as text in the soffice binary
  *
  */
 
const char copyright_text_1[] = "Copyright � 2008 Sun Microsystems, Inc., All rights reserved.";
const char copyright_text_2[] = "Sun Microsystems, Inc. has intellectual property rights relating to technology embodied in the product that is described in this document. In particular, and without limitation, these intellectual property rights may include one or more of the U.S. patents listed at http://www.sun.com/patents and one or more additional patents or pending patent applications in the U.S. and in other countries.";
const char copyright_text_3[] = "U.S. Government Rights - Commercial software. Government users are subject to the Sun Microsystems, Inc. standard license agreement and applicable provisions of the FAR and its supplements.  Use is subject to license terms.";
const char copyright_text_4[] = "This distribution may include materials developed by third parties.Sun, Sun Microsystems, the Sun logo, Java, Solaris and StarOffice are trademarks or registered trademarks of Sun Microsystems, Inc. in the U.S. and other countries. All SPARC trademarks are used under license and are trademarks or registered trademarks of SPARC International, Inc. in the U.S. and other countries.";
const char copyright_text_5[] = "UNIX is a registered trademark in the U.S. and other countries, exclusively licensed through X/Open Company, Ltd.";
const char copyright_text_21[] = "Copyright � 2008 Sun Microsystems, Tous droits r�serv�s.";
const char copyright_text_22[] = "Sun Microsystems, Inc. a les droits de propri�t� intellectuels relatants � la technologie incorpor�e dans ce produit. En particulier, et sans la limitation, ces droits de propri�t� intellectuels peuvent inclure un ou plus des brevets am�ricains �num�r�s � http://www.sun.com/patents et un ou les brevets plus suppl�mentaires ou les applications de brevet en attente dans les �tats - Unis et les autres pays.";
const char copyright_text_23[] = "Ce produit ou document est prot�g� par un copyright et distribu� avec des licences qui en restreignent l'utilisation, la copie, la distribution, et la d�compilation. Aucune partie de ce produit ou document ne peut �tre reproduite sous aucune forme, par quelque moyen que ce soit, sans l'autorisation pr�alable et �crite de Sun et de ses bailleurs de licence, s'il y ena.";
const char copyright_text_24[] = "L'utilisation est soumise aux termes du contrat de licence.";
const char copyright_text_25[] = "Cette distribution peut comprendre des composants développés par des tierces parties.";
const char copyright_text_26[] = "Sun, Sun Microsystems, le logo Sun, Java, Solaris et StarOffice sont des marques de fabrique ou des marques déposées de Sun Microsystems, Inc. aux Etats-Unis et dans d'autres pay";
const char copyright_text_27[] = "Toutes les marques SPARC sont utilisées sous licence et sont des marques de fabrique ou des marques déposées de SPARC International, Inc. aux Etats-Unis et dans d'autres pay";
const char copyright_text_28[] = "UNIX est une marque déposée aux Etats-Unis et dans d'autres pays et licenciée exlusivement par X/Open Company, Ltd.";

