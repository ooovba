 
 /* 
  * copyright text to see as text in the soffice binary
  *
  */
 
extern const char copyright_text_1[];
extern const char copyright_text_2[];
extern const char copyright_text_21[];
extern const char copyright_text_22[];

const char copyright_text_1[] = "Copyright � 2008 Sun Microsystems, Inc., All rights reserved.";
const char copyright_text_2[] = "Sun Microsystems, Inc. has intellectual property rights relating to technology embodied in the product that is described in this document. In particular, and without limitation, these intellectual property rights may include one or more of the U.S. patents listed at http://www.sun.com/patents and one or more additional patents or pending patent applications in the U.S. and in other countries.";
const char copyright_text_21[] = "Copyright � 2008 Sun Microsystems, Tous droits r�serv�s.";
const char copyright_text_22[] = "Sun Microsystems, Inc. a les droits de propri�t� intellectuels relatants � la technologie incorpor�e dans ce produit. En particulier, et sans la limitation, ces droits de propri�t� intellectuels peuvent inclure un ou plus des brevets am�ricains �num�r�s � http://www.sun.com/patents et un ou les brevets plus suppl�mentaires ou les applications de brevet en attente dans les �tats - Unis et les autres pays.";

