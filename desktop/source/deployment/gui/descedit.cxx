/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: descedit.cxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_desktop.hxx"

#include <vcl/scrbar.hxx>
#include <svtools/txtattr.hxx>
#include <svtools/xtextedt.hxx>

#include "descedit.hxx"

#include "dp_gui.hrc"

using dp_gui::DescriptionEdit;

// DescriptionEdit -------------------------------------------------------

DescriptionEdit::DescriptionEdit( Window* pParent, const ResId& rResId ) :

    ExtMultiLineEdit( pParent, rResId ),

    m_bIsVerticalScrollBarHidden( true )

{
    Init();
}

// -----------------------------------------------------------------------

void DescriptionEdit::Init()
{
    Clear();
    // no tabstop
    SetStyle( ( GetStyle() & ~WB_TABSTOP ) | WB_NOTABSTOP );
    // read-only
    SetReadOnly();
    // no cursor
    EnableCursor( FALSE );
}

// -----------------------------------------------------------------------

void DescriptionEdit::UpdateScrollBar()
{
    if ( m_bIsVerticalScrollBarHidden )
    {
        ScrollBar*  pVScrBar = GetVScrollBar();
        if ( pVScrBar && pVScrBar->GetVisibleSize() < pVScrBar->GetRangeMax() )
        {
            pVScrBar->Show();
            m_bIsVerticalScrollBarHidden = false;
        }
    }
}

// -----------------------------------------------------------------------

void DescriptionEdit::Clear()
{
    SetText( String() );

    m_bIsVerticalScrollBarHidden = true;
    ScrollBar*  pVScrBar = GetVScrollBar();
    if ( pVScrBar )
        pVScrBar->Hide();
}

// -----------------------------------------------------------------------

void DescriptionEdit::SetDescription( const String& rDescription )
{
    SetText( rDescription );
    UpdateScrollBar();
}

