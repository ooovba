#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: makefile.pmk,v $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

# --- Targets ------------------------------------------------------

.IF "$(GUI)"=="UNX"
# uppercase and no filename extension for txt
SYSTEXTDOCS=$(foreach,i,$(alllangiso) $(MISC)$/$(GUI)$/README_$i)
SYSHTMLDOCS=$(SYSTEXTDOCS:+".html")
.ELSE		# "$(GUI)"=="UNX"
SYSTEXTDOCS=$(foreach,i,$(alllangiso) $(MISC)$/$(GUI)$/readme_$i.txt)
SYSHTMLDOCS=$(SYSTEXTDOCS:s/.txt/.html/)
.ENDIF		# "$(GUI)"=="UNX"

.IF "$(WITH_LANG)"!=""
MERGEDXRM=$(COMMONMISC)$/$(TARGET)$/readme.xrm
.ELSE		# "$(WITH_LANG)"!=""
MERGEDXRM=.$/readme.xrm
.ENDIF		# "$(WITH_LANG)"!=""

.INCLUDE : target.mk

ALLTAR : $(SYSTEXTDOCS) $(SYSHTMLDOCS)

$(COMMONMISC)$/readme.dtd : ..$/readme.dtd  
    $(MKDIRHIER) $(MISC)$/$(GUI)
    $(COPY) $< $@

virtual : $(MERGEDXRM) $(COMMONMISC)$/readme.dtd $(PRJ)$/docs/readme.xsl

$(MISC)$/readme_text.xsl : virtual
.IF "$(USE_SHELL)"!="4nt"
     $(SED) 's#method="html".*>#method="text"/>#' < ..$/readme.xsl > $@
.ELSE			# "$(USE_SHELL)"!="4nt"
    $(SED) "s/method=\"html\".*>/method=\"text\"\/>/" < ..$/readme.xsl > $@
.ENDIF			# "$(USE_SHELL)"!="4nt"

$(MISC)$/$(GUI)$/$(eq,$(GUI),WNT readme README)_%.html : 'virtual'
    $(XSLTPROC) --nonet -o $@ \
    --stringparam os1 $(OS)	--stringparam gui1 $(GUI)	--stringparam com1  $(COM) \
    --stringparam cp1 $(CPUNAME)	--stringparam type html	--stringparam lang1 $* \
    ..$/readme.xsl	$(MERGEDXRM)
.IF "$(GUI)"=="UNX"
    chmod g+w $(MISC)$/$(GUI)
.ENDIF			# "$(GUI)"=="UNX"

$(MISC)$/$(GUI)$/$(eq,$(GUI),OS2 readme README)_%.html : 'virtual'
    $(XSLTPROC) --nonet -o $@ \
    --stringparam os1 $(OS)	--stringparam gui1 $(GUI)	--stringparam com1  $(COM) \
    --stringparam cp1 $(CPUNAME)	--stringparam type html	--stringparam lang1 $* \
    ..$/readme.xsl	$(MERGEDXRM)

# no percent-rule to avoid ambiguous inference chains for README_<lang>.html 
$(SYSTEXTDOCS) : $(MISC)$/readme_text.xsl
    $(XSLTPROC) --nonet -o $@ \
    --stringparam os1 $(OS)	--stringparam gui1 $(GUI)	--stringparam com1  $(COM) \
    --stringparam cp1 $(CPUNAME)	--stringparam type text	--stringparam lang1 $(@:b:s/readme_//:s/README_//) \
    $<	$(MERGEDXRM)
.IF "$(GUI)"=="UNX"
    chmod g+w $(MISC)$/$(GUI)
.ENDIF			# "$(GUI)"=="UNX"

