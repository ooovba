/*************************************************************************
 *
 *  $RCSfile: AccessibilityTree.java,v $
 *
 *  $Revision: 1.2 $
 *
 *  last change: $Author: obr $ $Date: 2003/09/19 09:21:39 $
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *  Copyright: 2000 by Sun Microsystems, Inc.
 *
 *  All Rights Reserved.
 *
 *  Contributor(s): _______________________________________
 *
 *
 ************************************************************************/

package org.openoffice.accessibility.awb.tree;

import org.openoffice.accessibility.misc.NameProvider;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

import com.sun.star.awt.XExtendedToolkit;
import com.sun.star.accessibility.XAccessible;
import com.sun.star.accessibility.XAccessibleContext;

/**
 *
 */
public class AccessibilityTree extends javax.swing.JTree {
    
    /** Creates a new instance of AccessibilityTree */
    public AccessibilityTree(javax.swing.tree.TreeModel model) {
        super(model);
        // always show handles to indicate expandable / collapsable
        showsRootHandles = true;
    }
    
    public void setToolkit(XExtendedToolkit xToolkit) {
        AccessibilityModel model = (AccessibilityModel) getModel();
        if (model != null) {
            // hide the root node when connected
            setRootVisible(xToolkit == null);
            // update the root node
            model.setRoot(xToolkit);
            model.reload();
        }
    }
    
     public String convertValueToText(Object value, boolean selected,  
            boolean expanded, boolean leaf, int row, boolean hasFocus) {

        if (value instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

            Object userObject = node.getUserObject();
            if (userObject != null && userObject instanceof XAccessible) {
                XAccessible xAccessible = (XAccessible) userObject;
                try {
                    XAccessibleContext xAC = xAccessible.getAccessibleContext();
                    if (xAC != null) {
                        String name = xAC.getAccessibleName();
                        if (name.length() == 0) {
                            name = new String ("<no name>");
                        }
                        value = name + " / " + NameProvider.getRoleName(xAC.getAccessibleRole());
                    }
                } catch (com.sun.star.uno.RuntimeException e) {
                    value = "???";
                }
            }
        }
            
        return super.convertValueToText(value, selected, expanded, leaf, row, hasFocus);
     }

}
