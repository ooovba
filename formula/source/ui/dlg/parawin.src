/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: parawin.src,v $
 * $Revision: 1.30 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "parawin.hrc"
#include "ForResId.hrc"
#include "helpids.hrc"

#define STD_MASKCOLOR Color { Red=0xFFFF; Green=0x0000; Blue=0xFFFF; }
 //---------------------------------------------------------------------------

#define FT_ARGBLOCK( y )					\
    Pos = MAP_APPFONT (6 , y ) ; \
    Size = MAP_APPFONT ( 74 , 8 ) ; \
    Right = TRUE ;

#define FXBUTTONBLOCK( y )				\
    Pos = MAP_APPFONT (83 , y-1 ) ; \
    Size = MAP_APPFONT ( 13 , 15 ) ; \
    TabStop = TRUE ; \
    ButtonImage = Image\
    {\
        ImageBitmap = Bitmap\
        {\
            File = "fx.bmp" ; \
        };\
        MaskColor = STD_MASKCOLOR;\
    };

#define ED_ARGBLOCK( y )				\
        Border = TRUE;					\
        Pos = MAP_APPFONT( 98, y );		\
        Size = MAP_APPFONT( 66, 12 );	\
        TabStop = TRUE;

#define RB_ARGBLOCK( y )				\
    Pos = MAP_APPFONT ( 166 , y-1 ) ; \
    Size = MAP_APPFONT ( 13 , 15 ) ; \
    TabStop = FALSE ; 	\
    QuickHelpText [ en-US ] = "Select";	\

 //---------------------------------------------------------------------------

 //	jetzt alles zusammen

TabPage RID_FORMULATAB_PARAMETER
{
    Border	= FALSE;
    Size	= MAP_APPFONT( 203, 128 );
    DialogControl        = TRUE;
    SVLook               = TRUE;

    FixedText FT_EDITDESC
    {
        Pos = MAP_APPFONT (3 , 6 ) ;
        Size = MAP_APPFONT ( 198 , 20 ) ;
        WordBreak = TRUE ;
        Text [ en-US ] = "Function not known";
    };

    FixedText FT_PARNAME
    {
        Pos = MAP_APPFONT ( 3, 29) ;
        Size = MAP_APPFONT ( 198 , 10 ) ;
    };
    FixedText FT_PARDESC
    {
        Pos = MAP_APPFONT ( 3, 42 ) ;
        Size = MAP_APPFONT ( 198 , 20 ) ;
        WordBreak = TRUE ;
    };

    FixedText FT_ARG1 { FT_ARGBLOCK (  66 ) };
    FixedText FT_ARG2 { FT_ARGBLOCK (  81 ) };
    FixedText FT_ARG3 { FT_ARGBLOCK (  96 ) };
    FixedText FT_ARG4 { FT_ARGBLOCK ( 111 ) };

    ImageButton BTN_FX1
    {
        HelpId=HID_FORMULA_FAP_BTN_FX1;
        FXBUTTONBLOCK (  64 )
    };
    ImageButton BTN_FX2
    {
        HelpId=HID_FORMULA_FAP_BTN_FX2;
        FXBUTTONBLOCK (  79 )
    };

    ImageButton BTN_FX3
    {
        HelpId=HID_FORMULA_FAP_BTN_FX3;
        FXBUTTONBLOCK (  94 )
    };
    ImageButton BTN_FX4
    {
        HelpId=HID_FORMULA_FAP_BTN_FX4;
        FXBUTTONBLOCK ( 109 )
    };
    Edit ED_ARG1 { ED_ARGBLOCK (  64 ) };
    Edit ED_ARG2 { ED_ARGBLOCK (  79 ) };
    Edit ED_ARG3 { ED_ARGBLOCK (  94 ) };
    Edit ED_ARG4 { ED_ARGBLOCK ( 109 ) };

    ImageButton RB_ARG1
    {
        HelpId=HID_FORMULA_FAP_BTN_REF1;
        RB_ARGBLOCK (  64 )
    };

    ImageButton RB_ARG2
    {
        HelpId=HID_FORMULA_FAP_BTN_REF2;
        RB_ARGBLOCK (  79 )
    };
    ImageButton RB_ARG3
    {
        HelpId=HID_FORMULA_FAP_BTN_REF3;
        RB_ARGBLOCK (  94 )
    };
    ImageButton RB_ARG4
    {
        HelpId=HID_FORMULA_FAP_BTN_REF4;
        RB_ARGBLOCK ( 109 )
    };

    ScrollBar WND_SLIDER
    {
        Pos = MAP_APPFONT ( 183, 63 ) ;
        Size = MAP_APPFONT ( 8 , 59 ) ;
        VScroll = TRUE ;
    };

    String STR_OPTIONAL
    {
        Text [ en-US ] = "(optional)" ;
    };
    String STR_REQUIRED
    {
        Text [ en-US ] = "(required)" ;
    };

    Image IMG_FX_H
    {
        ImageBitmap = Bitmap { File = "fx_h.bmp" ; };
        MaskColor = STD_MASKCOLOR;
    };
    
};


















