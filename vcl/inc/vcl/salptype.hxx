/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: salptype.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SV_SALPTYPE_HXX
#define _SV_SALPTYPE_HXX

#include <vcl/sv.h>

// --------------------
// - SalJobSetupFlags -
// --------------------

#define SAL_JOBSET_ORIENTATION					((ULONG)0x00000001)
#define SAL_JOBSET_PAPERBIN 					((ULONG)0x00000002)
#define SAL_JOBSET_PAPERSIZE					((ULONG)0x00000004)
#define SAL_JOBSET_ALL							(SAL_JOBSET_ORIENTATION | SAL_JOBSET_PAPERBIN | SAL_JOBSET_PAPERSIZE)

// -------------------
// - SalPrinterError -
// -------------------

#define SAL_PRINTER_ERROR_GENERALERROR			1
#define SAL_PRINTER_ERROR_ABORT 				2

// -------------------
// - SalPrinterProcs -
// -------------------

class SalPrinter;
typedef long (*SALPRNABORTPROC)( void* pInst, SalPrinter* pPrinter );

#endif // _SV_SALPTYPE_HXX
