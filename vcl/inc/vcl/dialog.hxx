/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: dialog.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SV_DIALOG_HXX
#define _SV_DIALOG_HXX

#include <vcl/sv.h>
#include <vcl/dllapi.h>
#include <vcl/syswin.hxx>

// parameter to pass to the dialogue constructor if really no parent is wanted
// whereas NULL chooses the deafult dialogue parent
#define DIALOG_NO_PARENT ((Window*)0xffffffff)

// ----------
// - Dialog -
// ----------

struct DialogImpl;

class VCL_DLLPUBLIC Dialog : public SystemWindow
{
private:
    Window* 		mpDialogParent;
    Dialog* 		mpPrevExecuteDlg;
    DialogImpl*     mpDialogImpl;
    long			mnMousePositioned;
    BOOL			mbInExecute;
    BOOL			mbOldSaveBack;
    BOOL			mbInClose;
    BOOL			mbModalMode;
	sal_Int8		mnCancelClose;  //liuchen 2009-7-22, support Excel VBA UserForm_QueryClose event

    SAL_DLLPRIVATE void    ImplInitDialogData();
    SAL_DLLPRIVATE void    ImplInitSettings();

//#if 0 // _SOLAR__PRIVATE
    // Copy assignment is forbidden and not implemented.
    SAL_DLLPRIVATE         Dialog (const Dialog &);
    SAL_DLLPRIVATE         Dialog & operator= (const Dialog &);

protected:
    using Window::ImplInit;
    SAL_DLLPRIVATE void    ImplInit( Window* pParent, WinBits nStyle );
    SAL_DLLPRIVATE void    ImplDialogRes( const ResId& rResId );
    SAL_DLLPRIVATE void    ImplCenterDialog();

public:
    SAL_DLLPRIVATE BOOL    IsInClose() const { return mbInClose; }
//#endif

protected:
                    Dialog( WindowType nType );
    virtual void	Draw( OutputDevice* pDev, const Point& rPos, const Size& rSize, ULONG nFlags );

public:
                    Dialog( Window* pParent, WinBits nStyle = WB_STDDIALOG );
                    Dialog( Window* pParent, const ResId& rResId );
    virtual         ~Dialog();

    virtual long	Notify( NotifyEvent& rNEvt );
    virtual void	StateChanged( StateChangedType nStateChange );
    virtual void	DataChanged( const DataChangedEvent& rDCEvt );

    virtual BOOL	Close();

    virtual short	Execute();
    BOOL			IsInExecute() const { return mbInExecute; }

	sal_Int8		GetCloseFlag() const { return mnCancelClose; }  //liuchen 2009-7-22, support Excel VBA UserForm_QueryClose event
	void			SetCloseFlag( sal_Int8 nCancel ) { mnCancelClose = nCancel; }  //liuchen 2009-7-22, support Excel VBA UserForm_QueryClose event

    ////////////////////////////////////////
    // Dialog::Execute replacement API
public:
    // Link impl: DECL_LINK( MyEndDialogHdl, Dialog* ); <= param is dialog just ended
    virtual void    StartExecuteModal( const Link& rEndDialogHdl );
    BOOL            IsStartedModal() const;
    long            GetResult() const;
private:
    BOOL            ImplStartExecuteModal();
    void            ImplEndExecuteModal();
public:

    // Dialog::Execute replacement API
    ////////////////////////////////////////

    void			EndDialog( long nResult = 0 );
    static void		EndAllDialogs( Window* pParent=NULL );

    void            GetDrawWindowBorder( sal_Int32& rLeftBorder, sal_Int32& rTopBorder,
                                         sal_Int32& rRightBorder, sal_Int32& rBottomBorder ) const;

    void			SetModalInputMode( BOOL bModal );
    void			SetModalInputMode( BOOL bModal, BOOL bSubModalDialogs );
    BOOL			IsModalInputMode() const { return mbModalMode; }

    void			GrabFocusToFirstControl();
};

// ------------------
// - ModelessDialog -
// ------------------

class VCL_DLLPUBLIC ModelessDialog : public Dialog
{
//#if 0 // _SOLAR__PRIVATE
    // Copy assignment is forbidden and not implemented.
    SAL_DLLPRIVATE         ModelessDialog (const ModelessDialog &);
    SAL_DLLPRIVATE         ModelessDialog & operator= (const ModelessDialog &);
//#endif

public:
                    ModelessDialog( Window* pParent, WinBits nStyle = WB_STDMODELESS );
                    ModelessDialog( Window* pParent, const ResId& rResId );
};

// ---------------
// - ModalDialog -
// ---------------

class VCL_DLLPUBLIC ModalDialog : public Dialog
{
public:
                    ModalDialog( Window* pParent, WinBits nStyle = WB_STDMODAL );
                    ModalDialog( Window* pParent, const ResId& rResId );

private:
    using Window::Show;
    void			Show( BOOL bVisible = TRUE );
    using Window::Hide;
    void			Hide();

//#if 0 // _SOLAR__PRIVATE
    // Copy assignment is forbidden and not implemented.
    SAL_DLLPRIVATE         ModalDialog (const ModalDialog &);
    SAL_DLLPRIVATE         ModalDialog & operator= (const ModalDialog &);
//#endif
};

#endif	// _SV_DIALOG_HXX
