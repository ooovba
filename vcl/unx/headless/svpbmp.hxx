/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svpbmp.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SVP_SVBMP_HXX
#define SVP_SVBMP_HXX

#include <vcl/salbmp.hxx>
#include "svpelement.hxx"

class SvpSalBitmap : public SalBitmap, public SvpElement
{
    basebmp::BitmapDeviceSharedPtr     m_aBitmap;
public:
    SvpSalBitmap() {}
    virtual ~SvpSalBitmap();
    
    const basebmp::BitmapDeviceSharedPtr& getBitmap() const { return m_aBitmap; }
    void setBitmap( const basebmp::BitmapDeviceSharedPtr& rSrc ) { m_aBitmap = rSrc; }
    
    // SvpElement
    virtual const basebmp::BitmapDeviceSharedPtr& getDevice() const { return m_aBitmap; }
    
    // SalBitmap
    virtual bool			Create( const Size& rSize, 
                                    USHORT nBitCount, 
                                    const BitmapPalette& rPal );
    virtual bool			Create( const SalBitmap& rSalBmp );
    virtual bool			Create( const SalBitmap& rSalBmp, 
                                    SalGraphics* pGraphics );
    virtual bool			Create( const SalBitmap& rSalBmp,
                                    USHORT nNewBitCount );
    virtual bool            Create( const ::com::sun::star::uno::Reference< ::com::sun::star::rendering::XBitmapCanvas > xBitmapCanvas,
                                    Size& rSize,
                                    bool bMask = false );
    virtual void			Destroy();
    virtual Size			GetSize() const;
    virtual USHORT			GetBitCount() const;
                        
    virtual BitmapBuffer*	AcquireBuffer( bool bReadOnly );
    virtual void			ReleaseBuffer( BitmapBuffer* pBuffer, bool bReadOnly );
    virtual bool            GetSystemData( BitmapSystemData& rData );

};

#endif
