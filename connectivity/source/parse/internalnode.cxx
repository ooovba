/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: internalnode.cxx,v $
 * $Revision: 1.7 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_connectivity.hxx"
#include "internalnode.hxx"

#include <algorithm>
#include <connectivity/sqlparse.hxx>

using namespace connectivity;

//-----------------------------------------------------------------------------
OSQLInternalNode::OSQLInternalNode(const sal_Char* pNewValue,
                                   SQLNodeType eNodeType,
                                   sal_uInt32 nNodeID)
                 : OSQLParseNode(pNewValue,eNodeType,nNodeID)
{
    OSL_ENSURE(OSQLParser::s_pGarbageCollector, "Collector not initialized");
    (*OSQLParser::s_pGarbageCollector)->push_back(this);
}

//-----------------------------------------------------------------------------
OSQLInternalNode::OSQLInternalNode(const ::rtl::OString &_NewValue,
                                 SQLNodeType eNodeType,
                                 sal_uInt32 nNodeID)
                :OSQLParseNode(_NewValue,eNodeType,nNodeID)
{
    OSL_ENSURE(OSQLParser::s_pGarbageCollector, "Collector not initialized");
    (*OSQLParser::s_pGarbageCollector)->push_back(this);
}

//-----------------------------------------------------------------------------
OSQLInternalNode::OSQLInternalNode(const sal_Unicode* pNewValue,
                                   SQLNodeType eNodeType,
                                   sal_uInt32 nNodeID)
                 :OSQLParseNode(pNewValue,eNodeType,nNodeID)
{
    OSL_ENSURE(OSQLParser::s_pGarbageCollector, "Collector not initialized");
    (*OSQLParser::s_pGarbageCollector)->push_back(this);
}

//-----------------------------------------------------------------------------
OSQLInternalNode::OSQLInternalNode(const ::rtl::OUString &_NewValue,
                                 SQLNodeType eNodeType,
                                 sal_uInt32 nNodeID)
                :OSQLParseNode(_NewValue,eNodeType,nNodeID)
{
    OSL_ENSURE(OSQLParser::s_pGarbageCollector, "Collector not initialized");
    (*OSQLParser::s_pGarbageCollector)->push_back(this);
}


//-----------------------------------------------------------------------------
OSQLInternalNode::~OSQLInternalNode()
{
    // remove the node from the garbage list

    OSL_ENSURE(OSQLParser::s_pGarbageCollector, "Collector not initialized");
    (*OSQLParser::s_pGarbageCollector)->erase(this);
}
