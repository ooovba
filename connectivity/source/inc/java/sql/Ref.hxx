/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Ref.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _CONNECTIVITY_JAVA_SQL_REF_HXX_
#define	_CONNECTIVITY_JAVA_SQL_REF_HXX_

#include "java/lang/Object.hxx"
#include <com/sun/star/sdbc/XRef.hpp>
#include <cppuhelper/implbase1.hxx>

namespace connectivity
{
    //**************************************************************
    //************ Class: java.sql.Ref
    //**************************************************************
    class java_sql_Ref :	public java_lang_Object,
                            public ::cppu::WeakImplHelper1< ::com::sun::star::sdbc::XRef>
    {
    protected:
    // statische Daten fuer die Klasse
        static jclass theClass;
        virtual ~java_sql_Ref();
    public:
        virtual jclass getMyClass() const;
        
        // ein Konstruktor, der fuer das Returnen des Objektes benoetigt wird:
        java_sql_Ref( JNIEnv * pEnv, jobject myObj );

        // XRef
        virtual ::rtl::OUString SAL_CALL getBaseTypeName(  ) throw(::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException);
    };
}
#endif // _CONNECTIVITY_JAVA_SQL_REF_HXX_

