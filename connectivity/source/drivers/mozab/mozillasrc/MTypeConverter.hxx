/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: MTypeConverter.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#ifndef _CONNECTIVITY_MAB_TYPECONVERTER_HXX_
#define _CONNECTIVITY_MAB_TYPECONVERTER_HXX_

#include <rtl/ustring.hxx>

#include<string>
 
namespace connectivity
{
    namespace mozab
    {
        class MTypeConverter
        {
        public:
            static void  ouStringToNsString(const ::rtl::OUString&, nsString&);
            static void  nsStringToOUString(const nsString&, ::rtl::OUString&);
            static void  prUnicharToOUString(const PRUnichar*, ::rtl::OUString&);
            // Use free() for the following 3 calls.
            static char *ouStringToCCharStringAscii(const ::rtl::OUString&);
            static char *nsStringToCCharStringAscii(const nsString&);
            static char *ouStringToCCharStringUtf8(const ::rtl::OUString&);
            // Convert to stl-string.
            static ::std::string ouStringToStlString(const ::rtl::OUString&);
            static ::std::string nsStringToStlString(const nsString&);

            static ::rtl::OUString nsACStringToOUString( const nsACString& _source );
            static ::rtl::OString nsACStringToOString( const nsACString& _source );
            static void asciiOUStringToNsACString( const ::rtl::OUString& _asciiString, nsACString& _dest );
            static void asciiToNsACString( const sal_Char* _asciiString, nsACString& _dest );

        private:
            MTypeConverter() {};
        };
    }
}

#endif // _CONNECTIVITY_MAB_TYPECONVERTER_HXX_

