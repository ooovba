/*************************************************************************
 *
 *  $RCSfile: pq_xview.cxx,v $
 *
 *  $Revision: 1.1.2.5 $
 *
 *  last change: $Author: jbu $ $Date: 2007/01/07 13:50:38 $
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Joerg Budischewski
 *
 *   Copyright: 2000 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 *   Contributor(s): Joerg Budischewski
 *
 *
 ************************************************************************/

#include <rtl/ustrbuf.hxx>

#include <cppuhelper/typeprovider.hxx>
#include <cppuhelper/queryinterface.hxx>

#include <com/sun/star/beans/PropertyAttribute.hpp>

#include <com/sun/star/sdbc/XRow.hpp>
#include <com/sun/star/sdbc/XParameters.hpp>

#include "pq_xview.hxx"
#include "pq_xviews.hxx"
#include "pq_statics.hxx"
#include "pq_tools.hxx"

using osl::MutexGuard;
using osl::Mutex;

using rtl::OUString;
using rtl::OUStringBuffer;
using rtl::OUStringToOString;

using com::sun::star::container::XNameAccess;
using com::sun::star::container::XIndexAccess;
using com::sun::star::container::ElementExistException;
using com::sun::star::container::NoSuchElementException;

using com::sun::star::uno::Reference;
using com::sun::star::uno::Exception;
using com::sun::star::uno::UNO_QUERY;
using com::sun::star::uno::XInterface;
using com::sun::star::uno::Sequence;
using com::sun::star::uno::Any;
using com::sun::star::uno::makeAny;
using com::sun::star::uno::Type;
using com::sun::star::uno::RuntimeException;

using com::sun::star::lang::IllegalArgumentException;
using com::sun::star::lang::IndexOutOfBoundsException;

using com::sun::star::beans::XPropertySetInfo;
using com::sun::star::beans::XFastPropertySet;
using com::sun::star::beans::XMultiPropertySet;
using com::sun::star::beans::XPropertySet;
using com::sun::star::beans::Property;

using com::sun::star::sdbc::XResultSet;
using com::sun::star::sdbc::XPreparedStatement;
using com::sun::star::sdbc::XStatement;
using com::sun::star::sdbc::XParameters;
using com::sun::star::sdbc::XRow;
using com::sun::star::sdbc::SQLException;

namespace pq_sdbc_driver
{
#define ASCII_STR(x) OUString( RTL_CONSTASCII_USTRINGPARAM( x ) )

View::View( const ::rtl::Reference< RefCountedMutex > & refMutex,
            const Reference< com::sun::star::sdbc::XConnection > & connection,
            ConnectionSettings *pSettings)
    : ReflectionBase(
        getStatics().refl.view.implName,
        getStatics().refl.view.serviceNames,
        refMutex,
        connection,
        pSettings,
        * getStatics().refl.view.pProps )
{}

Reference< XPropertySet > View::createDataDescriptor(  ) throw (RuntimeException)
{
    ViewDescriptor * pView = new ViewDescriptor(
        m_refMutex, m_conn, m_pSettings );
    pView->copyValuesFrom( this );

    return Reference< XPropertySet > ( pView );
}

void View::rename( const ::rtl::OUString& newName )
        throw (::com::sun::star::sdbc::SQLException,
               ::com::sun::star::container::ElementExistException,
               ::com::sun::star::uno::RuntimeException)
{
    MutexGuard guard( m_refMutex->mutex );

    Statics & st = getStatics();

    ::rtl::OUString oldName = extractStringProperty(this,st.NAME );
    ::rtl::OUString schema = extractStringProperty(this,st.SCHEMA_NAME );
    ::rtl::OUString fullOldName = concatQualified( schema, oldName );

    OUString newTableName;
    OUString newSchemaName;
    // OOo2.0 passes schema + dot + new-table-name while
    // OO1.1.x passes new Name without schema
    // in case name contains a dot, it is interpreted as schema.tablename
    if( newName.indexOf( '.' ) >= 0 )
    {
        splitConcatenatedIdentifier( newName, &newSchemaName, &newTableName );
    }
    else
    {
        newTableName = newName;
        newSchemaName = schema;
    }
    ::rtl::OUString fullNewName = concatQualified( newSchemaName, newTableName );
    
    if( ! schema.equals( newSchemaName ) )
    {
        try
        {
            OUStringBuffer buf(128);
            buf.appendAscii( RTL_CONSTASCII_STRINGPARAM( "ALTER TABLE" ) );
            bufferQuoteQualifiedIdentifier(buf, schema, oldName );
            buf.appendAscii( RTL_CONSTASCII_STRINGPARAM("SET SCHEMA" ) );
            bufferQuoteIdentifier( buf, newSchemaName );
            Reference< XStatement > statement = m_conn->createStatement();
            statement->executeUpdate( buf.makeStringAndClear() );
            setPropertyValue_NoBroadcast_public( st.SCHEMA_NAME, makeAny(newSchemaName) );
            disposeNoThrow( statement );
            schema = newSchemaName;
        }
        catch( com::sun::star::sdbc::SQLException &e )
        {
            OUStringBuffer buf( e.Message );
            buf.appendAscii( RTL_CONSTASCII_STRINGPARAM( "(NOTE: Only postgresql server >= V8.1 support changing a table's schema)" ) );
            e.Message = buf.makeStringAndClear();
            throw e;
        }

    }
    if( ! oldName.equals( newTableName ) )
    {
        OUStringBuffer buf(128);
        buf.appendAscii( RTL_CONSTASCII_STRINGPARAM( "ALTER TABLE" ) );
        bufferQuoteQualifiedIdentifier( buf, schema, oldName );
        buf.appendAscii( RTL_CONSTASCII_STRINGPARAM("RENAME TO" ) );
        bufferQuoteIdentifier( buf, newTableName );
        Reference< XStatement > statement = m_conn->createStatement();
        statement->executeUpdate( buf.makeStringAndClear() );
        setPropertyValue_NoBroadcast_public( st.NAME, makeAny(newTableName) );
    }

    // inform the container of the name change !
    if( m_pSettings->views.is() )
    {
        m_pSettings->pViewsImpl->rename( fullOldName, fullNewName );
    }
}
    
Sequence<Type > View::getTypes() throw( RuntimeException )
{
    static cppu::OTypeCollection *pCollection;
    if( ! pCollection )
    {
        MutexGuard guard( osl::Mutex::getGlobalMutex() );
        if( !pCollection )
        {
            static cppu::OTypeCollection collection(
                getCppuType( (Reference< com::sun::star::sdbcx::XRename> *) 0 ),
                ReflectionBase::getTypes());
            pCollection = &collection;
        }
    }
    return pCollection->getTypes();
}

Sequence< sal_Int8> View::getImplementationId() throw( RuntimeException )
{
    return getStatics().refl.view.implementationId;
}

Any View::queryInterface( const Type & reqType ) throw (RuntimeException)
{
    Any ret;

    ret = ReflectionBase::queryInterface( reqType );
    if( ! ret.hasValue() )
        ret = ::cppu::queryInterface(
            reqType,
            static_cast< com::sun::star::sdbcx::XRename * > ( this )
            );
    return ret;
}

::rtl::OUString View::getName(  ) throw (::com::sun::star::uno::RuntimeException)
{
    Statics & st = getStatics();
    return concatQualified(
        extractStringProperty( this, st.SCHEMA_NAME ),
        extractStringProperty( this, st.NAME ) );
}

void View::setName( const ::rtl::OUString& aName ) throw (::com::sun::star::uno::RuntimeException)
{
    rename( aName );
}

//____________________________________________________________________________________________

ViewDescriptor::ViewDescriptor(
    const ::rtl::Reference< RefCountedMutex > & refMutex,
    const Reference< com::sun::star::sdbc::XConnection > & connection,
    ConnectionSettings *pSettings)
    : ReflectionBase(
        getStatics().refl.viewDescriptor.implName,
        getStatics().refl.viewDescriptor.serviceNames,
        refMutex,
        connection,
        pSettings,
        * getStatics().refl.viewDescriptor.pProps )
{}

Reference< XPropertySet > ViewDescriptor::createDataDescriptor(  ) throw (RuntimeException)
{
    ViewDescriptor * pView = new ViewDescriptor(
        m_refMutex, m_conn, m_pSettings );
    pView->copyValuesFrom( this );

    return Reference< XPropertySet > ( pView );
}


}
