/*************************************************************************
 *
 *  $RCSfile: pq_sequenceresultset.cxx,v $
 *
 *  $Revision: 1.1.2.3 $
 *
 *  last change: $Author: jbu $ $Date: 2006/01/22 15:14:31 $
 *
 *  The Contents of this file are made available subject to the terms of
 *  either of the following licenses
 *
 *         - GNU Lesser General Public License Version 2.1
 *         - Sun Industry Standards Source License Version 1.1
 *
 *  Sun Microsystems Inc., October, 2000
 *
 *  GNU Lesser General Public License Version 2.1
 *  =============================================
 *  Copyright 2000 by Sun Microsystems, Inc.
 *  901 San Antonio Road, Palo Alto, CA 94303, USA
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License version 2.1, as published by the Free Software Foundation.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 *
 *
 *  Sun Industry Standards Source License Version 1.1
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.1 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://www.openoffice.org/license.html.
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *  The Initial Developer of the Original Code is: Joerg Budischewski
 *
 *   Copyright: 2000 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 *   Contributor(s): Joerg Budischewski
 *
 *
 ************************************************************************/

#include "pq_sequenceresultset.hxx"
#include "pq_sequenceresultsetmetadata.hxx"


using rtl::OUString;

using com::sun::star::sdbc::XResultSetMetaData;

using com::sun::star::uno::Sequence;
using com::sun::star::uno::Reference;
using com::sun::star::uno::Any;

namespace pq_sdbc_driver
{
#define ASCII_STR(x) OUString( RTL_CONSTASCII_USTRINGPARAM( x ) )

void SequenceResultSet::checkClosed()
    throw ( com::sun::star::sdbc::SQLException,
            com::sun::star::uno::RuntimeException )
{
    // we never close :o)
}
        

Any SequenceResultSet::getValue( sal_Int32 columnIndex )
{
    m_wasNull = ! m_data[m_row][columnIndex -1 ].hasValue();
    return m_data[m_row][columnIndex -1 ];
}

SequenceResultSet::SequenceResultSet(
    const ::rtl::Reference< RefCountedMutex > & mutex,
    const com::sun::star::uno::Reference< com::sun::star::uno::XInterface > &owner,
    const Sequence< OUString > &colNames,
    const Sequence< Sequence< Any > > &data,
    const Reference< com::sun::star::script::XTypeConverter > & tc,
    const ColumnMetaDataVector *pVec) :
    BaseResultSet( mutex, owner, data.getLength(), colNames.getLength(),tc ),
    m_data(data ),
    m_columnNames( colNames )
{
    if( pVec )
    {
        m_meta = new SequenceResultSetMetaData( mutex, *pVec, m_columnNames.getLength() );
    }
}

SequenceResultSet::~SequenceResultSet()
{

}
    
void SequenceResultSet::close(  )
    throw (::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException)
{
    // a noop
}

Reference< XResultSetMetaData > SAL_CALL SequenceResultSet::getMetaData(  )
    throw (::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException)
{
    if( ! m_meta.is() )
    {
        // Oh no, not again
        throw ::com::sun::star::sdbc::SQLException(
            ASCII_STR( "pq_sequenceresultset: no meta supported " ), *this,
            OUString(), 1, Any() );
    }
    return m_meta;
}


sal_Int32 SAL_CALL SequenceResultSet::findColumn(
    const ::rtl::OUString& columnName )
    throw (::com::sun::star::sdbc::SQLException, ::com::sun::star::uno::RuntimeException)
{
    // no need to guard, as all members are readonly !
    sal_Int32 ret = -1;
    for( int i = 0 ;i < m_fieldCount ; i ++ )
    {
        if( columnName == m_columnNames[i] )
        {
            ret = i+1;
            break;
        }
    }
    return ret;
}
}
