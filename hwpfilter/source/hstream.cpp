/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: hstream.cpp,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <string.h>
#include <stdlib.h>
#include "hstream.h"

HStream::HStream() : size(0), pos(0)
{
    seq = 0;
}


HStream::~HStream()
{
    if( seq )
        free( seq );
}


void HStream::addData( const byte *buf, int aToAdd)
{
    seq = (byte *)realloc( seq, size + aToAdd );
    memcpy( seq + size, buf, aToAdd );
    size += aToAdd;
}


int HStream::readBytes(byte * buf, int aToRead)
{
    if (aToRead >= (size - pos))
        aToRead = size - pos;
    for (int i = 0; i < aToRead; i++)
        buf[i] = seq[pos++];
    return aToRead;
}


int HStream::skipBytes(int aToSkip)
{
    if (aToSkip >= (size - pos))
        aToSkip = size - pos;
    pos += aToSkip;
    return aToSkip;
}


int HStream::available()
{
    return size - pos;
}


void HStream::closeInput()
{
}
