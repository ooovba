/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: precompiled_reportdesign.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): Generated on 2006-09-01 17:49:38.561560

#ifdef PRECOMPILED_HEADERS
#include "com/sun/star/sdb/CommandType.hpp"
//#include "com/sun/star/chart2/data/DatabaseDataProvider.hpp"
#include "com/sun/star/chart2/data/XDataReceiver.hpp"
#include "com/sun/star/reflection/XProxyFactory.hpp"
#include "com/sun/star/sdb/CommandType.hpp"
#include "comphelper/sequence.hxx"
#include "comphelper/sequenceashashmap.hxx"
#include "comphelper/documentconstants.hxx"
#include "xmloff/attrlist.hxx"
#include "xmloff/xmltoken.hxx"
#include "xmloff/xmlement.hxx"
#include "xmloff/xmluconv.hxx"
#include "xmloff/xmltkmap.hxx"
#include "xmloff/xmlnmspe.hxx"
#endif
