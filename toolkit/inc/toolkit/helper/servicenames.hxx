/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: servicenames.hxx,v $
 * $Revision: 1.12 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _TOOLKIT_HELPER_SERVICENAMES_HXX_
#define _TOOLKIT_HELPER_SERVICENAMES_HXX_

#include <sal/types.h>
#include <tools/solar.h>

extern const sal_Char __FAR_DATA szServiceName_Toolkit[], szServiceName2_Toolkit[];
extern const sal_Char __FAR_DATA szServiceName_MVCIntrospection[], szServiceName2_MVCIntrospection[];
extern const sal_Char __FAR_DATA szServiceName_PopupMenu[], szServiceName2_PopupMenu[];
extern const sal_Char __FAR_DATA szServiceName_MenuBar[], szServiceName2_MenuBar[];
extern const sal_Char __FAR_DATA szServiceName_Pointer[], szServiceName2_Pointer[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlContainer[], szServiceName2_UnoControlContainer[];
extern const sal_Char __FAR_DATA szServiceName_UnoMultiPageControl[], szServiceName2_UnoMultiPageControl[];
extern const sal_Char __FAR_DATA szServiceName_UnoMultiPageModel[], szServiceName2_UnoMultiPageModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlContainerModel[], szServiceName2_UnoControlContainerModel[];
extern const sal_Char __FAR_DATA szServiceName_TabController[], szServiceName2_TabController[];
extern const sal_Char __FAR_DATA szServiceName_TabControllerModel[], szServiceName2_TabControllerModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlDialog[], szServiceName2_UnoControlDialog[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlDialogModel[], szServiceName2_UnoControlDialogModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlEdit[], szServiceName2_UnoControlEdit[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlEditModel[], szServiceName2_UnoControlEditModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFileControl[], szServiceName2_UnoControlFileControl[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFileControlModel[], szServiceName2_UnoControlFileControlModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlButton[], szServiceName2_UnoControlButton[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlButtonModel[], szServiceName2_UnoControlButtonModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlImageButton[], szServiceName2_UnoControlImageButton[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlImageButtonModel[], szServiceName2_UnoControlImageButtonModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlImageControl[], szServiceName2_UnoControlImageControl[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlImageControlModel[], szServiceName2_UnoControlImageControlModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlRadioButton[], szServiceName2_UnoControlRadioButton[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlRadioButtonModel[], szServiceName2_UnoControlRadioButtonModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlCheckBox[], szServiceName2_UnoControlCheckBox[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlCheckBoxModel[], szServiceName2_UnoControlCheckBoxModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlListBox[], szServiceName2_UnoControlListBox[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlListBoxModel[], szServiceName2_UnoControlListBoxModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlComboBox[], szServiceName2_UnoControlComboBox[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlComboBoxModel[], szServiceName2_UnoControlComboBoxModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFixedText[], szServiceName2_UnoControlFixedText[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFixedTextModel[], szServiceName2_UnoControlFixedTextModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlGroupBox[], szServiceName2_UnoControlGroupBox[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlGroupBoxModel[], szServiceName2_UnoControlGroupBoxModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlDateField[], szServiceName2_UnoControlDateField[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlDateFieldModel[], szServiceName2_UnoControlDateFieldModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlTimeField[], szServiceName2_UnoControlTimeField[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlTimeFieldModel[], szServiceName2_UnoControlTimeFieldModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlNumericField[], szServiceName2_UnoControlNumericField[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlNumericFieldModel[], szServiceName2_UnoControlNumericFieldModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlCurrencyField[], szServiceName2_UnoControlCurrencyField[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlCurrencyFieldModel[], szServiceName2_UnoControlCurrencyFieldModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlPatternField[], szServiceName2_UnoControlPatternField[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlPatternFieldModel[], szServiceName2_UnoControlPatternFieldModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFormattedField[], szServiceName2_UnoControlFormattedField[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFormattedFieldModel[], szServiceName2_UnoControlFormattedFieldModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlProgressBar[], szServiceName2_UnoControlProgressBar[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlProgressBarModel[], szServiceName2_UnoControlProgressBarModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlScrollBar[], szServiceName2_UnoControlScrollBar[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlScrollBarModel[], szServiceName2_UnoControlScrollBarModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFixedLine[], szServiceName2_UnoControlFixedLine[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFixedLineModel[], szServiceName2_UnoControlFixedLineModel[];
extern const sal_Char __FAR_DATA szServiceName_PrinterServer[], szServiceName2_PrinterServer[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlRoadmap[], szServiceName2_UnoControlRoadmap[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlRoadmapModel[], szServiceName2_UnoControlRoadmapModel[];

extern const sal_Char __FAR_DATA szServiceName_UnoSpinButtonControl[], szServiceName_UnoSpinButtonModel[];

extern const sal_Char __FAR_DATA szServiceName_TreeControl[];
extern const sal_Char __FAR_DATA szServiceName_TreeControlModel[];
extern const sal_Char __FAR_DATA szServiceName_MutableTreeDataModel[];

extern const sal_Char __FAR_DATA szServiceName_GridControl[];
extern const sal_Char __FAR_DATA szServiceName_GridControlModel[];
extern const sal_Char __FAR_DATA szServiceName_DefaultGridDataModel[];
extern const sal_Char __FAR_DATA szServiceName_DefaultGridColumnModel[];
extern const sal_Char __FAR_DATA szServiceName_GridColumn[];

extern const sal_Char __FAR_DATA szServiceName_UnoSimpleAnimationControl[], szServiceName_UnoSimpleAnimationControlModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoThrobberControl[], szServiceName_UnoThrobberControlModel[];
extern const sal_Char __FAR_DATA szServiceName_UnoControlFixedHyperlink[], szServiceName_UnoControlFixedHyperlinkModel[];

// ExtUnoWrapper:
extern const char __FAR_DATA szServiceName_ImageProducer[], szServiceName2_ImageProducer[];



#endif // _TOOLKIT_HELPER_SERVICENAMES_HXX_

