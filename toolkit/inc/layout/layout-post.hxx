/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: layout-post.hxx,v $
 *
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _LAYOUT_POST_HXX
#define _LAYOUT_POST_HXX

#if ENABLE_LAYOUT

/* Allow re-inclusion for cxx file. */
#undef _LAYOUT_PRE_HXX


#undef AdvancedButton
#undef ApplyButton
#undef Box
#undef Button
#undef CancelButton
#undef CheckBox
#undef ComboBox
#undef Container
#undef Control
#undef Dialog
#undef Edit
#undef ErrorBox
#undef FixedImage
#undef FixedInfo
#undef FixedLine
#undef FixedText
#undef HBox
#undef HelpButton
#undef IgnoreButton
#undef ImageButton
#undef InfoBox
#undef ListBox
#undef MessBox
#undef MessageBox
#undef MetricField
#undef MetricFormatter
#undef MoreButton
#undef MultiLineEdit
#undef MultiListBox
#undef NoButton
#undef NumericField
#undef NumericFormatter
#undef OKButton
#undef Plugin
#undef ProgressBar
#undef PushButton
#undef QueryBox
#undef RadioButton
#undef ResetButton
#undef RetryButton
#undef SfxTabPage
#undef SfxTabDialog
#undef SpinField
#undef TabDialog
#undef TabControl
#undef TabPage
#undef Table
#undef VBox
#undef WarningBox
#undef YesButton

#undef SvxFontListBox
#undef SvxLanguageBox

#undef ModalDialog
#undef ModelessDialog
#undef ScExpandedFixedText
#undef SfxDialog
#undef SfxModalDialog
#undef SfxModelessDialog

#undef Window

#endif /* ENABLE_LAYOUT */

#endif /* _LAYOUT_POST_HXX */
