/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: getprocessworkingdir.hxx,v $
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef INCLUDED_TOOLS_GETPROCESSWORKINGDIR_HXX
#define INCLUDED_TOOLS_GETPROCESSWORKINGDIR_HXX

#include "sal/config.h"

#include "tools/toolsdllapi.h"

namespace rtl { class OUString; }

namespace tools {

// get the process's current working directory, taking OOO_CWD into account
//
// @param url
// a non-null pointer that receives the directory URL (with or without a final
// slash) upon successful return, and the empty string upon unsuccessful return
TOOLS_DLLPUBLIC bool getProcessWorkingDir(rtl::OUString * url);

}

#endif
