/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: agapi.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _AGAPI_HXX
#define _AGAPI_HXX

#include <tools/solar.h>

class INetURLObject;
class ChannelList;
class AgentItem;
class String;

class AgentApi
{
protected:
    friend class ChannelList;

    AgentItem*			pChannelAgent;
    virtual BOOL		StartAgent() = 0;

    AgentApi(AgentItem* pAgent) { pChannelAgent = pAgent; }

public:
    virtual ~AgentApi() {}

    virtual void	InitAgent() = 0;
    virtual void	ShutDownAgent() = 0;

    virtual BOOL	NewDataPermission(const String& rChannelName) = 0;
    virtual void 	NewData(const String& rChannelName,
                        const INetURLObject& rURL) = 0;
    virtual void	NotifyChannelObjFile(const INetURLObject& rURL,
                        const String& rFileName) = 0;
    virtual void	NotifyChannelObjData(const INetURLObject& rURL,
                        void* pBuffer, long nOffset, long nLen, long nTotalLen) = 0;

    virtual void	RegisterChannels() = 0;
    virtual void 	RegisterUpdateTransmitter() = 0;
};

#endif //_AGAPI_HXX

