/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: urlkeys.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __URLKEYS_HXX
#define __URLKEYS_HXX

// Defines for common keys in URL files

// ANSI version

#define A_URLSECTION_SHORTCUT	"InternetShortcut"
#define A_URLKEY_URL			"URL"
#define A_URLKEY_TITLE			"Title"
#define A_URLKEY_TARGET			"Target"
#define A_URLKEY_FRAME			"Frame"
#define A_URLKEY_OPENAS			"OpenAs"
#define A_URLKEY_SOICON			"SOIcon"
#define A_URLKEY_WIN_ICONFILE	"IconFile"
#define A_URLKEY_WIN_ICONINDEX	"IconIndex"
#define A_URLKEY_WORKDIR		"WorkingDirectory"
#define A_URLKEY_ARGUMENTS		"Arguments"
#define A_URLKEY_INTERN_ORIGURL	"[URL]"

// Unicode version

#define U_URLSECTION_SHORTCUT	L"InternetShortcut"
#define U_URLKEY_URL			L"URL"
#define U_URLKEY_TITLE			L"Title"
#define U_URLKEY_TARGET			L"Target"
#define U_URLKEY_FRAME			L"Frame"
#define U_URLKEY_OPENAS			L"OpenAs"
#define U_URLKEY_SOICON			L"SOIcon"
#define U_URLKEY_WIN_ICONFILE	L"IconFile"
#define U_URLKEY_WIN_ICONINDEX	L"IconIndex"
#define U_URLKEY_WORKDIR		L"WorkingDirectory"
#define U_URLKEY_ARGUMENTS		L"Arguments"
#define U_URLKEY_INTERN_ORIGURL	L"[URL]"

#	define URLSECTION_SHORTCUT	U_URLSECTION_SHORTCUT
#	define URLKEY_URL			U_URLKEY_URL
#	define URLKEY_TITLE			U_URLKEY_TITLE
#	define URLKEY_TARGET		U_URLKEY_TARGET
#	define URLKEY_FRAME			U_URLKEY_FRAME
#	define URLKEY_OPENAS		U_URLKEY_OPENAS
#	define URLKEY_SOICON		U_URLKEY_SOICON
#	define URLKEY_WIN_ICONFILE	U_URLKEY_WIN_ICONFILE
#	define URLKEY_WIN_ICONINDEX	U_URLKEY_WIN_ICONINDEX
#	define URLKEY_WORKDIR		U_URLKEY_WORKDIR
#	define URLKEY_ARGUMENTS		U_URLKEY_ARGUMENTS
#	define URLKEY_INTERN_ORIGURL U_URLKEY_INTERN_ORIGURL

#endif // __URLKEYS_HXX

