/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: agitem.hxx,v $
 * $Revision: 1.3 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _AGITEM_HXX
#define _AGITEM_HXX

#include <tools/solar.h>

class String;
class INetURLObject;
class AgentApi;
class ChApi;

class AgentItem
{
public:
    virtual const String&			GetChAgentName() const = 0;
    virtual const INetURLObject&	GetLocation() const = 0;
    virtual AgentApi*				GetApi() const = 0;
    virtual ChApi*					GetChApi() const = 0;

    virtual void   					SetIsActive(BOOL bNew) = 0;
    virtual BOOL   					IsActive() const = 0;
};

#endif //_AGITEM_HXX

