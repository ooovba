/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: precompiled_tools.hxx,v $
 * $Revision: 1.4 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): Generated on 2006-09-01 17:50:15.029610

#ifdef PRECOMPILED_HEADERS
//---MARKER---
#include "sal/config.h"
#include "sal/types.h"

#include "basegfx/point/b2dpoint.hxx"
#include "basegfx/polygon/b2dpolygon.hxx"
#include "basegfx/polygon/b2dpolygontools.hxx"
#include "basegfx/polygon/b2dpolypolygon.hxx"
#include "basegfx/polygon/b2dpolypolygontools.hxx"
#include "basegfx/vector/b2dvector.hxx"

#include "boost/static_assert.hpp"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/util/XStringWidth.hpp"

#include "comphelper/fileformat.h"

#include "cppuhelper/implbase1.hxx"

#include "i18npool/lang.h"
#include "i18npool/mslangid.hxx"

#include "osl/diagnose.h"
#include "osl/endian.h"
#include "osl/file.hxx"
#include "osl/interlck.h"
#include "osl/module.h"
#include "osl/module.hxx"
#include "osl/mutex.hxx"
#include "osl/process.h"
#include "osl/security.h"
#include "osl/thread.h"

#include "rtl/alloc.h"
#include "rtl/crc.h"
#include "rtl/digest.h"
#include "rtl/instance.hxx"
#include "rtl/logfile.hxx"
#include "rtl/math.hxx"
#include "rtl/memory.h"
#include "rtl/string.h"
#include "rtl/string.hxx"
#include "rtl/tencinfo.h"
#include "rtl/textcvt.h"
#include "rtl/textenc.h"
#include "rtl/ustrbuf.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

#include "sys/stat.h"
#include "sys/types.h"

#include "vos/macros.hxx"
#include "vos/mutex.hxx"
#include "vos/process.hxx"
#include "vos/signal.hxx"
#include "vos/timer.hxx"
//---MARKER---
#endif
