/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: svborder.cxx,v $
 * $Revision: 1.6 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// MARKER(update_precomp.py): autogen include statement, do not remove
#include "precompiled_tools.hxx"

#include <tools/svborder.hxx>
#include <osl/diagnose.h>

SvBorder::SvBorder( const Rectangle & rOuter, const Rectangle & rInner )
{
    Rectangle aOuter( rOuter );
    aOuter.Justify();
    Rectangle aInner( rInner );
    if( aInner.IsEmpty() )
        aInner = Rectangle( aOuter.Center(), aOuter.Center() );
    else
        aInner.Justify();

    OSL_ENSURE( aOuter.IsInside( aInner ),
                "SvBorder::SvBorder: FALSE == aOuter.IsInside( aInner )" );
    nTop	= aInner.Top()	  - aOuter.Top();
    nRight	= aOuter.Right()  - aInner.Right();
    nBottom = aOuter.Bottom() - aInner.Bottom();
    nLeft	= aInner.Left()   - aOuter.Left();
}

Rectangle & operator += ( Rectangle & rRect, const SvBorder & rBorder )
{
    // wegen Empty-Rect, GetSize muss als erstes gerufen werden
    Size aS( rRect.GetSize() );
    aS.Width()	+= rBorder.Left() + rBorder.Right();
    aS.Height() += rBorder.Top() + rBorder.Bottom();

    rRect.Left()   -= rBorder.Left();
    rRect.Top()    -= rBorder.Top();
    rRect.SetSize( aS );
    return rRect;
}

Rectangle & operator -= ( Rectangle & rRect, const SvBorder & rBorder )
{
    // wegen Empty-Rect, GetSize muss als erstes gerufen werden
    Size aS( rRect.GetSize() );
    aS.Width()	-= rBorder.Left() + rBorder.Right();
    aS.Height() -= rBorder.Top() + rBorder.Bottom();

    rRect.Left()   += rBorder.Left();
    rRect.Top()    += rBorder.Top();
    rRect.SetSize( aS );
    return rRect;
}

