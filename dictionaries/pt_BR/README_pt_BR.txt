Autor/Author: Raimundo Moura <raimundomoura@openoffice.org>

pt-BR: Este dicion�rio est� em desenvolvimento por Raimundo Moura e sua equipe. Ele est� licenciado sob os termos da Licen�a P�blica Geral Menor vers�o 2.1 (LGPLv2.1), como publicado pela Free Software Foundation. Os cr�ditos est�o dispon�veis em http://www.broffice.org/creditos e voc� pode encontrar novas vers�es em http://www.broffice.org/verortografico.

en-US: This dictionary is under development by Raimundo Moura and his team. It is licensed under the terms of the GNU Lesser General Public License version 2.1 (LGPLv2.1), as published by the Free Software Foundation. The credits are available at http://www.broffice.org/creditos and you can find new releases at http://www.broffice.org/verortografico.


Copyright (C) 2006 - 2007 por/by Raimundo Santos Moura <raimundomoura@openoffice.org>

=============
APRESENTA��O
=============

O Projeto Verificador Ortogr�fico do BrOffice.org � um projeto
colaborativo desenvolvido pela comunidade Brasileira.
A rela��o completa dos colaboradores deste projeto est� em:
http://www.broffice.org.br/creditos

***********************************************************************
* Este � um dicion�rio para corre��o ortogr�fica da l�ngua Portuguesa *
* para o Myspell.                                                     *
* Este programa � livre e pode ser redistribu�do e/ou modificado nos  *
* termos da GNU Lesser General Public License (LGPL) vers�o 2.1.      *
*                                                                     *
***********************************************************************

======================
SOBRE ESTA ATUALIZA��O
======================
. Inclus�o das regras para a forma��o das �nclises e Mes�clises;
. Nesta atualiza��o foram analisados 1.849 termos, em sua maioria 
  par�nimos. Foram encontradas palavras grafadas com mais de uma
  forma, a exemplo de 'charreu' e charr�u, e eliminadas as incorretas 
  (Colabora��o de Leonardo Fontenelle).
. Revis�o e exclus�o de 1.113 nomes pr�prios duplicados; 
. Exclus�o de associa��es err�neas de termos � regras (colabora��o de
  Leonardo Fontenelle), exemplos: a��o/K, ligeiro/M, duplex/B, etc;  
. Inclus�o de algumas letras como '�' e '�' para corre��o de falha na 
  vers�o 2.3 do BrOffice.org;
. Corre��o do ap�strofo em algumas palavras com d' como: 
  a�ucenas-d'�gua e pai-d'�gua, cuja grafia apresentava o d�
  (asento agudo ao inv�s do ap�strofo). Leonardo Fontenelle;
. Inclus�o dos termos 'triangulariza��o','dinamicidade', 'mestranda',
  colabora��o de Leo Barichello;
. Inclus�o do  termo 'cochilos', colabora��o de Rafael da Fonseca 
  Duarte;
. Inclus�o dos termos 'couch�', 'pr�-obliterado', 'pr�-filatelia' e 
  'regomado', colabora��o de Carlos Dalmiro Silva Soares;
. Cria��o do Tem�tico Filat�lico por Carlos Dalmiro Silva Soares;
. Corre��o da conjuga��o de verbos terminados em ear com som aberto,
  como: idear e estrear. Colabora��o Hort�ncia Maria Moura;
. Inclus�o do g�nero feminino dos numerais ordinais como:
  ducent�sima, trecent�sima, tricent�sima, quadringent�sima, 
  q�ingent�sima, sexcent�sima, seiscent�sima, septingent�sima,
  octingent�sima e nongent�sima;
. Inclus�o ds siglas dos Estados de Alagoas (AL) e Amap� (AP);
. Inclus�o de algumas palavras no g�nero feminino, tais como: 
  oficiala, hipop�tama, faquiresa, etc.
. Exclus�o da forma singular da palavra 'v�veres';
. Corre��o na regra de conjuga��o de verbos terminados em: aguar,
  equar e inquar. Exemplos adequar(ad�q�e,ad�q�em), desaguar (des�g�em),
  minguar (m�ng�e);
. Corre��o de 'arg�eiro' e 'desmiling�ir' (trema);
. Corre��o da conjuga��o dos verbos haver e reaver
  inclus�o de 'hemos' e 'heis', e exlus�o de 'reei',
  're�s', re� e re�o;
. Exclus�o de 'corma' e 'satividade', colabora��o Leo Barichello;
. Exclus�o de 'indentar' verbo e derivados;
. Inclus�o de 'hidroplanagem','biodisponibilidade';
. Corre��o de termos compostos com prefixos: Neo, Contra,
  ultra, semi, proto, supra, pseudo, auto, intra, extra,
  infra. Evitando-se erros como: semianalfabeto,
  infraestrutura, semi�rido, etc.
. Cria��o do Tem�tico Microbiologia por Gerv�sio Paulo da Silva;
. Inclus�o dos termos 'historicamente' e 'Finl�ndia', colabora��o de 
  S�rgio A. Elarrat Canto;
. Exclus�o de 1.309 termos terminados em 'icamente' derivados de
  palavras acentuadas terminadas em 'ia'. Aplica��o err�nea da regra;  
 
=======================================================
COMO INSTALAR O VERIFICADOR BRASILEIRO NO BROFFICE.ORG
=======================================================

Copie os arquivos pt_BR.dic e pt_BR.aff para o diret�rio <BrOffice.org>
/share/dict/ooo, onde <BrOffice.org> � o diret�rio em que o programa 
foi instalado.

No Windows, normalmente, o caminho � este: 
C:\Arquivos de programas\BrOffice.org 2.0\share\dict\ooo, e no  Linux
/opt/BrOffice.org/share/dict/ooo/.

No mesmo diret�rio, localize o arquivo dictionary.lst. Abra-o com um
editor de textos e acrescente a seguinte linha ao final(se n�o
existir):

DICT pt BR pt_BR

� necess�rio reiniciar o BrOffice, inclusive o in�cio r�pido da vers�o
para Windows que fica na barra de tarefas, para que o corretor
funcione.

===================
D�VIDAS FREQUENTES
===================

Os arquivos foram copiados mas o Verificador n�o est� funcionando.
O Verificador Ortogr�fico n�o deve estar configurado corretamente,
isto pode estar ocorrendo por um dos seguintes motivos:

1- O dicion�rio provavelmente n�o est� instalado.

Para se certificar de que est� utilizando o idioma correto confira como
est�o as informa��es em: Ferramentas >> Op��es >>   Configura��es de
Idioma >> Idiomas. O item Ocidental deve apresentar o dicion�rio
selecionado (deve aparecer um logo "Abc" do lado do idioma).

Se n�o estiver Portugu�s (Brasil) mude para esse idioma. Ap�s
configurado clique em 'OK'.
Feche o BrOffice, inclusive o Iniciador R�pido,  e em seguida reabra-o;


2 - O verificador n�o est� configurado para verificar texto ao digitar.
Neste caso confira como est�o as informa��es em: Ferramentas >> Op��es
>> Configura��es de Idiomas >> Recursos de Verifica��o Ortogr�fica e, 
no campo op��es deste formul�rio marque a op��o 'Verificar texto ao 
digitar';


Novas atualiza��es estar�o dispon�veis no site do BrOffice.Org, na
p�gina do Verificador Ortogr�fico.

http://www.openoffice.org.br/?q=verortografico


============
INTRODUCTION
============

The BrOffice.org Orthography Checker is a colaborative project developed
by the Brazilian community.
The complete list of participants in this project is at
http://www.broffice.org.br/creditos

***********************************************************************
* This is a dictionary for orthography correction for the Portuguese  *
* language for Myspell.                                               *
* This is a free program and it can be redistributed and/or           *
* modified under the terms of the GNU Lesser General Public License   *
* (LGPL) version 2.1.                                                 *
*                                                                     *
***********************************************************************

=================
ABOUT THIS UPDATE
=================

==============================================================
HOW TO INSTALL THE BRAZILIAN ORTOGRAPH CHECKER IN BROFFICE.ORG
==============================================================

Copy the files pt_BR.dic and pt_BR.aff to the directory <BrOffice.org>
/share/dict/ooo, where <BrOffice.org> is the directory where the software
has been installed.

In Windows, usually, the path is
C:\Arquivos de programas\BrOffice.org 2.0\share\dict\ooo, and in GNU/Linux
/opt/BrOffice.org/share/dict/ooo/.

In the same directory, locate the file dictionary.lst. Open it with a
text editor e add the following line to the end of the file (if it is
not already there):

DICT pt BR pt_BR

It is necessary to restart BrOffice, including the fast start for the Windows version
that resides on the task bar, in order to have the orthography checker to work.


==========================
FREQUENTLY ASKED QUESTIONS
==========================

The files have been copied but the checker is not working. The orthography checker may not be
configured correctly, this may be due to one of the following reasons:

1- The dictionary is probably not installed.

To make sure that you are using the right language, check the information at
Ferramentas >> Op��es >>  Configura��es de Idioma >> Idiomas.
The item "Ocidental" must present the selected dictionary (a logo "Abc" should
appear beside the language).
If the language selected is not "Portugu�s (Brasil)" change to this language.
After the configuration is correct, click on 'OK'.
Close BrOffice and the fast start, and open it afterwards;

2 - The checker is not configured to verify the orthography on typing. For this

problem, check the information at
"Ferramentas >> Op��es >> Configura��es de Idiomas >> Recursos de Verifica��o Ortogr�fica"
and, in the field "Op��es" of this form, check the option ''Verificar texto ao digitar';

New updates will be available at the BrOffice.Org website, on the page of the
Orthography Checker.

http://www.broffice.org/verortografico

