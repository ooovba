<?xml version='1.0' encoding='UTF-8'?>
<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: WriterWindowState.xcu,v $
 * $Revision: 1.28 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************ -->
<!DOCTYPE oor:component-data SYSTEM "../../../../../component-update.dtd">
<oor:component-data oor:name="WriterWindowState" oor:package="org.openoffice.Office.UI" xmlns:install="http://openoffice.org/2004/installation" xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <node oor:name="UIElements">
        <node oor:name="States">
            <node oor:name="private:resource/toolbar/standardbar" oor:op="replace">
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,0</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Standard</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/textobjectbar" oor:op="replace">
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Formatting</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/toolbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Tools</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/tableobjectbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Table</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/numobjectbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Bullets and Numbering</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/drawingobjectbar" oor:op="replace">
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Drawing Object Properties</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/alignmentbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Align</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/bezierobjectbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Edit Points</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/extrusionobjectbar" oor:op="replace">
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">3D-Settings</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/formtextobjectbar" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Text Box Formatting</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/formsfilterbar" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Form Filter</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="NoClose" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/formsnavigationbar" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Form Navigation</value>
                </prop>
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>1</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/formcontrols" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Form Controls</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/moreformcontrols" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">More Controls</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/formdesign" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Form Design</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>1</value>
                </prop>
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,2</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/frameobjectbar" oor:op="replace">
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Frame</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/fullscreenbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Style" oor:type="xs:int">
                    <value>2</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Full Screen</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="NoClose" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/graffilterbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Graphic Filter</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/graphicobjectbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Picture</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/insertbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Insert</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/oleobjectbar" oor:op="replace">
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">OLE-Object</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/optimizetablebar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Optimize</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/previewobjectbar" oor:op="replace">
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Page Preview</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="NoClose" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/drawtextobjectbar" oor:op="replace">
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Text Object</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/viewerbar" oor:op="replace">
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>0</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Standard (Viewing Mode)</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="NoClose" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/drawbar" oor:op="replace">
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Drawing</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/mediaobjectbar" oor:op="replace">
                <prop oor:name="DockPos" oor:type="xs:string">
                    <value>0,1</value>
                </prop>
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="DockingArea" oor:type="xs:int">
                    <value>1</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Media Playback</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/colorbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Color</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/basicshapes" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Basic Shapes</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/arrowshapes" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Block Arrows</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/flowchartshapes" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Flowchart</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/starshapes" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Stars and Banners</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/symbolshapes" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Symbol Shapes</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/calloutshapes" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Callouts</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/fontworkobjectbar" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Fontwork</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="ContextSensitive" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
            <node oor:name="private:resource/toolbar/fontworkshapetype" oor:op="replace">
                <prop oor:name="Docked" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="UIName" oor:type="xs:string">
                    <value xml:lang="en-US">Fontwork Shape</value>
                </prop>
                <prop oor:name="Visible" oor:type="xs:boolean">
                    <value>false</value>
                </prop>
                <prop oor:name="HideFromToolbarMenu" oor:type="xs:boolean">
                    <value>true</value>
                </prop>
            </node>
        </node>
    </node>
</oor:component-data>
