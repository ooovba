<?xml version='1.0' encoding='UTF-8'?>
<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Effects.xcs,v $
 * $Revision: 1.7.74.1 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************ -->
<!DOCTYPE oor:component-schema SYSTEM "../../../../../component-schema.dtd">
<oor:component-schema oor:name="Effects" oor:package="org.openoffice.Office.UI" xml:lang="en-US" xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <info>
		<author>CD</author>
		<desc>Contains strings to localize animation effects.</desc>
	</info>
	<templates>
		<group oor:name="LabelType">
            <info>
                <desc>Provides a mapping between effect preset ids and their textual representation on the user interface.</desc>
            </info>
			<prop oor:name="Label" oor:type="xs:string" oor:localized="true">
                <info>
                    <desc>A localized text that describes the animation effect.</desc>
                </info>
			</prop>
		</group>
		<group oor:name="PresetCategory">
            <info>
                <desc>Maps a custom animation effect node to an effect category.</desc>
            </info>
			<prop oor:name="Label" oor:type="xs:string" oor:localized="true">
                <info>
                    <desc>A localized text that names the preset category.</desc>
                </info>
			</prop>
			<prop oor:name="Effects" oor:type="oor:string-list" oor:localized="false">
                <info>
                    <desc>A list of custom effect node names</desc>
                </info>
			</prop>
		</group>
	</templates>
	<component>
		<group oor:name="UserInterface">
		    <info>
		        <desc>Contains user interface data for Office commands and identifiers that are used by the user interface."</desc>
		    </info>
			<set oor:name="Effects" oor:node-type="LabelType">
		        <info>
		            <desc>Contains preset ids for effects and identifiers that are used by the user interface."</desc>
		        </info>
			</set>
			<set oor:name="Transitions" oor:node-type="LabelType">
		        <info>
		            <desc>Contains transition ids for transitions and identifiers that are used by the user interface."</desc>
		        </info>
			</set>
			<set oor:name="Properties" oor:node-type="LabelType">
		        <info>
		            <desc>Contains preset ids for effect properties and identifiers that are used by the user interface."</desc>
		        </info>
			</set>
		</group>
		<group oor:name="Presets">
		    <info>
		        <desc>Contains user interface presets for custom animation effects."</desc>
		    </info>
		    <set oor:name="Entrance" oor:node-type="PresetCategory">
			    <info>
			        <desc>Contains the categories for entrance effects."</desc>
			    </info>
		    </set>
		    <set oor:name="Emphasis" oor:node-type="PresetCategory">
			    <info>
			        <desc>Contains the categories for emphasis effects."</desc>
			    </info>
		    </set>
		    <set oor:name="Exit" oor:node-type="PresetCategory">
			    <info>
			        <desc>Contains the categories for exit effects."</desc>
			    </info>
		    </set>
		    <set oor:name="MotionPaths" oor:node-type="PresetCategory">
			    <info>
			        <desc>Contains the categories for motion path effects."</desc>
			    </info>
		    </set>	    
		    <set oor:name="Misc" oor:node-type="PresetCategory">
		        <info>
		           <desc>Contains all the categories that don't fit anywhere else."</desc>
		       </info>
		    </set>
		</group>		
	</component>
</oor:component-schema>

