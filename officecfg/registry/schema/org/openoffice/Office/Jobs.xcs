<?xml version="1.0" encoding="UTF-8"?>
<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Jobs.xcs,v $
 * $Revision: 1.18 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************ -->
<!DOCTYPE oor:component-schema SYSTEM "../../../../component-schema.dtd">
<oor:component-schema xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" oor:name="Jobs" oor:package="org.openoffice.Office" xml:lang="en-US"> <info>
		<author>AS</author>
		<desc >Contains information about registered jobs, bound on events.</desc>
	</info>
	<templates>
		<group oor:name="Job">
            		<info>
				<desc>Describes an UNO service, which is registered for a special event.</desc>
			</info>
            		<prop oor:name="Service" oor:type="xs:string">
                		<info>
					<desc>Must contain an UNO implementation(!) name of the implemented job component.</desc>
				</info>
            		</prop>
            		<group oor:name="Arguments" oor:extensible="true">
                		<info>
					<desc>Can be filled with any argument, which is under control of the job component.</desc>
				</info>
            		</group>
        	</group>
        	<group oor:name="TimeStamp">
            		<info>
				<desc>Is used to enable/disable a job execution related to a triggered event.</desc>
			</info>
            		<prop oor:name="AdminTime" oor:type="xs:string">
                		<info>
					<desc>If it's newer then UserTime, the job will be reactivated next time.</desc>
				</info>
                		<value>2003-01-01T00:00:00+00:00</value>
            		</prop>
            		<prop oor:name="UserTime" oor:type="xs:string">
                		<info>
					<desc>If it's newer then AdminTime, the job is deactivated for execution.</desc>
				</info>
                		<value>2003-01-01T00:00:00+00:00</value>
            		</prop>
        	</group>
        	<group oor:name="Event">
            		<info>
				<desc>Describe a state, which can be detected at runtime and will be used to start jobs, which are registered for it.</desc>
			</info>
            		<set oor:name="JobList" oor:node-type="TimeStamp">
                		<info>
					<desc>This list contains all registered jobs, which wish to be executed, if the coressponding event was triggered.</desc>
				</info>
            		</set>
        	</group>
	</templates>
	<component>
		<set oor:name="Jobs" oor:node-type="Job">
            		<info>
				<desc>This list contains all well known job components and it's properties.</desc>
			</info>
		</set>
		<set oor:name="Events" oor:node-type="Event">
            		<info>
				<desc>Here jobs can be bound to events and can be enabled/disable by using TimeStamp values.</desc>
			</info>
		</set>
	</component>
</oor:component-schema>
