<?xml version='1.0' encoding='UTF-8'?>
<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: Paths.xcs,v $
 * $Revision: 1.5 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************ -->
<!DOCTYPE oor:component-schema SYSTEM "../../../../component-schema.dtd">
<oor:component-schema oor:name="Paths" oor:package="org.openoffice.Office" xml:lang="en-US" xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<info>
        <author>AS</author>
		<desc>Configuration of layered pathes replacing the old path configuration org.openoffice.Office.Common/Path.</desc>
	</info>

	<templates>

	    <group oor:name="MultiPath">
                <info>
                <desc>Describe a path in it's details.</desc>
                </info>
            <prop oor:name="Unused" oor:type="xs:string">
                <info>
                <desc>Because current implementation of configuration sets allow structured types only, these dummy property was inserted.</desc>
                </info>
            </prop>
        </group>

	    <group oor:name="NamedPath">
                <info>
                <desc>Bind a list of path values to a "path name" e.g. "Template".</desc>
                </info>
            <prop oor:name="IsSinglePath" oor:type="xs:boolean">
				<info>
					<desc>Mark a NamedPath as SinglePath (false=MultiPath). Only the property WritePath is used for such single pathes then.</desc>
				</info>
                <value>false</value>
            </prop>
            <set oor:name="InternalPaths" oor:node-type="MultiPath">
                <info>
                <desc>Contains path values configured by an administrator or package (not visible for the user).</desc>
                </info>
            </set>
            <prop oor:name="UserPaths" oor:type="oor:string-list">
                <info>
                <desc>Contains all path values configured by the user under "Tools->Options->Paths".</desc>
                </info>
            </prop>
            <prop oor:name="WritePath" oor:type="xs:string">
                <info>
                <desc>Define one path which is used as the writable path only.</desc>
                </info>
            </prop>
        </group>

	</templates>

	<component>

        <set oor:name="Paths" oor:node-type="NamedPath">
                <info>
                <desc>Contains all named paths of the office.</desc>
                </info>
        </set>

        <group oor:name="Variables">
            <info>
                <desc>Desktop specific defaults for "$" variables used in Paths</desc>
            </info>
            <prop oor:name="Work" oor:type="xs:string">
                <info>
                    <desc>Expanded value of "$work". Must be a valid URL.</desc>
                </info>
            </prop>
        </group>

	</component>

</oor:component-schema>
