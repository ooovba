/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * $RCSfile: solver.src,v $
 * $Revision: 1.2 $
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "solver.hrc"

String RID_SOLVER_COMPONENT
{
    Text [ en-US ] = "%PRODUCTNAME Linear Solver";
};

String RID_PROPERTY_NONNEGATIVE
{
    Text [ en-US ] = "Assume variables as non-negative";
};
String RID_PROPERTY_INTEGER
{
    Text [ en-US ] = "Assume variables as integer";
};
String RID_PROPERTY_TIMEOUT
{
    Text [ en-US ] = "Solving time limit (seconds)";
};
String RID_PROPERTY_EPSILONLEVEL
{
    Text [ en-US ] = "Epsilon level (0-3)";
};
String RID_PROPERTY_LIMITBBDEPTH
{
    Text [ en-US ] = "Limit branch-and-bound depth";
};

String RID_ERROR_NONLINEAR
{
    Text [ en-US ] = "The model is not linear.";
};
String RID_ERROR_EPSILONLEVEL
{
    Text [ en-US ] = "The epsilon level is invalid.";
};
String RID_ERROR_INFEASIBLE
{
    Text [ en-US ] = "The model is infeasible. Check limiting conditions.";
};
String RID_ERROR_UNBOUNDED
{
    Text [ en-US ] = "The model is unbounded.";
};
String RID_ERROR_TIMEOUT
{
    Text [ en-US ] = "The time limit was reached.";
};

