#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2008 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# $RCSfile: interfaceinheritance.tests,v $
#
# $Revision: 1.6 $
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

EXPECT FAILURE "interfaceinheritance.tests 1":
interface Base {};
interface Derived {
    interface Base;
    interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 2":
interface Base {};
interface Derived {
    interface Base;
    [optional] interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 3":
interface Base {};
interface Derived {
    [optional] interface Base;
    interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 4":
interface Base {};
interface Derived {
    [optional] interface Base;
    [optional] interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 5":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    interface Base1;
    interface Base2;
};


EXPECT FAILURE "interfaceinheritance.tests 6":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    interface Base2;
    interface Base1;
};


EXPECT FAILURE "interfaceinheritance.tests 7":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    [optional] interface Base1;
    interface Base2;
};


EXPECT FAILURE "interfaceinheritance.tests 8":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    interface Base2;
    [optional] interface Base1;
};


EXPECT SUCCESS "interfaceinheritance.tests 9":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    interface Base1;
    [optional] interface Base2;
};


EXPECT SUCCESS "interfaceinheritance.tests 10":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    [optional] interface Base2;
    interface Base1;
};


EXPECT SUCCESS "interfaceinheritance.tests 11":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    [optional] interface Base1;
    [optional] interface Base2;
};


EXPECT SUCCESS "interfaceinheritance.tests 12":
interface Base1 {};
interface Base2: Base1 {};
interface Derived {
    [optional] interface Base2;
    [optional] interface Base1;
};


EXPECT SUCCESS "interfaceinheritance.tests 13":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    interface Base1;
    interface Base2;
};


EXPECT SUCCESS "interfaceinheritance.tests 14":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    interface Base2;
    interface Base1;
};


EXPECT FAILURE "interfaceinheritance.tests 15":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    [optional] interface Base1;
    interface Base2;
};


EXPECT FAILURE "interfaceinheritance.tests 16":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    interface Base2;
    [optional] interface Base1;
};


EXPECT SUCCESS "interfaceinheritance.tests 17":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    interface Base1;
    [optional] interface Base2;
};


EXPECT SUCCESS "interfaceinheritance.tests 18":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    [optional] interface Base2;
    interface Base1;
};


EXPECT SUCCESS "interfaceinheritance.tests 19":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    [optional] interface Base1;
    [optional] interface Base2;
};


EXPECT SUCCESS "interfaceinheritance.tests 20":
interface Base1 {};
interface Base2 { [optional] interface Base1; };
interface Derived {
    [optional] interface Base2;
    [optional] interface Base1;
};


EXPECT SUCCESS "interfaceinheritance.tests 21":
interface Base1 {};
interface Base2: Base1 {};
interface Base3: Base1 {};
interface Derived {
    interface Base2;
    interface Base3;
};


EXPECT SUCCESS "interfaceinheritance.tests 22":
interface Base1 {};
interface Base2: Base1 {};
interface Base3: Base1 {};
interface Derived {
    [optional] interface Base2;
    interface Base3;
};


EXPECT SUCCESS "interfaceinheritance.tests 23":
interface Base1 {};
interface Base2: Base1 {};
interface Base3: Base1 {};
interface Derived {
    interface Base2;
    [optional] interface Base3;
};


EXPECT SUCCESS "interfaceinheritance.tests 24":
interface Base1 {};
interface Base2: Base1 {};
interface Base3: Base1 {};
interface Derived {
    [optional] interface Base2;
    [optional] interface Base3;
};


EXPECT SUCCESS "interfaceinheritance.tests 25":
interface Base {};
interface Derived {
    [optional] interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 26":
interface Base;
interface Derived {
    interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 27":
interface Base;
interface Derived {
    [optional] interface Base;
};


EXPECT FAILURE "interfaceinheritance.tests 28":
interface Base {};
typedef Base Hidden;
interface Derived {
    interface Base;
    interface Hidden;
};


EXPECT FAILURE "interfaceinheritance.tests 29":
interface Base {};
typedef Base Hidden;
interface Derived {
    interface Hidden;
    interface Base;
};
