//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by QUICKSTART.RC
//
#define IDR_MAINFRAME					128
#define IDD_QUICKSTART_DIALOG		102
#define IDD_ABOUTBOX					103
#define IDS_APP_TITLE					103
#define IDM_ABOUT						104
#define IDM_EXIT						105
#define IDS_HELLO						106
#define IDI_QUICKSTART	            107
#define IDI_SMALL						108
#define IDC_QUICKSTART	            109
#define IDM_QUICKSTART                  110
#define IDS_TOOLTIP                     111
#define IDS_EXIT                        112
#define ICON_ACTIVE                     1
#define ICON_INACTIVE                   2
#define IDC_MYICON						3
#define IDC_STATIC	                    -1
// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
